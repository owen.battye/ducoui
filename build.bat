@echo off

if ["%DevEnvDir%"] == [""] (
    call "C:\\Program Files\\Microsoft Visual Studio\\2022\\Enterprise\\Common7\\Tools\\VsDevCmd.bat"
)

call nuget restore || exit /b 1
call msbuild DucoUI.sln /property:Configuration=Release /property:Platform=x64 || exit /b 2
call msbuild DucoUI.sln /property:Configuration=Release /property:Platform=x86 || exit /b 3

echo Build successful (Release only, with installer)