#pragma once
#include "DucoWindowState.h"

namespace Duco
{
    class DucoConfiguration;
    class Peal;
}

namespace DucoUI
{
    ref class RingerList;

    public ref class RingerObjectBase :  public System::Windows::Forms::UserControl
    {
    public:
        RingerObjectBase(DucoUI::RingerList^ theparent, unsigned int theBellNumber, bool isAStrapper);
        virtual ~RingerObjectBase();

        virtual System::Void SetState(DucoWindowState state);
        virtual System::Boolean SetRinger(Duco::Peal& newPeal, int index, bool ringerIsConductor, int strapperIndex, bool strapperIsConductor, unsigned int noOfBellsInPeal);
        System::Void SetStrapper();
        virtual System::Void SetNameAndConductor(System::String^ ringerName, bool conductor, bool strapper, int index);
        virtual System::Void CheckConductor(bool isConductor, bool silentAndNonConducted, bool strapperIsConductor);

        inline System::Windows::Forms::ComboBox^ Editor();
        inline unsigned int BellNumber();
        inline System::Boolean IsStrapper();
        inline System::Boolean IsConductor();
        System::Boolean KnownRinger();
        System::Boolean RingerSet();
        virtual System::Boolean SetIndex(System::Boolean /*strapper*/, int index);
        System::String^ RingerText();

        System::Void BeginUpdate(System::Boolean saveOldValues);
        System::Void EndUpdate(DucoUI::RingerDisplayCache^ cache);

    protected:
        System::Void ringerEditor_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void ringerEditor_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void ringerEditor_Validated(System::Object^ sender, System::EventArgs^ e);
        System::Void ringerEditor_MouseWheel(System::Object^ sender, System::Windows::Forms::MouseEventArgs^ e);
        System::Void conductor_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void CheckControlColour();

    private:
        void InitializeComponent();

    protected:
        DucoUI::RingerList^                 parent;
        DucoUI::RingerItem^                 oldItem;
        System::String^                     oldString;
        unsigned int                        bellNumber;
        bool                                isStrapper;

        DucoWindowState                     state;

        System::ComponentModel::Container^  components;

        System::Windows::Forms::ComboBox^   ringerEditor;
        System::Windows::Forms::Label^      ringerLbl;
        System::Windows::Forms::CheckBox^   conductor;
    };

System::Windows::Forms::ComboBox^
RingerObjectBase::Editor()
{
    return ringerEditor;
}

unsigned int
RingerObjectBase::BellNumber()
{
    return bellNumber;
}

System::Boolean
RingerObjectBase::IsStrapper()
{
    return isStrapper;
}

System::Boolean
RingerObjectBase::IsConductor()
{
    return conductor->Checked;
}

}
