#include "RingingDatabaseObserver.h"
#include "DucoWindowState.h"
#include "DatabaseManager.h"
#include <Method.h>
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"
#include <StatisticFilters.h>

#include "ConductedStatsUI.h"

#include "DucoTableSorter.h"
#include <StatisticFilters.h>
#include "FiltersUI.h"
#include "SystemDefaultIcon.h"
#include "DatabaseManager.h"
#include "DucoUtils.h"
#include <RingingDatabase.h>
#include <DucoEngineUtils.h>
#include "SoundUtils.h"
#include <PealDatabase.h>
#include <Peal.h>
#include "MethodUI.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::Windows::Forms;

ConductedStatsUI::ConductedStatsUI(DucoUI::DatabaseManager^ theDatabase)
: database(theDatabase), ordersConductedGenerated(false), conductedMethodGenerated(false), loading(true)
{
    InitializeComponent();
    filters = new Duco::StatisticFilters(database->Database());
    filters->SetDefaultConductor();
}

ConductedStatsUI::!ConductedStatsUI()
{
    delete filters;
    database->RemoveObserver(this);
}

ConductedStatsUI::~ConductedStatsUI()
{
    this->!ConductedStatsUI();
    if (components)
    {
        delete components;
    }
}

System::Void
ConductedStatsUI::ConductedStatsUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    loading = false;
    database->AddObserver(this);
    Restart(ConductedStatsUI::TState::EConductedMethod);
}

System::Void
ConductedStatsUI::filtersBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    FiltersUI^ filtersDialog = gcnew FiltersUI(*filters, database);
    filtersDialog->RequireConductor();
    System::Windows::Forms::DialogResult returnVal = filtersDialog->ShowDialog(this);
    if (returnVal == System::Windows::Forms::DialogResult::OK)
    {
        ordersConductedGenerated = false;
        conductedMethodGenerated = false;

        ordersConductedData->Rows->Clear();
        methodsConductedData->Rows->Clear();

        Restart(methodsConductedData->Visible ? ConductedStatsUI::TState::EConductedMethod : ConductedStatsUI::TState::EConductedOrders);
    }
}


void
ConductedStatsUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    switch (type)
    {
    case TObjectType::EPeal:
        ordersConductedGenerated = false;
        conductedMethodGenerated = false;
        break;
    case TObjectType::EMethod:
        conductedMethodGenerated = false;
        break;

    default:
        break;
    }
}


System::Void
ConductedStatsUI::ringerSelector_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    ordersConductedGenerated = false;
    conductedMethodGenerated = false;

    ordersConductedData->Rows->Clear();
    methodsConductedData->Rows->Clear();

    Restart(methodsConductedData->Visible ? ConductedStatsUI::TState::EConductedMethod : ConductedStatsUI::TState::EConductedOrders);
}

System::Void
ConductedStatsUI::TabChangedOrdersConducted(System::Object^  sender, System::EventArgs^  e)
{
    if (!ordersConductedGenerated && ordersConductedData->Visible)
    {
        ordersConductedData->Rows->Clear();
        Restart(ConductedStatsUI::TState::EConductedOrders);
    }
}

System::Void
ConductedStatsUI::TabChangedMethodsConducted(System::Object^  sender, System::EventArgs^  e)
{
    if (!conductedMethodGenerated && methodsConductedData->Visible)
    {
        methodsConductedData->Rows->Clear();
        Restart(ConductedStatsUI::TState::EConductedMethod);
    }
}

System::Void
ConductedStatsUI::Restart(ConductedStatsUI::TState newState)
{
    if (state == newState || loading)
        return;
    if (backgroundWorker->IsBusy)
    {
        backgroundWorker->CancelAsync();
        nextState = newState;
    }
    else
    {
        state = newState;
        ConductedStatsUIArgs^ args = gcnew ConductedStatsUIArgs;
        args->newState = newState;
        backgroundWorker->RunWorkerAsync(args);
        this->Cursor = Cursors::WaitCursor;
    }
}

System::Void
ConductedStatsUI::backgroundWorker_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^ e)
{
    ConductedStatsUIArgs^ args = (ConductedStatsUIArgs^)e->Argument;

    switch (args->newState)
    {
    case ConductedStatsUI::TState::EConductedOrders:
        GenerateConductedOrderCounts(e);
        break;
    case ConductedStatsUI::TState::EConductedMethod:
        GenerateConductedMethodCounts(e);
        break;

    default:
        break;
    }
    delete args->ringerId;
}

System::Void
ConductedStatsUI::backgroundWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    progressBar->Value = e->ProgressPercentage;
    switch (state)
    {
    case ConductedStatsUI::TState::EConductedOrders:
        ordersConductedData->Rows->Add((DataGridViewRow^)e->UserState);
        break;
    case ConductedStatsUI::TState::EConductedMethod:
        methodsConductedData->Rows->Add((DataGridViewRow^)e->UserState);
        break;
    default:
        break;
    }
}

System::Void
ConductedStatsUI::backgroundWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    if (e->Cancelled)
    {
        TState newState = nextState;
        nextState = TState::ENon;
        Restart(newState);
    }
    else
    {
        switch (state)
        {
        case ConductedStatsUI::TState::EConductedMethod:
            {
            DucoTableSorter^ sorter = gcnew DucoTableSorter(true, EString);
            sorter->AddSortObject(ECount, false, 2);
            methodsConductedData->Sort(sorter);
            DataGridViewColumn^ newColumn = methodsConductedData->Columns[2];
            newColumn->HeaderCell->SortGlyphDirection = SortOrder::Descending;
            if (methodsConductedData->Rows->Count > 0)
                conductedMethodGenerated = true;
            }
        case ConductedStatsUI::TState::EConductedOrders:
            {
            if (ordersConductedData->Rows->Count > 0)
                ordersConductedGenerated = true;
            }
            break;

        default:
            break;
        }
        state = ConductedStatsUI::TState::ENon;
        this->Cursor = Cursors::Arrow;
        progressBar->Value = 0;
    }
}

System::Void
ConductedStatsUI::GenerateConductedOrderCounts(System::ComponentModel::DoWorkEventArgs^ e)
{
    std::map<unsigned int, Duco::PealLengthInfo> sortedPealCounts;
    database->Database().PealsDatabase().GetConductedOrderCount(*filters, sortedPealCounts);
    double noOfRows = double(sortedPealCounts.size());
    double count = 0;
    std::map<unsigned int, Duco::PealLengthInfo>::const_reverse_iterator it = sortedPealCounts.rbegin();
    while (it != sortedPealCounts.rend())
    {
        DataGridViewRow^ newRow = gcnew DataGridViewRow();
        System::String^ orderName = "";
        if (database->FindOrderName(it->first, orderName))
            newRow->HeaderCell->Value = orderName;
        else
            newRow->HeaderCell->Value = Convert::ToString(it->first);
        DataGridViewTextBoxCell^ newCell = gcnew DataGridViewTextBoxCell();
        newCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->second.TotalPeals(), true));
        newRow->Cells->Add(newCell);
        DataGridViewTextBoxCell^ changesCell = gcnew DataGridViewTextBoxCell();
        changesCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->second.TotalChanges(), true));
        newRow->Cells->Add(changesCell);
        DataGridViewTextBoxCell^ timeCell = gcnew DataGridViewTextBoxCell();
        timeCell->Value = DucoUtils::PrintPealTime(it->second.TotalMinutes(), false);
        newRow->Cells->Add(timeCell);
        backgroundWorker->ReportProgress(int((++count / noOfRows)*(double)100), newRow);
        ++it;
    }
}

System::Void
ConductedStatsUI::GenerateConductedMethodCounts(System::ComponentModel::DoWorkEventArgs^ e)
{
    state = ConductedStatsUI::TState::EConductedMethod;
    std::map<Duco::ObjectId, Duco::PealLengthInfo> sortedMethodIds;
    database->Database().PealsDatabase().GetConductedMethodCount(*filters, sortedMethodIds);

    std::multimap<Duco::PealLengthInfo, Duco::ObjectId> sortedPealCounts;
    std::map<Duco::ObjectId, Duco::PealLengthInfo>::const_iterator convIt = sortedMethodIds.begin();
    while (convIt != sortedMethodIds.end())
    {
        std::pair<Duco::PealLengthInfo, Duco::ObjectId> newObject(convIt->second, convIt->first);
        sortedPealCounts.insert(newObject);
        ++convIt;
    }

    double noOfRows = double(sortedPealCounts.size());
    double count = 0;
    size_t position = 0;
    size_t lastCount = -1;

    std::multimap<Duco::PealLengthInfo, Duco::ObjectId>::const_reverse_iterator it = sortedPealCounts.rbegin();
    while (it != sortedPealCounts.rend())
    {
        if (lastCount != it->first.TotalPeals())
        {
            position = int(count) + 1;
            lastCount = it->first.TotalPeals();
        }
        Method theMethod;
        if (database->FindMethod(it->second, theMethod, false))
        {
            DataGridViewRow^ newRow = gcnew DataGridViewRow();
            newRow->HeaderCell->Value = Convert::ToString(position);

            DataGridViewTextBoxCell^ idCell = gcnew DataGridViewTextBoxCell();
            idCell->Value = DucoUtils::ConvertString(it->second);
            newRow->Cells->Add(idCell);
            DataGridViewTextBoxCell^ nameCell = gcnew DataGridViewTextBoxCell();
            nameCell->Value = DucoUtils::ConvertString(theMethod.FullName(database->Database()));
            newRow->Cells->Add(nameCell);
            DataGridViewTextBoxCell^ newCell = gcnew DataGridViewTextBoxCell();
            newCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->first.TotalPeals(), true));
            newRow->Cells->Add(newCell);
            DataGridViewTextBoxCell^ changesCell = gcnew DataGridViewTextBoxCell();
            changesCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->first.TotalChanges(), true));
            newRow->Cells->Add(changesCell);
            DataGridViewTextBoxCell^ timeCell = gcnew DataGridViewTextBoxCell();
            timeCell->Value = DucoUtils::PrintPealTime(it->first.TotalMinutes(), false);
            newRow->Cells->Add(timeCell);
            backgroundWorker->ReportProgress(int((++count / noOfRows)*(double)100), newRow);
        }
        ++it;
    }
}

System::Void
ConductedStatsUI::methodsConductedData_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
    DataGridViewColumn^ newColumn = methodsConductedData->Columns[e->ColumnIndex];
    if (newColumn->SortMode != DataGridViewColumnSortMode::Programmatic)
    {
        methodsConductedData->Columns[2]->HeaderCell->SortGlyphDirection = SortOrder::None;
        return;
    }

    SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;
    if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
        newSortOrder = SortOrder::Ascending;
    else
        newSortOrder = SortOrder::Descending;

    newColumn->HeaderCell->SortGlyphDirection = newSortOrder;

    DucoTableSorter^ sorter = gcnew DucoTableSorter(newSortOrder == SortOrder::Ascending, EString);
    sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
    sorter->AddSortObject(EString, true, 1);
    methodsConductedData->Sort(sorter);
}

System::Void
ConductedStatsUI::ordersConductedData_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
    DataGridViewColumn^ newColumn = ordersConductedData->Columns[e->ColumnIndex];
    if (newColumn->SortMode != DataGridViewColumnSortMode::Programmatic)
    {
        ordersConductedData->Columns[0]->HeaderCell->SortGlyphDirection = SortOrder::None;
        return;
    }

    SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;
    if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
        newSortOrder = SortOrder::Ascending;
    else
        newSortOrder = SortOrder::Descending;

    newColumn->HeaderCell->SortGlyphDirection = newSortOrder;

    DucoTableSorter^ sorter = gcnew DucoTableSorter(newSortOrder == SortOrder::Ascending, EString);
    sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
    ordersConductedData->Sort(sorter);
}

System::Void
ConductedStatsUI::methodsConductedData_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    try
    {
        unsigned int rowIndex = e->RowIndex;
        DataGridViewRow^ currentRow = methodsConductedData->Rows[rowIndex];
        unsigned int methodId = Convert::ToInt16(currentRow->Cells[0]->Value);
        MethodUI::ShowMethod(database, this->MdiParent, methodId);
    }
    catch (Exception^)
    {
        SoundUtils::PlayErrorSound();
    }

}
