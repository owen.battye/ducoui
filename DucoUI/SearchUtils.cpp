#include "SearchUtils.h"

#include "DucoUtils.h"
#include <DatabaseSearch.h>
#include <SearchFieldStringArgument.h>
#include <SearchGroup.h>

using namespace DucoUI;
using namespace System;

System::Void
SearchUtils::AddStringSearchArgument(Duco::DatabaseSearch& search, String^ text,
                                     Duco::TSearchFieldId fieldId, bool excluding)
{
    if (text->Length > 0)
    {
        std::wstring tempStr;
        DucoUtils::ConvertString(text, tempStr);
        Duco::TSearchFieldStringArgument* newStr = new Duco::TSearchFieldStringArgument(fieldId, tempStr, true, excluding);
        search.AddArgument(newStr);
    }
}

System::Void
SearchUtils::AddStringSearchArgument(Duco::DatabaseSearch& search, System::String^ text,
                                     Duco::TSearchFieldId fieldId,
                                     Duco::TSearchFieldId altFieldId, bool excluding)
{
    if (text->Length > 0)
    {
        std::wstring tempStr;
        DucoUtils::ConvertString(text, tempStr);

        Duco::TSearchGroup* group = new Duco::TSearchGroup(Duco::EOrGroup);
        search.AddArgument(group);

        Duco::TSearchFieldStringArgument* newStr = new Duco::TSearchFieldStringArgument(fieldId, tempStr, true, excluding);
        group->AddArgument(newStr);
        Duco::TSearchFieldStringArgument* altNewStr = new Duco::TSearchFieldStringArgument(altFieldId, tempStr, true, excluding);
        group->AddArgument(altNewStr);
    }
}
