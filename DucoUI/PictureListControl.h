#pragma once

namespace DucoUI
{
    public ref class PictureListControl :   public System::Windows::Forms::UserControl,
                                            public DucoUI::RingingDatabaseObserver
    {
    public:
        PictureListControl(DucoUI::DatabaseManager^ theDatabase, DucoUI::IOpenObjectCallback^ theCallback);

        System::Void RefreshList(bool reset, bool move, bool forwards);
        System::Void SetSingleSelectionMode();
        System::Void PictureSelected(System::Windows::Forms::UserControl^ selectedPicture);
        System::Void ShowPicture(Duco::ObjectId& pictureId);

        //from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

    public:
        event System::EventHandler^         closeHandler;
        event System::EventHandler^         pictureSelectedHandler;

    private:
        !PictureListControl();
        ~PictureListControl();

        System::Void startBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void previousPageBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void nextPageBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void endBtn_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void newBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void pasteBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void deleteBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void deleteUnusedBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void selectBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void cancelBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void locationLbl_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void findBtn_Click(System::Object^  sender, System::EventArgs^  e);

        System::UInt16 NumberOfSelectedControls();
        System::Void PicturesToRemove(std::set<Duco::ObjectId>& unusedPictureIds);
        System::Windows::Forms::UserControl^ FindSelectedPicture();

        System::Windows::Forms::FlowLayoutPanel^  flowLayoutPanel1;
        System::UInt16 picturesToShowPerPage;
        DucoUI::DatabaseManager^ database;
        Duco::ObjectId* pictureIdToShow;
        Duco::ObjectId* lastShownPictureId;
        Duco::ObjectId* firstShowPictureOnCurrentPage;
        DucoUI::IOpenObjectCallback^ callback;
        System::Boolean singleSelectionMode;
        System::Boolean updateInProgress;
        System::Windows::Forms::Button^  previousPageBtn;
        System::Windows::Forms::Button^  nextPageBtn;
        System::Windows::Forms::Button^  newBtn;
        System::Windows::Forms::Button^  deleteBtn;
        System::Windows::Forms::ToolTip^  toolTip1;
        System::Windows::Forms::Button^  deleteUnusedBtn;
        System::Windows::Forms::Button^  selectBtn;
        System::Windows::Forms::Button^  cancelBtn;
    private: System::Windows::Forms::Label^  locationLbl;
    private: System::Windows::Forms::Button^  startBtn;
    private: System::Windows::Forms::Button^  endBtn;
    private: System::Windows::Forms::Button^  findBtn;
    private: System::Windows::Forms::Button^ pasteBtn;


           System::ComponentModel::IContainer^ components;

    #pragma region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        void InitializeComponent(void)
        {
            this->components = (gcnew System::ComponentModel::Container());
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
            this->pasteBtn = (gcnew System::Windows::Forms::Button());
            this->findBtn = (gcnew System::Windows::Forms::Button());
            this->startBtn = (gcnew System::Windows::Forms::Button());
            this->endBtn = (gcnew System::Windows::Forms::Button());
            this->flowLayoutPanel1 = (gcnew System::Windows::Forms::FlowLayoutPanel());
            this->previousPageBtn = (gcnew System::Windows::Forms::Button());
            this->nextPageBtn = (gcnew System::Windows::Forms::Button());
            this->newBtn = (gcnew System::Windows::Forms::Button());
            this->deleteBtn = (gcnew System::Windows::Forms::Button());
            this->deleteUnusedBtn = (gcnew System::Windows::Forms::Button());
            this->cancelBtn = (gcnew System::Windows::Forms::Button());
            this->selectBtn = (gcnew System::Windows::Forms::Button());
            this->locationLbl = (gcnew System::Windows::Forms::Label());
            this->toolTip1 = (gcnew System::Windows::Forms::ToolTip(this->components));
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            tableLayoutPanel1->SuspendLayout();
            this->SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 13;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->Controls->Add(this->pasteBtn, 9, 1);
            tableLayoutPanel1->Controls->Add(this->findBtn, 6, 1);
            tableLayoutPanel1->Controls->Add(this->startBtn, 0, 1);
            tableLayoutPanel1->Controls->Add(this->endBtn, 3, 1);
            tableLayoutPanel1->Controls->Add(this->flowLayoutPanel1, 0, 0);
            tableLayoutPanel1->Controls->Add(this->previousPageBtn, 1, 1);
            tableLayoutPanel1->Controls->Add(this->nextPageBtn, 2, 1);
            tableLayoutPanel1->Controls->Add(this->newBtn, 8, 1);
            tableLayoutPanel1->Controls->Add(this->deleteBtn, 10, 1);
            tableLayoutPanel1->Controls->Add(this->deleteUnusedBtn, 11, 1);
            tableLayoutPanel1->Controls->Add(this->cancelBtn, 12, 1);
            tableLayoutPanel1->Controls->Add(this->selectBtn, 7, 1);
            tableLayoutPanel1->Controls->Add(this->locationLbl, 4, 1);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 3;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->Size = System::Drawing::Size(844, 310);
            tableLayoutPanel1->TabIndex = 0;
            // 
            // pasteBtn
            // 
            this->pasteBtn->Location = System::Drawing::Point(523, 284);
            this->pasteBtn->Name = L"pasteBtn";
            tableLayoutPanel1->SetRowSpan(this->pasteBtn, 2);
            this->pasteBtn->Size = System::Drawing::Size(75, 23);
            this->pasteBtn->TabIndex = 11;
            this->pasteBtn->Text = L"Paste";
            this->toolTip1->SetToolTip(this->pasteBtn, L"Create new picture");
            this->pasteBtn->UseVisualStyleBackColor = true;
            this->pasteBtn->Click += gcnew System::EventHandler(this, &PictureListControl::pasteBtn_Click);
            // 
            // findBtn
            // 
            this->findBtn->Location = System::Drawing::Point(280, 284);
            this->findBtn->Name = L"findBtn";
            tableLayoutPanel1->SetRowSpan(this->findBtn, 2);
            this->findBtn->Size = System::Drawing::Size(75, 23);
            this->findBtn->TabIndex = 6;
            this->findBtn->Text = L"Find";
            this->findBtn->UseVisualStyleBackColor = true;
            this->findBtn->Click += gcnew System::EventHandler(this, &PictureListControl::findBtn_Click);
            // 
            // startBtn
            // 
            this->startBtn->AutoSize = true;
            this->startBtn->Location = System::Drawing::Point(3, 284);
            this->startBtn->Margin = System::Windows::Forms::Padding(3, 3, 0, 3);
            this->startBtn->Name = L"startBtn";
            tableLayoutPanel1->SetRowSpan(this->startBtn, 2);
            this->startBtn->Size = System::Drawing::Size(29, 23);
            this->startBtn->TabIndex = 2;
            this->startBtn->Text = L"|<";
            this->toolTip1->SetToolTip(this->startBtn, L"Back to the first picture");
            this->startBtn->UseVisualStyleBackColor = true;
            this->startBtn->Click += gcnew System::EventHandler(this, &PictureListControl::startBtn_Click);
            // 
            // endBtn
            // 
            this->endBtn->AutoSize = true;
            this->endBtn->Location = System::Drawing::Point(96, 284);
            this->endBtn->Margin = System::Windows::Forms::Padding(0, 3, 3, 3);
            this->endBtn->Name = L"endBtn";
            tableLayoutPanel1->SetRowSpan(this->endBtn, 2);
            this->endBtn->Size = System::Drawing::Size(29, 23);
            this->endBtn->TabIndex = 5;
            this->endBtn->Text = L">|";
            this->toolTip1->SetToolTip(this->endBtn, L"Forward to the last picture");
            this->endBtn->UseVisualStyleBackColor = true;
            this->endBtn->Click += gcnew System::EventHandler(this, &PictureListControl::endBtn_Click);
            // 
            // flowLayoutPanel1
            // 
            tableLayoutPanel1->SetColumnSpan(this->flowLayoutPanel1, 13);
            this->flowLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            this->flowLayoutPanel1->Location = System::Drawing::Point(0, 0);
            this->flowLayoutPanel1->Margin = System::Windows::Forms::Padding(0);
            this->flowLayoutPanel1->Name = L"flowLayoutPanel1";
            this->flowLayoutPanel1->Size = System::Drawing::Size(844, 281);
            this->flowLayoutPanel1->TabIndex = 1;
            // 
            // previousPageBtn
            // 
            this->previousPageBtn->AutoSize = true;
            this->previousPageBtn->Location = System::Drawing::Point(32, 284);
            this->previousPageBtn->Margin = System::Windows::Forms::Padding(0, 3, 3, 3);
            this->previousPageBtn->Name = L"previousPageBtn";
            tableLayoutPanel1->SetRowSpan(this->previousPageBtn, 2);
            this->previousPageBtn->Size = System::Drawing::Size(29, 23);
            this->previousPageBtn->TabIndex = 3;
            this->previousPageBtn->Text = L"<<";
            this->toolTip1->SetToolTip(this->previousPageBtn, L"Backwards one page of pictures");
            this->previousPageBtn->UseVisualStyleBackColor = true;
            this->previousPageBtn->Click += gcnew System::EventHandler(this, &PictureListControl::previousPageBtn_Click);
            // 
            // nextPageBtn
            // 
            this->nextPageBtn->AutoSize = true;
            this->nextPageBtn->Location = System::Drawing::Point(67, 284);
            this->nextPageBtn->Margin = System::Windows::Forms::Padding(3, 3, 0, 3);
            this->nextPageBtn->Name = L"nextPageBtn";
            tableLayoutPanel1->SetRowSpan(this->nextPageBtn, 2);
            this->nextPageBtn->Size = System::Drawing::Size(29, 23);
            this->nextPageBtn->TabIndex = 4;
            this->nextPageBtn->Text = L">>";
            this->toolTip1->SetToolTip(this->nextPageBtn, L"Forward one page of pictures");
            this->nextPageBtn->UseVisualStyleBackColor = true;
            this->nextPageBtn->Click += gcnew System::EventHandler(this, &PictureListControl::nextPageBtn_Click);
            // 
            // newBtn
            // 
            this->newBtn->Location = System::Drawing::Point(442, 284);
            this->newBtn->Name = L"newBtn";
            tableLayoutPanel1->SetRowSpan(this->newBtn, 2);
            this->newBtn->Size = System::Drawing::Size(75, 23);
            this->newBtn->TabIndex = 7;
            this->newBtn->Text = L"New";
            this->toolTip1->SetToolTip(this->newBtn, L"Create new picture");
            this->newBtn->UseVisualStyleBackColor = true;
            this->newBtn->Click += gcnew System::EventHandler(this, &PictureListControl::newBtn_Click);
            // 
            // deleteBtn
            // 
            this->deleteBtn->Location = System::Drawing::Point(604, 284);
            this->deleteBtn->Name = L"deleteBtn";
            tableLayoutPanel1->SetRowSpan(this->deleteBtn, 2);
            this->deleteBtn->Size = System::Drawing::Size(75, 23);
            this->deleteBtn->TabIndex = 8;
            this->deleteBtn->Text = L"Delete";
            this->toolTip1->SetToolTip(this->deleteBtn, L"Delete selected pictures - including removing them from performanes.");
            this->deleteBtn->UseVisualStyleBackColor = true;
            this->deleteBtn->Click += gcnew System::EventHandler(this, &PictureListControl::deleteBtn_Click);
            // 
            // deleteUnusedBtn
            // 
            this->deleteUnusedBtn->Location = System::Drawing::Point(685, 284);
            this->deleteUnusedBtn->Name = L"deleteUnusedBtn";
            this->deleteUnusedBtn->Size = System::Drawing::Size(75, 23);
            this->deleteUnusedBtn->TabIndex = 9;
            this->deleteUnusedBtn->Text = L"Del. Unused";
            this->toolTip1->SetToolTip(this->deleteUnusedBtn, L"Delete all unused pictures");
            this->deleteUnusedBtn->UseVisualStyleBackColor = true;
            this->deleteUnusedBtn->Click += gcnew System::EventHandler(this, &PictureListControl::deleteUnusedBtn_Click);
            // 
            // cancelBtn
            // 
            this->cancelBtn->Location = System::Drawing::Point(766, 284);
            this->cancelBtn->Name = L"cancelBtn";
            tableLayoutPanel1->SetRowSpan(this->cancelBtn, 2);
            this->cancelBtn->Size = System::Drawing::Size(75, 23);
            this->cancelBtn->TabIndex = 10;
            this->cancelBtn->Text = L"Cancel";
            this->cancelBtn->UseVisualStyleBackColor = true;
            this->cancelBtn->Click += gcnew System::EventHandler(this, &PictureListControl::cancelBtn_Click);
            // 
            // selectBtn
            // 
            this->selectBtn->Location = System::Drawing::Point(361, 284);
            this->selectBtn->Name = L"selectBtn";
            this->selectBtn->Size = System::Drawing::Size(75, 23);
            this->selectBtn->TabIndex = 10;
            this->selectBtn->Text = L"Select";
            this->selectBtn->UseVisualStyleBackColor = true;
            this->selectBtn->Visible = false;
            this->selectBtn->Click += gcnew System::EventHandler(this, &PictureListControl::selectBtn_Click);
            // 
            // locationLbl
            // 
            this->locationLbl->AutoSize = true;
            this->locationLbl->Location = System::Drawing::Point(131, 289);
            this->locationLbl->Margin = System::Windows::Forms::Padding(3, 8, 3, 0);
            this->locationLbl->Name = L"locationLbl";
            tableLayoutPanel1->SetRowSpan(this->locationLbl, 2);
            this->locationLbl->Size = System::Drawing::Size(35, 13);
            this->locationLbl->TabIndex = 5;
            this->locationLbl->Text = L"label1";
            this->locationLbl->Click += gcnew System::EventHandler(this, &PictureListControl::locationLbl_Click);
            // 
            // PictureListControl
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->Controls->Add(tableLayoutPanel1);
            this->Name = L"PictureListControl";
            this->Size = System::Drawing::Size(844, 310);
            tableLayoutPanel1->ResumeLayout(false);
            tableLayoutPanel1->PerformLayout();
            this->ResumeLayout(false);

        }
#pragma endregion
};
}
