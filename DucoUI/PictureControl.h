#pragma once

namespace DucoUI
{
    /// <summary>
    /// Summary for PictureControl
    /// </summary>
    public ref class PictureControl : public System::Windows::Forms::UserControl
    {
    public:
        PictureControl(DucoUI::DatabaseManager^ theDatabase);
        PictureControl(DucoUI::DatabaseManager^ theDatabase, DucoUI::PictureListControl^ theParent);
        PictureControl(DucoUI::DatabaseManager^ theDatabase, DucoUI::PictureListControl^ theParent, Duco::Picture& newPicture);

        System::Void SetViewMode();
        System::Void SetEditMode(System::Boolean containsPicture);
        System::Void SetPicture(const Duco::ObjectId& pictureId, System::Boolean showSummary);
        System::Void SetPeal(Duco::Peal* thePeal);
        Duco::ObjectId PictureId();

        System::Boolean Selected();
        System::Void Unselect();
        System::Void EnableSelection();
        System::Void DisableSelection();

        System::Void newBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void pasteBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void PictureSelected(System::Object^  sender, System::EventArgs^  e);

        DucoUI::IOpenObjectCallback^        callback;
        event System::EventHandler^         pictureEventHandler;

    protected:
        !PictureControl();
        ~PictureControl();

        System::Boolean ImageRequiresScaling();
        System::Drawing::Image^ ImageDimensions(System::IO::Stream^ pictureStream, System::Drawing::Size^% imageSize);
        System::Boolean Resize(System::IO::Stream^ fileStream, System::String^% newFileName, System::Boolean promptForConfirmation);
        System::Drawing::Imaging::ImageCodecInfo^ GetEncoder(System::Drawing::Imaging::ImageFormat^ format);
        System::IO::Stream^ GetPictureMemoryStream();
        System::Boolean ClipboardContainsSupportedImage();
        System::String^ TempFilename();

        System::Void UpdatePicture(System::Boolean showSummary, System::Boolean editMode);
        System::Void pictureBox_DoubleClick(System::Object^  sender, System::EventArgs^  e);
        System::Void AssociatedLbl_DoubleClick(System::Object^  sender, System::EventArgs^  e);
        System::Void existingBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void removeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void pictureBox_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void resizeBtn_Click(System::Object^  sender, System::EventArgs^  e);

    private:
        System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
        DucoUI::DatabaseManager^            database;
        Duco::Picture*                      currentPicture;
        Duco::Peal*                         currentPeal;
        DucoUI::PictureListControl^         parent;
        System::Windows::Forms::ToolTip^    toolTip1;
        System::Windows::Forms::PictureBox^ pictureBox;
        System::Windows::Forms::Label^      associatedLbl;
        System::Windows::Forms::Button^     newBtn;
        System::Windows::Forms::Button^     removeBtn;
        System::Windows::Forms::Button^     existingBtn;
        System::Windows::Forms::Button^     resizeBtn;
        System::Windows::Forms::Button^     pasteBtn;
        System::Windows::Forms::Label^      sizeLbl;
        System::ComponentModel::IContainer^ components;
        System::Boolean                     selected;
        System::Boolean                     enableSelection;

    private:
#pragma region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        void InitializeComponent(void)
        {
            this->components = (gcnew System::ComponentModel::Container());
            this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            this->pictureBox = (gcnew System::Windows::Forms::PictureBox());
            this->associatedLbl = (gcnew System::Windows::Forms::Label());
            this->newBtn = (gcnew System::Windows::Forms::Button());
            this->removeBtn = (gcnew System::Windows::Forms::Button());
            this->existingBtn = (gcnew System::Windows::Forms::Button());
            this->pasteBtn = (gcnew System::Windows::Forms::Button());
            this->resizeBtn = (gcnew System::Windows::Forms::Button());
            this->sizeLbl = (gcnew System::Windows::Forms::Label());
            this->toolTip1 = (gcnew System::Windows::Forms::ToolTip(this->components));
            this->tableLayoutPanel1->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox))->BeginInit();
            this->SuspendLayout();
            this->tableLayoutPanel1->ColumnCount = 5;
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
                100)));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->Controls->Add(this->pictureBox, 0, 0);
            this->tableLayoutPanel1->Controls->Add(this->associatedLbl, 0, 1);
            this->tableLayoutPanel1->Controls->Add(this->sizeLbl, 3, 1);
            this->tableLayoutPanel1->Controls->Add(this->existingBtn, 0, 2);
            this->tableLayoutPanel1->Controls->Add(this->newBtn, 1, 2);
            this->tableLayoutPanel1->Controls->Add(this->pasteBtn, 2, 2);
            this->tableLayoutPanel1->Controls->Add(this->removeBtn, 3, 2);
            this->tableLayoutPanel1->Controls->Add(this->resizeBtn, 4, 2);
            this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            this->tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
            this->tableLayoutPanel1->RowCount = 1;
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            this->tableLayoutPanel1->Size = System::Drawing::Size(304, 215);
            this->tableLayoutPanel1->TabIndex = 2;
            this->tableLayoutPanel1->SetColumnSpan(this->pictureBox, 5);
            this->pictureBox->Dock = System::Windows::Forms::DockStyle::Fill;
            this->pictureBox->Location = System::Drawing::Point(0, 0);
            this->pictureBox->Margin = System::Windows::Forms::Padding(0);
            this->pictureBox->Name = L"pictureBox";
            this->pictureBox->Size = System::Drawing::Size(304, 172);
            this->pictureBox->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
            this->pictureBox->TabIndex = 0;
            this->pictureBox->TabStop = false;
            this->pictureBox->Click += gcnew System::EventHandler(this, &PictureControl::pictureBox_Click);
            this->pictureBox->DoubleClick += gcnew System::EventHandler(this, &PictureControl::pictureBox_DoubleClick);
            this->associatedLbl->AutoSize = true;
            this->tableLayoutPanel1->SetColumnSpan(this->associatedLbl, 2);
            this->associatedLbl->Location = System::Drawing::Point(3, 175);
            this->associatedLbl->Margin = System::Windows::Forms::Padding(3);
            this->associatedLbl->Name = L"associatedLbl";
            this->associatedLbl->Size = System::Drawing::Size(73, 13);
            this->associatedLbl->TabIndex = 1;
            this->associatedLbl->Text = L"Peal or Tower";
            this->associatedLbl->DoubleClick += gcnew System::EventHandler(this, &PictureControl::AssociatedLbl_DoubleClick);
            this->newBtn->Location = System::Drawing::Point(76, 192);
            this->newBtn->Margin = System::Windows::Forms::Padding(3, 0, 0, 0);
            this->newBtn->Name = L"newBtn";
            this->newBtn->Size = System::Drawing::Size(75, 23);
            this->newBtn->TabIndex = 4;
            this->newBtn->Text = L"New";
            this->toolTip1->SetToolTip(this->newBtn, L"Import a new picture and link to this object");
            this->newBtn->UseVisualStyleBackColor = true;
            this->newBtn->Visible = false;
            this->newBtn->Click += gcnew System::EventHandler(this, &PictureControl::newBtn_Click);
            this->removeBtn->Location = System::Drawing::Point(155, 192);
            this->removeBtn->Margin = System::Windows::Forms::Padding(3, 0, 3, 0);
            this->removeBtn->Name = L"removeBtn";
            this->removeBtn->Size = System::Drawing::Size(71, 23);
            this->removeBtn->TabIndex = 4;
            this->removeBtn->Text = L"Remove";
            this->toolTip1->SetToolTip(this->removeBtn, L"Remove this picure from this object - Note: this does not remove it from the data"
                L"base");
            this->removeBtn->UseVisualStyleBackColor = true;
            this->removeBtn->Visible = false;
            this->removeBtn->Click += gcnew System::EventHandler(this, &PictureControl::removeBtn_Click);
            this->existingBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->existingBtn->Location = System::Drawing::Point(0, 192);
            this->existingBtn->Margin = System::Windows::Forms::Padding(0);
            this->existingBtn->Name = L"existingBtn";
            this->existingBtn->Size = System::Drawing::Size(73, 23);
            this->existingBtn->TabIndex = 2;
            this->existingBtn->Text = L"Existing";
            this->toolTip1->SetToolTip(this->existingBtn, L"Link an existing picture to this object");
            this->existingBtn->UseVisualStyleBackColor = true;
            this->existingBtn->Visible = false;
            this->existingBtn->Click += gcnew System::EventHandler(this, &PictureControl::existingBtn_Click);
            this->resizeBtn->Location = System::Drawing::Point(229, 192);
            this->resizeBtn->Margin = System::Windows::Forms::Padding(0);
            this->resizeBtn->Name = L"resizeBtn";
            this->resizeBtn->Size = System::Drawing::Size(75, 23);
            this->resizeBtn->TabIndex = 3;
            this->resizeBtn->Text = L"Resize";
            this->resizeBtn->UseVisualStyleBackColor = true;
            this->resizeBtn->Click += gcnew System::EventHandler(this, &PictureControl::resizeBtn_Click);
            this->pasteBtn->Location = System::Drawing::Point(229, 192);
            this->pasteBtn->Margin = System::Windows::Forms::Padding(0);
            this->pasteBtn->Name = L"pasteBtn";
            this->pasteBtn->Size = System::Drawing::Size(75, 23);
            this->pasteBtn->TabIndex = 4;
            this->pasteBtn->Text = L"Paste";
            this->pasteBtn->UseVisualStyleBackColor = true;
            this->pasteBtn->Click += gcnew System::EventHandler(this, &PictureControl::pasteBtn_Click);
            this->sizeLbl->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->sizeLbl->AutoSize = true;
            this->tableLayoutPanel1->SetColumnSpan(this->sizeLbl, 2);
            this->sizeLbl->Location = System::Drawing::Point(238, 175);
            this->sizeLbl->Margin = System::Windows::Forms::Padding(3);
            this->sizeLbl->Name = L"sizeLbl";
            this->sizeLbl->Size = System::Drawing::Size(63, 13);
            this->sizeLbl->TabIndex = 5;
            this->sizeLbl->Text = L"Picture Size";
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->Controls->Add(this->tableLayoutPanel1);
            this->Name = L"PictureControl";
            this->Size = System::Drawing::Size(304, 215);
            this->tableLayoutPanel1->ResumeLayout(false);
            this->tableLayoutPanel1->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox))->EndInit();
            this->ResumeLayout(false);

        }
#pragma endregion
    };
}
