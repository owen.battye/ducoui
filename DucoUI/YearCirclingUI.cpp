#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"

#include "YearCirclingUI.h"

#include <StatisticFilters.h>
#include "PrintYearCirclingUtil.h"
#include "DucoUIUtils.h"
#include "DucoUtils.h"
#include "SystemDefaultIcon.h"
#include "FiltersUI.h"
#include <RingingDatabase.h>
#include <PealDatabase.h>
#include "DucoCommon.h"
#include <DatabaseSettings.h>

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Drawing;
using namespace System::Drawing::Printing;
using namespace System::Windows::Forms;

YearCirclingUI::YearCirclingUI(DucoUI::DatabaseManager^ theDatabase)
    : database(theDatabase), loading(true)
{
    InitializeComponent();
    filters = new Duco::StatisticFilters(database->Database());
    this->statsGenerator->DoWork += gcnew DoWorkEventHandler( this, &YearCirclingUI::GenerateStats );
    this->statsGenerator->RunWorkerCompleted += gcnew RunWorkerCompletedEventHandler( this, &YearCirclingUI::StatsCompleted );
    this->statsGenerator->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &YearCirclingUI::statsGenerator_ProgressChanged);
}

YearCirclingUI::~YearCirclingUI()
{
    database->RemoveObserver(this);
    this->statsGenerator->CancelAsync();
    delete filters;
    if (components)
    {
        delete components;
    }
}

void
YearCirclingUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    switch (type)
    {
        case TObjectType::EPeal:
        case TObjectType::ETower:
            if (statsGenerator->IsBusy)
            {
                statsGenerator->CancelAsync();
                restartGenerator = true;
            }
            else
            {
                StartGenerator();
            }
            break;

        default:
            break;
    }
}

System::Void 
YearCirclingUI::SetMonthNames()
{
    CreateMonthRow(L"January");
    CreateMonthRow(L"February");
    CreateMonthRow(L"March");
    CreateMonthRow(L"April");
    CreateMonthRow(L"May");
    CreateMonthRow(L"June");
    CreateMonthRow(L"July");
    CreateMonthRow(L"August");
    CreateMonthRow(L"September");
    CreateMonthRow(L"October");
    CreateMonthRow(L"November");
    CreateMonthRow(L"December");
}

System::Void 
YearCirclingUI::CreateMonthRow(const wchar_t* name)
{
    DataGridViewRow^ newRow = gcnew DataGridViewRow();
    newRow->HeaderCell->Value = gcnew String(name);
    yearCirclingData->Rows->Add(newRow);
}


System::Void 
YearCirclingUI::GenerateStats(System::Object^ sender, System::ComponentModel::DoWorkEventArgs^ e)
{
    noOfTimesYearCircled = 0;
    noOfDaysRemaining = 0;
    BackgroundWorker^ worker = dynamic_cast<BackgroundWorker^>(sender);
    if ( worker->CancellationPending )
    {
        e->Cancel = true;
    }

    std::map<Duco::PealYearDay, Duco::YearCirclingStats, Duco::PealYearDay> results;
    database->Database().PealsDatabase().GetYearCirclingStats(*filters, results);

    std::map<Duco::PealYearDay, Duco::YearCirclingStats, Duco::PealYearDay>::const_iterator it = results.begin();
    double count (0);
    unsigned int thisYear = DateTime::Today.Year;
    while (it != results.end() && !worker->CancellationPending)
    {
        AddDayToStats(worker, it->first.month, it->first.day, it->second.count, thisYear == it->second.firstYear);
        ++count;
        if (!worker->CancellationPending)
            worker->ReportProgress(int(count / 366 * 100));

        ++it;
    }
}

System::Void 
YearCirclingUI::statsGenerator_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    if (statsGenerator->CancellationPending)
        return;

    if (e->ProgressPercentage > 100)
    {
        progressBar->Value = 100;
    }
    else
    {
        progressBar->Value = e->ProgressPercentage;
    }
}

System::Void 
YearCirclingUI::StatsCompleted(System::Object^ sender, System::ComponentModel::RunWorkerCompletedEventArgs^ e)
{
    yearCirclingData->ClearSelection();
    if (restartGenerator)
    {
        restartGenerator = false;
        StartGenerator();
    }
    else if (!e->Cancelled)
    {
        progressBar->Value = 0;
        SetNumberOfTimesYearCircled();
        SetBlanks();
        if (noOfTimesYearCircled == 0)
            yearCircledEditor->Text = "Year not circled";
        else if (noOfTimesYearCircled == 1)
            yearCircledEditor->Text = "Year circled once";
        else
            yearCircledEditor->Text = "Year circled " + Convert::ToString(noOfTimesYearCircled) + " times";
        if (noOfDaysRemaining == 0)
            remainingDaysEditor->Text = "365 days remaining";
        else if (noOfDaysRemaining == 1)
            remainingDaysEditor->Text = "One day remaining";
        else
            remainingDaysEditor->Text = Convert::ToString(noOfDaysRemaining) + " days remaining";
    }
}

System::Void 
YearCirclingUI::AddDayToStats(BackgroundWorker^ worker, unsigned int month, unsigned int day, unsigned int pealCount, bool firstPealWasThisYear)
{
    DataGridViewRow^ currentRow = yearCirclingData->Rows[month-1];
    DataGridViewCell^ currentCell = currentRow->Cells[day-1];
   	currentCell->Value = Convert::ToString(pealCount);

    if (firstPealWasThisYear)
    {
        currentCell->Style->ForeColor = SystemColors::HighlightText;
        currentCell->Style->BackColor = SystemColors::Highlight;
        currentCell->ToolTipText = "First peal was this year";
    }
}

System::Void 
YearCirclingUI::SetBlanks()
{
    SetBlank(2, 30);
    SetBlank(4, 31);
    SetBlank(6, 31);
    SetBlank(9, 31);
    SetBlank(11, 31);
}

System::Void 
YearCirclingUI::ClearAll()
{
    try
    {
        ClearRow(1, 31);
        ClearRow(2, 29);
        ClearRow(3, 31);
        ClearRow(4, 30);
        ClearRow(5, 31);
        ClearRow(6, 30);
        ClearRow(7, 31);
        ClearRow(8, 31);
        ClearRow(9, 30);
        ClearRow(10, 31);
        ClearRow(11, 30);
        ClearRow(12, 31);
    }
    catch (Exception^)
    {
    }
}

System::Void 
YearCirclingUI::ClearRow(unsigned int month, unsigned int maxDays)
{
    DataGridViewRow^ currentRow = yearCirclingData->Rows[month-1];
    while (maxDays > 0)
    {
        DataGridViewCell^ currentCell = currentRow->Cells[maxDays-- -1];
        currentCell->Value = "";
        currentCell->ToolTipText = "";
        currentCell->Style->BackColor = yearCirclingData->DefaultCellStyle->BackColor;
        currentCell->Style->ForeColor = yearCirclingData->DefaultCellStyle->ForeColor;
    }
}

System::Void 
YearCirclingUI::SetBlank(unsigned int month, unsigned int maxDays)
{
    DataGridViewRow^ currentRow = yearCirclingData->Rows[month-1];
    while (maxDays <= 31)
    {
        DataGridViewCell^ currentCell = currentRow->Cells[maxDays++ -1];
        currentCell->Style->BackColor = SystemColors::ControlDark;
    }
}

System::Void
YearCirclingUI::SetNumberOfTimesYearCircled()
{
    short yearCount = 9999;
    int months[12]={31,29,31,30,31,30,31,31,30,31,30,31};
    noOfDaysRemaining = 0;
    for (int rowCount = 0; rowCount < yearCirclingData->RowCount; ++rowCount)
    {
        DataGridViewRow^ currentRow = yearCirclingData->Rows[rowCount];
        for (int cellCount = 0; cellCount < currentRow->Cells->Count; ++cellCount)
        {
            DataGridViewCell^ currentCell = currentRow->Cells[cellCount];
            short value = DucoUtils::ToInt(currentCell);
            if (months[rowCount] >= (cellCount+1))
            {
                if (yearCount > value)
                {
                    yearCount = value;
                    noOfDaysRemaining = 1;
                }
                else if (yearCount == value)
                {
                    ++noOfDaysRemaining;
                }
            }
        }
    }
    noOfTimesYearCircled = yearCount;

//    if (noOfDaysRemaining < 20)
    {
        for (int rowCount = 0; rowCount < yearCirclingData->RowCount; ++rowCount)
        {
            DataGridViewRow^ currentRow = yearCirclingData->Rows[rowCount];
            for (int cellCount = 0; cellCount < currentRow->Cells->Count; ++cellCount)
            {
                if (months[rowCount] >= (cellCount+1))
                {
                    DataGridViewCell^ currentCell = currentRow->Cells[cellCount];
                    short value = DucoUtils::ToInt(currentCell);
                    if (value == noOfTimesYearCircled)
                    {
                        currentCell->Style->BackColor = KErrorColour;
                        currentCell->ToolTipText = "Day required for next circle";
                    }
                }
            }
        }
    }
}

System::Void 
YearCirclingUI::YearCirclingUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    loading = false;
    database->AddObserver(this);
    CreateColumns();
    SetMonthNames();
    StartGenerator();
}

System::Void 
YearCirclingUI::conducted_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    StartGenerator();
}

System::Void
YearCirclingUI::ringerSelector_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    StartGenerator();
}

System::Void
YearCirclingUI::filtersBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    FiltersUI^ filtersDialog = gcnew FiltersUI(*filters, database);
    System::Windows::Forms::DialogResult returnVal = filtersDialog->ShowDialog(this);
    if (returnVal == System::Windows::Forms::DialogResult::OK)
    {
        StartGenerator();
    }
}

System::Void 
YearCirclingUI::StartGenerator()
{
    if (loading)
        return;

    ClearAll();
    noOfTimesYearCircled = 0;
    progressBar->Value = 0;

    if (!statsGenerator->IsBusy)
        statsGenerator->RunWorkerAsync();
}

System::Void 
YearCirclingUI::CreateColumns()
{
    System::Windows::Forms::DataGridViewCellStyle^ style = gcnew DataGridViewCellStyle();
    style->Alignment = DataGridViewContentAlignment::MiddleCenter;
    DataGridViewTextBoxColumn^ column1 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column1, "Column1","1st",style);
    DataGridViewTextBoxColumn^ column2 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column2, "Column2","2nd",style);
    DataGridViewTextBoxColumn^ column3 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column3, "Column3","3rd",style);
    DataGridViewTextBoxColumn^ column4 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column4, "Column4","4th",style);
    DataGridViewTextBoxColumn^ column5 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column5, "Column5","5th",style);
    DataGridViewTextBoxColumn^ column6 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column6, "Column6","6th",style);
    DataGridViewTextBoxColumn^ column7 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column7, "Column7","7th",style);
    DataGridViewTextBoxColumn^ column8 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column8, "Column8","8th",style);
    DataGridViewTextBoxColumn^ column9 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column9, "Column9","9th",style);
    DataGridViewTextBoxColumn^ column10 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column10, "Column10","10th",style);
    DataGridViewTextBoxColumn^ column11 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column11, "Column11","11th",style);
    DataGridViewTextBoxColumn^ column12 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column12, "Column12","12th",style);
    DataGridViewTextBoxColumn^ column13 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column13, "Column13","13th",style);
    DataGridViewTextBoxColumn^ column14 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column14, "Column14","14th",style);
    DataGridViewTextBoxColumn^ column15 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column15, "Column15","15th",style);
    DataGridViewTextBoxColumn^ column16 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column16, "Column16","16th",style);
    DataGridViewTextBoxColumn^ column17 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column17, "Column17","17th",style);
    DataGridViewTextBoxColumn^ column18 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column18, "Column18","18th",style);
    DataGridViewTextBoxColumn^ column19 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column19, "Column19","19th",style);
    DataGridViewTextBoxColumn^ column20 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column20, "Column20","20th",style);
    DataGridViewTextBoxColumn^ column21 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column21, "Column21","21st",style);
    DataGridViewTextBoxColumn^ column22 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column22, "Column22","22nd",style);
    DataGridViewTextBoxColumn^ column23 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column23, "Column23","23rd",style);
    DataGridViewTextBoxColumn^ column24 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column24, "Column24","24th",style);
    DataGridViewTextBoxColumn^ column25 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column25, "Column25","25th",style);
    DataGridViewTextBoxColumn^ column26 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column26, "Column26","26th",style);
    DataGridViewTextBoxColumn^ column27 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column27, "Column27","27th",style);
    DataGridViewTextBoxColumn^ column28 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column28, "Column28","28th",style);
    DataGridViewTextBoxColumn^ column29 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column29, "Column29","29th",style);
    DataGridViewTextBoxColumn^ column30 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column30, "Column30","30th",style);
    DataGridViewTextBoxColumn^ column31 = gcnew DataGridViewTextBoxColumn();
    InitColumn(column31, "Column31","31st",style);
}

System::Void 
YearCirclingUI::InitColumn(System::Windows::Forms::DataGridViewTextBoxColumn^ theColumn, String^ name, String^ text, System::Windows::Forms::DataGridViewCellStyle^ style)
{
    theColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
    theColumn->DefaultCellStyle = style;
    theColumn->HeaderText = text;
    theColumn->MaxInputLength = 3;
    theColumn->Name = name;
    theColumn->ReadOnly = true;
    theColumn->Width = 35;
    theColumn->SortMode = DataGridViewColumnSortMode::NotSortable;
    yearCirclingData->Columns->Add(theColumn);
}

System::Void 
YearCirclingUI::printBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    DucoUIUtils::Print(this, sender, gcnew System::Drawing::Printing::PrintPageEventHandler( this, &YearCirclingUI::printData ), "year circling", database->Database().Settings().UsePrintPreview(), true);
}

System::Void
YearCirclingUI::printData(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args)
{
    PrintYearCirclingUtil printUtil(database, args);
    Duco::ObjectId conductorId;
    printUtil.SetObjects(yearCirclingData, filters->RingerId(), noOfDaysRemaining, noOfTimesYearCircled, filters->Conductor(conductorId));
    PrintDocument^ printDoc = static_cast<PrintDocument^>(sender);
    printUtil.printObject(args, printDoc->PrinterSettings);
}
