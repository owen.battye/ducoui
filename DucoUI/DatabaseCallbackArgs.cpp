#include "DatabaseCallbackArgs.h"
#include <ObjectId.h>

using namespace Duco;
using namespace DucoUI;

DatabaseCallbackArgs::DatabaseCallbackArgs(System::Boolean newInternalising)
    : totalObjects(-1), objectsOfThisType(-1), databaseVersion(-1), fileName(""),
    objectType(TObjectType::ENon), internalising(newInternalising), error(0)
{
    currentObjectId = new Duco::ObjectId();
}

DatabaseCallbackArgs::!DatabaseCallbackArgs()
{
    delete currentObjectId;
}

DatabaseCallbackArgs::~DatabaseCallbackArgs()
{
    this->!DatabaseCallbackArgs();
}
