#pragma once

namespace DucoUI {
	public ref class BandStats : public System::Windows::Forms::Form
	{
	public:
		BandStats(DucoUI::DatabaseManager^ theDatabase, const Duco::ObjectId& towerId);

	protected:
		~BandStats();

	private:
		System::Windows::Forms::ListBox^ towerCtrl;
		System::Windows::Forms::ListBox^ ringersCtrl;
		System::ComponentModel::Container ^components;

        System::Void BandStats_Load(System::Object^ sender, System::EventArgs^ e);
        System::Collections::Generic::List<System::Int16>^ allTowerIds;
        DucoUI::RingerDisplayCache^ ringerCache;
        DucoUI::DatabaseManager^ database;
        DucoUI::PrintBandSummaryUtil^ printBandStatsUtil;
        Duco::ObjectId* towerId;
        std::set<Duco::ObjectId>* ringerIds;
        System::Void SetRingers();
        System::Void printBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void previewBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void closeBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void towerCtrl_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void PrintBandStatsInTower(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args);


#pragma region Windows Form Designer generated code
		void InitializeComponent(void)
		{
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
            System::Windows::Forms::Label^ towerLbl;
            System::Windows::Forms::Label^ ringerLbl;
            System::Windows::Forms::Button^ closeBtn;
            System::Windows::Forms::Button^ printBtn;
            System::Windows::Forms::Button^ previewBtn;
            System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(BandStats::typeid));
            this->towerCtrl = (gcnew System::Windows::Forms::ListBox());
            this->ringersCtrl = (gcnew System::Windows::Forms::ListBox());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            towerLbl = (gcnew System::Windows::Forms::Label());
            ringerLbl = (gcnew System::Windows::Forms::Label());
            closeBtn = (gcnew System::Windows::Forms::Button());
            printBtn = (gcnew System::Windows::Forms::Button());
            previewBtn = (gcnew System::Windows::Forms::Button());
            tableLayoutPanel1->SuspendLayout();
            this->SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 4;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->Controls->Add(towerLbl, 0, 0);
            tableLayoutPanel1->Controls->Add(this->towerCtrl, 1, 0);
            tableLayoutPanel1->Controls->Add(this->ringersCtrl, 1, 1);
            tableLayoutPanel1->Controls->Add(ringerLbl, 0, 1);
            tableLayoutPanel1->Controls->Add(closeBtn, 3, 2);
            tableLayoutPanel1->Controls->Add(printBtn, 1, 2);
            tableLayoutPanel1->Controls->Add(previewBtn, 2, 2);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 3;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 40)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 28)));
            tableLayoutPanel1->Size = System::Drawing::Size(827, 371);
            tableLayoutPanel1->TabIndex = 0;
            // 
            // towerLbl
            // 
            towerLbl->AutoSize = true;
            towerLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            towerLbl->ImageAlign = System::Drawing::ContentAlignment::BottomCenter;
            towerLbl->Location = System::Drawing::Point(3, 0);
            towerLbl->Name = L"towerLbl";
            towerLbl->Padding = System::Windows::Forms::Padding(0, 4, 0, 0);
            towerLbl->Size = System::Drawing::Size(43, 40);
            towerLbl->TabIndex = 0;
            towerLbl->Text = L"Tower:";
            towerLbl->TextAlign = System::Drawing::ContentAlignment::TopRight;
            // 
            // towerCtrl
            // 
            this->towerCtrl->CausesValidation = false;
            tableLayoutPanel1->SetColumnSpan(this->towerCtrl, 3);
            this->towerCtrl->Dock = System::Windows::Forms::DockStyle::Fill;
            this->towerCtrl->FormattingEnabled = true;
            this->towerCtrl->Location = System::Drawing::Point(52, 3);
            this->towerCtrl->Name = L"towerCtrl";
            this->towerCtrl->Size = System::Drawing::Size(772, 34);
            this->towerCtrl->TabIndex = 1;
            this->towerCtrl->SelectedIndexChanged += gcnew System::EventHandler(this, &BandStats::towerCtrl_SelectedIndexChanged);
            // 
            // ringersCtrl
            // 
            tableLayoutPanel1->SetColumnSpan(this->ringersCtrl, 3);
            this->ringersCtrl->Dock = System::Windows::Forms::DockStyle::Fill;
            this->ringersCtrl->FormattingEnabled = true;
            this->ringersCtrl->Location = System::Drawing::Point(52, 43);
            this->ringersCtrl->MultiColumn = true;
            this->ringersCtrl->Name = L"ringersCtrl";
            this->ringersCtrl->SelectionMode = System::Windows::Forms::SelectionMode::MultiExtended;
            this->ringersCtrl->Size = System::Drawing::Size(772, 297);
            this->ringersCtrl->TabIndex = 2;
            // 
            // ringerLbl
            // 
            ringerLbl->AutoSize = true;
            ringerLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            ringerLbl->Location = System::Drawing::Point(3, 40);
            ringerLbl->Name = L"ringerLbl";
            ringerLbl->Padding = System::Windows::Forms::Padding(0, 4, 0, 0);
            ringerLbl->Size = System::Drawing::Size(43, 303);
            ringerLbl->TabIndex = 3;
            ringerLbl->Text = L"Ringers";
            ringerLbl->TextAlign = System::Drawing::ContentAlignment::TopRight;
            // 
            // closeBtn
            // 
            closeBtn->Location = System::Drawing::Point(749, 346);
            closeBtn->Name = L"closeBtn";
            closeBtn->Size = System::Drawing::Size(75, 22);
            closeBtn->TabIndex = 4;
            closeBtn->Text = L"Close";
            closeBtn->UseVisualStyleBackColor = true;
            closeBtn->Click += gcnew System::EventHandler(this, &BandStats::closeBtn_Click);
            // 
            // printBtn
            // 
            printBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            printBtn->Location = System::Drawing::Point(587, 346);
            printBtn->Name = L"printBtn";
            printBtn->Size = System::Drawing::Size(75, 22);
            printBtn->TabIndex = 5;
            printBtn->Text = L"Print";
            printBtn->UseVisualStyleBackColor = true;
            printBtn->Click += gcnew System::EventHandler(this, &BandStats::printBtn_Click);
            // 
            // previewBtn
            // 
            previewBtn->Location = System::Drawing::Point(668, 346);
            previewBtn->Name = L"previewBtn";
            previewBtn->Size = System::Drawing::Size(75, 22);
            previewBtn->TabIndex = 6;
            previewBtn->Text = L"Preview";
            previewBtn->UseVisualStyleBackColor = true;
            previewBtn->Click += gcnew System::EventHandler(this, &BandStats::previewBtn_Click);
            // 
            // BandStats
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(827, 371);
            this->Controls->Add(tableLayoutPanel1);
            this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
            this->Name = L"BandStats";
            this->Text = L"BandStats";
            this->Load += gcnew System::EventHandler(this, &BandStats::BandStats_Load);
            tableLayoutPanel1->ResumeLayout(false);
            tableLayoutPanel1->PerformLayout();
            this->ResumeLayout(false);

        }
#pragma endregion
    };
}
