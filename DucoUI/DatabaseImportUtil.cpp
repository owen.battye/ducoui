#include "DatabaseImportUtil.h"
#include "DatabaseManager.h"
#include "DucoUtils.h"
#include <DucoEngineUtils.h>

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace std;
using namespace System::Data::Odbc;
using namespace System::ComponentModel;
using namespace System::Windows::Forms;

DatabaseImportUtil::DatabaseImportUtil(DucoUI::DatabaseManager^ theDatabase, DucoUI::WinRkImportProgressCallback^ newCallback, System::String^ theFileName)
: database(theDatabase), callback(newCallback), fileName(theFileName), connection(nullptr)
{
    this->importTool = (gcnew System::ComponentModel::BackgroundWorker());
    this->importTool->WorkerReportsProgress = true;
    this->importTool->WorkerSupportsCancellation = true;
    this->importTool->DoWork += gcnew DoWorkEventHandler( this, &DatabaseImportUtil::ImportFile );
    this->importTool->RunWorkerCompleted += gcnew RunWorkerCompletedEventHandler( this, &DatabaseImportUtil::ImportCompleted );
    this->importTool->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &DatabaseImportUtil::ImportProgressChanged);
}

DatabaseImportUtil::~DatabaseImportUtil()
{
    if (connection != nullptr)
        connection->Close();

    System::GC::Collect();
}

void
DatabaseImportUtil::StartImport()
{
    database->ConvertStarted();
    database->ClearDatabase(false, true);
    importTool->RunWorkerAsync();
}

void
DatabaseImportUtil::Cancel()
{
    if (importTool != nullptr)
        importTool->CancelAsync();
}

System::Void
DatabaseImportUtil::ReportProgress(DucoUI::WinRkImportProgressCallback::TImportStage stage, int total, int recordNo)
{
    ImportProgressArgs^ args = gcnew ImportProgressArgs;
    args->stage = stage;
    if (recordNo == 0)
    {
        importTool->ReportProgress(0, args);
    }
    else
    {
        double percentage = (double(recordNo) / double(total)) * 100;
        importTool->ReportProgress(int(percentage), args);
    }
}

/*****************************************************************************************
* General import from a table with only one string
******************************************************************************************/
void
DatabaseImportUtil::ImportSingleStringFromTable(OdbcCommand^ command,
                                             String^ tableName,
                                             map<unsigned int,wstring>& strings,
                                             DucoUI::WinRkImportProgressCallback::TImportStage stage,
                                             System::ComponentModel::BackgroundWorker^ worker)
{
    command->CommandText = "SELECT COUNT(*) FROM " + tableName;
    int noOfRecords = (int)command->ExecuteScalar();
    int count (0);

    ReportProgress(stage, noOfRecords, count);

    command->CommandText = "SELECT * FROM " + tableName;
    OdbcDataReader^ reader = command->ExecuteReader();

    while (reader->Read() && !worker->CancellationPending)
    {
        ++count;
        int idField =  reader->GetInt16(0);
        std::wstring nameField;
        ReadString(reader,1, nameField);
        pair<unsigned int, wstring> newObject(idField, nameField);
        strings.insert(newObject);
        ReportProgress(stage, noOfRecords, count);
    }
    reader->Close();
}

/*****************************************************************************************
* General utilities
******************************************************************************************/

System::Void
DatabaseImportUtil::ReadString(OdbcDataReader^ reader, unsigned int fieldId, std::wstring& str)
{
    String^ temp = ReadString(reader, fieldId);
    DucoUtils::ConvertString(temp, str);
}

String^
DatabaseImportUtil::ReadString(OdbcDataReader^ reader, unsigned int fieldId)
{
    try
    {
        if (reader->IsDBNull(fieldId))
        {
            return "";
        }
        else
        {
            String^ tempStr = reader->GetString(fieldId);
            return tempStr->Trim();
        }
    }
    catch (InvalidCastException^)
    {

    }
    return "";
}


Duco::PerformanceDate
DatabaseImportUtil::ReadDateFromV3(OdbcDataReader^ reader, unsigned int fieldId)
{
    std::wstring convertedStr;
    ReadString(reader, fieldId, convertedStr);
    return PerformanceDate(convertedStr);
}

Duco::PerformanceDate
DatabaseImportUtil::ReadDateFromV2(OdbcDataReader^ reader, unsigned int fieldId)
{
    DateTime^ tempDate = reader->GetDate(fieldId);
    return DucoUtils::ConvertDate(tempDate);
}

Duco::PerformanceTime
DatabaseImportUtil::ReadTime(OdbcDataReader^ reader, unsigned int fieldId)
{
    String^ tempStr = reader->GetString(fieldId);
    std::wstring convertedStr;
    DucoUtils::ConvertString(tempStr, convertedStr);
    return PerformanceTime(convertedStr);
}

System::Void
DatabaseImportUtil::ImportFile(System::Object^ sender, System::ComponentModel::DoWorkEventArgs^ e)
{
    try
    {
        Import(dynamic_cast<BackgroundWorker^>(sender), e);
    }
    catch (Exception^ ex)
    {
        MessageBox::Show(ex->ToString(), "Error during file import");
    }
}

System::Void
DatabaseImportUtil::ImportCompleted(System::Object^ sender, System::ComponentModel::RunWorkerCompletedEventArgs^ e)
{
    callback->Complete(e->Cancelled);
}

System::Void
DatabaseImportUtil::ImportProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    try
    {
        if (e->ProgressPercentage == 0)
        {
            ImportProgressArgs^ args = (ImportProgressArgs^)e->UserState;
            callback->Initialised(args->stage);
        }
        else
        {
            callback->Step(e->ProgressPercentage);
        }
    }
    catch (Exception^ ex)
    {
        Console::Error->Write("Error during Duco Import");
        Console::Error->WriteLine();
        Console::Error->Write(ex->ToString());
        Console::Error->WriteLine();
    }
}

Duco::ObjectId
DatabaseImportUtil::CheckRingerAkaIds(ObjectId idToCheck, const std::map<ObjectId, ObjectId>& ringerAkas)
{
    std::map<ObjectId, ObjectId>::const_iterator it = ringerAkas.find(idToCheck);
    if (it != ringerAkas.end())
    {
        return it->second;
    }
    return idToCheck;
}

bool
DatabaseImportUtil::ImportInProgress()
{
    return importTool->IsBusy;
}