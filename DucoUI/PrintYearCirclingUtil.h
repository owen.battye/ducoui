#pragma once
#include "PrintUtilBase.h"
#include <ObjectId.h>

namespace DucoUI
{
public ref class PrintYearCirclingUtil : public PrintUtilBase
{
public:
    PrintYearCirclingUtil(DucoUI::DatabaseManager^ theDatabase, System::Drawing::Printing::PrintPageEventArgs^ args);
    !PrintYearCirclingUtil();
    ~PrintYearCirclingUtil();
    void SetObjects(System::Windows::Forms::DataGridView^ theDataGridView, const Duco::ObjectId& newRingerId, unsigned int noOfDaysRemaining, unsigned int noOfTimesYearCircled, bool conductedOnly);
    virtual System::Void printObject(System::Drawing::Printing::PrintPageEventArgs^ args, System::Drawing::Printing::PrinterSettings^ printSettings) override;

protected:
    System::Windows::Forms::DataGridView^   dataGridView;
    Duco::ObjectId*                         ringerId;
    unsigned int                            numberOfDaysRemaining;
    unsigned int                            numberOfTimesYearCircled;
    bool                                    conductedPealsOnly;
};

}