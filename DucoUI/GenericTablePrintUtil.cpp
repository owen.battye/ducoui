#include "GenericTablePrintUtil.h"

#include <ObjectId.h>
#include <numeric>
#include "DatabaseManager.h"

using namespace System;
using namespace System::Drawing;
using namespace System::Windows::Forms;
using namespace Duco;
using namespace DucoUI;

GenericTablePrintUtil::GenericTablePrintUtil(DucoUI::DatabaseManager^ theDatabase, System::Drawing::Printing::PrintPageEventArgs^ args, System::String^ newTitle)
:   PrintUtilBase(theDatabase, 7, args, false), title(newTitle), pageNumber(0), startDataColumn(-1), endDataColumn(-1), ignoreFirstColumn(true)
{
    objectId = new Duco::ObjectId();
}

GenericTablePrintUtil::!GenericTablePrintUtil()
{
    delete objectId;
}

GenericTablePrintUtil::~GenericTablePrintUtil()
{
    this->!GenericTablePrintUtil();
}

System::Void
GenericTablePrintUtil::SetObjects(System::Windows::Forms::DataGridView^ theDataGridView, const Duco::ObjectId& newObjectId, TObjectType newObjectType, System::String^ newSecondaryTitle, System::Int16 startingDataColumn, System::Int16 endingDataColumn)
{
    dataGridView = theDataGridView;
    *objectId = newObjectId;
    printingResultNumber = 0;
    objectType = newObjectType;
    title += database->FullName(objectType, *objectId);
    startDataColumn = startingDataColumn;
    endDataColumn = endingDataColumn;
    if (newSecondaryTitle != nullptr && newSecondaryTitle->Length > 0)
        secondaryTitle = newSecondaryTitle;
    else
        secondaryTitle = "";
}

System::Void
GenericTablePrintUtil::SetObjects(System::Windows::Forms::DataGridView^ theDataGridView, bool ignoreTheFirstColumn)
{
    ignoreFirstColumn = ignoreTheFirstColumn;
    dataGridView = theDataGridView;
    objectId->ClearId();
    printingResultNumber = 0;
    startDataColumn = 0;
    endDataColumn = 0;
}

System::Void
GenericTablePrintUtil::SetSecondaryTitle(System::String^ newSecondaryTitle)
{
    secondaryTitle = newSecondaryTitle;
}

System::Void
GenericTablePrintUtil::printObject(System::Drawing::Printing::PrintPageEventArgs^ printArgs, System::Drawing::Printing::PrinterSettings^ printSettings)
{
    ++pageNumber;
    printThisPage = printSettings->PrintRange == System::Drawing::Printing::PrintRange::AllPages || (pageNumber >= printSettings->FromPage && pageNumber <= printSettings->ToPage);

    StringFormat^ stringFormat = gcnew StringFormat;
    addDucoFooter(normalFont, stringFormat, printArgs, pageNumber);

    PrintLongString(title, largeFont, leftMargin, printArgs);
    PrintLongString(secondaryTitle, normalFont, leftMargin, printArgs);
    if (pageNumber == 1)
    {
        String^ lastPealDate = database->LastDateString();
        PrintLongString(lastPealDate, normalFont, leftMargin, printArgs);    
    }

    std::vector<float> columnWidths;
    GenerateColumnWidths(columnWidths, printArgs, dataGridView);
    if (ignoreFirstColumn)
        columnWidths[0] = 0;
    float totalWidth = std::accumulate(columnWidths.begin(), columnWidths.end(), 0.0f);

    float xPos (leftMargin);
    for (int i(0); i < dataGridView->Columns->Count; ++i)
    {
        DataGridViewColumn^ currentColumn = dataGridView->Columns[i];
        if (columnWidths[i] > 0)
        {
            String^ printBuffer = Convert::ToString(currentColumn->HeaderCell->Value);
            if (i >= startDataColumn && i <= endDataColumn)
                PrintStringRightJustified(printBuffer, boldFont, xPos, columnWidths[i], printArgs);
            else
                PrintStringMaxLength(printBuffer, boldFont, xPos, columnWidths[i], printArgs);
        }
    }
    --noOfLines;

    xPos = leftMargin;
    while (noOfLines > 0 && printingResultNumber < dataGridView->Rows->Count)
    {
        DataGridViewRow^ currentRow = dataGridView->Rows[printingResultNumber];
        yPos += lineHeight;

        for (int j(0); j < dataGridView->Columns->Count; ++j)
        {
            String^ printBuffer = Convert::ToString(currentRow->Cells[j]->Value);
            if (j >= startDataColumn && j <= endDataColumn)
                PrintStringRightJustified(printBuffer, normalFont, xPos, columnWidths[j], printArgs);
            else
                PrintStringMaxLength(printBuffer, normalFont, xPos, columnWidths[j], printArgs);
        }
        if (printingResultNumber % 3 == 0)
        {
            Point startOfLinePoint(leftMargin, yPos);
            Point endOfLinePoint(printArgs->MarginBounds.Right, yPos);
            PrintLine(leftMargin, totalWidth + leftMargin, yPos, printArgs);
        }

        noOfLines--;
        printingResultNumber++;
        xPos = leftMargin;
    }

    if ( printingResultNumber < dataGridView->Rows->Count)
    {
        printArgs->HasMorePages = printSettings->PrintRange == System::Drawing::Printing::PrintRange::AllPages || pageNumber < printSettings->ToPage;
        ResetPosition(printArgs);
    }
    else
    {
        printingResultNumber = 0;
        printArgs->HasMorePages = false;
    }

    if (!printThisPage)
    {
        if (pageNumber < printSettings->ToPage)
        {
            printObject(printArgs, printSettings);
        }
    }
}

System::Void
GenericTablePrintUtil::GenerateColumnWidths(std::vector<float>& columnWidths, System::Drawing::Printing::PrintPageEventArgs^ printArgs, System::Windows::Forms::DataGridView^ dataGridView)
{
    PrintUtilBase::GenerateColumnWidths(columnWidths, printArgs, dataGridView);
    
    float maxColumnWidth (0);
    for (Int16 i = startDataColumn; i < endDataColumn; ++i)
    {
        maxColumnWidth = Math::Max(columnWidths[i], maxColumnWidth);
    }

    for (Int16 j = startDataColumn; j < endDataColumn; ++j)
    {
        if (columnWidths[j] < maxColumnWidth)
            columnWidths[j] = maxColumnWidth;
    }
}