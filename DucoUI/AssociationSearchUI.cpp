#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "ProgressWrapper.h"
#include <DatabaseSearch.h>

#include "AssociationSearchUI.h"

#include "DucoWindowState.h"
#include "Association.h"
#include "AssociationsUI.h"
#include "SystemDefaultIcon.h"
#include "DatabaseGenUtils.h"
#include "SoundUtils.h"
#include "SearchUtils.h"
#include "DucoUIUtils.h"
#include "DucoUtils.h"
#include <DucoEngineUtils.h>
#include "SearchFieldIdArgument.h"
#include "RingingDatabase.h"
#include "PealDatabase.h"
#include "StatisticFilters.h"

using namespace Duco;
using namespace DucoUI;

AssociationSearchUI::AssociationSearchUI(DucoUI::DatabaseManager^ theDatabase)
    : database(theDatabase)
{
    progressWrapper = new DucoUI::ProgressCallbackWrapper(this);
    foundAssociationIds = new std::set<Duco::ObjectId>;
    InitializeComponent();
}

AssociationSearchUI::!AssociationSearchUI()
{
    delete foundAssociationIds;
    delete progressWrapper;
}

AssociationSearchUI::~AssociationSearchUI()
{
    if (components)
    {
        delete components;
    }
}

System::Void
AssociationSearchUI::AssociationSearchUI_Load(System::Object^ sender, System::EventArgs^ e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    allAssociationIds = gcnew System::Collections::Generic::List<System::Int16>();
    DatabaseGenUtils::GenerateAssociationOptions(linkedAssociation, allAssociationIds, database, KNoId, true);

}

System::Void
AssociationSearchUI::AssociationSearchUI_FormClosing(System::Object^ sender, System::Windows::Forms::FormClosingEventArgs^ e)
{
    if (backgroundWorker1->IsBusy)
    {
        backgroundWorker1->CancelAsync();
        e->Cancel = true;
    }
}


System::Void
AssociationSearchUI::clearBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (!backgroundWorker1->IsBusy)
    {
        results->Text = "";
        progressBar->Value = 0;
        dataGridView1->Rows->Clear();
        searchBtn->Enabled = true;
        associationName->Text = "";
        linkedAssociation->SelectedIndex = -1;
    }
}

System::Void 
AssociationSearchUI::searchBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (!backgroundWorker1->IsBusy)
    {
        searchBtn->Enabled = false;
        //foundCompositionIds->clear();
        //nameColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
        //lengthColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
        //methodColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
        //composerColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
        dataGridView1->Rows->Clear();
        results->Text = "";
        //dataGridView1->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::EnableResizing;
        //dataGridView1->ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode::EnableResizing;

        DatabaseSearch* search = new DatabaseSearch(database->Database());
        if (CreateSearchParameters(*search))
        {
            SearchUIArgs^ args = gcnew SearchUIArgs();
            args->search = search;
            backgroundWorker1->RunWorkerAsync(args);
        }
        else
        {
            SoundUtils::PlayErrorSound();
            delete search;
            searchBtn->Enabled = true;
        }
    }
}

System::Boolean
AssociationSearchUI::CreateSearchParameters(Duco::DatabaseSearch& search)
{
    try
    {
        SearchUtils::AddStringSearchArgument(search, associationName->Text, EAssociationName, false);
        if (linkedAssociation->SelectedIndex != -1)
        {
            Duco::ObjectId methodId = DatabaseGenUtils::FindId(allAssociationIds, linkedAssociation->SelectedIndex);
            TSearchFieldIdArgument* newId = new TSearchFieldIdArgument(Duco::ELinkedAssociationId, methodId);
            search.AddArgument(newId);
        }
    }
    catch (System::Exception^)
    {
        return false;
    }
    return true;
}


System::Void
AssociationSearchUI::closeBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    Close();
}

System::Void
AssociationSearchUI::Initialised()
{
    backgroundWorker1->ReportProgress(0);
}

System::Void
AssociationSearchUI::Step(int progressPercent)
{
    backgroundWorker1->ReportProgress(progressPercent);
}

System::Void
AssociationSearchUI::Complete()
{
}

System::Void
AssociationSearchUI::backgroundWorker1_DoWork(System::Object^ sender, System::ComponentModel::DoWorkEventArgs^ e)
{
    SearchUIArgs^ args = static_cast<SearchUIArgs^>(e->Argument);
    System::ComponentModel::BackgroundWorker^ bgworker = static_cast<System::ComponentModel::BackgroundWorker^>(sender);
    args->search->Search(*progressWrapper, *foundAssociationIds, TObjectType::EAssociationOrSociety);
    PopulateResults(bgworker);
    delete args->search;
}

System::Void
AssociationSearchUI::backgroundWorker1_ProgressChanged(System::Object^ sender, System::ComponentModel::ProgressChangedEventArgs^ e)
{
    progressBar->Value = e->ProgressPercentage;
    if (e->UserState != nullptr)
    {
        DataGridViewRow^ newRow = static_cast<DataGridViewRow^>(e->UserState);
        dataGridView1->Rows->Add(newRow);
    }
}

System::Void
AssociationSearchUI::backgroundWorker1_RunWorkerCompleted(System::Object^ sender, System::ComponentModel::RunWorkerCompletedEventArgs^ e)
{
    //dataGridView1->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
    //dataGridView1->ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode::AutoSize;
    searchBtn->Enabled = true;
    progressBar->Value = 0;
    results->Text = System::Convert::ToString(foundAssociationIds->size());
    if (foundAssociationIds->empty())
        SoundUtils::PlayErrorSound();
}

System::Void
AssociationSearchUI::PopulateResults(System::ComponentModel::BackgroundWorker^ bgworker)
{
    //    bool errorFound (false);
    float total = float(foundAssociationIds->size());
    float count(0);
    std::set<Duco::ObjectId>::const_iterator it = foundAssociationIds->begin();
    while (it != foundAssociationIds->end() && !bgworker->CancellationPending)
    {
        Duco::Association theAssociation;
        if (database->FindAssociation(*it, theAssociation, false))
        {
            StatisticFilters filters(database->Database());
            std::map<Duco::ObjectId, Duco::PealLengthInfo> associationCounts;
            database->Database().PealsDatabase().GetAssociationsPealCount(filters, associationCounts, false);

            DataGridViewRow^ newRow = gcnew DataGridViewRow();
            newRow->HeaderCell->Value = DucoUtils::ConvertString(*it);
            DataGridViewCellCollection^ theCells = newRow->Cells;
            DataGridViewTextBoxCell^ nameCell = gcnew DataGridViewTextBoxCell();
            nameCell->Value = DucoUtils::ConvertString(theAssociation.Name());
            theCells->Add(nameCell);

            DataGridViewTextBoxCell^ totalCell = gcnew DataGridViewTextBoxCell();
            DataGridViewTextBoxCell^ firstCell = gcnew DataGridViewTextBoxCell();
            DataGridViewTextBoxCell^ lastCell = gcnew DataGridViewTextBoxCell();
            if (associationCounts.find(*it) != associationCounts.end())
            {
                totalCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(associationCounts[*it].TotalPeals(), true));
                firstCell->Value = DucoUtils::ConvertString(associationCounts[*it].DateOfFirstPeal(), true);
                lastCell->Value = DucoUtils::ConvertString(associationCounts[*it].DateOfLastPeal(), false);
            }

            theCells->Add(totalCell);
            theCells->Add(firstCell);
            theCells->Add(lastCell);

            ++count;
            backgroundWorker1->ReportProgress(int(((count / total) * 50) + 50), newRow);
        }
        ++it;
    }
}

System::Void
AssociationSearchUI::dataGridView_CellDoubleClick(System::Object^ sender, System::Windows::Forms::DataGridViewCellEventArgs^ e)
{
    try
    {
        if (!backgroundWorker1->IsBusy)
        {
            unsigned int rowIndex = e->RowIndex;
            DataGridViewRow^ currentRow = dataGridView1->Rows[rowIndex];
            unsigned int compositionId = System::Convert::ToInt16(currentRow->HeaderCell->Value);
            AssociationsUI::ShowAssociation(database, this->MdiParent, compositionId);
        }
    }
    catch (System::Exception^)
    {
        SoundUtils::PlayErrorSound();
    }
}
