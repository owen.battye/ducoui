#pragma once

namespace DucoUI {

    /// <summary>
    /// Summary for AssociationsUI
    /// </summary>
    public ref class AssociationsUI : public System::Windows::Forms::Form, public DucoUI::RingingDatabaseObserver
    {
    public:
        AssociationsUI(DatabaseManager^ theDatabase, DucoUI::DucoWindowState newState);
        static void ShowAssociation(DucoUI::DatabaseManager^ theDatabase, System::Windows::Forms::Form^ parent, const Duco::ObjectId& associationId);

        // from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);
        System::Void statisticsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);

    protected:
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        ~AssociationsUI();
        System::Windows::Forms::ToolStripStatusLabel^  numberOfObjectsLbl;
        DucoUI::DatabaseManager^            database;
        Duco::Association*                  association;
        DucoUI::DucoWindowState             windowState;
        System::Boolean                     dataChanged;
    private: System::Windows::Forms::Button^  editBtn;
    private: System::Windows::Forms::TextBox^  nameEditor;
    private: System::Windows::Forms::Button^  endBtn;
    private: System::Windows::Forms::Button^  startBtn;
    private: System::Windows::Forms::Button^  back10Btn;
    private: System::Windows::Forms::Button^  backBtn;
    private: System::Windows::Forms::Button^  forwardBtn;
    private: System::Windows::Forms::Button^  forward10Btn;

    private: System::Windows::Forms::TextBox^  pealbaseEditor;
    private: System::Windows::Forms::Button^  saveBtn;
    private: System::Windows::Forms::Button^  cancelBtn;
    private: System::Windows::Forms::Button^  closeBtn;
    private: System::Windows::Forms::ToolStripMenuItem^  statisticsToolStripMenuItem;
    private: System::Windows::Forms::Button^ openBtn;
    private: System::Windows::Forms::CheckedListBox^ linkedAssociations;
    private: System::Collections::Generic::List<System::Int16>^ allAssociationIds;
    private: System::Windows::Forms::RichTextBox^ notesEditor;


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

        System::Void AssociationsUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void ClosingEvent(System::Object^ object, System::ComponentModel::CancelEventArgs^ e);
        System::Void PopulateView();
        System::Void PopulateViewWithId(const Duco::ObjectId& composerId);
        System::Void UpdateIdLabel();
        System::Void startBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void back10Btn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void backBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void forwardBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void forward10Btn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void endBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void editBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void cancelBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void saveBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void SetViewMode();
        System::Void SetEditMode();
        System::Void ClearView();
        System::Void nameEditor_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void notesEditor_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void pealbaseEditor_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void openBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void linkedAssociations_ItemCheck(System::Object^ sender, System::Windows::Forms::ItemCheckEventArgs^ e);

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent()
		{
            System::Windows::Forms::StatusStrip^ statusStrip1;
            System::Windows::Forms::Label^ nameLabel;
            System::Windows::Forms::TabControl^ associationDetails;
            System::Windows::Forms::TabPage^ detailsPage;
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel2;
            System::Windows::Forms::Label^ notesLbl;
            System::Windows::Forms::TabPage^ referencesTab;
            System::Windows::Forms::GroupBox^ pealbaseGroup;
            System::Windows::Forms::TabPage^ linksTab;
            System::Windows::Forms::TableLayoutPanel^ linkedAssociationsLayout;
            System::Windows::Forms::TextBox^ linkedDescriptionLbl;
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
            System::Windows::Forms::MenuStrip^ menuStrip1;
            System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(AssociationsUI::typeid));
            this->numberOfObjectsLbl = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->nameEditor = (gcnew System::Windows::Forms::TextBox());
            this->notesEditor = (gcnew System::Windows::Forms::RichTextBox());
            this->pealbaseEditor = (gcnew System::Windows::Forms::TextBox());
            this->openBtn = (gcnew System::Windows::Forms::Button());
            this->linkedAssociations = (gcnew System::Windows::Forms::CheckedListBox());
            this->saveBtn = (gcnew System::Windows::Forms::Button());
            this->cancelBtn = (gcnew System::Windows::Forms::Button());
            this->editBtn = (gcnew System::Windows::Forms::Button());
            this->endBtn = (gcnew System::Windows::Forms::Button());
            this->closeBtn = (gcnew System::Windows::Forms::Button());
            this->forward10Btn = (gcnew System::Windows::Forms::Button());
            this->startBtn = (gcnew System::Windows::Forms::Button());
            this->forwardBtn = (gcnew System::Windows::Forms::Button());
            this->back10Btn = (gcnew System::Windows::Forms::Button());
            this->backBtn = (gcnew System::Windows::Forms::Button());
            this->statisticsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            nameLabel = (gcnew System::Windows::Forms::Label());
            associationDetails = (gcnew System::Windows::Forms::TabControl());
            detailsPage = (gcnew System::Windows::Forms::TabPage());
            tableLayoutPanel2 = (gcnew System::Windows::Forms::TableLayoutPanel());
            notesLbl = (gcnew System::Windows::Forms::Label());
            referencesTab = (gcnew System::Windows::Forms::TabPage());
            pealbaseGroup = (gcnew System::Windows::Forms::GroupBox());
            linksTab = (gcnew System::Windows::Forms::TabPage());
            linkedAssociationsLayout = (gcnew System::Windows::Forms::TableLayoutPanel());
            linkedDescriptionLbl = (gcnew System::Windows::Forms::TextBox());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
            statusStrip1->SuspendLayout();
            associationDetails->SuspendLayout();
            detailsPage->SuspendLayout();
            tableLayoutPanel2->SuspendLayout();
            referencesTab->SuspendLayout();
            pealbaseGroup->SuspendLayout();
            linksTab->SuspendLayout();
            linkedAssociationsLayout->SuspendLayout();
            tableLayoutPanel1->SuspendLayout();
            menuStrip1->SuspendLayout();
            this->SuspendLayout();
            // 
            // statusStrip1
            // 
            statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->numberOfObjectsLbl });
            statusStrip1->Location = System::Drawing::Point(0, 256);
            statusStrip1->Name = L"statusStrip1";
            statusStrip1->Size = System::Drawing::Size(437, 22);
            statusStrip1->TabIndex = 0;
            statusStrip1->Text = L"statusStrip1";
            // 
            // numberOfObjectsLbl
            // 
            this->numberOfObjectsLbl->Name = L"numberOfObjectsLbl";
            this->numberOfObjectsLbl->Size = System::Drawing::Size(422, 17);
            this->numberOfObjectsLbl->Spring = true;
            this->numberOfObjectsLbl->Text = L"1/1";
            this->numberOfObjectsLbl->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // nameLabel
            // 
            nameLabel->AutoSize = true;
            nameLabel->Dock = System::Windows::Forms::DockStyle::Fill;
            nameLabel->Location = System::Drawing::Point(3, 3);
            nameLabel->Margin = System::Windows::Forms::Padding(3);
            nameLabel->Name = L"nameLabel";
            nameLabel->Size = System::Drawing::Size(35, 20);
            nameLabel->TabIndex = 3;
            nameLabel->Text = L"Name";
            nameLabel->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // associationDetails
            // 
            tableLayoutPanel1->SetColumnSpan(associationDetails, 9);
            associationDetails->Controls->Add(detailsPage);
            associationDetails->Controls->Add(referencesTab);
            associationDetails->Controls->Add(linksTab);
            associationDetails->Dock = System::Windows::Forms::DockStyle::Fill;
            associationDetails->Location = System::Drawing::Point(3, 3);
            associationDetails->Name = L"associationDetails";
            associationDetails->SelectedIndex = 0;
            associationDetails->Size = System::Drawing::Size(431, 168);
            associationDetails->TabIndex = 11;
            // 
            // detailsPage
            // 
            detailsPage->Controls->Add(tableLayoutPanel2);
            detailsPage->Location = System::Drawing::Point(4, 22);
            detailsPage->Name = L"detailsPage";
            detailsPage->Padding = System::Windows::Forms::Padding(3);
            detailsPage->Size = System::Drawing::Size(423, 142);
            detailsPage->TabIndex = 0;
            detailsPage->Text = L"Details";
            detailsPage->UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            tableLayoutPanel2->ColumnCount = 2;
            tableLayoutPanel2->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel2->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel2->Controls->Add(this->nameEditor, 1, 0);
            tableLayoutPanel2->Controls->Add(notesLbl, 0, 1);
            tableLayoutPanel2->Controls->Add(nameLabel, 0, 0);
            tableLayoutPanel2->Controls->Add(this->notesEditor, 1, 1);
            tableLayoutPanel2->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel2->Location = System::Drawing::Point(3, 3);
            tableLayoutPanel2->Name = L"tableLayoutPanel2";
            tableLayoutPanel2->RowCount = 2;
            tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel2->Size = System::Drawing::Size(417, 136);
            tableLayoutPanel2->TabIndex = 7;
            // 
            // nameEditor
            // 
            this->nameEditor->Dock = System::Windows::Forms::DockStyle::Fill;
            this->nameEditor->Location = System::Drawing::Point(44, 3);
            this->nameEditor->Name = L"nameEditor";
            this->nameEditor->ReadOnly = true;
            this->nameEditor->Size = System::Drawing::Size(370, 20);
            this->nameEditor->TabIndex = 4;
            this->nameEditor->TextChanged += gcnew System::EventHandler(this, &AssociationsUI::nameEditor_TextChanged);
            // 
            // notesLbl
            // 
            notesLbl->AutoSize = true;
            notesLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            notesLbl->Location = System::Drawing::Point(3, 29);
            notesLbl->Margin = System::Windows::Forms::Padding(3);
            notesLbl->Name = L"notesLbl";
            notesLbl->Size = System::Drawing::Size(35, 104);
            notesLbl->TabIndex = 5;
            notesLbl->Text = L"Notes";
            notesLbl->TextAlign = System::Drawing::ContentAlignment::TopRight;
            // 
            // notesEditor
            // 
            this->notesEditor->Dock = System::Windows::Forms::DockStyle::Fill;
            this->notesEditor->Location = System::Drawing::Point(44, 29);
            this->notesEditor->Name = L"notesEditor";
            this->notesEditor->ReadOnly = true;
            this->notesEditor->Size = System::Drawing::Size(370, 104);
            this->notesEditor->TabIndex = 6;
            this->notesEditor->Text = L"";
            // 
            // referencesTab
            // 
            referencesTab->Controls->Add(pealbaseGroup);
            referencesTab->Location = System::Drawing::Point(4, 22);
            referencesTab->Name = L"referencesTab";
            referencesTab->Padding = System::Windows::Forms::Padding(3);
            referencesTab->Size = System::Drawing::Size(423, 142);
            referencesTab->TabIndex = 1;
            referencesTab->Text = L"Web links";
            referencesTab->UseVisualStyleBackColor = true;
            // 
            // pealbaseGroup
            // 
            pealbaseGroup->AutoSize = true;
            pealbaseGroup->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            pealbaseGroup->Controls->Add(this->pealbaseEditor);
            pealbaseGroup->Controls->Add(this->openBtn);
            pealbaseGroup->Location = System::Drawing::Point(6, 6);
            pealbaseGroup->Name = L"pealbaseGroup";
            pealbaseGroup->Size = System::Drawing::Size(319, 87);
            pealbaseGroup->TabIndex = 4;
            pealbaseGroup->TabStop = false;
            pealbaseGroup->Text = L"Pealbase";
            // 
            // pealbaseEditor
            // 
            this->pealbaseEditor->Location = System::Drawing::Point(6, 19);
            this->pealbaseEditor->Name = L"pealbaseEditor";
            this->pealbaseEditor->ReadOnly = true;
            this->pealbaseEditor->Size = System::Drawing::Size(307, 20);
            this->pealbaseEditor->TabIndex = 2;
            this->pealbaseEditor->TextChanged += gcnew System::EventHandler(this, &AssociationsUI::pealbaseEditor_TextChanged);
            // 
            // openBtn
            // 
            this->openBtn->Location = System::Drawing::Point(238, 45);
            this->openBtn->Name = L"openBtn";
            this->openBtn->Size = System::Drawing::Size(75, 23);
            this->openBtn->TabIndex = 3;
            this->openBtn->Text = L"Open";
            this->openBtn->UseVisualStyleBackColor = true;
            this->openBtn->Click += gcnew System::EventHandler(this, &AssociationsUI::openBtn_Click);
            // 
            // linksTab
            // 
            linksTab->Controls->Add(linkedAssociationsLayout);
            linksTab->Location = System::Drawing::Point(4, 22);
            linksTab->Name = L"linksTab";
            linksTab->Padding = System::Windows::Forms::Padding(3);
            linksTab->Size = System::Drawing::Size(423, 142);
            linksTab->TabIndex = 2;
            linksTab->Text = L"Linked associations";
            linksTab->UseVisualStyleBackColor = true;
            // 
            // linkedAssociationsLayout
            // 
            linkedAssociationsLayout->ColumnCount = 1;
            linkedAssociationsLayout->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
                100)));
            linkedAssociationsLayout->Controls->Add(this->linkedAssociations, 0, 0);
            linkedAssociationsLayout->Controls->Add(linkedDescriptionLbl, 0, 1);
            linkedAssociationsLayout->Dock = System::Windows::Forms::DockStyle::Fill;
            linkedAssociationsLayout->Location = System::Drawing::Point(3, 3);
            linkedAssociationsLayout->Name = L"linkedAssociationsLayout";
            linkedAssociationsLayout->RowCount = 2;
            linkedAssociationsLayout->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            linkedAssociationsLayout->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            linkedAssociationsLayout->Size = System::Drawing::Size(417, 136);
            linkedAssociationsLayout->TabIndex = 1;
            // 
            // linkedAssociations
            // 
            this->linkedAssociations->CheckOnClick = true;
            this->linkedAssociations->Dock = System::Windows::Forms::DockStyle::Fill;
            this->linkedAssociations->Enabled = false;
            this->linkedAssociations->FormattingEnabled = true;
            this->linkedAssociations->Location = System::Drawing::Point(3, 3);
            this->linkedAssociations->Name = L"linkedAssociations";
            this->linkedAssociations->Size = System::Drawing::Size(411, 89);
            this->linkedAssociations->TabIndex = 0;
            this->linkedAssociations->ThreeDCheckBoxes = true;
            this->linkedAssociations->ItemCheck += gcnew System::Windows::Forms::ItemCheckEventHandler(this, &AssociationsUI::linkedAssociations_ItemCheck);
            // 
            // linkedDescriptionLbl
            // 
            linkedDescriptionLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            linkedDescriptionLbl->Location = System::Drawing::Point(3, 98);
            linkedDescriptionLbl->Multiline = true;
            linkedDescriptionLbl->Name = L"linkedDescriptionLbl";
            linkedDescriptionLbl->ReadOnly = true;
            linkedDescriptionLbl->Size = System::Drawing::Size(411, 35);
            linkedDescriptionLbl->TabIndex = 1;
            linkedDescriptionLbl->Text = L"Association listed here, will have the performance count for this association add"
                L"ed to them, and this association removed from the statistics.";
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 9;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->Controls->Add(this->saveBtn, 7, 2);
            tableLayoutPanel1->Controls->Add(this->cancelBtn, 8, 2);
            tableLayoutPanel1->Controls->Add(associationDetails, 0, 0);
            tableLayoutPanel1->Controls->Add(this->editBtn, 7, 1);
            tableLayoutPanel1->Controls->Add(this->endBtn, 5, 1);
            tableLayoutPanel1->Controls->Add(this->closeBtn, 8, 1);
            tableLayoutPanel1->Controls->Add(this->forward10Btn, 4, 1);
            tableLayoutPanel1->Controls->Add(this->startBtn, 0, 1);
            tableLayoutPanel1->Controls->Add(this->forwardBtn, 3, 1);
            tableLayoutPanel1->Controls->Add(this->back10Btn, 1, 1);
            tableLayoutPanel1->Controls->Add(this->backBtn, 2, 1);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 24);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 3;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->Size = System::Drawing::Size(437, 232);
            tableLayoutPanel1->TabIndex = 12;
            // 
            // saveBtn
            // 
            this->saveBtn->AutoSize = true;
            this->saveBtn->Location = System::Drawing::Point(278, 206);
            this->saveBtn->Name = L"saveBtn";
            this->saveBtn->Size = System::Drawing::Size(75, 23);
            this->saveBtn->TabIndex = 13;
            this->saveBtn->Text = L"Save";
            this->saveBtn->UseVisualStyleBackColor = true;
            this->saveBtn->Visible = false;
            this->saveBtn->Click += gcnew System::EventHandler(this, &AssociationsUI::saveBtn_Click);
            // 
            // cancelBtn
            // 
            this->cancelBtn->AutoSize = true;
            this->cancelBtn->Location = System::Drawing::Point(359, 206);
            this->cancelBtn->Name = L"cancelBtn";
            this->cancelBtn->Size = System::Drawing::Size(75, 23);
            this->cancelBtn->TabIndex = 12;
            this->cancelBtn->Text = L"Cancel";
            this->cancelBtn->UseVisualStyleBackColor = true;
            this->cancelBtn->Visible = false;
            this->cancelBtn->Click += gcnew System::EventHandler(this, &AssociationsUI::cancelBtn_Click);
            // 
            // editBtn
            // 
            this->editBtn->AutoSize = true;
            this->editBtn->Location = System::Drawing::Point(278, 177);
            this->editBtn->Name = L"editBtn";
            this->editBtn->Size = System::Drawing::Size(75, 23);
            this->editBtn->TabIndex = 2;
            this->editBtn->Text = L"Edit";
            this->editBtn->UseVisualStyleBackColor = true;
            this->editBtn->Click += gcnew System::EventHandler(this, &AssociationsUI::editBtn_Click);
            // 
            // endBtn
            // 
            this->endBtn->Location = System::Drawing::Point(164, 177);
            this->endBtn->Margin = System::Windows::Forms::Padding(0, 3, 0, 3);
            this->endBtn->Name = L"endBtn";
            this->endBtn->Size = System::Drawing::Size(31, 23);
            this->endBtn->TabIndex = 5;
            this->endBtn->Text = L">|";
            this->endBtn->UseVisualStyleBackColor = true;
            this->endBtn->Click += gcnew System::EventHandler(this, &AssociationsUI::endBtn_Click);
            // 
            // closeBtn
            // 
            this->closeBtn->AutoSize = true;
            this->closeBtn->Location = System::Drawing::Point(359, 177);
            this->closeBtn->Name = L"closeBtn";
            this->closeBtn->Size = System::Drawing::Size(75, 23);
            this->closeBtn->TabIndex = 1;
            this->closeBtn->Text = L"Close";
            this->closeBtn->UseVisualStyleBackColor = true;
            this->closeBtn->Click += gcnew System::EventHandler(this, &AssociationsUI::closeBtn_Click);
            // 
            // forward10Btn
            // 
            this->forward10Btn->Location = System::Drawing::Point(133, 177);
            this->forward10Btn->Margin = System::Windows::Forms::Padding(0, 3, 0, 3);
            this->forward10Btn->Name = L"forward10Btn";
            this->forward10Btn->Size = System::Drawing::Size(31, 23);
            this->forward10Btn->TabIndex = 10;
            this->forward10Btn->Text = L">>";
            this->forward10Btn->UseVisualStyleBackColor = true;
            this->forward10Btn->Click += gcnew System::EventHandler(this, &AssociationsUI::forward10Btn_Click);
            // 
            // startBtn
            // 
            this->startBtn->Location = System::Drawing::Point(1, 177);
            this->startBtn->Margin = System::Windows::Forms::Padding(1, 3, 0, 3);
            this->startBtn->Name = L"startBtn";
            this->startBtn->Size = System::Drawing::Size(31, 23);
            this->startBtn->TabIndex = 6;
            this->startBtn->Text = L"|<";
            this->startBtn->UseVisualStyleBackColor = true;
            this->startBtn->Click += gcnew System::EventHandler(this, &AssociationsUI::startBtn_Click);
            // 
            // forwardBtn
            // 
            this->forwardBtn->Location = System::Drawing::Point(102, 177);
            this->forwardBtn->Margin = System::Windows::Forms::Padding(8, 3, 0, 3);
            this->forwardBtn->Name = L"forwardBtn";
            this->forwardBtn->Size = System::Drawing::Size(31, 23);
            this->forwardBtn->TabIndex = 9;
            this->forwardBtn->Text = L">";
            this->forwardBtn->UseVisualStyleBackColor = true;
            this->forwardBtn->Click += gcnew System::EventHandler(this, &AssociationsUI::forwardBtn_Click);
            // 
            // back10Btn
            // 
            this->back10Btn->Location = System::Drawing::Point(32, 177);
            this->back10Btn->Margin = System::Windows::Forms::Padding(0, 3, 0, 3);
            this->back10Btn->Name = L"back10Btn";
            this->back10Btn->Size = System::Drawing::Size(31, 23);
            this->back10Btn->TabIndex = 7;
            this->back10Btn->Text = L"<<";
            this->back10Btn->UseVisualStyleBackColor = true;
            this->back10Btn->Click += gcnew System::EventHandler(this, &AssociationsUI::back10Btn_Click);
            // 
            // backBtn
            // 
            this->backBtn->Location = System::Drawing::Point(63, 177);
            this->backBtn->Margin = System::Windows::Forms::Padding(0, 3, 0, 3);
            this->backBtn->Name = L"backBtn";
            this->backBtn->Size = System::Drawing::Size(31, 23);
            this->backBtn->TabIndex = 8;
            this->backBtn->Text = L"<";
            this->backBtn->UseVisualStyleBackColor = true;
            this->backBtn->Click += gcnew System::EventHandler(this, &AssociationsUI::backBtn_Click);
            // 
            // menuStrip1
            // 
            menuStrip1->AllowMerge = false;
            menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->statisticsToolStripMenuItem });
            menuStrip1->Location = System::Drawing::Point(0, 0);
            menuStrip1->Name = L"menuStrip1";
            menuStrip1->Size = System::Drawing::Size(437, 24);
            menuStrip1->TabIndex = 13;
            menuStrip1->Text = L"menuStrip1";
            // 
            // statisticsToolStripMenuItem
            // 
            this->statisticsToolStripMenuItem->Name = L"statisticsToolStripMenuItem";
            this->statisticsToolStripMenuItem->Size = System::Drawing::Size(65, 20);
            this->statisticsToolStripMenuItem->Text = L"Statistics";
            this->statisticsToolStripMenuItem->Click += gcnew System::EventHandler(this, &AssociationsUI::statisticsToolStripMenuItem_Click);
            // 
            // AssociationsUI
            // 
            this->ClientSize = System::Drawing::Size(437, 278);
            this->Controls->Add(tableLayoutPanel1);
            this->Controls->Add(statusStrip1);
            this->Controls->Add(menuStrip1);
            this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
            this->MainMenuStrip = menuStrip1;
            this->MaximizeBox = false;
            this->MinimizeBox = false;
            this->Name = L"AssociationsUI";
            this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
            this->Text = L"Association";
            this->Closing += gcnew System::ComponentModel::CancelEventHandler(this, &AssociationsUI::ClosingEvent);
            this->Load += gcnew System::EventHandler(this, &AssociationsUI::AssociationsUI_Load);
            statusStrip1->ResumeLayout(false);
            statusStrip1->PerformLayout();
            associationDetails->ResumeLayout(false);
            detailsPage->ResumeLayout(false);
            tableLayoutPanel2->ResumeLayout(false);
            tableLayoutPanel2->PerformLayout();
            referencesTab->ResumeLayout(false);
            referencesTab->PerformLayout();
            pealbaseGroup->ResumeLayout(false);
            pealbaseGroup->PerformLayout();
            linksTab->ResumeLayout(false);
            linkedAssociationsLayout->ResumeLayout(false);
            linkedAssociationsLayout->PerformLayout();
            tableLayoutPanel1->ResumeLayout(false);
            tableLayoutPanel1->PerformLayout();
            menuStrip1->ResumeLayout(false);
            menuStrip1->PerformLayout();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion
    };
}
