#include "DatabaseManager.h"
#include "NewStageUI.h"
#include "SystemDefaultIcon.h"

using namespace DucoUI;
using namespace System;
using namespace System::Windows::Forms;

NewStageUI::NewStageUI(DatabaseManager^ theManager)
    : database(theManager)
{
    InitializeComponent();
}

NewStageUI::~NewStageUI()
{
    if (components)
    {
        delete components;
    }
}

System::Void
NewStageUI::addBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    System::String^ orderName = "";
    unsigned int order (Convert::ToInt16(noOfBellsEditor->Value));
    if (database->FindOrderName(order, orderName))
    {
        MessageBox::Show(this, "An order name for this number of bells already exists", "Error", MessageBoxButtons::OK, MessageBoxIcon::Error);
    }
    else
    {
        orderName = stageNameEditor->Text;
        if (!database->AddOrderName(order, orderName))
        {
            MessageBox::Show(this, "Could not create order name", "Error", MessageBoxButtons::OK, MessageBoxIcon::Error);
        }
        Close();
    }
}

System::Void
NewStageUI::cancelBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void
NewStageUI::NewStageUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
}
