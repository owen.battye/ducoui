#include "RenumberProgressWrapper.h"
#include "DatabaseManager.h"

#include "RenumberUI.h"

#include "SystemDefaultIcon.h"
#include "SoundUtils.h"
#include "RingingDatabaseObserver.h"
#include "PealTableUI.h"

using namespace Duco;
using namespace System::Windows::Forms;
using namespace System::ComponentModel;

using namespace DucoUI;

RenumberUI::RenumberUI(DatabaseManager^ theDatabase, System::Windows::Forms::Form^ parent)
:   database(theDatabase), mainForm(parent), stage(RenumberProgressCallback::ENotStarted)
{
    progressWrapper = new RenumberProgressWrapper(this);
    InitializeComponent();
}

RenumberUI::!RenumberUI()
{
    delete progressWrapper;
    progressWrapper = NULL;
}

RenumberUI::~RenumberUI()
{
    this->!RenumberUI();
    backgroundWorker1->CancelAsync();
    if (components)
    {
        delete components;
    }
}

System::Void
RenumberUI::RenumberUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    if (database->NothingInEditMode())
    {
        backgroundWorker1->RunWorkerAsync(this);
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void 
RenumberUI::RenumberUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e)
{
    if (this->backgroundWorker1->IsBusy)
    {
        MessageBox::Show(this, "Cannot close while rebuild is in progress", "Rebuild in progress", MessageBoxButtons::OK, MessageBoxIcon::Error);
        e->Cancel = true;
    }
    else
    {
        PealTableUI::ShowAllPeals(database, mainForm);
    }
}

System::Void 
RenumberUI::backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
    BackgroundWorker^ worker = dynamic_cast<BackgroundWorker^>(sender);
    if ( worker->CancellationPending )
    {
        e->Cancel = true;
    }

    totalProgress->Value = 0;
    totalProgress->Minimum = 0;
    totalProgress->Maximum = 100;
    taskProgress->Value = 0;
    taskProgress->Minimum = 0;
    taskProgress->Maximum = 100;

	database->RebuildDatabase(progressWrapper);
}

System::Void 
RenumberUI::backgroundWorker1_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    taskProgress->Value = 0;
    totalProgress->Value = 0;
    taskLbl->Text = "Completed";
    Close();
}

System::Void 
RenumberUI::backgroundWorker1_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    if (updateText)
    {
        switch (stage)
        {
        case RenumberProgressCallback::ENotStarted:
            taskLbl->Text = "Starting";
            break;
        case RenumberProgressCallback::ERenumberPeals:
            taskLbl->Text = "Renumber Peals";
            break;
        case RenumberProgressCallback::ESortingPeals:
            taskLbl->Text = "Sorting Peals";
            break;
        case RenumberProgressCallback::EReindexTowers:
            taskLbl->Text = "Reindexing Towers";
            break;
        case RenumberProgressCallback::ERenumberTowers:
            taskLbl->Text = "Renumber Towers";
            break;
        case RenumberProgressCallback::ERebuildTowers:
            taskLbl->Text = "Rebuilding Towers";
            break;
        case RenumberProgressCallback::ERebuildRings:
            taskLbl->Text = "Rebuilding Rings";
            break;
        case RenumberProgressCallback::EReindexMethods:
            taskLbl->Text = "Reindexing methods";
            break;
        case RenumberProgressCallback::ERenumberMethods:
            taskLbl->Text = "Renumber methods";
            break;
        case RenumberProgressCallback::ERebuildMethods:
            taskLbl->Text = "Rebuilding methods";
            break;
        case RenumberProgressCallback::ERebuildMethodSeries:
            taskLbl->Text = "Rebuilding method series";
            break;
        case RenumberProgressCallback::EReindexRingers:
            taskLbl->Text = "Reindexing ringers";
            break;
        case RenumberProgressCallback::ERenumberRingers:
            taskLbl->Text = "Renumber ringers";
            break;
        case RenumberProgressCallback::ERebuildRingers:
            taskLbl->Text = "Rebuilding ringers";
            break;
        case RenumberProgressCallback::EReindexMethodSeries:
            taskLbl->Text = "Reindexing method series";
            break;
        case RenumberProgressCallback::ERenumberMethodsSeries:
            taskLbl->Text = "Renumber method series";
            break;
        case RenumberProgressCallback::ERebuildMethodsSeries:
            taskLbl->Text = "Rebuilding method series";
            break;

        case RenumberProgressCallback::EReindexCompositions:
            taskLbl->Text = "Reindexing compositions";
            break;
        case RenumberProgressCallback::ERenumberCompositions:
            taskLbl->Text = "Renumber compositions";
            break;
        case RenumberProgressCallback::ERebuildCompositions:
            taskLbl->Text = "Rebuilding compositions";

        case RenumberProgressCallback::EReindexPictures:
            taskLbl->Text = "Reindexing pictures";
            break;
        case RenumberProgressCallback::ERenumberPictures:
            taskLbl->Text = "Renumber pictures";
            break;
        case RenumberProgressCallback::ERebuildPictures:
            taskLbl->Text = "Rebuilding picture references in peals";

        default:
            break;
        }
        updateText = false;
        previousTotalProgressPercent = int(double(int(stage)-1) * (double(100) / double(RenumberProgressCallback::ENumberOfRebuildStages)));
    }
    int progressPercentage = e->ProgressPercentage;
	_ASSERT(progressPercentage < 100);
    taskProgress->Value = progressPercentage;
    int taskProgressAsPercentOfTotal = int(double (progressPercentage)/double(RenumberProgressCallback::ENumberOfRebuildStages));
	if ( (taskProgressAsPercentOfTotal+previousTotalProgressPercent) < 100)
		totalProgress->Value = taskProgressAsPercentOfTotal + previousTotalProgressPercent;
	else
		totalProgress->Value = 100;
}

void 
RenumberUI::RenumberInitialised(RenumberProgressCallback::TRenumberStage newStage)
{
    if (newStage != stage)
    {
        stage = newStage;
        updateText = true;
        backgroundWorker1->ReportProgress(0);
    }
}

void 
RenumberUI::RenumberStep(size_t progress, size_t total)
{
    if (progress < total)
    {
        int progressPercent = int ((double(progress) / double(total)) * 100);
		_ASSERT(progressPercent < 100);
        backgroundWorker1->ReportProgress(progressPercent);
    }
    else
    {
        backgroundWorker1->ReportProgress(99);
    }
}

void 
RenumberUI::RenumberComplete()
{
}
