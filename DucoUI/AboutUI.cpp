#include <DucoVersionNumber.h>
#include "DatabaseManager.h"

#include "AboutUI.h"

#include "DucoCommon.h"
#include "DucoUtils.h"
#include "SystemDefaultIcon.h"
#include <RingingDatabase.h>
#include <DucoConfiguration.h>

using namespace DucoUI;
using namespace System;

AboutUI::AboutUI(DucoUI::DatabaseManager^ theDatabase)
: database(theDatabase)
{
    InitializeComponent();

#ifdef _WIN64
    versionLabel->Text = "Version: " + KUIVersionNumber + " (x64)";
#else
    versionLabel->Text = "Version: " + KUIVersionNumber + " (x86)";
#endif

    aboutBox->Text = "Please send any comments or details of defects discovered to mailto:owen@rocketpole.co.uk?subject=Suggestion%20for%20Duco%20version%20" + KUIVersionNumber + "\n";
    aboutBox->Text += "\nNo responsibility can be accepted by the authors of this software for any loss or damage, however caused, to any computer hardware, software, or data, however perceived, whether through the use of this software or it's installation.";
    aboutBox->Text += "\nStriking quality can deteriorate as well as improve. Do not base any band placings on statistics from this program!";
    aboutBox->Text += "\n\nUsing Duco engine version: " + DucoUtils::ConvertString(Duco::DucoConfiguration::VersionNumber().Str()) + ".";
    aboutBox->Text += "\n\nDuco engine is available to developers here https://www.nuget.org/packages/DucoEngine/ and https://gitlab.com/owen.battye/ducoengine.";
    aboutBox->Text += "\nSupports Duco database versions up to and including: " + Convert::ToSingle(Duco::DucoConfiguration::LatestDatabaseVersion());

    aboutBox->Text += "\n\nUses Xerces to process XML files, see http://xerces.apache.org/.";
    aboutBox->Text += "\n\nUses libcpr download files from webpages, see https://github.com/libcpr/cpr.";
    aboutBox->Text += "\nUses RudeConfig to process configuration files, see http://rudeserver.com/config/";
    aboutBox->Text += "\nUses CEFSharp to host a chrome browser to download performances and show performance maps, see https://github.com/cefsharp/CefSharp";
    aboutBox->Text += "\nUses MapBox to generate performance maps, see https://docs.mapbox.com/";
    aboutBox->Text += "\nOriginal performance icons from https://icons8.com/";
}

AboutUI::!AboutUI()
{
    delete versionNo;
}

AboutUI::~AboutUI()
{
    this->!AboutUI();
    if (components)
    {
        delete components;
    }
}

System::Void
AboutUI::CloseButton_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void
AboutUI::AboutUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
}

System::Void
AboutUI::AboutUI_Activated(System::Object^  sender, System::EventArgs^  e)
{
    if (database->DatabaseOpen())
    {
        fileNameLbl->Text = "\t" + database->Filename();
        databaseCreatedDateLbl->Visible = true;
        dbCreatedDateLbl->Visible = true;
        databaseCreatedDateLbl->Text = database->DatabaseCreatedDate()->ToShortDateString();
        databaseVersionLbl->Text = Convert::ToString(database->Database().DatabaseVersion());
    }
    else
    {
        fileNameLbl->Text = "\tNo database open";
        databaseCreatedDateLbl->Visible = false;
        dbCreatedDateLbl->Visible = false;
    }
}

System::Void
AboutUI::AboutBox_LinkClicked(System::Object^  sender, System::Windows::Forms::LinkClickedEventArgs^  e)
{
    System::Diagnostics::Process::Start( e->LinkText );
}
