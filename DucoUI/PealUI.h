#pragma once
namespace DucoUI
{
    public ref class PealUI : public System::Windows::Forms::Form, public DucoUI::RingingDatabaseObserver
    {
    public:
        PealUI(DucoUI::DatabaseManager^ theDatabase, DucoUI::DucoWindowState state);
        static void ShowPeal(DucoUI::DatabaseManager^ theDatabase, System::Windows::Forms::Form^ parent, const Duco::ObjectId& pealId);
        static PealUI^ PreparePealUI(DucoUI::DatabaseManager^ theDatabase, System::Windows::Forms::Form^ parent, const Duco::DownloadedPeal& thePeal);

        // from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

        //For setting specific settings when peal download had errors
        System::Void SetMissingAssociationFromDownload(System::String^ newAssociation);
        System::Void SetMissingMethodFromDownload(System::String^ methodName);
        System::Void SetMissingTowerFromDownload(System::String^ towerName, System::String^ tenorWeight, System::String^ tenorKey, System::String^ towerbaseId, System::String^ doveId);
        System::Void SetMissingRingFromDownload(System::String^ ringName);
        System::Void SetMissingRingerFromDownload(int bellNumber, System::String^ name, bool strapper, bool conductor);
        System::Void DownloadComplete();

    protected:
        PealUI(DucoUI::DatabaseManager^ theDatabase, const Duco::ObjectId& pealId);
        PealUI(DucoUI::DatabaseManager^ theDatabase, const Duco::DownloadedPeal& thePeal);
        ~PealUI();
        !PealUI();
        void CreateRingersList();
        void SetWindowTitle();
        void InitializePictureControl();

        void SetViewState();
        void SetEditState();
        void SetNewState();
        void PopulateView();
        void PopulateIndexIndicator();
        void PopulateViewWithPeal(const Duco::ObjectId& pealId);
        void ClearView();
        System::Void SetViewStateAndShowLastPeal();
        System::Void RingerObjectChanged(System::Object^  sender, System::EventArgs^  e);

        System::Void PealUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void ClosingEvent(System::Object^ object, System::ComponentModel::CancelEventArgs^ e);

        System::Void EditBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void CancelBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void CloseBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void SaveBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void CreateMissingMethod(System::Object^  sender, System::EventArgs^  e);
        System::Void CreateMissingAssociation(System::Object^  sender, System::EventArgs^  e);
        System::Void CreateMissingTower(System::Object^  sender, System::EventArgs^  e);
        System::Void StartBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void Back10Btn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void PreviousPeal(System::Object^  sender, System::EventArgs^  e);
        System::Void NextPeal(System::Object^  sender, System::EventArgs^  e);
        System::Void Forward10Btn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void EndBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void CreateMissingRingersBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void PealIdLabel_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void DeleteBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void PrintCmd_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void CopyToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void ViewOnlineToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void uploadBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void openBellboardBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void openRingingWorldBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void StatsBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void BandStatsBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void PrintBandStatsInTower(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args);
        System::Void FindSeriesBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void FindCompositionBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void PealFeesBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void PictureEvent(System::Object^  sender, System::EventArgs^  e);
        System::Void PrintPeal(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args);

        System::Void TowerChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void RingChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void TowerTextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void RingTextChanged(System::Object^  sender, System::EventArgs^  e);

        System::Void ChangesEditor_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void TimeEditor_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void FootnotesEditor_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void DateEditor_ValueChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void ComposerEditor_ValueChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void MultiMethodEditor_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void AssociationEditor_ValueChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void AssociationEditor_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void HandbellSelector_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void WrongStagesSelector_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void DoubleHanded_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void notable_CheckedChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void SimulatedSound_CheckedChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void WithdrawnEditor_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void MethodEditor_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void SeriesEditor_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void SeriesEditor_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void CompositionEditor_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void CompositionEditor_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void MethodEditor_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void TabPage1_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
        System::Void RwReference_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void BellBoardReference_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void NotesEditor_TextChanged(System::Object^  sender, System::EventArgs^  e);

        System::Void SingleConductor_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void SilentConductor_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void JointConductor_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void SetConductorStates();

        System::Void GenerateComposerOptions();
        System::Void UpdateValidationLabel();

        System::Void NumericEditor_KeyPressed( System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e );
        System::Void NumericEditor_KeyDown( System::Object^ sender, System::Windows::Forms::KeyEventArgs^ e );
    private:
        System::ComponentModel::IContainer^ components;
        DucoUI::DatabaseManager^            database;
        DucoUI::DucoWindowState             state;
        DucoUI::PictureControl^             pictureCtrl;
        DucoUI::PrintBandSummaryUtil^       printBandStatsUtil;

    private: System::Windows::Forms::TabControl^  tabControl1;
    private: System::Windows::Forms::TextBox^  changesEditor;
    private: System::Windows::Forms::TextBox^  timeEditor;
    private: System::Windows::Forms::TextBox^  footnotesEditor;
    private: System::Windows::Forms::TextBox^  multiMethodEditor;
    private: System::Windows::Forms::ComboBox^  towerEditor;
    private: System::Windows::Forms::ComboBox^  associationEditor;
    private: System::Windows::Forms::ComboBox^  methodEditor;
    private: System::Windows::Forms::ComboBox^  ringSelector;
    private: System::Windows::Forms::ComboBox^  composerEditor;
    private: System::Windows::Forms::ComboBox^  seriesEditor;
    private: System::Windows::Forms::ComboBox^  compositionEditor;
    private: System::Windows::Forms::CheckBox^  handbellSelector;
    private: System::Windows::Forms::CheckBox^  wrongStagesSelector;
    private: System::Windows::Forms::CheckBox^  doubleHanded;
    private: System::Windows::Forms::DateTimePicker^  dateEditor;
    private: System::Windows::Forms::RadioButton^  jointConductor;
    private: System::Windows::Forms::RadioButton^  silentConductor;
    private: System::Windows::Forms::RadioButton^  singleConductor;
        System::Windows::Forms::Label^          ringTenorLbl;
        DucoUI::RingerList^                             ringerList;

    private: System::Windows::Forms::Button^  startBtn;
    private: System::Windows::Forms::Button^  previousBtn;
    private: System::Windows::Forms::Button^  back10Btn;
    private: System::Windows::Forms::Button^  nextBtn;
    private: System::Windows::Forms::Button^  forward10Btn;
    private: System::Windows::Forms::Button^  endBtn;
    private: System::Windows::Forms::Button^  editBtn;
    private: System::Windows::Forms::Button^  saveBtn;
    private: System::Windows::Forms::Button^  closeBtn;
    private: System::Windows::Forms::Button^  cancelBtn;
    private: System::Windows::Forms::Button^  findMethodBtn;
    private: System::Windows::Forms::Button^  findTowerBtn;
    private: System::Windows::Forms::Button^  deleteBtn;
    private: System::Windows::Forms::Button^  createMissingRingersBtn;
    private: System::Windows::Forms::Button^  findSeriesBtn;
    private: System::Windows::Forms::Button^  findCompositionBtn;
    private: System::Windows::Forms::Button^  pealFeesBtn;
        System::Windows::Forms::ToolStripMenuItem^  printCmd;
        System::Windows::Forms::ToolStripStatusLabel^  pealIdLbl;
        System::Windows::Forms::ToolStripMenuItem^  copyToolStripMenuItem;
        System::Windows::Forms::TextBox^    rwReference;
        System::Windows::Forms::TextBox^    bellBoardReference;
        System::Windows::Forms::TextBox^    notesEditor;
        System::Windows::Forms::CheckBox^   withdrawnEditor;
        System::Windows::Forms::ToolTip^    toolTip;
        System::Windows::Forms::GroupBox^   seriesGroup;

        Duco::Peal*                         currentPeal;
        Duco::Tower*                        currentTower;

        System::Collections::Generic::List<System::Int16>^  allTowerNames;
        System::Collections::Generic::List<System::Int16>^  allRingNames;
        System::Collections::Generic::List<System::Int16>^  allMethodNames;
        System::Collections::Generic::List<System::Int16>^  allCompositionNames;
        System::Collections::Generic::List<System::Int16>^  allAssociationNames;
        System::Collections::Generic::List<System::Int16>^  allSeriesNames;

        bool                    nonNumberEntered;
        bool                    generatingNewMethod;
        bool                    generatingNewSeries;
        bool                    generatingNewComposition;
        bool                    generatingNewTower;
        bool                    updatingExistingTower;
        bool                    dataChanged;
        bool                    populatingFromDownload;

        System::String^         downloadTenorWeight; // used by download dialog to auto gen tenor of new tower
        System::String^         downloadTenorKey; // used by download dialog to auto gen tenor of new tower
        System::String^         downloadTowerbaseId; // used by download dialog to auto gen tenor of new tower
        System::String^         downloadDoveId; // used by download dialog to auto gen tenor of new tower

private: System::Windows::Forms::TabPage^  picturePage;
private: System::Windows::Forms::GroupBox^  conductorGroup;
private: System::Windows::Forms::Label^  label4;
private: System::Windows::Forms::Label^  label3;
private: System::Windows::Forms::GroupBox^  compositionGroup;
private: System::Windows::Forms::TabPage^  otherPage;
private: System::Windows::Forms::ToolStripMenuItem^  viewOnlineBtn;
private: System::Windows::Forms::ToolStripMenuItem^  uploadBtn;
private: System::Windows::Forms::ToolStripMenuItem^  statisticsToolStripMenuItem1;
private: System::Windows::Forms::ToolStripMenuItem^  bandStatsBtn;
private: System::Windows::Forms::ToolStripMenuItem^  pealSpeedBtn;
private: System::Windows::Forms::ToolStripStatusLabel^ validationLabel;
private: System::Windows::Forms::Button^ openBellboardBtn;
private: System::Windows::Forms::Label^ footNotesLbl;
private: System::Windows::Forms::Label^ multiMethodLbl;
private: System::Windows::Forms::Button^ openRingingWorldBtn;
private: System::Windows::Forms::CheckBox^ simulatedSound;
private: System::Windows::Forms::TableLayoutPanel^ ringerTabLayoutPanel;
private: System::Windows::Forms::CheckBox^ notable;

        void InitializeComponent(void)
        {
            this->components = (gcnew System::ComponentModel::Container());
            System::Windows::Forms::TabPage^ tabPage1;
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
            System::Windows::Forms::Label^ AssociationLbl;
            System::Windows::Forms::Label^ ComposerLbl;
            System::Windows::Forms::Label^ MethodLbl;
            System::Windows::Forms::Label^ TowerLbl;
            System::Windows::Forms::Label^ DateLbl;
            System::Windows::Forms::Label^ TimeLbl;
            System::Windows::Forms::Label^ RingLbl;
            System::Windows::Forms::Label^ label1;
            System::Windows::Forms::MenuStrip^ pealUIMenu;
            System::Windows::Forms::ToolStripMenuItem^ moreMenu;
            System::Windows::Forms::StatusStrip^ statusStrip1;
            System::Windows::Forms::Label^ bellboardReferenceLbl;
            System::Windows::Forms::GroupBox^ rwGroup;
            System::Windows::Forms::GroupBox^ notesGroup;
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel2;
            System::Windows::Forms::Panel^ buttonsPanel;
            System::Windows::Forms::TabPage^ ringerTab;
            System::Windows::Forms::TableLayoutPanel^ otherPageLayoutPanel;
            System::Windows::Forms::Panel^ otherSettingdPanel;
            System::Windows::Forms::GroupBox^ bellboardGroup;
            this->footNotesLbl = (gcnew System::Windows::Forms::Label());
            this->footnotesEditor = (gcnew System::Windows::Forms::TextBox());
            this->multiMethodEditor = (gcnew System::Windows::Forms::TextBox());
            this->composerEditor = (gcnew System::Windows::Forms::ComboBox());
            this->multiMethodLbl = (gcnew System::Windows::Forms::Label());
            this->methodEditor = (gcnew System::Windows::Forms::ComboBox());
            this->changesEditor = (gcnew System::Windows::Forms::TextBox());
            this->dateEditor = (gcnew System::Windows::Forms::DateTimePicker());
            this->ringTenorLbl = (gcnew System::Windows::Forms::Label());
            this->towerEditor = (gcnew System::Windows::Forms::ComboBox());
            this->associationEditor = (gcnew System::Windows::Forms::ComboBox());
            this->timeEditor = (gcnew System::Windows::Forms::TextBox());
            this->ringSelector = (gcnew System::Windows::Forms::ComboBox());
            this->findTowerBtn = (gcnew System::Windows::Forms::Button());
            this->findMethodBtn = (gcnew System::Windows::Forms::Button());
            this->viewOnlineBtn = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->uploadBtn = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->statisticsToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->bandStatsBtn = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->pealSpeedBtn = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->copyToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->printCmd = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->validationLabel = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->pealIdLbl = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->openRingingWorldBtn = (gcnew System::Windows::Forms::Button());
            this->rwReference = (gcnew System::Windows::Forms::TextBox());
            this->notesEditor = (gcnew System::Windows::Forms::TextBox());
            this->cancelBtn = (gcnew System::Windows::Forms::Button());
            this->endBtn = (gcnew System::Windows::Forms::Button());
            this->deleteBtn = (gcnew System::Windows::Forms::Button());
            this->nextBtn = (gcnew System::Windows::Forms::Button());
            this->forward10Btn = (gcnew System::Windows::Forms::Button());
            this->back10Btn = (gcnew System::Windows::Forms::Button());
            this->closeBtn = (gcnew System::Windows::Forms::Button());
            this->previousBtn = (gcnew System::Windows::Forms::Button());
            this->saveBtn = (gcnew System::Windows::Forms::Button());
            this->startBtn = (gcnew System::Windows::Forms::Button());
            this->editBtn = (gcnew System::Windows::Forms::Button());
            this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
            this->ringerTabLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
            this->conductorGroup = (gcnew System::Windows::Forms::GroupBox());
            this->singleConductor = (gcnew System::Windows::Forms::RadioButton());
            this->silentConductor = (gcnew System::Windows::Forms::RadioButton());
            this->jointConductor = (gcnew System::Windows::Forms::RadioButton());
            this->createMissingRingersBtn = (gcnew System::Windows::Forms::Button());
            this->pealFeesBtn = (gcnew System::Windows::Forms::Button());
            this->otherPage = (gcnew System::Windows::Forms::TabPage());
            this->simulatedSound = (gcnew System::Windows::Forms::CheckBox());
            this->notable = (gcnew System::Windows::Forms::CheckBox());
            this->withdrawnEditor = (gcnew System::Windows::Forms::CheckBox());
            this->doubleHanded = (gcnew System::Windows::Forms::CheckBox());
            this->wrongStagesSelector = (gcnew System::Windows::Forms::CheckBox());
            this->handbellSelector = (gcnew System::Windows::Forms::CheckBox());
            this->compositionGroup = (gcnew System::Windows::Forms::GroupBox());
            this->compositionEditor = (gcnew System::Windows::Forms::ComboBox());
            this->findCompositionBtn = (gcnew System::Windows::Forms::Button());
            this->openBellboardBtn = (gcnew System::Windows::Forms::Button());
            this->bellBoardReference = (gcnew System::Windows::Forms::TextBox());
            this->seriesGroup = (gcnew System::Windows::Forms::GroupBox());
            this->seriesEditor = (gcnew System::Windows::Forms::ComboBox());
            this->findSeriesBtn = (gcnew System::Windows::Forms::Button());
            this->picturePage = (gcnew System::Windows::Forms::TabPage());
            this->toolTip = (gcnew System::Windows::Forms::ToolTip(this->components));
            tabPage1 = (gcnew System::Windows::Forms::TabPage());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            AssociationLbl = (gcnew System::Windows::Forms::Label());
            ComposerLbl = (gcnew System::Windows::Forms::Label());
            MethodLbl = (gcnew System::Windows::Forms::Label());
            TowerLbl = (gcnew System::Windows::Forms::Label());
            DateLbl = (gcnew System::Windows::Forms::Label());
            TimeLbl = (gcnew System::Windows::Forms::Label());
            RingLbl = (gcnew System::Windows::Forms::Label());
            label1 = (gcnew System::Windows::Forms::Label());
            pealUIMenu = (gcnew System::Windows::Forms::MenuStrip());
            moreMenu = (gcnew System::Windows::Forms::ToolStripMenuItem());
            statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            bellboardReferenceLbl = (gcnew System::Windows::Forms::Label());
            rwGroup = (gcnew System::Windows::Forms::GroupBox());
            notesGroup = (gcnew System::Windows::Forms::GroupBox());
            tableLayoutPanel2 = (gcnew System::Windows::Forms::TableLayoutPanel());
            buttonsPanel = (gcnew System::Windows::Forms::Panel());
            ringerTab = (gcnew System::Windows::Forms::TabPage());
            otherPageLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
            otherSettingdPanel = (gcnew System::Windows::Forms::Panel());
            bellboardGroup = (gcnew System::Windows::Forms::GroupBox());
            tabPage1->SuspendLayout();
            tableLayoutPanel1->SuspendLayout();
            pealUIMenu->SuspendLayout();
            statusStrip1->SuspendLayout();
            rwGroup->SuspendLayout();
            notesGroup->SuspendLayout();
            tableLayoutPanel2->SuspendLayout();
            buttonsPanel->SuspendLayout();
            this->tabControl1->SuspendLayout();
            ringerTab->SuspendLayout();
            this->ringerTabLayoutPanel->SuspendLayout();
            this->conductorGroup->SuspendLayout();
            this->otherPage->SuspendLayout();
            otherPageLayoutPanel->SuspendLayout();
            otherSettingdPanel->SuspendLayout();
            this->compositionGroup->SuspendLayout();
            bellboardGroup->SuspendLayout();
            this->seriesGroup->SuspendLayout();
            this->SuspendLayout();
            // 
            // tabPage1
            // 
            tabPage1->Controls->Add(tableLayoutPanel1);
            tabPage1->Location = System::Drawing::Point(4, 22);
            tabPage1->Name = L"tabPage1";
            tabPage1->Padding = System::Windows::Forms::Padding(3);
            tabPage1->Size = System::Drawing::Size(377, 387);
            tabPage1->TabIndex = 0;
            tabPage1->Text = L"Details";
            tabPage1->UseVisualStyleBackColor = true;
            tabPage1->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &PealUI::TabPage1_MouseDown);
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 7;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute, 50)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute, 20)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute, 50)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute, 21)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->Controls->Add(AssociationLbl, 0, 0);
            tableLayoutPanel1->Controls->Add(this->footNotesLbl, 0, 7);
            tableLayoutPanel1->Controls->Add(this->footnotesEditor, 1, 7);
            tableLayoutPanel1->Controls->Add(this->multiMethodEditor, 1, 6);
            tableLayoutPanel1->Controls->Add(this->composerEditor, 1, 5);
            tableLayoutPanel1->Controls->Add(this->multiMethodLbl, 0, 6);
            tableLayoutPanel1->Controls->Add(ComposerLbl, 0, 5);
            tableLayoutPanel1->Controls->Add(this->methodEditor, 2, 4);
            tableLayoutPanel1->Controls->Add(this->changesEditor, 1, 4);
            tableLayoutPanel1->Controls->Add(this->dateEditor, 1, 3);
            tableLayoutPanel1->Controls->Add(this->ringTenorLbl, 3, 2);
            tableLayoutPanel1->Controls->Add(MethodLbl, 0, 4);
            tableLayoutPanel1->Controls->Add(TowerLbl, 0, 1);
            tableLayoutPanel1->Controls->Add(this->towerEditor, 1, 1);
            tableLayoutPanel1->Controls->Add(DateLbl, 0, 3);
            tableLayoutPanel1->Controls->Add(this->associationEditor, 1, 0);
            tableLayoutPanel1->Controls->Add(TimeLbl, 4, 3);
            tableLayoutPanel1->Controls->Add(RingLbl, 0, 2);
            tableLayoutPanel1->Controls->Add(this->timeEditor, 5, 3);
            tableLayoutPanel1->Controls->Add(this->ringSelector, 1, 2);
            tableLayoutPanel1->Controls->Add(this->findTowerBtn, 6, 2);
            tableLayoutPanel1->Controls->Add(this->findMethodBtn, 6, 4);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(3, 3);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 8;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 35)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 35)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 35)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->Size = System::Drawing::Size(371, 381);
            tableLayoutPanel1->TabIndex = 23;
            tableLayoutPanel1->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &PealUI::TabPage1_MouseDown);
            // 
            // AssociationLbl
            // 
            AssociationLbl->AutoSize = true;
            AssociationLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            AssociationLbl->Location = System::Drawing::Point(3, 0);
            AssociationLbl->Name = L"AssociationLbl";
            AssociationLbl->Padding = System::Windows::Forms::Padding(0, 6, 0, 0);
            AssociationLbl->Size = System::Drawing::Size(61, 35);
            AssociationLbl->TabIndex = 0;
            AssociationLbl->Text = L"Association";
            AssociationLbl->TextAlign = System::Drawing::ContentAlignment::TopRight;
            // 
            // footNotesLbl
            // 
            this->footNotesLbl->AutoSize = true;
            this->footNotesLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            this->footNotesLbl->Location = System::Drawing::Point(3, 267);
            this->footNotesLbl->Name = L"footNotesLbl";
            this->footNotesLbl->Padding = System::Windows::Forms::Padding(0, 6, 0, 0);
            this->footNotesLbl->Size = System::Drawing::Size(61, 117);
            this->footNotesLbl->TabIndex = 11;
            this->footNotesLbl->Text = L"Footnotes";
            this->footNotesLbl->TextAlign = System::Drawing::ContentAlignment::TopRight;
            // 
            // footnotesEditor
            // 
            this->footnotesEditor->AcceptsReturn = true;
            this->footnotesEditor->AllowDrop = true;
            tableLayoutPanel1->SetColumnSpan(this->footnotesEditor, 6);
            this->footnotesEditor->Dock = System::Windows::Forms::DockStyle::Fill;
            this->footnotesEditor->Location = System::Drawing::Point(70, 270);
            this->footnotesEditor->Multiline = true;
            this->footnotesEditor->Name = L"footnotesEditor";
            this->footnotesEditor->ScrollBars = System::Windows::Forms::ScrollBars::Both;
            this->footnotesEditor->Size = System::Drawing::Size(298, 111);
            this->footnotesEditor->TabIndex = 12;
            this->footnotesEditor->TextChanged += gcnew System::EventHandler(this, &PealUI::FootnotesEditor_TextChanged);
            // 
            // multiMethodEditor
            // 
            this->multiMethodEditor->AcceptsReturn = true;
            this->multiMethodEditor->AllowDrop = true;
            tableLayoutPanel1->SetColumnSpan(this->multiMethodEditor, 6);
            this->multiMethodEditor->Dock = System::Windows::Forms::DockStyle::Fill;
            this->multiMethodEditor->Location = System::Drawing::Point(70, 189);
            this->multiMethodEditor->Multiline = true;
            this->multiMethodEditor->Name = L"multiMethodEditor";
            this->multiMethodEditor->ScrollBars = System::Windows::Forms::ScrollBars::Both;
            this->multiMethodEditor->Size = System::Drawing::Size(298, 75);
            this->multiMethodEditor->TabIndex = 11;
            this->multiMethodEditor->TextChanged += gcnew System::EventHandler(this, &PealUI::MultiMethodEditor_TextChanged);
            // 
            // composerEditor
            // 
            this->composerEditor->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
            this->composerEditor->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            tableLayoutPanel1->SetColumnSpan(this->composerEditor, 6);
            this->composerEditor->Dock = System::Windows::Forms::DockStyle::Fill;
            this->composerEditor->FormattingEnabled = true;
            this->composerEditor->Location = System::Drawing::Point(70, 162);
            this->composerEditor->Name = L"composerEditor";
            this->composerEditor->Size = System::Drawing::Size(298, 21);
            this->composerEditor->TabIndex = 10;
            this->composerEditor->TextChanged += gcnew System::EventHandler(this, &PealUI::ComposerEditor_ValueChanged);
            // 
            // multiMethodLbl
            // 
            this->multiMethodLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            this->multiMethodLbl->Location = System::Drawing::Point(3, 186);
            this->multiMethodLbl->Name = L"multiMethodLbl";
            this->multiMethodLbl->Padding = System::Windows::Forms::Padding(0, 6, 0, 0);
            this->multiMethodLbl->Size = System::Drawing::Size(61, 81);
            this->multiMethodLbl->TabIndex = 21;
            this->multiMethodLbl->Text = L"Multi methods";
            this->multiMethodLbl->TextAlign = System::Drawing::ContentAlignment::TopRight;
            // 
            // ComposerLbl
            // 
            ComposerLbl->AutoSize = true;
            ComposerLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            ComposerLbl->Location = System::Drawing::Point(3, 159);
            ComposerLbl->Name = L"ComposerLbl";
            ComposerLbl->Size = System::Drawing::Size(61, 27);
            ComposerLbl->TabIndex = 9;
            ComposerLbl->Text = L"Composer";
            ComposerLbl->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // methodEditor
            // 
            this->methodEditor->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
            this->methodEditor->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            tableLayoutPanel1->SetColumnSpan(this->methodEditor, 4);
            this->methodEditor->Dock = System::Windows::Forms::DockStyle::Fill;
            this->methodEditor->FormattingEnabled = true;
            this->methodEditor->Location = System::Drawing::Point(120, 135);
            this->methodEditor->Name = L"methodEditor";
            this->methodEditor->Size = System::Drawing::Size(215, 21);
            this->methodEditor->TabIndex = 8;
            this->methodEditor->SelectedIndexChanged += gcnew System::EventHandler(this, &PealUI::MethodEditor_SelectedIndexChanged);
            this->methodEditor->TextChanged += gcnew System::EventHandler(this, &PealUI::MethodEditor_TextChanged);
            // 
            // changesEditor
            // 
            this->changesEditor->Location = System::Drawing::Point(70, 135);
            this->changesEditor->Name = L"changesEditor";
            this->changesEditor->Size = System::Drawing::Size(44, 20);
            this->changesEditor->TabIndex = 7;
            this->changesEditor->TextChanged += gcnew System::EventHandler(this, &PealUI::ChangesEditor_TextChanged);
            this->changesEditor->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &PealUI::NumericEditor_KeyDown);
            this->changesEditor->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &PealUI::NumericEditor_KeyPressed);
            // 
            // dateEditor
            // 
            tableLayoutPanel1->SetColumnSpan(this->dateEditor, 3);
            this->dateEditor->CustomFormat = L"dddd, d MMMM yyyy";
            this->dateEditor->Dock = System::Windows::Forms::DockStyle::Fill;
            this->dateEditor->Format = System::Windows::Forms::DateTimePickerFormat::Custom;
            this->dateEditor->Location = System::Drawing::Point(70, 100);
            this->dateEditor->Name = L"dateEditor";
            this->dateEditor->Size = System::Drawing::Size(194, 20);
            this->dateEditor->TabIndex = 5;
            this->dateEditor->ValueChanged += gcnew System::EventHandler(this, &PealUI::DateEditor_ValueChanged);
            // 
            // ringTenorLbl
            // 
            this->ringTenorLbl->AutoSize = true;
            tableLayoutPanel1->SetColumnSpan(this->ringTenorLbl, 3);
            this->ringTenorLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            this->ringTenorLbl->Location = System::Drawing::Point(250, 62);
            this->ringTenorLbl->Name = L"ringTenorLbl";
            this->ringTenorLbl->Padding = System::Windows::Forms::Padding(0, 6, 0, 0);
            this->ringTenorLbl->Size = System::Drawing::Size(85, 35);
            this->ringTenorLbl->TabIndex = 22;
            // 
            // MethodLbl
            // 
            MethodLbl->AutoSize = true;
            MethodLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            MethodLbl->Location = System::Drawing::Point(3, 132);
            MethodLbl->Name = L"MethodLbl";
            MethodLbl->Size = System::Drawing::Size(61, 27);
            MethodLbl->TabIndex = 8;
            MethodLbl->Text = L"Method";
            MethodLbl->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // TowerLbl
            // 
            TowerLbl->AutoSize = true;
            TowerLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            TowerLbl->Location = System::Drawing::Point(3, 35);
            TowerLbl->Name = L"TowerLbl";
            TowerLbl->Size = System::Drawing::Size(61, 27);
            TowerLbl->TabIndex = 2;
            TowerLbl->Text = L"Tower";
            TowerLbl->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // towerEditor
            // 
            this->towerEditor->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
            this->towerEditor->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            tableLayoutPanel1->SetColumnSpan(this->towerEditor, 6);
            this->towerEditor->Dock = System::Windows::Forms::DockStyle::Fill;
            this->towerEditor->FormattingEnabled = true;
            this->towerEditor->Location = System::Drawing::Point(70, 38);
            this->towerEditor->Name = L"towerEditor";
            this->towerEditor->Size = System::Drawing::Size(298, 21);
            this->towerEditor->TabIndex = 2;
            this->towerEditor->SelectedValueChanged += gcnew System::EventHandler(this, &PealUI::TowerChanged);
            this->towerEditor->TextChanged += gcnew System::EventHandler(this, &PealUI::TowerTextChanged);
            // 
            // DateLbl
            // 
            DateLbl->AutoSize = true;
            DateLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            DateLbl->Location = System::Drawing::Point(3, 97);
            DateLbl->Name = L"DateLbl";
            DateLbl->Padding = System::Windows::Forms::Padding(0, 6, 0, 0);
            DateLbl->Size = System::Drawing::Size(61, 35);
            DateLbl->TabIndex = 7;
            DateLbl->Text = L"Date";
            DateLbl->TextAlign = System::Drawing::ContentAlignment::TopRight;
            // 
            // associationEditor
            // 
            this->associationEditor->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
            this->associationEditor->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            tableLayoutPanel1->SetColumnSpan(this->associationEditor, 6);
            this->associationEditor->Dock = System::Windows::Forms::DockStyle::Fill;
            this->associationEditor->FormattingEnabled = true;
            this->associationEditor->Location = System::Drawing::Point(70, 3);
            this->associationEditor->Name = L"associationEditor";
            this->associationEditor->Size = System::Drawing::Size(298, 21);
            this->associationEditor->TabIndex = 1;
            this->associationEditor->SelectedIndexChanged += gcnew System::EventHandler(this, &PealUI::AssociationEditor_SelectedIndexChanged);
            this->associationEditor->TextChanged += gcnew System::EventHandler(this, &PealUI::AssociationEditor_ValueChanged);
            // 
            // TimeLbl
            // 
            TimeLbl->AutoSize = true;
            TimeLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            TimeLbl->Location = System::Drawing::Point(270, 97);
            TimeLbl->Name = L"TimeLbl";
            TimeLbl->Padding = System::Windows::Forms::Padding(0, 6, 0, 0);
            TimeLbl->Size = System::Drawing::Size(44, 35);
            TimeLbl->TabIndex = 16;
            TimeLbl->Text = L"Time";
            TimeLbl->TextAlign = System::Drawing::ContentAlignment::TopRight;
            // 
            // RingLbl
            // 
            RingLbl->AutoSize = true;
            RingLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            RingLbl->Location = System::Drawing::Point(3, 62);
            RingLbl->Name = L"RingLbl";
            RingLbl->Padding = System::Windows::Forms::Padding(0, 6, 0, 0);
            RingLbl->Size = System::Drawing::Size(61, 35);
            RingLbl->TabIndex = 18;
            RingLbl->Text = L"Ring";
            RingLbl->TextAlign = System::Drawing::ContentAlignment::TopRight;
            // 
            // timeEditor
            // 
            tableLayoutPanel1->SetColumnSpan(this->timeEditor, 2);
            this->timeEditor->Dock = System::Windows::Forms::DockStyle::Fill;
            this->timeEditor->Location = System::Drawing::Point(320, 100);
            this->timeEditor->Name = L"timeEditor";
            this->timeEditor->Size = System::Drawing::Size(48, 20);
            this->timeEditor->TabIndex = 6;
            this->timeEditor->TextChanged += gcnew System::EventHandler(this, &PealUI::TimeEditor_TextChanged);
            this->timeEditor->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &PealUI::NumericEditor_KeyDown);
            this->timeEditor->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &PealUI::NumericEditor_KeyPressed);
            // 
            // ringSelector
            // 
            tableLayoutPanel1->SetColumnSpan(this->ringSelector, 2);
            this->ringSelector->Dock = System::Windows::Forms::DockStyle::Fill;
            this->ringSelector->FormattingEnabled = true;
            this->ringSelector->Location = System::Drawing::Point(70, 65);
            this->ringSelector->Name = L"ringSelector";
            this->ringSelector->Size = System::Drawing::Size(174, 21);
            this->ringSelector->TabIndex = 3;
            this->ringSelector->SelectedIndexChanged += gcnew System::EventHandler(this, &PealUI::RingChanged);
            this->ringSelector->TextChanged += gcnew System::EventHandler(this, &PealUI::RingTextChanged);
            // 
            // findTowerBtn
            // 
            this->findTowerBtn->Location = System::Drawing::Point(341, 65);
            this->findTowerBtn->Name = L"findTowerBtn";
            this->findTowerBtn->Size = System::Drawing::Size(27, 20);
            this->findTowerBtn->TabIndex = 4;
            this->findTowerBtn->Text = L">>";
            this->findTowerBtn->UseVisualStyleBackColor = true;
            this->findTowerBtn->Click += gcnew System::EventHandler(this, &PealUI::CreateMissingTower);
            // 
            // findMethodBtn
            // 
            this->findMethodBtn->Dock = System::Windows::Forms::DockStyle::Fill;
            this->findMethodBtn->Enabled = false;
            this->findMethodBtn->Location = System::Drawing::Point(341, 135);
            this->findMethodBtn->Name = L"findMethodBtn";
            this->findMethodBtn->Size = System::Drawing::Size(27, 21);
            this->findMethodBtn->TabIndex = 9;
            this->findMethodBtn->Text = L">>";
            this->findMethodBtn->UseVisualStyleBackColor = true;
            this->findMethodBtn->Click += gcnew System::EventHandler(this, &PealUI::CreateMissingMethod);
            // 
            // label1
            // 
            label1->AutoSize = true;
            label1->Location = System::Drawing::Point(7, 18);
            label1->Name = L"label1";
            label1->Size = System::Drawing::Size(57, 13);
            label1->TabIndex = 0;
            label1->Text = L"Reference";
            // 
            // pealUIMenu
            // 
            pealUIMenu->AllowMerge = false;
            pealUIMenu->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {
                this->viewOnlineBtn, this->uploadBtn,
                    this->statisticsToolStripMenuItem1, moreMenu
            });
            pealUIMenu->Location = System::Drawing::Point(0, 0);
            pealUIMenu->Name = L"pealUIMenu";
            pealUIMenu->Size = System::Drawing::Size(391, 24);
            pealUIMenu->TabIndex = 85;
            // 
            // viewOnlineBtn
            // 
            this->viewOnlineBtn->Name = L"viewOnlineBtn";
            this->viewOnlineBtn->Size = System::Drawing::Size(80, 20);
            this->viewOnlineBtn->Text = L"&View online";
            this->viewOnlineBtn->ToolTipText = L"View peal on bell board";
            this->viewOnlineBtn->Click += gcnew System::EventHandler(this, &PealUI::ViewOnlineToolStripMenuItem_Click);
            // 
            // uploadBtn
            // 
            this->uploadBtn->Name = L"uploadBtn";
            this->uploadBtn->Size = System::Drawing::Size(57, 20);
            this->uploadBtn->Text = L"&Upload";
            this->uploadBtn->ToolTipText = L"Upload peal to bellboard";
            this->uploadBtn->Click += gcnew System::EventHandler(this, &PealUI::uploadBtn_Click);
            // 
            // statisticsToolStripMenuItem1
            // 
            this->statisticsToolStripMenuItem1->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
                this->bandStatsBtn,
                    this->pealSpeedBtn
            });
            this->statisticsToolStripMenuItem1->Name = L"statisticsToolStripMenuItem1";
            this->statisticsToolStripMenuItem1->Size = System::Drawing::Size(65, 20);
            this->statisticsToolStripMenuItem1->Text = L"Statistics";
            // 
            // bandStatsBtn
            // 
            this->bandStatsBtn->Name = L"bandStatsBtn";
            this->bandStatsBtn->Size = System::Drawing::Size(149, 22);
            this->bandStatsBtn->Text = L"Band statistics";
            this->bandStatsBtn->Click += gcnew System::EventHandler(this, &PealUI::BandStatsBtn_Click);
            // 
            // pealSpeedBtn
            // 
            this->pealSpeedBtn->Name = L"pealSpeedBtn";
            this->pealSpeedBtn->Size = System::Drawing::Size(149, 22);
            this->pealSpeedBtn->Text = L"Peal speed";
            this->pealSpeedBtn->Click += gcnew System::EventHandler(this, &PealUI::StatsBtn_Click);
            // 
            // moreMenu
            // 
            moreMenu->Alignment = System::Windows::Forms::ToolStripItemAlignment::Right;
            moreMenu->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
                this->copyToolStripMenuItem,
                    this->printCmd
            });
            moreMenu->Name = L"moreMenu";
            moreMenu->Size = System::Drawing::Size(47, 20);
            moreMenu->Text = L"&More";
            // 
            // copyToolStripMenuItem
            // 
            this->copyToolStripMenuItem->Name = L"copyToolStripMenuItem";
            this->copyToolStripMenuItem->Size = System::Drawing::Size(102, 22);
            this->copyToolStripMenuItem->Text = L"Copy";
            this->copyToolStripMenuItem->Click += gcnew System::EventHandler(this, &PealUI::CopyToolStripMenuItem_Click);
            // 
            // printCmd
            // 
            this->printCmd->Name = L"printCmd";
            this->printCmd->Size = System::Drawing::Size(102, 22);
            this->printCmd->Text = L"Print";
            this->printCmd->Click += gcnew System::EventHandler(this, &PealUI::PrintCmd_Click);
            // 
            // statusStrip1
            // 
            statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) { this->validationLabel, this->pealIdLbl });
            statusStrip1->Location = System::Drawing::Point(0, 478);
            statusStrip1->Name = L"statusStrip1";
            statusStrip1->Size = System::Drawing::Size(391, 22);
            statusStrip1->TabIndex = 86;
            statusStrip1->Text = L"statusStrip1";
            // 
            // validationLabel
            // 
            this->validationLabel->AutoSize = false;
            this->validationLabel->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
            this->validationLabel->Name = L"validationLabel";
            this->validationLabel->Size = System::Drawing::Size(290, 17);
            this->validationLabel->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
            // 
            // pealIdLbl
            // 
            this->pealIdLbl->AutoSize = false;
            this->pealIdLbl->Name = L"pealIdLbl";
            this->pealIdLbl->Size = System::Drawing::Size(80, 17);
            this->pealIdLbl->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            this->pealIdLbl->Click += gcnew System::EventHandler(this, &PealUI::PealIdLabel_Click);
            // 
            // bellboardReferenceLbl
            // 
            bellboardReferenceLbl->AutoSize = true;
            bellboardReferenceLbl->Location = System::Drawing::Point(7, 18);
            bellboardReferenceLbl->Name = L"bellboardReferenceLbl";
            bellboardReferenceLbl->Size = System::Drawing::Size(57, 13);
            bellboardReferenceLbl->TabIndex = 5;
            bellboardReferenceLbl->Text = L"Reference";
            // 
            // rwGroup
            // 
            rwGroup->AutoSize = true;
            rwGroup->Controls->Add(this->openRingingWorldBtn);
            rwGroup->Controls->Add(this->rwReference);
            rwGroup->Controls->Add(label1);
            rwGroup->Dock = System::Windows::Forms::DockStyle::Fill;
            rwGroup->Location = System::Drawing::Point(3, 3);
            rwGroup->Name = L"rwGroup";
            rwGroup->Size = System::Drawing::Size(252, 55);
            rwGroup->TabIndex = 47;
            rwGroup->TabStop = false;
            rwGroup->Text = L"Ringing world";
            // 
            // openRingingWorldBtn
            // 
            this->openRingingWorldBtn->Location = System::Drawing::Point(176, 13);
            this->openRingingWorldBtn->Name = L"openRingingWorldBtn";
            this->openRingingWorldBtn->Size = System::Drawing::Size(75, 23);
            this->openRingingWorldBtn->TabIndex = 2;
            this->openRingingWorldBtn->Text = L"Open";
            this->openRingingWorldBtn->UseVisualStyleBackColor = true;
            this->openRingingWorldBtn->Click += gcnew System::EventHandler(this, &PealUI::openRingingWorldBtn_Click);
            // 
            // rwReference
            // 
            this->rwReference->Location = System::Drawing::Point(70, 15);
            this->rwReference->Name = L"rwReference";
            this->rwReference->Size = System::Drawing::Size(100, 20);
            this->rwReference->TabIndex = 1;
            this->rwReference->TextChanged += gcnew System::EventHandler(this, &PealUI::RwReference_TextChanged);
            this->rwReference->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &PealUI::NumericEditor_KeyDown);
            this->rwReference->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &PealUI::NumericEditor_KeyPressed);
            // 
            // notesGroup
            // 
            notesGroup->AutoSize = true;
            otherPageLayoutPanel->SetColumnSpan(notesGroup, 2);
            notesGroup->Controls->Add(this->notesEditor);
            notesGroup->Dock = System::Windows::Forms::DockStyle::Fill;
            notesGroup->Location = System::Drawing::Point(3, 119);
            notesGroup->Name = L"notesGroup";
            notesGroup->Size = System::Drawing::Size(365, 157);
            notesGroup->TabIndex = 7;
            notesGroup->TabStop = false;
            notesGroup->Text = L"Notes";
            // 
            // notesEditor
            // 
            this->notesEditor->AcceptsReturn = true;
            this->notesEditor->Dock = System::Windows::Forms::DockStyle::Fill;
            this->notesEditor->Location = System::Drawing::Point(3, 16);
            this->notesEditor->Margin = System::Windows::Forms::Padding(3, 3, 3, 0);
            this->notesEditor->MinimumSize = System::Drawing::Size(4, 100);
            this->notesEditor->Multiline = true;
            this->notesEditor->Name = L"notesEditor";
            this->notesEditor->Size = System::Drawing::Size(359, 138);
            this->notesEditor->TabIndex = 8;
            this->notesEditor->TextChanged += gcnew System::EventHandler(this, &PealUI::NotesEditor_TextChanged);
            // 
            // tableLayoutPanel2
            // 
            tableLayoutPanel2->ColumnCount = 1;
            tableLayoutPanel2->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel2->Controls->Add(buttonsPanel, 0, 1);
            tableLayoutPanel2->Controls->Add(this->tabControl1, 0, 0);
            tableLayoutPanel2->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel2->Location = System::Drawing::Point(0, 24);
            tableLayoutPanel2->Name = L"tableLayoutPanel2";
            tableLayoutPanel2->RowCount = 2;
            tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel2->Size = System::Drawing::Size(391, 454);
            tableLayoutPanel2->TabIndex = 87;
            // 
            // buttonsPanel
            // 
            buttonsPanel->AutoSize = true;
            buttonsPanel->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            buttonsPanel->Controls->Add(this->cancelBtn);
            buttonsPanel->Controls->Add(this->endBtn);
            buttonsPanel->Controls->Add(this->deleteBtn);
            buttonsPanel->Controls->Add(this->nextBtn);
            buttonsPanel->Controls->Add(this->forward10Btn);
            buttonsPanel->Controls->Add(this->back10Btn);
            buttonsPanel->Controls->Add(this->closeBtn);
            buttonsPanel->Controls->Add(this->previousBtn);
            buttonsPanel->Controls->Add(this->saveBtn);
            buttonsPanel->Controls->Add(this->startBtn);
            buttonsPanel->Controls->Add(this->editBtn);
            buttonsPanel->Dock = System::Windows::Forms::DockStyle::Fill;
            buttonsPanel->Location = System::Drawing::Point(3, 422);
            buttonsPanel->Name = L"buttonsPanel";
            buttonsPanel->Size = System::Drawing::Size(385, 29);
            buttonsPanel->TabIndex = 0;
            // 
            // cancelBtn
            // 
            this->cancelBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->cancelBtn->Location = System::Drawing::Point(307, 3);
            this->cancelBtn->Name = L"cancelBtn";
            this->cancelBtn->Size = System::Drawing::Size(75, 23);
            this->cancelBtn->TabIndex = 61;
            this->cancelBtn->Text = L"Cancel";
            this->cancelBtn->UseVisualStyleBackColor = true;
            this->cancelBtn->Click += gcnew System::EventHandler(this, &PealUI::CancelBtn_Click);
            // 
            // endBtn
            // 
            this->endBtn->AutoSize = true;
            this->endBtn->Location = System::Drawing::Point(171, 3);
            this->endBtn->Name = L"endBtn";
            this->endBtn->Size = System::Drawing::Size(31, 23);
            this->endBtn->TabIndex = 17;
            this->endBtn->Text = L">|";
            this->endBtn->UseVisualStyleBackColor = true;
            this->endBtn->Click += gcnew System::EventHandler(this, &PealUI::EndBtn_Click);
            // 
            // deleteBtn
            // 
            this->deleteBtn->Location = System::Drawing::Point(4, 3);
            this->deleteBtn->Name = L"deleteBtn";
            this->deleteBtn->Size = System::Drawing::Size(75, 23);
            this->deleteBtn->TabIndex = 48;
            this->deleteBtn->Text = L"Delete";
            this->deleteBtn->UseVisualStyleBackColor = true;
            this->deleteBtn->Visible = false;
            this->deleteBtn->Click += gcnew System::EventHandler(this, &PealUI::DeleteBtn_Click);
            // 
            // nextBtn
            // 
            this->nextBtn->AutoSize = true;
            this->nextBtn->Location = System::Drawing::Point(109, 3);
            this->nextBtn->Name = L"nextBtn";
            this->nextBtn->Size = System::Drawing::Size(31, 23);
            this->nextBtn->TabIndex = 15;
            this->nextBtn->Text = L">";
            this->nextBtn->UseVisualStyleBackColor = true;
            this->nextBtn->Click += gcnew System::EventHandler(this, &PealUI::NextPeal);
            // 
            // forward10Btn
            // 
            this->forward10Btn->AutoSize = true;
            this->forward10Btn->Location = System::Drawing::Point(140, 3);
            this->forward10Btn->Name = L"forward10Btn";
            this->forward10Btn->Size = System::Drawing::Size(31, 23);
            this->forward10Btn->TabIndex = 16;
            this->forward10Btn->Text = L">>";
            this->forward10Btn->UseVisualStyleBackColor = true;
            this->forward10Btn->Click += gcnew System::EventHandler(this, &PealUI::Forward10Btn_Click);
            // 
            // back10Btn
            // 
            this->back10Btn->AutoSize = true;
            this->back10Btn->Location = System::Drawing::Point(35, 3);
            this->back10Btn->Name = L"back10Btn";
            this->back10Btn->Size = System::Drawing::Size(31, 23);
            this->back10Btn->TabIndex = 13;
            this->back10Btn->Text = L"<<";
            this->back10Btn->UseVisualStyleBackColor = true;
            this->back10Btn->Click += gcnew System::EventHandler(this, &PealUI::Back10Btn_Click);
            // 
            // closeBtn
            // 
            this->closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            this->closeBtn->Location = System::Drawing::Point(307, 3);
            this->closeBtn->Name = L"closeBtn";
            this->closeBtn->Size = System::Drawing::Size(75, 23);
            this->closeBtn->TabIndex = 21;
            this->closeBtn->Text = L"Close";
            this->closeBtn->UseVisualStyleBackColor = true;
            this->closeBtn->Click += gcnew System::EventHandler(this, &PealUI::CloseBtn_Click);
            // 
            // previousBtn
            // 
            this->previousBtn->AutoSize = true;
            this->previousBtn->Location = System::Drawing::Point(66, 3);
            this->previousBtn->Name = L"previousBtn";
            this->previousBtn->Size = System::Drawing::Size(31, 23);
            this->previousBtn->TabIndex = 14;
            this->previousBtn->Text = L"<";
            this->previousBtn->UseVisualStyleBackColor = true;
            this->previousBtn->Click += gcnew System::EventHandler(this, &PealUI::PreviousPeal);
            // 
            // saveBtn
            // 
            this->saveBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->saveBtn->Location = System::Drawing::Point(232, 3);
            this->saveBtn->Name = L"saveBtn";
            this->saveBtn->Size = System::Drawing::Size(75, 23);
            this->saveBtn->TabIndex = 60;
            this->saveBtn->Text = L"Save";
            this->saveBtn->UseVisualStyleBackColor = true;
            this->saveBtn->Click += gcnew System::EventHandler(this, &PealUI::SaveBtn_Click);
            // 
            // startBtn
            // 
            this->startBtn->AutoSize = true;
            this->startBtn->Location = System::Drawing::Point(4, 3);
            this->startBtn->Name = L"startBtn";
            this->startBtn->Size = System::Drawing::Size(31, 23);
            this->startBtn->TabIndex = 13;
            this->startBtn->Text = L"|<";
            this->startBtn->UseVisualStyleBackColor = true;
            this->startBtn->Click += gcnew System::EventHandler(this, &PealUI::StartBtn_Click);
            // 
            // editBtn
            // 
            this->editBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->editBtn->Location = System::Drawing::Point(232, 3);
            this->editBtn->Name = L"editBtn";
            this->editBtn->Size = System::Drawing::Size(75, 23);
            this->editBtn->TabIndex = 19;
            this->editBtn->Text = L"Edit";
            this->editBtn->UseVisualStyleBackColor = true;
            this->editBtn->Click += gcnew System::EventHandler(this, &PealUI::EditBtn_Click);
            // 
            // tabControl1
            // 
            this->tabControl1->Controls->Add(tabPage1);
            this->tabControl1->Controls->Add(ringerTab);
            this->tabControl1->Controls->Add(this->otherPage);
            this->tabControl1->Controls->Add(this->picturePage);
            this->tabControl1->Dock = System::Windows::Forms::DockStyle::Fill;
            this->tabControl1->Location = System::Drawing::Point(3, 3);
            this->tabControl1->MinimumSize = System::Drawing::Size(385, 413);
            this->tabControl1->Name = L"tabControl1";
            this->tabControl1->SelectedIndex = 0;
            this->tabControl1->Size = System::Drawing::Size(385, 413);
            this->tabControl1->TabIndex = 84;
            // 
            // ringerTab
            // 
            ringerTab->Controls->Add(this->ringerTabLayoutPanel);
            ringerTab->Location = System::Drawing::Point(4, 22);
            ringerTab->Name = L"ringerTab";
            ringerTab->Padding = System::Windows::Forms::Padding(3);
            ringerTab->Size = System::Drawing::Size(377, 387);
            ringerTab->TabIndex = 1;
            ringerTab->Text = L"Ringers";
            ringerTab->UseVisualStyleBackColor = true;
            // 
            // ringerTabLayoutPanel
            // 
            this->ringerTabLayoutPanel->ColumnCount = 1;
            this->ringerTabLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
                100)));
            this->ringerTabLayoutPanel->Controls->Add(this->conductorGroup, 0, 3);
            this->ringerTabLayoutPanel->Controls->Add(this->createMissingRingersBtn, 0, 1);
            this->ringerTabLayoutPanel->Controls->Add(this->pealFeesBtn, 0, 2);
            this->ringerTabLayoutPanel->Dock = System::Windows::Forms::DockStyle::Fill;
            this->ringerTabLayoutPanel->Location = System::Drawing::Point(3, 3);
            this->ringerTabLayoutPanel->Name = L"ringerTabLayoutPanel";
            this->ringerTabLayoutPanel->RowCount = 4;
            this->ringerTabLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent,
                100)));
            this->ringerTabLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            this->ringerTabLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            this->ringerTabLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            this->ringerTabLayoutPanel->Size = System::Drawing::Size(371, 381);
            this->ringerTabLayoutPanel->TabIndex = 49;
            // 
            // conductorGroup
            // 
            this->conductorGroup->Controls->Add(this->singleConductor);
            this->conductorGroup->Controls->Add(this->silentConductor);
            this->conductorGroup->Controls->Add(this->jointConductor);
            this->conductorGroup->Dock = System::Windows::Forms::DockStyle::Fill;
            this->conductorGroup->Location = System::Drawing::Point(3, 329);
            this->conductorGroup->Name = L"conductorGroup";
            this->conductorGroup->Size = System::Drawing::Size(365, 49);
            this->conductorGroup->TabIndex = 47;
            this->conductorGroup->TabStop = false;
            this->conductorGroup->Text = L"Conductor";
            // 
            // singleConductor
            // 
            this->singleConductor->AutoSize = true;
            this->singleConductor->Checked = true;
            this->singleConductor->Location = System::Drawing::Point(20, 19);
            this->singleConductor->Name = L"singleConductor";
            this->singleConductor->Size = System::Drawing::Size(58, 17);
            this->singleConductor->TabIndex = 47;
            this->singleConductor->TabStop = true;
            this->singleConductor->Text = L"Normal";
            this->singleConductor->UseVisualStyleBackColor = true;
            this->singleConductor->CheckedChanged += gcnew System::EventHandler(this, &PealUI::SingleConductor_CheckedChanged);
            // 
            // silentConductor
            // 
            this->silentConductor->AutoSize = true;
            this->silentConductor->Location = System::Drawing::Point(136, 19);
            this->silentConductor->Name = L"silentConductor";
            this->silentConductor->Size = System::Drawing::Size(95, 17);
            this->silentConductor->TabIndex = 48;
            this->silentConductor->Text = L"Silent and Non";
            this->silentConductor->UseVisualStyleBackColor = true;
            this->silentConductor->CheckedChanged += gcnew System::EventHandler(this, &PealUI::SilentConductor_CheckedChanged);
            // 
            // jointConductor
            // 
            this->jointConductor->AutoSize = true;
            this->jointConductor->Location = System::Drawing::Point(289, 19);
            this->jointConductor->Name = L"jointConductor";
            this->jointConductor->Size = System::Drawing::Size(47, 17);
            this->jointConductor->TabIndex = 49;
            this->jointConductor->Text = L"Joint";
            this->jointConductor->UseVisualStyleBackColor = true;
            this->jointConductor->CheckedChanged += gcnew System::EventHandler(this, &PealUI::JointConductor_CheckedChanged);
            // 
            // createMissingRingersBtn
            // 
            this->createMissingRingersBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
            this->createMissingRingersBtn->Location = System::Drawing::Point(293, 271);
            this->createMissingRingersBtn->Name = L"createMissingRingersBtn";
            this->createMissingRingersBtn->Size = System::Drawing::Size(75, 23);
            this->createMissingRingersBtn->TabIndex = 46;
            this->createMissingRingersBtn->Text = L"Create";
            this->createMissingRingersBtn->UseVisualStyleBackColor = true;
            this->createMissingRingersBtn->Click += gcnew System::EventHandler(this, &PealUI::CreateMissingRingersBtn_Click);
            // 
            // pealFeesBtn
            // 
            this->pealFeesBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
            this->pealFeesBtn->Location = System::Drawing::Point(3, 300);
            this->pealFeesBtn->Name = L"pealFeesBtn";
            this->pealFeesBtn->Size = System::Drawing::Size(75, 23);
            this->pealFeesBtn->TabIndex = 48;
            this->pealFeesBtn->Text = L"Peal fees";
            this->pealFeesBtn->UseVisualStyleBackColor = true;
            this->pealFeesBtn->Click += gcnew System::EventHandler(this, &PealUI::PealFeesBtn_Click);
            // 
            // otherPage
            // 
            this->otherPage->Controls->Add(otherPageLayoutPanel);
            this->otherPage->Location = System::Drawing::Point(4, 22);
            this->otherPage->Name = L"otherPage";
            this->otherPage->Padding = System::Windows::Forms::Padding(3);
            this->otherPage->Size = System::Drawing::Size(377, 387);
            this->otherPage->TabIndex = 2;
            this->otherPage->Text = L"Other";
            this->otherPage->UseVisualStyleBackColor = true;
            // 
            // otherPageLayoutPanel
            // 
            otherPageLayoutPanel->ColumnCount = 2;
            otherPageLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
                100)));
            otherPageLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
                113)));
            otherPageLayoutPanel->Controls->Add(notesGroup, 0, 2);
            otherPageLayoutPanel->Controls->Add(otherSettingdPanel, 1, 0);
            otherPageLayoutPanel->Controls->Add(this->compositionGroup, 0, 3);
            otherPageLayoutPanel->Controls->Add(bellboardGroup, 0, 1);
            otherPageLayoutPanel->Controls->Add(this->seriesGroup, 0, 4);
            otherPageLayoutPanel->Controls->Add(rwGroup, 0, 0);
            otherPageLayoutPanel->Dock = System::Windows::Forms::DockStyle::Fill;
            otherPageLayoutPanel->Location = System::Drawing::Point(3, 3);
            otherPageLayoutPanel->Name = L"otherPageLayoutPanel";
            otherPageLayoutPanel->RowCount = 5;
            otherPageLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            otherPageLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            otherPageLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            otherPageLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            otherPageLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            otherPageLayoutPanel->Size = System::Drawing::Size(371, 381);
            otherPageLayoutPanel->TabIndex = 50;
            // 
            // otherSettingdPanel
            // 
            otherSettingdPanel->Controls->Add(this->simulatedSound);
            otherSettingdPanel->Controls->Add(this->notable);
            otherSettingdPanel->Controls->Add(this->withdrawnEditor);
            otherSettingdPanel->Controls->Add(this->doubleHanded);
            otherSettingdPanel->Controls->Add(this->wrongStagesSelector);
            otherSettingdPanel->Controls->Add(this->handbellSelector);
            otherSettingdPanel->Dock = System::Windows::Forms::DockStyle::Fill;
            otherSettingdPanel->Location = System::Drawing::Point(261, 3);
            otherSettingdPanel->Name = L"otherSettingdPanel";
            otherPageLayoutPanel->SetRowSpan(otherSettingdPanel, 2);
            otherSettingdPanel->Size = System::Drawing::Size(107, 110);
            otherSettingdPanel->TabIndex = 49;
            // 
            // simulatedSound
            // 
            this->simulatedSound->AutoSize = true;
            this->simulatedSound->Location = System::Drawing::Point(3, 93);
            this->simulatedSound->Name = L"simulatedSound";
            this->simulatedSound->Size = System::Drawing::Size(104, 17);
            this->simulatedSound->TabIndex = 47;
            this->simulatedSound->Text = L"Simulated sound";
            this->simulatedSound->UseVisualStyleBackColor = true;
            this->simulatedSound->CheckedChanged += gcnew System::EventHandler(this, &PealUI::SimulatedSound_CheckedChanged);
            // 
            // notable
            // 
            this->notable->AutoSize = true;
            this->notable->Location = System::Drawing::Point(3, 75);
            this->notable->Name = L"notable";
            this->notable->Size = System::Drawing::Size(63, 17);
            this->notable->TabIndex = 46;
            this->notable->Text = L"Notable";
            this->notable->UseVisualStyleBackColor = true;
            this->notable->CheckedChanged += gcnew System::EventHandler(this, &PealUI::notable_CheckedChanged);
            // 
            // withdrawnEditor
            // 
            this->withdrawnEditor->AutoSize = true;
            this->withdrawnEditor->Location = System::Drawing::Point(3, 3);
            this->withdrawnEditor->Name = L"withdrawnEditor";
            this->withdrawnEditor->Size = System::Drawing::Size(77, 17);
            this->withdrawnEditor->TabIndex = 9;
            this->withdrawnEditor->Text = L"Withdrawn";
            this->withdrawnEditor->UseVisualStyleBackColor = true;
            this->withdrawnEditor->CheckedChanged += gcnew System::EventHandler(this, &PealUI::WithdrawnEditor_CheckedChanged);
            // 
            // doubleHanded
            // 
            this->doubleHanded->AutoSize = true;
            this->doubleHanded->Location = System::Drawing::Point(3, 57);
            this->doubleHanded->Name = L"doubleHanded";
            this->doubleHanded->Size = System::Drawing::Size(99, 17);
            this->doubleHanded->TabIndex = 45;
            this->doubleHanded->Text = L"Double handed";
            this->doubleHanded->UseVisualStyleBackColor = true;
            this->doubleHanded->CheckedChanged += gcnew System::EventHandler(this, &PealUI::DoubleHanded_CheckedChanged);
            // 
            // wrongStagesSelector
            // 
            this->wrongStagesSelector->AutoSize = true;
            this->wrongStagesSelector->Location = System::Drawing::Point(3, 39);
            this->wrongStagesSelector->Name = L"wrongStagesSelector";
            this->wrongStagesSelector->Size = System::Drawing::Size(73, 17);
            this->wrongStagesSelector->TabIndex = 45;
            this->wrongStagesSelector->Text = L"Any stage";
            this->wrongStagesSelector->UseVisualStyleBackColor = true;
            this->wrongStagesSelector->CheckedChanged += gcnew System::EventHandler(this, &PealUI::WrongStagesSelector_CheckedChanged);
            // 
            // handbellSelector
            // 
            this->handbellSelector->AutoSize = true;
            this->handbellSelector->Location = System::Drawing::Point(3, 21);
            this->handbellSelector->Name = L"handbellSelector";
            this->handbellSelector->Size = System::Drawing::Size(91, 17);
            this->handbellSelector->TabIndex = 45;
            this->handbellSelector->Text = L"Handbell peal";
            this->handbellSelector->UseVisualStyleBackColor = true;
            this->handbellSelector->CheckedChanged += gcnew System::EventHandler(this, &PealUI::HandbellSelector_CheckedChanged);
            // 
            // compositionGroup
            // 
            this->compositionGroup->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            otherPageLayoutPanel->SetColumnSpan(this->compositionGroup, 2);
            this->compositionGroup->Controls->Add(this->compositionEditor);
            this->compositionGroup->Controls->Add(this->findCompositionBtn);
            this->compositionGroup->Dock = System::Windows::Forms::DockStyle::Fill;
            this->compositionGroup->Location = System::Drawing::Point(3, 282);
            this->compositionGroup->Name = L"compositionGroup";
            this->compositionGroup->Size = System::Drawing::Size(365, 45);
            this->compositionGroup->TabIndex = 7;
            this->compositionGroup->TabStop = false;
            this->compositionGroup->Text = L"Composition";
            // 
            // compositionEditor
            // 
            this->compositionEditor->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
            this->compositionEditor->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->compositionEditor->FormattingEnabled = true;
            this->compositionEditor->Location = System::Drawing::Point(5, 15);
            this->compositionEditor->Name = L"compositionEditor";
            this->compositionEditor->Size = System::Drawing::Size(324, 21);
            this->compositionEditor->TabIndex = 10;
            this->compositionEditor->SelectedIndexChanged += gcnew System::EventHandler(this, &PealUI::CompositionEditor_SelectedIndexChanged);
            this->compositionEditor->TextChanged += gcnew System::EventHandler(this, &PealUI::CompositionEditor_TextChanged);
            // 
            // findCompositionBtn
            // 
            this->findCompositionBtn->Location = System::Drawing::Point(333, 15);
            this->findCompositionBtn->Name = L"findCompositionBtn";
            this->findCompositionBtn->Size = System::Drawing::Size(27, 20);
            this->findCompositionBtn->TabIndex = 9;
            this->findCompositionBtn->Text = L">>";
            this->findCompositionBtn->UseVisualStyleBackColor = true;
            this->findCompositionBtn->Click += gcnew System::EventHandler(this, &PealUI::FindCompositionBtn_Click);
            // 
            // bellboardGroup
            // 
            bellboardGroup->Controls->Add(this->openBellboardBtn);
            bellboardGroup->Controls->Add(this->bellBoardReference);
            bellboardGroup->Controls->Add(bellboardReferenceLbl);
            bellboardGroup->Dock = System::Windows::Forms::DockStyle::Fill;
            bellboardGroup->Location = System::Drawing::Point(3, 64);
            bellboardGroup->Name = L"bellboardGroup";
            bellboardGroup->Size = System::Drawing::Size(252, 49);
            bellboardGroup->TabIndex = 48;
            bellboardGroup->TabStop = false;
            bellboardGroup->Text = L"Bellboard";
            // 
            // openBellboardBtn
            // 
            this->openBellboardBtn->Location = System::Drawing::Point(176, 13);
            this->openBellboardBtn->Name = L"openBellboardBtn";
            this->openBellboardBtn->Size = System::Drawing::Size(75, 23);
            this->openBellboardBtn->TabIndex = 6;
            this->openBellboardBtn->Text = L"Open";
            this->openBellboardBtn->UseVisualStyleBackColor = true;
            this->openBellboardBtn->Click += gcnew System::EventHandler(this, &PealUI::openBellboardBtn_Click);
            // 
            // bellBoardReference
            // 
            this->bellBoardReference->Location = System::Drawing::Point(70, 15);
            this->bellBoardReference->Name = L"bellBoardReference";
            this->bellBoardReference->Size = System::Drawing::Size(100, 20);
            this->bellBoardReference->TabIndex = 4;
            this->bellBoardReference->TextChanged += gcnew System::EventHandler(this, &PealUI::BellBoardReference_TextChanged);
            this->bellBoardReference->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &PealUI::NumericEditor_KeyDown);
            this->bellBoardReference->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &PealUI::NumericEditor_KeyPressed);
            // 
            // seriesGroup
            // 
            otherPageLayoutPanel->SetColumnSpan(this->seriesGroup, 2);
            this->seriesGroup->Controls->Add(this->seriesEditor);
            this->seriesGroup->Controls->Add(this->findSeriesBtn);
            this->seriesGroup->Dock = System::Windows::Forms::DockStyle::Fill;
            this->seriesGroup->Location = System::Drawing::Point(3, 333);
            this->seriesGroup->Name = L"seriesGroup";
            this->seriesGroup->Size = System::Drawing::Size(365, 45);
            this->seriesGroup->TabIndex = 7;
            this->seriesGroup->TabStop = false;
            this->seriesGroup->Text = L"Spliced series";
            // 
            // seriesEditor
            // 
            this->seriesEditor->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
            this->seriesEditor->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->seriesEditor->FormattingEnabled = true;
            this->seriesEditor->Location = System::Drawing::Point(5, 15);
            this->seriesEditor->Name = L"seriesEditor";
            this->seriesEditor->Size = System::Drawing::Size(324, 21);
            this->seriesEditor->TabIndex = 10;
            this->seriesEditor->SelectedIndexChanged += gcnew System::EventHandler(this, &PealUI::SeriesEditor_SelectedIndexChanged);
            this->seriesEditor->TextChanged += gcnew System::EventHandler(this, &PealUI::SeriesEditor_TextChanged);
            // 
            // findSeriesBtn
            // 
            this->findSeriesBtn->Location = System::Drawing::Point(333, 15);
            this->findSeriesBtn->Name = L"findSeriesBtn";
            this->findSeriesBtn->Size = System::Drawing::Size(27, 20);
            this->findSeriesBtn->TabIndex = 9;
            this->findSeriesBtn->Text = L">>";
            this->findSeriesBtn->UseVisualStyleBackColor = true;
            this->findSeriesBtn->Click += gcnew System::EventHandler(this, &PealUI::FindSeriesBtn_Click);
            // 
            // picturePage
            // 
            this->picturePage->Location = System::Drawing::Point(4, 22);
            this->picturePage->Name = L"picturePage";
            this->picturePage->Padding = System::Windows::Forms::Padding(3);
            this->picturePage->Size = System::Drawing::Size(377, 387);
            this->picturePage->TabIndex = 0;
            this->picturePage->Text = L"Picture";
            this->picturePage->UseVisualStyleBackColor = true;
            // 
            // PealUI
            // 
            this->AcceptButton = this->saveBtn;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            this->CancelButton = this->closeBtn;
            this->ClientSize = System::Drawing::Size(391, 500);
            this->Controls->Add(tableLayoutPanel2);
            this->Controls->Add(statusStrip1);
            this->Controls->Add(pealUIMenu);
            this->MainMenuStrip = pealUIMenu;
            this->MaximizeBox = false;
            this->MinimumSize = System::Drawing::Size(407, 539);
            this->Name = L"PealUI";
            this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Show;
            this->Load += gcnew System::EventHandler(this, &PealUI::PealUI_Load);
            tabPage1->ResumeLayout(false);
            tableLayoutPanel1->ResumeLayout(false);
            tableLayoutPanel1->PerformLayout();
            pealUIMenu->ResumeLayout(false);
            pealUIMenu->PerformLayout();
            statusStrip1->ResumeLayout(false);
            statusStrip1->PerformLayout();
            rwGroup->ResumeLayout(false);
            rwGroup->PerformLayout();
            notesGroup->ResumeLayout(false);
            notesGroup->PerformLayout();
            tableLayoutPanel2->ResumeLayout(false);
            tableLayoutPanel2->PerformLayout();
            buttonsPanel->ResumeLayout(false);
            buttonsPanel->PerformLayout();
            this->tabControl1->ResumeLayout(false);
            ringerTab->ResumeLayout(false);
            this->ringerTabLayoutPanel->ResumeLayout(false);
            this->conductorGroup->ResumeLayout(false);
            this->conductorGroup->PerformLayout();
            this->otherPage->ResumeLayout(false);
            otherPageLayoutPanel->ResumeLayout(false);
            otherPageLayoutPanel->PerformLayout();
            otherSettingdPanel->ResumeLayout(false);
            otherSettingdPanel->PerformLayout();
            this->compositionGroup->ResumeLayout(false);
            bellboardGroup->ResumeLayout(false);
            bellboardGroup->PerformLayout();
            this->seriesGroup->ResumeLayout(false);
            this->ResumeLayout(false);
            this->PerformLayout();

        }
};
}
