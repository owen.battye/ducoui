#pragma once
namespace DucoUI
{
	public ref class PrintPealsUI : public System::Windows::Forms::Form
	{
	public:
		PrintPealsUI(DucoUI::DatabaseManager^ theDatabase);

	protected:
		!PrintPealsUI();
		~PrintPealsUI();
		System::Void PrintPealsUI_Load(System::Object^  sender, System::EventArgs^  e);
		System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
		System::Void printBtn_Click(System::Object^  sender, System::EventArgs^  e);
		System::Void allCheckBox_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
		System::Void printPeals(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args);

	private:
		DucoUI::DatabaseManager^ database;
		Duco::ObjectId*         fromId;
		Duco::ObjectId*         toId;
        bool                    printSummary;
		System::ComponentModel::Container ^components;
		System::Windows::Forms::Button^  closeBtn;
		System::Windows::Forms::Button^  printBtn;
	    System::Windows::Forms::CheckBox^  allCheckBox;
	    System::Windows::Forms::NumericUpDown^  toSelector;
	    System::Windows::Forms::NumericUpDown^  fromSelector;
	    System::Windows::Forms::Label^  toLbl;
    private: System::Windows::Forms::CheckBox^ includeWithdrawn;
           System::Windows::Forms::ToolStripProgressBar^ progressBar;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
            System::Windows::Forms::GroupBox^ pealsGrpBox;
            System::Windows::Forms::Label^ fromLbl;
            System::Windows::Forms::StatusStrip^ statusStrip1;
            this->toSelector = (gcnew System::Windows::Forms::NumericUpDown());
            this->fromSelector = (gcnew System::Windows::Forms::NumericUpDown());
            this->toLbl = (gcnew System::Windows::Forms::Label());
            this->allCheckBox = (gcnew System::Windows::Forms::CheckBox());
            this->progressBar = (gcnew System::Windows::Forms::ToolStripProgressBar());
            this->closeBtn = (gcnew System::Windows::Forms::Button());
            this->printBtn = (gcnew System::Windows::Forms::Button());
            this->includeWithdrawn = (gcnew System::Windows::Forms::CheckBox());
            pealsGrpBox = (gcnew System::Windows::Forms::GroupBox());
            fromLbl = (gcnew System::Windows::Forms::Label());
            statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            pealsGrpBox->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->toSelector))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->fromSelector))->BeginInit();
            statusStrip1->SuspendLayout();
            this->SuspendLayout();
            // 
            // pealsGrpBox
            // 
            pealsGrpBox->AutoSize = true;
            pealsGrpBox->Controls->Add(this->includeWithdrawn);
            pealsGrpBox->Controls->Add(this->toSelector);
            pealsGrpBox->Controls->Add(this->fromSelector);
            pealsGrpBox->Controls->Add(this->toLbl);
            pealsGrpBox->Controls->Add(fromLbl);
            pealsGrpBox->Controls->Add(this->allCheckBox);
            pealsGrpBox->Location = System::Drawing::Point(9, 9);
            pealsGrpBox->Margin = System::Windows::Forms::Padding(3, 3, 9, 3);
            pealsGrpBox->Name = L"pealsGrpBox";
            pealsGrpBox->Size = System::Drawing::Size(220, 112);
            pealsGrpBox->TabIndex = 0;
            pealsGrpBox->TabStop = false;
            pealsGrpBox->Text = L"Peals";
            // 
            // toSelector
            // 
            this->toSelector->Location = System::Drawing::Point(141, 43);
            this->toSelector->Name = L"toSelector";
            this->toSelector->Size = System::Drawing::Size(67, 20);
            this->toSelector->TabIndex = 4;
            // 
            // fromSelector
            // 
            this->fromSelector->Location = System::Drawing::Point(42, 42);
            this->fromSelector->Name = L"fromSelector";
            this->fromSelector->Size = System::Drawing::Size(67, 20);
            this->fromSelector->TabIndex = 2;
            // 
            // toLbl
            // 
            this->toLbl->AutoSize = true;
            this->toLbl->Location = System::Drawing::Point(115, 45);
            this->toLbl->Name = L"toLbl";
            this->toLbl->Size = System::Drawing::Size(20, 13);
            this->toLbl->TabIndex = 3;
            this->toLbl->Text = L"To";
            // 
            // fromLbl
            // 
            fromLbl->AutoSize = true;
            fromLbl->Location = System::Drawing::Point(6, 45);
            fromLbl->Name = L"fromLbl";
            fromLbl->Size = System::Drawing::Size(30, 13);
            fromLbl->TabIndex = 1;
            fromLbl->Text = L"From";
            // 
            // allCheckBox
            // 
            this->allCheckBox->AutoSize = true;
            this->allCheckBox->Location = System::Drawing::Point(44, 19);
            this->allCheckBox->Name = L"allCheckBox";
            this->allCheckBox->Size = System::Drawing::Size(65, 17);
            this->allCheckBox->TabIndex = 0;
            this->allCheckBox->Text = L"All peals";
            this->allCheckBox->UseVisualStyleBackColor = true;
            this->allCheckBox->CheckedChanged += gcnew System::EventHandler(this, &PrintPealsUI::allCheckBox_CheckedChanged);
            // 
            // statusStrip1
            // 
            statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->progressBar });
            statusStrip1->Location = System::Drawing::Point(0, 157);
            statusStrip1->Name = L"statusStrip1";
            statusStrip1->Size = System::Drawing::Size(238, 22);
            statusStrip1->TabIndex = 4;
            statusStrip1->Text = L"statusStrip1";
            // 
            // progressBar
            // 
            this->progressBar->Name = L"progressBar";
            this->progressBar->Size = System::Drawing::Size(100, 16);
            // 
            // closeBtn
            // 
            this->closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            this->closeBtn->Location = System::Drawing::Point(154, 127);
            this->closeBtn->Margin = System::Windows::Forms::Padding(3, 3, 3, 31);
            this->closeBtn->Name = L"closeBtn";
            this->closeBtn->Size = System::Drawing::Size(75, 23);
            this->closeBtn->TabIndex = 2;
            this->closeBtn->Text = L"Close";
            this->closeBtn->UseVisualStyleBackColor = true;
            this->closeBtn->Click += gcnew System::EventHandler(this, &PrintPealsUI::closeBtn_Click);
            // 
            // printBtn
            // 
            this->printBtn->Location = System::Drawing::Point(69, 127);
            this->printBtn->Margin = System::Windows::Forms::Padding(3, 3, 3, 31);
            this->printBtn->Name = L"printBtn";
            this->printBtn->Size = System::Drawing::Size(75, 23);
            this->printBtn->TabIndex = 1;
            this->printBtn->Text = L"Print";
            this->printBtn->UseVisualStyleBackColor = true;
            this->printBtn->Click += gcnew System::EventHandler(this, &PrintPealsUI::printBtn_Click);
            // 
            // includeWithdrawn
            // 
            this->includeWithdrawn->AutoSize = true;
            this->includeWithdrawn->Location = System::Drawing::Point(44, 69);
            this->includeWithdrawn->Name = L"includeWithdrawn";
            this->includeWithdrawn->Size = System::Drawing::Size(112, 17);
            this->includeWithdrawn->TabIndex = 5;
            this->includeWithdrawn->Text = L"Include withdrawn";
            this->includeWithdrawn->UseVisualStyleBackColor = true;
            // 
            // PrintPealsUI
            // 
            this->AcceptButton = this->printBtn;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->AutoSize = true;
            this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            this->CancelButton = this->closeBtn;
            this->ClientSize = System::Drawing::Size(238, 179);
            this->Controls->Add(statusStrip1);
            this->Controls->Add(pealsGrpBox);
            this->Controls->Add(this->printBtn);
            this->Controls->Add(this->closeBtn);
            this->MaximizeBox = false;
            this->Name = L"PrintPealsUI";
            this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
            this->Text = L"Print peals";
            this->Load += gcnew System::EventHandler(this, &PrintPealsUI::PrintPealsUI_Load);
            pealsGrpBox->ResumeLayout(false);
            pealsGrpBox->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->toSelector))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->fromSelector))->EndInit();
            statusStrip1->ResumeLayout(false);
            statusStrip1->PerformLayout();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion
};
}
