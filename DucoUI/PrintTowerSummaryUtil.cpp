#include "PrintTowerSummaryUtil.h"
#include <RingerCirclingData.h>
#include "DucoCommon.h"
#include "DucoUtils.h"
#include "DatabaseManager.h"
#include <PealLengthInfo.h>
#include <StatisticFilters.h>
#include "DatabaseManager.h"
#include <DatabaseSettings.h>
#include <RingingDatabase.h>
#include <Ringer.h>
#include <Tower.h>
#include <Method.h>
#include <Peal.h>
#include <PealDatabase.h>
#include <DucoEngineUtils.h>

using namespace System;
using namespace System::Drawing;
using namespace Duco;
using namespace DucoUI;

const float KFontSize = 10;
const float KMarginOffset = 30;

PrintTowerSummaryUtil::PrintTowerSummaryUtil(DatabaseManager^ theDatabase, System::Drawing::Printing::PrintPageEventArgs^ printArgs, const Duco::ObjectId& newTowerId)
:	PrintUtilBase(theDatabase, KFontSize, printArgs, false)
{
    smallBoldFont = gcnew System::Drawing::Font( KDefaultPrintFont, KFontSize-2, FontStyle::Bold);
    towerId = new Duco::ObjectId(newTowerId);
}

PrintTowerSummaryUtil::~PrintTowerSummaryUtil()
{
    delete towerId;
}

System::Void
PrintTowerSummaryUtil::printObject(System::Drawing::Printing::PrintPageEventArgs^ args, System::Drawing::Printing::PrinterSettings^ printSettings)
{
    addDucoFooter(smallFont, gcnew StringFormat, args);
    Duco::StatisticFilters filters(database->Database());
    filters.SetTower(true, *towerId);

    StringFormat^ defaultStringFormat = gcnew StringFormat();
    array<Single>^ tabStops = { KMarginOffset };
    defaultStringFormat->SetTabStops(KMarginOffset, tabStops);

    String^ printBuffer = "Summary from database: ";
    printBuffer += DucoUtils::FormatFilename(database->Filename());
    args->Graphics->DrawString(printBuffer, boldFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
    yPos += lineHeight;
    printBuffer = "for tower: " + PrintTower();
    args->Graphics->DrawString(printBuffer, boldFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
    yPos += 2 * lineHeight;

    PealLengthInfo pealInfo = database->Database().PealsDatabase().PerformanceInfo(filters);
    if (pealInfo.TotalPeals() > 0)
    {
        String^ printBuffer = "There are ";
        printBuffer += DucoUtils::ConvertString(DucoEngineUtils::ToString(pealInfo.TotalPeals(), true));
        printBuffer += " " + database->PerformanceString(false) + "s ";
        if (database->Settings().DefaultRingerSet())
        {
            printBuffer += "during the last ";
        }
        else
        {
            printBuffer += "in the database, which have been rung in the ";
        }
        printBuffer += DucoUtils::ConvertString(pealInfo.DurationBetweenFirstAndLastPealString());

    }

    if (database->Settings().PealDatabase())
    {
        args->Graphics->DrawString("Record peal lengths", boldFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
    }
    else
    {
        args->Graphics->DrawString("Record quarter peal lengths", boldFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
    }

    yPos += lineHeight;
    PrintPealTitle("Longest performance by changes: ", pealInfo.LongestChangesId(), false, args);
    PrintPealTitle("Longest performance by time: ", pealInfo.LongestMinutesId(), false, args);
    PrintPealTitle("Shortest performance by changes: ", pealInfo.ShortestChangesId(), false, args);
    PrintPealTitle("Shortest performance by time: ", pealInfo.ShortestMinutesId(), false, args);
    PrintPealTitle("Slowest performance: ", pealInfo.SlowestCpmId(), true, args);
    PrintPealTitle("Fastest performance: ", pealInfo.FastestCpmId(), true, args);
    yPos += lineHeight;

    args->Graphics->DrawString("Leading ringers", boldFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
    yPos += lineHeight;
    {
        std::multimap<Duco::PealLengthInfo, ObjectId> topFiveRingers;
        database->Database().PealsDatabase().GetTopRingers(filters, 5, true, topFiveRingers);
        std::multimap<Duco::PealLengthInfo, ObjectId>::const_reverse_iterator it = topFiveRingers.rbegin();
        while (it != topFiveRingers.rend())
        {
            PrintRinger(it->second, it->first, args);
            ++it;
        }
    }
    yPos += lineHeight;

    args->Graphics->DrawString("Leading methods", boldFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
    yPos += lineHeight;
    {
        std::multimap<Duco::PealLengthInfo, Duco::ObjectId> topFiveMethods;
        database->Database().PealsDatabase().GetTopFiveMethods(filters, topFiveMethods);
        std::multimap<Duco::PealLengthInfo, Duco::ObjectId>::const_reverse_iterator it = topFiveMethods.rbegin();
        while (it != topFiveMethods.rend())
        {
            PrintMethod(it->second, it->first, args);
            ++it;
        }
    }
    yPos += lineHeight;

    if (database->Settings().PealDatabase())
    {
        args->Graphics->DrawString("Total peal lengths", boldFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
    }
    else
    {
        args->Graphics->DrawString("Total quarter peal lengths", boldFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
    }

    yPos += lineHeight;
    PrintLongString(DucoUtils::ConvertString(DucoEngineUtils::ToString(pealInfo.TotalChanges(), true)) + " changes", smallFont, float(args->MarginBounds.Left + KMarginOffset), args);
    PrintLongString(DucoUtils::PrintPealTime(pealInfo.TotalMinutes(), true), smallFont, float(args->MarginBounds.Left + KMarginOffset), args);
    yPos += lineHeight;

    std::multimap<PerformanceDate, RingerCirclingData*> circlingData;
    database->Database().PealsDatabase().GetLeadingRingersTowerCircling(*towerId, circlingData, 5, false);

    if (circlingData.size() > 0)
    {
        args->Graphics->DrawString("Ringers who have circled the tower on " + TowerHighestNumberOfBellsName() + " bells ", boldFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
        yPos += lineHeight;
        std::multimap<PerformanceDate, RingerCirclingData*>::iterator it2 = circlingData.begin();
        while (it2 != circlingData.end())
        {
            Duco::Ringer ringer;
            if (database->FindRinger(it2->second->RingerId(), ringer, false))
            {
                String^ printBuffer = "";
                printBuffer += DucoUtils::ConvertString(ringer.FullName(false));
                printBuffer += " first circled on ";
                printBuffer += DucoUtils::ConvertDate(it2->first)->ToShortDateString();
                PrintLongString(printBuffer, smallFont, float(args->MarginBounds.Left + KMarginOffset), args);

                Duco::PerformanceDate lastCircledDate;
                size_t numberOfCircles;
                if (it2->second->LastCircleDate(lastCircledDate, numberOfCircles, NumberOfBellsInTower()))
                {
                    printBuffer = "and has circled " + numberOfCircles.ToString() + " times in total, the last one was on " + DucoUtils::ConvertDate(lastCircledDate)->ToShortDateString();
                    PrintLongString(printBuffer, smallFont, float(args->MarginBounds.Left + KMarginOffset + KMarginOffset), args);
                }
            }
            ++it2;
        }
    }

    database->Database().PealsDatabase().GetLeadingRingersTowerCircling(*towerId, circlingData, 5, true);
    if (circlingData.size() > 0)
    {
        yPos += lineHeight;
        args->Graphics->DrawString("Ringers who have circled the tower to " + TowerHighestOrder(), boldFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
        yPos += lineHeight;
        std::multimap<PerformanceDate, RingerCirclingData*>::iterator it2 = circlingData.begin();
        while (it2 != circlingData.end())
        {
            Duco::Ringer ringer;
            if (database->FindRinger(it2->second->RingerId(), ringer, false))
            {
                String^ printBuffer = "";
                printBuffer += DucoUtils::ConvertString(ringer.FullName(false));
                printBuffer += " first circled on ";
                printBuffer += DucoUtils::ConvertDate(it2->first)->ToShortDateString();
                PrintLongString(printBuffer, smallFont, float(args->MarginBounds.Left + KMarginOffset), args);

                Duco::PerformanceDate lastCircledDate;
                size_t numberOfCircles;
                if (it2->second->LastCircleDate(lastCircledDate, numberOfCircles, NumberOfBellsInTower()))
                {
                    printBuffer = "and has circled " + numberOfCircles.ToString() + " times in total, the last one was on " + DucoUtils::ConvertDate(lastCircledDate)->ToShortDateString();
                    PrintLongString(printBuffer, smallFont, float(args->MarginBounds.Left + KMarginOffset + KMarginOffset), args);
                }
            }
            ++it2;
        }
    }
}

System::Void
PrintTowerSummaryUtil::PrintPealTitle(System::String^ title, const Duco::ObjectId& pealId, bool includeSpeed, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
{
    Duco::Peal peal;
    if (database->FindPeal(pealId, peal, false))
    {
        PrintLongString(title, smallBoldFont, float(printArgs->MarginBounds.Left + KMarginOffset), printArgs);

        std::wstring pealTitle;
        peal.SummaryTitle(pealTitle, database->Database());
        String^ printBuffer = "\t" + DucoUtils::ConvertString(pealTitle);
        if (includeSpeed)
        {
            printBuffer += " at ";
            printBuffer += DucoUtils::FormatFloatToString(peal.ChangesPerMinute());
            printBuffer += " changes per minute.";
        }
        else
        {
            printBuffer += ".";
        }
        PrintLongString(printBuffer, smallFont, float(printArgs->MarginBounds.Left + KMarginOffset), printArgs);
    }
}

System::Void
PrintTowerSummaryUtil::PrintRinger(const ObjectId& ringerId, const Duco::PealLengthInfo& noOfPeals, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
{
    Duco::Ringer ringer;
    if (database->FindRinger(ringerId, ringer, false))
    {
        String^ printBuffer = "";
        if (database->Settings().PealDatabase())
        {
            printBuffer += noOfPeals.TotalPeals() + " peals: ";
        }
        else
        {
            printBuffer += noOfPeals.TotalPeals() + " quarters: ";
        }
        printBuffer += DucoUtils::ConvertString(ringer.FullName(false));
        PrintLongString(printBuffer, smallFont, float(printArgs->MarginBounds.Left + KMarginOffset), printArgs);
    }
}

System::String^
PrintTowerSummaryUtil::PrintTower()
{
    String^ printBuffer = "";
    Duco::Tower tower;
    if (database->FindTower(*towerId, tower, false))
    {
        printBuffer += DucoUtils::ConvertString(tower.FullName());
    }
    return printBuffer;
}

size_t
PrintTowerSummaryUtil::NumberOfBellsInTower()
{
    Duco::Tower tower;
    if (database->FindTower(*towerId, tower, false))
    {
        return tower.Bells();
    }
    return 0;
}

System::String^
PrintTowerSummaryUtil::TowerHighestOrder()
{
    String^ orderName = "";
    Duco::Tower tower;
    if (database->FindTower(*towerId, tower, false))
    {
        database->FindOrderName(tower.Bells(), orderName);
    }
    return orderName;
}

System::String^
PrintTowerSummaryUtil::TowerHighestNumberOfBellsName()
{
    String^ orderName = "";
    Duco::Tower tower;
    if (database->FindTower(*towerId, tower, false))
    {
        std::wstring orderNameStr = DucoEngineUtils::ToName(tower.Bells());
        orderName = DucoUtils::ConvertString(orderNameStr);
    }
    return orderName;
}

System::Void
PrintTowerSummaryUtil::PrintMethod(const Duco::ObjectId& methodId, const Duco::PealLengthInfo& noOfPeals, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
{
    Duco::Method method;
    if (database->FindMethod(methodId, method, false))
    {
        String^ printBuffer = "";
        if (database->Settings().PealDatabase())
            printBuffer += noOfPeals.TotalPeals() + " peals: ";
        else
            printBuffer += noOfPeals.TotalPeals() + " quarters: ";

        printBuffer += DucoUtils::ConvertString(method.FullName(database->Database()));
        PrintLongString(printBuffer, smallFont, float(printArgs->MarginBounds.Left + KMarginOffset), printArgs);
    }
}

System::Void
PrintTowerSummaryUtil::PrintPeal(const Duco::ObjectId& pealId, System::String^ title, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
{
    Duco::Peal peal;
    if (database->FindPeal(pealId, peal, false))
    {
        Duco::Tower tower;
        if (database->FindTower(peal.TowerId(), tower, false))
        {
            String^ printBuffer = "Most " + title;
            if (database->Settings().PealDatabase())
                printBuffer += " peal:";
            else
                printBuffer += " quarter:";

            PrintLongString(printBuffer, smallBoldFont, float(printArgs->MarginBounds.Left + KMarginOffset), printArgs);
            std::wstring pealTitle;
            peal.SummaryTitle(pealTitle, database->Database());
            printBuffer = DucoUtils::ConvertString(pealTitle);
            PrintLongString(printBuffer, smallFont, float(printArgs->MarginBounds.Left + KMarginOffset), printArgs);
        }
    }
}
