#include "RingingDatabaseObserver.h"
#include "DatabaseManager.h"

#include "CompletedSeriesUI.h"

#include "MethodSeries.h"
#include "SystemDefaultIcon.h"
#include "DucoUtils.h"
#include <RingingDatabase.h>
#include <MethodSeries.h>
#include <MethodSeriesDatabase.h>
#include <PealDatabase.h>
#include "DucoTableSorter.h"
#include "DucoWindowState.h"
#include "MethodSeriesUI.h"
#include "SoundUtils.h"
#include <DucoEngineUtils.h>

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Windows::Forms;

CompletedSeriesUI::CompletedSeriesUI(DucoUI::DatabaseManager^ theDatabase)
:   database(theDatabase), restartGenerator(false)
{
    InitializeComponent();
}

CompletedSeriesUI::!CompletedSeriesUI()
{
    database->RemoveObserver(this);
}

CompletedSeriesUI::~CompletedSeriesUI()
{
    this->!CompletedSeriesUI();
    if (components)
    {
        delete components;
    }
}

System::Void
CompletedSeriesUI::CompletedSeriesUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    StartGenerator();
    database->AddObserver(this);
}

void
CompletedSeriesUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    switch (type)
    {
        case TObjectType::EPeal:
        case TObjectType::EMethodSeries:
            StartGenerator();
            break;
    default:
        break;
    }
}

System::Void
CompletedSeriesUI::backgroundWorker_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
	std::set<Duco::ObjectId> allIds;
	database->Database().MethodSeriesDatabase().GetAllObjectIds(allIds);

	const double noOfRows = double(allIds.size());
	double count = 0;
	std::set<Duco::ObjectId>::const_iterator it = allIds.begin();
	while (it != allIds.end())
	{
		Duco::ObjectId seriesId = *it;
		Duco::MethodSeries nextMethSeries;
		if (database->FindMethodSeries(seriesId, nextMethSeries, false))
		{
			PerformanceDate completedDate;
			size_t remainingMethods;
            Duco::PealLengthInfo pealCount;
			bool completed = nextMethSeries.SeriesCompletedDate(database->Database(), completedDate, remainingMethods, pealCount);

			DataGridViewRow^ newRow = gcnew DataGridViewRow();
			// Id
			DataGridViewTextBoxCell^ idCell = gcnew DataGridViewTextBoxCell();
			idCell->Value = DucoUtils::ConvertString(seriesId);
			newRow->Cells->Add(idCell);
			// Series name
			DataGridViewTextBoxCell^ nameCell = gcnew DataGridViewTextBoxCell();
			nameCell->Value = DucoUtils::ConvertString(nextMethSeries.Name());
			nameCell->ToolTipText = "Series name";
			newRow->Cells->Add(nameCell);
			// No of methods
			DataGridViewTextBoxCell^ noofMethodsCell = gcnew DataGridViewTextBoxCell();
			noofMethodsCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(nextMethSeries.NoOfMethods()));
			noofMethodsCell->ToolTipText = "The number of methods in the series";
			newRow->Cells->Add(noofMethodsCell);

            // First peal & Peal count
            DataGridViewTextBoxCell^ firstPealCell = gcnew DataGridViewTextBoxCell();
            firstPealCell->ToolTipText = "The first time a peal in the series was rung";
            DataGridViewTextBoxCell^ lastPealCell = gcnew DataGridViewTextBoxCell();
            lastPealCell->ToolTipText = "The last time a peal in the series was rung";
            DataGridViewTextBoxCell^ pealCountCell = gcnew DataGridViewTextBoxCell();
            pealCountCell->ToolTipText = "The number of times the methods in series has been rung.";
            DataGridViewTextBoxCell^ remainingCell = gcnew DataGridViewTextBoxCell();
            remainingCell->ToolTipText = "The number of unrung methods in the series";
            DataGridViewTextBoxCell^ completedCell = gcnew DataGridViewTextBoxCell();
            completedCell->ToolTipText = "The first date all methods in the series have been rung in individual peals";
            DataGridViewTextBoxCell^ daysTakenCell = gcnew DataGridViewTextBoxCell();

            if (pealCount.TotalPeals() > 0)
            {
                firstPealCell->Value = DucoUtils::ConvertString(pealCount.DateOfFirstPeal(), false);
                if (pealCount.TotalPeals() > 1)
                {
                    lastPealCell->Value = DucoUtils::ConvertString(pealCount.DateOfLastPeal(), false);
                }
                if (completed)
                {
                    completedCell->Value = DucoUtils::ConvertString(completedDate, false);
                    daysTakenCell->Value = DucoUtils::ConvertString(pealCount.DurationBetweenFirstAndLastPealString());
                }
                else
                {
                    remainingCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(remainingMethods));
                }
            }
            else
            {
                remainingCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(remainingMethods));
            }
            pealCountCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(pealCount.TotalPeals(), true));

            newRow->Cells->Add(pealCountCell);
            newRow->Cells->Add(firstPealCell);
            newRow->Cells->Add(lastPealCell);
            newRow->Cells->Add(remainingCell);
            newRow->Cells->Add(completedCell);
            newRow->Cells->Add(daysTakenCell);

			backgroundWorker->ReportProgress(int((++count / noOfRows)*(double)100), newRow);
		}

		++it;
	}
}

System::Void
CompletedSeriesUI::backgroundWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    progressBar->Value = e->ProgressPercentage;
    dataGridView->Rows->Add((DataGridViewRow^)e->UserState);
}

System::Void
CompletedSeriesUI::backgroundWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    if (restartGenerator)
    {
        restartGenerator = false;
        StartGenerator();
    }
    else
    {
        dataGridView->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::AllCells;

        DucoTableSorter^ sorter = gcnew DucoTableSorter(false, EString);
        sorter->AddSortObject(ECount, true, 2);
        sorter->AddSortObject(EString, true, 1);
        dataGridView->Sort(sorter);

        NoOfMethods->HeaderCell->SortGlyphDirection = SortOrder::Ascending;
        progressBar->Value = 0;
    }
}

System::Void
CompletedSeriesUI::StartGenerator()
{
    if (backgroundWorker->IsBusy)
    {
        restartGenerator = true;
        return;
    }
    dataGridView->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::None;
    dataGridView->Rows->Clear();
    backgroundWorker->RunWorkerAsync(this);
}

System::Void
CompletedSeriesUI::dataGridView_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    try
    {
        System::Windows::Forms::DataGridView^ theDataView = static_cast<System::Windows::Forms::DataGridView^>(sender);
        unsigned int rowIndex = e->RowIndex;
        DataGridViewRow^ currentRow = theDataView->Rows[rowIndex];
        unsigned int seriesId = Convert::ToInt16(currentRow->Cells[0]->Value);
        MethodSeriesUI::ShowSeries(database, this->MdiParent, seriesId);
    }
    catch (Exception^)
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
CompletedSeriesUI::dataGridView_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
	DataGridViewColumn^ newColumn = dataGridView->Columns[e->ColumnIndex];
	SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;
	ResetProgrammicSortGlyphs();
	if (newColumn->SortMode != DataGridViewColumnSortMode::Programmatic)
	{
		return;
	}
	ResetAllSortGlyphs();

	if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
		newSortOrder = SortOrder::Ascending;
	else
		newSortOrder = SortOrder::Descending;

	newColumn->HeaderCell->SortGlyphDirection = newSortOrder;
	DucoTableSorter^ sorter = gcnew DucoTableSorter(newSortOrder == SortOrder::Ascending, EString);

	switch (e->ColumnIndex)
	{
	case 3:
	case 5:
		{
		sorter->AddSortObject(EDate, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
		sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, 6);
		sorter->AddSortObject(EString, newSortOrder == SortOrder::Ascending, 1);
		}
		break;
	case 2:
	case 4:
	case 6:
		{
		sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
		sorter->AddSortObject(EString, newSortOrder == SortOrder::Ascending, 1);
		}
		break;
	default:
		break;
	}
	dataGridView->Sort(sorter);
}

System::Void
CompletedSeriesUI::ResetAllSortGlyphs()
{
    Seriesname->HeaderCell->SortGlyphDirection = SortOrder::None;
    ResetProgrammicSortGlyphs();
}

System::Void
CompletedSeriesUI::ResetProgrammicSortGlyphs()
{
    CompletedDate->HeaderCell->SortGlyphDirection = SortOrder::None;
    NoOfMethods->HeaderCell->SortGlyphDirection = SortOrder::None;
    Remaining->HeaderCell->SortGlyphDirection = SortOrder::None;
    FirstPeal->HeaderCell->SortGlyphDirection = SortOrder::None;
    PealCount->HeaderCell->SortGlyphDirection = SortOrder::None;
}
