#pragma once
#include "PrintUtilBase.h"

namespace Duco
{
    class ObjectId;
}

namespace DucoUI
{
    ref class DatabaseManager;

public ref class PrintSeriesCirclingUtil : public PrintUtilBase
{
public:
    PrintSeriesCirclingUtil(DucoUI::DatabaseManager^ theDatabase, System::Drawing::Printing::PrintPageEventArgs^ args);
    !PrintSeriesCirclingUtil();
    ~PrintSeriesCirclingUtil();
    void SetObjects(System::Windows::Forms::DataGridView^ theDataGridView, const Duco::ObjectId& newSeriesId);
    virtual System::Void printObject(System::Drawing::Printing::PrintPageEventArgs^ args, System::Drawing::Printing::PrinterSettings^ printSettings) override;

protected:
    Duco::ObjectId*                         seriesId;
    System::Windows::Forms::DataGridView^   dataGridView;
};
}