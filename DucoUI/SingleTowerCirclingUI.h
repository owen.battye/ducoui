#pragma once
namespace DucoUI
{
    public ref class SingleTowerCirclingUI : public System::Windows::Forms::Form, public DucoUI::RingingDatabaseObserver
    {
    public:
        SingleTowerCirclingUI(DucoUI::DatabaseManager^ theDatabase, const Duco::ObjectId& theTowerId, System::Boolean includeLinked);

        //from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

    protected:
        ~SingleTowerCirclingUI();
        !SingleTowerCirclingUI();
        System::Void InitializeComponent();

        System::Void SingleTowerCirclingUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void StartGenerator();
        System::Void backgroundWorker1_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void backgroundWorker1_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);
        System::Void backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);

        System::Void CheckEnoughColumnsExist(const Duco::ObjectId& ringId);
        System::Void ResetAllSortGlyphs();

        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void printBtn_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void dataGridView_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);
        System::Void circledTowerData_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);
        System::Void stageSelector_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void ringSelector_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void methodSelector_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);

        System::Void conducted_CheckedChanged(System::Object^  sender, System::EventArgs^  e);

        System::Void printCircling(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args);
        System::String^ DaysToString(System::UInt64 days);

    private:
        System::ComponentModel::Container^  components;
        DucoUI::DatabaseManager^            database;
        System::Boolean                     restartGenerator;
        const Duco::ObjectId*               towerId;
        System::Boolean                     includeLinkedTowers;
        GenericTablePrintUtil^              printingUtil;
        System::Boolean                     populatingView;

        System::Windows::Forms::DataGridView^               dataGridView;
        System::Windows::Forms::ComboBox^                   stageSelector;
        System::Windows::Forms::ComboBox^                   ringSelector;
        System::Windows::Forms::CheckBox^                   conducted;

        System::Windows::Forms::ToolStripProgressBar^       progressBar;
        System::Windows::Forms::CheckBox^                   printLandscape;
        System::ComponentModel::BackgroundWorker^           backgroundWorker1;
        System::Windows::Forms::Panel^                      settingsPanel;
        System::Windows::Forms::ComboBox^                   methodSelector;
        System::Windows::Forms::Panel^                      buttonPanel;
        System::Collections::Generic::List<System::Int16>^  allRingIds;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ ringerId;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ ringerName;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ circledCount;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ pealCount;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ circledDate;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ numberOfDaysColumn;
           System::Collections::Generic::List<System::Int16>^ allMethodIds;
    };

    ref class SingleTowerCirclingUIArgs
    {
    public:
        Duco::ObjectId* towerId;
        System::Boolean includeLinked;
        unsigned int    orderNumber;
        Duco::ObjectId* ringId;
        Duco::ObjectId* methodId;
        System::Boolean conducted;
    };
}
