#pragma once
namespace DucoUI
{

    public ref class RingerUI : public System::Windows::Forms::Form, public DucoUI::RingingDatabaseObserver
    {
    public:
        RingerUI(DucoUI::DatabaseManager^ theDatabase, DucoUI::DucoWindowState state);
        static void ShowRinger(DucoUI::DatabaseManager^ theDatabase, System::Windows::Forms::Form^ parent, const Duco::ObjectId& ringerId);

        //RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

    protected:
        RingerUI(DucoUI::DatabaseManager^ newDatabase, const Duco::ObjectId& ringerId);
        ~RingerUI();
        !RingerUI();

        void InitializeComponent();
        System::Void RingerUI_Load(System::Object^ sender, System::EventArgs^ e);
        System::Void StartBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void Back10Btn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void PreviousBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void NextBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void Forward10Btn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void EndBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void RingerIdLabel_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void StatsBtn_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void AddAkaBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void DeleteAkaBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void PealbaseBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void EditBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void CancelBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void SaveBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void CloseBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void ActiveNameChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void RingerDataChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void NotesEditor_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void AkaEditor_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void PealbaseRefEditor_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void nonHuman_CheckedChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void bellsBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void linkedRingers_SelectedValueChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void similarBtn_Click(System::Object^ sender, System::EventArgs^ e);

        System::Void Male_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void Female_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void deceased_CheckedChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void deceasedDate_ValueChanged(System::Object ^ sender, System::EventArgs ^ e);

        System::Void ClosingEvent(System::Object^ object, System::ComponentModel::CancelEventArgs^ args);

        System::Void SetViewState();
        System::Void SetEditState();
        System::Void SetNewState();

        System::Void PopulateView();
        System::Void PopulateViewWithId(const Duco::ObjectId& ringerId);
        System::Void SetActiveName(unsigned int nameNumber);
        System::Void UpdateIdLabel();

    private:
        DucoUI::DatabaseManager^      database;
        System::ComponentModel::Container^  components; 
        System::Windows::Forms::TextBox^    forenameEditor;
        System::Windows::Forms::TextBox^    surnameEditor;
        System::Windows::Forms::TextBox^    pealbaseRefEditor;
        System::Windows::Forms::TextBox^    notesEditor;
        System::Windows::Forms::TextBox^    akaEditor;
        System::Windows::Forms::RadioButton^  male;
        System::Windows::Forms::RadioButton^  female;
        System::Windows::Forms::DateTimePicker^  akaChangedDate;
        System::Windows::Forms::TrackBar^   ringerNameChanges;
        System::Windows::Forms::ToolStripStatusLabel^  ringerIdLabel;
        System::Windows::Forms::Button^     startBtn;
        System::Windows::Forms::Button^     back10Btn;
        System::Windows::Forms::Button^     previousBtn;
        System::Windows::Forms::Button^     nextBtn;
        System::Windows::Forms::Button^     forward10Btn;
        System::Windows::Forms::Button^     endBtn;
        System::Windows::Forms::Button^     editBtn;
        System::Windows::Forms::Button^     closeBtn;
        System::Windows::Forms::Button^     saveBtn;
        System::Windows::Forms::Button^     cancelBtn;
        System::Windows::Forms::Button^     addAkaBtn;
        System::Windows::Forms::Button^     deleteAkaBtn;

        System::Windows::Forms::Button^     pealbaseBtn;
        DucoWindowState                     windowState;
        System::Boolean                     dataChanged;
        System::Boolean                     changingName;
    private: System::Windows::Forms::TabPage^  namePage;
    private: System::Windows::Forms::Label^  fromDateLbl;
    private: System::Windows::Forms::TabPage^  referencesPage;
    private: System::Windows::Forms::TabPage^  otherPage;
    private: System::Windows::Forms::TabPage^  notesPage;
    private: System::Windows::Forms::MenuStrip^  menuStrip1;
    private: System::Windows::Forms::ToolStripMenuItem^  statsBtn;
    private: System::Windows::Forms::ToolStripMenuItem^  bellsBtn;
    private: System::Windows::Forms::CheckBox^ nonHuman;
    private: System::Windows::Forms::TabPage^ linkedRingersPage;
    private: System::Windows::Forms::CheckedListBox^ linkedRingers;

    private: System::Windows::Forms::Button^ similarBtn;

           Duco::Ringer*                           currentRinger;

    private: System::Windows::Forms::DateTimePicker^ deceasedDate;
    private: System::Windows::Forms::CheckBox^ deceased;
           std::vector<Duco::ObjectId>* currentRingerLinkedIds;
    };
}
