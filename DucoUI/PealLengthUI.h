#pragma once

namespace DucoUI
{
    public ref class PealLengthUI : public System::Windows::Forms::Form, public DucoUI::RingingDatabaseObserver
    {
    public:
        PealLengthUI(DucoUI::DatabaseManager^ theDatabase);

        // from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

    protected:
        !PealLengthUI();
        ~PealLengthUI();

        System::Void PealLengthUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void filtersBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void GenerateGeneral();

        System::Void longestTime_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
        System::Void longestChanges_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
        System::Void longestCPM_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
        System::Void shortestTime_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
        System::Void shortestChanges_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
        System::Void shortestCPM_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
        System::Void averageTime_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
        System::Void averageChanges_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
        System::Void averageCPM_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);

    private:
        Duco::PealLengthInfo*               pealInfo;
        Duco::StatisticFilters*             filters;
        System::ComponentModel::Container^  components;
        DucoUI::DatabaseManager^      database;
        System::Windows::Forms::TextBox^  longestTime;
        System::Windows::Forms::TextBox^  shortestTime;
        System::Windows::Forms::TextBox^  averageTime;
        System::Windows::Forms::TextBox^  totalTime;
        System::Windows::Forms::TextBox^  longestChanges;
        System::Windows::Forms::TextBox^  shortestChanges;
        System::Windows::Forms::TextBox^  averageChanges;
        System::Windows::Forms::TextBox^  totalChanges;
        System::Windows::Forms::TextBox^  longestCPM;
        System::Windows::Forms::TextBox^  shortestCPM;
        System::Windows::Forms::TextBox^  averageCPM;

        void InitializeComponent()
        {
            System::Windows::Forms::Label^  timeLbl;
            System::Windows::Forms::Label^  changesLbl;
            System::Windows::Forms::Label^  longestLbl;
            System::Windows::Forms::Label^  shortestLbl;
            System::Windows::Forms::Label^  averageLbl;
            System::Windows::Forms::Label^  totalLbl;
            System::Windows::Forms::Label^  changesPerMinuteLbl;
            System::Windows::Forms::ToolStrip^  toolStrip1;
            System::Windows::Forms::ToolStripButton^  filtersBtn;
            System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(PealLengthUI::typeid));
            System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
            this->longestTime = (gcnew System::Windows::Forms::TextBox());
            this->totalChanges = (gcnew System::Windows::Forms::TextBox());
            this->averageCPM = (gcnew System::Windows::Forms::TextBox());
            this->totalTime = (gcnew System::Windows::Forms::TextBox());
            this->shortestCPM = (gcnew System::Windows::Forms::TextBox());
            this->averageChanges = (gcnew System::Windows::Forms::TextBox());
            this->longestChanges = (gcnew System::Windows::Forms::TextBox());
            this->longestCPM = (gcnew System::Windows::Forms::TextBox());
            this->averageTime = (gcnew System::Windows::Forms::TextBox());
            this->shortestChanges = (gcnew System::Windows::Forms::TextBox());
            this->shortestTime = (gcnew System::Windows::Forms::TextBox());
            timeLbl = (gcnew System::Windows::Forms::Label());
            changesLbl = (gcnew System::Windows::Forms::Label());
            longestLbl = (gcnew System::Windows::Forms::Label());
            shortestLbl = (gcnew System::Windows::Forms::Label());
            averageLbl = (gcnew System::Windows::Forms::Label());
            totalLbl = (gcnew System::Windows::Forms::Label());
            changesPerMinuteLbl = (gcnew System::Windows::Forms::Label());
            toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
            filtersBtn = (gcnew System::Windows::Forms::ToolStripButton());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            toolStrip1->SuspendLayout();
            tableLayoutPanel1->SuspendLayout();
            this->SuspendLayout();
            // 
            // timeLbl
            // 
            timeLbl->AutoSize = true;
            timeLbl->Location = System::Drawing::Point(56, 3);
            timeLbl->Margin = System::Windows::Forms::Padding(3);
            timeLbl->Name = L"timeLbl";
            timeLbl->Size = System::Drawing::Size(30, 13);
            timeLbl->TabIndex = 1;
            timeLbl->Text = L"Time";
            // 
            // changesLbl
            // 
            changesLbl->AutoSize = true;
            changesLbl->Location = System::Drawing::Point(162, 3);
            changesLbl->Margin = System::Windows::Forms::Padding(3);
            changesLbl->Name = L"changesLbl";
            changesLbl->Size = System::Drawing::Size(49, 13);
            changesLbl->TabIndex = 2;
            changesLbl->Text = L"Changes";
            // 
            // longestLbl
            // 
            longestLbl->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            longestLbl->AutoSize = true;
            longestLbl->Location = System::Drawing::Point(5, 25);
            longestLbl->Margin = System::Windows::Forms::Padding(3, 6, 3, 3);
            longestLbl->Name = L"longestLbl";
            longestLbl->Size = System::Drawing::Size(45, 13);
            longestLbl->TabIndex = 3;
            longestLbl->Text = L"Longest";
            // 
            // shortestLbl
            // 
            shortestLbl->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            shortestLbl->AutoSize = true;
            shortestLbl->Location = System::Drawing::Point(4, 52);
            shortestLbl->Margin = System::Windows::Forms::Padding(3, 7, 3, 3);
            shortestLbl->Name = L"shortestLbl";
            shortestLbl->Size = System::Drawing::Size(46, 13);
            shortestLbl->TabIndex = 4;
            shortestLbl->Text = L"Shortest";
            // 
            // averageLbl
            // 
            averageLbl->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            averageLbl->AutoSize = true;
            averageLbl->Location = System::Drawing::Point(3, 79);
            averageLbl->Margin = System::Windows::Forms::Padding(3, 8, 3, 3);
            averageLbl->Name = L"averageLbl";
            averageLbl->Size = System::Drawing::Size(47, 13);
            averageLbl->TabIndex = 5;
            averageLbl->Text = L"Average";
            // 
            // totalLbl
            // 
            totalLbl->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            totalLbl->AutoSize = true;
            totalLbl->Location = System::Drawing::Point(19, 106);
            totalLbl->Margin = System::Windows::Forms::Padding(3, 9, 3, 3);
            totalLbl->Name = L"totalLbl";
            totalLbl->Size = System::Drawing::Size(31, 13);
            totalLbl->TabIndex = 6;
            totalLbl->Text = L"Total";
            // 
            // changesPerMinuteLbl
            // 
            changesPerMinuteLbl->AutoSize = true;
            changesPerMinuteLbl->Location = System::Drawing::Point(268, 3);
            changesPerMinuteLbl->Margin = System::Windows::Forms::Padding(3);
            changesPerMinuteLbl->Name = L"changesPerMinuteLbl";
            changesPerMinuteLbl->Size = System::Drawing::Size(101, 13);
            changesPerMinuteLbl->TabIndex = 13;
            changesPerMinuteLbl->Text = L"Changes per minute";
            // 
            // toolStrip1
            // 
            toolStrip1->GripStyle = System::Windows::Forms::ToolStripGripStyle::Hidden;
            toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { filtersBtn });
            toolStrip1->Location = System::Drawing::Point(3, 3);
            toolStrip1->Name = L"toolStrip1";
            toolStrip1->Padding = System::Windows::Forms::Padding(0);
            toolStrip1->Size = System::Drawing::Size(378, 25);
            toolStrip1->TabIndex = 0;
            toolStrip1->Text = L"toolStrip1";
            // 
            // filtersBtn
            // 
            filtersBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
            filtersBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"filtersBtn.Image")));
            filtersBtn->ImageTransparentColor = System::Drawing::Color::Magenta;
            filtersBtn->Name = L"filtersBtn";
            filtersBtn->Size = System::Drawing::Size(42, 22);
            filtersBtn->Text = L"&Filters";
            filtersBtn->Click += gcnew System::EventHandler(this, &PealLengthUI::filtersBtn_Click);
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 4;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->Controls->Add(this->longestTime, 1, 1);
            tableLayoutPanel1->Controls->Add(this->totalChanges, 2, 4);
            tableLayoutPanel1->Controls->Add(this->averageCPM, 3, 3);
            tableLayoutPanel1->Controls->Add(this->totalTime, 1, 4);
            tableLayoutPanel1->Controls->Add(this->shortestCPM, 3, 2);
            tableLayoutPanel1->Controls->Add(this->averageChanges, 2, 3);
            tableLayoutPanel1->Controls->Add(this->longestChanges, 2, 1);
            tableLayoutPanel1->Controls->Add(this->longestCPM, 3, 1);
            tableLayoutPanel1->Controls->Add(this->averageTime, 1, 3);
            tableLayoutPanel1->Controls->Add(shortestLbl, 0, 2);
            tableLayoutPanel1->Controls->Add(this->shortestChanges, 2, 2);
            tableLayoutPanel1->Controls->Add(totalLbl, 0, 4);
            tableLayoutPanel1->Controls->Add(timeLbl, 1, 0);
            tableLayoutPanel1->Controls->Add(averageLbl, 0, 3);
            tableLayoutPanel1->Controls->Add(changesPerMinuteLbl, 3, 0);
            tableLayoutPanel1->Controls->Add(changesLbl, 2, 0);
            tableLayoutPanel1->Controls->Add(this->shortestTime, 1, 2);
            tableLayoutPanel1->Controls->Add(longestLbl, 0, 1);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(3, 28);
            tableLayoutPanel1->Margin = System::Windows::Forms::Padding(6);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 5;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
            tableLayoutPanel1->Size = System::Drawing::Size(378, 124);
            tableLayoutPanel1->TabIndex = 20;
            // 
            // longestTime
            // 
            this->longestTime->Dock = System::Windows::Forms::DockStyle::Fill;
            this->longestTime->Location = System::Drawing::Point(56, 22);
            this->longestTime->Name = L"longestTime";
            this->longestTime->ReadOnly = true;
            this->longestTime->Size = System::Drawing::Size(100, 20);
            this->longestTime->TabIndex = 8;
            this->longestTime->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
            this->longestTime->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &PealLengthUI::longestTime_MouseDown);
            // 
            // totalChanges
            // 
            this->totalChanges->Dock = System::Windows::Forms::DockStyle::Fill;
            this->totalChanges->Location = System::Drawing::Point(162, 100);
            this->totalChanges->Name = L"totalChanges";
            this->totalChanges->ReadOnly = true;
            this->totalChanges->Size = System::Drawing::Size(100, 20);
            this->totalChanges->TabIndex = 16;
            this->totalChanges->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
            // 
            // averageCPM
            // 
            this->averageCPM->Dock = System::Windows::Forms::DockStyle::Fill;
            this->averageCPM->Location = System::Drawing::Point(268, 74);
            this->averageCPM->Name = L"averageCPM";
            this->averageCPM->ReadOnly = true;
            this->averageCPM->Size = System::Drawing::Size(107, 20);
            this->averageCPM->TabIndex = 19;
            this->averageCPM->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
            this->averageCPM->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &PealLengthUI::averageCPM_MouseDown);
            // 
            // totalTime
            // 
            this->totalTime->Dock = System::Windows::Forms::DockStyle::Fill;
            this->totalTime->Location = System::Drawing::Point(56, 100);
            this->totalTime->Name = L"totalTime";
            this->totalTime->ReadOnly = true;
            this->totalTime->Size = System::Drawing::Size(100, 20);
            this->totalTime->TabIndex = 11;
            this->totalTime->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
            // 
            // shortestCPM
            // 
            this->shortestCPM->Dock = System::Windows::Forms::DockStyle::Fill;
            this->shortestCPM->Location = System::Drawing::Point(268, 48);
            this->shortestCPM->Name = L"shortestCPM";
            this->shortestCPM->ReadOnly = true;
            this->shortestCPM->Size = System::Drawing::Size(107, 20);
            this->shortestCPM->TabIndex = 18;
            this->shortestCPM->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
            this->shortestCPM->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &PealLengthUI::shortestCPM_MouseDown);
            // 
            // averageChanges
            // 
            this->averageChanges->Dock = System::Windows::Forms::DockStyle::Fill;
            this->averageChanges->Location = System::Drawing::Point(162, 74);
            this->averageChanges->Name = L"averageChanges";
            this->averageChanges->ReadOnly = true;
            this->averageChanges->Size = System::Drawing::Size(100, 20);
            this->averageChanges->TabIndex = 15;
            this->averageChanges->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
            this->averageChanges->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &PealLengthUI::averageChanges_MouseDown);
            // 
            // longestChanges
            // 
            this->longestChanges->Dock = System::Windows::Forms::DockStyle::Fill;
            this->longestChanges->Location = System::Drawing::Point(162, 22);
            this->longestChanges->Name = L"longestChanges";
            this->longestChanges->ReadOnly = true;
            this->longestChanges->Size = System::Drawing::Size(100, 20);
            this->longestChanges->TabIndex = 12;
            this->longestChanges->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
            this->longestChanges->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &PealLengthUI::longestChanges_MouseDown);
            // 
            // longestCPM
            // 
            this->longestCPM->Dock = System::Windows::Forms::DockStyle::Fill;
            this->longestCPM->Location = System::Drawing::Point(268, 22);
            this->longestCPM->Name = L"longestCPM";
            this->longestCPM->ReadOnly = true;
            this->longestCPM->Size = System::Drawing::Size(107, 20);
            this->longestCPM->TabIndex = 17;
            this->longestCPM->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
            this->longestCPM->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &PealLengthUI::longestCPM_MouseDown);
            // 
            // averageTime
            // 
            this->averageTime->Dock = System::Windows::Forms::DockStyle::Fill;
            this->averageTime->Location = System::Drawing::Point(56, 74);
            this->averageTime->Name = L"averageTime";
            this->averageTime->ReadOnly = true;
            this->averageTime->Size = System::Drawing::Size(100, 20);
            this->averageTime->TabIndex = 10;
            this->averageTime->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
            this->averageTime->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &PealLengthUI::averageTime_MouseDown);
            // 
            // shortestChanges
            // 
            this->shortestChanges->Dock = System::Windows::Forms::DockStyle::Fill;
            this->shortestChanges->Location = System::Drawing::Point(162, 48);
            this->shortestChanges->Name = L"shortestChanges";
            this->shortestChanges->ReadOnly = true;
            this->shortestChanges->Size = System::Drawing::Size(100, 20);
            this->shortestChanges->TabIndex = 14;
            this->shortestChanges->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
            this->shortestChanges->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &PealLengthUI::shortestChanges_MouseDown);
            // 
            // shortestTime
            // 
            this->shortestTime->Dock = System::Windows::Forms::DockStyle::Fill;
            this->shortestTime->Location = System::Drawing::Point(56, 48);
            this->shortestTime->Name = L"shortestTime";
            this->shortestTime->ReadOnly = true;
            this->shortestTime->Size = System::Drawing::Size(100, 20);
            this->shortestTime->TabIndex = 9;
            this->shortestTime->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
            this->shortestTime->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &PealLengthUI::shortestTime_MouseDown);
            // 
            // PealLengthUI
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(384, 155);
            this->Controls->Add(tableLayoutPanel1);
            this->Controls->Add(toolStrip1);
            this->MaximumSize = System::Drawing::Size(400, 283);
            this->MinimumSize = System::Drawing::Size(400, 150);
            this->Name = L"PealLengthUI";
            this->Padding = System::Windows::Forms::Padding(3);
            this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
            this->Text = L"Performance lengths";
            this->Load += gcnew System::EventHandler(this, &PealLengthUI::PealLengthUI_Load);
            toolStrip1->ResumeLayout(false);
            toolStrip1->PerformLayout();
            tableLayoutPanel1->ResumeLayout(false);
            tableLayoutPanel1->PerformLayout();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
    };
}
