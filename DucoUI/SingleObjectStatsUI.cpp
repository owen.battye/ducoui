#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"
#include <PealLengthInfo.h>
#include "ProgressWrapper.h"
#include <DatabaseSearch.h>
#include "SearchUtils.h"
#include "PrintPealSearchUtil.h"

#include "SingleObjectStatsUI.h"

#include "PrintBandSummaryUtil.h"
#include "BandStats.h"
#include "DucoUtils.h"
#include <DatabaseSettings.h>
#include <Tower.h>
#include <Ringer.h>
#include "SystemDefaultIcon.h"
#include "DucoUIUtils.h"
#include "DucoWindowState.h"
#include "OpenObjectCallback.h"
#include "PictureListControl.h"
#include "PictureControl.h"
#include "RingerList.h"
#include <DownloadedPeal.h>
#include "PealUI.h"
#include <RingingDatabase.h>
#include <PealDatabase.h>
#include <Method.h>
#include "GenericTablePrintUtil.h"
#include "SingleTowerCirclingUI.h"
#include "MethodStatsUI.h"
#include "PrintTowerSummaryUtil.h"
#include "PealSearchUI.h"
#include <StatisticFilters.h>
#include <Association.h>
#include <DucoEngineUtils.h>
#include "SoundUtils.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Drawing::Printing;
using namespace System::Windows::Forms;

SingleObjectStatsUI::SingleObjectStatsUI(DucoUI::DatabaseManager^ theDatabase, System::Collections::Generic::List<System::UInt64>^ newPealIds, TObjectType theObjectType)
    :   database(theDatabase), objectType(theObjectType)
{
    objectIds = gcnew  System::Collections::Generic::List<System::UInt64>(newPealIds);
    pealIds = gcnew System::Collections::Generic::List<System::UInt64>(newPealIds);
    objectId = new Duco::ObjectId();
    InitializeComponent();
    pealInfo = new PealLengthInfo;
    this->Text = "Stats for all " + database->PerformanceString(false) + "s in search results";
    ringersCircledBtn->Visible = false;
    methodsBtn->Visible = false;
    summaryBtn->Visible = false;
    linkTowers->Enabled = false;
    linkTowers->Visible = false;
}

SingleObjectStatsUI::SingleObjectStatsUI(DatabaseManager^ theDatabase, Duco::ObjectId theObjectId, TObjectType theObjectType)
:   database(theDatabase), objectType(theObjectType)
{
    objectIds = gcnew  System::Collections::Generic::List<System::UInt64>();
    pealIds = gcnew System::Collections::Generic::List<System::UInt64>();
    objectIds->Add(theObjectId.Id());
    if (theObjectType == TObjectType::ETower)
        objectId = new Duco::ObjectId(theObjectId);
    else
        objectId = new Duco::ObjectId();

    InitializeComponent();
    pealInfo = new PealLengthInfo;

    switch (objectType)
    {
    case TObjectType::ETower:
    {
        Duco::Tower currentTower;
        if (database->FindTower(*objectId, currentTower, false))
        {
            this->Text = "Tower Statistics - " + DucoUtils::ConvertString(currentTower.FullName());
            ringersCircledBtn->Visible = true;
            methodsBtn->Visible = true;
            summaryBtn->Visible = true;
            menuStrip1->Visible = true;
        }
        else
        {
            ringersCircledBtn->Visible = false;
            methodsBtn->Visible = false;
            summaryBtn->Visible = false;
            menuStrip1->Visible = false;
        }
        if (currentTower.Handbell())
        {
            ringersCircledBtn->Visible = false;
        }
    }
    break;
    case TObjectType::EMethod:
        {
            Method currentMethod;
            if (database->FindMethod(theObjectId, currentMethod, false))
            {
                this->Text = "Method Statistics - " + DucoUtils::ConvertString(currentMethod.FullName(database->Database()));
            }
            linkTowers->Visible = false;
            ringersCircledBtn->Visible = false;
            methodsBtn->Visible = false;
            summaryBtn->Visible = false;
            menuStrip1->Visible = false;
        }
        break;
    case TObjectType::ERinger:
        {
            Duco::Ringer currentRinger;
            if (database->FindRinger(theObjectId, currentRinger, false))
            {
                this->Text = "Ringer Statistics - " + DucoUtils::ConvertString(currentRinger.FullName(database->Settings().LastNameFirst()));
            }
            ringersCircledBtn->Visible = false;
            methodsBtn->Visible = false;
            summaryBtn->Visible = false;
            linkTowers->Visible = false;
            menuStrip1->Visible = false;
        }
        break;
    case TObjectType::EAssociationOrSociety:
    {
        Duco::Association currentAssociation;
        if (database->FindAssociation(theObjectId, currentAssociation, false))
        {
            this->Text = "Association Statistics - " + DucoUtils::ConvertString(currentAssociation.Name());
        }
        ringersCircledBtn->Visible = false;
        methodsBtn->Visible = false;
        summaryBtn->Visible = false;
        linkTowers->Visible = false;
        menuStrip1->Visible = false;
    }
    break;
    default:
        ringersCircledBtn->Visible = false;
        methodsBtn->Visible = false;
        summaryBtn->Visible = false;
        includeWithdrawn->Visible = false;
        linkTowers->Visible = false;
        menuStrip1->Visible = false;
        this->Text = "Object Statistics - Error undefined object";
        break;
    }
}

SingleObjectStatsUI::~SingleObjectStatsUI()
{
    this->!SingleObjectStatsUI();
    if (components)
    {
        delete components;
    }
    delete pealInfo;
}

SingleObjectStatsUI::!SingleObjectStatsUI()
{
    database->RemoveObserver(this);
    delete objectIds;
    delete pealIds;
    delete objectId;
}

void
SingleObjectStatsUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    switch (type)
    {
        case TObjectType::EPeal:
            GenerateStats();
            break;
    }
}

System::Void
SingleObjectStatsUI::SingleObjectStatsUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    database->AddObserver(this);
    GenerateStats();
}

System::Void
SingleObjectStatsUI::GenerateStats()
{
    pealInfo->Clear();
    if (objectIds->Count == 0)
    {
        SoundUtils::PlayErrorSound();
        return;
    }
    Duco::ObjectId firstId (objectIds[0]);
    Duco::StatisticFilters filters(database->Database());
    filters.SetIncludeWithdrawn(includeWithdrawn->Checked);
    filters.SetIncludeLinkedTowers(linkTowers->Checked);
    std::set<ObjectId> ids;
    if (objectType == TObjectType::EPeal)
    {
        DucoUtils::Convert(objectIds, ids);
        *pealInfo = database->Database().PealsDatabase().PerformanceInfo(filters, ids);
    }
    else
    {
        switch (objectType)
        {
        case TObjectType::ETower:
            filters.SetTower(true, firstId);
            (*pealInfo) += database->Database().PealsDatabase().AllMatches(filters, ids);
            break;
        case TObjectType::EMethod:
            filters.SetMethod(true, firstId);
            (*pealInfo) += database->Database().PealsDatabase().AllMatches(filters, ids);
            break;
        case TObjectType::ERinger:
            filters.SetRinger(true, firstId);
            (*pealInfo) += database->Database().PealsDatabase().AllMatches( filters, ids);
            break;
        case TObjectType::EAssociationOrSociety:
            filters.SetAssociation(true, firstId);
            (*pealInfo) += database->Database().PealsDatabase().AllMatches(filters, ids);
            break;
        default:
            return;
        }
        DucoUtils::Convert(ids, pealIds);
    }
    Duco::Peal longestChangesPeal;
    if (database->FindPeal(pealInfo->LongestChangesId(), longestChangesPeal, false))
    {
        longestChanges->Text = DucoUtils::ConvertString(DucoEngineUtils::ToString(longestChangesPeal.NoOfChanges(), true));
    }
    else
    {
        longestChanges->Text = "--";
    }
    Duco::Peal shortestChangesPeal;
    if (database->FindPeal(pealInfo->ShortestChangesId(), shortestChangesPeal, false))
    {
        shortestChanges->Text = DucoUtils::ConvertString(DucoEngineUtils::ToString(shortestChangesPeal.NoOfChanges(), true));
    }
    else
    {
        shortestChanges->Text = "--";
    }
    Duco::Peal longestMinutesPeal;
    if (database->FindPeal(pealInfo->LongestMinutesId(), longestMinutesPeal, false))
    {
        longestTime->Text = DucoUtils::PrintPealTime(longestMinutesPeal.Time(), false);
    }
    else
    {
        longestTime->Text = "--";
    }
    Duco::Peal shortestMinutesPeal;
    if (database->FindPeal(pealInfo->ShortestMinutesId(), shortestMinutesPeal, false))
    {
        shortestTime->Text = DucoUtils::PrintPealTime(shortestMinutesPeal.Time(), false);
    }
    else
    {
        shortestTime->Text = "--";
    }
    Duco::Peal fastestCpmPeal;
    if (database->FindPeal(pealInfo->SlowestCpmId(), fastestCpmPeal, false))
    {
        longestCPM->Text = DucoUtils::FormatFloatToString(fastestCpmPeal.ChangesPerMinute());
    }
    else
    {
        longestCPM->Text = "--";
    }
    Duco::Peal slowestCpmPeal;
    if (database->FindPeal(pealInfo->FastestCpmId(), slowestCpmPeal, false))
    {
        shortestCPM->Text = DucoUtils::FormatFloatToString(slowestCpmPeal.ChangesPerMinute());
    }
    else
    {
        shortestCPM->Text = "--";
    }

    averageChanges->Text = DucoUtils::ConvertString(DucoEngineUtils::ToString(pealInfo->AverageChanges(), true));
    averageTime->Text = DucoUtils::PrintPealTime(pealInfo->AverageMinutes(), false);
    totalChanges->Text = DucoUtils::ConvertString(DucoEngineUtils::ToString(pealInfo->TotalChanges(), true));
    totalTime->Text = DucoUtils::PrintPealTime(pealInfo->TotalMinutes(), false);
    if (pealInfo->TotalChanges() > 0 && pealInfo->TotalMinutes() > 0)
        averageCPM->Text = DucoUtils::FormatDoubleToString(double(pealInfo->TotalChanges()) / double(pealInfo->TotalMinutes()), 2);
    else
        averageCPM->Text = "--";

    firstPeal->Text = DucoUtils::ConvertString(pealInfo->DateOfFirstPealString());
    lastPeal->Text = DucoUtils::ConvertString(pealInfo->DateOfLastPealString());
    totalNumberOfPeals->Text = DucoUtils::ConvertString(DucoEngineUtils::ToString(pealInfo->TotalPeals(), true));
}

System::Void
SingleObjectStatsUI::SingleObjectStatsUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e)
{
    database->RemoveObserver(this);
}

System::Void
SingleObjectStatsUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void
SingleObjectStatsUI::ringersCircledBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (objectIds->Count != 0)
    {
        SingleTowerCirclingUI^ circlingUI = gcnew SingleTowerCirclingUI(database, objectIds[0], linkTowers->Checked);
        circlingUI->MdiParent = this->MdiParent;
        circlingUI->Show();
    }
}

System::Void
SingleObjectStatsUI::methodsBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (objectType == TObjectType::ETower)
    {
        Duco::StatisticFilters filters(database->Database());
        filters.SetTower(true, *objectId);
        filters.SetIncludeLinkedTowers(linkTowers->Checked);
        filters.SetIncludeWithdrawn(includeWithdrawn->Checked);

        MethodStatsUI^ formUI = gcnew MethodStatsUI(database, filters);
        formUI->MdiParent = this->MdiParent;
        formUI->Show();

    }
}

System::Void
SingleObjectStatsUI::summaryBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    DucoUIUtils::Print(this, sender, gcnew System::Drawing::Printing::PrintPageEventHandler(this, &SingleObjectStatsUI::printSummary), "summary", database->Database().Settings().UsePrintPreview(), false);
}

System::Void
SingleObjectStatsUI::printSummary(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args)
{
    PrintDocument^ printDoc = static_cast<PrintDocument^>(sender);

    PrintTowerSummaryUtil printUtil(database, args, *objectId);
    printUtil.printObject(args, printDoc->PrinterSettings);
}

System::Void
SingleObjectStatsUI::longestTime_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
    PealUI::ShowPeal(database, this->MdiParent, pealInfo->LongestMinutesId());
}

System::Void
SingleObjectStatsUI::longestChanges_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
    PealUI::ShowPeal(database, this->MdiParent, pealInfo->LongestChangesId());
}

System::Void
SingleObjectStatsUI::longestCPM_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
    PealUI::ShowPeal(database, this->MdiParent, pealInfo->SlowestCpmId());
}

System::Void
SingleObjectStatsUI::shortestTime_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
    PealUI::ShowPeal(database, this->MdiParent, pealInfo->ShortestMinutesId());
}

System::Void
SingleObjectStatsUI::shortestChanges_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
    PealUI::ShowPeal(database, this->MdiParent, pealInfo->ShortestChangesId());
}

System::Void
SingleObjectStatsUI::shortestCPM_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
    PealUI::ShowPeal(database, this->MdiParent, pealInfo->FastestCpmId());
}

System::Void
SingleObjectStatsUI::firstPeal_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
    PealUI::ShowPeal(database, this->MdiParent, pealInfo->FirstPealId());
}

System::Void
SingleObjectStatsUI::lastPeal_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
    PealUI::ShowPeal(database, this->MdiParent, pealInfo->LastPealId());
}

System::Void
SingleObjectStatsUI::totalNumberOfPeals_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
    if (pealIds->Count == 1)
    {
        PealUI::ShowPeal(database, this->MdiParent, pealIds[0]);
    }
    else if (pealIds->Count > 1)
    {
        PealSearchUI^ pealsUI = gcnew PealSearchUI(database, false);
        if (objectType == TObjectType::ETower)
        {
            pealsUI->SearchPealsInTower(this->MdiParent, objectId->Id());
        }
        else
        {
            pealsUI->Show(this->MdiParent, pealIds);
        }
    }
}

System::Void
SingleObjectStatsUI::linkTowers_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    GenerateStats();
}

System::Void
SingleObjectStatsUI::bandMetricsToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e)
{
    BandStats^ bandStats = gcnew BandStats(database, *objectId);
    bandStats->MdiParent = MdiParent;
    bandStats->Show();
}

#pragma region Windows Form Designer generated code
void SingleObjectStatsUI::InitializeComponent(void)
{
    System::Windows::Forms::Label^ totalPealsLbl;
    System::Windows::Forms::Label^ changesPerMinuteLbl;
    System::Windows::Forms::Label^ totalChangesLbl;
    System::Windows::Forms::Label^ averagePealLbl;
    System::Windows::Forms::Label^ shortestPealLbl;
    System::Windows::Forms::Label^ longestPealLbl;
    System::Windows::Forms::Label^ changesLbl;
    System::Windows::Forms::Label^ timeLbl;
    System::Windows::Forms::Button^ closeBtn;
    System::Windows::Forms::Label^ firstPealLbl;
    System::Windows::Forms::Label^ lastPealLbl;
    System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
    System::Windows::Forms::ToolStripMenuItem^ bandMetricsToolStripMenuItem;
    this->linkTowers = (gcnew System::Windows::Forms::CheckBox());
    this->includeWithdrawn = (gcnew System::Windows::Forms::CheckBox());
    this->longestCPM = (gcnew System::Windows::Forms::TextBox());
    this->shortestCPM = (gcnew System::Windows::Forms::TextBox());
    this->totalNumberOfPeals = (gcnew System::Windows::Forms::TextBox());
    this->averageCPM = (gcnew System::Windows::Forms::TextBox());
    this->shortestChanges = (gcnew System::Windows::Forms::TextBox());
    this->lastPeal = (gcnew System::Windows::Forms::TextBox());
    this->firstPeal = (gcnew System::Windows::Forms::TextBox());
    this->longestChanges = (gcnew System::Windows::Forms::TextBox());
    this->averageChanges = (gcnew System::Windows::Forms::TextBox());
    this->totalChanges = (gcnew System::Windows::Forms::TextBox());
    this->totalTime = (gcnew System::Windows::Forms::TextBox());
    this->longestTime = (gcnew System::Windows::Forms::TextBox());
    this->shortestTime = (gcnew System::Windows::Forms::TextBox());
    this->averageTime = (gcnew System::Windows::Forms::TextBox());
    this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
    this->summaryBtn = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->methodsBtn = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->ringersCircledBtn = (gcnew System::Windows::Forms::ToolStripMenuItem());
    totalPealsLbl = (gcnew System::Windows::Forms::Label());
    changesPerMinuteLbl = (gcnew System::Windows::Forms::Label());
    totalChangesLbl = (gcnew System::Windows::Forms::Label());
    averagePealLbl = (gcnew System::Windows::Forms::Label());
    shortestPealLbl = (gcnew System::Windows::Forms::Label());
    longestPealLbl = (gcnew System::Windows::Forms::Label());
    changesLbl = (gcnew System::Windows::Forms::Label());
    timeLbl = (gcnew System::Windows::Forms::Label());
    closeBtn = (gcnew System::Windows::Forms::Button());
    firstPealLbl = (gcnew System::Windows::Forms::Label());
    lastPealLbl = (gcnew System::Windows::Forms::Label());
    tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
    bandMetricsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    tableLayoutPanel1->SuspendLayout();
    this->menuStrip1->SuspendLayout();
    this->SuspendLayout();
    // 
    // totalPealsLbl
    // 
    totalPealsLbl->AutoSize = true;
    totalPealsLbl->Dock = System::Windows::Forms::DockStyle::Fill;
    totalPealsLbl->Location = System::Drawing::Point(3, 189);
    totalPealsLbl->Margin = System::Windows::Forms::Padding(3);
    totalPealsLbl->Name = L"totalPealsLbl";
    totalPealsLbl->Size = System::Drawing::Size(70, 20);
    totalPealsLbl->TabIndex = 24;
    totalPealsLbl->Text = L"Total peals";
    totalPealsLbl->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
    // 
    // changesPerMinuteLbl
    // 
    changesPerMinuteLbl->AutoSize = true;
    changesPerMinuteLbl->Location = System::Drawing::Point(301, 3);
    changesPerMinuteLbl->Margin = System::Windows::Forms::Padding(3);
    changesPerMinuteLbl->Name = L"changesPerMinuteLbl";
    changesPerMinuteLbl->Size = System::Drawing::Size(101, 13);
    changesPerMinuteLbl->TabIndex = 2;
    changesPerMinuteLbl->Text = L"Changes per minute";
    // 
    // totalChangesLbl
    // 
    totalChangesLbl->AutoSize = true;
    totalChangesLbl->Dock = System::Windows::Forms::DockStyle::Fill;
    totalChangesLbl->Location = System::Drawing::Point(3, 100);
    totalChangesLbl->Margin = System::Windows::Forms::Padding(3);
    totalChangesLbl->Name = L"totalChangesLbl";
    totalChangesLbl->Size = System::Drawing::Size(70, 20);
    totalChangesLbl->TabIndex = 15;
    totalChangesLbl->Text = L"Total";
    totalChangesLbl->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
    // 
    // averagePealLbl
    // 
    averagePealLbl->AutoSize = true;
    averagePealLbl->Dock = System::Windows::Forms::DockStyle::Fill;
    averagePealLbl->Location = System::Drawing::Point(3, 74);
    averagePealLbl->Margin = System::Windows::Forms::Padding(3);
    averagePealLbl->Name = L"averagePealLbl";
    averagePealLbl->Size = System::Drawing::Size(70, 20);
    averagePealLbl->TabIndex = 11;
    averagePealLbl->Text = L"Average peal";
    averagePealLbl->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
    // 
    // shortestPealLbl
    // 
    shortestPealLbl->AutoSize = true;
    shortestPealLbl->Dock = System::Windows::Forms::DockStyle::Fill;
    shortestPealLbl->Location = System::Drawing::Point(3, 48);
    shortestPealLbl->Margin = System::Windows::Forms::Padding(3);
    shortestPealLbl->Name = L"shortestPealLbl";
    shortestPealLbl->Size = System::Drawing::Size(70, 20);
    shortestPealLbl->TabIndex = 7;
    shortestPealLbl->Text = L"Shortest peal";
    shortestPealLbl->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
    // 
    // longestPealLbl
    // 
    longestPealLbl->AutoSize = true;
    longestPealLbl->Dock = System::Windows::Forms::DockStyle::Fill;
    longestPealLbl->Location = System::Drawing::Point(3, 22);
    longestPealLbl->Margin = System::Windows::Forms::Padding(3);
    longestPealLbl->Name = L"longestPealLbl";
    longestPealLbl->Size = System::Drawing::Size(70, 20);
    longestPealLbl->TabIndex = 3;
    longestPealLbl->Text = L"Longest peal";
    longestPealLbl->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
    // 
    // changesLbl
    // 
    changesLbl->AutoSize = true;
    changesLbl->Location = System::Drawing::Point(190, 3);
    changesLbl->Margin = System::Windows::Forms::Padding(3);
    changesLbl->Name = L"changesLbl";
    changesLbl->Size = System::Drawing::Size(49, 13);
    changesLbl->TabIndex = 1;
    changesLbl->Text = L"Changes";
    // 
    // timeLbl
    // 
    timeLbl->AutoSize = true;
    timeLbl->Location = System::Drawing::Point(79, 3);
    timeLbl->Margin = System::Windows::Forms::Padding(3);
    timeLbl->Name = L"timeLbl";
    timeLbl->Size = System::Drawing::Size(30, 13);
    timeLbl->TabIndex = 0;
    timeLbl->Text = L"Time";
    // 
    // closeBtn
    // 
    closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
    closeBtn->AutoSize = true;
    closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
    closeBtn->Location = System::Drawing::Point(339, 215);
    closeBtn->Name = L"closeBtn";
    closeBtn->Size = System::Drawing::Size(73, 23);
    closeBtn->TabIndex = 29;
    closeBtn->Text = L"Close";
    closeBtn->UseVisualStyleBackColor = true;
    closeBtn->Click += gcnew System::EventHandler(this, &SingleObjectStatsUI::closeBtn_Click);
    // 
    // firstPealLbl
    // 
    firstPealLbl->AutoSize = true;
    firstPealLbl->Dock = System::Windows::Forms::DockStyle::Fill;
    firstPealLbl->Location = System::Drawing::Point(3, 137);
    firstPealLbl->Margin = System::Windows::Forms::Padding(3);
    firstPealLbl->Name = L"firstPealLbl";
    firstPealLbl->Size = System::Drawing::Size(70, 20);
    firstPealLbl->TabIndex = 18;
    firstPealLbl->Text = L"First peal";
    firstPealLbl->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
    // 
    // lastPealLbl
    // 
    lastPealLbl->AutoSize = true;
    lastPealLbl->Dock = System::Windows::Forms::DockStyle::Fill;
    lastPealLbl->Location = System::Drawing::Point(3, 163);
    lastPealLbl->Margin = System::Windows::Forms::Padding(3);
    lastPealLbl->Name = L"lastPealLbl";
    lastPealLbl->Size = System::Drawing::Size(70, 20);
    lastPealLbl->TabIndex = 21;
    lastPealLbl->Text = L"Last peal";
    lastPealLbl->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
    // 
    // tableLayoutPanel1
    // 
    tableLayoutPanel1->ColumnCount = 4;
    tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
    tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 33)));
    tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 33)));
    tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 34)));
    tableLayoutPanel1->Controls->Add(changesPerMinuteLbl, 3, 0);
    tableLayoutPanel1->Controls->Add(this->linkTowers, 2, 7);
    tableLayoutPanel1->Controls->Add(this->includeWithdrawn, 2, 6);
    tableLayoutPanel1->Controls->Add(this->longestCPM, 3, 1);
    tableLayoutPanel1->Controls->Add(this->shortestCPM, 3, 2);
    tableLayoutPanel1->Controls->Add(this->totalNumberOfPeals, 1, 8);
    tableLayoutPanel1->Controls->Add(totalPealsLbl, 0, 8);
    tableLayoutPanel1->Controls->Add(shortestPealLbl, 0, 2);
    tableLayoutPanel1->Controls->Add(totalChangesLbl, 0, 4);
    tableLayoutPanel1->Controls->Add(this->averageCPM, 3, 3);
    tableLayoutPanel1->Controls->Add(lastPealLbl, 0, 7);
    tableLayoutPanel1->Controls->Add(this->shortestChanges, 2, 2);
    tableLayoutPanel1->Controls->Add(this->lastPeal, 1, 7);
    tableLayoutPanel1->Controls->Add(this->firstPeal, 1, 6);
    tableLayoutPanel1->Controls->Add(averagePealLbl, 0, 3);
    tableLayoutPanel1->Controls->Add(firstPealLbl, 0, 6);
    tableLayoutPanel1->Controls->Add(longestPealLbl, 0, 1);
    tableLayoutPanel1->Controls->Add(changesLbl, 2, 0);
    tableLayoutPanel1->Controls->Add(this->longestChanges, 2, 1);
    tableLayoutPanel1->Controls->Add(this->averageChanges, 2, 3);
    tableLayoutPanel1->Controls->Add(this->totalChanges, 2, 4);
    tableLayoutPanel1->Controls->Add(timeLbl, 1, 0);
    tableLayoutPanel1->Controls->Add(this->totalTime, 1, 4);
    tableLayoutPanel1->Controls->Add(this->longestTime, 1, 1);
    tableLayoutPanel1->Controls->Add(this->shortestTime, 1, 2);
    tableLayoutPanel1->Controls->Add(this->averageTime, 1, 3);
    tableLayoutPanel1->Controls->Add(closeBtn, 3, 9);
    tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
    tableLayoutPanel1->Location = System::Drawing::Point(0, 24);
    tableLayoutPanel1->Name = L"tableLayoutPanel1";
    tableLayoutPanel1->RowCount = 10;
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
    tableLayoutPanel1->Size = System::Drawing::Size(415, 241);
    tableLayoutPanel1->TabIndex = 30;
    // 
    // linkTowers
    // 
    this->linkTowers->AutoSize = true;
    tableLayoutPanel1->SetColumnSpan(this->linkTowers, 2);
    this->linkTowers->Dock = System::Windows::Forms::DockStyle::Fill;
    this->linkTowers->Location = System::Drawing::Point(190, 163);
    this->linkTowers->Name = L"linkTowers";
    this->linkTowers->Size = System::Drawing::Size(222, 20);
    this->linkTowers->TabIndex = 23;
    this->linkTowers->Text = L"Combine Linked towers";
    this->linkTowers->UseVisualStyleBackColor = true;
    this->linkTowers->CheckedChanged += gcnew System::EventHandler(this, &SingleObjectStatsUI::linkTowers_CheckedChanged);
    // 
    // includeWithdrawn
    // 
    this->includeWithdrawn->AutoSize = true;
    tableLayoutPanel1->SetColumnSpan(this->includeWithdrawn, 2);
    this->includeWithdrawn->Dock = System::Windows::Forms::DockStyle::Fill;
    this->includeWithdrawn->Location = System::Drawing::Point(190, 137);
    this->includeWithdrawn->Name = L"includeWithdrawn";
    this->includeWithdrawn->Size = System::Drawing::Size(222, 20);
    this->includeWithdrawn->TabIndex = 20;
    this->includeWithdrawn->Text = L"Include withdrawn performances";
    this->includeWithdrawn->UseVisualStyleBackColor = true;
    this->includeWithdrawn->CheckedChanged += gcnew System::EventHandler(this, &SingleObjectStatsUI::linkTowers_CheckedChanged);
    // 
    // longestCPM
    // 
    this->longestCPM->Dock = System::Windows::Forms::DockStyle::Fill;
    this->longestCPM->Location = System::Drawing::Point(301, 22);
    this->longestCPM->MinimumSize = System::Drawing::Size(75, 20);
    this->longestCPM->Name = L"longestCPM";
    this->longestCPM->ReadOnly = true;
    this->longestCPM->Size = System::Drawing::Size(111, 20);
    this->longestCPM->TabIndex = 6;
    this->longestCPM->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
    this->longestCPM->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &SingleObjectStatsUI::longestCPM_MouseDown);
    // 
    // shortestCPM
    // 
    this->shortestCPM->Dock = System::Windows::Forms::DockStyle::Fill;
    this->shortestCPM->Location = System::Drawing::Point(301, 48);
    this->shortestCPM->Name = L"shortestCPM";
    this->shortestCPM->ReadOnly = true;
    this->shortestCPM->Size = System::Drawing::Size(111, 20);
    this->shortestCPM->TabIndex = 10;
    this->shortestCPM->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
    this->shortestCPM->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &SingleObjectStatsUI::shortestCPM_MouseDown);
    // 
    // totalNumberOfPeals
    // 
    this->totalNumberOfPeals->Dock = System::Windows::Forms::DockStyle::Fill;
    this->totalNumberOfPeals->Location = System::Drawing::Point(79, 189);
    this->totalNumberOfPeals->Name = L"totalNumberOfPeals";
    this->totalNumberOfPeals->ReadOnly = true;
    this->totalNumberOfPeals->Size = System::Drawing::Size(105, 20);
    this->totalNumberOfPeals->TabIndex = 25;
    this->totalNumberOfPeals->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
    this->totalNumberOfPeals->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &SingleObjectStatsUI::totalNumberOfPeals_MouseDown);
    // 
    // averageCPM
    // 
    this->averageCPM->Dock = System::Windows::Forms::DockStyle::Fill;
    this->averageCPM->Location = System::Drawing::Point(301, 74);
    this->averageCPM->Name = L"averageCPM";
    this->averageCPM->ReadOnly = true;
    this->averageCPM->Size = System::Drawing::Size(111, 20);
    this->averageCPM->TabIndex = 14;
    this->averageCPM->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
    // 
    // shortestChanges
    // 
    this->shortestChanges->Dock = System::Windows::Forms::DockStyle::Fill;
    this->shortestChanges->Location = System::Drawing::Point(190, 48);
    this->shortestChanges->Name = L"shortestChanges";
    this->shortestChanges->ReadOnly = true;
    this->shortestChanges->Size = System::Drawing::Size(105, 20);
    this->shortestChanges->TabIndex = 9;
    this->shortestChanges->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
    this->shortestChanges->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &SingleObjectStatsUI::shortestChanges_MouseDown);
    // 
    // lastPeal
    // 
    this->lastPeal->Dock = System::Windows::Forms::DockStyle::Fill;
    this->lastPeal->Location = System::Drawing::Point(79, 163);
    this->lastPeal->Name = L"lastPeal";
    this->lastPeal->ReadOnly = true;
    this->lastPeal->Size = System::Drawing::Size(105, 20);
    this->lastPeal->TabIndex = 22;
    this->lastPeal->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
    this->lastPeal->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &SingleObjectStatsUI::lastPeal_MouseDown);
    // 
    // firstPeal
    // 
    this->firstPeal->Dock = System::Windows::Forms::DockStyle::Fill;
    this->firstPeal->Location = System::Drawing::Point(79, 137);
    this->firstPeal->Name = L"firstPeal";
    this->firstPeal->ReadOnly = true;
    this->firstPeal->Size = System::Drawing::Size(105, 20);
    this->firstPeal->TabIndex = 19;
    this->firstPeal->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
    this->firstPeal->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &SingleObjectStatsUI::firstPeal_MouseDown);
    // 
    // longestChanges
    // 
    this->longestChanges->Dock = System::Windows::Forms::DockStyle::Fill;
    this->longestChanges->Location = System::Drawing::Point(190, 22);
    this->longestChanges->MinimumSize = System::Drawing::Size(75, 20);
    this->longestChanges->Name = L"longestChanges";
    this->longestChanges->ReadOnly = true;
    this->longestChanges->Size = System::Drawing::Size(105, 20);
    this->longestChanges->TabIndex = 5;
    this->longestChanges->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
    this->longestChanges->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &SingleObjectStatsUI::longestChanges_MouseDown);
    // 
    // averageChanges
    // 
    this->averageChanges->Dock = System::Windows::Forms::DockStyle::Fill;
    this->averageChanges->Location = System::Drawing::Point(190, 74);
    this->averageChanges->Name = L"averageChanges";
    this->averageChanges->ReadOnly = true;
    this->averageChanges->Size = System::Drawing::Size(105, 20);
    this->averageChanges->TabIndex = 13;
    this->averageChanges->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
    // 
    // totalChanges
    // 
    this->totalChanges->Dock = System::Windows::Forms::DockStyle::Fill;
    this->totalChanges->Location = System::Drawing::Point(190, 100);
    this->totalChanges->Name = L"totalChanges";
    this->totalChanges->ReadOnly = true;
    this->totalChanges->Size = System::Drawing::Size(105, 20);
    this->totalChanges->TabIndex = 17;
    this->totalChanges->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
    // 
    // totalTime
    // 
    this->totalTime->Dock = System::Windows::Forms::DockStyle::Fill;
    this->totalTime->Location = System::Drawing::Point(79, 100);
    this->totalTime->Name = L"totalTime";
    this->totalTime->ReadOnly = true;
    this->totalTime->Size = System::Drawing::Size(105, 20);
    this->totalTime->TabIndex = 16;
    this->totalTime->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
    // 
    // longestTime
    // 
    this->longestTime->Dock = System::Windows::Forms::DockStyle::Fill;
    this->longestTime->Location = System::Drawing::Point(79, 22);
    this->longestTime->Name = L"longestTime";
    this->longestTime->ReadOnly = true;
    this->longestTime->Size = System::Drawing::Size(105, 20);
    this->longestTime->TabIndex = 4;
    this->longestTime->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
    this->longestTime->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &SingleObjectStatsUI::longestTime_MouseDown);
    // 
    // shortestTime
    // 
    this->shortestTime->Dock = System::Windows::Forms::DockStyle::Fill;
    this->shortestTime->Location = System::Drawing::Point(79, 48);
    this->shortestTime->Name = L"shortestTime";
    this->shortestTime->ReadOnly = true;
    this->shortestTime->Size = System::Drawing::Size(105, 20);
    this->shortestTime->TabIndex = 8;
    this->shortestTime->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
    this->shortestTime->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &SingleObjectStatsUI::shortestTime_MouseDown);
    // 
    // averageTime
    // 
    this->averageTime->Dock = System::Windows::Forms::DockStyle::Fill;
    this->averageTime->Location = System::Drawing::Point(79, 74);
    this->averageTime->Name = L"averageTime";
    this->averageTime->ReadOnly = true;
    this->averageTime->Size = System::Drawing::Size(105, 20);
    this->averageTime->TabIndex = 12;
    this->averageTime->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
    // 
    // bandMetricsToolStripMenuItem
    // 
    bandMetricsToolStripMenuItem->Name = L"bandMetricsToolStripMenuItem";
    bandMetricsToolStripMenuItem->Size = System::Drawing::Size(88, 20);
    bandMetricsToolStripMenuItem->Text = L"Band metrics";
    bandMetricsToolStripMenuItem->Click += gcnew System::EventHandler(this, &SingleObjectStatsUI::bandMetricsToolStripMenuItem_Click);
    // 
    // menuStrip1
    // 
    this->menuStrip1->AllowMerge = false;
    this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {
        this->summaryBtn, this->methodsBtn,
            this->ringersCircledBtn, bandMetricsToolStripMenuItem
    });
    this->menuStrip1->Location = System::Drawing::Point(0, 0);
    this->menuStrip1->Name = L"menuStrip1";
    this->menuStrip1->Size = System::Drawing::Size(415, 24);
    this->menuStrip1->TabIndex = 31;
    this->menuStrip1->Text = L"menuStrip1";
    // 
    // summaryBtn
    // 
    this->summaryBtn->Name = L"summaryBtn";
    this->summaryBtn->Size = System::Drawing::Size(70, 20);
    this->summaryBtn->Text = L"Summary";
    this->summaryBtn->Click += gcnew System::EventHandler(this, &SingleObjectStatsUI::summaryBtn_Click);
    // 
    // methodsBtn
    // 
    this->methodsBtn->Name = L"methodsBtn";
    this->methodsBtn->Size = System::Drawing::Size(66, 20);
    this->methodsBtn->Text = L"Methods";
    this->methodsBtn->Click += gcnew System::EventHandler(this, &SingleObjectStatsUI::methodsBtn_Click);
    // 
    // ringersCircledBtn
    // 
    this->ringersCircledBtn->Name = L"ringersCircledBtn";
    this->ringersCircledBtn->Size = System::Drawing::Size(95, 20);
    this->ringersCircledBtn->Text = L"Ringer circling";
    this->ringersCircledBtn->Click += gcnew System::EventHandler(this, &SingleObjectStatsUI::ringersCircledBtn_Click);
    // 
    // SingleObjectStatsUI
    // 
    this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
    this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
    this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
    this->CancelButton = closeBtn;
    this->CausesValidation = false;
    this->ClientSize = System::Drawing::Size(415, 265);
    this->Controls->Add(tableLayoutPanel1);
    this->Controls->Add(this->menuStrip1);
    this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
    this->MainMenuStrip = this->menuStrip1;
    this->MinimumSize = System::Drawing::Size(411, 304);
    this->Name = L"SingleObjectStatsUI";
    this->Text = L"Statistics";
    this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &SingleObjectStatsUI::SingleObjectStatsUI_FormClosing);
    this->Load += gcnew System::EventHandler(this, &SingleObjectStatsUI::SingleObjectStatsUI_Load);
    tableLayoutPanel1->ResumeLayout(false);
    tableLayoutPanel1->PerformLayout();
    this->menuStrip1->ResumeLayout(false);
    this->menuStrip1->PerformLayout();
    this->ResumeLayout(false);
    this->PerformLayout();

}
#pragma endregion
