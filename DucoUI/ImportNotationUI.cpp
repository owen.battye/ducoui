#include "ImportNotationUI.h"
#include "SystemDefaultIcon.h"
#include "SoundUtils.h"
#include <map>
#include <DucoEngineUtils.h>
#include <Method.h>
#include "DatabaseManager.h"
#include <RingingDatabase.h>
#include "DucoUtils.h"
#include <MethodDatabase.h>

using namespace Duco;
using namespace DucoUI;

using namespace System;
using namespace System::IO;
using namespace System::Net;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Threading;

#define KMethodCollectionWebsite "http://www.methods.org.uk/method-collections/xml-zip-files/index.htm"
#define KMethodCollectionFile "http://www.methods.org.uk/method-collections/xml-zip-files/allmeths-xml.zip"

namespace DucoUI
{
    ref class ImportNotationUIArgs
    {
    public:
        String^ fileName;
        std::map<std::wstring, Duco::ObjectId>* methods;
    };
}

ImportNotationUI::ImportNotationUI(DatabaseManager^ theDatabase)
: database(theDatabase)
{
    InitializeComponent();
    progressWrapper = new ProgressCallbackWrapper(this);
    methods = new std::map<Duco::ObjectId, std::wstring>;
}

ImportNotationUI::!ImportNotationUI()
{
    delete methods;
    delete progressWrapper;
}

ImportNotationUI::~ImportNotationUI()
{
    this->!ImportNotationUI();
    if (components)
    {
        delete components;
    }
}

System::Void
ImportNotationUI::ImportNotationUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    toolTip->SetToolTip(downloadFileBtn, "Download the default file from the Central Council so you can extract it somewhere to use: " + KMethodCollectionFile);
    this->importBtn->Enabled = (database->XmlMethodCollectionFile()->Length > 0);
    fileNameEditor->Text = database->XmlMethodCollectionFile();
    GenerateMethodOptions();
    if (fileNameEditor->Text->Length <= 0)
    {
        OpenFileSelectionDlg();
    }
}

System::Void
ImportNotationUI::ImportNotationUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e)
{
    if (!closeBtn->Enabled )
        e->Cancel = true;

    if (downloadClient->IsBusy)
    {
        downloadClient->CancelAsync();
        e->Cancel = true;
    }

    if (importMethodsWorker->IsBusy)
    {
        importMethodsWorker->CancelAsync();
        e->Cancel = true;
    }
}

System::Void
ImportNotationUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void
ImportNotationUI::importBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
        return;
    }

    closeBtn->Enabled = false;
    downloadFileBtn->Enabled = false;
    noOfMethodsUpdated->Text = "";

    std::map<std::wstring, Duco::ObjectId>* methodsToUpdate = new std::map<std::wstring, Duco::ObjectId>();
    if (allMethods->Checked)
    {
        std::map<Duco::ObjectId, std::wstring>::const_iterator it = methods->begin();
        while (it != methods->end())
        {
            std::wstring lowerCaseName;
            DucoEngineUtils::ToLowerCase(DucoEngineUtils::MethodComparisionCharacters(it->second), lowerCaseName);

            std::pair<std::wstring, Duco::ObjectId> newObject (lowerCaseName, it->first);
            methodsToUpdate->insert(newObject);
            ++it;
        }
    }
    else if (oneMethod->Checked)
    {
        unsigned int selectedIndex = methodList->SelectedIndex;
        std::map<Duco::ObjectId, std::wstring>::const_iterator it = methods->begin();
        for (unsigned int i = 0; i < selectedIndex && it != methods->end(); ++i)
        {
            ++it;
        }
        if (it != methods->end())
        {
            std::wstring lowerCaseName;
            DucoEngineUtils::ToLowerCase(DucoEngineUtils::MethodComparisionCharacters(it->second), lowerCaseName);

            std::pair<std::wstring, Duco::ObjectId> newObject (lowerCaseName, it->first);
            methodsToUpdate->insert(newObject);
        }
    }
    database->SetXmlMethodCollectionFile(fileNameEditor->Text);
    ImportNotationUIArgs^ args = gcnew ImportNotationUIArgs();
    args->fileName = fileNameEditor->Text;
    args->methods = methodsToUpdate;
    importMethodsWorker->RunWorkerAsync(args);
}

System::Void
ImportNotationUI::openFileBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    OpenFileSelectionDlg();
}

System::Void
ImportNotationUI::oneMethod_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    methodList->Enabled = oneMethod->Checked;
    importBtn->Enabled = methodList->SelectedIndex != -1;
}

System::Void
ImportNotationUI::methodList_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    importBtn->Enabled = methodList->SelectedIndex != -1;
}

System::Void
ImportNotationUI::allMethods_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    methodList->Enabled = !allMethods->Checked;
    importBtn->Enabled = true;
}

bool
ImportNotationUI::OpenFileSelectionDlg()
{
    OpenFileDialog^ fileDialog = gcnew OpenFileDialog ();
    fileDialog->Filter = "Xml files (*.xml)|*.xml" ;
    try 
    {
        fileDialog->FileName = Path::GetFileName(database->XmlMethodCollectionFile());
        fileDialog->InitialDirectory = Path::GetDirectoryName(database->XmlMethodCollectionFile());
    }
    catch (Exception^)
    {
    }
    fileDialog->RestoreDirectory = true;

    switch (fileDialog->ShowDialog())
    {
    case System::Windows::Forms::DialogResult::OK:
        {
            try
            {
                String^ fileName = fileDialog->FileName;
                fileDialog->OpenFile()->Close();
                fileNameEditor->Text = fileName;
            }
            catch (Exception^ ex)
            {
                SoundUtils::PlayErrorSound();
                MessageBox::Show(this, "Error: Could not open file: " + ex->Message);
            }
        }
    default:
        return true;

    case System::Windows::Forms::DialogResult::Cancel:
        if (fileNameEditor->Text->Length == 0)
            importBtn->Enabled = false;
        return false;
    }
}

System::Void
ImportNotationUI::GenerateMethodOptions()
{
    methodList->BeginUpdate();
    methodList->Items->Clear();
    methodList->Text = "";

    methods->clear();
    database->Database().MethodsDatabase().GetMethodsWithoutPlaceNotation(*methods);

    std::map<Duco::ObjectId, std::wstring>::const_iterator it = methods->begin();
    while (it != methods->end())
    {
        const std::wstring& tempMethodname = it->second;
        methodList->Items->Add(DucoUtils::ConvertString(tempMethodname));
        ++it;
    }
    methodList->EndUpdate();
}

System::Void
ImportNotationUI::importMethodsWorker_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
    ImportNotationUIArgs^ args = static_cast<ImportNotationUIArgs^>(e->Argument);
    unsigned int noOfUpdatedMethods (database->ImportMethodPlaceNotations(progressWrapper, args->fileName, *args->methods));
    if (noOfUpdatedMethods <= 0)
    {
        noOfMethodsUpdated->Text = "No methods updated";
        SoundUtils::PlayErrorSound();
    }
    else
    {
        noOfMethodsUpdated->Text = Convert::ToString(noOfUpdatedMethods) + " methods updated";
        SoundUtils::PlayFinishedSound();
    }
    delete args->methods;
}

void
ImportNotationUI::Initialised()
{

}

void
ImportNotationUI::Step(int progressPercent)
{
    importMethodsWorker->ReportProgress(progressPercent);
}

void
ImportNotationUI::Complete()
{
    importMethodsWorker->ReportProgress(0);
}

System::Void
ImportNotationUI::importMethodsWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    if (e->ProgressPercentage > 100)
        progressBar->Value = 100;
    else
        progressBar->Value = e->ProgressPercentage;
}

System::Void
ImportNotationUI::importMethodsWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    progressBar->Value = 0;
    closeBtn->Enabled = true;
    downloadFileBtn->Enabled = true;
}

System::Void
ImportNotationUI::downloadFileBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    downloadFileBtn->Enabled = false;
    importBtn->Enabled = false;
    String^ tempFilename = Path::GetFileName(Path::GetTempFileName());
    tempFilename = Path::ChangeExtension(tempFilename, ".zip");
    String^ fullFilename = Path::GetTempPath();
    fullFilename += tempFilename;
    Uri^ fileLocation = gcnew Uri(KMethodCollectionFile);
    downloadClient->DownloadFileAsync(fileLocation, fullFilename, fullFilename);
}

System::Void
ImportNotationUI::downloadClient_DownloadFileCompleted(System::Object^  sender, System::ComponentModel::AsyncCompletedEventArgs^  e)
{
    downloadFileBtn->Enabled = true;
    importBtn->Enabled = true;

    if (e->Cancelled || e->Error != nullptr)
    {
        SoundUtils::PlayErrorSound();
        if (e->Error != nullptr)
        {
            MessageBox::Show(this, e->Error->ToString(), "Error downloading file");
        }
    }
    else if (e->UserState != nullptr)
    {
        System::Diagnostics::Process::Start( static_cast<String^>(e->UserState) );
    }
}

void
ImportNotationUI::InitializeComponent()
{
    this->components = (gcnew System::ComponentModel::Container());
    System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(ImportNotationUI::typeid));
    this->closeBtn = (gcnew System::Windows::Forms::Button());
    this->fileNameEditor = (gcnew System::Windows::Forms::TextBox());
    this->importBtn = (gcnew System::Windows::Forms::Button());
    this->allMethods = (gcnew System::Windows::Forms::RadioButton());
    this->oneMethod = (gcnew System::Windows::Forms::RadioButton());
    this->methodList = (gcnew System::Windows::Forms::ComboBox());
    this->openFileBtn = (gcnew System::Windows::Forms::Button());
    this->downloadFileBtn = (gcnew System::Windows::Forms::Button());
    this->statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
    this->progressBar = (gcnew System::Windows::Forms::ToolStripProgressBar());
    this->noOfMethodsUpdated = (gcnew System::Windows::Forms::ToolStripStatusLabel());
    this->importMethodsWorker = (gcnew System::ComponentModel::BackgroundWorker());
    this->websiteBtn = (gcnew System::Windows::Forms::Button());
    this->downloadClient = (gcnew System::Net::WebClient());
    this->toolTip = (gcnew System::Windows::Forms::ToolTip(this->components));
    this->statusStrip1->SuspendLayout();
    this->SuspendLayout();
    // 
    // closeBtn
    // 
    this->closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
    this->closeBtn->Location = System::Drawing::Point(383, 89);
    this->closeBtn->Name = L"closeBtn";
    this->closeBtn->Size = System::Drawing::Size(75, 23);
    this->closeBtn->TabIndex = 9;
    this->closeBtn->Text = L"&Close";
    this->closeBtn->UseVisualStyleBackColor = true;
    this->closeBtn->Click += gcnew System::EventHandler(this, &ImportNotationUI::closeBtn_Click);
    // 
    // fileNameEditor
    // 
    this->fileNameEditor->Location = System::Drawing::Point(9, 12);
    this->fileNameEditor->Name = L"fileNameEditor";
    this->fileNameEditor->ReadOnly = true;
    this->fileNameEditor->Size = System::Drawing::Size(374, 20);
    this->fileNameEditor->TabIndex = 1;
    this->toolTip->SetToolTip(this->fileNameEditor, L"Name of the file containing the CC method collection");
    // 
    // importBtn
    // 
    this->importBtn->Location = System::Drawing::Point(308, 89);
    this->importBtn->Name = L"importBtn";
    this->importBtn->Size = System::Drawing::Size(75, 23);
    this->importBtn->TabIndex = 8;
    this->importBtn->Text = L"Import";
    this->toolTip->SetToolTip(this->importBtn, L"Start importing the selected methods");
    this->importBtn->UseVisualStyleBackColor = true;
    this->importBtn->Click += gcnew System::EventHandler(this, &ImportNotationUI::importBtn_Click);
    // 
    // allMethods
    // 
    this->allMethods->AutoSize = true;
    this->allMethods->Location = System::Drawing::Point(9, 40);
    this->allMethods->Name = L"allMethods";
    this->allMethods->Size = System::Drawing::Size(311, 17);
    this->allMethods->TabIndex = 3;
    this->allMethods->Text = L"Import place notation for all existing methods without notation";
    this->toolTip->SetToolTip(this->allMethods, L"Update all methods without notation");
    this->allMethods->UseVisualStyleBackColor = true;
    this->allMethods->CheckedChanged += gcnew System::EventHandler(this, &ImportNotationUI::allMethods_CheckedChanged);
    // 
    // oneMethod
    // 
    this->oneMethod->AutoSize = true;
    this->oneMethod->Checked = true;
    this->oneMethod->Location = System::Drawing::Point(9, 63);
    this->oneMethod->Name = L"oneMethod";
    this->oneMethod->Size = System::Drawing::Size(198, 17);
    this->oneMethod->TabIndex = 4;
    this->oneMethod->TabStop = true;
    this->oneMethod->Text = L"Import place notation for one method";
    this->toolTip->SetToolTip(this->oneMethod, L"Update only the selected method");
    this->oneMethod->UseVisualStyleBackColor = true;
    this->oneMethod->CheckedChanged += gcnew System::EventHandler(this, &ImportNotationUI::oneMethod_CheckedChanged);
    // 
    // methodList
    // 
    this->methodList->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
    this->methodList->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
    this->methodList->FormattingEnabled = true;
    this->methodList->Location = System::Drawing::Point(260, 62);
    this->methodList->Name = L"methodList";
    this->methodList->Size = System::Drawing::Size(198, 21);
    this->methodList->TabIndex = 5;
    this->methodList->SelectedIndexChanged += gcnew System::EventHandler(this, &ImportNotationUI::methodList_SelectedIndexChanged);
    // 
    // openFileBtn
    // 
    this->openFileBtn->Location = System::Drawing::Point(386, 10);
    this->openFileBtn->Margin = System::Windows::Forms::Padding(0, 2, 2, 0);
    this->openFileBtn->Name = L"openFileBtn";
    this->openFileBtn->Size = System::Drawing::Size(75, 23);
    this->openFileBtn->TabIndex = 2;
    this->openFileBtn->Text = L"Choose file";
    this->toolTip->SetToolTip(this->openFileBtn, L"Choose the file to import");
    this->openFileBtn->UseVisualStyleBackColor = true;
    this->openFileBtn->Click += gcnew System::EventHandler(this, &ImportNotationUI::openFileBtn_Click);
    // 
    // downloadFileBtn
    // 
    this->downloadFileBtn->Location = System::Drawing::Point(185, 89);
    this->downloadFileBtn->Margin = System::Windows::Forms::Padding(0, 2, 2, 0);
    this->downloadFileBtn->Name = L"downloadFileBtn";
    this->downloadFileBtn->Size = System::Drawing::Size(75, 23);
    this->downloadFileBtn->TabIndex = 7;
    this->downloadFileBtn->Text = L"Download";
    this->downloadFileBtn->UseVisualStyleBackColor = true;
    this->downloadFileBtn->Click += gcnew System::EventHandler(this, &ImportNotationUI::downloadFileBtn_Click);
    // 
    // statusStrip1
    // 
    this->statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) { this->progressBar, this->noOfMethodsUpdated });
    this->statusStrip1->Location = System::Drawing::Point(0, 123);
    this->statusStrip1->Name = L"statusStrip1";
    this->statusStrip1->Size = System::Drawing::Size(470, 22);
    this->statusStrip1->TabIndex = 6;
    this->statusStrip1->Text = L"statusStrip1";
    // 
    // progressBar
    // 
    this->progressBar->Name = L"progressBar";
    this->progressBar->Size = System::Drawing::Size(100, 16);
    // 
    // noOfMethodsUpdated
    // 
    this->noOfMethodsUpdated->Name = L"noOfMethodsUpdated";
    this->noOfMethodsUpdated->Size = System::Drawing::Size(0, 17);
    // 
    // importMethodsWorker
    // 
    this->importMethodsWorker->WorkerReportsProgress = true;
    this->importMethodsWorker->WorkerSupportsCancellation = true;
    this->importMethodsWorker->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &ImportNotationUI::importMethodsWorker_DoWork);
    this->importMethodsWorker->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &ImportNotationUI::importMethodsWorker_ProgressChanged);
    this->importMethodsWorker->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &ImportNotationUI::importMethodsWorker_RunWorkerCompleted);
    // 
    // websiteBtn
    // 
    this->websiteBtn->Location = System::Drawing::Point(9, 89);
    this->websiteBtn->Name = L"websiteBtn";
    this->websiteBtn->Size = System::Drawing::Size(171, 23);
    this->websiteBtn->TabIndex = 6;
    this->websiteBtn->Text = L"Central Council Methods website";
    this->toolTip->SetToolTip(this->websiteBtn, L"Go to the Central Council method collections website");
    this->websiteBtn->UseVisualStyleBackColor = true;
    this->websiteBtn->Click += gcnew System::EventHandler(this, &ImportNotationUI::websiteBtn_Click);
    // 
    // downloadClient
    // 
    this->downloadClient->AllowReadStreamBuffering = false;
    this->downloadClient->AllowWriteStreamBuffering = false;
    this->downloadClient->BaseAddress = L"";
    this->downloadClient->CachePolicy = nullptr;
    this->downloadClient->Credentials = nullptr;
    this->downloadClient->Encoding = (cli::safe_cast<System::Text::Encoding^>(resources->GetObject(L"downloadClient.Encoding")));
    this->downloadClient->Headers = (cli::safe_cast<System::Net::WebHeaderCollection^>(resources->GetObject(L"downloadClient.Headers")));
    this->downloadClient->QueryString = (cli::safe_cast<System::Collections::Specialized::NameValueCollection^>(resources->GetObject(L"downloadClient.QueryString")));
    this->downloadClient->UseDefaultCredentials = false;
    this->downloadClient->DownloadFileCompleted += gcnew System::ComponentModel::AsyncCompletedEventHandler(this, &ImportNotationUI::downloadClient_DownloadFileCompleted);
    // 
    // ImportNotationUI
    // 
    this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
    this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
    this->CancelButton = this->closeBtn;
    this->ClientSize = System::Drawing::Size(470, 145);
    this->Controls->Add(this->websiteBtn);
    this->Controls->Add(this->statusStrip1);
    this->Controls->Add(this->methodList);
    this->Controls->Add(this->oneMethod);
    this->Controls->Add(this->allMethods);
    this->Controls->Add(this->importBtn);
    this->Controls->Add(this->openFileBtn);
    this->Controls->Add(this->downloadFileBtn);
    this->Controls->Add(this->fileNameEditor);
    this->Controls->Add(this->closeBtn);
    this->HelpButton = true;
    this->MaximizeBox = false;
    this->MinimizeBox = false;
    this->Name = L"ImportNotationUI";
    this->Text = L"Import place notation from libraries";
    this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &ImportNotationUI::ImportNotationUI_FormClosing);
    this->Load += gcnew System::EventHandler(this, &ImportNotationUI::ImportNotationUI_Load);
    this->statusStrip1->ResumeLayout(false);
    this->statusStrip1->PerformLayout();
    this->ResumeLayout(false);
    this->PerformLayout();

}


System::Void
ImportNotationUI::websiteBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    String^ fullUrl = KMethodCollectionWebsite;
    try
    {
        System::Diagnostics::Process::Start(fullUrl);
    }
    catch (Exception^)
    {

    }
}
