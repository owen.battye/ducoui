#include "ImportExportProgressWrapper.h"

using namespace DucoUI;

ImportExportProgressWrapper::ImportExportProgressWrapper(ImportExportProgressCallbackUI^ theUiCallback)
	: uiCallback(theUiCallback)
{

}

System::Void
ImportExportProgressWrapper::InitialisingImportExport(System::Boolean internalising, unsigned int versionNumber, size_t totalNumberOfObjects)
{
	uiCallback->InitialisingImportExport(internalising, versionNumber, totalNumberOfObjects);
}

System::Void
ImportExportProgressWrapper::ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
{
	uiCallback->ObjectProcessed(internalising, objectType, objectId, totalPercentage, numberInThisSubDatabase);
}

System::Void
ImportExportProgressWrapper::ImportExportComplete(System::Boolean internalising)
{
	uiCallback->ImportExportComplete(internalising);
}

System::Void
ImportExportProgressWrapper::ImportExportFailed(System::Boolean internalising, int errorCode)
{
    uiCallback->ImportExportFailed(internalising, errorCode);
}