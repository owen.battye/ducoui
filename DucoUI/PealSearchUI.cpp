#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"
#include "ProgressWrapper.h"
#include <DatabaseSearch.h>
#include "SearchUtils.h"
#include "PrintPealSearchUtil.h"

#include "PealSearchUI.h"

#include "SystemDefaultIcon.h"
#include "DatabaseGenUtils.h"
#include "SoundUtils.h"
#include "DucoUtils.h"
#include <Method.h>
#include "DucoUIUtils.h"
#include <SearchFieldNumberArgument.h>
#include <RingingDatabase.h>
#include <SearchFieldIdArgument.h>
#include <SearchFieldPealFeesArgument.h>
#include <SearchFieldBoolArgument.h>
#include <SearchFieldDateArgument.h>
#include <SearchValidObject.h>
#include <SearchFieldLinkedTowerIdArgument.h>
#include <SearchDuplicateObject.h>
#include <DucoConfiguration.h>
#include <SearchFieldRingerArgument.h>
#include <SearchFieldRingerPosition.h>
#include <SearchFieldNegativeArgument.h>
#include <SearchFieldTimeArgument.h>
#include <SearchFieldStringArgument.h>
#include "RingerSearchControl.h"
#include <PealLengthInfo.h>
#include "SingleObjectStatsUI.h"
#include <PealDatabase.h>
#include <StatisticFilters.h>
#include "SearchAndReplaceUI.h"
#include <Ringer.h>
#include <Ring.h>
#include <Tower.h>
#include <DatabaseExporter.h>
#include <DatabaseRtfExporter.h>
#include "DucoWindowState.h"
#include "OpenObjectCallback.h"
#include "PictureListControl.h"
#include "PictureControl.h"
#include "RingerList.h"
#include <Tower.h>
#include "PrintBandSummaryUtil.h"
#include <DownloadedPeal.h>
#include "PealUI.h"
#include <DatabaseSettings.h>
#include "DucoUILog.h"
#include <DucoEngineUtils.h>

using namespace Duco;
using namespace DucoUI;
using namespace std;
using namespace System;
using namespace System::Collections::Generic;
using namespace System::Drawing;
using namespace System::Drawing::Printing;
using namespace System::Windows::Forms;

PealSearchUI::PealSearchUI(DatabaseManager^ theDatabase, bool setStartInvalidSearch)
:    database(theDatabase), startInvalidSearch(setStartInvalidSearch), populating(false)
{
    startTowerId = KNoId;
    progressWrapper = new ImportExportProgressWrapper(this);
    updateWrapper = new ProgressCallbackWrapper(this);
    populateWrapper = new ProgressCallbackWrapper(this);
    ringerCache = RingerDisplayCache::CreateDisplayCacheWithRingers(database, false, true);
    conductorCache = RingerDisplayCache::CreateDisplayCacheWithConductors(database, true);
    allTowerNames = gcnew System::Collections::Generic::List<Int16>();
    allRingNames = gcnew System::Collections::Generic::List<Int16>();
    allMethodNames = gcnew System::Collections::Generic::List<Int16>();
    allMethodSeriesNames = gcnew System::Collections::Generic::List<Int16>();
    allCompositionNames = gcnew System::Collections::Generic::List<Int16>();
    allAssociationNames = gcnew System::Collections::Generic::List<Int16>();
    InitializeComponent();
    foundPealIds = gcnew System::Collections::Generic::List<System::UInt64>();
    withdrawnPealStyle = (gcnew System::Windows::Forms::DataGridViewCellStyle(dataGridView->DefaultCellStyle));
    withdrawnPealStyle->ForeColor = System::Drawing::SystemColors::GrayText;

    advBellNo->Items->Add("Any bell");
    advBellNo->Items->Add("Treble");
    advBellNo->Items->Add("2");
    advBellNo->Items->Add("3");
    advBellNo->Items->Add("4");
    advBellNo->Items->Add("5");
    advBellNo->Items->Add("6");
    advBellNo->Items->Add("N-5");
    advBellNo->Items->Add("N-4");
    advBellNo->Items->Add("N-3");
    advBellNo->Items->Add("N-2");
    advBellNo->Items->Add("N-1");
    advBellNo->Items->Add("Tenor");
}

PealSearchUI::!PealSearchUI()
{
    database->RemoveObserver(this);
    delete progressWrapper;
    delete updateWrapper;
    delete populateWrapper;
}

PealSearchUI::~PealSearchUI()
{
    this->!PealSearchUI();
    if (components)
    {
        delete components;
    }
}

System::Void
PealSearchUI::PealSearchUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    populating = true;
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    ringerCache->PopulateControl(ringerSelector, false);
    ringerCache->PopulateControl(advRingerName, false);
    conductorCache->PopulateControl(conductorSelector, false);
    DatabaseGenUtils::GenerateMethodOptions(allMethodNames, methodSelector, database, KNoId, KNoId, false, true, true);
    DatabaseGenUtils::GenerateMethodSeriesOptions(methodSeriesSelector, allMethodSeriesNames, database, KNoId, KNoId, false);
    DatabaseGenUtils::GenerateTowerOptions(towerSelector, allTowerNames, database, Duco::ObjectId(startTowerId), false, false, true);
    DatabaseGenUtils::GenerateCompositionOptions(compositionSelector, allCompositionNames, database, KNoId, KNoId, false);
    DatabaseGenUtils::GenerateAssociationOptions(associationEditor, allAssociationNames, database, KNoId, false);
    showAssociationColumn->Checked = database->Settings().ShowAssociationColumn();
    showComposerColumn->Checked = database->Settings().ShowComposerColumn();
    showConductorColumn->Checked = database->Settings().ShowConductorColumn();
    showRingColumn->Checked = database->Settings().ShowRingColumn();
    showBellRungColumn->Checked = database->Settings().ShowBellRungColumn();
    showReferencesColumns->Checked = database->Settings().ShowReferencesColumn();
    database->AddObserver(this);

    populating = false;
    if (startInvalidSearch)
    {
        invalid->Checked = true;
        searchBtn_Click(sender, e);
    }
}

System::Void
PealSearchUI::ClosingEvent( Object^ /*sender*/, System::ComponentModel::CancelEventArgs^ e )
{
    if (searchThread->IsBusy)
    {
        searchThread->CancelAsync();
        e->Cancel = true;
    }
}

void
PealSearchUI::Show(System::Windows::Forms::Form^ parent, System::Collections::Generic::List<UInt64>^ newPealIds)
{
    if (!searchThread->IsBusy)
    {
        startInvalidSearch = false;
        foundPealIds = newPealIds;
        this->MdiParent = parent;
        this->Show();
        searchThread->RunWorkerAsync();
    }
}

void
PealSearchUI::SearchPealsInTower(System::Windows::Forms::Form^ parent, System::UInt64 towerId)
{
    if (!searchThread->IsBusy)
    {
        startInvalidSearch = false;
        this->MdiParent = parent;
        this->startTowerId = towerId;
        this->Show();
        this->searchBtn->PerformClick();
    }
}

void
PealSearchUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    switch (type)
    {
    case TObjectType::ETower:
        switch (eventId)
        {
        case EAdded:
            DatabaseGenUtils::AddTowerOption(towerSelector, id, allTowerNames, database);
            break;
        case EUpdated:
            DatabaseGenUtils::UpdateTowerOption(towerSelector, id, allTowerNames, database);
            break;
        case EDeleted:
            DatabaseGenUtils::RemoveTowerOption(towerSelector, id, allTowerNames);
            break;
        }
        break;

    case TObjectType::EMethod:
        switch (eventId)
        {
        case EAdded:
            DatabaseGenUtils::AddMethodOption(id, allMethodNames, -1, database, methodSelector);
            break;
        case EUpdated:
            DatabaseGenUtils::UpdateMethodOption(id, allMethodNames, -1, database, methodSelector);
            break;
        case EDeleted:
            DatabaseGenUtils::RemoveMethodOption(id, allMethodNames, -1, methodSelector);
            break;
        }
        break;

    case TObjectType::EMethodSeries:
        switch (eventId)
        {
        case EAdded:
            DatabaseGenUtils::AddMethodSeriesOption(id, allMethodSeriesNames, -1, database, methodSeriesSelector);
            break;
        case EUpdated:
            DatabaseGenUtils::UpdateMethodSeriesOption(id, allMethodSeriesNames, -1, database, methodSeriesSelector);
            break;
        case EDeleted:
            DatabaseGenUtils::RemoveMethodSeriesOption(id, allMethodSeriesNames, -1, methodSeriesSelector);
            break;
        }
        break;

    case TObjectType::EComposition:
        switch (eventId)
        {
        case EAdded:
            DatabaseGenUtils::AddCompositionOption(id, allCompositionNames, -1, database, compositionSelector);
            break;
        case EUpdated:
            DatabaseGenUtils::UpdateCompositionOption(id, allCompositionNames, -1, database, compositionSelector);
            break;
        case EDeleted:
            DatabaseGenUtils::RemoveCompositionOption(id, allCompositionNames, -1, compositionSelector);
            break;
        }
        break;

    case TObjectType::EPeal: // Not required.
    case TObjectType::ERinger: // Not required.
    default:
        break;
    }
}

System::Void
PealSearchUI::searchBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (searchThread->IsBusy)
        return;

    updateBtn->Enabled = false;
    clearbtn->Enabled = false;
    statsBtn->Enabled = false;
    printBtn->Enabled = false;
    saveAsBtn->Enabled = false;
    searchBtn->Enabled = false;
    foundPealIds->Clear();

    dataGridView->RowCount = 0;
    dataGridView->Rows->Clear();

    resultCount->Text = "";
    withdrawnCount = 0;
    DatabaseSearch* search = new DatabaseSearch(database->Database());
    if (CreateSearchParameters(*search))
    {
        errorFound = false;
        rowCountColumn->Visible = false;
        SearchUIArgs^ args = gcnew SearchUIArgs();
        args->search = search;
        searchThread->RunWorkerAsync(args);
        searchBtn->Enabled = false;
    }
    else
    {
        SoundUtils::PlayErrorSound();
        delete search;
        updateBtn->Enabled = true;
        clearbtn->Enabled = true;
        statsBtn->Enabled = true;
        printBtn->Enabled = true;
        saveAsBtn->Enabled = true;
        searchBtn->Enabled = true;
    }
}

bool
PealSearchUI::CreateSearchParameters(Duco::DatabaseSearch& search)
{
    try
    {
        unsigned int noOfChanges = 0;
        if (DucoUtils::ConvertTextToNumber(changesEditor->Text, noOfChanges))
        {
            TSearchType searchtype (EEqualTo);
            if (changesMoreThan->Checked)
                searchtype = EMoreThan;
            else if (changesLessThan->Checked)
                searchtype = ELessThan;
            TSearchFieldNumberArgument* newNumber = new TSearchFieldNumberArgument(Duco::EChanges, noOfChanges, searchtype);
            search.AddArgument(newNumber);
        }

        unsigned int noOfBells = 0;
        if (DucoUtils::ConvertTextToNumber(bellsEditor->Text, noOfBells))
        {
            TSearchType searchtype (EEqualTo);
            if (bellsMoreThan->Checked)
                searchtype = EMoreThan;
            else if (bellsLessThan->Checked)
                searchtype = ELessThan;
            TSearchFieldNumberArgument* newNumber = new TSearchFieldNumberArgument(Duco::ENoOfBells, noOfBells, searchtype);
            search.AddArgument(newNumber);
        }

        if (methodSelector->SelectedIndex != -1)
        {
            Duco::ObjectId methodId = DatabaseGenUtils::FindId(allMethodNames, methodSelector->SelectedIndex);
            TSearchFieldIdArgument* newId = new TSearchFieldIdArgument(splicedMethods->Checked ? Duco::EMethodIdIncludingInSplicedSeries : Duco::EMethodId , methodId);
            search.AddArgument(newId);
        }
        else
        {
            if (splicedMethods->Checked)
            {
                std::string tempMethodName;
                DucoUtils::ConvertString(methodSelector->Text, tempMethodName);
                SearchUtils::AddStringSearchArgument(search, methodSelector->Text, Duco::EFullMethodName, Duco::EMethodMultiStr, false);
            }
            else
            {
                SearchUtils::AddStringSearchArgument(search, methodSelector->Text, Duco::EFullMethodName, false);
            }
        }
        if (splicedMethod->Checked)
        {
            TSearchFieldBoolArgument* newId = new TSearchFieldBoolArgument(Duco::EMethodIsSpliced);
            search.AddArgument(newId);
        }
        if (dualOrder->Checked)
        {
            TSearchFieldBoolArgument* newDualOrder = new TSearchFieldBoolArgument(Duco::EMethodDualOrder, true);
            search.AddArgument(newDualOrder);
        }
        if (methodSeriesSelector->SelectedIndex == -1)
        {
            SearchUtils::AddStringSearchArgument(search, methodSeriesSelector->Text, Duco::EMethodSeriesStr, false);
        }
        else
        {
            Duco::ObjectId methodSeriesId = DatabaseGenUtils::FindId(allMethodSeriesNames, methodSeriesSelector->SelectedIndex);
            TSearchFieldIdArgument* newId = new TSearchFieldIdArgument(Duco::EMethodSeriesId , methodSeriesId);
            search.AddArgument(newId);
        }
        if (noOfMethodsInSeries->Value > 0)
        {
            unsigned int noOfSeries = Convert::ToInt16(noOfMethodsInSeries->Value);
            TSearchFieldNumberArgument* newSeriesCount = new TSearchFieldNumberArgument(Duco::EMethodSeriesCount, noOfSeries);
            search.AddArgument(newSeriesCount);
        }
        else if (containsMethodSeries->Checked)
        {
            TSearchFieldBoolArgument* newSeriesPresent = new TSearchFieldBoolArgument(Duco::EMethodSeriesPresent);
            search.AddArgument(newSeriesPresent);
        }
        if (compositionSelector->SelectedIndex == -1)
        {
            SearchUtils::AddStringSearchArgument(search, compositionSelector->Text, Duco::ECompositionStr, false);
        }
        else
        {
            Duco::ObjectId compositionId = DatabaseGenUtils::FindId(allCompositionNames, compositionSelector->SelectedIndex);
            TSearchFieldIdArgument* newId = new TSearchFieldIdArgument(Duco::ECompositionId, compositionId);
            search.AddArgument(newId);
        }
        if (withdrawnOnlyBtn->Checked)
        {
            TSearchFieldBoolArgument* newWithdrawn = new TSearchFieldBoolArgument(Duco::EWithdrawn, true);
            search.AddArgument(newWithdrawn);
        }
        else if (validOnlyBtn->Checked)
        {
            TSearchFieldBoolArgument* newWithdrawn = new TSearchFieldBoolArgument(Duco::EWithdrawn, false);
            search.AddArgument(newWithdrawn);
        }

        if (towerSelector->SelectedIndex != -1)
        {
            Duco::ObjectId towerId = DatabaseGenUtils::FindId(allTowerNames, towerSelector->SelectedIndex);
            if (towerId.ValidId())
            {
                if (linkedTowers->Checked && linkedTowers->Enabled)
                {
                    TSearchFieldLinkedTowerIdArgument* linkedTowerId = new TSearchFieldLinkedTowerIdArgument(towerId);
                    search.AddArgument(linkedTowerId);
                }
                else
                {
                    TSearchFieldIdArgument* newId = new TSearchFieldIdArgument(Duco::ETowerId, towerId);
                    search.AddArgument(newId);
                    if (ringSelector->SelectedIndex != -1)
                    {
                        Duco::ObjectId ringId = DatabaseGenUtils::FindId(allRingNames, ringSelector->SelectedIndex);
                        if (ringId.ValidId())
                        {
                            TSearchFieldIdArgument* ringIdArg = new TSearchFieldIdArgument(Duco::ERingId, ringId);
                            search.AddArgument(ringIdArg);
                        }
                    }
                }
            }
        }
        else
        {
            SearchUtils::AddStringSearchArgument(search, towerSelector->Text, Duco::ETowerName, false);
        }

        GenerateRingerArguments(search, ringerSelector, ringerEditor, ERingerId, ERingerName, excludingRinger->Checked);
        if (withStrapper->Checked)
        {
            TSearchFieldBoolArgument* newStr = new TSearchFieldBoolArgument(Duco::EStrapper);
            search.AddArgument(newStr);
        }
        else if (withoutStrapper->Checked)
        {
            TSearchFieldBoolArgument* newStr = new TSearchFieldBoolArgument(Duco::EStrapper, false);
            search.AddArgument(newStr);
        }
        GenerateConductorArguments(search, conductorSelector, EConductorId, EConductorName);
        if (doubleHanded->Checked)
        {
            TSearchFieldBoolArgument* newDoubleHandedArg = new TSearchFieldBoolArgument(Duco::EDoubleHanded);
            search.AddArgument(newDoubleHandedArg);
        }

        SearchUtils::AddStringSearchArgument(search, composerEditor->Text, Duco::EComposer, false);
        if (emptyFootnotes->Checked)
        {
            TSearchFieldBoolArgument* newArg = new TSearchFieldBoolArgument(Duco::EBlankFootnote);
            search.AddArgument(newArg);
        }
        else if (nonEmptyFootnotes->Checked)
        {
            TSearchFieldBoolArgument* newArg = new TSearchFieldBoolArgument(Duco::ENonBlankFootnote);
            search.AddArgument(newArg);
        }
        else
        {
            SearchUtils::AddStringSearchArgument(search, footnotesEditor->Text, Duco::EFootnotes, false);
        }

        if (pealTimeEditor->Text->Length > 0)
        {
            TSearchType timeSearchType (EEqualTo);
            if (timeMoreThan->Checked)
                timeSearchType = EMoreThan;
            else if (timeLessThan->Checked)
                timeSearchType = ELessThan;

            std::wstring tmp;
            DucoUtils::ConvertString(pealTimeEditor->Text, tmp);
            PerformanceTime pealTime (tmp);
            TSearchFieldTimeArgument* newTime = new TSearchFieldTimeArgument(Duco::EPealTime, pealTime, timeSearchType);
            search.AddArgument(newTime);
        }

        if (!dateOff->Checked)
        {
            if (dateBetween->Checked)
            {
                PerformanceDate beforePealDate = DucoUtils::ConvertDate(pealDate2->Value);
                TSearchFieldDateArgument* newbeforeDate = new TSearchFieldDateArgument(Duco::EPealDate, beforePealDate, ELessThanOrEqualTo);
                search.AddArgument(newbeforeDate);
                PerformanceDate afterPealDate = DucoUtils::ConvertDate(pealDate1->Value);
                TSearchFieldDateArgument* newAfterDate = new TSearchFieldDateArgument(Duco::EPealDate, afterPealDate, EMoreThanOrEqualTo);
                search.AddArgument(newAfterDate);
            }
            else if (dateBefore->Checked)
            {
                PerformanceDate pealDate = DucoUtils::ConvertDate(pealDate1->Value);
                TSearchFieldDateArgument* newDate = new TSearchFieldDateArgument(Duco::EPealDate, pealDate, ELessThan);
                search.AddArgument(newDate);
            }
            else if (dateOn->Checked)
            {
                PerformanceDate pealDate = DucoUtils::ConvertDate(pealDate1->Value);
                TSearchFieldDateArgument* newDate = new TSearchFieldDateArgument(Duco::EPealDate, pealDate, EEqualTo);
                search.AddArgument(newDate);
            }
            else if (dateAfter->Checked)
            {
                PerformanceDate pealDate = DucoUtils::ConvertDate(pealDate1->Value);
                TSearchFieldDateArgument* newDate = new TSearchFieldDateArgument(Duco::EPealDate, pealDate, EMoreThan);
                search.AddArgument(newDate);
            }
        }
        if (blankAssociation->Checked)
        {
            TSearchFieldStringArgument* newStr = new TSearchFieldStringArgument(Duco::EAssociationName, L"", false, false);
            search.AddArgument(newStr);
        }
        else if (associationEditor->SelectedIndex != -1 && associationEditor->Text->Length > 0)
        {
            Duco::ObjectId associationId = DatabaseGenUtils::FindId(allAssociationNames, associationEditor->SelectedIndex);
            if (associationId.ValidId())
            {
                TSearchFieldIdArgument* newId = new TSearchFieldIdArgument(Duco::EAssociationId, associationId);
                search.AddArgument(newId);
            }
        }
        else
        {
            SearchUtils::AddStringSearchArgument(search, associationEditor->Text, Duco::EAssociationName, false);
        }

        if (towerBellSelector->Checked)
        {
            TSearchFieldBoolArgument* newStr = new TSearchFieldBoolArgument(Duco::ETowerBells, true);
            search.AddArgument(newStr);
        }
        else if (handbellSelector->Checked)
        {
            TSearchFieldBoolArgument* newStr = new TSearchFieldBoolArgument(Duco::ETowerBells, false);
            search.AddArgument(newStr);
        }
        if (tenorWeightEditor->TextLength > 0)
        {
            SearchUtils::AddStringSearchArgument(search, tenorWeightEditor->Text, Duco::ETenorWeight, false);
        }
        if (tenorKeyEditor->TextLength > 0)
        {
            SearchUtils::AddStringSearchArgument(search, tenorKeyEditor->Text, Duco::ETenorKey, false);
        }

        if (emptyRWRef->Checked)
        {
            TSearchFieldStringArgument* newStr = new TSearchFieldStringArgument(Duco::EBlankRWReference, L"", false, false);
            search.AddArgument(newStr);
        }
        else if (nonEmptyRWRef->Checked)
        {
            TSearchFieldStringArgument* newStr = new TSearchFieldStringArgument(Duco::EBlankRWReference, L"", false, true);
            search.AddArgument(newStr);
        }
        else
            SearchUtils::AddStringSearchArgument(search, rwRef->Text, Duco::EBlankRWReference, false);

        if (invalidRWReference->Checked)
        {
            TSearchFieldBoolArgument* newStr = new TSearchFieldBoolArgument(Duco::EValidRWReference, false);
            search.AddArgument(newStr);
        }
        if (fullValidRWReference->Checked)
        {
            TSearchFieldBoolArgument* newStr = new TSearchFieldBoolArgument(Duco::EValidRWReference);
            search.AddArgument(newStr);
        }
        if (blankBellBoardRef->Checked)
        {
            TSearchFieldStringArgument* newStr = new TSearchFieldStringArgument(Duco::EBellBoardReference, L"", false, false);
            search.AddArgument(newStr);
        }
        else if (nonBlankBellBoardRef->Checked)
        {
            TSearchFieldStringArgument* newStr = new TSearchFieldStringArgument(Duco::EBellBoardReference, L"", false, true);
            search.AddArgument(newStr);
        }
        else
            SearchUtils::AddStringSearchArgument(search, bellBoardRef->Text, Duco::EBellBoardReference, false);

        if (nonBlankNotes->Checked)
        {
            TSearchFieldStringArgument* newNotesStr = new TSearchFieldStringArgument(Duco::ENotes, L"", false, false);
            TSearchFieldNegativeArgument* newNotNotesStr = new TSearchFieldNegativeArgument(newNotesStr);
            search.AddArgument(newNotNotesStr);
        }
        else
            SearchUtils::AddStringSearchArgument(search, notes->Text, Duco::ENotes, false);

        if (notable->Checked)
        {
            TSearchFieldBoolArgument* newNotableArg = new TSearchFieldBoolArgument(Duco::ENotablePerformance);
            search.AddArgument(newNotableArg);
        }
        if (antiClockwise->Checked)
        {
            TSearchFieldBoolArgument* newAntiClockwiseArg = new TSearchFieldBoolArgument(Duco::EAntiClockwise);
            search.AddArgument(newAntiClockwiseArg);
        }
        if (simulatedSound->Checked)
        {
            TSearchFieldBoolArgument* newArg = new TSearchFieldBoolArgument(Duco::ESimulatedSound);
            search.AddArgument(newArg);
        }

        if (unpaidPealFees->Checked)
        {
            TSearchFieldBoolArgument* newUnpaidFees = new TSearchFieldBoolArgument(Duco::EUnpaidFees, false);
            search.AddArgument(newUnpaidFees);
        }

        if (inconsistantPealFees->Checked && inconsistantPealFees->Enabled)
        {
            Duco::ObjectId associationId = DatabaseGenUtils::FindId(allAssociationNames, associationEditor->SelectedIndex);
            TSearchFieldPealFeesArgument* newConsistantFees = new TSearchFieldPealFeesArgument(associationId);
            search.AddArgument(newConsistantFees);
        }
        if (hasPicture->Checked)
        {
            TSearchFieldBoolArgument* newPictureState = new TSearchFieldBoolArgument(EContainsPicture);
            search.AddArgument(newPictureState);
        }
        else if (doesNotHavePicture->Checked)
        {
            TSearchFieldBoolArgument* newPictureState = new TSearchFieldBoolArgument(EContainsPicture, false);
            search.AddArgument(newPictureState);
        }
        if (invalid->Checked)  // Duplicate and invalid last - as they're slowest 
        {
            TSearchValidObject* newInvalidField = new TSearchValidObject(database->Config().IncludeWarningsInValidation());
            search.AddValidationArgument(newInvalidField);
        }
        else if (duplicate->Checked) // Duplicate and invalid last - as they're slowest 
        {
            TSearchDuplicateObject* newDuplicateField = new TSearchDuplicateObject(true);
            search.AddValidationArgument(newDuplicateField);
        }

    }
    catch (Exception^)
    {
        SoundUtils::PlayErrorSound();
        return false;
    }
    return true;
}

System::Void
PealSearchUI::saveAsBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    SaveFileDialog^ saveFileDialog = gcnew SaveFileDialog();
    saveFileDialog->Filter = "Duco files (*.duc)|*.duc|Rich text files (*.rtf)|*.rtf|Bellboard xml (*.xml)|*.xml";
    saveFileDialog->RestoreDirectory = true;

    if (saveFileDialog->ShowDialog() == System::Windows::Forms::DialogResult::OK)
    {
        try
        {
            String^ fileName = saveFileDialog->FileName;
            saveFileDialog->OpenFile()->Close();
            std::string fileNameStr;
            DucoUtils::ConvertString(fileName, fileNameStr);
            std::set<ObjectId> ids;
            DucoUtils::Convert(foundPealIds, ids);
            if (saveFileDialog->FilterIndex == 1)
            {
                DatabaseExporter exporter(database->Database());
                exporter.AddAllObjectsForPeals(ids);
                exporter.Export(fileNameStr.c_str(), progressWrapper);
            }
            else if (saveFileDialog->FilterIndex == 2)
            {
                DatabaseRtfExporter exporter(database->Database(), ids);
                exporter.Export(fileNameStr.c_str(), progressWrapper);
            }
            else if (saveFileDialog->FilterIndex == 3)
            {
                database->Database().ExternaliseToBellboardXml(fileNameStr.c_str(), ids, progressWrapper);
            }
        }
        catch (Exception^ ex)
        {
            SoundUtils::PlayErrorSound();
            MessageBox::Show(this, "Error: Could not open file: " + ex->Message);
        }
    }
}

System::Void
PealSearchUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void
PealSearchUI::updateBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    std::set<ObjectId> ids;
    DucoUtils::Convert(foundPealIds, ids);
    SearchAndReplaceUI^ uiForm = gcnew SearchAndReplaceUI(database, updateWrapper, ids, TObjectType::EPeal);
    uiForm->ShowDialog();
}

System::Void
PealSearchUI::clearbtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (!searchThread->IsBusy)
    {
        dataGridView->Rows->Clear();
        resultCount->Text = "";
        withdrawnCount = 0;

        changesEditor->Text = "";
        changesEqualTo->Checked = true;
        ringerSelector->ClearSelected();
        CheckedListBox::CheckedIndexCollection^ checkedItems = ringerSelector->CheckedIndices;
        System::Collections::IEnumerator^ it = checkedItems->GetEnumerator();
        while (it->MoveNext())
        {
            Int32 indexChecked =  *safe_cast<Int32^>(it->Current);
            ringerSelector->SetItemChecked(indexChecked, false);
        }
        excludingRinger->Checked = false;
        ringerEditor->Text = "";
        towerSelector->Text = "";
        towerSelector->SelectedIndex = -1;
        compositionSelector->Text = "";
        compositionSelector->SelectedIndex = -1;
        bellsEqualTo->Checked = true;
        bellsEditor->Text = "";
        conductorSelector->Text = "";
        conductorSelector->SelectedIndex = -1;
        pealTimeEditor->Text = "";
        timeEqualTo->Checked = true;
        dateOff->Checked = true;
        bothStrapperOptions->Checked = true;
        asStrapper->Checked = false;
        conducted->Checked = true;
        composerEditor->Text = "";
        methodSelector->Text = "";
        methodSelector->SelectedIndex = -1;
        methodSeriesSelector->Text = "";
        methodSeriesSelector->SelectedIndex = -1;
        noOfMethodsInSeries->Value = 0;
        containsMethodSeries->Checked = false;
        footnotesEditor->Text = "";
        emptyFootnotes->Checked = false;
        nonEmptyFootnotes->Checked = false;
        associationEditor->Text = "";
        associationEditor->SelectedIndex = -1;
        dualOrder->Checked = false;
        blankAssociation->Checked = false;
        allPealsSelector->Checked = true;
        splicedMethods->Checked = false;
        splicedMethod->Checked = false;
        unpaidPealFees->Checked = false;
        inconsistantPealFees->Checked = false;

        blankBellBoardRef->Checked = false;
        nonBlankBellBoardRef->Checked = false;
        nonEmptyRWRef->Checked = false;
        emptyRWRef->Checked = false;
        rwRef->Text = "";
        bellBoardRef->Text = "";
        notes->Text = "";
        nonBlankNotes->Checked = false;
        notable->Checked = false;
        antiClockwise->Checked = false;
        simulatedSound->Checked = false;
        linkedTowers->Checked = false;
        bothValidAndWithdrawnBtn->Checked = true;
        invalid->Checked = false;
        duplicate->Checked = false;
        doubleHanded->Checked = false;
        withAndWithoutPictures->Checked = true;

        advRingerName->Text = "";
        advRingerName->SelectedIndex = -1;
        advBellNo->Text = "";
        ringerAdvSearchParams->Controls->Clear();
    }
}

System::Void
PealSearchUI::statsBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (!searchThread->IsBusy)
    {
        SingleObjectStatsUI^ statsUI = gcnew SingleObjectStatsUI(database, foundPealIds, TObjectType::EPeal);
        statsUI->MdiParent = this->MdiParent;
        statsUI->Show();
    }
}

System::Void
PealSearchUI::printBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (!searchThread->IsBusy)
    {
        DucoUIUtils::Print(this, sender, gcnew System::Drawing::Printing::PrintPageEventHandler( this, &PealSearchUI::printPeals ), database->PerformanceString(false) + "s summary", database->Database().Settings().UsePrintPreview(), true);
    }
}

System::Void
PealSearchUI::printPeals(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args)
{
    if (!searchThread->IsBusy)
    {
        if (printUtil == nullptr)
        {
            printUtil = gcnew PrintPealSearchUtil(database, args);
            printUtil->SetObjects(dataGridView);
        }

        PrintDocument^ printDoc = static_cast<PrintDocument^>(sender);
        printUtil->printObject(args, printDoc->PrinterSettings);
        if (!args->HasMorePages)
        {
            printUtil = nullptr;
        }
    }
}

System::Void
PealSearchUI::towerSelector_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (!searchThread->IsBusy)
    {
        if (towerSelector->SelectedIndex == -1)
        {
            ringSelector->Enabled = false;
        }
        else
        { 
            Duco::ObjectId towerId = DatabaseGenUtils::FindId(allTowerNames,towerSelector->SelectedIndex);
            if (towerId.ValidId())
            {
                DatabaseGenUtils::GenerateRingOptions(ringSelector, nullptr, allRingNames, database, towerId, -1, -1, true);
                ringSelector->Enabled = true;
                linkedTowers->Checked = true;
            }
        }
    }
}

System::Void
PealSearchUI::ringSelector_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e)
{
    Duco::ObjectId ringId = DatabaseGenUtils::FindId(allRingNames, ringSelector->SelectedIndex);
    linkedTowers->Enabled = !ringId.ValidId();
}

System::Void
PealSearchUI::towerSelector_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (!searchThread->IsBusy && towerSelector->SelectedIndex == -1)
    {
        ringSelector->Text = "";
        ringSelector->Enabled = false;
    }
}

System::Void
PealSearchUI::GenerateRingerArguments(DatabaseSearch& search, System::Windows::Forms::CheckedListBox^ control, System::Windows::Forms::TextBox^ editorControl, TSearchFieldId idField, TSearchFieldId nameField, bool excluding)
{
    if (asStrapper->Checked)
    {
        if (control->CheckedIndices->Count > 0)
        {
            RingerItem^ ringer = (RingerItem^)ringerSelector->CheckedItems[0];
            Duco::ObjectId ringerId (ringer->Id());
            if (ringerId.ValidId())
            {
                TSearchFieldIdArgument* newStrapperIdArg = new TSearchFieldIdArgument(EStrapper, ringerId, excluding ? ENotEqualTo : EEqualTo);
                search.AddArgument(newStrapperIdArg);
            }
        }
        else
        {
            SearchUtils::AddStringSearchArgument(search, editorControl->Text, Duco::EStrapper, excluding);
        }

    }
    else
    {
        System::Collections::IEnumerator^ myEnumerator = control->CheckedItems->GetEnumerator();
        while (myEnumerator->MoveNext() != false)
        {
            RingerItem^ ringerIndex =  safe_cast<RingerItem^>(myEnumerator->Current);
            Duco::ObjectId ringerId = ringerIndex->Id();
            if (ringerId.ValidId())
            {
                TSearchFieldIdArgument* newRingerId = new TSearchFieldIdArgument(idField, ringerId, excluding ? ENotEqualTo : EEqualTo);
                search.AddArgument(newRingerId);
            }
        }
        SearchUtils::AddStringSearchArgument(search, editorControl->Text, nameField, excluding);
    }
    System::Collections::IEnumerator^ myEnumerator2 = ringerAdvSearchParams->Controls->GetEnumerator();
    while (myEnumerator2->MoveNext() != false)
    {
        RingerSearchControl^ ringer =  safe_cast<RingerSearchControl^>(myEnumerator2->Current);
        if (!ringer->ShouldBeRemoved())
        {
            ringer->AddSearchControl(search);
        }
    }
    associationColumn->Visible = database->Settings().ShowAssociationColumn();
    bellRungColumn->Visible = database->Settings().ShowBellRungColumn();
    composerColumn->Visible = database->Settings().ShowComposerColumn();
    conductorColumn->Visible = database->Settings().ShowConductorColumn();
    ringColumn->Visible = database->Settings().ShowRingColumn();
    ringingWorldColumn->Visible = database->Settings().ShowReferencesColumn();
    bellboardColumn->Visible = database->Settings().ShowReferencesColumn();
}

System::Void
PealSearchUI::GenerateConductorArguments(DatabaseSearch& search, System::Windows::Forms::ComboBox^ control, TSearchFieldId idField, TSearchFieldId nameField)
{
    if (control->SelectedIndex != -1)
    {
        RingerItem^ ringer = (RingerItem^)control->SelectedItem;
        Duco::ObjectId ringerId = ringer->Id();
        if (ringerId.ValidId())
        {
            TSearchFieldIdArgument* newRingerId = new TSearchFieldIdArgument(idField, ringerId);
            search.AddArgument(newRingerId);
        }
    }
    else
    {
        SearchUtils::AddStringSearchArgument(search, control->Text, nameField, false);
    }
    if (silentConductor->Checked)
    {
        TSearchFieldNumberArgument* silentArg = new TSearchFieldNumberArgument(EConductorType, ESilentAndNonConducted);
        search.AddArgument(silentArg);
    }
    else if (jointConductor->Checked)
    {
        TSearchFieldNumberArgument* jointConductorArg = new TSearchFieldNumberArgument(EConductorType, EJointlyConducted);
        search.AddArgument(jointConductorArg);
    }
    else if (control->SelectedIndex > 0 || control->Text->Length > 0)
    {
        TSearchFieldNumberArgument* conductedNormallyArg = new TSearchFieldNumberArgument(EConductorType, ESingleConductor);
        search.AddArgument(conductedNormallyArg);
    }
}

System::Void
PealSearchUI::updateThread_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
}

System::Void
PealSearchUI::updateThread_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    progressBar->Value = e->ProgressPercentage;
}

System::Void
PealSearchUI::updateThread_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    progressBar->Value = 0;
    SoundUtils::PlayFinishedSound();
}

System::Void
PealSearchUI::searchThread_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
    System::ComponentModel::BackgroundWorker^ bgworker = static_cast<System::ComponentModel::BackgroundWorker^>(sender);
    if (e->Argument != nullptr)
    {
        SearchUIArgs^ args = static_cast<SearchUIArgs^>(e->Argument);
        std::set<ObjectId> ids;
        args->search->Search(*populateWrapper, ids, TObjectType::EPeal);
        delete args->search;
        DucoUtils::Convert(ids, foundPealIds);

        StatisticFilters filters(database->Database());
        filters.SetExcludeValid(true);
        filters.SetIncludeWithdrawn(true);

        withdrawnCount = (unsigned int)database->Database().PealsDatabase().PerformanceInfo(filters, ids).TotalPeals();
    }
}

System::Void
PealSearchUI::searchThread_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    progressBar->Value = e->ProgressPercentage;
}

System::Void
PealSearchUI::searchThread_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    updateBtn->Enabled = true;
    clearbtn->Enabled = true;
    statsBtn->Enabled = true;
    printBtn->Enabled = true;
    saveAsBtn->Enabled = true;
    searchBtn->Enabled = true;
    progressBar->Value = 0;
    resultCount->Text = Convert::ToString(foundPealIds->Count - withdrawnCount);
    if (withdrawnCount > 0)
    {
        resultCount->Text += " (" + Convert::ToString(withdrawnCount) + " withdrawn)";
    }
    dataGridView->RowCount = foundPealIds->Count;
    if (foundPealIds->Count == 0)
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
PealSearchUI::dataGridView_CellValueNeeded(System::Object^  sender, System::Windows::Forms::DataGridViewCellValueEventArgs^  e)
{
    if (e->RowIndex >= foundPealIds->Count)
    {
        DUCOUILOGDEBUG("PealSearchUI::dataGridView1_CellValueNeeded - OUT OF RANGE");
        return;
    }

    bool errorFoundChanged = false;
    Duco::ObjectId pealIdToFind(foundPealIds[e->RowIndex]);
    DUCOUILOGDEBUG("PealSearchUI::dataGridView1_CellValueNeeded Row: " + Convert::ToString(e->RowIndex) + ", Column: " + e->ColumnIndex.ToString());
    Duco::Peal foundPeal;
    if (database->FindPeal(pealIdToFind, foundPeal, true))
    {
        if (foundPeal.ErrorString(database->Database().Settings(), database->Config().IncludeWarningsInValidation()).length() > 0)
        {
            if (!errorFound)
            {
                errorFound = true;
                errorFoundChanged = true;
            }
        }

        switch (e->ColumnIndex)
        {
        default:
            break;
        case 0:
        {
            e->Value = Convert::ToString(e->RowIndex + 1);
        }
        break;

        case 1: // Id column;
        {
            e->Value = Convert::ToString(pealIdToFind.Id());
            if (pealIdToFind.Id() != (e->RowIndex+1) && !rowCountColumn->Visible)
            {
                rowCountColumn->Visible = true;
            }
        }
        break;

        case 2: // DateColumn;
            e->Value = DucoUtils::ConvertString(foundPeal.Date().Str());
            break;

        case 3: //Association Column;
            e->Value = DucoUtils::ConvertString(foundPeal.AssociationName(database->Database()));
            break;

        case 4: // Changes
            e->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(foundPeal.NoOfChanges(), true));
            break;

        case 5: // Method
        {
            Method theMethod;
            if (database->FindMethod(foundPeal.MethodId(), theMethod, false))
                e->Value = DucoUtils::ConvertString(theMethod.FullName(database->Database()));
        }
        break;

        case 6: //Tower Column;
        {
            Duco::Tower theTower;
            if (database->FindTower(foundPeal.TowerId(), theTower, false))
            {
                e->Value = DucoUtils::ConvertString(theTower.FullName());
            }
        }
        break;

        case 7: //Ring Column;
        {
            Duco::Tower theTower;
            if (database->FindTower(foundPeal.TowerId(), theTower, false))
            {
                const Ring* const theRing = theTower.FindRing(foundPeal.RingId());
                if (theRing != NULL)
                {
                    e->Value = DucoUtils::ConvertString(theRing->TenorDescription());
                }
            }
        }
        break;

        case 8: // Time
            e->Value = DucoUtils::PrintPealTime(foundPeal.Time(), false);
            break;

        case 9: //conductorColumn;
            if (foundPeal.ConductedType() == ESilentAndNonConducted)
            {
                e->Value = gcnew String("Non Conducted");
            }
            else if (foundPeal.ConductedType() == EJointlyConducted)
            {
                e->Value = gcnew String("Jointly conducted");
            }
            else
            {
                Ringer theConductor;
                if (database->FindRinger(foundPeal.FirstConductor(), theConductor, false))
                    e->Value = DucoUtils::ConvertString(theConductor.FullName(foundPeal.Date(), database->Settings().LastNameFirst()));
            }
            break;

        case 10: //ComposerColumn;
            e->Value = DucoUtils::ConvertString(foundPeal.Composer());
            break;

        case 11: //RingingWorldColumn;
            e->Value = DucoUtils::ConvertString(foundPeal.RingingWorldReference());
            break;

        case 12: //Bellboard column;
            e->Value = DucoUtils::ConvertString(foundPeal.BellBoardId());
            break;

        case 13: //Bell Rung column
            {
                Duco::Tower theTower;
                if (database->Settings().DefaultRingerSet() && database->FindTower(foundPeal.TowerId(), theTower, false))
                {
                    const Ring* const theRing = theTower.FindRing(foundPeal.RingId());
                    if (foundPeal.Handbell())
                    {
                        std::wstring bellNumbersStr;
                        if (foundPeal.HandBellsRung(database->Settings().DefaultRinger(), bellNumbersStr))
                        {
                            e->Value = DucoUtils::ConvertString(bellNumbersStr);
                        }
                    }
                    else
                    {
                        std::set<unsigned int> bellsrung;
                        bool asStrapper(false);
                        if (theRing != NULL && foundPeal.BellRung(database->Settings().DefaultRinger(), bellsrung, asStrapper))
                        {
                            unsigned int firstBell(*bellsrung.begin());
                            if (realBellNumber->Checked)
                            {
                                if (theRing->RealBellNumber(firstBell))
                                {
                                    e->Value = Convert::ToString(firstBell);
                                }
                            }
                            else
                            {
                                e->Value = Convert::ToString(firstBell);
                            }
                        }
                    }
                }
            }
            break;

        case 14: //errorCell;
            e->Value = DucoUtils::ConvertString(foundPeal.ErrorString(database->Database().Settings(), database->Config().IncludeWarningsInValidation()));
            break;
        }
        if (errorFoundChanged)
        {
            errorColumn->Visible = errorFound;
        }
        if (foundPeal.Withdrawn())
        {
            DataGridViewRow^ currentRow = dataGridView->Rows[e->RowIndex];
            currentRow->DefaultCellStyle = withdrawnPealStyle;
        }
    }
}

void
PealSearchUI::Initialised()
{
    if (searchThread->IsBusy)
        searchThread->ReportProgress(0);
    else
        progressBar->Value = 0;
}

void
PealSearchUI::Step(int progressPercent)
{
    if (searchThread->IsBusy)
        searchThread->ReportProgress(progressPercent/2);
    else
        progressBar->Value = progressPercent;
}

void
PealSearchUI::Complete()
{
    if (!searchThread->IsBusy)
        progressBar->Value = 0;
}

System::Void
PealSearchUI::dataGridView_CellDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    try
    {
        unsigned int rowIndex = e->RowIndex;
        DataGridViewRow^ currentRow = dataGridView->Rows[rowIndex];
        unsigned int pealId = Convert::ToInt16(currentRow->Cells[1]->Value);
        PealUI::ShowPeal(database, this->MdiParent, pealId);
    }
    catch (Exception^)
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void 
PealSearchUI::Editor_KeyDown( Object^ sender, System::Windows::Forms::KeyEventArgs^ e )
{
    Keys currentKeys = Control::ModifierKeys;
    nonNumberEntered = !DucoUIUtils::NumericKey(e, currentKeys);

    if (nonNumberEntered)
    {
        if (e->KeyCode == Keys::OemSemicolon && sender == pealTimeEditor && currentKeys == Keys::Shift)
            nonNumberEntered = false;
        else if((e->KeyCode == Keys::OemPeriod || e->KeyCode == Keys::Decimal) && sender == rwRef && currentKeys == Keys::None)
            nonNumberEntered = false;
        else if (e->KeyCode == Keys::V && currentKeys == Keys::Control) // Paste
            nonNumberEntered = false;
    }
}

System::Void
PealSearchUI::Editor_KeyPressed(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e)
{
    // Check for the flag being set in the KeyDown event.
    if ( nonNumberEntered == true )
    {
    // Stop the character from being entered into the control since it is non-numerical.
        e->Handled = true;
    }
}

void
PealSearchUI::InitialisingImportExport(System::Boolean /*internalising*/, unsigned int /*versionNumber*/, size_t totalNumberOfObjects)
{
    progressBar->Minimum = 0;
    progressBar->Value = 0;
    progressBar->Maximum = 100;
}

void 
PealSearchUI::ObjectProcessed(System::Boolean internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
{
    progressBar->Value = totalPercentage;
}

void 
PealSearchUI::ImportExportComplete(System::Boolean internalising)
{
    progressBar->Value = 0;
    progressBar->Maximum = 100;
}

void
PealSearchUI::ImportExportFailed(System::Boolean internalising, int /*errorCode*/)
{
    ImportExportComplete(internalising);
}

System::Void
PealSearchUI::ringerSelector_ItemCheck(System::Object^  sender, System::Windows::Forms::ItemCheckEventArgs^  e)
{
    int count = ringerSelector->CheckedItems->Count;
    e->NewValue == CheckState::Checked ? ++count : --count;
    SetAsStrapperAvailability(count, ringerEditor->Text->Length);
}

System::Void
PealSearchUI::ringerEditor_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    TextBox^ ringerEd = static_cast<System::Windows::Forms::TextBox^>(sender);
    SetAsStrapperAvailability(ringerSelector->CheckedItems->Count, ringerEd->Text->Length);
}

System::Void
PealSearchUI::AssociationEditor_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    inconsistantPealFees->Enabled = associationEditor->SelectedIndex != -1 && associationEditor->Text != "";
}

System::Void
PealSearchUI::SetAsStrapperAvailability(int noOfRingersSelected, int ringerTextLength)
{
    if ( (noOfRingersSelected == 1 && ringerTextLength == 0) ||
         (ringerTextLength > 0 && noOfRingersSelected == 0) )
    {
        asStrapper->Enabled = true;
    }
    else
    {
        asStrapper->Checked = false;
        asStrapper->Enabled = false;
    }
}

System::Void
PealSearchUI::dateBefore_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    pealDate1->Enabled = true;
    pealDate2->Visible = false;
}

System::Void
PealSearchUI::dateOn_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    pealDate1->Enabled = true;
    pealDate2->Visible = false;
}

System::Void
PealSearchUI::dateAfter_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    pealDate1->Enabled = true;
    pealDate2->Visible = false;
}

System::Void
PealSearchUI::dateOff_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    pealDate1->Enabled = false;
    pealDate2->Visible = false;
}

System::Void
PealSearchUI::dateBetween_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    pealDate2->Visible = !pealDate2->Visible;
    pealDate1->Enabled = true;
}

System::Void
PealSearchUI::blankAssociation_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    associationEditor->Enabled = !blankAssociation->Checked;
}

System::Void
PealSearchUI::emptyFootnotes_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
{
    footnotesEditor->Enabled = !emptyFootnotes->Checked;
}

System::Void
PealSearchUI::nonEmptyFootnotes_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
{
    footnotesEditor->Enabled = !nonEmptyFootnotes->Checked;
}

System::Void
PealSearchUI::linkedTowers_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
{
    if (linkedTowers->Checked)
    {
        ringSelector->SelectedIndex = -1;
    }
    else
    {

    }
//    ringSelector->Enabled = !linkedTowers->Checked;
}

System::Void
PealSearchUI::conducted_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    conductorSelector->Enabled = true;
}

System::Void
PealSearchUI::silentConductor_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    conductorSelector->Enabled = false;
}

System::Void
PealSearchUI::jointConductor_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    conductorSelector->Enabled = true;
}

System::Void
PealSearchUI::emptyRWRef_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    rwRef->Enabled = !emptyRWRef->Checked;
    nonEmptyRWRef->Enabled = !emptyRWRef->Checked;
}

System::Void
PealSearchUI::nonEmptyRWRef_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
{
    rwRef->Enabled = !nonEmptyRWRef->Checked;
    emptyRWRef->Enabled = !nonEmptyRWRef->Checked;
}

System::Void
PealSearchUI::fullValidRWReference_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
{
    invalidRWReference->Enabled = !fullValidRWReference->Checked;
}

System::Void
PealSearchUI::invalidRWReference_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
{
    fullValidRWReference->Enabled = !invalidRWReference->Checked;
}



System::Void
PealSearchUI::blankBellBoardRef_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    bellBoardRef->Enabled = !blankBellBoardRef->Checked;
    nonBlankBellBoardRef->Enabled = !blankBellBoardRef->Checked;
}

System::Void
PealSearchUI::nonBlankBellBoardRef_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    bellBoardRef->Enabled = !blankBellBoardRef->Checked;
    blankBellBoardRef->Enabled = !nonBlankBellBoardRef->Checked;
}

System::Void
PealSearchUI::nonBlankNotes_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    notes->Enabled = !nonBlankNotes->Checked;
}

System::Void
PealSearchUI::showAssociationColumn_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
{
    if (!populating)
    {
        database->Settings().SetShowAssociationColumn(((System::Windows::Forms::CheckBox^)(sender))->Checked);
    }
}

System::Void
PealSearchUI::showComposerColumn_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
{
    if (!populating)
    {
        database->Settings().SetShowComposerColumn(((System::Windows::Forms::CheckBox^)(sender))->Checked);
    }
}

System::Void
PealSearchUI::showBellRungColumn_CheckedChanged(System::Object^ sender, System::EventArgs^ e) 
{
    if (!populating)
    {
        database->Settings().SetShowBellRungColumn(((System::Windows::Forms::CheckBox^)(sender))->Checked);
    }
}

System::Void
PealSearchUI::showReferencesColumns_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
{
    if (!populating)
    {
        database->Settings().SetShowReferencesColumn(((System::Windows::Forms::CheckBox^)(sender))->Checked);
    }
}

System::Void
PealSearchUI::showConductorColumn_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
{
    if (!populating)
    {
        database->Settings().SetShowConductorColumn(((System::Windows::Forms::CheckBox^)(sender))->Checked);
    }
}

System::Void
PealSearchUI::showRingColumn_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
{
    if (!populating)
    {
        database->Settings().SetShowRingColumn(((System::Windows::Forms::CheckBox^)(sender))->Checked);
    }
}

System::Void
PealSearchUI::AddAdvancedRinger_Click(System::Object^ sender, System::EventArgs^  e)
{
    TSearchFieldRingerArgument* newSearchParam = NULL;

    std::wstring convertedStr;
    DucoUtils::ConvertString(advBellNo->Text, convertedStr);
    TSearchFieldRingerPosition position (convertedStr);
    if (advRingerName->SelectedIndex == -1)
    {
        std::wstring converted;
        DucoUtils::ConvertString(advRingerName->Text->Trim(), converted);
        if (converted.length() > 0)
        {
            newSearchParam = new TSearchFieldRingerArgument(converted, position);
        }
    }
    else
    {
        RingerItem^ ringer = (RingerItem^)advRingerName->SelectedItem;
        Duco::ObjectId ringerId = ringer->Id();
        if (ringerId.ValidId())
        {
            newSearchParam = new TSearchFieldRingerArgument(ringerId, position);
        }
    }

    if (newSearchParam != NULL)
    {
        RingerSearchControl^ newRinger = gcnew RingerSearchControl(newSearchParam, database);
        newRinger->Name = L"newRinger";
        ringerAdvSearchParams->Controls->Add(newRinger);
    }
}

System::Void
PealSearchUI::RemoveAdvancedRingers_Click(System::Object^  sender, System::EventArgs^  e)
{
    System::Collections::IEnumerator^ it = ringerAdvSearchParams->Controls->GetEnumerator();
    while (it->MoveNext())
    {
        RingerSearchControl^ indexChecked =  safe_cast<RingerSearchControl^>(it->Current);
        if (indexChecked->ShouldBeRemoved())
        {
            ringerAdvSearchParams->Controls->Remove(indexChecked);
            it = ringerAdvSearchParams->Controls->GetEnumerator();
        }
    }
}

System::Void
PealSearchUI::copyPealDataCmd_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (dataGridView->GetCellCount(DataGridViewElementStates::Selected) > 0)
    {
        // Add the selection to the clipboard.
        Clipboard::SetDataObject(dataGridView->GetClipboardContent());
    }
}
