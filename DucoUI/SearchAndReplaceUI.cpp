#include "SearchAndReplaceUI.h"
#include "DucoUtils.h"
#include "SystemDefaultIcon.h"
#include <SearchCommon.h>
#include <DatabaseSearch.h>
#include "DatabaseManager.h"
#include <UppercaseField.h>
#include "SoundUtils.h"
#include <ReplaceStringField.h>

using namespace System;
using namespace DucoUI;
using namespace Duco;

SearchAndReplaceUI::SearchAndReplaceUI(DucoUI::DatabaseManager^ theDatabase, DucoUI::ProgressCallbackWrapper* theCallBack, const std::set<Duco::ObjectId>& objectIds, TObjectType objIdTypesSupplied)
    :    database(theDatabase), callBack(theCallBack), objectIdTypesSupplied(objIdTypesSupplied)
{
    objectIdsToUpdate = new std::set<Duco::ObjectId>(objectIds);
    InitializeComponent();
    CreateOptions();
}

SearchAndReplaceUI::~SearchAndReplaceUI()
{
    delete objectIdsToUpdate;
    objectIdsToUpdate = NULL;
    if (components)
    {
        delete components;
    }
}

System::Void
SearchAndReplaceUI::SearchAndReplaceUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
}

System::Void
SearchAndReplaceUI::CreateOptions()
{
    if (objectIdTypesSupplied == TObjectType::EPeal)
    {
        this->fieldChooser->Items->AddRange(gcnew cli::array< System::Object^  >(3) {L"Peal: Association", L"Peal: Composer", L"Peal: Footnotes"} );
    }
    if (objectIdTypesSupplied == TObjectType::EPeal || objectIdTypesSupplied == TObjectType::EMethod)
    {
        this->fieldChooser->Items->AddRange(gcnew cli::array< System::Object^  >(2) {L"Method: Name", L"Method: Type"} );
    }
    if (objectIdTypesSupplied == TObjectType::EPeal || objectIdTypesSupplied == TObjectType::ETower)
    {
        this->fieldChooser->Items->AddRange(gcnew cli::array< System::Object^  >(2) {L"Tower: Name", L"Tower: City"} );
    }
}

System::Void
SearchAndReplaceUI::updateBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    DatabaseSearch search(database->Database());
    if (fieldChooser->SelectedIndex != -1)
    {
        TUpdateFieldId field;
        TObjectType requiredType (TObjectType::ENon);
        if (fieldChooser->Text->CompareTo("Peal: Composer") == 0)
        {
            field = Duco::EPeal_Composer;
            requiredType = TObjectType::EPeal;
        }
        else if (fieldChooser->Text->CompareTo("Peal: Footnotes") == 0)
        {
            field = Duco::EPeal_Footnotes;
            requiredType = TObjectType::EPeal;
        }
        else if (fieldChooser->Text->CompareTo("Method: Name") == 0)
        {
            field = Duco::EMethod_Name;
            requiredType = TObjectType::EMethod;
        }
        else if (fieldChooser->Text->CompareTo("Method: Type") == 0)
        {
            field = Duco::EMethod_Type;
            requiredType = TObjectType::EMethod;
        }
        else if (fieldChooser->Text->CompareTo("Tower: Name") == 0)
        {
            field = Duco::ETower_Name;
            requiredType = TObjectType::ETower;
        }
        else if (fieldChooser->Text->CompareTo("Tower: City") == 0)
        {
            field = Duco::ETower_City;
            requiredType = TObjectType::ETower;
        }
        if (capitalise->Checked)
        {
            TUppercaseField* newField = new TUppercaseField(field);
            search.AddUpdateArgument(newField);
        }
        else
        {
            std::wstring searchStr;
            DucoUtils::ConvertString(searchString->Text, searchStr);
            std::wstring replaceStr;
            DucoUtils::ConvertString(replaceWith->Text, replaceStr);
            TReplaceStringField* newField = new TReplaceStringField(field, exactMatch->Checked, searchStr, replaceStr);
            search.AddUpdateArgument(newField);
        }
        if (!search.Update(*callBack, *objectIdsToUpdate, requiredType, objectIdTypesSupplied))
        {
            SoundUtils::PlayErrorSound();
        }
    }
}

System::Void
SearchAndReplaceUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void
SearchAndReplaceUI::capitalise_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    exactMatch->Enabled = !capitalise->Checked;
    searchString->Enabled = !capitalise->Checked;
    replaceWith->Enabled = !capitalise->Checked;
}
