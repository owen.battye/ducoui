#pragma once

namespace DucoUI
{
    public ref class DownloadPealUI : public System::Windows::Forms::Form, public DucoUI::BellboardParserUIWrapper, public DucoUI::RingingDatabaseObserver, public DucoUI::BellboardPerformanceParserConfirmRingerCallbackUIWrapper
    {
    public:
        DownloadPealUI(DucoUI::DatabaseManager^ theDatabase);

        // from BellboardParserUIWrapper
        virtual System::Void Cancelled(System::String^ bellboardId, System::String^ errorMessage);
        virtual System::Void Completed(System::String^ bellboardId, System::Boolean errors);

        //from BellboardPerformanceParserConfirmRingerCallbackUIWrapper
        virtual Duco::ObjectId ChooseRinger(System::String^ missingRingerName, const std::set<Duco::RingerPealDates>& nearMatches);

        // from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

    protected:
        ~DownloadPealUI();
        !DownloadPealUI();
        System::Void DownloadPeal_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void DownloadPealUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e);

        static System::Void BrowserLoadingStateChanged(System::Object ^, CefSharp::LoadingStateChangedEventArgs ^  e);
        System::Void SetSearchParameters();
        System::Void GoToSearch();
        System::Void LoadingStatusChanged(CefSharp::LoadingStateChangedEventArgs ^  e);
        System::Void SetStatusLabelText();
        System::Void AppendStatusLabelText(System::String^ nextText);

        System::Void downloadBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void downloadAllBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void backButton_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void bellBoardBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void zoomOutBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void zoomInBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void clearZoom_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void highlightBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void useBellboardIds_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void clearHighlightsToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e);

        System::Void InitializeComponent();
        System::Void InitializeCEFSharp();
        System::Void GoToWebSite();
        void StartNextDownload();
        System::Void ClearPealFlags();
        System::Void HighlightKnownPeal(System::String^ pealId, System::String^ colour);
        System::Void HighlightKnownPealsAndSetMissingIds(System::Boolean updateMissingPealIds);

    protected: 
        // member data
        DucoUI::DatabaseManager^                    database;
        Duco::BellboardPerformanceParser*           parser;
        DucoUI::BellboardParserWrapper*             callbackWrapper;
        DucoUI::BellboardRingerParserWrapper*       ringerCallbackWrapper;
        System::Collections::Generic::Queue<System::String^>^  missingPealIds;
        System::UInt64                              numberDownloaded;

        System::ComponentModel::Container^          components;
        System::Windows::Forms::Button^             downloadBtn;
        CefSharp::WinForms::ChromiumWebBrowser^     chromeBrowser;

        System::Windows::Forms::Button^             zoomOutBtn;
        System::Net::Http::HttpClient^              downloadHandler;
        System::Windows::Forms::Button^             downloadAllBtn;
        System::Windows::Forms::Button^             zoomInBtn;

        System::Windows::Forms::Button^             resetZoomBtn;
        System::Windows::Forms::TableLayoutPanel^   tableLayoutPanel1;
        System::Boolean                             firstLoad;
    
        System::Windows::Forms::ToolStripStatusLabel^  statusLabel;
        System::Windows::Forms::Button^             bellBoardBtn;
        System::Windows::Forms::ToolStripMenuItem^ highlightKnownPealsBtn;
        System::Windows::Forms::ToolStripMenuItem^ useBellboardIds;
        System::Windows::Forms::ToolStripMenuItem^ clearHighlightsToolStripMenuItem;
    public:
        delegate void LoadStatusDelegate(CefSharp::LoadingStateChangedEventArgs ^  e);
        LoadStatusDelegate^                         loadDelegate;
    };
}
