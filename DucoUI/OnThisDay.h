#pragma once

namespace DucoUI {

    public ref class OnThisDay : public System::Windows::Forms::Form
    {
    public:
        OnThisDay(DucoUI::DatabaseManager^ theDatabase);

    private:
        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void OnThisDay_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void OnThisDay_Activated(System::Object^  sender, System::EventArgs^  e);

    protected:
        ~OnThisDay();
        System::Void OnThisDay_Resize(Object^ sender, System::EventArgs^ args);

    private:
        DucoUI::DatabaseManager^ database;
        System::ComponentModel::Container ^components;
        System::Windows::Forms::FlowLayoutPanel^  pealsPanel;

#pragma region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        void InitializeComponent(void)
        {
            System::Windows::Forms::Button^  closeBtn;
            System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
            this->pealsPanel = (gcnew System::Windows::Forms::FlowLayoutPanel());
            closeBtn = (gcnew System::Windows::Forms::Button());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            tableLayoutPanel1->SuspendLayout();
            this->SuspendLayout();
            // 
            // closeBtn
            // 
            closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
            closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            closeBtn->Location = System::Drawing::Point(754, 346);
            closeBtn->Name = L"closeBtn";
            closeBtn->Size = System::Drawing::Size(75, 23);
            closeBtn->TabIndex = 1;
            closeBtn->Text = L"Close";
            closeBtn->UseVisualStyleBackColor = true;
            closeBtn->Click += gcnew System::EventHandler(this, &OnThisDay::closeBtn_Click);
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 3;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->Controls->Add(closeBtn, 2, 1);
            tableLayoutPanel1->Controls->Add(this->pealsPanel, 0, 0);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 2;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->Size = System::Drawing::Size(832, 372);
            tableLayoutPanel1->TabIndex = 6;
            // 
            // pealsPanel
            // 
            this->pealsPanel->AutoScroll = true;
            tableLayoutPanel1->SetColumnSpan(this->pealsPanel, 3);
            this->pealsPanel->Dock = System::Windows::Forms::DockStyle::Fill;
            this->pealsPanel->FlowDirection = System::Windows::Forms::FlowDirection::TopDown;
            this->pealsPanel->Location = System::Drawing::Point(0, 0);
            this->pealsPanel->Margin = System::Windows::Forms::Padding(0);
            this->pealsPanel->Name = L"pealsPanel";
            this->pealsPanel->Size = System::Drawing::Size(832, 343);
            this->pealsPanel->TabIndex = 3;
            this->pealsPanel->WrapContents = false;
            // 
            // OnThisDay
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->CancelButton = closeBtn;
            this->ClientSize = System::Drawing::Size(832, 372);
            this->Controls->Add(tableLayoutPanel1);
            this->Name = L"OnThisDay";
            this->Text = L"Performances on this day";
            this->Activated += gcnew System::EventHandler(this, &OnThisDay::OnThisDay_Activated);
            this->Load += gcnew System::EventHandler(this, &OnThisDay::OnThisDay_Load);
            tableLayoutPanel1->ResumeLayout(false);
            this->ResumeLayout(false);

        }
#pragma endregion
    };
}
