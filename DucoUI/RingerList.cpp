#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"

#include "RingerList.h"

#include <RingingDatabase.h>
#include <Peal.h>
#include "RingerObject.h"
#include "DucoUtils.h"
#include "DucoCommon.h"
#include <Ringer.h>
#include "RingerEventArgs.h"
#include <DatabaseSearch.h>
#include <DatabaseSettings.h>
#include <StatisticFilters.h>
#include "ChooseOrGenerateRinger.h"

using namespace Duco;
using namespace DucoUI;

using namespace System;
using namespace System::Drawing;
using namespace System::ComponentModel;
using namespace System::Windows::Forms;

RingerList::RingerList(DatabaseManager^ theManager)
    : System::Windows::Forms::Panel(), database(theManager)
{
    ringerCache = nullptr;
    database->AddObserver(this);
    InitializeComponent();
}

RingerList::~RingerList()
{
    database->RemoveObserver(this);
    this->!RingerList();
    if (components)
    {
        delete components;
    }
}

RingerList::!RingerList()
{
    currentPeal = NULL;
}

System::Void
RingerList::SetPeal(Duco::Peal* thePeal)
{
    currentPeal = thePeal;
    if (ringerCache == nullptr)
    {
        ringerCache = RingerDisplayCache::CreateDisplayCacheWithRingers(database, currentPeal->Date(), true, true);
    }
    else if (ringerCache->NeedRegeneration(currentPeal->Date()))
    {
        BeginUpdate(false);
        ringerCache->GenerateRingerOptions(currentPeal->Date());
        EndUpdate();
    }
    UpdateNoOfBells(-1);
}

System::Void
RingerList::SetState(DucoWindowState newState)
{
    state = newState;
    System::Collections::IEnumerator^ iter = components->Components->GetEnumerator();
    while (iter->MoveNext())
    {
        RingerObject^ nextRinger = static_cast<RingerObject^>(iter->Current);
        nextRinger->SetState(state);
    }
}

System::Void
RingerList::SetHandbell(bool handbell)
{
    System::Collections::IEnumerator^ iter = components->Components->GetEnumerator();
    while (iter->MoveNext())
    {
        RingerObject^ nextRinger = static_cast<RingerObject^>(iter->Current);
        nextRinger->SetHandbell(handbell);
    }
}

System::Void
RingerList::UpdateNoOfBells(unsigned int noOfBells)
{
    bool noBellsSpecified (noOfBells == -1);
    if (noOfBells == -1)
    {
        noOfBells = currentPeal->NoOfBellsRung(database->Database());
    }
    if (currentPeal->Handbell())
        noOfBells /= 2;

    unsigned int bellNumber (0);
    System::Collections::IEnumerator^ iter = components->Components->GetEnumerator();
    while (iter->MoveNext())
    {
        ++bellNumber;
        RingerObject^ nextRinger = static_cast<RingerObject^>(iter->Current);
        if (bellNumber > noOfBells)
        {
            if (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode)
            {
                currentPeal->RemoveRinger(nextRinger->BellNumber(), false);            
                nextRinger->Visible = false;
            }
            else
            {
                Controls->Remove(nextRinger);
                components->Remove(nextRinger);
                delete nextRinger;
            }
        }
        else
        {
            if ((state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode) && (nextRinger->BellNumber() != -1))
            {
                if (nextRinger->Editor()->SelectedIndex != -1)
                {
                    RingerItem^ ringer = (RingerItem^)nextRinger->Editor()->SelectedItem;
                    currentPeal->SetRingerId(ringer->Id(), nextRinger->BellNumber(), false);
                }
            }
            SetRinger(nextRinger, bellNumber);
            nextRinger->Visible = true;
        }
    }

    while (unsigned int (components->Components->Count) < noOfBells)
    {
        CreateRingerControl(components->Components->Count+1, true);
    }
    Relayout();
}

System::Void
RingerList::Relayout()
{
    System::Collections::IEnumerator^ iter = components->Components->GetEnumerator();
    int yPosition (KFirstControlYOffSet + AutoScrollPosition.Y);
    while (iter->MoveNext())
    {
        RingerObject^ nextRinger = static_cast<RingerObject^>(iter->Current);
        nextRinger->Location = Point(0, yPosition);

        yPosition += nextRinger->Size.Height;
    }
}

System::Void 
RingerList::AddMissingRinger(int bellNumber, System::String^ name, System::Boolean conductor, System::Boolean strapper)
{
    RingerObject^ nextRinger = nullptr;
    if (components->Components->Count >= bellNumber)
        nextRinger = static_cast<RingerObject^>(components->Components[bellNumber-1]);
    else
    {
        while (components->Components->Count < bellNumber)
        {
            nextRinger = CreateRingerControl(components->Components->Count+1, false);
        }
    }
    if (nextRinger != nullptr)
    {
        Duco::ObjectId ringerId;
        if (database->FindRingerIdDuringDownload(name, ringerId))
        {
            nextRinger->SetNameAndConductor(name, conductor, strapper, ringerCache->FindIndexOrAdd(ringerId));
        }
        else
        {
            nextRinger->SetNameAndConductor(name, conductor, strapper, -1);
        }
    }
}

System::Void
RingerList::SetConductor(int bellNumber, bool conductor)
{
    RingerObject^ nextRinger = nullptr;
    if (components->Components->Count >= bellNumber)
    {
        nextRinger = static_cast<RingerObject^>(components->Components[bellNumber - 1]);
    }
    else
    {
        while (components->Components->Count < bellNumber)
        {
            nextRinger = CreateRingerControl(components->Components->Count+1, true);
        }
    }
    if (nextRinger != nullptr)
    {
        //nextRinger->SetConductor(allRingerIds, *currentPeal);
    }
}

System::Void
RingerList::SetRinger(RingerObject^ currentRinger, unsigned int bellNumber)
{
    Duco::ObjectId ringerId;
    int strapperIndex = -1;
    int ringerIndex = -1;
    bool strapperIsConductor (false);
    bool ringerIsConductor (false);
    if (currentPeal->RingerId(bellNumber, false, ringerId))
    {
        if (ringerId.ValidId())
        {
            ringerIndex = ringerCache->FindIndexOrAdd(ringerId);
            ringerIsConductor = currentPeal->ContainsConductor(ringerId);
        }
    }
    if (currentPeal->RingerId(bellNumber, true, ringerId))
    {
        strapperIndex = ringerCache->FindIndexOrAdd(ringerId);
        strapperIsConductor = currentPeal->ContainsConductor(ringerId);
    }
    currentRinger->SetRinger(*currentPeal, ringerIndex, ringerIsConductor, strapperIndex, strapperIsConductor, currentPeal->NoOfBellsRung(database->Database()));
}

RingerObject^
RingerList::CreateRingerControl(unsigned int bellNumber, System::Boolean setRinger)
{
    RingerObject^ newRinger = gcnew RingerObject(this, bellNumber);
    components->Add(newRinger);
    newRinger->SetState(state);
    ringerCache->PopulateControl(newRinger->Editor(), false);
    if (setRinger)
    {
        SetRinger(newRinger, bellNumber);
    }
    newRinger->Location = System::Drawing::Point(0, components->Components->Count*KRingerLineHeight + KFirstControlYOffSet +  AutoScrollPosition.Y);
    Controls->AddRange(gcnew cli::array< System::Windows::Forms::Control^>(1){newRinger});
    newRinger->Visible = true;
    return newRinger;
}

System::Void
RingerList::InitializeComponent()
{
    components = gcnew System::ComponentModel::Container();
    this->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;

    System::Windows::Forms::Label^ conductorLbl;
    System::Windows::Forms::Label^ strapperLbl;

    this->SuspendLayout();
    conductorLbl = (gcnew System::Windows::Forms::Label());
    strapperLbl = (gcnew System::Windows::Forms::Label());

    // 
    // conductorLbl
    // 
    conductorLbl->AutoSize = true;
    conductorLbl->Location = System::Drawing::Point(265, 4);
    conductorLbl->Name = L"conductorLbl";
    conductorLbl->Size = System::Drawing::Size(35, 13);
    conductorLbl->TabIndex = 0;
    conductorLbl->Text = L"Cond";
    conductorLbl->TextAlign = ContentAlignment::MiddleRight;
    // 
    // strapperLbl
    // 
    strapperLbl->AutoSize = true;
    strapperLbl->Location = System::Drawing::Point(295, 4);
    strapperLbl->Name = L"strapperLbl";
    strapperLbl->Size = System::Drawing::Size(35, 13);
    strapperLbl->TabIndex = 0;
    strapperLbl->Text = L"Strapper";
    strapperLbl->TextAlign = ContentAlignment::MiddleRight;

    Controls->AddRange(gcnew cli::array< System::Windows::Forms::Control^  >(2){conductorLbl, strapperLbl});
    this->ResumeLayout(false);
}

System::Void
RingerList::RefreshConductorControls()
{
    System::Collections::IEnumerator^ iter = components->Components->GetEnumerator();
    while (iter->MoveNext())
    {
        RingerObject^ nextRinger = static_cast<RingerObject^>(iter->Current);
        bool strapperIsConductor(false);
        bool ringerIsConductor(false);
        Duco::ObjectId ringerId;
        bool eitherRingerFound(false);
        if (currentPeal)
        {
            if (currentPeal->RingerId(nextRinger->BellNumber(), false, ringerId) && ringerId.ValidId())
            {
                ringerIsConductor = currentPeal->ContainsConductor(ringerId);
                eitherRingerFound = true;
            }
            if (currentPeal->RingerId(nextRinger->BellNumber(), true, ringerId) && ringerId.ValidId())
            {
                strapperIsConductor = currentPeal->ContainsConductor(ringerId);
                eitherRingerFound = true;
            }
        }
        if (eitherRingerFound)
        {
            nextRinger->CheckConductor(ringerIsConductor, currentPeal->ConductedType() == ESilentAndNonConducted, strapperIsConductor);
        }
    }
}

System::Boolean
RingerList::CreateNewRingers(System::Windows::Forms::TabControl^ pealTabControl)
{
    bool allRingersOK (true);
    System::Collections::IEnumerator^ iter = components->Components->GetEnumerator();
    while (iter->MoveNext())
    {
        RingerObject^ nextRinger = static_cast<RingerObject^>(iter->Current);
        allRingersOK &= CreateNewRinger(nextRinger, pealTabControl);
        if (nextRinger->IncludesStrapper())
        {
            allRingersOK &= CreateNewRinger(nextRinger->Strapper(), pealTabControl);
        }
    }
    return allRingersOK;
}

System::Boolean
RingerList::CreateNewRinger(RingerObjectBase^ nextRinger, System::Windows::Forms::TabControl^ pealTabControl)
{
    bool ringerOk (true);
    if (!nextRinger->KnownRinger())
    {
        String^ temp = nextRinger->RingerText();
        if (temp->Length != 0)
        {
            Duco::ObjectId newRingerId;
            std::set<Duco::RingerPealDates> nearMatches;
            newRingerId = database->SuggestRingers(temp, currentPeal->Date(), nearMatches);
            if (!newRingerId.ValidId() && nearMatches.size() > 0)
            {
                DucoUI::ChooseOrGenerateRinger^ chooseRingerDialog = gcnew DucoUI::ChooseOrGenerateRinger(database, nearMatches, *currentPeal, temp, true);
                if (chooseRingerDialog->ShowDialog() == ::DialogResult::OK)
                {
                    newRingerId = chooseRingerDialog->SelectedId();
                }
            }
            if (!newRingerId.ValidId())
            {
                newRingerId = database->AddRinger(temp);
            }
            if (newRingerId.ValidId())
            {
                currentPeal->SetRingerId(newRingerId, nextRinger->BellNumber(), nextRinger->IsStrapper());
                if (!nextRinger->SetIndex(nextRinger->IsStrapper(), ringerCache->FindIndexOrAdd(newRingerId)))
                {
                    ringerCache->RepopulateControl(nextRinger->Editor(), newRingerId);
                }
                if (nextRinger->IsConductor())
                {
                    currentPeal->SetConductorId(newRingerId);
                }
            }
        }
    }
    return ringerOk;
}

System::Void
RingerList::StrapperStatusChanged(unsigned int bellNo, bool strapper)
{
    if (state != DucoWindowState::EViewMode)
    {
        if (!strapper)
        {
            currentPeal->RemoveRinger(bellNo, true);
        }
        Relayout();
    }
}

System::Boolean
RingerList::ShowStrapperCheckBox(unsigned int bellNo)
{
    if (state != DucoWindowState::EViewMode && currentPeal != NULL)
    {
        return currentPeal->ShowStrapperOption(database->Database(), bellNo);
    }

    return false;
}

System::Void
RingerList::RingerChanged(DucoUI::RingerItem^ object, System::Boolean conductor, unsigned int bellNo, System::Boolean strapper)
{
    if (state != DucoWindowState::EViewMode)
    {
        Duco::ObjectId newRingerId (object->Id());
        if (strapper && newRingerId == KNoId)
        {
            currentPeal->RemoveRinger(bellNo, strapper);
        }
        else
        {
            currentPeal->SetRingerId(newRingerId, bellNo, strapper);
            if (conductor)
            {
                currentPeal->SetConductorIdFromBell(bellNo, conductor, strapper);
            }
        }

        RingerEventArgs^ datachangedEvent = gcnew RingerEventArgs(TRingerChangeEventType::ERingerIdChanged, newRingerId, conductor);
        OnRingerDataChanged(datachangedEvent);
    }
}

System::Void
RingerList::RingerChanged(System::String^ newRingerName, System::Boolean conductor, unsigned int bellNo, System::Boolean strapper)
{
    if (state != DucoWindowState::EViewMode)
    {
        currentPeal->SetRingerId(KNoId, bellNo, strapper);
        if (conductor)
        {
            currentPeal->SetConductorIdFromBell(bellNo, conductor, strapper);
        }

        RingerEventArgs^ datachangedEvent = gcnew RingerEventArgs(TRingerChangeEventType::ERingerIdChanged, KNoId, conductor);
        OnRingerDataChanged(datachangedEvent);
    }
}

System::Int32
RingerList::RingerChangedToUnknown(System::String^ newRingerName, System::Boolean conductor, unsigned int bellNo, System::Boolean strapper)
{ // Used when a user leaves the control, and the ringer is unrecognised from the cache check its not just been filtered out for performance
    if (state != DucoWindowState::EViewMode && ringerCache->RingerSubsetDisplayEnabled())
    {
        std::set<Duco::RingerPealDates> nearMatches;
        Duco::ObjectId ringerId = database->SuggestRingers(newRingerName, currentPeal->Date(), nearMatches);
        if (!ringerId.ValidId() && nearMatches.size() > 0)
        {
            DucoUI::ChooseOrGenerateRinger^ chooseRingerDialog = gcnew DucoUI::ChooseOrGenerateRinger(database, nearMatches, *currentPeal, newRingerName, true);
            if (chooseRingerDialog->ShowDialog() == ::DialogResult::OK)
            {
                ringerId = chooseRingerDialog->SelectedId();
            }
        }

        if (ringerId.ValidId())
        {
            BeginUpdate(true);
            Int32 newIndex = ringerCache->FindIndexOrAdd(ringerId);
            EndUpdate();
            if (newIndex != Int32::MaxValue)
            {
                currentPeal->SetRingerId(ringerId, bellNo, strapper);
                if (conductor)
                {
                    currentPeal->SetConductorIdFromBell(bellNo, conductor, strapper);
                }

                RingerEventArgs^ datachangedEvent = gcnew RingerEventArgs(TRingerChangeEventType::ERingerIdChanged, ringerId, conductor);
                OnRingerDataChanged(datachangedEvent);
                return newIndex;
            }
        }
    }
    return Int32::MaxValue;
}

System::Void
RingerList::ConductorChanged(System::Boolean conductor, unsigned int bellNumber, System::Boolean isStrapper)
{
    if (state != DucoWindowState::EViewMode)
    {
        Duco::ObjectId newRingerId;
        currentPeal->RingerId(bellNumber, isStrapper, newRingerId);
        if (newRingerId.ValidId())
        {
            currentPeal->SetConductorIdFromBell(bellNumber, conductor, isStrapper);
            RefreshConductorControls();
        }

        RingerEventArgs^ datachangedEvent = gcnew RingerEventArgs(TRingerChangeEventType::EConductor, newRingerId, conductor);
        OnRingerDataChanged(datachangedEvent);
    }
}

System::Void
RingerList::OnRingerDataChanged(RingerEventArgs^ e)
{
    dataChangedEventHandler(this, e);
}

System::Void
RingerList::PealDateChanged(const Duco::PerformanceDate& newDate)
{
    if (ringerCache->NeedRegeneration(newDate))
    {
        BeginUpdate(true);
        ringerCache->GenerateRingerOptions(newDate);
        EndUpdate();
    }
}

System::Void
RingerList::BeginUpdate(System::Boolean saveOldValues)
{
    System::Collections::IEnumerator^ beginIter = components->Components->GetEnumerator();
    while (beginIter->MoveNext())
    {
        RingerObject^ nextRinger = static_cast<RingerObject^>(beginIter->Current);
        nextRinger->BeginUpdate(saveOldValues);
    }
}

System::Void
RingerList::EndUpdate()
{
    System::Collections::IEnumerator^ endIter = components->Components->GetEnumerator();
    while (endIter->MoveNext())
    {
        RingerObject^ nextRinger = static_cast<RingerObject^>(endIter->Current);
        nextRinger->EndUpdate(ringerCache);
    }
}

System::Void
RingerList::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    switch (type)
    {
    case TObjectType::ERinger:
    {
        BeginUpdate(true);
        switch (eventId)
        {
        case EAdded:
            ringerCache->AddRingerOption(id);
            break;
        case EUpdated:
            if (!id2.ValidId())
            {
                ringerCache->UpdateRingerOption(id);
            }
            break;
        case EDeleted:
            ringerCache->RemoveRingerOption(id);
            break;
        }
        EndUpdate();
    }
        break;

    default:
        break;
    }
}