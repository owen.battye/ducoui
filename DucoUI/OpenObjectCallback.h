#pragma once
#include <ObjectId.h>
#include <ObjectType.h>

namespace DucoUI
{
    /// <summary>
    /// Summary for IOpenPealCallback
    /// </summary>
    public interface class IOpenObjectCallback
    {
        void OpenObject(Duco::ObjectId& pealId, Duco::TObjectType objectType);
    };
}
