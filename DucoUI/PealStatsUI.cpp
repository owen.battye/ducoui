#include <Peal.h>
#include "DatabaseManager.h"

#include "PealStatsUI.h"

#include "DucoUtils.h"
#include <RingingDatabase.h>
#include "SystemDefaultIcon.h"
#include <PealDatabase.h>
#include <StatisticFilters.h>
#include <DucoEngineUtils.h>

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Windows::Forms;

PealStatsUI::PealStatsUI(DatabaseManager^ theDatabase, const Duco::ObjectId& newPealId)
:   database(theDatabase)
{
    InitializeComponent();

    currentPeal = new Duco::Peal();
    if (database->FindPeal(newPealId, *currentPeal, false))
    {
        std::wstring pealTitle;
        currentPeal->ShortTitle(pealTitle);
        this->Text += " - " + DucoUtils::ConvertString(pealTitle);
    }
}

PealStatsUI::!PealStatsUI()
{
    delete currentPeal;
}

PealStatsUI::~PealStatsUI()
{
    this->!PealStatsUI();
    if (components)
    {
        delete components;
    }
}

System::Void
PealStatsUI::PealStatsUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    changesPerMinute->Text = DucoUtils::FormatDoubleToString(currentPeal->ChangesPerMinute(), 2);

    StatisticFilters filters(database->Database());
    Duco::PealLengthInfo totalPealsInfo = database->Database().PealsDatabase().PerformanceInfo(filters);

    allPealsSlowestLbl->Text = DucoUtils::FormatDoubleToString(totalPealsInfo.SlowestChangesPerMinute(), 2);
    allPealsFastestLbl->Text = DucoUtils::FormatDoubleToString(totalPealsInfo.FastestChangesPerMinute(), 2);
    allPealsComparision->Minimum = int(totalPealsInfo.SlowestChangesPerMinute() *100);
    allPealsComparision->Maximum = int(totalPealsInfo.FastestChangesPerMinute() *100);
    int changesPerMinute = int(currentPeal->ChangesPerMinute()*100);
    if (changesPerMinute != 0)
    {
        allPealsComparision->Value = changesPerMinute;
    }

    filters.SetTower(true, currentPeal->TowerId());
    totalPealsInfo = database->Database().PealsDatabase().PerformanceInfo(filters);

    thisTowerSlowestLbl->Text = DucoUtils::FormatDoubleToString(totalPealsInfo.SlowestChangesPerMinute(), 2);
    thisTowerFastestLbl->Text = DucoUtils::FormatDoubleToString(totalPealsInfo.FastestChangesPerMinute(), 2);
    towerComparision->Minimum = int(totalPealsInfo.SlowestChangesPerMinute() *100);
    towerComparision->Maximum = int(totalPealsInfo.FastestChangesPerMinute() *100);
    towerComparision->Value = int(currentPeal->ChangesPerMinute()*100);

    totalPeals->Text = DucoUtils::ConvertString(DucoEngineUtils::ToString(totalPealsInfo.TotalPeals(), true));
}

System::Void
PealStatsUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}
