#pragma once

#include <ImportCancellationCallback.h>
#include <vcclr.h>

namespace DucoUI
{
    interface class ImportCancellationCallbackUI
    {
    public:
        virtual System::Boolean ContinueImport() = 0;
    };


    class ImportCancellationWrapper : public Duco::ImportCancellationCallback
    {
    public:
        ImportCancellationWrapper(DucoUI::ImportCancellationCallbackUI^ theUiCallback);

        virtual System::Boolean ContinueImport();

    protected:
        gcroot<ImportCancellationCallbackUI^> uiCallback;
    };
}
