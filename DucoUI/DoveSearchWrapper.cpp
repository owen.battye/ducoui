#include "DoveSearchWrapper.h"

using namespace DucoUI;

DoveSearchWrapper::DoveSearchWrapper(DucoUI::DoveSearchCallbackUI^ theUiCallback)
: uiCallback(theUiCallback)
{

}

void
DoveSearchWrapper::Initialised()
{
	uiCallback->Initialised();
}

void
DoveSearchWrapper::Step(int progressPercent)
{
	uiCallback->Step(progressPercent);
}

void
DoveSearchWrapper::Complete(bool error)
{
	uiCallback->Complete();
}

void
DoveSearchWrapper::TowerFound(const Duco::DoveObject& newTower)
{
    uiCallback->TowerFound(newTower);
}
