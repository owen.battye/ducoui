#include "ProgressWrapper.h"
#include "DatabaseManager.h"
#include <DatabaseSearch.h>
#include "GenericTablePrintUtil.h"
#include "RingerDisplayCache.h"

#include "RingerSearchUI.h"

#include "SystemDefaultIcon.h"
#include "SoundUtils.h"
#include "DucoUtils.h"
#include "DucoUIUtils.h"
#include "DucoUILog.h"
#include <SearchFieldIdArgument.h>
#include <SearchFieldBoolArgument.h>
#include <SearchFieldNumberArgument.h>
#include <SearchGenderArgument.h>
#include "SearchUtils.h"
#include <RingingDatabase.h>
#include "RingingDatabaseObserver.h"
#include "DucoWindowState.h"
#include <PealDatabase.h>
#include <Ringer.h>
#include <SearchValidObject.h>
#include "RingerUI.h"
#include <DucoConfiguration.h>
#include <DatabaseSettings.h>
#include <StatisticFilters.h>
#include <chrono>
#include "RingerDisplayCache.h"
#include "ReplaceRingerUI.h"
#include <RingerDatabase.h>
#include <SearchDuplicateObject.h>

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::Windows::Forms;
using namespace std::chrono;

RingerSearchUI::RingerSearchUI(DatabaseManager^ newDatabase, bool setStartSearchInvalid)
:   database(newDatabase), startSearchInvalid(setStartSearchInvalid)
{
    foundRingerIds = gcnew System::Collections::Generic::List<System::UInt64>;
    ringerCache = RingerDisplayCache::CreateDisplayCacheWithRingers(database, true, true);
    InitializeComponent();
    progressWrapper = new ProgressCallbackWrapper(this);
    ringerCache->PopulateControl(similarRinger, false);
}

System::Void
RingerSearchUI::RingerSearchUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    if (startSearchInvalid)
    {
        invalid->Checked = true;
        searchBtn_Click(sender, e);
    }
    else if (similarRinger->SelectedIndex > 1)
    {
        searchBtn_Click(sender, e);
    }
}

void
RingerSearchUI::SetSimilarRingerId(const Duco::ObjectId& ringerId)
{
    this->similarRinger->SelectedIndex = ringerCache->FindIndexOrAdd(ringerId);
}

System::Void
RingerSearchUI::ClosingEvent( Object^ /*sender*/, System::ComponentModel::CancelEventArgs^ e )
{
    if (backgroundWorker->IsBusy)
    {
        backgroundWorker->CancelAsync();
        e->Cancel = true;
    }
}

RingerSearchUI::!RingerSearchUI()
{
    delete progressWrapper;
}

RingerSearchUI::~RingerSearchUI()
{
    this->!RingerSearchUI();
    if (components)
    {
        delete components;
    }
}

System::Void 
RingerSearchUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void 
RingerSearchUI::clearBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    resultCount->Text = "";
    surnameEditor->Text = "";
    forenameEditor->Text = "";
    notesEditor->Text = "";
    referencesEditor->Text = "";
    male->Checked = false;
    female->Checked = false;
    nonHuman->Checked = false;
    withAkas->Checked = false;
    links->Checked = false;
    showAll->Checked = false;
    invalid->Checked = false;
    nonBlankNotes->Checked = false;
    allReferencesBlank->Checked = false;
    nonBlankReferences->Checked = false;
    similarRinger->Text = "";
    ringersData->Rows->Clear();
}

System::Void 
RingerSearchUI::searchBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (backgroundWorker->IsBusy)
    {
        return;
    }

    errorsFound = false;
    searchBtn->Enabled = false;
    foundRingerIds->Clear();
    ringersData->Rows->Clear();
    resultCount->Text = "";
    DatabaseSearch* search = new DatabaseSearch(database->Database());
    if (CreateSearchParameters(*search))
    {
        SearchUIArgs^ args = gcnew SearchUIArgs();
        args->search = search;
        backgroundWorker->RunWorkerAsync(args);
    }
    else
    {
        SoundUtils::PlayErrorSound();
        delete search;
        searchBtn->Enabled = true;
    }
}

System::Void
RingerSearchUI::printBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (!backgroundWorker->IsBusy)
    {
        DucoUIUtils::Print(this, sender, gcnew System::Drawing::Printing::PrintPageEventHandler( this, &RingerSearchUI::printTowers ), "towers summary", database->Database().Settings().UsePrintPreview(), false);
    }
}

System::Void
RingerSearchUI::printTowers(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args)
{
    if (!backgroundWorker->IsBusy)
    {
        if (printUtil == nullptr)
        {
            printUtil = gcnew GenericTablePrintUtil(database, args, "Results of ringer search");
            printUtil->SetObjects(ringersData, false);
        }

        System::Drawing::Printing::PrintDocument^ printDoc = static_cast<System::Drawing::Printing::PrintDocument^>(sender);
        printUtil->printObject(args, printDoc->PrinterSettings);
        if (!args->HasMorePages)
        {
            printUtil = nullptr;
        }
    }
}

System::Boolean
RingerSearchUI::CreateSearchParameters(Duco::DatabaseSearch& search)
{
    try
    {
        if (showAll->Checked)
        {
            TSearchFieldIdArgument* newArg = new TSearchFieldIdArgument(EAllRingers, -1);
            search.AddArgument(newArg);
        }
        else
        {
            if (nonBlankNotes->Checked)
            {
                TSearchFieldBoolArgument* newNumber = new TSearchFieldBoolArgument(Duco::ENotes, true);
                search.AddArgument(newNumber);
            }
            else
                SearchUtils::AddStringSearchArgument(search, notesEditor->Text, ENotes, false);
            if (withAkas->Checked)
            {
                TSearchFieldNumberArgument* newNumber = new TSearchFieldNumberArgument(Duco::EContainsNameChanges, 1);
                search.AddArgument(newNumber);
            }
            if (links->Checked)
            {
                TSearchFieldBoolArgument* newNumber = new TSearchFieldBoolArgument(Duco::ELinkedRinger);
                search.AddArgument(newNumber);
            }
            if (male->Checked || female->Checked)
            {
                TSearchGenderArgument* newGenderArg = new TSearchGenderArgument(male->Checked, female->Checked);
                search.AddArgument(newGenderArg);
            }
            if (nonHuman->Checked)
            {
                TSearchFieldBoolArgument* newNonHumanArgument = new TSearchFieldBoolArgument(TSearchFieldId::ENonHumanRinger);
                search.AddArgument(newNonHumanArgument);
            }
            if (allReferencesBlank->Checked)
            {
                TSearchFieldBoolArgument* newNumber = new TSearchFieldBoolArgument(Duco::EAllBlankRingerReferences, true);
                search.AddArgument(newNumber);
            }
            else if (nonBlankReferences->Checked)
            {
                TSearchFieldBoolArgument* newNumber = new TSearchFieldBoolArgument(Duco::ENonBlankRingerReferences, true);
                search.AddArgument(newNumber);
            }
            else
            {
                SearchUtils::AddStringSearchArgument(search, referencesEditor->Text, EPealBaseReference, false);
            }
            SearchUtils::AddStringSearchArgument(search, forenameEditor->Text, EForename, false);
            SearchUtils::AddStringSearchArgument(search, surnameEditor->Text, ESurname, false);
            if (invalid->Checked)
            {
                TSearchValidObject* newArg = new TSearchValidObject(database->Config().IncludeWarningsInValidation());
                search.AddValidationArgument(newArg);
            }
            else if (similarRinger->SelectedIndex != -1)
            {
                RingerItem^ currentRinger = (RingerItem^)similarRinger->SelectedItem;
                if (Duco::ObjectId(currentRinger->Id()).ValidId())
                {
                    TSearchFieldIdArgument* idArg = new TSearchFieldIdArgument(ERingerId, currentRinger->Id());
                    search.AddArgument(idArg);

                    Duco::TSearchDuplicateObject* newStr = new Duco::TSearchDuplicateObject(true);
                    search.AddValidationArgument(newStr);
                }
            }
        }
    }
    catch (Exception^)
    {
        SoundUtils::PlayErrorSound();
        return false;
    }
    return true;
}

System::Void
RingerSearchUI::backgroundWorker_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
    SearchUIArgs^ args = static_cast<SearchUIArgs^>(e->Argument);
    System::ComponentModel::BackgroundWorker^ bgworker = static_cast<System::ComponentModel::BackgroundWorker^>(sender);

    std::set<ObjectId> ids;
    args->search->Search(*progressWrapper, ids, TObjectType::ERinger);
    delete args->search;
    DucoUtils::Convert(ids, foundRingerIds);
}

System::Void
RingerSearchUI::backgroundWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    progressBar->Value = e->ProgressPercentage;
}

System::Void
RingerSearchUI::backgroundWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    ringersData->RowCount = (int)foundRingerIds->Count;
    searchBtn->Enabled = true;
    progressBar->Value = 0;
    resultCount->Text = Convert::ToString(foundRingerIds->Count);
    if (foundRingerIds->Count == 0)
        SoundUtils::PlayErrorSound();
}

void
RingerSearchUI::Initialised()
{
    backgroundWorker->ReportProgress(0);
}

void
RingerSearchUI::Step(int progressPercent)
{
    backgroundWorker->ReportProgress(progressPercent);
}

void
RingerSearchUI::Complete()
{
}

System::Void
RingerSearchUI::ringersData_CellValueNeeded(System::Object^  sender, System::Windows::Forms::DataGridViewCellValueEventArgs^  e)
{
    if (e->RowIndex >= foundRingerIds->Count)
    {
        DUCOUILOGDEBUG("RingerSearchUI::ringersData_CellValueNeeded - OUT OF RANGE");
        return;
    }

    std::chrono::steady_clock::time_point start = high_resolution_clock::now();
    Duco::ObjectId ringerIdToFind(foundRingerIds[e->RowIndex]);
    Duco::Ringer foundRinger;
    bool updateErrorColumn = e->ColumnIndex == 0 || e->ColumnIndex == 7;
    if (database->FindRinger(ringerIdToFind, foundRinger, true))
    {
        std::wstring errorString = foundRinger.ErrorString(database->Database().Settings(), database->Config().IncludeWarningsInValidation());
        if (errorString.length() > 0)
        {
            errorsFound = true;
        }

        switch (e->ColumnIndex)
        {
        default:
            break;

        case 0: // Forename;
            {
            DataGridViewRow^ currentRow = ringersData->Rows[e->RowIndex];
            currentRow->HeaderCell->Value = Convert::ToString(ringerIdToFind.Id());
            e->Value = DucoUtils::ConvertString(foundRinger.Forename());
            }
            break;

        case 1: //Surname Column;
            e->Value = DucoUtils::ConvertString(foundRinger.Surname());
            break;

        case 2: //Currentname Column;
            if (foundRinger.NoOfNameChanges() != 0)
            {
                DataGridViewTextBoxCell^ currentNameCell = gcnew DataGridViewTextBoxCell();
                e->Value = DucoUtils::ConvertString(foundRinger.FullName(database->Settings().LastNameFirst()));
            }
            break;

        case 3: // First peal
        {
            Duco::StatisticFilters filters(database->Database());
            filters.SetRinger(true, foundRinger.Id());
            filters.SetIncludeLinkedRingers(false);

            PealLengthInfo info = database->Database().PealsDatabase().AllMatches(filters);
            if (info.TotalPeals() > 0)
            {
                e->Value = DucoUtils::ConvertString(info.DateOfFirstPealString());
            }
        }
        break;

        case 4: // Last peal
        {
            Duco::StatisticFilters filters(database->Database());
            filters.SetRinger(true, foundRinger.Id());
            filters.SetIncludeLinkedRingers(false);

            PealLengthInfo info = database->Database().PealsDatabase().AllMatches(filters);
            if (info.TotalPeals() > 1)
            {
                e->Value = DucoUtils::ConvertString(info.DateOfLastPealString());
            }
        }
        break;

        case 5: // Peal count
        {
            Duco::StatisticFilters filters(database->Database());
            filters.SetRinger(true, foundRinger.Id());
            filters.SetIncludeLinkedRingers(false);

            PealLengthInfo info = database->Database().PealsDatabase().AllMatches(filters);
            e->Value = Convert::ToString(info.TotalPeals());
        }
        break;

        case 6: // Gender;
        {
            if (foundRinger.NonHuman())
            {
                e->Value = gcnew String("Non human");
            }
            else if (foundRinger.Male())
                e->Value = gcnew String("Male");
            else if (foundRinger.Female())
                e->Value = gcnew String("Female");
        }
        break;

        case 7: //Error Column;
        {
            e->Value = DucoUtils::ConvertString(errorString);
        }
        break;
        }
    }
    else
    {
        updateErrorColumn = false;
    }
    if (invalidColumn->Visible != errorsFound && updateErrorColumn)
    {
        invalidColumn->Visible = errorsFound;
    }
    DUCOUILOGDEBUGWITHTIME(start, "RingerSearchUI::ringersData_CellValueNeeded Row: " + Convert::ToString(e->RowIndex) + ", Column: " + e->ColumnIndex.ToString());

}

System::Void
RingerSearchUI::RingerSearchUI_Scroll(System::Object^  sender, System::Windows::Forms::ScrollEventArgs^  e)
{
    ringersData_Scroll(sender, gcnew ScrollEventArgs(ScrollEventType::EndScroll, 0));
}

System::Void
RingerSearchUI::ringersData_Scroll(System::Object^  sender, System::Windows::Forms::ScrollEventArgs^  e)
{
    ringersData->AutoResizeColumns(System::Windows::Forms::DataGridViewAutoSizeColumnsMode::DisplayedCells);
}

System::Void
RingerSearchUI::ringersData_CellDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    try
    {
        unsigned int rowIndex = e->RowIndex;
        DataGridViewRow^ currentRow = ringersData->Rows[rowIndex];
        unsigned int ringerId = Convert::ToInt16(currentRow->HeaderCell->Value);
        RingerUI::ShowRinger(database, this->MdiParent, ringerId);
    }
    catch (Exception^)
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
RingerSearchUI::showAll_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    surnameEditor->Enabled = !surnameEditor->Enabled;
    forenameEditor->Enabled = !forenameEditor->Enabled;
    notesEditor->Enabled = !notesEditor->Enabled;
    referencesEditor->Enabled = !referencesEditor->Enabled;
    withAkas->Enabled = !withAkas->Enabled;
    male->Enabled = !male->Enabled;
    female->Enabled = !female->Enabled;
    nonBlankNotes->Enabled = !nonBlankNotes->Enabled;
    allReferencesBlank->Enabled = !allReferencesBlank->Enabled;
    nonBlankReferences->Enabled = !nonBlankReferences->Enabled;
    invalid->Enabled = !invalid->Enabled;
}

System::Void
RingerSearchUI::allReferencesBlank_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (allReferencesBlank->Checked)
    {
        referencesEditor->Text = "";
        nonBlankReferences->Checked = false;
    }
    referencesEditor->Enabled = !allReferencesBlank->Checked;
}

System::Void
RingerSearchUI::nonBlankReferences_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (nonBlankReferences->Checked)
    {
        allReferencesBlank->Checked = false;
    }
}

System::Void
RingerSearchUI::MenuStrip_Opening(System::Object^ sender, System::ComponentModel::CancelEventArgs^ e)
{
    linkRingersMenuItem->Enabled = ringersData->SelectedRows->Count == 2;
    swapRingersMenuItem->Enabled = ringersData->SelectedRows->Count == 2;
}

System::Void
RingerSearchUI::SetSelectionMale(System::Object^ sender, System::EventArgs^  e)
{
    System::Collections::IEnumerator^ it = ringersData->SelectedRows->GetEnumerator();
    while (it->MoveNext())
    {
        DataGridViewRow^ cellRow = static_cast<DataGridViewRow^>(it->Current);
        Duco::ObjectId ringerId = Convert::ToInt16(cellRow->HeaderCell->Value);
        if (ringerId.ValidId())
        {
            database->SetRingerGender(ringerId, true);
        }
    }
}

System::Void
RingerSearchUI::SetSelectionFemale(System::Object^ sender, System::EventArgs^  e)
{
    System::Collections::IEnumerator^ it = ringersData->SelectedRows->GetEnumerator();
    while (it->MoveNext())
    {
        DataGridViewRow^ cellRow = static_cast<DataGridViewRow^>(it->Current);
        Duco::ObjectId ringerId = Convert::ToInt16(cellRow->HeaderCell->Value);
        if (ringerId.ValidId())
        {
            database->SetRingerGender(ringerId, false);
        }
    }
}

System::Void
RingerSearchUI::LinkRingers(System::Object^ sender, System::EventArgs^ e)
{
    std::set<Duco::ObjectId> ringerIds;
    System::Collections::IEnumerator^ it = ringersData->SelectedRows->GetEnumerator();
    while (it->MoveNext())
    {
        DataGridViewRow^ cellRow = static_cast<DataGridViewRow^>(it->Current);
        Duco::ObjectId ringerId = Convert::ToInt16(cellRow->HeaderCell->Value);
        if (ringerId.ValidId())
        {
            ringerIds.insert(ringerId);
        }
    }
    if (ringerIds.size() == 2)
    {
        if (database->Database().PealsDatabase().AnyPealContainingBothRingers(*ringerIds.begin(), *ringerIds.rbegin(), true))
        {
            MessageBox::Show(this, "Cannot link these ringers, both ringers already exist in a single peal!", "Both ringers already exist in single peal", MessageBoxButtons::OK, MessageBoxIcon::Warning);
            return;
        }
        if (!database->LinkRingers(*ringerIds.begin(), *ringerIds.rbegin()))
        {
            MessageBox::Show(this, "Could not link ringers", "Could not link ringers", MessageBoxButtons::OK, MessageBoxIcon::Warning);
        }
    }
}

System::Void
RingerSearchUI::SwapRingers(System::Object^ sender, System::EventArgs^ e)
{
    std::set<Duco::ObjectId> ringerIds;
    System::Collections::IEnumerator^ it = ringersData->SelectedRows->GetEnumerator();
    while (it->MoveNext())
    {
        DataGridViewRow^ cellRow = static_cast<DataGridViewRow^>(it->Current);
        Duco::ObjectId ringerId = Convert::ToInt16(cellRow->HeaderCell->Value);
        if (ringerId.ValidId())
        {
            ringerIds.insert(ringerId);
        }
    }
    if (ringerIds.size() == 2)
    {
        ReplaceRingerUI^ uiForm = gcnew ReplaceRingerUI(database, *ringerIds.begin(), *ringerIds.rbegin());
        uiForm->MdiParent = this->MdiParent;
        uiForm->Show();
    }
}

System::Void
RingerSearchUI::copyFromTableCmd_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (ringersData->GetCellCount(DataGridViewElementStates::Selected) > 0)
    {
        // Add the selection to the clipboard.
        Clipboard::SetDataObject(ringersData->GetClipboardContent());
    }
}

System::Void
RingerSearchUI::similarRinger_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e)
{
    bool disableErrorSearch = false;
    RingerItem^ currentRinger = (RingerItem^)similarRinger->SelectedItem;
    if (Duco::ObjectId(currentRinger->Id()).ValidId())
    {
        disableErrorSearch = true;
    }
    invalid->Enabled = !disableErrorSearch;
}

System::Void
RingerSearchUI::invalid_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
{
    similarRinger->Enabled = !invalid->Checked;
}
