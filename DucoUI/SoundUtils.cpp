#include "SoundUtils.h"

using namespace DucoUI;

System::Void 
SoundUtils::PlayErrorSound()
{
    System::Media::SystemSounds::Exclamation->Play();
}

System::Void 
SoundUtils::PlayFinishedSound()
{
    System::Media::SystemSounds::Beep->Play();
}