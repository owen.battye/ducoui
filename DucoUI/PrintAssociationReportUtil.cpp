#include "PrintAssociationReportUtil.h"
#include "DatabaseManager.h"
#include "DucoCommon.h"
#include "DucoUtils.h"
#include <Peal.h>
#include <PealLengthInfo.h>
#include <DatabaseSettings.h>
#include <RingingDatabase.h>
#include <PealDatabase.h>
#include <TowerDatabase.h>
#include <Ring.h>
#include <Ringer.h>
#include <Tower.h>
#include <Method.h>
#include <StatisticFilters.h>

using namespace System;
using namespace System::Drawing;
using namespace Duco;
using namespace DucoUI;

PrintAssociationReportUtil::PrintAssociationReportUtil(DatabaseManager^ theDatabase, System::Drawing::Printing::PrintPageEventArgs^ printArgs,
      float fontSize, Int32 newNoOfColumns, float newColumnSeperation, System::String^ fontName)
:   PrintUtilBase(theDatabase, fontSize, printArgs, false)
{
    noOfColumns = newNoOfColumns;
    maxWidth = (float)(printArgs->MarginBounds.Width / noOfColumns);
    pealYear = KNoId;
    pealIds = NULL;
    lastPrintedYear = KNoId;
    SetFont(fontName, fontSize, printArgs);

    columnSeperation = newColumnSeperation;
    maxWidth -= columnSeperation;
}

PrintAssociationReportUtil::~PrintAssociationReportUtil()
{
    this->!PrintAssociationReportUtil();
}

PrintAssociationReportUtil::!PrintAssociationReportUtil()
{
    if (pealIds != NULL)
    {
        delete pealIds;
        pealIds = NULL;
    }
}

System::Void
PrintAssociationReportUtil::FindPeals(unsigned int newPealYear, const std::set<Duco::ObjectId>& additionalPealIds, unsigned int newNumberingStart)
{
    pealYear = newPealYear;
    pealIds = new std::set<Duco::ObjectId>();

    if (pealYear == KNoId)
    {
        database->Database().PealsDatabase().GetAllObjectIds(*pealIds);
    }
    else
    {
        database->Database().PealsDatabase().GetPealsForYear(pealYear, *pealIds);
        std::set<Duco::ObjectId>::const_iterator it = additionalPealIds.begin();
        while (it != additionalPealIds.end())
        {
            pealIds->emplace(*it);
            ++it;
        }
    }
    if (newNumberingStart != KNoId)
    {
        indexId = new Duco::ObjectId(newNumberingStart);
    }

    lastPealId = new Duco::ObjectId(KNoId);
}

System::Void
PrintAssociationReportUtil::printObject(System::Drawing::Printing::PrintPageEventArgs^ args, System::Drawing::Printing::PrinterSettings^ printSettings)
{
    addDucoFooter(smallFont, gcnew StringFormat, args);
    if (pealIds == NULL)
    {
        std::set<Duco::ObjectId> additionalPealIds;
        FindPeals(KNoId, additionalPealIds, KNoId);
    }
    std::set<Duco::ObjectId>::const_iterator pealId = pealIds->begin();

    if (lastPealId->ValidId())
    {
        while (pealId != pealIds->end() && (*pealId) != (*lastPealId))
        {
            ++pealId;
        }
        ++pealId;
    }

    unsigned int columnNumber = 1;
    leftMargin = (float)args->MarginBounds.Left;
    ResetPosition(args);
    while (pealId != pealIds->end())
    {
        const Duco::Peal* currentPeal = database->Database().PealsDatabase().FindPeal(*pealId);

        unsigned int noOfLinesReqForNextPeal = measurePeal(*currentPeal, maxWidth, args);
        if (noOfLinesReqForNextPeal > noOfLines)
        {
            ResetPosition(args);
            leftMargin += maxWidth;
            leftMargin += columnSeperation;

            columnNumber += 1;
            if (columnNumber > noOfColumns)
            {
                break;
            }
        }
        printPeal(*currentPeal, maxWidth, args);
        *lastPealId = *pealId;
        ++pealId;
    }

    if (pealId == pealIds->end())
    {
        lastPealId->ClearId();
        args->HasMorePages = false;
    }
    else
    {
        args->HasMorePages = true;
    }
}

unsigned int
PrintAssociationReportUtil::measurePeal(const Duco::Peal& currentPeal, System::Single maxWidth, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
{
    unsigned int numberOfLines = 4;
    if (lastPrintedYear != currentPeal.Date().Year())
    {
        numberOfLines += MeasureLongStringHeight(GetTextForHeaderLine(currentPeal), gcnew StringFormat, largeFont, maxWidth, printArgs);
        numberOfLines += 1;
    }
    if (pealYear != KNoId && currentPeal.Date().Year() == pealYear)
    {
        numberOfLines += MeasureLongStringHeight(GetTextForDateTimeLine(currentPeal, false), gcnew StringFormat, normalFont, maxWidth, printArgs);
    }

    const Duco::Tower* currentTower = database->Database().TowersDatabase().FindTower(currentPeal.TowerId());
    if (currentTower != NULL)
    {
        numberOfLines += MeasureLongStringHeight(GetTextForLocationLineOne(*currentTower), gcnew StringFormat, boldFont, maxWidth, printArgs);
        numberOfLines += MeasureLongStringHeight(GetTextForLocationLineTwo(*currentTower), gcnew StringFormat, normalFont, maxWidth, printArgs);
    }

    // Date and time line
    numberOfLines += MeasureLongStringHeight(GetTextForDateLine(currentPeal), gcnew StringFormat, normalFont, maxWidth, printArgs);

    if (currentTower != NULL)
    {
        // Date and time line
        numberOfLines += MeasureLongStringHeight(GetTextForTimeLine(currentPeal, *currentTower), gcnew StringFormat, normalFont, maxWidth, printArgs);
    }

    // Method line
    numberOfLines += MeasureLongStringHeight(GetTextForMethodLine(currentPeal), gcnew StringFormat, normalFont, maxWidth, printArgs);

    if (currentPeal.ContainsMultiMethods())
    {
        numberOfLines += MeasureLongStringHeight(DucoUtils::ConvertString(currentPeal.MultiMethods()), gcnew StringFormat, normalFont, maxWidth, printArgs);
    }
    // Composer line
    if (currentPeal.ContainsComposer())
    {
        numberOfLines += MeasureLongStringHeight(GetTextForComposerLine(currentPeal), gcnew StringFormat, normalFont, maxWidth, printArgs);
    }

    numberOfLines += MeasureLongStringHeight(GetTextForRingerLines(currentPeal), GetStringFormatForRingerLines(currentPeal.Handbell(), printArgs), normalFont, maxWidth, printArgs);

    if (currentPeal.ContainsFootnotes())
    {
        numberOfLines += MeasureLongStringHeight(DucoUtils::ConvertString(currentPeal.Footnotes()), gcnew StringFormat, normalFont, maxWidth, printArgs);
    }

    return numberOfLines;
}

System::Void
PrintAssociationReportUtil::printPeal(const Duco::Peal& currentPeal, System::Single maxWidths,
    System::Drawing::Printing::PrintPageEventArgs^ printArgs)
{
    StringFormat^ stringFormat = gcnew StringFormat;
    array<Single>^ tabStops = { 20.0f };
    Single firstTab(0.0f);
    stringFormat->SetTabStops(firstTab, tabStops);
    const Duco::Tower* currentTower = database->Database().TowersDatabase().FindTower(currentPeal.TowerId());

    if (lastPrintedYear != currentPeal.Date().Year())
    {
        PrintLongString(GetTextForHeaderLine(currentPeal), stringFormat, largeFont, leftMargin, maxWidth, printArgs);
        yPos += smallFont->Height;
        noOfLines -= 1;
        lastPrintedYear = currentPeal.Date().Year();
    }
    if (pealYear != KNoId && currentPeal.Date().Year() == pealYear)
    {
        PrintLongString(GetTextForDateTimeLine(currentPeal, true), stringFormat, normalFont, leftMargin, maxWidth, printArgs);
    }

    if (currentTower != NULL)
    {
        // TODO: Adding tenor weight to shortest line (maybe)
        PrintLongString(GetTextForLocationLineOne(*currentTower), stringFormat, boldFont, leftMargin, maxWidth, printArgs);
        PrintLongString(GetTextForLocationLineTwo(*currentTower), stringFormat, normalFont, leftMargin, maxWidth, printArgs);
    }

    // Date and time lines
    PrintLongString(GetTextForDateLine(currentPeal), stringFormat, normalFont, leftMargin, maxWidth, printArgs);
    if (currentTower != NULL)
    {
        PrintLongString(GetTextForTimeLine(currentPeal, *currentTower), stringFormat, normalFont, leftMargin, maxWidth, printArgs);
    }

    // Method line
    PrintLongString(GetTextForMethodLine(currentPeal), stringFormat, normalFont, leftMargin, maxWidth, printArgs);

    if (currentPeal.ContainsMultiMethods())
    {
        PrintLongString(DucoUtils::ConvertString(currentPeal.MultiMethods()), stringFormat, normalFont, leftMargin, maxWidth, printArgs);
    }
    // Composer line
    if (currentPeal.ContainsComposer())
    {
        PrintLongString(GetTextForComposerLine(currentPeal), stringFormat, normalFont, leftMargin, maxWidth, printArgs);
    }

    PrintLongString(GetTextForRingerLines(currentPeal), GetStringFormatForRingerLines(currentPeal.Handbell(), printArgs), normalFont, leftMargin, maxWidth, printArgs);

    if (currentPeal.ContainsFootnotes())
    {
        PrintLongString(DucoUtils::ConvertString(currentPeal.Footnotes()), stringFormat, normalFont, leftMargin, maxWidth, printArgs);
    }

    yPos += boldFont->Height;
    if (noOfLines > 0)
    noOfLines -= 1;
}

System::String^
PrintAssociationReportUtil::GetTextForDateTimeLine(const Duco::Peal& currentPeal, System::Boolean incrementPealCount)
{
    String^ printBuffer = Convert::ToString(currentPeal.Id().Id());
    if (indexId != NULL)
    {
        printBuffer = Convert::ToString(indexId->Id());
        if (incrementPealCount)
        {
            Duco::ObjectId newIndexId(indexId->Id() + 1);
            delete indexId;
            indexId = new ObjectId(newIndexId);
        }
    }
    return printBuffer;
}

System::String^
PrintAssociationReportUtil::GetTextForLocationLineOne(const Duco::Tower& currentTower)
{
    String^ locationLine = DucoUtils::ConvertString(currentTower.City()) + ",";
    return locationLine;
}

System::String^
PrintAssociationReportUtil::GetTextForLocationLineTwo(const Duco::Tower& currentTower)
{
    if (currentTower.County().length() > 0)
        return DucoUtils::ConvertString(currentTower.County()) + ", " + DucoUtils::ConvertString(currentTower.Name());

    return DucoUtils::ConvertString(currentTower.Name());
}

System::String^
PrintAssociationReportUtil::GetTextForDateLine(const Duco::Peal& currentPeal)
{
    String^ dateLine = DucoUtils::ConvertString(currentPeal.Date().DateForAssociationReports());
    return dateLine;
}

System::String^
PrintAssociationReportUtil::GetTextForTimeLine(const Duco::Peal& currentPeal, const Duco::Tower& currentTower)
{
    String^ timeLine = DucoUtils::ConvertString(currentPeal.Time().TimeForAssociationReports());
    const Ring* const theRing = currentTower.FindRing(currentPeal.RingId());
    System::Boolean showTenorWeights = false;
    if (theRing != NULL)
    {
        if (!currentTower.RingOnBackBells(*theRing))
        {
            showTenorWeights = true;
        }
    }

    if (showTenorWeights)
    {
        String^ ringLine = "";
        if (theRing != NULL && theRing->TenorWeight().length() > 0)
        {
            ringLine = " (";
            ringLine += DucoUtils::ConvertString(theRing->TenorWeight());
            ringLine += ")";
        }
        if (ringLine->Length > 0)
        {
            timeLine += ringLine;
        }
    }
    return timeLine;
}

System::String^
PrintAssociationReportUtil::GetTextForMethodLine(const Duco::Peal& currentPeal)
{
    String^ methodLine = Convert::ToString(currentPeal.NoOfChanges());
    Method theMethod;
    if (database->FindMethod(currentPeal.MethodId(), theMethod, false))
    {
        methodLine += " " + DucoUtils::ConvertString(theMethod.FullName(database->Database()));
    }
    return methodLine;
}

System::String^
PrintAssociationReportUtil::GetTextForComposerLine(const Duco::Peal& currentPeal)
{
    String^ composerLine = "Comp: ";
    composerLine += DucoUtils::ConvertString(currentPeal.Composer());
    return composerLine;
}

System::String^
PrintAssociationReportUtil::GetTextForHeaderLine(const Duco::Peal& currentPeal)
{
    String^ printBuffer = "Quarters ";
    if (database->Settings().PealDatabase())
    {
        printBuffer = "Peals ";
    }
    printBuffer += "rung in " + Convert::ToString(currentPeal.Date().Year());
    return printBuffer;
}

System::String^
PrintAssociationReportUtil::GetTextForRingerLines(const Duco::Peal& currentPeal)
{
    String^ printBuffer = "";
    for (unsigned int bellNo(1); bellNo <= currentPeal.NoOfBellsRung(database->Database()); ++bellNo)
    {
        Duco::ObjectId ringerId;
        if (currentPeal.RingerId(bellNo, false, ringerId))
        {
            String^ ringerLine = "";
            std::wstring bellNumbers;
            if (currentPeal.Handbell() && currentPeal.HandBellsRung(ringerId, bellNumbers))
            {
                ringerLine += DucoUtils::ConvertString(bellNumbers);
            }
            else
            {
                ringerLine = Convert::ToString(bellNo);
            }
            ringerLine += "\t";
            Duco::Ringer theRinger;
            if (database->FindRinger(ringerId, theRinger, false))
            {
                ringerLine += DucoUtils::ConvertString(theRinger.FullName(currentPeal.Date(), database->Settings().PrintSurnameFirst()));
                if (currentPeal.ContainsConductor(ringerId))
                {
                    ringerLine += " (C)";
                }
                printBuffer += ringerLine + "\n";
            }
        }
        if (currentPeal.RingerId(bellNo, true, ringerId))
        {
            String^ ringerLine = "Strapper\t";
            Duco::Ringer theRinger;
            if (database->FindRinger(ringerId, theRinger, false))
            {
                ringerLine += DucoUtils::ConvertString(theRinger.FullName(currentPeal.Date(), database->Settings().PrintSurnameFirst()));
                printBuffer += ringerLine + "\n";
            }
        }
    }
    return printBuffer;
}

System::Drawing::StringFormat^
PrintAssociationReportUtil::GetStringFormatForRingerLines(System::Boolean handbell, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
{
    RectangleF drawInside(0, 0, 100, 100);
    int linesFilled(0);
    int charactersFitted(0);
    SizeF size = printArgs->Graphics->MeasureString("99 ", normalFont, drawInside.Size, gcnew StringFormat, charactersFitted, linesFilled);
    if (handbell)
    {
        size = printArgs->Graphics->MeasureString("13-14 ", normalFont, drawInside.Size, gcnew StringFormat, charactersFitted, linesFilled);
    }

    array<Single>^ ringerTabStops = { size.Width };
    StringFormat^ ringerStringFormat = gcnew StringFormat();
    ringerStringFormat->SetTabStops(0.0f, ringerTabStops);
    return ringerStringFormat;
}
