#pragma once
namespace DucoUI
{
    public ref class MethodStatsUI : public System::Windows::Forms::Form, public DucoUI::RingingDatabaseObserver
    {
    public:
        MethodStatsUI(DucoUI::DatabaseManager^ theDatabase);
        MethodStatsUI(DucoUI::DatabaseManager^ theDatabase, const Duco::StatisticFilters& newFilters);

        // from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

    protected:
        !MethodStatsUI();
        ~MethodStatsUI();
        System::Void MethodStatsUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void filtersBtn_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void tabbedControl_Selected(System::Object^  sender, System::Windows::Forms::TabControlEventArgs^  e);

        System::Void nameData_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);
        System::Void methodData_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);
        System::Void typesData_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);
        System::Void stagesData_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);

        System::Void backgroundGenerator_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void backgroundGenerator_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void backgroundGenerator_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);

        System::Void GenerateMethodCounts(System::ComponentModel::DoWorkEventArgs^ e);
        System::Void GenerateNames(System::ComponentModel::DoWorkEventArgs^ e);
        System::Void GenerateTypes(System::ComponentModel::DoWorkEventArgs^ e);
        System::Void GenerateStages(System::ComponentModel::DoWorkEventArgs^ e);
        System::Windows::Forms::TabControl^  tabbedControl;

    protected:




    private: System::Windows::Forms::DataGridViewTextBoxColumn^ namePealCount;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ namesChangesColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ nameTimeColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ typesCount;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ typesChangesColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ typeTimeColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ stagesCount;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ stagesChangesColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ stagesTimeColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ Id;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ MethodName;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ methodCount;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ methodsChangesColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ methodTimeColumn;











    protected:

        enum class TState
        {
            ENon = 0,
            EMethods,
            ENames,
            ETypes,
            EStages
        } state;

        System::Void AddStage(const std::pair<unsigned int, Duco::PealLengthInfo>& object, int percentProgress, bool spliced);
        System::Void StartGenerator();
        System::Void Restart(MethodStatsUI::TState newState);

        System::Void methodData_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);

    private:
        DucoUI::DatabaseManager^                      database;
        Duco::StatisticFilters*                             filters;

        System::Windows::Forms::DataGridView^  methodData;
        System::Windows::Forms::DataGridView^  nameData;
        System::Windows::Forms::DataGridView^  typesData;
        System::Windows::Forms::DataGridView^  stagesData;








        System::Windows::Forms::ToolStripProgressBar^       progressBar;

        System::ComponentModel::BackgroundWorker^           backgroundGenerator;
        System::ComponentModel::Container^                  components;
        TState nextState;
        bool methodsGenerated;
        bool namesGenerated;
        bool typesGenerated;
        bool stagesGenerated;

        ref class MethodStatsUIArgs
        {
        public:
            MethodStatsUI::TState  newState;
        };


        System::Void InitializeComponent()
        {
            System::Windows::Forms::StatusStrip^ statusStrip1;
            System::Windows::Forms::TabPage^ methodsPage;
            System::Windows::Forms::TabPage^ typesPage;
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle3 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle4 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::TabPage^ stagesPage;
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle5 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle6 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::TabPage^ namesPage;
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle7 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle8 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::ToolStrip^ toolStrip1;
            System::Windows::Forms::ToolStripButton^ filtersBtn;
            System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(MethodStatsUI::typeid));
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle1 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle2 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            this->progressBar = (gcnew System::Windows::Forms::ToolStripProgressBar());
            this->methodData = (gcnew System::Windows::Forms::DataGridView());
            this->typesData = (gcnew System::Windows::Forms::DataGridView());
            this->typesCount = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->typesChangesColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->typeTimeColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->stagesData = (gcnew System::Windows::Forms::DataGridView());
            this->stagesCount = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->stagesChangesColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->stagesTimeColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->nameData = (gcnew System::Windows::Forms::DataGridView());
            this->namePealCount = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->namesChangesColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->nameTimeColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->tabbedControl = (gcnew System::Windows::Forms::TabControl());
            this->backgroundGenerator = (gcnew System::ComponentModel::BackgroundWorker());
            this->Id = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->MethodName = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->methodCount = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->methodsChangesColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->methodTimeColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            methodsPage = (gcnew System::Windows::Forms::TabPage());
            typesPage = (gcnew System::Windows::Forms::TabPage());
            stagesPage = (gcnew System::Windows::Forms::TabPage());
            namesPage = (gcnew System::Windows::Forms::TabPage());
            toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
            filtersBtn = (gcnew System::Windows::Forms::ToolStripButton());
            statusStrip1->SuspendLayout();
            methodsPage->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->methodData))->BeginInit();
            typesPage->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->typesData))->BeginInit();
            stagesPage->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->stagesData))->BeginInit();
            namesPage->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->nameData))->BeginInit();
            toolStrip1->SuspendLayout();
            this->tabbedControl->SuspendLayout();
            this->SuspendLayout();
            // 
            // statusStrip1
            // 
            statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->progressBar });
            statusStrip1->Location = System::Drawing::Point(0, 580);
            statusStrip1->Name = L"statusStrip1";
            statusStrip1->Size = System::Drawing::Size(555, 22);
            statusStrip1->TabIndex = 0;
            statusStrip1->Text = L"statusStrip1";
            // 
            // progressBar
            // 
            this->progressBar->Name = L"progressBar";
            this->progressBar->Size = System::Drawing::Size(100, 16);
            // 
            // methodsPage
            // 
            methodsPage->Controls->Add(this->methodData);
            methodsPage->Location = System::Drawing::Point(4, 22);
            methodsPage->Name = L"methodsPage";
            methodsPage->Padding = System::Windows::Forms::Padding(3);
            methodsPage->Size = System::Drawing::Size(547, 529);
            methodsPage->TabIndex = 0;
            methodsPage->Text = L"Methods";
            methodsPage->UseVisualStyleBackColor = true;
            // 
            // methodData
            // 
            this->methodData->AllowUserToAddRows = false;
            this->methodData->AllowUserToDeleteRows = false;
            this->methodData->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::AllCells;
            this->methodData->AutoSizeRowsMode = System::Windows::Forms::DataGridViewAutoSizeRowsMode::AllCells;
            this->methodData->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->methodData->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(5) {
                this->Id, this->MethodName,
                    this->methodCount, this->methodsChangesColumn, this->methodTimeColumn
            });
            this->methodData->Dock = System::Windows::Forms::DockStyle::Fill;
            this->methodData->Location = System::Drawing::Point(3, 3);
            this->methodData->Margin = System::Windows::Forms::Padding(3, 28, 3, 3);
            this->methodData->Name = L"methodData";
            this->methodData->ReadOnly = true;
            this->methodData->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
            this->methodData->Size = System::Drawing::Size(541, 523);
            this->methodData->TabIndex = 0;
            this->methodData->CellContentDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &MethodStatsUI::methodData_CellContentDoubleClick);
            this->methodData->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &MethodStatsUI::methodData_ColumnHeaderMouseClick);
            // 
            // typesPage
            // 
            typesPage->Controls->Add(this->typesData);
            typesPage->Location = System::Drawing::Point(4, 22);
            typesPage->Name = L"typesPage";
            typesPage->Padding = System::Windows::Forms::Padding(3);
            typesPage->Size = System::Drawing::Size(547, 529);
            typesPage->TabIndex = 1;
            typesPage->Text = L"Types";
            typesPage->UseVisualStyleBackColor = true;
            // 
            // typesData
            // 
            this->typesData->AllowUserToAddRows = false;
            this->typesData->AllowUserToDeleteRows = false;
            this->typesData->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->typesData->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(3) {
                this->typesCount,
                    this->typesChangesColumn, this->typeTimeColumn
            });
            this->typesData->Dock = System::Windows::Forms::DockStyle::Fill;
            this->typesData->Location = System::Drawing::Point(3, 3);
            this->typesData->Name = L"typesData";
            this->typesData->ReadOnly = true;
            this->typesData->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
            this->typesData->Size = System::Drawing::Size(541, 523);
            this->typesData->TabIndex = 0;
            this->typesData->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &MethodStatsUI::typesData_ColumnHeaderMouseClick);
            // 
            // typesCount
            // 
            this->typesCount->HeaderText = L"Performances";
            this->typesCount->Name = L"typesCount";
            this->typesCount->ReadOnly = true;
            this->typesCount->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            // 
            // typesChangesColumn
            // 
            this->typesChangesColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            dataGridViewCellStyle3->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->typesChangesColumn->DefaultCellStyle = dataGridViewCellStyle3;
            this->typesChangesColumn->HeaderText = L"Changes";
            this->typesChangesColumn->Name = L"typesChangesColumn";
            this->typesChangesColumn->ReadOnly = true;
            this->typesChangesColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->typesChangesColumn->Width = 74;
            // 
            // typeTimeColumn
            // 
            this->typeTimeColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            dataGridViewCellStyle4->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->typeTimeColumn->DefaultCellStyle = dataGridViewCellStyle4;
            this->typeTimeColumn->HeaderText = L"Time";
            this->typeTimeColumn->Name = L"typeTimeColumn";
            this->typeTimeColumn->ReadOnly = true;
            this->typeTimeColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->typeTimeColumn->Width = 55;
            // 
            // stagesPage
            // 
            stagesPage->Controls->Add(this->stagesData);
            stagesPage->Location = System::Drawing::Point(4, 22);
            stagesPage->Name = L"stagesPage";
            stagesPage->Padding = System::Windows::Forms::Padding(3);
            stagesPage->Size = System::Drawing::Size(547, 529);
            stagesPage->TabIndex = 2;
            stagesPage->Text = L"Stages";
            stagesPage->UseVisualStyleBackColor = true;
            // 
            // stagesData
            // 
            this->stagesData->AllowUserToAddRows = false;
            this->stagesData->AllowUserToDeleteRows = false;
            this->stagesData->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->stagesData->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(3) {
                this->stagesCount,
                    this->stagesChangesColumn, this->stagesTimeColumn
            });
            this->stagesData->Dock = System::Windows::Forms::DockStyle::Fill;
            this->stagesData->Location = System::Drawing::Point(3, 3);
            this->stagesData->Name = L"stagesData";
            this->stagesData->ReadOnly = true;
            this->stagesData->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
            this->stagesData->Size = System::Drawing::Size(541, 523);
            this->stagesData->TabIndex = 0;
            this->stagesData->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &MethodStatsUI::stagesData_ColumnHeaderMouseClick);
            // 
            // stagesCount
            // 
            this->stagesCount->HeaderText = L"Performances";
            this->stagesCount->Name = L"stagesCount";
            this->stagesCount->ReadOnly = true;
            this->stagesCount->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            // 
            // stagesChangesColumn
            // 
            this->stagesChangesColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            dataGridViewCellStyle5->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->stagesChangesColumn->DefaultCellStyle = dataGridViewCellStyle5;
            this->stagesChangesColumn->HeaderText = L"Changes";
            this->stagesChangesColumn->Name = L"stagesChangesColumn";
            this->stagesChangesColumn->ReadOnly = true;
            this->stagesChangesColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->stagesChangesColumn->Width = 74;
            // 
            // stagesTimeColumn
            // 
            this->stagesTimeColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            dataGridViewCellStyle6->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->stagesTimeColumn->DefaultCellStyle = dataGridViewCellStyle6;
            this->stagesTimeColumn->HeaderText = L"Time";
            this->stagesTimeColumn->Name = L"stagesTimeColumn";
            this->stagesTimeColumn->ReadOnly = true;
            this->stagesTimeColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->stagesTimeColumn->Width = 55;
            // 
            // namesPage
            // 
            namesPage->Controls->Add(this->nameData);
            namesPage->Location = System::Drawing::Point(4, 22);
            namesPage->Name = L"namesPage";
            namesPage->Padding = System::Windows::Forms::Padding(3);
            namesPage->Size = System::Drawing::Size(547, 529);
            namesPage->TabIndex = 3;
            namesPage->Text = L"Names";
            namesPage->UseVisualStyleBackColor = true;
            // 
            // nameData
            // 
            this->nameData->AllowUserToAddRows = false;
            this->nameData->AllowUserToDeleteRows = false;
            this->nameData->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::DisplayedCells;
            this->nameData->AutoSizeRowsMode = System::Windows::Forms::DataGridViewAutoSizeRowsMode::DisplayedCells;
            this->nameData->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(3) {
                this->namePealCount,
                    this->namesChangesColumn, this->nameTimeColumn
            });
            this->nameData->Dock = System::Windows::Forms::DockStyle::Fill;
            this->nameData->Location = System::Drawing::Point(3, 3);
            this->nameData->Name = L"nameData";
            this->nameData->ReadOnly = true;
            this->nameData->Size = System::Drawing::Size(541, 523);
            this->nameData->TabIndex = 0;
            this->nameData->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &MethodStatsUI::nameData_ColumnHeaderMouseClick);
            // 
            // namePealCount
            // 
            this->namePealCount->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            this->namePealCount->HeaderText = L"Performances";
            this->namePealCount->Name = L"namePealCount";
            this->namePealCount->ReadOnly = true;
            this->namePealCount->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->namePealCount->Width = 97;
            // 
            // namesChangesColumn
            // 
            this->namesChangesColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            dataGridViewCellStyle7->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->namesChangesColumn->DefaultCellStyle = dataGridViewCellStyle7;
            this->namesChangesColumn->HeaderText = L"Changes";
            this->namesChangesColumn->Name = L"namesChangesColumn";
            this->namesChangesColumn->ReadOnly = true;
            this->namesChangesColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->namesChangesColumn->Width = 74;
            // 
            // nameTimeColumn
            // 
            this->nameTimeColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            dataGridViewCellStyle8->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->nameTimeColumn->DefaultCellStyle = dataGridViewCellStyle8;
            this->nameTimeColumn->HeaderText = L"Time";
            this->nameTimeColumn->Name = L"nameTimeColumn";
            this->nameTimeColumn->ReadOnly = true;
            this->nameTimeColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->nameTimeColumn->Width = 55;
            // 
            // toolStrip1
            // 
            toolStrip1->GripStyle = System::Windows::Forms::ToolStripGripStyle::Hidden;
            toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { filtersBtn });
            toolStrip1->Location = System::Drawing::Point(0, 0);
            toolStrip1->Name = L"toolStrip1";
            toolStrip1->Size = System::Drawing::Size(555, 25);
            toolStrip1->TabIndex = 1;
            toolStrip1->Text = L"toolStrip1";
            // 
            // filtersBtn
            // 
            filtersBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
            filtersBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"filtersBtn.Image")));
            filtersBtn->ImageTransparentColor = System::Drawing::Color::Magenta;
            filtersBtn->Name = L"filtersBtn";
            filtersBtn->Size = System::Drawing::Size(42, 22);
            filtersBtn->Text = L"&Filters";
            filtersBtn->Click += gcnew System::EventHandler(this, &MethodStatsUI::filtersBtn_Click);
            // 
            // tabbedControl
            // 
            this->tabbedControl->Controls->Add(methodsPage);
            this->tabbedControl->Controls->Add(namesPage);
            this->tabbedControl->Controls->Add(typesPage);
            this->tabbedControl->Controls->Add(stagesPage);
            this->tabbedControl->Dock = System::Windows::Forms::DockStyle::Fill;
            this->tabbedControl->Location = System::Drawing::Point(0, 25);
            this->tabbedControl->Name = L"tabbedControl";
            this->tabbedControl->SelectedIndex = 0;
            this->tabbedControl->Size = System::Drawing::Size(555, 555);
            this->tabbedControl->TabIndex = 2;
            this->tabbedControl->Selected += gcnew System::Windows::Forms::TabControlEventHandler(this, &MethodStatsUI::tabbedControl_Selected);
            // 
            // backgroundGenerator
            // 
            this->backgroundGenerator->WorkerReportsProgress = true;
            this->backgroundGenerator->WorkerSupportsCancellation = true;
            this->backgroundGenerator->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &MethodStatsUI::backgroundGenerator_DoWork);
            this->backgroundGenerator->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &MethodStatsUI::backgroundGenerator_ProgressChanged);
            this->backgroundGenerator->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &MethodStatsUI::backgroundGenerator_RunWorkerCompleted);
            // 
            // Id
            // 
            this->Id->HeaderText = L"IdColumn";
            this->Id->Name = L"Id";
            this->Id->ReadOnly = true;
            this->Id->Visible = false;
            this->Id->Width = 76;
            // 
            // MethodName
            // 
            this->MethodName->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
            this->MethodName->HeaderText = L"Name";
            this->MethodName->Name = L"MethodName";
            this->MethodName->ReadOnly = true;
            this->MethodName->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->MethodName->ToolTipText = L"Full method name";
            // 
            // methodCount
            // 
            this->methodCount->HeaderText = L"Count";
            this->methodCount->Name = L"methodCount";
            this->methodCount->ReadOnly = true;
            this->methodCount->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->methodCount->ToolTipText = L"No of performances";
            this->methodCount->Width = 60;
            // 
            // methodsChangesColumn
            // 
            dataGridViewCellStyle1->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->methodsChangesColumn->DefaultCellStyle = dataGridViewCellStyle1;
            this->methodsChangesColumn->HeaderText = L"Changes";
            this->methodsChangesColumn->Name = L"methodsChangesColumn";
            this->methodsChangesColumn->ReadOnly = true;
            this->methodsChangesColumn->Width = 74;
            // 
            // methodTimeColumn
            // 
            this->methodTimeColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            dataGridViewCellStyle2->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->methodTimeColumn->DefaultCellStyle = dataGridViewCellStyle2;
            this->methodTimeColumn->HeaderText = L"Time";
            this->methodTimeColumn->Name = L"methodTimeColumn";
            this->methodTimeColumn->ReadOnly = true;
            this->methodTimeColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->methodTimeColumn->Width = 55;
            // 
            // MethodStatsUI
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(555, 602);
            this->Controls->Add(this->tabbedControl);
            this->Controls->Add(toolStrip1);
            this->Controls->Add(statusStrip1);
            this->Name = L"MethodStatsUI";
            this->Text = L"Methods";
            this->Load += gcnew System::EventHandler(this, &MethodStatsUI::MethodStatsUI_Load);
            statusStrip1->ResumeLayout(false);
            statusStrip1->PerformLayout();
            methodsPage->ResumeLayout(false);
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->methodData))->EndInit();
            typesPage->ResumeLayout(false);
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->typesData))->EndInit();
            stagesPage->ResumeLayout(false);
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->stagesData))->EndInit();
            namesPage->ResumeLayout(false);
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->nameData))->EndInit();
            toolStrip1->ResumeLayout(false);
            toolStrip1->PerformLayout();
            this->tabbedControl->ResumeLayout(false);
            this->ResumeLayout(false);
            this->PerformLayout();

        }
    };
}
