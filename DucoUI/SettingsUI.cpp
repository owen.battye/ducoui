#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"

#include "SettingsUI.h"

#include "DatabaseSettings.h"
#include "WebsiteLinksUI.h"
#include "DucoUtils.h"
#include "SystemDefaultIcon.h"
#include <DucoConfiguration.h>
#include "WindowsSettings.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;

SettingsUI::SettingsUI(DatabaseManager^ theDatabase)
:   database(theDatabase), populatingData(true), dataChangedCallObservers(false)
{
    ringerCache = RingerDisplayCache::CreateDisplayCacheWithRingers(database, true, true);
    InitializeComponent();
    ringerCache->PopulateControl(defaultRingerSelector, true);
}

SettingsUI::~SettingsUI()
{
    if (dataChangedCallObservers)
    {
        database->CallObservers(EUpdated, TObjectType::ESettings, KNoId, KNoId);
    }
    if (components)
    {
        delete components;
    }
}

System::Void
SettingsUI::SettingsUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    this->Closing += gcnew System::ComponentModel::CancelEventHandler(this, &SettingsUI::ClosingEvent);
    PopulateSettings();
    populatingData = false;
    windowsSettingsChanged = false;
    databaseSettingsChanged = false;
}

System::Void
SettingsUI::ClosingEvent( Object^ /*sender*/, System::ComponentModel::CancelEventArgs^ e )
{
    if ( windowsSettingsChanged || databaseSettingsChanged)
    {
        if ( MessageBox::Show(this, "Do you want to save changes?", "Unsaved settings changes", MessageBoxButtons::YesNo, MessageBoxIcon::Warning ) == ::DialogResult::Yes )
        {
            SaveSettings();
        }
    }
}

void
SettingsUI::SetDataChanged(bool windows, bool database)
{
    if (!populatingData)
    {
        if (windows)
            windowsSettingsChanged = true;
        if (database)
            databaseSettingsChanged = true;
        if (windows || database)
            saveBtn->Enabled = true;
    }
}

System::Void
SettingsUI::defaultRingerSelector_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    SetDataChanged(false, true);
}

System::Void
SettingsUI::ChangedWindowsSetting(System::Object^  sender, System::EventArgs^  e)
{
    SetDataChanged(true, false);
}

System::Void
SettingsUI::ChangedDatabaseSetting(System::Object^  sender, System::EventArgs^  e)
{
    SetDataChanged(false, true);
}

System::Void
SettingsUI::showGrid_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    SetDataChanged(false, true);
    if (!populatingData && showGrid->Checked && !normalLine->Checked)
    {
        normalLine->Checked = true;
    }
}

System::Void 
SettingsUI::methodGrid_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    SetDataChanged(false, true);
    if (!populatingData && methodGrid->Checked && longLine->Checked)
    {
        normalLine->Checked = true;
    }
}

System::Void 
SettingsUI::longLine_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    SetDataChanged(false, true);
    if (!populatingData && longLine->Checked)
    {
        methodGrid->Checked = false;
        showGrid->Checked = false;
    }
}

System::Void 
SettingsUI::allBells_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    SetDataChanged(false, true);
    if (!populatingData && allBells->Checked)
    {
        showGrid->Checked = false;
    }
}

System::Void
SettingsUI::googleMapsKey_TextChanged(System::Object^ sender, System::EventArgs^ e)
{
    SetDataChanged(true, false);
}

System::Void
SettingsUI::mapBoxKey_TextChanged(System::Object^ sender, System::EventArgs^ e)
{
    SetDataChanged(true, false);
}

System::Void
SettingsUI::googleMapsBtn_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
{
    SetDataChanged(false, true);
}

System::Void
SettingsUI::mapBoxBtn_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
{
    SetDataChanged(false, true);
}

System::Void
SettingsUI::cancelBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void 
SettingsUI::saveBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    SaveSettings();
    Close();
}

System::Void 
SettingsUI::resetBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::DatabaseSettings newSettings;
    newSettings.ClearSettings(!database->Settings().PealDatabase());
    database->Config().ClearUISettings();
    PopulateSettingsFrom(newSettings, database->WindowsSettings());
    SetDataChanged(true, true);
}

System::Void
SettingsUI::websitesBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    WebsiteLinksUI^ uiForm = gcnew WebsiteLinksUI(database);
    uiForm->MdiParent = MdiParent;
    uiForm->Show();
}

System::Void 
SettingsUI::PopulateSettings()
{
    PopulateSettingsFrom(database->Settings(), database->WindowsSettings());
}

System::Void
SettingsUI::PopulateSettingsFrom(const Duco::DatabaseSettings& settings, WindowsSettings^ windowsSettings)
{
    populatingData = true;
    mapBoxKey->Text = DucoUtils::ConvertString(database->Config().MapBoxApi());

    doveURLEditor->Text = DucoUtils::ConvertString(settings.DoveURL());
    surnamesFirst->Checked = settings.LastNameFirst();
    missingAssociationsWarning->Checked = settings.ValidationCheckDisabled(EPealWarning_AssociationMissing, TObjectType::EPeal) || settings.ValidationCheckDisabled(EPealWarning_AssociationBlank, TObjectType::EPeal);
    seriesMissingWarning->Checked = settings.ValidationCheckDisabled(EPealWarning_SeriesMissing, TObjectType::EPeal);
    missingCallingNotation->Checked = settings.ValidationCheckDisabled(EMethodWarning_MissingBobPlaceNotation, TObjectType::EMethod) || settings.ValidationCheckDisabled(EMethodWarning_MissingSinglePlaceNotation, TObjectType::EMethod);
    ignoreMissingTime->Checked = settings.ValidationCheckDisabled(EPeal_TimeInvalid, TObjectType::EPeal);
    if (settings.AlphabeticReordering())
        alphabeticRenumber->Checked = true;
    showTreble->Checked = settings.ShowTreble();
    methodGrid->Checked = settings.ShowFullGrid();
    showCallings->Checked = settings.ShowBobsAndSingles();
    if (settings.LineType() == Duco::DatabaseSettings::ELongLine)
        longLine->Checked = true;
    else if (settings.LineType() == Duco::DatabaseSettings::EAnnotated)
        allBells->Checked = true;
    else
        normalLine->Checked = true;

    showGrid->Checked = settings.ShowGridLines();
    if (settings.BlueLineWebsite() == DatabaseSettings::EVisualMethodArchive)
    {
        visMethSelector->Checked = true;
    }
    printPreview->Checked = settings.UsePrintPreview();
    disablePerformance->Checked = database->Config().DisableFeaturesForPerformance();
    allowErrors->Checked = settings.AllowErrors();
    if (settings.PealPrintFormat() == DatabaseSettings::EPealboardStyle)
    {
        pealBoardFormat->Checked = true;
    }
    else
    {
        rwFormat->Checked = true;
    }
    printSurnameFirst->Checked = settings.PrintSurnameFirst();
    enableSetRWPageOnUpload->Checked = settings.BellBoardAutoSetRingingWorldPage();
    createNewMethod->Checked = !database->Config().ImportNewMethod();
    importNewMethod->Checked = database->Config().ImportNewMethod();
    keepTowersWithDate->Checked = settings.KeepTowersWithRungDate();
    if (settings.PealDatabase())
        fullPeals->Checked = true;
    else
        quarterPeals->Checked = true;

    exportToXmlIncludesIds->Checked = database->Config().ExportDucoIdsWithXml();
    populatingData = false;
}

System::Void 
SettingsUI::SaveSettings()
{
    if (!databaseSettingsChanged && !windowsSettingsChanged)
        return;

    if (databaseSettingsChanged)
    {
        DatabaseSettings& settings = database->Settings();
        std::wstring tempUrl;
        
        DucoUtils::ConvertString(DucoUtils::ParseUrl(doveURLEditor->Text), tempUrl);
        settings.SetDoveURL(tempUrl);
        settings.SetLastNameFirst(surnamesFirst->Checked);
        settings.SetValidationCheck(EPealWarning_AssociationMissing, TObjectType::EPeal, missingAssociationsWarning->Checked);
        settings.SetValidationCheck(EPealWarning_AssociationBlank, TObjectType::EPeal, missingAssociationsWarning->Checked);
        settings.SetValidationCheck(EPealWarning_SeriesMissing, TObjectType::EPeal, seriesMissingWarning->Checked);
        settings.SetValidationCheck(EMethodWarning_MissingBobPlaceNotation, TObjectType::EMethod, missingCallingNotation->Checked);
        settings.SetValidationCheck(EMethodWarning_MissingSinglePlaceNotation, TObjectType::EMethod, missingCallingNotation->Checked);
        settings.SetValidationCheck(EPeal_TimeInvalid, TObjectType::EPeal, ignoreMissingTime->Checked);
        settings.SetShowTreble(showTreble->Checked);
        settings.SetShowGridLines(showGrid->Checked);
        settings.SetShowFullGrid(methodGrid->Checked);
        settings.SetShowBobsAndSingles(showCallings->Checked);
        if (longLine->Checked)
            settings.SetLineType(Duco::DatabaseSettings::ELongLine);
        else if (allBells->Checked)
            settings.SetLineType(Duco::DatabaseSettings::EAnnotated);
        else
            settings.SetLineType(Duco::DatabaseSettings::ENormalLine);
        if (visMethSelector->Checked)
            settings.SetBlueLineWebsite(DatabaseSettings::EVisualMethodArchive);
        else
            settings.SetBlueLineWebsite(DatabaseSettings::EPaulGraupnerBlueLine);

        settings.SetAlphabeticReordering(alphabeticRenumber->Checked);
        if (defaultRingerSelector->SelectedIndex != -1)
        {
            RingerItem^ ringer = (RingerItem^)defaultRingerSelector->SelectedItem;
            settings.SetDefaultRinger(ringer->Id());
        }
        settings.SetPrintPreview(printPreview->Checked);
        settings.SetAllowErrors(allowErrors->Checked);
        if (rwFormat->Checked)
        {
            settings.SetPealPrintFormat(DatabaseSettings::ERingingWorldStyle);
        }
        else
        {
            settings.SetPealPrintFormat(DatabaseSettings::EPealboardStyle);
        }
        settings.SetPrintSurnameFirst(printSurnameFirst->Checked);
        settings.SetBellBoardAutoSetRingingWorldPage(enableSetRWPageOnUpload->Checked);
        database->Config().SetImportNewMethod(importNewMethod->Checked);
        settings.SetKeepTowersWithRungDate(keepTowersWithDate->Checked);
        settings.SetPealDatabase(fullPeals->Checked);
        databaseSettingsChanged = false;
    }
    if (windowsSettingsChanged)
    {
        database->Config().SetImportNewMethod(importNewMethod->Checked);
        database->Config().SetDisableFeaturesForPerformance(disablePerformance->Checked);
        database->Config().SetExportDucoIdsWithXml(exportToXmlIncludesIds->Checked);
        std::string mapBoxKeyNative = "";
        DucoUtils::ConvertString(DucoUtils::ParseUrl(mapBoxKey->Text), mapBoxKeyNative);
        database->Config().SetMapBoxApi(mapBoxKeyNative);

        windowsSettingsChanged = false;
    }

    dataChangedCallObservers = true;
}
