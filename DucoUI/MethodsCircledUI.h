#pragma once
namespace Duco
{
    class StatisticFilters;
}

namespace DucoUI
{
    ref class DatabaseManager;
    ref class GenericTablePrintUtil;

    public ref class MethodsCircledUI : public System::Windows::Forms::Form
    {
    public:
        MethodsCircledUI(DucoUI::DatabaseManager^ theDatabase);
        MethodsCircledUI(DucoUI::DatabaseManager^ theDatabase, const Duco::StatisticFilters& newFilters);

    protected:
        ~MethodsCircledUI();
        !MethodsCircledUI();

        System::Void MethodsCircledUI_Load(System::Object^  sender, System::EventArgs^  e);

        System::Void printBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void filtersBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void printData(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args);

        System::Void SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void conducted_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void dataGridView_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);
        System::Void dataGridView_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);

        System::Void StartGenerator();
        System::Void CheckEnoughColumnsExist();
        System::Void ResetAllSortGlyphs();

        System::Void backgroundWorker_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void backgroundWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void backgroundWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);

    private:
        System::Windows::Forms::DataGridView^   dataGridView;
        DucoUI::GenericTablePrintUtil^          printUtil;
        System::ComponentModel::BackgroundWorker^       backgroundWorker;
        System::Windows::Forms::ToolStripProgressBar^   progressBar;
        Duco::StatisticFilters*                         filters;

    private:
        unsigned int                                        totalNumberOfMethodsCircled;
        bool                                                restartGenerator;
        bool                                                loading;
        bool                                                startingGenerator;

        System::ComponentModel::Container^                  components;
        DucoUI::DatabaseManager^                      database;

        System::Windows::Forms::DataGridViewTextBoxColumn^  idColumn;
        System::Windows::Forms::DataGridViewTextBoxColumn^  methodColumn;
        System::Windows::Forms::DataGridViewTextBoxColumn^  circlesColumn;
        System::Windows::Forms::DataGridViewTextBoxColumn^  pealsColumn;

        System::Windows::Forms::ToolStripStatusLabel^  resultsCount;
        System::Windows::Forms::DataGridViewTextBoxColumn^  dateColumn;


#pragma region Windows Form Designer generated code
        void InitializeComponent()
        {
            System::Windows::Forms::ToolStrip^  toolStrip1;
            System::Windows::Forms::ToolStripButton^  filtersBtn;
            System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(MethodsCircledUI::typeid));
            System::Windows::Forms::ToolStripButton^  printBtn;
            System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle1 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle2 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle3 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle4 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle5 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::StatusStrip^  statusStrip1;
            this->dataGridView = (gcnew System::Windows::Forms::DataGridView());
            this->idColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->methodColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->circlesColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->pealsColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->dateColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->progressBar = (gcnew System::Windows::Forms::ToolStripProgressBar());
            this->resultsCount = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->backgroundWorker = (gcnew System::ComponentModel::BackgroundWorker());
            toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
            filtersBtn = (gcnew System::Windows::Forms::ToolStripButton());
            printBtn = (gcnew System::Windows::Forms::ToolStripButton());
            statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            toolStrip1->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->BeginInit();
            statusStrip1->SuspendLayout();
            this->SuspendLayout();
            // 
            // toolStrip1
            // 
            toolStrip1->GripStyle = System::Windows::Forms::ToolStripGripStyle::Hidden;
            toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) { filtersBtn, printBtn });
            toolStrip1->Location = System::Drawing::Point(0, 0);
            toolStrip1->Name = L"toolStrip1";
            toolStrip1->Size = System::Drawing::Size(723, 25);
            toolStrip1->TabIndex = 4;
            toolStrip1->Text = L"toolStrip1";
            // 
            // filtersBtn
            // 
            filtersBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
            filtersBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"filtersBtn.Image")));
            filtersBtn->ImageTransparentColor = System::Drawing::Color::Magenta;
            filtersBtn->Name = L"filtersBtn";
            filtersBtn->Size = System::Drawing::Size(42, 22);
            filtersBtn->Text = L"&Filters";
            filtersBtn->Click += gcnew System::EventHandler(this, &MethodsCircledUI::filtersBtn_Click);
            // 
            // printBtn
            // 
            printBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
            printBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"printBtn.Image")));
            printBtn->ImageTransparentColor = System::Drawing::Color::Magenta;
            printBtn->Name = L"printBtn";
            printBtn->Size = System::Drawing::Size(36, 22);
            printBtn->Text = L"&Print";
            printBtn->Click += gcnew System::EventHandler(this, &MethodsCircledUI::printBtn_Click);
            // 
            // dataGridView
            // 
            this->dataGridView->AllowUserToAddRows = false;
            this->dataGridView->AllowUserToDeleteRows = false;
            this->dataGridView->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::AllCells;
            this->dataGridView->AutoSizeRowsMode = System::Windows::Forms::DataGridViewAutoSizeRowsMode::AllCells;
            this->dataGridView->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->dataGridView->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(5) {
                this->idColumn,
                    this->methodColumn, this->circlesColumn, this->pealsColumn, this->dateColumn
            });
            this->dataGridView->Dock = System::Windows::Forms::DockStyle::Fill;
            this->dataGridView->Location = System::Drawing::Point(0, 25);
            this->dataGridView->Name = L"dataGridView";
            this->dataGridView->ReadOnly = true;
            this->dataGridView->Size = System::Drawing::Size(723, 395);
            this->dataGridView->TabIndex = 0;
            this->dataGridView->CellContentDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &MethodsCircledUI::dataGridView_CellContentDoubleClick);
            this->dataGridView->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &MethodsCircledUI::dataGridView_ColumnHeaderMouseClick);
            // 
            // idColumn
            // 
            dataGridViewCellStyle1->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->idColumn->DefaultCellStyle = dataGridViewCellStyle1;
            this->idColumn->Frozen = true;
            this->idColumn->HeaderText = L"Id";
            this->idColumn->Name = L"idColumn";
            this->idColumn->ReadOnly = true;
            this->idColumn->Visible = false;
            this->idColumn->Width = 41;
            // 
            // methodColumn
            // 
            this->methodColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            dataGridViewCellStyle2->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->methodColumn->DefaultCellStyle = dataGridViewCellStyle2;
            this->methodColumn->Frozen = true;
            this->methodColumn->HeaderText = L"Method";
            this->methodColumn->Name = L"methodColumn";
            this->methodColumn->ReadOnly = true;
            this->methodColumn->Width = 68;
            // 
            // circlesColumn
            // 
            this->circlesColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            dataGridViewCellStyle3->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->circlesColumn->DefaultCellStyle = dataGridViewCellStyle3;
            this->circlesColumn->Frozen = true;
            this->circlesColumn->HeaderText = L"Circled";
            this->circlesColumn->Name = L"circlesColumn";
            this->circlesColumn->ReadOnly = true;
            this->circlesColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->circlesColumn->Width = 64;
            // 
            // pealsColumn
            // 
            this->pealsColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            dataGridViewCellStyle4->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->pealsColumn->DefaultCellStyle = dataGridViewCellStyle4;
            this->pealsColumn->Frozen = true;
            this->pealsColumn->HeaderText = L"Peals";
            this->pealsColumn->Name = L"pealsColumn";
            this->pealsColumn->ReadOnly = true;
            this->pealsColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->pealsColumn->Width = 58;
            // 
            // dateColumn
            // 
            this->dateColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            dataGridViewCellStyle5->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->dateColumn->DefaultCellStyle = dataGridViewCellStyle5;
            this->dateColumn->Frozen = true;
            this->dateColumn->HeaderText = L"Date";
            this->dateColumn->Name = L"dateColumn";
            this->dateColumn->ReadOnly = true;
            this->dateColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->dateColumn->Width = 55;
            // 
            // statusStrip1
            // 
            statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) { this->progressBar, this->resultsCount });
            statusStrip1->Location = System::Drawing::Point(0, 420);
            statusStrip1->Name = L"statusStrip1";
            statusStrip1->Size = System::Drawing::Size(723, 22);
            statusStrip1->TabIndex = 1;
            statusStrip1->Text = L"statusStrip1";
            // 
            // progressBar
            // 
            this->progressBar->Name = L"progressBar";
            this->progressBar->Size = System::Drawing::Size(100, 16);
            // 
            // resultsCount
            // 
            this->resultsCount->Name = L"resultsCount";
            this->resultsCount->Size = System::Drawing::Size(606, 17);
            this->resultsCount->Spring = true;
            this->resultsCount->Text = L"Number of methods circled \?";
            this->resultsCount->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // backgroundWorker
            // 
            this->backgroundWorker->WorkerReportsProgress = true;
            this->backgroundWorker->WorkerSupportsCancellation = true;
            this->backgroundWorker->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &MethodsCircledUI::backgroundWorker_DoWork);
            this->backgroundWorker->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &MethodsCircledUI::backgroundWorker_ProgressChanged);
            this->backgroundWorker->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &MethodsCircledUI::backgroundWorker_RunWorkerCompleted);
            // 
            // MethodsCircledUI
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(723, 442);
            this->Controls->Add(this->dataGridView);
            this->Controls->Add(toolStrip1);
            this->Controls->Add(statusStrip1);
            this->MinimumSize = System::Drawing::Size(555, 480);
            this->Name = L"MethodsCircledUI";
            this->Text = L"Method circling";
            this->Load += gcnew System::EventHandler(this, &MethodsCircledUI::MethodsCircledUI_Load);
            toolStrip1->ResumeLayout(false);
            toolStrip1->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->EndInit();
            statusStrip1->ResumeLayout(false);
            statusStrip1->PerformLayout();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion
    };
}
