#pragma once
namespace DucoUI
{
    public ref class TowerSearchUI : public System::Windows::Forms::Form,
                                     public DucoUI::ProgressCallbackUI
    {
    public:
        TowerSearchUI(DucoUI::DatabaseManager^ newDatabase, bool setSearchInvalid);
        void ShowTowers(System::Windows::Forms::Form^ parent, System::Collections::Generic::List<System::UInt64>^ towerIds);

        // from ProgressCallbackUI
        virtual void Initialised();
        virtual void Step(int progressPercent);
        virtual void Complete();

    protected:
        !TowerSearchUI();
        ~TowerSearchUI();
        System::Void TowerSearchUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void ClosingEvent( Object^ /*sender*/, System::ComponentModel::CancelEventArgs^ e );

    private:
        bool CreateSearchParameters(Duco::DatabaseSearch& search);

        System::Void searchBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void clearBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void updateBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void printBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void printTowers(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args);
        System::Void copyTowersCmd_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void statisticBtn_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void towersData_CellDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);
        System::Void blankDoveRef_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void blankPealBaseOrFelsteadReference_CheckedChanged(System::Object^ sender, System::EventArgs^ e);

        void BellsEditor_KeyDown( Object^ sender, System::Windows::Forms::KeyEventArgs^ e );
        void BellsEditor_KeyPressed(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e);

        System::Void backgroundWorker_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void backgroundWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void backgroundWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);
        System::Void towersData_Scroll(System::Object^  sender, System::Windows::Forms::ScrollEventArgs^  e);
        System::Void TowerSearchUI_Scroll(System::Object^  sender, System::Windows::Forms::ScrollEventArgs^  e);
        System::Void towersData_CellValueNeeded(System::Object^  sender, System::Windows::Forms::DataGridViewCellValueEventArgs^  e);
        System::Void nameOrCity_CheckedChanged(System::Object^ sender, System::EventArgs^ e);

    private:
        DucoUI::ProgressCallbackWrapper*            progressWrapper;
        System::ComponentModel::BackgroundWorker^   backgroundWorker;

        System::Boolean                             startSearchInvalid;
        System::ComponentModel::IContainer^         components;
        System::Collections::Generic::List<System::Int16>^ allTowerIds;
        System::Windows::Forms::DataGridViewCellStyle^ removedTowerStyle;

        System::Windows::Forms::DataGridView^   towersData;
        System::Windows::Forms::ComboBox^       nameEditor;
        System::Windows::Forms::CheckBox^       nameIncludesAka;
        System::Windows::Forms::ComboBox^       cityEditor;
        System::Windows::Forms::CheckBox^       cityIncludesAka;
        System::Windows::Forms::ComboBox^       countyEditor;
        System::Windows::Forms::CheckBox^       countyIncludesAka;

        System::Windows::Forms::TextBox^        bellsEditor;
        System::Windows::Forms::RadioButton^    bellsLessThan;
        System::Windows::Forms::RadioButton^    bellsEqualTo;
        System::Windows::Forms::RadioButton^    bellsMoreThan;

        System::Windows::Forms::TextBox^        ringNameEditor;
        System::Windows::Forms::CheckBox^       multipleRings;
        System::Windows::Forms::CheckBox^       specialBells;

        System::Windows::Forms::TextBox^        tenorWeight;
        System::Windows::Forms::TextBox^        tenorKey;

        System::Windows::Forms::TextBox^        notes;
        System::Windows::Forms::TextBox^        doveRef;
        System::Windows::Forms::CheckBox^       blankDoveRef;
        System::Windows::Forms::CheckBox^       removed;
        System::Windows::Forms::CheckBox^       invalid;
        System::Windows::Forms::ComboBox^       towerSelector;

        System::Windows::Forms::TextBox^        results;
        System::Windows::Forms::ProgressBar^    progressBar;

        DucoUI::DatabaseManager^                database;
        System::Windows::Forms::Button^         searchBtn;
        System::Windows::Forms::Button^         clearBtn;
        System::Collections::Generic::List<System::UInt64>^ foundTowerIds;
        DucoUI::GenericTablePrintUtil^          printUtil;
        System::Windows::Forms::ComboBox^       linkedTowerSelector;
        System::Windows::Forms::CheckBox^       linkedTowersOnly;
        System::Windows::Forms::CheckBox^       immediateLinkedTowersOnly;

        System::Windows::Forms::Button^        updateBtn;
    private: System::Windows::Forms::Button^    statisticBtn;
    private: System::Windows::Forms::RadioButton^ withAndWithoutPictures;
    private: System::Windows::Forms::RadioButton^ doesNotHavePicture;
    private: System::Windows::Forms::RadioButton^ hasPicture;
private: System::Windows::Forms::CheckBox^ blankTowerbaseReference;
private: System::Windows::Forms::TextBox^ towerbaseReference;











private: System::Windows::Forms::CheckBox^ nameOrCity;
private: System::Windows::Forms::RadioButton^ allVenueTypes;
private: System::Windows::Forms::RadioButton^ handbells;
private: System::Windows::Forms::RadioButton^ towerBells;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ NameColumn;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ CityColumn;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ CountColumn;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ towerBellsColumn;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ BellsColumn;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ TenorColumn;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ KeyColumn;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ DoveRefColumn;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ FirstPealColumn;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ lastPealDateColumn;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ distanceColumn;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ errorColumn;
private: System::Windows::Forms::CheckBox^ antiClockwise;
private: System::Windows::Forms::CheckBox^ invalidPosition;

       bool                                   nonNumberEntered;

    void InitializeComponent()
        {
        this->components = (gcnew System::ComponentModel::Container());
        System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle11 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle12 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle13 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle14 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle15 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle16 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle17 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle18 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle19 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle20 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::ContextMenuStrip^ towerDataMenu;
        System::Windows::Forms::ToolStripMenuItem^ copyTowersCmd;
        System::Windows::Forms::TabControl^ tabbedControl;
        System::Windows::Forms::TabPage^ namePage;
        System::Windows::Forms::GroupBox^ nameGroup;
        System::Windows::Forms::Label^ nameLbl;
        System::Windows::Forms::Label^ townLbl;
        System::Windows::Forms::Label^ countyLbl;
        System::Windows::Forms::TabPage^ bellsPage;
        System::Windows::Forms::GroupBox^ towerTypeGroup;
        System::Windows::Forms::GroupBox^ bellsGroup;
        System::Windows::Forms::GroupBox^ ringsGroup;
        System::Windows::Forms::Label^ ringNameLbl;
        System::Windows::Forms::GroupBox^ tenorGroup;
        System::Windows::Forms::Label^ tenorKeyLbl;
        System::Windows::Forms::Label^ tenorWeightLbl;
        System::Windows::Forms::TabPage^ linkedTowersPage;
        System::Windows::Forms::GroupBox^ linkedTowersGrp;
        System::Windows::Forms::TabPage^ webLinksPage;
        System::Windows::Forms::GroupBox^ towerbaseIdGroup;
        System::Windows::Forms::GroupBox^ doveGroup;
        System::Windows::Forms::TabPage^ otherPage;
        System::Windows::Forms::GroupBox^ picturesGroup;
        System::Windows::Forms::GroupBox^ notesGroup;
        System::Windows::Forms::Label^ notesLbl;
        System::Windows::Forms::GroupBox^ positionGroup;
        System::Windows::Forms::Label^ towerSelectorLbl;
        System::Windows::Forms::Panel^ resultsPanel;
        System::Windows::Forms::Button^ printBtn;
        System::Windows::Forms::Label^ resultsLbl;
        System::Windows::Forms::Button^ closeBtn;
        System::Windows::Forms::ToolTip^ toolTip1;
        this->towersData = (gcnew System::Windows::Forms::DataGridView());
        this->NameColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->CityColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->CountColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->towerBellsColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->BellsColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->TenorColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->KeyColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->DoveRefColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->FirstPealColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->lastPealDateColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->distanceColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->errorColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->nameOrCity = (gcnew System::Windows::Forms::CheckBox());
        this->nameEditor = (gcnew System::Windows::Forms::ComboBox());
        this->cityEditor = (gcnew System::Windows::Forms::ComboBox());
        this->countyEditor = (gcnew System::Windows::Forms::ComboBox());
        this->nameIncludesAka = (gcnew System::Windows::Forms::CheckBox());
        this->cityIncludesAka = (gcnew System::Windows::Forms::CheckBox());
        this->countyIncludesAka = (gcnew System::Windows::Forms::CheckBox());
        this->antiClockwise = (gcnew System::Windows::Forms::CheckBox());
        this->allVenueTypes = (gcnew System::Windows::Forms::RadioButton());
        this->handbells = (gcnew System::Windows::Forms::RadioButton());
        this->towerBells = (gcnew System::Windows::Forms::RadioButton());
        this->bellsMoreThan = (gcnew System::Windows::Forms::RadioButton());
        this->bellsEqualTo = (gcnew System::Windows::Forms::RadioButton());
        this->bellsLessThan = (gcnew System::Windows::Forms::RadioButton());
        this->bellsEditor = (gcnew System::Windows::Forms::TextBox());
        this->ringNameEditor = (gcnew System::Windows::Forms::TextBox());
        this->multipleRings = (gcnew System::Windows::Forms::CheckBox());
        this->specialBells = (gcnew System::Windows::Forms::CheckBox());
        this->tenorKey = (gcnew System::Windows::Forms::TextBox());
        this->tenorWeight = (gcnew System::Windows::Forms::TextBox());
        this->linkedTowersOnly = (gcnew System::Windows::Forms::CheckBox());
        this->immediateLinkedTowersOnly = (gcnew System::Windows::Forms::CheckBox());
        this->linkedTowerSelector = (gcnew System::Windows::Forms::ComboBox());
        this->blankTowerbaseReference = (gcnew System::Windows::Forms::CheckBox());
        this->towerbaseReference = (gcnew System::Windows::Forms::TextBox());
        this->blankDoveRef = (gcnew System::Windows::Forms::CheckBox());
        this->doveRef = (gcnew System::Windows::Forms::TextBox());
        this->withAndWithoutPictures = (gcnew System::Windows::Forms::RadioButton());
        this->doesNotHavePicture = (gcnew System::Windows::Forms::RadioButton());
        this->hasPicture = (gcnew System::Windows::Forms::RadioButton());
        this->notes = (gcnew System::Windows::Forms::TextBox());
        this->removed = (gcnew System::Windows::Forms::CheckBox());
        this->invalid = (gcnew System::Windows::Forms::CheckBox());
        this->towerSelector = (gcnew System::Windows::Forms::ComboBox());
        this->statisticBtn = (gcnew System::Windows::Forms::Button());
        this->updateBtn = (gcnew System::Windows::Forms::Button());
        this->clearBtn = (gcnew System::Windows::Forms::Button());
        this->results = (gcnew System::Windows::Forms::TextBox());
        this->progressBar = (gcnew System::Windows::Forms::ProgressBar());
        this->searchBtn = (gcnew System::Windows::Forms::Button());
        this->backgroundWorker = (gcnew System::ComponentModel::BackgroundWorker());
        this->invalidPosition = (gcnew System::Windows::Forms::CheckBox());
        tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
        towerDataMenu = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
        copyTowersCmd = (gcnew System::Windows::Forms::ToolStripMenuItem());
        tabbedControl = (gcnew System::Windows::Forms::TabControl());
        namePage = (gcnew System::Windows::Forms::TabPage());
        nameGroup = (gcnew System::Windows::Forms::GroupBox());
        nameLbl = (gcnew System::Windows::Forms::Label());
        townLbl = (gcnew System::Windows::Forms::Label());
        countyLbl = (gcnew System::Windows::Forms::Label());
        bellsPage = (gcnew System::Windows::Forms::TabPage());
        towerTypeGroup = (gcnew System::Windows::Forms::GroupBox());
        bellsGroup = (gcnew System::Windows::Forms::GroupBox());
        ringsGroup = (gcnew System::Windows::Forms::GroupBox());
        ringNameLbl = (gcnew System::Windows::Forms::Label());
        tenorGroup = (gcnew System::Windows::Forms::GroupBox());
        tenorKeyLbl = (gcnew System::Windows::Forms::Label());
        tenorWeightLbl = (gcnew System::Windows::Forms::Label());
        linkedTowersPage = (gcnew System::Windows::Forms::TabPage());
        linkedTowersGrp = (gcnew System::Windows::Forms::GroupBox());
        webLinksPage = (gcnew System::Windows::Forms::TabPage());
        towerbaseIdGroup = (gcnew System::Windows::Forms::GroupBox());
        doveGroup = (gcnew System::Windows::Forms::GroupBox());
        otherPage = (gcnew System::Windows::Forms::TabPage());
        picturesGroup = (gcnew System::Windows::Forms::GroupBox());
        notesGroup = (gcnew System::Windows::Forms::GroupBox());
        notesLbl = (gcnew System::Windows::Forms::Label());
        positionGroup = (gcnew System::Windows::Forms::GroupBox());
        towerSelectorLbl = (gcnew System::Windows::Forms::Label());
        resultsPanel = (gcnew System::Windows::Forms::Panel());
        printBtn = (gcnew System::Windows::Forms::Button());
        resultsLbl = (gcnew System::Windows::Forms::Label());
        closeBtn = (gcnew System::Windows::Forms::Button());
        toolTip1 = (gcnew System::Windows::Forms::ToolTip(this->components));
        tableLayoutPanel1->SuspendLayout();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->towersData))->BeginInit();
        towerDataMenu->SuspendLayout();
        tabbedControl->SuspendLayout();
        namePage->SuspendLayout();
        nameGroup->SuspendLayout();
        bellsPage->SuspendLayout();
        towerTypeGroup->SuspendLayout();
        bellsGroup->SuspendLayout();
        ringsGroup->SuspendLayout();
        tenorGroup->SuspendLayout();
        linkedTowersPage->SuspendLayout();
        linkedTowersGrp->SuspendLayout();
        webLinksPage->SuspendLayout();
        towerbaseIdGroup->SuspendLayout();
        doveGroup->SuspendLayout();
        otherPage->SuspendLayout();
        picturesGroup->SuspendLayout();
        notesGroup->SuspendLayout();
        positionGroup->SuspendLayout();
        resultsPanel->SuspendLayout();
        this->SuspendLayout();
        // 
        // tableLayoutPanel1
        // 
        tableLayoutPanel1->ColumnCount = 1;
        tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
        tableLayoutPanel1->Controls->Add(this->towersData, 0, 1);
        tableLayoutPanel1->Controls->Add(tabbedControl, 0, 0);
        tableLayoutPanel1->Controls->Add(resultsPanel, 0, 2);
        tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
        tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
        tableLayoutPanel1->Name = L"tableLayoutPanel1";
        tableLayoutPanel1->RowCount = 3;
        tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
        tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
        tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 34)));
        tableLayoutPanel1->Size = System::Drawing::Size(837, 369);
        tableLayoutPanel1->TabIndex = 0;
        // 
        // towersData
        // 
        this->towersData->AllowUserToAddRows = false;
        this->towersData->AllowUserToDeleteRows = false;
        this->towersData->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::DisplayedCells;
        this->towersData->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
        this->towersData->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(12) {
            this->NameColumn,
                this->CityColumn, this->CountColumn, this->towerBellsColumn, this->BellsColumn, this->TenorColumn, this->KeyColumn, this->DoveRefColumn,
                this->FirstPealColumn, this->lastPealDateColumn, this->distanceColumn, this->errorColumn
        });
        this->towersData->ContextMenuStrip = towerDataMenu;
        this->towersData->Dock = System::Windows::Forms::DockStyle::Fill;
        this->towersData->Location = System::Drawing::Point(3, 152);
        this->towersData->Name = L"towersData";
        this->towersData->ReadOnly = true;
        this->towersData->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToDisplayedHeaders;
        this->towersData->Size = System::Drawing::Size(831, 180);
        this->towersData->TabIndex = 1;
        this->towersData->VirtualMode = true;
        this->towersData->CellDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &TowerSearchUI::towersData_CellDoubleClick);
        this->towersData->CellValueNeeded += gcnew System::Windows::Forms::DataGridViewCellValueEventHandler(this, &TowerSearchUI::towersData_CellValueNeeded);
        this->towersData->Scroll += gcnew System::Windows::Forms::ScrollEventHandler(this, &TowerSearchUI::towersData_Scroll);
        // 
        // NameColumn
        // 
        this->NameColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
        dataGridViewCellStyle11->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
        this->NameColumn->DefaultCellStyle = dataGridViewCellStyle11;
        this->NameColumn->HeaderText = L"Name";
        this->NameColumn->Name = L"NameColumn";
        this->NameColumn->ReadOnly = true;
        this->NameColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->NameColumn->Width = 41;
        // 
        // CityColumn
        // 
        this->CityColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
        dataGridViewCellStyle12->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
        this->CityColumn->DefaultCellStyle = dataGridViewCellStyle12;
        this->CityColumn->HeaderText = L"City";
        this->CityColumn->Name = L"CityColumn";
        this->CityColumn->ReadOnly = true;
        this->CityColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->CityColumn->Width = 30;
        // 
        // CountColumn
        // 
        this->CountColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
        dataGridViewCellStyle13->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
        this->CountColumn->DefaultCellStyle = dataGridViewCellStyle13;
        this->CountColumn->HeaderText = L"County";
        this->CountColumn->Name = L"CountColumn";
        this->CountColumn->ReadOnly = true;
        this->CountColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->CountColumn->Width = 46;
        // 
        // towerBellsColumn
        // 
        this->towerBellsColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
        this->towerBellsColumn->HeaderText = L"T";
        this->towerBellsColumn->Name = L"towerBellsColumn";
        this->towerBellsColumn->ReadOnly = true;
        this->towerBellsColumn->Width = 25;
        // 
        // BellsColumn
        // 
        this->BellsColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
        dataGridViewCellStyle14->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
        this->BellsColumn->DefaultCellStyle = dataGridViewCellStyle14;
        this->BellsColumn->HeaderText = L"Bells";
        this->BellsColumn->Name = L"BellsColumn";
        this->BellsColumn->ReadOnly = true;
        this->BellsColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->BellsColumn->Width = 35;
        // 
        // TenorColumn
        // 
        this->TenorColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
        dataGridViewCellStyle15->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
        this->TenorColumn->DefaultCellStyle = dataGridViewCellStyle15;
        this->TenorColumn->HeaderText = L"Tenor";
        this->TenorColumn->Name = L"TenorColumn";
        this->TenorColumn->ReadOnly = true;
        this->TenorColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->TenorColumn->Width = 50;
        // 
        // KeyColumn
        // 
        this->KeyColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
        dataGridViewCellStyle16->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
        this->KeyColumn->DefaultCellStyle = dataGridViewCellStyle16;
        this->KeyColumn->HeaderText = L"Key";
        this->KeyColumn->MinimumWidth = 30;
        this->KeyColumn->Name = L"KeyColumn";
        this->KeyColumn->ReadOnly = true;
        this->KeyColumn->Width = 30;
        // 
        // DoveRefColumn
        // 
        this->DoveRefColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
        dataGridViewCellStyle17->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
        this->DoveRefColumn->DefaultCellStyle = dataGridViewCellStyle17;
        this->DoveRefColumn->HeaderText = L"Dove";
        this->DoveRefColumn->MinimumWidth = 50;
        this->DoveRefColumn->Name = L"DoveRefColumn";
        this->DoveRefColumn->ReadOnly = true;
        this->DoveRefColumn->Width = 50;
        // 
        // FirstPealColumn
        // 
        this->FirstPealColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
        dataGridViewCellStyle18->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
        this->FirstPealColumn->DefaultCellStyle = dataGridViewCellStyle18;
        this->FirstPealColumn->HeaderText = L"First performance";
        this->FirstPealColumn->MinimumWidth = 25;
        this->FirstPealColumn->Name = L"FirstPealColumn";
        this->FirstPealColumn->ReadOnly = true;
        this->FirstPealColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->FirstPealColumn->Width = 94;
        // 
        // lastPealDateColumn
        // 
        this->lastPealDateColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
        this->lastPealDateColumn->HeaderText = L"Last Performance";
        this->lastPealDateColumn->Name = L"lastPealDateColumn";
        this->lastPealDateColumn->ReadOnly = true;
        this->lastPealDateColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->lastPealDateColumn->Width = 96;
        // 
        // distanceColumn
        // 
        this->distanceColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
        dataGridViewCellStyle19->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
        this->distanceColumn->DefaultCellStyle = dataGridViewCellStyle19;
        this->distanceColumn->HeaderText = L"Distance";
        this->distanceColumn->MinimumWidth = 25;
        this->distanceColumn->Name = L"distanceColumn";
        this->distanceColumn->ReadOnly = true;
        this->distanceColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->distanceColumn->Visible = false;
        // 
        // errorColumn
        // 
        this->errorColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
        dataGridViewCellStyle20->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
        this->errorColumn->DefaultCellStyle = dataGridViewCellStyle20;
        this->errorColumn->HeaderText = L"Error";
        this->errorColumn->MinimumWidth = 25;
        this->errorColumn->Name = L"errorColumn";
        this->errorColumn->ReadOnly = true;
        this->errorColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->errorColumn->Visible = false;
        // 
        // towerDataMenu
        // 
        towerDataMenu->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { copyTowersCmd });
        towerDataMenu->Name = L"copyTowerDataCmd";
        towerDataMenu->Size = System::Drawing::Size(103, 26);
        towerDataMenu->Text = L"Copy";
        // 
        // copyTowersCmd
        // 
        copyTowersCmd->Name = L"copyTowersCmd";
        copyTowersCmd->Size = System::Drawing::Size(102, 22);
        copyTowersCmd->Text = L"Copy";
        copyTowersCmd->Click += gcnew System::EventHandler(this, &TowerSearchUI::copyTowersCmd_Click);
        // 
        // tabbedControl
        // 
        tabbedControl->Controls->Add(namePage);
        tabbedControl->Controls->Add(bellsPage);
        tabbedControl->Controls->Add(linkedTowersPage);
        tabbedControl->Controls->Add(webLinksPage);
        tabbedControl->Controls->Add(otherPage);
        tabbedControl->Dock = System::Windows::Forms::DockStyle::Fill;
        tabbedControl->Location = System::Drawing::Point(1, 1);
        tabbedControl->Margin = System::Windows::Forms::Padding(1);
        tabbedControl->Name = L"tabbedControl";
        tabbedControl->SelectedIndex = 0;
        tabbedControl->Size = System::Drawing::Size(835, 147);
        tabbedControl->TabIndex = 0;
        // 
        // namePage
        // 
        namePage->Controls->Add(nameGroup);
        namePage->Location = System::Drawing::Point(4, 22);
        namePage->Name = L"namePage";
        namePage->Padding = System::Windows::Forms::Padding(3);
        namePage->Size = System::Drawing::Size(827, 121);
        namePage->TabIndex = 0;
        namePage->Text = L"Name and details";
        namePage->UseVisualStyleBackColor = true;
        // 
        // nameGroup
        // 
        nameGroup->Controls->Add(this->nameOrCity);
        nameGroup->Controls->Add(nameLbl);
        nameGroup->Controls->Add(townLbl);
        nameGroup->Controls->Add(countyLbl);
        nameGroup->Controls->Add(this->nameEditor);
        nameGroup->Controls->Add(this->cityEditor);
        nameGroup->Controls->Add(this->countyEditor);
        nameGroup->Controls->Add(this->nameIncludesAka);
        nameGroup->Controls->Add(this->cityIncludesAka);
        nameGroup->Controls->Add(this->countyIncludesAka);
        nameGroup->Location = System::Drawing::Point(3, 3);
        nameGroup->Name = L"nameGroup";
        nameGroup->Size = System::Drawing::Size(747, 112);
        nameGroup->TabIndex = 0;
        nameGroup->TabStop = false;
        nameGroup->Text = L"Tower name";
        // 
        // nameOrCity
        // 
        this->nameOrCity->AutoSize = true;
        this->nameOrCity->Checked = true;
        this->nameOrCity->CheckState = System::Windows::Forms::CheckState::Checked;
        this->nameOrCity->Location = System::Drawing::Point(9, 83);
        this->nameOrCity->Name = L"nameOrCity";
        this->nameOrCity->Size = System::Drawing::Size(133, 17);
        this->nameOrCity->TabIndex = 9;
        this->nameOrCity->Text = L"Check name and town";
        this->nameOrCity->UseVisualStyleBackColor = true;
        this->nameOrCity->CheckedChanged += gcnew System::EventHandler(this, &TowerSearchUI::nameOrCity_CheckedChanged);
        // 
        // nameLbl
        // 
        nameLbl->AutoSize = true;
        nameLbl->Location = System::Drawing::Point(6, 16);
        nameLbl->Name = L"nameLbl";
        nameLbl->Size = System::Drawing::Size(91, 13);
        nameLbl->TabIndex = 0;
        nameLbl->Text = L"Name/Dedication";
        // 
        // townLbl
        // 
        townLbl->AutoSize = true;
        townLbl->Location = System::Drawing::Point(272, 16);
        townLbl->Name = L"townLbl";
        townLbl->Size = System::Drawing::Size(34, 13);
        townLbl->TabIndex = 1;
        townLbl->Text = L"Town";
        // 
        // countyLbl
        // 
        countyLbl->AutoSize = true;
        countyLbl->Location = System::Drawing::Point(513, 16);
        countyLbl->Name = L"countyLbl";
        countyLbl->Size = System::Drawing::Size(40, 13);
        countyLbl->TabIndex = 2;
        countyLbl->Text = L"County";
        // 
        // nameEditor
        // 
        this->nameEditor->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
        this->nameEditor->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
        this->nameEditor->FormattingEnabled = true;
        this->nameEditor->Location = System::Drawing::Point(9, 32);
        this->nameEditor->Name = L"nameEditor";
        this->nameEditor->Size = System::Drawing::Size(260, 21);
        this->nameEditor->TabIndex = 3;
        // 
        // cityEditor
        // 
        this->cityEditor->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
        this->cityEditor->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
        this->cityEditor->Enabled = false;
        this->cityEditor->FormattingEnabled = true;
        this->cityEditor->Location = System::Drawing::Point(275, 32);
        this->cityEditor->Name = L"cityEditor";
        this->cityEditor->Size = System::Drawing::Size(235, 21);
        this->cityEditor->TabIndex = 4;
        // 
        // countyEditor
        // 
        this->countyEditor->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
        this->countyEditor->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
        this->countyEditor->FormattingEnabled = true;
        this->countyEditor->Location = System::Drawing::Point(516, 32);
        this->countyEditor->Name = L"countyEditor";
        this->countyEditor->Size = System::Drawing::Size(216, 21);
        this->countyEditor->TabIndex = 5;
        // 
        // nameIncludesAka
        // 
        this->nameIncludesAka->AutoSize = true;
        this->nameIncludesAka->Checked = true;
        this->nameIncludesAka->CheckState = System::Windows::Forms::CheckState::Checked;
        this->nameIncludesAka->Location = System::Drawing::Point(9, 59);
        this->nameIncludesAka->Name = L"nameIncludesAka";
        this->nameIncludesAka->Size = System::Drawing::Size(132, 17);
        this->nameIncludesAka->TabIndex = 6;
        this->nameIncludesAka->Text = L"Also check tower AKA";
        this->nameIncludesAka->UseVisualStyleBackColor = true;
        // 
        // cityIncludesAka
        // 
        this->cityIncludesAka->AutoSize = true;
        this->cityIncludesAka->Enabled = false;
        this->cityIncludesAka->Location = System::Drawing::Point(275, 59);
        this->cityIncludesAka->Name = L"cityIncludesAka";
        this->cityIncludesAka->Size = System::Drawing::Size(132, 17);
        this->cityIncludesAka->TabIndex = 7;
        this->cityIncludesAka->Text = L"Also check tower AKA";
        this->cityIncludesAka->UseVisualStyleBackColor = true;
        // 
        // countyIncludesAka
        // 
        this->countyIncludesAka->AutoSize = true;
        this->countyIncludesAka->Location = System::Drawing::Point(516, 59);
        this->countyIncludesAka->Name = L"countyIncludesAka";
        this->countyIncludesAka->Size = System::Drawing::Size(132, 17);
        this->countyIncludesAka->TabIndex = 8;
        this->countyIncludesAka->Text = L"Also check tower AKA";
        this->countyIncludesAka->UseVisualStyleBackColor = true;
        // 
        // bellsPage
        // 
        bellsPage->Controls->Add(this->antiClockwise);
        bellsPage->Controls->Add(towerTypeGroup);
        bellsPage->Controls->Add(bellsGroup);
        bellsPage->Controls->Add(ringsGroup);
        bellsPage->Controls->Add(tenorGroup);
        bellsPage->Location = System::Drawing::Point(4, 22);
        bellsPage->Name = L"bellsPage";
        bellsPage->Padding = System::Windows::Forms::Padding(3);
        bellsPage->Size = System::Drawing::Size(827, 121);
        bellsPage->TabIndex = 0;
        bellsPage->Text = L"Bells";
        bellsPage->UseVisualStyleBackColor = true;
        // 
        // antiClockwise
        // 
        this->antiClockwise->AutoSize = true;
        this->antiClockwise->Location = System::Drawing::Point(627, 24);
        this->antiClockwise->Name = L"antiClockwise";
        this->antiClockwise->Size = System::Drawing::Size(95, 17);
        this->antiClockwise->TabIndex = 28;
        this->antiClockwise->Text = L"Anti Clockwise";
        this->antiClockwise->UseVisualStyleBackColor = true;
        // 
        // towerTypeGroup
        // 
        towerTypeGroup->Controls->Add(this->allVenueTypes);
        towerTypeGroup->Controls->Add(this->handbells);
        towerTypeGroup->Controls->Add(this->towerBells);
        towerTypeGroup->Location = System::Drawing::Point(7, 6);
        towerTypeGroup->Name = L"towerTypeGroup";
        towerTypeGroup->Size = System::Drawing::Size(82, 97);
        towerTypeGroup->TabIndex = 4;
        towerTypeGroup->TabStop = false;
        towerTypeGroup->Text = L"Venue type";
        // 
        // allVenueTypes
        // 
        this->allVenueTypes->AutoSize = true;
        this->allVenueTypes->Location = System::Drawing::Point(7, 66);
        this->allVenueTypes->Name = L"allVenueTypes";
        this->allVenueTypes->Size = System::Drawing::Size(47, 17);
        this->allVenueTypes->TabIndex = 2;
        this->allVenueTypes->TabStop = true;
        this->allVenueTypes->Text = L"Both";
        this->allVenueTypes->UseVisualStyleBackColor = true;
        // 
        // handbells
        // 
        this->handbells->AutoSize = true;
        this->handbells->BackColor = System::Drawing::Color::Transparent;
        this->handbells->Location = System::Drawing::Point(7, 43);
        this->handbells->Name = L"handbells";
        this->handbells->Size = System::Drawing::Size(75, 17);
        this->handbells->TabIndex = 1;
        this->handbells->TabStop = true;
        this->handbells->Text = L"Hand bells";
        this->handbells->UseVisualStyleBackColor = false;
        // 
        // towerBells
        // 
        this->towerBells->AutoSize = true;
        this->towerBells->Location = System::Drawing::Point(7, 20);
        this->towerBells->Name = L"towerBells";
        this->towerBells->Size = System::Drawing::Size(60, 17);
        this->towerBells->TabIndex = 0;
        this->towerBells->TabStop = true;
        this->towerBells->Text = L"Towers";
        this->towerBells->UseVisualStyleBackColor = true;
        // 
        // bellsGroup
        // 
        bellsGroup->Controls->Add(this->bellsMoreThan);
        bellsGroup->Controls->Add(this->bellsEqualTo);
        bellsGroup->Controls->Add(this->bellsLessThan);
        bellsGroup->Controls->Add(this->bellsEditor);
        bellsGroup->Location = System::Drawing::Point(95, 6);
        bellsGroup->Name = L"bellsGroup";
        bellsGroup->Size = System::Drawing::Size(128, 97);
        bellsGroup->TabIndex = 0;
        bellsGroup->TabStop = false;
        bellsGroup->Text = L"No of Bells";
        // 
        // bellsMoreThan
        // 
        this->bellsMoreThan->AutoSize = true;
        this->bellsMoreThan->Location = System::Drawing::Point(49, 68);
        this->bellsMoreThan->Name = L"bellsMoreThan";
        this->bellsMoreThan->Size = System::Drawing::Size(73, 17);
        this->bellsMoreThan->TabIndex = 3;
        this->bellsMoreThan->Text = L"More than";
        this->bellsMoreThan->UseVisualStyleBackColor = true;
        // 
        // bellsEqualTo
        // 
        this->bellsEqualTo->AutoSize = true;
        this->bellsEqualTo->Checked = true;
        this->bellsEqualTo->Location = System::Drawing::Point(49, 45);
        this->bellsEqualTo->Name = L"bellsEqualTo";
        this->bellsEqualTo->Size = System::Drawing::Size(64, 17);
        this->bellsEqualTo->TabIndex = 2;
        this->bellsEqualTo->TabStop = true;
        this->bellsEqualTo->Text = L"Equal to";
        this->bellsEqualTo->UseVisualStyleBackColor = true;
        // 
        // bellsLessThan
        // 
        this->bellsLessThan->AutoSize = true;
        this->bellsLessThan->Location = System::Drawing::Point(49, 20);
        this->bellsLessThan->Name = L"bellsLessThan";
        this->bellsLessThan->Size = System::Drawing::Size(71, 17);
        this->bellsLessThan->TabIndex = 1;
        this->bellsLessThan->Text = L"Less than";
        this->bellsLessThan->UseVisualStyleBackColor = true;
        // 
        // bellsEditor
        // 
        this->bellsEditor->Location = System::Drawing::Point(6, 19);
        this->bellsEditor->Name = L"bellsEditor";
        this->bellsEditor->Size = System::Drawing::Size(37, 20);
        this->bellsEditor->TabIndex = 0;
        this->bellsEditor->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &TowerSearchUI::BellsEditor_KeyDown);
        this->bellsEditor->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &TowerSearchUI::BellsEditor_KeyPressed);
        // 
        // ringsGroup
        // 
        ringsGroup->Controls->Add(this->ringNameEditor);
        ringsGroup->Controls->Add(ringNameLbl);
        ringsGroup->Controls->Add(this->multipleRings);
        ringsGroup->Controls->Add(this->specialBells);
        ringsGroup->Location = System::Drawing::Point(229, 6);
        ringsGroup->Name = L"ringsGroup";
        ringsGroup->Size = System::Drawing::Size(193, 97);
        ringsGroup->TabIndex = 1;
        ringsGroup->TabStop = false;
        ringsGroup->Text = L"Rings";
        // 
        // ringNameEditor
        // 
        this->ringNameEditor->Location = System::Drawing::Point(70, 19);
        this->ringNameEditor->Name = L"ringNameEditor";
        this->ringNameEditor->Size = System::Drawing::Size(100, 20);
        this->ringNameEditor->TabIndex = 1;
        // 
        // ringNameLbl
        // 
        ringNameLbl->AutoSize = true;
        ringNameLbl->Location = System::Drawing::Point(6, 22);
        ringNameLbl->Name = L"ringNameLbl";
        ringNameLbl->Size = System::Drawing::Size(58, 13);
        ringNameLbl->TabIndex = 0;
        ringNameLbl->Text = L"Ring name";
        // 
        // multipleRings
        // 
        this->multipleRings->AutoSize = true;
        this->multipleRings->Location = System::Drawing::Point(70, 68);
        this->multipleRings->Name = L"multipleRings";
        this->multipleRings->Size = System::Drawing::Size(87, 17);
        this->multipleRings->TabIndex = 3;
        this->multipleRings->Text = L"Multiple rings";
        this->multipleRings->UseVisualStyleBackColor = true;
        // 
        // specialBells
        // 
        this->specialBells->AutoSize = true;
        this->specialBells->Location = System::Drawing::Point(70, 45);
        this->specialBells->Name = L"specialBells";
        this->specialBells->Size = System::Drawing::Size(74, 17);
        this->specialBells->TabIndex = 2;
        this->specialBells->Text = L"Extra bells";
        this->specialBells->UseVisualStyleBackColor = true;
        // 
        // tenorGroup
        // 
        tenorGroup->Controls->Add(tenorKeyLbl);
        tenorGroup->Controls->Add(this->tenorKey);
        tenorGroup->Controls->Add(tenorWeightLbl);
        tenorGroup->Controls->Add(this->tenorWeight);
        tenorGroup->Location = System::Drawing::Point(428, 6);
        tenorGroup->Name = L"tenorGroup";
        tenorGroup->Size = System::Drawing::Size(193, 97);
        tenorGroup->TabIndex = 2;
        tenorGroup->TabStop = false;
        tenorGroup->Text = L"Tenor";
        // 
        // tenorKeyLbl
        // 
        tenorKeyLbl->AutoSize = true;
        tenorKeyLbl->Location = System::Drawing::Point(27, 22);
        tenorKeyLbl->Name = L"tenorKeyLbl";
        tenorKeyLbl->Size = System::Drawing::Size(25, 13);
        tenorKeyLbl->TabIndex = 0;
        tenorKeyLbl->Text = L"Key";
        // 
        // tenorKey
        // 
        this->tenorKey->Location = System::Drawing::Point(55, 19);
        this->tenorKey->Name = L"tenorKey";
        this->tenorKey->Size = System::Drawing::Size(100, 20);
        this->tenorKey->TabIndex = 1;
        // 
        // tenorWeightLbl
        // 
        tenorWeightLbl->AutoSize = true;
        tenorWeightLbl->Location = System::Drawing::Point(12, 48);
        tenorWeightLbl->Name = L"tenorWeightLbl";
        tenorWeightLbl->Size = System::Drawing::Size(41, 13);
        tenorWeightLbl->TabIndex = 2;
        tenorWeightLbl->Text = L"Weight";
        // 
        // tenorWeight
        // 
        this->tenorWeight->Location = System::Drawing::Point(55, 45);
        this->tenorWeight->Name = L"tenorWeight";
        this->tenorWeight->Size = System::Drawing::Size(100, 20);
        this->tenorWeight->TabIndex = 3;
        // 
        // linkedTowersPage
        // 
        linkedTowersPage->Controls->Add(this->linkedTowersOnly);
        linkedTowersPage->Controls->Add(linkedTowersGrp);
        linkedTowersPage->Location = System::Drawing::Point(4, 22);
        linkedTowersPage->Name = L"linkedTowersPage";
        linkedTowersPage->Padding = System::Windows::Forms::Padding(3);
        linkedTowersPage->Size = System::Drawing::Size(827, 121);
        linkedTowersPage->TabIndex = 1;
        linkedTowersPage->Text = L"Linked towers";
        linkedTowersPage->UseVisualStyleBackColor = true;
        // 
        // linkedTowersOnly
        // 
        this->linkedTowersOnly->AutoSize = true;
        this->linkedTowersOnly->Location = System::Drawing::Point(15, 84);
        this->linkedTowersOnly->Name = L"linkedTowersOnly";
        this->linkedTowersOnly->Size = System::Drawing::Size(176, 17);
        this->linkedTowersOnly->TabIndex = 1;
        this->linkedTowersOnly->Text = L"Only towers with a Linked tower";
        toolTip1->SetToolTip(this->linkedTowersOnly, L"Only show towers with a linked tower");
        this->linkedTowersOnly->UseVisualStyleBackColor = true;
        // 
        // linkedTowersGrp
        // 
        linkedTowersGrp->Controls->Add(this->immediateLinkedTowersOnly);
        linkedTowersGrp->Controls->Add(this->linkedTowerSelector);
        linkedTowersGrp->Location = System::Drawing::Point(8, 7);
        linkedTowersGrp->Name = L"linkedTowersGrp";
        linkedTowersGrp->Size = System::Drawing::Size(342, 71);
        linkedTowersGrp->TabIndex = 0;
        linkedTowersGrp->TabStop = false;
        linkedTowersGrp->Text = L"Linked to tower";
        // 
        // immediateLinkedTowersOnly
        // 
        this->immediateLinkedTowersOnly->AutoSize = true;
        this->immediateLinkedTowersOnly->Checked = true;
        this->immediateLinkedTowersOnly->CheckState = System::Windows::Forms::CheckState::Checked;
        this->immediateLinkedTowersOnly->Location = System::Drawing::Point(7, 48);
        this->immediateLinkedTowersOnly->Name = L"immediateLinkedTowersOnly";
        this->immediateLinkedTowersOnly->Size = System::Drawing::Size(153, 17);
        this->immediateLinkedTowersOnly->TabIndex = 1;
        this->immediateLinkedTowersOnly->Text = L"Check immediate links only";
        toolTip1->SetToolTip(this->immediateLinkedTowersOnly, L"Check for links of linked towers. ");
        this->immediateLinkedTowersOnly->UseVisualStyleBackColor = true;
        // 
        // linkedTowerSelector
        // 
        this->linkedTowerSelector->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
        this->linkedTowerSelector->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
        this->linkedTowerSelector->FormattingEnabled = true;
        this->linkedTowerSelector->Location = System::Drawing::Point(7, 20);
        this->linkedTowerSelector->Name = L"linkedTowerSelector";
        this->linkedTowerSelector->Size = System::Drawing::Size(322, 21);
        this->linkedTowerSelector->TabIndex = 0;
        toolTip1->SetToolTip(this->linkedTowerSelector, L"Find all towers linked to the tower specified here.");
        // 
        // webLinksPage
        // 
        webLinksPage->Controls->Add(towerbaseIdGroup);
        webLinksPage->Controls->Add(doveGroup);
        webLinksPage->Location = System::Drawing::Point(4, 22);
        webLinksPage->Name = L"webLinksPage";
        webLinksPage->Padding = System::Windows::Forms::Padding(3);
        webLinksPage->Size = System::Drawing::Size(827, 121);
        webLinksPage->TabIndex = 2;
        webLinksPage->Text = L"Web links";
        webLinksPage->UseVisualStyleBackColor = true;
        // 
        // towerbaseIdGroup
        // 
        towerbaseIdGroup->Controls->Add(this->blankTowerbaseReference);
        towerbaseIdGroup->Controls->Add(this->towerbaseReference);
        towerbaseIdGroup->Location = System::Drawing::Point(145, 6);
        towerbaseIdGroup->Name = L"towerbaseIdGroup";
        towerbaseIdGroup->Size = System::Drawing::Size(175, 64);
        towerbaseIdGroup->TabIndex = 2;
        towerbaseIdGroup->TabStop = false;
        towerbaseIdGroup->Text = L"Towerbase reference";
        // 
        // blankTowerbaseReference
        // 
        this->blankTowerbaseReference->AutoSize = true;
        this->blankTowerbaseReference->Location = System::Drawing::Point(8, 43);
        this->blankTowerbaseReference->Name = L"blankTowerbaseReference";
        this->blankTowerbaseReference->Size = System::Drawing::Size(101, 17);
        this->blankTowerbaseReference->TabIndex = 1;
        this->blankTowerbaseReference->Text = L"Blank reference";
        this->blankTowerbaseReference->UseVisualStyleBackColor = true;
        this->blankTowerbaseReference->CheckedChanged += gcnew System::EventHandler(this, &TowerSearchUI::blankPealBaseOrFelsteadReference_CheckedChanged);
        // 
        // towerbaseReference
        // 
        this->towerbaseReference->Location = System::Drawing::Point(8, 19);
        this->towerbaseReference->Name = L"towerbaseReference";
        this->towerbaseReference->Size = System::Drawing::Size(155, 20);
        this->towerbaseReference->TabIndex = 0;
        // 
        // doveGroup
        // 
        doveGroup->Controls->Add(this->blankDoveRef);
        doveGroup->Controls->Add(this->doveRef);
        doveGroup->Location = System::Drawing::Point(6, 6);
        doveGroup->Name = L"doveGroup";
        doveGroup->Size = System::Drawing::Size(133, 64);
        doveGroup->TabIndex = 1;
        doveGroup->TabStop = false;
        doveGroup->Text = L"Dove reference";
        // 
        // blankDoveRef
        // 
        this->blankDoveRef->AutoSize = true;
        this->blankDoveRef->Location = System::Drawing::Point(8, 43);
        this->blankDoveRef->Name = L"blankDoveRef";
        this->blankDoveRef->Size = System::Drawing::Size(101, 17);
        this->blankDoveRef->TabIndex = 1;
        this->blankDoveRef->Text = L"Blank reference";
        this->blankDoveRef->UseVisualStyleBackColor = true;
        this->blankDoveRef->CheckedChanged += gcnew System::EventHandler(this, &TowerSearchUI::blankDoveRef_CheckedChanged);
        // 
        // doveRef
        // 
        this->doveRef->Location = System::Drawing::Point(8, 19);
        this->doveRef->Name = L"doveRef";
        this->doveRef->Size = System::Drawing::Size(115, 20);
        this->doveRef->TabIndex = 0;
        // 
        // otherPage
        // 
        otherPage->Controls->Add(picturesGroup);
        otherPage->Controls->Add(notesGroup);
        otherPage->Controls->Add(positionGroup);
        otherPage->Location = System::Drawing::Point(4, 22);
        otherPage->Name = L"otherPage";
        otherPage->Padding = System::Windows::Forms::Padding(3);
        otherPage->Size = System::Drawing::Size(827, 121);
        otherPage->TabIndex = 0;
        otherPage->Text = L"Other";
        otherPage->UseVisualStyleBackColor = true;
        // 
        // picturesGroup
        // 
        picturesGroup->AutoSize = true;
        picturesGroup->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
        picturesGroup->Controls->Add(this->withAndWithoutPictures);
        picturesGroup->Controls->Add(this->doesNotHavePicture);
        picturesGroup->Controls->Add(this->hasPicture);
        picturesGroup->Location = System::Drawing::Point(210, 6);
        picturesGroup->Name = L"picturesGroup";
        picturesGroup->Size = System::Drawing::Size(114, 102);
        picturesGroup->TabIndex = 26;
        picturesGroup->TabStop = false;
        picturesGroup->Text = L"Pictures";
        // 
        // withAndWithoutPictures
        // 
        this->withAndWithoutPictures->AutoSize = true;
        this->withAndWithoutPictures->Checked = true;
        this->withAndWithoutPictures->Location = System::Drawing::Point(7, 66);
        this->withAndWithoutPictures->Name = L"withAndWithoutPictures";
        this->withAndWithoutPictures->Size = System::Drawing::Size(52, 17);
        this->withAndWithoutPictures->TabIndex = 2;
        this->withAndWithoutPictures->TabStop = true;
        this->withAndWithoutPictures->Text = L"Either";
        this->withAndWithoutPictures->UseVisualStyleBackColor = true;
        // 
        // doesNotHavePicture
        // 
        this->doesNotHavePicture->AutoSize = true;
        this->doesNotHavePicture->Location = System::Drawing::Point(7, 43);
        this->doesNotHavePicture->Name = L"doesNotHavePicture";
        this->doesNotHavePicture->Size = System::Drawing::Size(94, 17);
        this->doesNotHavePicture->TabIndex = 1;
        this->doesNotHavePicture->Text = L"Has no picture";
        this->doesNotHavePicture->UseVisualStyleBackColor = true;
        // 
        // hasPicture
        // 
        this->hasPicture->AutoSize = true;
        this->hasPicture->Location = System::Drawing::Point(7, 20);
        this->hasPicture->Name = L"hasPicture";
        this->hasPicture->Size = System::Drawing::Size(101, 17);
        this->hasPicture->TabIndex = 0;
        this->hasPicture->Text = L"Contains picture";
        this->hasPicture->UseVisualStyleBackColor = true;
        // 
        // notesGroup
        // 
        notesGroup->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
        notesGroup->Controls->Add(notesLbl);
        notesGroup->Controls->Add(this->notes);
        notesGroup->Controls->Add(this->removed);
        notesGroup->Controls->Add(this->invalid);
        notesGroup->Location = System::Drawing::Point(6, 6);
        notesGroup->Name = L"notesGroup";
        notesGroup->Size = System::Drawing::Size(198, 102);
        notesGroup->TabIndex = 0;
        notesGroup->TabStop = false;
        notesGroup->Text = L"Notes";
        // 
        // notesLbl
        // 
        notesLbl->AutoSize = true;
        notesLbl->Location = System::Drawing::Point(3, 22);
        notesLbl->Name = L"notesLbl";
        notesLbl->Size = System::Drawing::Size(35, 13);
        notesLbl->TabIndex = 0;
        notesLbl->Text = L"Notes";
        // 
        // notes
        // 
        this->notes->Location = System::Drawing::Point(43, 19);
        this->notes->Name = L"notes";
        this->notes->Size = System::Drawing::Size(149, 20);
        this->notes->TabIndex = 1;
        // 
        // removed
        // 
        this->removed->AutoSize = true;
        this->removed->Location = System::Drawing::Point(43, 45);
        this->removed->Name = L"removed";
        this->removed->Size = System::Drawing::Size(95, 17);
        this->removed->TabIndex = 2;
        this->removed->Text = L"Removed peal";
        this->removed->UseVisualStyleBackColor = true;
        // 
        // invalid
        // 
        this->invalid->AutoSize = true;
        this->invalid->Location = System::Drawing::Point(43, 68);
        this->invalid->Name = L"invalid";
        this->invalid->Size = System::Drawing::Size(91, 17);
        this->invalid->TabIndex = 3;
        this->invalid->Text = L"Contains error";
        this->invalid->UseVisualStyleBackColor = true;
        // 
        // positionGroup
        // 
        positionGroup->AutoSize = true;
        positionGroup->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
        positionGroup->Controls->Add(this->invalidPosition);
        positionGroup->Controls->Add(towerSelectorLbl);
        positionGroup->Controls->Add(this->towerSelector);
        positionGroup->Location = System::Drawing::Point(330, 6);
        positionGroup->Name = L"positionGroup";
        positionGroup->Size = System::Drawing::Size(419, 82);
        positionGroup->TabIndex = 3;
        positionGroup->TabStop = false;
        positionGroup->Text = L"Position";
        // 
        // towerSelectorLbl
        // 
        towerSelectorLbl->AutoSize = true;
        towerSelectorLbl->Location = System::Drawing::Point(6, 22);
        towerSelectorLbl->Name = L"towerSelectorLbl";
        towerSelectorLbl->Size = System::Drawing::Size(106, 13);
        towerSelectorLbl->TabIndex = 0;
        towerSelectorLbl->Text = L"Sort by distance from";
        // 
        // towerSelector
        // 
        this->towerSelector->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
        this->towerSelector->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
        this->towerSelector->FormattingEnabled = true;
        this->towerSelector->Location = System::Drawing::Point(118, 19);
        this->towerSelector->Name = L"towerSelector";
        this->towerSelector->Size = System::Drawing::Size(295, 21);
        this->towerSelector->TabIndex = 1;
        toolTip1->SetToolTip(this->towerSelector, L"Limit result to towers with Dove positions set and sort by distance from this tow"
            L"er.");
        // 
        // resultsPanel
        // 
        resultsPanel->Controls->Add(this->statisticBtn);
        resultsPanel->Controls->Add(this->updateBtn);
        resultsPanel->Controls->Add(printBtn);
        resultsPanel->Controls->Add(this->clearBtn);
        resultsPanel->Controls->Add(this->results);
        resultsPanel->Controls->Add(resultsLbl);
        resultsPanel->Controls->Add(this->progressBar);
        resultsPanel->Controls->Add(closeBtn);
        resultsPanel->Controls->Add(this->searchBtn);
        resultsPanel->Dock = System::Windows::Forms::DockStyle::Fill;
        resultsPanel->Location = System::Drawing::Point(3, 338);
        resultsPanel->Name = L"resultsPanel";
        resultsPanel->Size = System::Drawing::Size(831, 28);
        resultsPanel->TabIndex = 2;
        // 
        // statisticBtn
        // 
        this->statisticBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
        this->statisticBtn->Location = System::Drawing::Point(440, 2);
        this->statisticBtn->Name = L"statisticBtn";
        this->statisticBtn->Size = System::Drawing::Size(75, 23);
        this->statisticBtn->TabIndex = 7;
        this->statisticBtn->Text = L"Statistics";
        this->statisticBtn->UseVisualStyleBackColor = true;
        this->statisticBtn->Click += gcnew System::EventHandler(this, &TowerSearchUI::statisticBtn_Click);
        // 
        // updateBtn
        // 
        this->updateBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
        this->updateBtn->Location = System::Drawing::Point(359, 2);
        this->updateBtn->Name = L"updateBtn";
        this->updateBtn->Size = System::Drawing::Size(75, 23);
        this->updateBtn->TabIndex = 3;
        this->updateBtn->Text = L"Update";
        this->updateBtn->UseVisualStyleBackColor = true;
        this->updateBtn->Click += gcnew System::EventHandler(this, &TowerSearchUI::updateBtn_Click);
        // 
        // printBtn
        // 
        printBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
        printBtn->Location = System::Drawing::Point(521, 2);
        printBtn->Name = L"printBtn";
        printBtn->Size = System::Drawing::Size(75, 23);
        printBtn->TabIndex = 4;
        printBtn->Text = L"Print";
        printBtn->UseVisualStyleBackColor = true;
        printBtn->Click += gcnew System::EventHandler(this, &TowerSearchUI::printBtn_Click);
        // 
        // clearBtn
        // 
        this->clearBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
        this->clearBtn->Location = System::Drawing::Point(602, 2);
        this->clearBtn->Margin = System::Windows::Forms::Padding(3, 3, 0, 3);
        this->clearBtn->Name = L"clearBtn";
        this->clearBtn->Size = System::Drawing::Size(75, 23);
        this->clearBtn->TabIndex = 5;
        this->clearBtn->Text = L"Clear";
        this->clearBtn->UseVisualStyleBackColor = true;
        this->clearBtn->Click += gcnew System::EventHandler(this, &TowerSearchUI::clearBtn_Click);
        // 
        // results
        // 
        this->results->Location = System::Drawing::Point(160, 4);
        this->results->Name = L"results";
        this->results->ReadOnly = true;
        this->results->Size = System::Drawing::Size(34, 20);
        this->results->TabIndex = 2;
        // 
        // resultsLbl
        // 
        resultsLbl->AutoSize = true;
        resultsLbl->Location = System::Drawing::Point(111, 8);
        resultsLbl->Name = L"resultsLbl";
        resultsLbl->Size = System::Drawing::Size(42, 13);
        resultsLbl->TabIndex = 1;
        resultsLbl->Text = L"Results";
        // 
        // progressBar
        // 
        this->progressBar->Location = System::Drawing::Point(4, 5);
        this->progressBar->Margin = System::Windows::Forms::Padding(5, 5, 4, 4);
        this->progressBar->Name = L"progressBar";
        this->progressBar->Size = System::Drawing::Size(100, 18);
        this->progressBar->TabIndex = 0;
        // 
        // closeBtn
        // 
        closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
        closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
        closeBtn->Location = System::Drawing::Point(756, 2);
        closeBtn->Margin = System::Windows::Forms::Padding(1, 2, 3, 3);
        closeBtn->Name = L"closeBtn";
        closeBtn->Size = System::Drawing::Size(75, 23);
        closeBtn->TabIndex = 6;
        closeBtn->Text = L"Close";
        closeBtn->UseVisualStyleBackColor = true;
        closeBtn->Click += gcnew System::EventHandler(this, &TowerSearchUI::closeBtn_Click);
        // 
        // searchBtn
        // 
        this->searchBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
        this->searchBtn->Location = System::Drawing::Point(677, 2);
        this->searchBtn->Margin = System::Windows::Forms::Padding(0, 3, 3, 3);
        this->searchBtn->Name = L"searchBtn";
        this->searchBtn->Size = System::Drawing::Size(75, 23);
        this->searchBtn->TabIndex = 5;
        this->searchBtn->Text = L"Search";
        this->searchBtn->UseVisualStyleBackColor = true;
        this->searchBtn->Click += gcnew System::EventHandler(this, &TowerSearchUI::searchBtn_Click);
        // 
        // backgroundWorker
        // 
        this->backgroundWorker->WorkerReportsProgress = true;
        this->backgroundWorker->WorkerSupportsCancellation = true;
        this->backgroundWorker->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &TowerSearchUI::backgroundWorker_DoWork);
        this->backgroundWorker->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &TowerSearchUI::backgroundWorker_ProgressChanged);
        this->backgroundWorker->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &TowerSearchUI::backgroundWorker_RunWorkerCompleted);
        // 
        // invalidPosition
        // 
        this->invalidPosition->AutoSize = true;
        this->invalidPosition->Location = System::Drawing::Point(118, 46);
        this->invalidPosition->Name = L"invalidPosition";
        this->invalidPosition->Size = System::Drawing::Size(96, 17);
        this->invalidPosition->TabIndex = 2;
        this->invalidPosition->Text = L"Invalid position";
        this->invalidPosition->UseVisualStyleBackColor = true;
        // 
        // TowerSearchUI
        // 
        this->AcceptButton = this->searchBtn;
        this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
        this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
        this->CancelButton = closeBtn;
        this->ClientSize = System::Drawing::Size(837, 369);
        this->Controls->Add(tableLayoutPanel1);
        this->MinimumSize = System::Drawing::Size(612, 323);
        this->Name = L"TowerSearchUI";
        this->Text = L"Search towers";
        this->Closing += gcnew System::ComponentModel::CancelEventHandler(this, &TowerSearchUI::ClosingEvent);
        this->Load += gcnew System::EventHandler(this, &TowerSearchUI::TowerSearchUI_Load);
        this->Scroll += gcnew System::Windows::Forms::ScrollEventHandler(this, &TowerSearchUI::TowerSearchUI_Scroll);
        tableLayoutPanel1->ResumeLayout(false);
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->towersData))->EndInit();
        towerDataMenu->ResumeLayout(false);
        tabbedControl->ResumeLayout(false);
        namePage->ResumeLayout(false);
        nameGroup->ResumeLayout(false);
        nameGroup->PerformLayout();
        bellsPage->ResumeLayout(false);
        bellsPage->PerformLayout();
        towerTypeGroup->ResumeLayout(false);
        towerTypeGroup->PerformLayout();
        bellsGroup->ResumeLayout(false);
        bellsGroup->PerformLayout();
        ringsGroup->ResumeLayout(false);
        ringsGroup->PerformLayout();
        tenorGroup->ResumeLayout(false);
        tenorGroup->PerformLayout();
        linkedTowersPage->ResumeLayout(false);
        linkedTowersPage->PerformLayout();
        linkedTowersGrp->ResumeLayout(false);
        linkedTowersGrp->PerformLayout();
        webLinksPage->ResumeLayout(false);
        towerbaseIdGroup->ResumeLayout(false);
        towerbaseIdGroup->PerformLayout();
        doveGroup->ResumeLayout(false);
        doveGroup->PerformLayout();
        otherPage->ResumeLayout(false);
        otherPage->PerformLayout();
        picturesGroup->ResumeLayout(false);
        picturesGroup->PerformLayout();
        notesGroup->ResumeLayout(false);
        notesGroup->PerformLayout();
        positionGroup->ResumeLayout(false);
        positionGroup->PerformLayout();
        resultsPanel->ResumeLayout(false);
        resultsPanel->PerformLayout();
        this->ResumeLayout(false);

    }
    };
}
