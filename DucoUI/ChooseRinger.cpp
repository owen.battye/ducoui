#include <set>
#include <ObjectId.h>
#include "DatabaseManager.h"
#include <RingerPealDates.h>

#include "ChooseRinger.h"

#include <Peal.h>
#include "DucoUtils.h"
#include "SystemDefaultIcon.h"
#include <StatisticFilters.h>
#include <RingingDatabase.h>
#include <PealDatabase.h>
#include "SoundUtils.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace DucoUI;

ChooseRinger::ChooseRinger(DatabaseManager^ theDatabase, System::String^ missingRinger)
    : database(theDatabase)
{
    ringerIds = gcnew System::Collections::Generic::List<System::Int64>();
    missingRingerName = missingRinger;

    InitializeComponent();
    this->Text += L" to replace " + missingRinger;
    CreateRingerControls(missingRinger);
    ringerLbl->Text = L"Could not find a match for " + missingRingerName + ".";
    this->tableLayoutPanel1->PerformLayout();
}

ChooseRinger::~ChooseRinger()
{
    if (components)
    {
        delete components;
    }
}

void
ChooseRinger::CreateRingerControls(System::String^ ringerName)
{
    ringerIds = database->SuggestAllRingers(ringerName);

    System::Collections::Generic::List<System::Int64>::Enumerator^ it = ringerIds->GetEnumerator();

    while (it->MoveNext())
    {
        System::Windows::Forms::RadioButton^ radioButton1 = gcnew System::Windows::Forms::RadioButton();
        ringers->Controls->Add(radioButton1);
        radioButton1->AutoSize = true;
        radioButton1->Location = System::Drawing::Point(3, 3);
        radioButton1->Name = L"radioButton1";
        radioButton1->Size = System::Drawing::Size(85, 17);
        radioButton1->TabIndex = 0;
        radioButton1->TabStop = true;
        radioButton1->Text = database->RingerName(Duco::ObjectId(it->Current));
        radioButton1->UseVisualStyleBackColor = true;
    }
    if (ringerIds->Count == 0)
    {
        System::Windows::Forms::Label^ noRingersLbl = (gcnew System::Windows::Forms::Label());
        noRingersLbl->AutoSize = true;
        ringers->Controls->Add(noRingersLbl);
        noRingersLbl->Dock = System::Windows::Forms::DockStyle::Fill;
        noRingersLbl->Location = System::Drawing::Point(3, 3);
        noRingersLbl->Margin = System::Windows::Forms::Padding(3);
        noRingersLbl->Name = L"noRingersLbl";
        noRingersLbl->Size = System::Drawing::Size(448, 13);
        noRingersLbl->TabIndex = 3;
        noRingersLbl->Text = L"No similar ringers found";
    }
}

System::Void
ChooseRinger::ChooseRinger_Load(System::Object^ sender, System::EventArgs^ e) 
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
}

Duco::ObjectId
ChooseRinger::SelectedId()
{
    for (int i = 0; i < ringers->Controls->Count; ++i)
    {
        System::Windows::Forms::RadioButton^ radioButton = dynamic_cast<System::Windows::Forms::RadioButton^>(ringers->Controls[i]);
        if (radioButton->Checked)
        {
            return Duco::ObjectId(ringerIds[i]);
        }
    }
    return Duco::ObjectId();
}

System::Void
ChooseRinger::okButton_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (SelectedId().ValidId())
    {
        this->DialogResult = ::DialogResult::OK;
        Close();
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}
