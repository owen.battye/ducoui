#pragma once
namespace DucoUI
{
	public ref class ReplaceAssociationUI : public System::Windows::Forms::Form, public DucoUI::ProgressCallbackUI
	{
	public:
		ReplaceAssociationUI(DucoUI::DatabaseManager^ theDatabase);

        // from ProgressCallback
        virtual void Step(int progressPercent);
        virtual void Initialised();
        virtual void Complete();

    protected:
		!ReplaceAssociationUI();
		~ReplaceAssociationUI();

        System::Void ReplaceAssociationUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void ReplaceAssociationUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e);
        System::Void EnableReplaceBtn(const Duco::ObjectId& oldAssociationId, const Duco::ObjectId& newAssociationId);

        System::Void backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void backgroundWorker1_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void backgroundWorker1_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);

        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void replaceBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void swapBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void oldAssociation_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void newAssociation_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);

        ref class ReplaceAssociationUIArgs
        {
        public:
            const Duco::ObjectId* oldAssociationId;
            const Duco::ObjectId* newAssociationId;
        };


	private:
		DucoUI::ProgressCallbackWrapper*			    progressWrapper;
        DucoUI::DatabaseManager^                        database;
		System::ComponentModel::Container^              components;
        System::Windows::Forms::ComboBox^               oldAssociation;
        System::Windows::Forms::ComboBox^               newAssociation;
        System::Windows::Forms::Button^                 replaceBtn;
        System::Windows::Forms::Button^                 closeBtn;
        System::Windows::Forms::ToolStripProgressBar^   progressBar;
        System::ComponentModel::BackgroundWorker^       backgroundWorker1;
    private: System::Windows::Forms::ToolStripStatusLabel^  statusLabel;
    private: System::Windows::Forms::Button^  swapBtn;
    private: System::Windows::Forms::Label^  oldAssociationLbl;
    private: System::Windows::Forms::Label^  newAssociationLbl;

             System::Collections::Generic::List<System::Int16>^  allAssociationNames;

#pragma region Windows Form Designer generated code
    void InitializeComponent(void)
    {
        System::Windows::Forms::Label^  label1;
        System::Windows::Forms::Label^  label2;
        System::Windows::Forms::StatusStrip^  statusStrip1;
        System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
        this->progressBar = (gcnew System::Windows::Forms::ToolStripProgressBar());
        this->statusLabel = (gcnew System::Windows::Forms::ToolStripStatusLabel());
        this->newAssociation = (gcnew System::Windows::Forms::ComboBox());
        this->replaceBtn = (gcnew System::Windows::Forms::Button());
        this->oldAssociation = (gcnew System::Windows::Forms::ComboBox());
        this->closeBtn = (gcnew System::Windows::Forms::Button());
        this->swapBtn = (gcnew System::Windows::Forms::Button());
        this->oldAssociationLbl = (gcnew System::Windows::Forms::Label());
        this->newAssociationLbl = (gcnew System::Windows::Forms::Label());
        this->backgroundWorker1 = (gcnew System::ComponentModel::BackgroundWorker());
        label1 = (gcnew System::Windows::Forms::Label());
        label2 = (gcnew System::Windows::Forms::Label());
        statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
        tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
        statusStrip1->SuspendLayout();
        tableLayoutPanel1->SuspendLayout();
        this->SuspendLayout();
        // 
        // label1
        // 
        label1->AutoSize = true;
        label1->Dock = System::Windows::Forms::DockStyle::Fill;
        label1->Location = System::Drawing::Point(3, 0);
        label1->Name = L"label1";
        label1->Size = System::Drawing::Size(111, 27);
        label1->TabIndex = 0;
        label1->Text = L"Association to remove";
        label1->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
        // 
        // label2
        // 
        label2->AutoSize = true;
        label2->Dock = System::Windows::Forms::DockStyle::Fill;
        label2->Location = System::Drawing::Point(3, 27);
        label2->Name = L"label2";
        label2->Size = System::Drawing::Size(111, 27);
        label2->TabIndex = 1;
        label2->Text = L"New Association";
        label2->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
        // 
        // statusStrip1
        // 
        statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) { this->progressBar, this->statusLabel });
        statusStrip1->Location = System::Drawing::Point(0, 81);
        statusStrip1->Name = L"statusStrip1";
        statusStrip1->Size = System::Drawing::Size(489, 22);
        statusStrip1->TabIndex = 6;
        statusStrip1->Text = L"statusStrip1";
        // 
        // progressBar
        // 
        this->progressBar->Name = L"progressBar";
        this->progressBar->Size = System::Drawing::Size(100, 16);
        // 
        // statusLabel
        // 
        this->statusLabel->Name = L"statusLabel";
        this->statusLabel->Size = System::Drawing::Size(372, 17);
        this->statusLabel->Spring = true;
        this->statusLabel->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
        // 
        // tableLayoutPanel1
        // 
        tableLayoutPanel1->ColumnCount = 3;
        tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
        tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
        tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
        tableLayoutPanel1->Controls->Add(label1, 0, 0);
        tableLayoutPanel1->Controls->Add(label2, 0, 1);
        tableLayoutPanel1->Controls->Add(this->newAssociation, 1, 1);
        tableLayoutPanel1->Controls->Add(this->replaceBtn, 1, 3);
        tableLayoutPanel1->Controls->Add(this->oldAssociation, 1, 0);
        tableLayoutPanel1->Controls->Add(this->closeBtn, 2, 3);
        tableLayoutPanel1->Controls->Add(this->swapBtn, 0, 3);
        tableLayoutPanel1->Controls->Add(this->oldAssociationLbl, 2, 0);
        tableLayoutPanel1->Controls->Add(this->newAssociationLbl, 2, 1);
        tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
        tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
        tableLayoutPanel1->Name = L"tableLayoutPanel1";
        tableLayoutPanel1->RowCount = 4;
        tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
        tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
        tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
        tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
        tableLayoutPanel1->Size = System::Drawing::Size(489, 81);
        tableLayoutPanel1->TabIndex = 7;
        // 
        // newAssociation
        // 
        this->newAssociation->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
        this->newAssociation->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
        this->newAssociation->Dock = System::Windows::Forms::DockStyle::Fill;
        this->newAssociation->FormattingEnabled = true;
        this->newAssociation->Location = System::Drawing::Point(120, 30);
        this->newAssociation->Name = L"newAssociation";
        this->newAssociation->Size = System::Drawing::Size(285, 21);
        this->newAssociation->TabIndex = 3;
        this->newAssociation->SelectedIndexChanged += gcnew System::EventHandler(this, &ReplaceAssociationUI::newAssociation_SelectedIndexChanged);
        // 
        // replaceBtn
        // 
        this->replaceBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
        this->replaceBtn->Location = System::Drawing::Point(330, 55);
        this->replaceBtn->Name = L"replaceBtn";
        this->replaceBtn->Size = System::Drawing::Size(75, 23);
        this->replaceBtn->TabIndex = 4;
        this->replaceBtn->Text = L"Replace";
        this->replaceBtn->UseVisualStyleBackColor = true;
        this->replaceBtn->Click += gcnew System::EventHandler(this, &ReplaceAssociationUI::replaceBtn_Click);
        // 
        // oldAssociation
        // 
        this->oldAssociation->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
        this->oldAssociation->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
        this->oldAssociation->Dock = System::Windows::Forms::DockStyle::Fill;
        this->oldAssociation->FormattingEnabled = true;
        this->oldAssociation->Location = System::Drawing::Point(120, 3);
        this->oldAssociation->Name = L"oldAssociation";
        this->oldAssociation->Size = System::Drawing::Size(285, 21);
        this->oldAssociation->TabIndex = 2;
        this->oldAssociation->SelectedIndexChanged += gcnew System::EventHandler(this, &ReplaceAssociationUI::oldAssociation_SelectedIndexChanged);
        // 
        // closeBtn
        // 
        this->closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
        this->closeBtn->Location = System::Drawing::Point(411, 55);
        this->closeBtn->Name = L"closeBtn";
        this->closeBtn->Size = System::Drawing::Size(75, 23);
        this->closeBtn->TabIndex = 5;
        this->closeBtn->Text = L"Close";
        this->closeBtn->UseVisualStyleBackColor = true;
        this->closeBtn->Click += gcnew System::EventHandler(this, &ReplaceAssociationUI::closeBtn_Click);
        // 
        // swapBtn
        // 
        this->swapBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
        this->swapBtn->Location = System::Drawing::Point(39, 55);
        this->swapBtn->Name = L"swapBtn";
        this->swapBtn->Size = System::Drawing::Size(75, 23);
        this->swapBtn->TabIndex = 6;
        this->swapBtn->Text = L"Swap";
        this->swapBtn->UseVisualStyleBackColor = true;
        this->swapBtn->Click += gcnew System::EventHandler(this, &ReplaceAssociationUI::swapBtn_Click);
        // 
        // oldAssociationLbl
        // 
        this->oldAssociationLbl->AutoSize = true;
        this->oldAssociationLbl->Dock = System::Windows::Forms::DockStyle::Fill;
        this->oldAssociationLbl->Location = System::Drawing::Point(411, 0);
        this->oldAssociationLbl->Name = L"oldAssociationLbl";
        this->oldAssociationLbl->Size = System::Drawing::Size(75, 27);
        this->oldAssociationLbl->TabIndex = 7;
        this->oldAssociationLbl->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
        // 
        // newAssociationLbl
        // 
        this->newAssociationLbl->AutoSize = true;
        this->newAssociationLbl->Dock = System::Windows::Forms::DockStyle::Fill;
        this->newAssociationLbl->Location = System::Drawing::Point(411, 27);
        this->newAssociationLbl->Name = L"newAssociationLbl";
        this->newAssociationLbl->Size = System::Drawing::Size(75, 27);
        this->newAssociationLbl->TabIndex = 8;
        this->newAssociationLbl->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
        // 
        // backgroundWorker1
        // 
        this->backgroundWorker1->WorkerReportsProgress = true;
        this->backgroundWorker1->WorkerSupportsCancellation = true;
        this->backgroundWorker1->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &ReplaceAssociationUI::backgroundWorker1_DoWork);
        this->backgroundWorker1->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &ReplaceAssociationUI::backgroundWorker1_ProgressChanged);
        this->backgroundWorker1->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &ReplaceAssociationUI::backgroundWorker1_RunWorkerCompleted);
        // 
        // ReplaceAssociationUI
        // 
        this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
        this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
        this->ClientSize = System::Drawing::Size(489, 103);
        this->Controls->Add(tableLayoutPanel1);
        this->Controls->Add(statusStrip1);
        this->Name = L"ReplaceAssociationUI";
        this->Text = L"Replace association";
        this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &ReplaceAssociationUI::ReplaceAssociationUI_FormClosing);
        this->Load += gcnew System::EventHandler(this, &ReplaceAssociationUI::ReplaceAssociationUI_Load);
        statusStrip1->ResumeLayout(false);
        statusStrip1->PerformLayout();
        tableLayoutPanel1->ResumeLayout(false);
        tableLayoutPanel1->PerformLayout();
        this->ResumeLayout(false);
        this->PerformLayout();

    }
#pragma endregion
};
}
