#include <Peal.h>
#include "DatabaseManager.h"
#include "RingerEventArgs.h"

#include "PealFeeObjectBase.h"

#include "DucoUtils.h"
#include <DatabaseSettings.h>
#include <Ringer.h>
#include "DucoCommon.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Drawing;
using namespace Duco;
using namespace DucoUI;

PealFeeObjectBase::PealFeeObjectBase(DatabaseManager^ theDatabase, unsigned int theBellNumber, bool isAStrapper, Duco::Peal* thePeal)
:   System::Windows::Forms::UserControl(), database(theDatabase), currentPeal(thePeal),
    bellNumber(theBellNumber), isStrapper(isAStrapper)
{
    InitializeComponent();
    SetPeal();
}

PealFeeObjectBase::!PealFeeObjectBase()
{
}

PealFeeObjectBase::~PealFeeObjectBase()
{
    this->!PealFeeObjectBase();
    if (components)
    {
        delete components;
    }
}

System::Void
PealFeeObjectBase::maskedTextBox1_Leave(System::Object^  sender, System::EventArgs^  e)
{
    if (maskedTextBox1->MaskCompleted)
    {
        float value = Convert::ToSingle(maskedTextBox1->Text->Substring(1));
        value *= 100;
        if (currentPeal->SetPealFee(bellNumber, isStrapper, unsigned int(value)))
        {
            OnRingerDataChanged(nullptr);
        }
    }
}

bool
PealFeeObjectBase::SetPeal()
{
    if (currentPeal == NULL)
        return false;

    bool allPopulated (true);

    String^ bellNoString = "";
    if (isStrapper)
    {
        bellNoString = "Strapper";
    }
    else if (bellNumber == 1 && !currentPeal->Handbell())
    {
        bellNoString = "Treble";
    }
    else if (currentPeal->MethodId() != -1 && bellNumber == currentPeal->NoOfBellsRung(database->Database()) && !currentPeal->Handbell())
    {
        bellNoString = "Tenor";
    }
    else 
    {
        bellNoString = Convert::ToString(bellNumber);
        if (currentPeal->Handbell())
        {
            bellNoString = Convert::ToString((bellNumber*2) -1);
            bellNoString += "-";
            bellNoString += Convert::ToString(bellNumber*2);
        }
    }
    ringerLbl->Text = bellNoString;

    Duco::ObjectId ringerId;
    bool asStrapper(isStrapper);
    if (currentPeal->RingerId(bellNumber, asStrapper, ringerId))
    {
        Duco::Ringer theRinger;
        if (database->FindRinger(ringerId, theRinger, false))
        {
            ringerNameLbl->Text = DucoUtils::ConvertString(theRinger.FullName(database->Settings().LastNameFirst()));
        }
    }
    else
    {
        ringerNameLbl->Text = "Unknown ringer";
        allPopulated = false;
    }

    float pealFeeInPence = float(currentPeal->PealFee(bellNumber, asStrapper));
    if (pealFeeInPence > 0)
    {
        pealFeeInPence /= 100;
        String^ temp = String::Format("{0:00.00}", pealFeeInPence);
        maskedTextBox1->Text = temp;
    }

    return allPopulated;
}

void
PealFeeObjectBase::OnRingerDataChanged(RingerEventArgs^ e)
{
    dataChangedEventHandler(this, e);
}

System::Void
PealFeeObjectBase::PealFeeObjectChanged(System::Object^  sender, System::EventArgs^  e)
{
    OnRingerDataChanged(nullptr);
}

void
PealFeeObjectBase::InitializeComponent()
{
    this->ringerNameLbl = (gcnew System::Windows::Forms::Label());
    this->ringerLbl = (gcnew System::Windows::Forms::Label());
    this->maskedTextBox1 = (gcnew System::Windows::Forms::MaskedTextBox());
    this->SuspendLayout();
    // 
    // ringerLbl
    // 
    this->ringerLbl->AutoSize = false;
    this->ringerLbl->Location = System::Drawing::Point(0, 4);
    this->ringerLbl->Name = L"ringerLbl";
    this->ringerLbl->Size = System::Drawing::Size(KLabelWidth, KLabelHeight);
    this->ringerLbl->TabIndex = 0;
    this->ringerLbl->Text = L"ringerLbl";
    this->ringerLbl->TextAlign = ContentAlignment::MiddleRight;
    // 
    // ringerNameLbl
    // 
    this->ringerNameLbl->AutoSize = false;
    this->ringerNameLbl->Location = System::Drawing::Point(50, 4);
    this->ringerNameLbl->Name = L"ringerNameLbl";
    this->ringerNameLbl->Size = System::Drawing::Size(KEditorWidth, KLabelHeight);
    this->ringerNameLbl->TabIndex = 0;
    this->ringerNameLbl->TextAlign = ContentAlignment::MiddleLeft;
    // 
    // maskedTextBox1
    // 
    this->maskedTextBox1->Location = System::Drawing::Point(275, 1);
    this->maskedTextBox1->Mask = L"$90.00";
    this->maskedTextBox1->Name = L"maskedTextBox1";
    this->maskedTextBox1->Size = System::Drawing::Size(40, 20);
    this->maskedTextBox1->TabIndex = 2;
    this->maskedTextBox1->ReadOnly = false;
    this->maskedTextBox1->BeepOnError = true;
    this->maskedTextBox1->Leave += gcnew System::EventHandler(this, &PealFeeObjectBase::maskedTextBox1_Leave);
    // 
    // PealFeeObjectBase
    // 
    this->Name = L"PealFeeObjectBase";
    this->Size = System::Drawing::Size(KRingerWidth, KRingerLineHeight);
    this->AutoSize = true;

    Controls->AddRange(gcnew cli::array< System::Windows::Forms::Control^  >(2){ringerLbl, ringerNameLbl});
    this->Controls->Add(this->maskedTextBox1);
    this->ResumeLayout(false);
}
