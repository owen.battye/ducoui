#pragma once

#include <chrono>

namespace DucoUI
{
//#define ENABLELOGGING 1
#if defined(_DEBUG) || defined(ENABLELOGGING)
#define DUCOUILOGDEBUG(a) DucoUILog::PrintDebug(a)
#define DUCOUILOGDEBUGWITHTIME(a,b) DucoUILog::PrintDebug(a,b)
#else
#define DUCOUILOGDEBUG(a)
#define DUCOUILOGDEBUGWITHTIME(a,b)
#endif

    ref class DucoUILog
    {
        public:
            static void PrintDebugToConsole(const std::string& text);
            static void PrintDebug(std::chrono::steady_clock::time_point startTime, System::String^ text);
            static void PrintDebug(System::String^ text);
            static void PrintDebug(System::String^ text, System::DateTime^ startTime);
            static void PrintError(System::String^ text);
            static void Close();

        protected:
            DucoUILog();

        protected:
            static System::IO::FileStream^ fileHandle;
    };
}

