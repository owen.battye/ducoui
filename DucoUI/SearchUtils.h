#pragma once

#include <SearchCommon.h>
namespace Duco
{
    class DatabaseSearch;
}

namespace DucoUI
{
    ref class SearchUtils
    {
    public:
        static System::Void AddStringSearchArgument(Duco::DatabaseSearch& search, System::String^ text, Duco::TSearchFieldId fieldId, bool excluding);
        static System::Void AddStringSearchArgument(Duco::DatabaseSearch& search, System::String^ text, Duco::TSearchFieldId fieldId, Duco::TSearchFieldId altFieldId, bool excluding);
    };
}