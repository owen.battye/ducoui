#pragma once
namespace DucoUI
{
    public ref class MonthsUI : public System::Windows::Forms::Form, public DucoUI::RingingDatabaseObserver
    {
    public:
        MonthsUI(DucoUI::DatabaseManager^ theDatabase);

        //from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

    protected:
        !MonthsUI();
        ~MonthsUI();
        void InitializeComponent();

        System::Void GenerateData();
        System::Void MonthsUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void settingsChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void filtersBtn_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void backgroundGenerator_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void backgroundGenerator_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void backgroundGenerator_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);

    private:
        DucoUI::DatabaseManager^                            database;
        Duco::StatisticFilters*                             filters;
        System::Windows::Forms::DataGridView^               dataGridView;
        bool                                                restartGenerator;

        System::Windows::Forms::ToolStripProgressBar^       progressBar;
        System::ComponentModel::BackgroundWorker^           backgroundGenerator;

        System::Windows::Forms::DataGridViewTextBoxColumn^  januaryColumn;
        System::Windows::Forms::DataGridViewTextBoxColumn^  februaryColumn;
        System::Windows::Forms::DataGridViewTextBoxColumn^  marchColumn;
        System::Windows::Forms::DataGridViewTextBoxColumn^  aprilColumn;
        System::Windows::Forms::DataGridViewTextBoxColumn^  mayColumn;
        System::Windows::Forms::DataGridViewTextBoxColumn^  juneColumn;
        System::Windows::Forms::DataGridViewTextBoxColumn^  julyColumn;
        System::Windows::Forms::DataGridViewTextBoxColumn^  augustColumn;
        System::Windows::Forms::DataGridViewTextBoxColumn^  septemberColumn;
        System::Windows::Forms::DataGridViewTextBoxColumn^  octoberColumn;
        System::Windows::Forms::DataGridViewTextBoxColumn^  novemberColumn;
        System::Windows::Forms::DataGridViewTextBoxColumn^  decemberColumn;
        System::Windows::Forms::DataGridViewTextBoxColumn^  totalColumn;
        System::Windows::Forms::DataGridViewTextBoxColumn^  runningTotalColumn;

        System::ComponentModel::Container^                  components;
    };
}
