#include "DownloadNewVersionUI.h"
#include "DucoUtils.h"
#include <DucoVersionNumber.h>
#include <DucoConfiguration.h>
#include "DucoCommon.h"

using namespace DucoUI;
using namespace System::IO;
using namespace System::Windows::Forms;
using namespace System::Drawing;

bool
DownloadNewVersionUI::Show(System::Windows::Forms::IWin32Window^ owner, System::Net::WebClient^ client, Duco::DucoVersionNumber& newVersionNumber, System::String^ downloadFilename, Duco::DucoConfiguration& theConfig)
{
    DownloadNewVersionUI^ dialog = gcnew DownloadNewVersionUI(client, newVersionNumber, downloadFilename, theConfig);
    System::Windows::Forms::DialogResult returnVal = dialog->ShowDialog(owner);

    return returnVal == System::Windows::Forms::DialogResult::Yes;
}

DownloadNewVersionUI::DownloadNewVersionUI(System::Net::WebClient^ client, Duco::DucoVersionNumber& newVersionNo, System::String^ downloadFlnm, Duco::DucoConfiguration& theConfig)
    : downloadClient(client), config(theConfig), downloadFilepath(downloadFlnm), newVersionNumber(newVersionNo)
{
    InitializeComponent();
}

DownloadNewVersionUI::~DownloadNewVersionUI()
{
    if (components)
    {
        delete components;
    }
}

System::Void
DownloadNewVersionUI::yesButton_Click(System::Object^  sender, System::EventArgs^  e)
{
    try
    {
#ifdef _WIN64
        System::String^ typePrefix = "_x64_v";
#else
        System::String^ typePrefix = "_v";
#endif

        System::String^ downloadFilename = "DucoSetup" + typePrefix + DucoUtils::ConvertString(newVersionNumber.Str()) + ".msi";

        System::Uri^ fileLocation = gcnew System::Uri("http://" + KWebSite + "/downloads/" + downloadFilename);
        downloadClient->DownloadFileAsync(fileLocation, downloadFilepath);
    }
    catch (System::Net::WebException^ ex) 
    {
        downloadFilepath = nullptr;
        MessageBox::Show(ex->Message);
    }
    catch (System::Exception^ other)
    {
        downloadFilepath = nullptr;
        MessageBox::Show(other->Message);
    }
    Close();
}

System::Void
DownloadNewVersionUI::noButton_Click(System::Object^  sender, System::EventArgs^  e)
{
}

System::Void
DownloadNewVersionUI::ignoreButton_Click(System::Object^  sender, System::EventArgs^  e)
{
    config.DisableNextVersion();
    Close();
}

System::Void
DownloadNewVersionUI::DownloadNewVersionUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Bitmap^ bitmap1 = Bitmap::FromHicon(SystemIcons::Question->Handle);
    this->newDownloadLabel->Text = L"Version " + DucoUtils::ConvertString(newVersionNumber.Str()) + " of Duco is available, do you wish to download it now\?";
    this->newDownloadLabel->Image = bitmap1;
}
