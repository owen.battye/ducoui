#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"
#include "PrintBandSummaryUtil.h"

#include "BandStats.h"

#include "DatabaseGenUtils.h"
#include "DucoUIUtils.h"
#include <RingingDatabase.h>
#include <PealDatabase.h>

using namespace DucoUI;
using namespace System;
using namespace System::Drawing::Printing;

BandStats::BandStats(DucoUI::DatabaseManager^ theDatabase, const Duco::ObjectId& theTowerId)
    : database(theDatabase)
{
    ringerCache = DucoUI::RingerDisplayCache::CreateDisplayCacheWithRingers(database, false, true);
    towerId = new Duco::ObjectId(theTowerId);
    ringerIds = new std::set<Duco::ObjectId>();
    InitializeComponent();
    allTowerIds = gcnew System::Collections::Generic::List<System::Int16>();
}

BandStats::~BandStats()
{
    delete towerId;
    delete ringerIds;
    if (components)
    {
        delete components;
    }
}

System::Void
BandStats::BandStats_Load(System::Object^ sender, System::EventArgs^ e)
{
    ringerCache->PopulateControl(ringersCtrl, false);
    DatabaseGenUtils::GenerateTowerOptions(towerCtrl, allTowerIds, database, *towerId, false, false, false);
}

System::Void
BandStats::towerCtrl_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e)
{
    SetRingers();
}


System::Void
BandStats::SetRingers()
{
    *towerId = DatabaseGenUtils::FindId(allTowerIds, towerCtrl->SelectedIndex);
    towerCtrl->BeginUpdate();

    std::set<Duco::ObjectId> ringerIds;
    database->Database().PealsDatabase().GetRingersInTower(*towerId, ringerIds, 20);

    System::Windows::Forms::ListBox::SelectedIndexCollection^ indexes = ringersCtrl->SelectedIndices;
    indexes->Clear();

    std::set<Duco::ObjectId>::const_reverse_iterator it = ringerIds.rbegin();
    while (it != ringerIds.rend())
    {
        System::Int32 index = ringerCache->FindIndexOrAdd(*it);
        indexes->Add(index);
        ++it;
    }
    towerCtrl->EndUpdate();
}

System::Void
BandStats::closeBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    Close();
}

System::Void
BandStats::previewBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    *ringerIds = ringerCache->FindIds(ringersCtrl->SelectedItems);
    DucoUIUtils::Print(this, sender, gcnew System::Drawing::Printing::PrintPageEventHandler(this, &BandStats::PrintBandStatsInTower), "band", true, false);
}

System::Void
BandStats::printBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    *ringerIds = *ringerIds = ringerCache->FindIds(ringersCtrl->SelectedItems);
    DucoUIUtils::Print(this, sender, gcnew System::Drawing::Printing::PrintPageEventHandler(this, &BandStats::PrintBandStatsInTower), "band", true, false);
}

System::Void
BandStats::PrintBandStatsInTower(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args)
{
    PrintDocument^ printDoc = static_cast<PrintDocument^>(sender);
    if (printBandStatsUtil == nullptr)
    {
        printBandStatsUtil = gcnew PrintBandSummaryUtil(database, args, *towerId, *ringerIds);
        printBandStatsUtil->printObject(args, printDoc->PrinterSettings);
    }
    if (printBandStatsUtil->morePages)
    {
        args->HasMorePages = true;
    }
    else
    {
        printBandStatsUtil = nullptr;
    }
}

