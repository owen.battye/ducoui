#pragma once
#include "PrintUtilBase.h"
#include "ObjectType.h"

namespace Duco
{
    class ObjectId;
}

namespace DucoUI
{
    public ref class GenericTablePrintUtil : public PrintUtilBase
    {
    public:
        GenericTablePrintUtil(DucoUI::DatabaseManager^ theDatabase, System::Drawing::Printing::PrintPageEventArgs^ args, System::String^ newTitle);
        !GenericTablePrintUtil();
        ~GenericTablePrintUtil();

        System::Void SetObjects(System::Windows::Forms::DataGridView^ theDataGridView, const Duco::ObjectId& newObjectId, Duco::TObjectType newObjectType, System::String^ secondaryTitle, System::Int16 startingDataColumn, System::Int16 endingDataColumn);
        System::Void SetObjects(System::Windows::Forms::DataGridView^ theDataGridView, bool ignoreTheFirstColumn);
        System::Void SetSecondaryTitle(System::String^ newSecondaryTitle);

        virtual System::Void printObject(System::Drawing::Printing::PrintPageEventArgs^ args, System::Drawing::Printing::PrinterSettings^ printSettings) override;

    protected:
        virtual System::Void GenerateColumnWidths(std::vector<float>& columnWidths, System::Drawing::Printing::PrintPageEventArgs^ printArgs, System::Windows::Forms::DataGridView^ dataGridView) override;

        System::Windows::Forms::DataGridView^   dataGridView;
        Duco::ObjectId*                         objectId;
        Duco::TObjectType                       objectType;
        System::String^                         title;
        System::String^                         secondaryTitle;
        System::Int16                           pageNumber;
        System::Int16                           startDataColumn;
        System::Int16                           endDataColumn;
        System::Boolean                         ignoreFirstColumn;
    };
}
