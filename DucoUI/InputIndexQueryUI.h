#pragma once

namespace DucoUI
{
    public ref class InputIndexQueryUI : public System::Windows::Forms::Form
    {
    public:
        InputIndexQueryUI(System::String^ promptStr, System::Int16& newId);

    protected:
        ~InputIndexQueryUI();

        System::Void okBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void cancelBtn_Click(System::Object^  sender, System::EventArgs^  e);
        void KeyDown( Object^ sender, System::Windows::Forms::KeyEventArgs^ e );
        void KeyPressed(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e);
        System::Void InputIndexQueryUI_Load(System::Object^  sender, System::EventArgs^  e);

    protected:
        System::Int16&      id;
        System::Boolean     nonNumberEntered;

    private:
        System::Windows::Forms::TextBox^  idEditor;
        System::ComponentModel::Container^  components;

        void InitializeComponent()
        {
            System::Windows::Forms::Button^  okBtn;
            System::Windows::Forms::Button^  cancelBtn;
            System::Windows::Forms::Label^  typeLabel;
            System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
            this->idEditor = (gcnew System::Windows::Forms::TextBox());
            okBtn = (gcnew System::Windows::Forms::Button());
            cancelBtn = (gcnew System::Windows::Forms::Button());
            typeLabel = (gcnew System::Windows::Forms::Label());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            tableLayoutPanel1->SuspendLayout();
            this->SuspendLayout();
            // 
            // okBtn
            // 
            okBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
            okBtn->Location = System::Drawing::Point(50, 33);
            okBtn->Name = L"okBtn";
            okBtn->Size = System::Drawing::Size(75, 23);
            okBtn->TabIndex = 2;
            okBtn->Text = L"OK";
            okBtn->UseVisualStyleBackColor = true;
            okBtn->Click += gcnew System::EventHandler(this, &InputIndexQueryUI::okBtn_Click);
            // 
            // cancelBtn
            // 
            cancelBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
            cancelBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            cancelBtn->Location = System::Drawing::Point(131, 33);
            cancelBtn->Name = L"cancelBtn";
            cancelBtn->Size = System::Drawing::Size(75, 23);
            cancelBtn->TabIndex = 3;
            cancelBtn->Text = L"Cancel";
            cancelBtn->UseVisualStyleBackColor = true;
            cancelBtn->Click += gcnew System::EventHandler(this, &InputIndexQueryUI::cancelBtn_Click);
            // 
            // typeLabel
            // 
            typeLabel->Anchor = System::Windows::Forms::AnchorStyles::Right;
            typeLabel->AutoSize = true;
            typeLabel->Location = System::Drawing::Point(3, 8);
            typeLabel->Name = L"typeLabel";
            typeLabel->Size = System::Drawing::Size(41, 13);
            typeLabel->TabIndex = 4;
            typeLabel->Text = L"Find id:";
            typeLabel->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
            // 
            // idEditor
            // 
            tableLayoutPanel1->SetColumnSpan(this->idEditor, 2);
            this->idEditor->Dock = System::Windows::Forms::DockStyle::Fill;
            this->idEditor->Location = System::Drawing::Point(53, 6);
            this->idEditor->Margin = System::Windows::Forms::Padding(6, 6, 3, 3);
            this->idEditor->Name = L"idEditor";
            this->idEditor->Size = System::Drawing::Size(153, 20);
            this->idEditor->TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 3;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->Controls->Add(okBtn, 1, 1);
            tableLayoutPanel1->Controls->Add(this->idEditor, 1, 0);
            tableLayoutPanel1->Controls->Add(typeLabel, 0, 0);
            tableLayoutPanel1->Controls->Add(cancelBtn, 2, 1);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 2;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->Size = System::Drawing::Size(209, 59);
            tableLayoutPanel1->TabIndex = 5;
            // 
            // InputIndexQueryUI
            // 
            this->AcceptButton = okBtn;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->AutoSize = true;
            this->CancelButton = cancelBtn;
            this->ClientSize = System::Drawing::Size(209, 59);
            this->Controls->Add(tableLayoutPanel1);
            this->MaximizeBox = false;
            this->MaximumSize = System::Drawing::Size(225, 98);
            this->MinimizeBox = false;
            this->MinimumSize = System::Drawing::Size(225, 98);
            this->Name = L"InputIndexQueryUI";
            this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
            this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
            this->TopMost = true;
            this->Load += gcnew System::EventHandler(this, &InputIndexQueryUI::InputIndexQueryUI_Load);
            tableLayoutPanel1->ResumeLayout(false);
            tableLayoutPanel1->PerformLayout();
            this->ResumeLayout(false);

        }
    };
}
