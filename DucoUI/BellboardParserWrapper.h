#pragma once

#include <BellboardPerformanceParserCallback.h>
#include <vcclr.h>
#include <ObjectId.h>

namespace DucoUI
{
interface class BellboardParserUIWrapper
{
public:
    virtual void Cancelled(System::String^ bellboardId, System::String^ message);
    virtual System::Void Completed(System::String^ bellboardId, System::Boolean errors) = 0;
};

class BellboardParserWrapper : public Duco::BellboardPerformanceParserCallback
{
public:
    BellboardParserWrapper(DucoUI::BellboardParserUIWrapper^ theCallback);

    virtual void Cancelled(const std::wstring& bellboardId, const char* message);
    virtual System::Void Completed(const std::wstring& bellboardId, bool errors);

protected:
	gcroot<BellboardParserUIWrapper^> uiCallback;
};



interface class BellboardPerformanceParserConfirmRingerCallbackUIWrapper
{
public:
    virtual Duco::ObjectId ChooseRinger(System::String^ missingRingerName, const std::set<Duco::RingerPealDates>& nearMatches) = 0;
};

class BellboardRingerParserWrapper : public Duco::BellboardPerformanceParserConfirmRingerCallback
{
public:
    BellboardRingerParserWrapper(DucoUI::BellboardPerformanceParserConfirmRingerCallbackUIWrapper^ theCallback);

    virtual Duco::ObjectId ChooseRinger(const std::wstring& missingRingerName, const std::set<Duco::RingerPealDates>& nearMatches);

protected:
    gcroot<BellboardPerformanceParserConfirmRingerCallbackUIWrapper^> uiCallback;
};

} // end namespace DucoUI

