#pragma once
#include "EventType.h"
#include <ObjectType.h>
#include <ObjectId.h>

namespace DucoUI
{
    public interface class RingingDatabaseObserver
    {
    public:
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2) = 0;
    };

} // end namespace Duco
