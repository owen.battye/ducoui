#include "DatabaseManager.h"
#include "SystemDefaultIcon.h"
#include <Peal.h>
#include <Tower.h>
#include <Method.h>
#include <Ring.h>
#include <Ringer.h>
#include <DucoEngineUtils.h>

#include "UploadPealUI.h"

#include "DucoUtils.h"
#include "SoundUtils.h"
#include <RingingDatabase.h>
#include <DatabaseSettings.h>
#include "DucoUILog.h"
#include <PealDatabase.h>

using namespace DucoUI;
using namespace Duco;
using namespace System::Threading::Tasks;

UploadPealUI::UploadPealUI(DucoUI::DatabaseManager^ theDatabase, const Duco::Peal& thePeal)
: database(theDatabase), firstActivation(true), submitted(false), rwpagesubmitted(false)
{
    currentPeal = new Duco::Peal(thePeal);
    InitializeComponent();
    InitializeCEFSharp();
}

UploadPealUI::!UploadPealUI()
{
    delete currentPeal;
}

UploadPealUI::~UploadPealUI()
{
    this->!UploadPealUI();
    if (components)
    {
        delete components;
    }
}

System::Void
UploadPealUI::UploadPealUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    this->Icon = SystemDefaultIcon::DefaultSystemIcon();
    this->chromeBrowser->LoadingStateChanged += gcnew System::EventHandler<CefSharp::LoadingStateChangedEventArgs ^>(BrowserLoadingStateChanged);
}

System::Void
UploadPealUI::UploadPealUI_Activated(System::Object^  sender, System::EventArgs^  e)
{
    if (firstActivation)
    {
        GoToWebSite();
    }
    firstActivation = false;
}

System::Void
UploadPealUI::populatePerformanceToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    SetUploadParameters();
}

System::Void
UploadPealUI::InitializeCEFSharp()
{
    this->chromeBrowser = gcnew CefSharp::WinForms::ChromiumWebBrowser();
    this->Controls->Add(this->chromeBrowser);
    // 
    // chromeBrowser
    // 
    this->chromeBrowser->Dock = System::Windows::Forms::DockStyle::Fill;
    this->chromeBrowser->Location = System::Drawing::Point(3, 50);
    this->chromeBrowser->Margin = System::Windows::Forms::Padding(3, 0, 3, 0);
    this->chromeBrowser->Name = L"chromeBrowser";
    this->chromeBrowser->TabIndex = 5;
}

System::Void
UploadPealUI::GoToWebSite()
{
    try
    {
        String^ downloadString = DucoUtils::ConvertString(database->Settings().PreferedDownloadWebsiteUrl());
        chromeBrowser->Load(downloadString);
    }
    catch (Exception^ ex)
    {
        SoundUtils::PlayErrorSound();
        MessageBox::Show(this, "Please check the peal download URL in settings\n" + ex->ToString(), "Bad download URL", MessageBoxButtons::OK, MessageBoxIcon::Warning);
    }
}

System::Void
UploadPealUI::GoToSubmit()
{
    String^ javaScript = "var elements = document.getElementsByTagName('a'); for (var i = 0; i < elements.length; i++) { if (elements[i].innerText.includes('ubmit')) { elements[i].click(); } }";
    CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, javaScript);
}

System::Void
UploadPealUI::BrowserLoadingStateChanged(System::Object^ object, CefSharp::LoadingStateChangedEventArgs ^  e)
{
    if (!e->IsLoading)
    {
        CefSharp::WinForms::ChromiumWebBrowser^ browser = static_cast<CefSharp::WinForms::ChromiumWebBrowser^>(object);
        DUCOUILOGDEBUG("BrowserLoadingStateChanged: " + browser->Address);
        UploadPealUI^ parentForm = static_cast<UploadPealUI^>(browser->FindForm());
        if (browser->Address->EndsWith(".co.uk/"))
        {
            if (parentForm->submitted)
            {
                parentForm->submitted = false;
                parentForm->rwpagesubmitted = false;
            }
            else
            {
                parentForm->GoToSubmit();
            }
        }
        else if (browser->Address->Contains("submit.php") && !parentForm->submitted)
        {
            parentForm->SetUploadParameters();
        }
        else if (browser->Address->Contains("submit.php") && parentForm->submitted)
        {
            parentForm->RequestView();
        }
        else if (browser->Address->Contains("/view.php?id="))
        {
            if (parentForm->submitted && !parentForm->rwpagesubmitted)
            {
                parentForm->SavePealId();
            }
        }
        else if (browser->Address->Contains("send-to-rw") && !parentForm->rwpagesubmitted)
        {
            if (parentForm->submitted)
            {
                parentForm->SetRWParameters(browser->Address);
            }
        }
        else if (browser->Address->Contains("send-to-rw.php") && parentForm->rwpagesubmitted)
        {
            parentForm->RequestView();
        }
        else
        {
            parentForm->submitted = false;
            parentForm->rwpagesubmitted = false;
        }
    }
}

System::Void
UploadPealUI::SetUploadParameters()
{
    String^ javaScript = "";

    if (currentPeal->AssociationId().ValidId())
    {
        javaScript = "var x = document.getElementById('association'); x.value ='";
        javaScript += DucoUtils::EncodeString(currentPeal->AssociationName(database->Database()));
        javaScript += "';"; // x.click()";
        CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, javaScript);
    }

    javaScript = "var x = document.getElementById('date_rung'); x.value ='";
    javaScript += DucoUtils::EncodeString(currentPeal->Date().Str());
    javaScript += "';";
    CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, javaScript);

    Duco::Tower theTower;
    if (database->FindTower(currentPeal->TowerId(), theTower, false))
    {
        javaScript = "var x = document.getElementById('place'); x.value ='";
        javaScript += DucoUtils::EncodeString(theTower.City());
        javaScript += "';";
        CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, javaScript);

        javaScript = "var x = document.getElementById('region'); x.value ='";
        javaScript += DucoUtils::EncodeString(theTower.County());
        javaScript += "';";
        CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, javaScript);

        javaScript = "var x = document.getElementById('address'); x.value ='";
        javaScript += DucoUtils::EncodeString(theTower.Name());
        javaScript += "';";
        CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, javaScript);

        const Duco::Ring* theRing = theTower.FindRing(currentPeal->RingId());
        if (theRing != NULL)
        {
            javaScript = "var x = document.getElementById('tenor_size'); x.value ='";
            javaScript += DucoUtils::EncodeString(theRing->TenorWeight());
            javaScript += "';";
            CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, javaScript);
        }
    }

    javaScript = "var x = document.getElementById('changes'); x.value ='";
    javaScript += DucoUtils::ConvertString(DucoEngineUtils::ToString(currentPeal->NoOfChanges(), true));
    javaScript += "';";
    CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, javaScript);

    Duco::Method theMethod;
    if (database->FindMethod(currentPeal->MethodId(), theMethod, false))
    {
        javaScript = "var x = document.getElementById('title'); x.value = '";
        javaScript += DucoUtils::EncodeString(theMethod.FullName(database->Database()));
        javaScript += "';";
        CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, javaScript);
    }

    if (currentPeal->MultiMethods().length() > 0)
    {
        javaScript = "var x = document.getElementById('details'); x.value = '";
        javaScript += DucoUtils::EncodeString(currentPeal->MultiMethods());
        javaScript += "';";
        CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, javaScript);
    }

    javaScript = "var x = document.getElementById('duration'); x.value ='";
    javaScript += DucoUtils::ConvertString(currentPeal->Time().PrintShortPealTime());
    javaScript += "';";
    CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, javaScript);

    if (currentPeal->Composer().length() > 0)
    {
        javaScript = "var x = document.getElementById('composer'); x.value ='";
        javaScript += DucoUtils::EncodeString(currentPeal->Composer());
        javaScript += "';";
        CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, javaScript);
    }

    bool databaseHasLastNameFirst = false;
    if (database->Database().Settings().LastNameFirst())
    {
        databaseHasLastNameFirst = true;
        database->Database().Settings().SetLastNameFirst(false);
    }

    //Ringers
    for (unsigned int bellNo = 1; bellNo <= currentPeal->NoOfBellsRung(database->Database()) && bellNo <= 16; ++bellNo)
    {
        javaScript = "var x = document.getElementById('ringers." + bellNo + "'); x.value ='";
        bool ringerFound = false;

        std::wstring ringerName = currentPeal->RingerName(bellNo, false, database->Database());
        if (ringerName.length() > 0)
        {
            javaScript += DucoUtils::EncodeString(ringerName);
            ringerFound = true;
            if (currentPeal->ConductorOnBell(bellNo, false))
            {
                javaScript += " (C)";
            }
        }
        std::wstring strapperName = currentPeal->RingerName(bellNo, true, database->Database());
        if (strapperName.length() > 0)
        {
            javaScript += " & " + DucoUtils::EncodeString(strapperName);
            ringerFound = true;
            if (currentPeal->ConductorOnBell(bellNo, true))
            {
                javaScript += " (C)";
            }
        }
        javaScript += "';";
        if (ringerFound)
        {
            CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, javaScript);
        }
    }

    if (databaseHasLastNameFirst)
    {
        database->Database().Settings().SetLastNameFirst(true);
    }

    if (currentPeal->Footnotes().length() > 0 || currentPeal->RingingWorldReference().length() > 0 || currentPeal->Notes().length() > 0)
    {
        bool addFootnotes = false;
        javaScript = "var x = document.getElementById('footnotes'); x.value = '";
        if (currentPeal->Footnotes().length() > 0)
        {
            javaScript += DucoUtils::EncodeString(currentPeal->Footnotes());
            addFootnotes = true;
        }
        if (addFootnotes)
        {
            javaScript += "\\r\\n";
        }
        if (currentPeal->RingingWorldReference().length() > 0)
        {
            addFootnotes = true;
            javaScript += "Ringing world: ";
            javaScript += DucoUtils::EncodeString(currentPeal->RingingWorldReference());
        }
        else
        {
            std::wstring anotherReference = currentPeal->FindReference();
            if (anotherReference.length() > 0)
            {
                if (addFootnotes)
                {
                    javaScript += "\\r\\n";
                }
                addFootnotes = true;
                javaScript += DucoUtils::EncodeString(currentPeal->FindReference());
            }
        }
        javaScript += "';";
        if (addFootnotes)
        {
            CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, javaScript);
        }
    }
    statusLabel->Text = "Check details are correct before clicking submit";
    submitted = true;
}

System::Void
UploadPealUI::SetRWParameters(System::String^ address)
{
    ObjectId idOfPealWithNewBBId;
    int startOfId = address->IndexOf("?id=");
    if (startOfId != -1)
    {
        int endOfId = address->IndexOf("&rwissue", startOfId);
        if (endOfId != -1)
        {
            String^ idOnPage = address->Substring(startOfId+4, endOfId-startOfId-4);
            std::wstring nativeId;
            DucoUtils::ConvertString(idOnPage, nativeId);
            idOfPealWithNewBBId = database->Database().PealsDatabase().FindPealByBellboardId(nativeId);
            if (nativeId.compare(currentPeal->BellBoardId()) != 0)
            {
                if (idOfPealWithNewBBId.ValidId() && idOfPealWithNewBBId != currentPeal->Id())
                {
                    *currentPeal = *database->Database().PealsDatabase().FindPeal(idOfPealWithNewBBId);
                }
            }
        }
    }
    if (idOfPealWithNewBBId == currentPeal->Id() && currentPeal->ValidRingingWorldReferenceForUpload())
    {
        String^ javaScript = "var x = document.getElementById('page'); x.value = '";
        javaScript += DucoUtils::EncodeString(currentPeal->RingingWorldPage());
        javaScript += "';";
        CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, javaScript);
        javaScript = "var x = document.getElementById('edition'); x.value = '";
        javaScript += DucoUtils::EncodeString(currentPeal->RingingWorldIssue());
        javaScript += "';";
        CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, javaScript);
        javaScript = "document.getElementById('freeze').checked = true;";
        CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, javaScript);
        rwpagesubmitted = true;
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
UploadPealUI::RequestView()
{
    String^ javaScript = "var element = document.querySelector('#page-content a[href*=\"view\"]'); element.click()";
    CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, javaScript);

}

System::Void
UploadPealUI::SavePealId()
{
    bool startEdit = false;
    try
    {
        String^ viewUrl = "/view.php?id=";
        Int32 index = chromeBrowser->Address->IndexOf(viewUrl);

        String^ pealId = chromeBrowser->Address->Substring(index + viewUrl->Length);
        DUCOUILOGDEBUG("UploadPealUI SavePealId: " + pealId);

        std::wstring pealIdStr;
        DucoUtils::ConvertString(pealId, pealIdStr);

        if (database->StartEdit(Duco::TObjectType::EPeal, currentPeal->Id()))
        {
            startEdit = true;
            currentPeal->SetBellBoardId(pealIdStr);
            if (database->UpdatePeal(*currentPeal, false)) // Cant update observers - we have no control of the thread we're on
            {
                statusLabel->Text = "Saved " + pealId + " to performance " + Convert::ToString(currentPeal->Id().Id());
                startEdit = false;

                if (currentPeal->ValidRingingWorldReferenceForUpload() && database->Settings().BellBoardAutoSetRingingWorldPage())
                {
                    String^ javaScript = "var element = document.querySelector('#page-content a[href*=\"rwissue=true\"]'); element.click()";
                    CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, javaScript);
                }
                else
                {
                    submitted = false;
                }
            }
            else
            {
                statusLabel->Text = "Unable to save bellboard id for performance " + Convert::ToString(currentPeal->Id().Id());
            }
        }
        else
        {
            statusLabel->Text = "Could not save bellboard id for performance " + Convert::ToString(currentPeal->Id().Id());
        }
    }
    catch (Exception^ ex)
    {
        DucoUILog::PrintError("UploadPealUI SavePealId Exception: " + ex->ToString());
        statusLabel->Text = "";
    }
    if (startEdit)
    {
        database->CancelEdit(Duco::TObjectType::EPeal, currentPeal->Id());
    }
}
