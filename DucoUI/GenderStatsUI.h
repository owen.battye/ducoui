#pragma once
namespace DucoUI
{
    public ref class GenderStatsUI : public System::Windows::Forms::Form
    {
    public:
        GenderStatsUI(DucoUI::DatabaseManager^ theDatabase);

    protected:
        !GenderStatsUI();
        ~GenderStatsUI();
        System::Void GenderStatsUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void filtersBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void Generate();

    private:
        Duco::StatisticFilters* filters;
        System::ComponentModel::Container ^components;
        DucoUI::DatabaseManager^ database;
        System::Windows::Forms::DataVisualization::Charting::Chart^  totalRingers;
        System::Windows::Forms::DataVisualization::Charting::Chart^  pealRingers;
        System::Windows::Forms::DataVisualization::Charting::Chart^  pealConductors;

#pragma region Windows Form Designer generated code
        void InitializeComponent(void)
        {
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
            System::Windows::Forms::DataVisualization::Charting::ChartArea^ chartArea1 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
            System::Windows::Forms::DataVisualization::Charting::Legend^ legend1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Legend());
            System::Windows::Forms::DataVisualization::Charting::Series^ series1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
            System::Windows::Forms::DataVisualization::Charting::Series^ series2 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
            System::Windows::Forms::DataVisualization::Charting::Series^ series3 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
            System::Windows::Forms::DataVisualization::Charting::Series^ series4 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
            System::Windows::Forms::DataVisualization::Charting::ChartArea^ chartArea2 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
            System::Windows::Forms::DataVisualization::Charting::Legend^ legend2 = (gcnew System::Windows::Forms::DataVisualization::Charting::Legend());
            System::Windows::Forms::DataVisualization::Charting::Series^ series5 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
            System::Windows::Forms::DataVisualization::Charting::Series^ series6 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
            System::Windows::Forms::DataVisualization::Charting::Series^ series7 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
            System::Windows::Forms::DataVisualization::Charting::Series^ series8 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
            System::Windows::Forms::DataVisualization::Charting::ChartArea^ chartArea3 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
            System::Windows::Forms::DataVisualization::Charting::Legend^ legend3 = (gcnew System::Windows::Forms::DataVisualization::Charting::Legend());
            System::Windows::Forms::DataVisualization::Charting::Series^ series9 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
            System::Windows::Forms::DataVisualization::Charting::Series^ series10 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
            System::Windows::Forms::DataVisualization::Charting::Series^ series11 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
            System::Windows::Forms::DataVisualization::Charting::Series^ series12 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
            System::Windows::Forms::ToolStrip^ toolStrip1;
            System::Windows::Forms::ToolStripButton^ filterBtn;
            System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(GenderStatsUI::typeid));
            this->pealConductors = (gcnew System::Windows::Forms::DataVisualization::Charting::Chart());
            this->pealRingers = (gcnew System::Windows::Forms::DataVisualization::Charting::Chart());
            this->totalRingers = (gcnew System::Windows::Forms::DataVisualization::Charting::Chart());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
            filterBtn = (gcnew System::Windows::Forms::ToolStripButton());
            tableLayoutPanel1->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pealConductors))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pealRingers))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->totalRingers))->BeginInit();
            toolStrip1->SuspendLayout();
            this->SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->AutoSize = true;
            tableLayoutPanel1->ColumnCount = 1;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->Controls->Add(this->pealConductors, 0, 3);
            tableLayoutPanel1->Controls->Add(this->pealRingers, 0, 2);
            tableLayoutPanel1->Controls->Add(this->totalRingers, 0, 1);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->GrowStyle = System::Windows::Forms::TableLayoutPanelGrowStyle::FixedSize;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 4;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 28)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 33.33F)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 33.33F)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 33.34F)));
            tableLayoutPanel1->Size = System::Drawing::Size(520, 336);
            tableLayoutPanel1->TabIndex = 5;
            // 
            // pealConductors
            // 
            chartArea1->AxisX->Enabled = System::Windows::Forms::DataVisualization::Charting::AxisEnabled::False;
            chartArea1->AxisX->IntervalType = System::Windows::Forms::DataVisualization::Charting::DateTimeIntervalType::Number;
            chartArea1->AxisX->IsStartedFromZero = false;
            chartArea1->AxisX->Maximum = 1;
            chartArea1->AxisX->Minimum = 1;
            chartArea1->AxisX2->Enabled = System::Windows::Forms::DataVisualization::Charting::AxisEnabled::False;
            chartArea1->AxisY->IsStartedFromZero = false;
            chartArea1->AxisY->Title = L"Percent of total conductors in peals";
            chartArea1->AxisY2->Enabled = System::Windows::Forms::DataVisualization::Charting::AxisEnabled::False;
            chartArea1->Name = L"TotalRingersChartArea";
            this->pealConductors->ChartAreas->Add(chartArea1);
            this->pealConductors->Dock = System::Windows::Forms::DockStyle::Fill;
            legend1->Name = L"totalConductorsLegend";
            legend1->Title = L"Total peal conductors";
            this->pealConductors->Legends->Add(legend1);
            this->pealConductors->Location = System::Drawing::Point(3, 235);
            this->pealConductors->Name = L"pealConductors";
            this->pealConductors->Palette = System::Windows::Forms::DataVisualization::Charting::ChartColorPalette::EarthTones;
            series1->ChartArea = L"TotalRingersChartArea";
            series1->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::StackedBar100;
            series1->Legend = L"totalConductorsLegend";
            series1->Name = L"Male";
            series1->ToolTip = L"Total male conductors as a percentage of people in peals";
            series1->XValueType = System::Windows::Forms::DataVisualization::Charting::ChartValueType::Single;
            series2->ChartArea = L"TotalRingersChartArea";
            series2->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::StackedBar100;
            series2->Legend = L"totalConductorsLegend";
            series2->Name = L"Female";
            series2->ToolTip = L"Total female conductors as a percentage of people in peals";
            series3->ChartArea = L"TotalRingersChartArea";
            series3->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::StackedBar100;
            series3->Legend = L"totalConductorsLegend";
            series3->Name = L"Not set";
            series4->ChartArea = L"TotalRingersChartArea";
            series4->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::StackedBar100;
            series4->Legend = L"totalConductorsLegend";
            series4->Name = L"Non human";
            this->pealConductors->Series->Add(series1);
            this->pealConductors->Series->Add(series2);
            this->pealConductors->Series->Add(series3);
            this->pealConductors->Series->Add(series4);
            this->pealConductors->Size = System::Drawing::Size(514, 98);
            this->pealConductors->TabIndex = 3;
            this->pealConductors->Text = L"Total ringers";
            // 
            // pealRingers
            // 
            chartArea2->AxisX->Enabled = System::Windows::Forms::DataVisualization::Charting::AxisEnabled::False;
            chartArea2->AxisX->IntervalType = System::Windows::Forms::DataVisualization::Charting::DateTimeIntervalType::Number;
            chartArea2->AxisX->IsStartedFromZero = false;
            chartArea2->AxisX->Maximum = 1;
            chartArea2->AxisX->Minimum = 1;
            chartArea2->AxisX2->Enabled = System::Windows::Forms::DataVisualization::Charting::AxisEnabled::False;
            chartArea2->AxisY->IsStartedFromZero = false;
            chartArea2->AxisY->Title = L"Percent of total ringers in peals";
            chartArea2->AxisY2->Enabled = System::Windows::Forms::DataVisualization::Charting::AxisEnabled::False;
            chartArea2->Name = L"TotalRingersChartArea";
            this->pealRingers->ChartAreas->Add(chartArea2);
            this->pealRingers->Dock = System::Windows::Forms::DockStyle::Fill;
            legend2->Name = L"totalRingersLegend";
            legend2->Title = L"Total peal ringers";
            this->pealRingers->Legends->Add(legend2);
            this->pealRingers->Location = System::Drawing::Point(3, 133);
            this->pealRingers->Name = L"pealRingers";
            this->pealRingers->Palette = System::Windows::Forms::DataVisualization::Charting::ChartColorPalette::EarthTones;
            series5->ChartArea = L"TotalRingersChartArea";
            series5->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::StackedBar100;
            series5->Legend = L"totalRingersLegend";
            series5->Name = L"Male";
            series5->ToolTip = L"Total male ringers as a percentage of people in peals";
            series5->XValueType = System::Windows::Forms::DataVisualization::Charting::ChartValueType::Single;
            series6->ChartArea = L"TotalRingersChartArea";
            series6->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::StackedBar100;
            series6->Legend = L"totalRingersLegend";
            series6->Name = L"Female";
            series6->ToolTip = L"Total female ringers as a percentage of people in peals";
            series7->ChartArea = L"TotalRingersChartArea";
            series7->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::StackedBar100;
            series7->Legend = L"totalRingersLegend";
            series7->Name = L"Not set";
            series8->ChartArea = L"TotalRingersChartArea";
            series8->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::StackedBar100;
            series8->Legend = L"totalRingersLegend";
            series8->Name = L"Non human";
            this->pealRingers->Series->Add(series5);
            this->pealRingers->Series->Add(series6);
            this->pealRingers->Series->Add(series7);
            this->pealRingers->Series->Add(series8);
            this->pealRingers->Size = System::Drawing::Size(514, 96);
            this->pealRingers->TabIndex = 2;
            this->pealRingers->Text = L"Total ringers";
            // 
            // totalRingers
            // 
            this->totalRingers->BackImageTransparentColor = System::Drawing::Color::White;
            chartArea3->AxisX->Enabled = System::Windows::Forms::DataVisualization::Charting::AxisEnabled::False;
            chartArea3->AxisX->IntervalType = System::Windows::Forms::DataVisualization::Charting::DateTimeIntervalType::Number;
            chartArea3->AxisX->IsStartedFromZero = false;
            chartArea3->AxisX->Maximum = 1;
            chartArea3->AxisX->Minimum = 1;
            chartArea3->AxisX2->Enabled = System::Windows::Forms::DataVisualization::Charting::AxisEnabled::False;
            chartArea3->AxisY->IsStartedFromZero = false;
            chartArea3->AxisY->Title = L"Percent of total ringers";
            chartArea3->AxisY2->Enabled = System::Windows::Forms::DataVisualization::Charting::AxisEnabled::False;
            chartArea3->Name = L"TotalRingersChartArea";
            this->totalRingers->ChartAreas->Add(chartArea3);
            this->totalRingers->Dock = System::Windows::Forms::DockStyle::Fill;
            legend3->Name = L"totalRingersLegend";
            legend3->Title = L"Total ringers";
            this->totalRingers->Legends->Add(legend3);
            this->totalRingers->Location = System::Drawing::Point(3, 31);
            this->totalRingers->Name = L"totalRingers";
            this->totalRingers->Palette = System::Windows::Forms::DataVisualization::Charting::ChartColorPalette::EarthTones;
            series9->ChartArea = L"TotalRingersChartArea";
            series9->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::StackedBar100;
            series9->Legend = L"totalRingersLegend";
            series9->Name = L"Male";
            series9->ToolTip = L"Total male ringers";
            series9->XValueType = System::Windows::Forms::DataVisualization::Charting::ChartValueType::Single;
            series10->ChartArea = L"TotalRingersChartArea";
            series10->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::StackedBar100;
            series10->Legend = L"totalRingersLegend";
            series10->Name = L"Female";
            series10->ToolTip = L"Total female ringers";
            series11->ChartArea = L"TotalRingersChartArea";
            series11->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::StackedBar100;
            series11->Legend = L"totalRingersLegend";
            series11->Name = L"Not set";
            series11->ToolTip = L"Total ringers of unknown or questionable gender";
            series12->ChartArea = L"TotalRingersChartArea";
            series12->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::StackedBar100;
            series12->Legend = L"totalRingersLegend";
            series12->Name = L"Non human";
            this->totalRingers->Series->Add(series9);
            this->totalRingers->Series->Add(series10);
            this->totalRingers->Series->Add(series11);
            this->totalRingers->Series->Add(series12);
            this->totalRingers->Size = System::Drawing::Size(514, 96);
            this->totalRingers->TabIndex = 1;
            this->totalRingers->Text = L"Total ringers";
            // 
            // toolStrip1
            // 
            toolStrip1->GripStyle = System::Windows::Forms::ToolStripGripStyle::Hidden;
            toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { filterBtn });
            toolStrip1->Location = System::Drawing::Point(0, 0);
            toolStrip1->Name = L"toolStrip1";
            toolStrip1->Padding = System::Windows::Forms::Padding(0);
            toolStrip1->Size = System::Drawing::Size(520, 25);
            toolStrip1->TabIndex = 6;
            toolStrip1->Text = L"toolStrip1";
            // 
            // filterBtn
            // 
            filterBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
            filterBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"filterBtn.Image")));
            filterBtn->ImageTransparentColor = System::Drawing::Color::Magenta;
            filterBtn->Name = L"filterBtn";
            filterBtn->Size = System::Drawing::Size(37, 22);
            filterBtn->Text = L"&Filter";
            filterBtn->Click += gcnew System::EventHandler(this, &GenderStatsUI::filtersBtn_Click);
            // 
            // GenderStatsUI
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            this->ClientSize = System::Drawing::Size(520, 336);
            this->Controls->Add(toolStrip1);
            this->Controls->Add(tableLayoutPanel1);
            this->Name = L"GenderStatsUI";
            this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
            this->Text = L"Gender statistics";
            this->Load += gcnew System::EventHandler(this, &GenderStatsUI::GenderStatsUI_Load);
            tableLayoutPanel1->ResumeLayout(false);
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pealConductors))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pealRingers))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->totalRingers))->EndInit();
            toolStrip1->ResumeLayout(false);
            toolStrip1->PerformLayout();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion
    };
}
