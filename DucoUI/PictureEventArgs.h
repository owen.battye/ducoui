#pragma once

namespace DucoUI
{
    /// <summary>
    /// Summary for IOpenPealCallback
    /// </summary>
    public ref class PictureEventArgs : public System::EventArgs
    {
    public: PictureEventArgs()
    {
        pictureId = NULL;
    }
    public: !PictureEventArgs()
    {
        delete pictureId;
    }
    public: ~PictureEventArgs()
    {
        this->!PictureEventArgs();
    }

    public: Duco::ObjectId*  pictureId;
    };
}