#pragma once

#include "ImportProgress.h"
#include <PerformanceDate.h>
#include <ObjectId.h>
#include <map>

const wchar_t fieldSeperator = ',';
const wchar_t altFieldSeperator = ' ';

namespace DucoUI
{
    ref class DatabaseManager;

ref class DatabaseImportUtil abstract
{
public:
    DatabaseImportUtil(DucoUI::DatabaseManager^ theDatabase, DucoUI::WinRkImportProgressCallback^ newCallback, System::String^ theFileName);
    virtual ~DatabaseImportUtil();

    virtual void StartImport();
    void Cancel();
    virtual System::String^ FileType() = 0;
    virtual double TotalNoOfStages() = 0;
    virtual double StageNumber(DucoUI::WinRkImportProgressCallback::TImportStage currentStage) = 0;

    bool ImportInProgress();

protected:
    virtual void Import(System::ComponentModel::BackgroundWorker^ worker, System::ComponentModel::DoWorkEventArgs^  e) = 0;
    void ImportSingleStringFromTable(System::Data::Odbc::OdbcCommand^ command,
                                     System::String^ tableName,
                                     std::map<unsigned int,std::wstring>& strings,
                                     DucoUI::WinRkImportProgressCallback::TImportStage stage,
                                     System::ComponentModel::BackgroundWorker^ worker);

    void ReadString(System::Data::Odbc::OdbcDataReader^ reader, unsigned int fieldId, std::wstring& str);
    System::String^ ReadString(System::Data::Odbc::OdbcDataReader^ reader, unsigned int fieldId);
    Duco::PerformanceDate ReadDateFromV2(System::Data::Odbc::OdbcDataReader^ reader, unsigned int fieldId);
    Duco::PerformanceDate ReadDateFromV3(System::Data::Odbc::OdbcDataReader^ reader, unsigned int fieldId);
    Duco::PerformanceTime ReadTime(System::Data::Odbc::OdbcDataReader^ reader, unsigned int fieldId);

    System::Void ImportFile(System::Object^ sender, System::ComponentModel::DoWorkEventArgs^ e);
    System::Void ImportCompleted(System::Object^ sender, System::ComponentModel::RunWorkerCompletedEventArgs^ e);
    System::Void ImportProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
    System::Void ReportProgress(DucoUI::WinRkImportProgressCallback::TImportStage stage, int total, int recordNo);
    Duco::ObjectId CheckRingerAkaIds(Duco::ObjectId idToCheck, const std::map<Duco::ObjectId, Duco::ObjectId>& ringerAkas);

protected:
    // member data
    DucoUI::DatabaseManager^                    database;
    System::String^                             fileName;
    System::ComponentModel::BackgroundWorker^   importTool;
    System::Data::Odbc::OdbcConnection^         connection;
    DucoUI::WinRkImportProgressCallback^        callback;
};

ref class ImportProgressArgs
{
    public:
        DucoUI::WinRkImportProgressCallback::TImportStage stage;
};

}