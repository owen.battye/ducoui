#include "NormalMethodLineDrawer.h"

#include <Method.h>
#include <DucoEngineUtils.h>
#include <Lead.h>
#include "DatabaseManager.h"
#include "DucoUtils.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::Drawing;
using namespace System::Drawing::Drawing2D;

NormalMethodLineDrawer::NormalMethodLineDrawer(System::Drawing::Graphics^ lineDrwr, Duco::Method& theMethod, DucoUI::DatabaseManager^ theDatabase, System::ComponentModel::BackgroundWorker^ bgWorker)
: MethodLineDrawer(lineDrwr, theMethod, theDatabase, bgWorker)
{
}

NormalMethodLineDrawer::!NormalMethodLineDrawer()
{
}

NormalMethodLineDrawer::~NormalMethodLineDrawer()
{
    this->!NormalMethodLineDrawer();
}

void 
NormalMethodLineDrawer::DrawGridLines()
{
    float lineX (KPositionOffset + KPositionGap);
    float finalYPosition (float(singleLead->BlowsInLead()) * KBlowGap);
    for (int i(0); i < currentMethod.NoOfLeads(); ++i)
    {
        for (unsigned int j(1); j < currentMethod.Order(); j += 2)
        {
            lineDrawer->FillRectangle(linesDrawingBrush, lineX, KBlowOffset, KPositionGap, finalYPosition);
            lineX += 2*KPositionGap;
        }
        if (!DucoEngineUtils::IsEven(currentMethod.Order()))
        {
            lineX += KPositionGap;
        }
        lineX += KPositionOffset;
    }

}

bool  
NormalMethodLineDrawer::DrawLine()
{
    // init variables
    if (database->Settings().ShowFullGrid())
    {
        CreateGridPens();
    }
    float gridOffset (perLeadXOffsetValue*currentMethod.NoOfLeads());

    bool endedWithRounds (false);
    while (!endedWithRounds && !worker->CancellationPending)
    {
        DrawLead(lineDrawingPen);
        if (database->Settings().ShowFullGrid())
        {
            GraphicsPath^ gridPath = gcnew GraphicsPath;
            if (leadLine->Count > 1)
            {
                gridPath->AddLines(leadLine->ToArray());
                Matrix^ translateMatrix = gcnew Matrix;
                translateMatrix->Translate( gridOffset, 0 );
                gridPath->Transform(translateMatrix);
                gridOffset -= perLeadXOffsetValue;
                lineDrawer->DrawPath(gridDrawingPen, gridPath);
            }
        }
        xPosWrapOffset += perLeadXOffsetValue;
        endedWithRounds = singleLead->EndsWithRounds();
        CreateNextLead();
    }
    if (database->Settings().ShowFullGrid())
    {
        MoveTrebleLineAndDraw(gridTrebleDrawingPen);
    }

    return true;
}

System::Void 
NormalMethodLineDrawer::DrawLead(System::Drawing::Pen^% penToUse)
{
    bool generateTreble (treblePath->PointCount <= 0);
    System::Collections::Generic::List<System::Drawing::PointF>^ trebleLine = nullptr;
    if (generateTreble && ShowTreble())
    {
        trebleLine = gcnew System::Collections::Generic::List<System::Drawing::PointF>(singleLead->BlowsInLead()+1);
    }

    if (database->Settings().LineType() == DatabaseSettings::EAnnotated)
    {
        for (unsigned int i (0); i <= singleLead->BlowsInLead(); ++i)
        {
            PrintRow(i);
        }
    }

    leadLine->Clear();
    size_t changeNumber(0);
    unsigned int currentPosition (drawingBell);
    unsigned int currentTreblePosition (1);
    while (changeNumber <= singleLead->BlowsInLead() && !worker->CancellationPending)
    {
        if (singleLead->Position(unsigned int(changeNumber), drawingBell, currentPosition))
        {
            if (changeNumber == 0)
            {
                String^ leadStartText = DucoUtils::ConvertString(DucoEngineUtils::ToChar(currentPosition));
                SizeF leadStartStrSize = lineDrawer->MeasureString(leadStartText, leadStartFont);
                lineDrawer->DrawString(leadStartText, leadStartFont, leadStartBrush, xPosWrapOffset + KPositionOffset + KPositionGap - leadStartStrSize.Width, 0);
            }

            float nextXPos (KPositionOffset + (float(currentPosition)*KPositionGap) + xPosWrapOffset);
            float nextYPos (KBlowOffset + (float(changeNumber)*KBlowGap));
            PointF nextPosition(nextXPos, nextYPos);
            leadLine->Add(nextPosition);

            if (generateTreble && ShowTreble() && singleLead->TreblePosition(changeNumber, currentTreblePosition))
            {
                float nextTrebleXPos = KPositionOffset + (float(currentTreblePosition)*KPositionGap) + xPosWrapOffset;
                PointF nextTreblePosition(nextTrebleXPos, nextYPos);
                trebleLine->Add(nextTreblePosition);
            }
        }
        else
        {
            changeNumber = singleLead->BlowsInLead();
        }
        ++changeNumber;
    }
    if (generateTreble && ShowTreble())
    {
        treblePath->AddLines(trebleLine->ToArray());
        delete trebleLine;
    }
    MoveTrebleLineAndDraw(trebleDrawingPen);
    if (leadLine->Count > 1)
        lineDrawer->DrawLines(penToUse, leadLine->ToArray());
}

System::Void
NormalMethodLineDrawer::MoveTrebleLineAndDraw(System::Drawing::Pen^% penToUse)
{
    if (ShowTreble())
    {
        if (leadNumber > 0)
        {
            Matrix^ translateMatrix = gcnew Matrix;
            translateMatrix->Translate( perLeadXOffsetValue, 0 );
            treblePath->Transform(translateMatrix);
        }
        lineDrawer->DrawPath(penToUse, treblePath);
    }
    ++leadNumber;
}
