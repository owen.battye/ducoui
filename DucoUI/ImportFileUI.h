#pragma once
namespace DucoUI
{
    public ref class ImportFileUI : public System::Windows::Forms::Form, public DucoUI::WinRkImportProgressCallback
    {
    public:
        ImportFileUI(DucoUI::DatabaseManager^ theDatabase, System::String^ newFileName);

        // from WinRkImportProgressCallback
        virtual void Initialised(DucoUI::WinRkImportProgressCallback::TImportStage newStage);
        virtual void Step(int percent);
        virtual void Complete(bool cancelled);

    protected:
        ~ImportFileUI();
        void InitializeComponent();

        void CheckFileType();
        bool OpenFileSelectionDlg();
        System::Void settingsBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void startBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        bool StartImport();
        System::Void fileName_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
        System::Void ImportFileUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void ImportFileUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e);
        System::Void ImportFileUI_Shown(System::Object^  sender, System::EventArgs^  e);

    private:
        System::ComponentModel::IContainer^     components;
        System::Windows::Forms::TextBox^        fileNameTextBox;
        System::Windows::Forms::TextBox^        fileTypeTextBox;
        System::Windows::Forms::ProgressBar^    taskProgress;
        System::Windows::Forms::ProgressBar^    totalProgress;
        System::Windows::Forms::Label^          taskLbl;

        System::Windows::Forms::Button^         settingsBtn;
        System::Windows::Forms::Button^         startBtn;
        System::Windows::Forms::Button^         closeBtn;

        DucoUI::DatabaseManager^                database;
        DucoUI::DatabaseImportUtil^             importer;
        WinRkImportProgressCallback::TImportStage    stage;
        long                                    fileSize;
        System::String^                         fileName;
        bool                                    preventOpening;
    };
}
