#pragma once

namespace DucoUI
{
    public ref class BellsRungUI : public System::Windows::Forms::Form
    {
    public:
        BellsRungUI(DucoUI::DatabaseManager^ theDatabase);
    protected:
        !BellsRungUI();
        ~BellsRungUI();

    private:
        System::Void backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void backgroundWorker1_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void backgroundWorker1_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);

        System::Void stageCtrl_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void ringerCtrl_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void towerCtrl_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void fromBack_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void methodCtrl_TextUpdate(System::Object^  sender, System::EventArgs^  e);

        System::Void BellsRungUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void filtersBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void printBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void printData(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args);

        System::Void BellsRungUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e);
        System::Void StartGeneration();


        System::ComponentModel::BackgroundWorker^       backgroundWorker1;
        Duco::StatisticFilters*                         filters;
        DucoUI::DatabaseManager^                        database;
        System::Boolean                                 loading;
        System::Boolean                                 restart;
        System::Double                                  totalPeals;

        System::Windows::Forms::ToolStripProgressBar^   progressBar;
    private: System::Windows::Forms::DataGridView^  dataGridView1;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  percentageColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  summaryColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  minorColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  majorColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  royalColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  maximusColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  fourteenColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  sixteenColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  eighteenColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  twentyColumn;
    private: System::Windows::Forms::ToolStripButton^ fromBackBtn;





           System::ComponentModel::Container^ components;

#pragma region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        void InitializeComponent(void)
        {
            System::Windows::Forms::StatusStrip^ statusStrip1;
            System::Windows::Forms::ToolStrip^ toolStrip1;
            System::Windows::Forms::ToolStripButton^ filtersBtn;
            System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(BellsRungUI::typeid));
            System::Windows::Forms::ToolStripButton^ printBtn;
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle1 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle2 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle3 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle4 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle5 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle6 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle7 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle8 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle9 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle10 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            this->progressBar = (gcnew System::Windows::Forms::ToolStripProgressBar());
            this->fromBackBtn = (gcnew System::Windows::Forms::ToolStripButton());
            this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
            this->percentageColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->summaryColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->minorColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->majorColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->royalColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->maximusColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->fourteenColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->sixteenColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->eighteenColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->twentyColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->backgroundWorker1 = (gcnew System::ComponentModel::BackgroundWorker());
            statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
            filtersBtn = (gcnew System::Windows::Forms::ToolStripButton());
            printBtn = (gcnew System::Windows::Forms::ToolStripButton());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            statusStrip1->SuspendLayout();
            toolStrip1->SuspendLayout();
            tableLayoutPanel1->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
            this->SuspendLayout();
            // 
            // statusStrip1
            // 
            statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->progressBar });
            statusStrip1->Location = System::Drawing::Point(0, 437);
            statusStrip1->Name = L"statusStrip1";
            statusStrip1->Size = System::Drawing::Size(478, 22);
            statusStrip1->TabIndex = 7;
            statusStrip1->Text = L"statusStrip1";
            // 
            // progressBar
            // 
            this->progressBar->Name = L"progressBar";
            this->progressBar->Size = System::Drawing::Size(100, 16);
            // 
            // toolStrip1
            // 
            toolStrip1->GripStyle = System::Windows::Forms::ToolStripGripStyle::Hidden;
            toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) { filtersBtn, printBtn, this->fromBackBtn });
            toolStrip1->Location = System::Drawing::Point(0, 0);
            toolStrip1->Name = L"toolStrip1";
            toolStrip1->Size = System::Drawing::Size(478, 25);
            toolStrip1->TabIndex = 8;
            toolStrip1->Text = L"toolStrip1";
            // 
            // filtersBtn
            // 
            filtersBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
            filtersBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"filtersBtn.Image")));
            filtersBtn->ImageTransparentColor = System::Drawing::Color::Magenta;
            filtersBtn->Name = L"filtersBtn";
            filtersBtn->Size = System::Drawing::Size(42, 22);
            filtersBtn->Text = L"&Filters";
            filtersBtn->Click += gcnew System::EventHandler(this, &BellsRungUI::filtersBtn_Click);
            // 
            // printBtn
            // 
            printBtn->Alignment = System::Windows::Forms::ToolStripItemAlignment::Right;
            printBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
            printBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"printBtn.Image")));
            printBtn->ImageTransparentColor = System::Drawing::Color::Magenta;
            printBtn->Name = L"printBtn";
            printBtn->Size = System::Drawing::Size(36, 22);
            printBtn->Text = L"&Print";
            printBtn->Click += gcnew System::EventHandler(this, &BellsRungUI::printBtn_Click);
            // 
            // fromBackBtn
            // 
            this->fromBackBtn->Checked = true;
            this->fromBackBtn->CheckOnClick = true;
            this->fromBackBtn->CheckState = System::Windows::Forms::CheckState::Checked;
            this->fromBackBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
            this->fromBackBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"fromBackBtn.Image")));
            this->fromBackBtn->ImageTransparentColor = System::Drawing::Color::Magenta;
            this->fromBackBtn->Margin = System::Windows::Forms::Padding(2);
            this->fromBackBtn->Name = L"fromBackBtn";
            this->fromBackBtn->Size = System::Drawing::Size(70, 21);
            this->fromBackBtn->Text = L"From &tenor";
            this->fromBackBtn->ToolTipText = L"When checked, from tenor rather than from the treble";
            this->fromBackBtn->CheckedChanged += gcnew System::EventHandler(this, &BellsRungUI::fromBack_CheckedChanged);
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 1;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->Controls->Add(this->dataGridView1, 0, 1);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 2;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 25)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->Size = System::Drawing::Size(478, 437);
            tableLayoutPanel1->TabIndex = 0;
            // 
            // dataGridView1
            // 
            this->dataGridView1->AllowUserToAddRows = false;
            this->dataGridView1->AllowUserToDeleteRows = false;
            this->dataGridView1->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::AllCells;
            this->dataGridView1->AutoSizeRowsMode = System::Windows::Forms::DataGridViewAutoSizeRowsMode::AllCells;
            this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->dataGridView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(10) {
                this->percentageColumn,
                    this->summaryColumn, this->minorColumn, this->majorColumn, this->royalColumn, this->maximusColumn, this->fourteenColumn, this->sixteenColumn,
                    this->eighteenColumn, this->twentyColumn
            });
            this->dataGridView1->Dock = System::Windows::Forms::DockStyle::Fill;
            this->dataGridView1->Location = System::Drawing::Point(3, 28);
            this->dataGridView1->Name = L"dataGridView1";
            this->dataGridView1->ReadOnly = true;
            this->dataGridView1->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
            this->dataGridView1->Size = System::Drawing::Size(472, 406);
            this->dataGridView1->TabIndex = 8;
            // 
            // percentageColumn
            // 
            dataGridViewCellStyle1->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->percentageColumn->DefaultCellStyle = dataGridViewCellStyle1;
            this->percentageColumn->Frozen = true;
            this->percentageColumn->HeaderText = L"%";
            this->percentageColumn->Name = L"percentageColumn";
            this->percentageColumn->ReadOnly = true;
            this->percentageColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->percentageColumn->Width = 21;
            // 
            // summaryColumn
            // 
            this->summaryColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            dataGridViewCellStyle2->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->summaryColumn->DefaultCellStyle = dataGridViewCellStyle2;
            this->summaryColumn->Frozen = true;
            this->summaryColumn->HeaderText = L"Total";
            this->summaryColumn->Name = L"summaryColumn";
            this->summaryColumn->ReadOnly = true;
            this->summaryColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->summaryColumn->Width = 37;
            // 
            // minorColumn
            // 
            this->minorColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            dataGridViewCellStyle3->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->minorColumn->DefaultCellStyle = dataGridViewCellStyle3;
            this->minorColumn->Frozen = true;
            this->minorColumn->HeaderText = L"Six";
            this->minorColumn->Name = L"minorColumn";
            this->minorColumn->ReadOnly = true;
            this->minorColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->minorColumn->Width = 27;
            // 
            // majorColumn
            // 
            this->majorColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            dataGridViewCellStyle4->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->majorColumn->DefaultCellStyle = dataGridViewCellStyle4;
            this->majorColumn->Frozen = true;
            this->majorColumn->HeaderText = L"Eight";
            this->majorColumn->Name = L"majorColumn";
            this->majorColumn->ReadOnly = true;
            this->majorColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->majorColumn->Width = 37;
            // 
            // royalColumn
            // 
            this->royalColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            dataGridViewCellStyle5->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->royalColumn->DefaultCellStyle = dataGridViewCellStyle5;
            this->royalColumn->Frozen = true;
            this->royalColumn->HeaderText = L"Ten";
            this->royalColumn->Name = L"royalColumn";
            this->royalColumn->ReadOnly = true;
            this->royalColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->royalColumn->Width = 32;
            // 
            // maximusColumn
            // 
            this->maximusColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            dataGridViewCellStyle6->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->maximusColumn->DefaultCellStyle = dataGridViewCellStyle6;
            this->maximusColumn->Frozen = true;
            this->maximusColumn->HeaderText = L"Twelve";
            this->maximusColumn->Name = L"maximusColumn";
            this->maximusColumn->ReadOnly = true;
            this->maximusColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->maximusColumn->Width = 48;
            // 
            // fourteenColumn
            // 
            this->fourteenColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            dataGridViewCellStyle7->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->fourteenColumn->DefaultCellStyle = dataGridViewCellStyle7;
            this->fourteenColumn->Frozen = true;
            this->fourteenColumn->HeaderText = L"Fourteen";
            this->fourteenColumn->Name = L"fourteenColumn";
            this->fourteenColumn->ReadOnly = true;
            this->fourteenColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->fourteenColumn->Width = 55;
            // 
            // sixteenColumn
            // 
            this->sixteenColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            dataGridViewCellStyle8->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->sixteenColumn->DefaultCellStyle = dataGridViewCellStyle8;
            this->sixteenColumn->Frozen = true;
            this->sixteenColumn->HeaderText = L"Sixteen";
            this->sixteenColumn->Name = L"sixteenColumn";
            this->sixteenColumn->ReadOnly = true;
            this->sixteenColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->sixteenColumn->Width = 48;
            // 
            // eighteenColumn
            // 
            this->eighteenColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            dataGridViewCellStyle9->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->eighteenColumn->DefaultCellStyle = dataGridViewCellStyle9;
            this->eighteenColumn->Frozen = true;
            this->eighteenColumn->HeaderText = L"Eighteen";
            this->eighteenColumn->Name = L"eighteenColumn";
            this->eighteenColumn->ReadOnly = true;
            this->eighteenColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->eighteenColumn->Width = 55;
            // 
            // twentyColumn
            // 
            this->twentyColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            dataGridViewCellStyle10->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->twentyColumn->DefaultCellStyle = dataGridViewCellStyle10;
            this->twentyColumn->Frozen = true;
            this->twentyColumn->HeaderText = L"Twenty";
            this->twentyColumn->Name = L"twentyColumn";
            this->twentyColumn->ReadOnly = true;
            this->twentyColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->twentyColumn->Width = 48;
            // 
            // backgroundWorker1
            // 
            this->backgroundWorker1->WorkerReportsProgress = true;
            this->backgroundWorker1->WorkerSupportsCancellation = true;
            this->backgroundWorker1->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &BellsRungUI::backgroundWorker1_DoWork);
            this->backgroundWorker1->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &BellsRungUI::backgroundWorker1_ProgressChanged);
            this->backgroundWorker1->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &BellsRungUI::backgroundWorker1_RunWorkerCompleted);
            // 
            // BellsRungUI
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(478, 459);
            this->Controls->Add(toolStrip1);
            this->Controls->Add(tableLayoutPanel1);
            this->Controls->Add(statusStrip1);
            this->Name = L"BellsRungUI";
            this->Text = L"Bells rung";
            this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &BellsRungUI::BellsRungUI_FormClosing);
            this->Load += gcnew System::EventHandler(this, &BellsRungUI::BellsRungUI_Load);
            statusStrip1->ResumeLayout(false);
            statusStrip1->PerformLayout();
            toolStrip1->ResumeLayout(false);
            toolStrip1->PerformLayout();
            tableLayoutPanel1->ResumeLayout(false);
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion
    };
}
