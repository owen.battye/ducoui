#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"
#include <StatisticFilters.h>

#include "TowerStatsUI.h"

#include <map>
#include <DucoConfiguration.h>
#include <DucoEngineUtils.h>
#include "DucoTableSorter.h"
#include "DucoUtils.h"
#include "DucoWindowState.h"
#include "FiltersUI.h"
#include <PealDatabase.h>
#include "OpenObjectCallback.h"
#include "PictureListControl.h"
#include "PictureControl.h"
#include <RingingDatabase.h>
#include "SoundUtils.h"
#include "SystemDefaultIcon.h"
#include <Tower.h>
#include "TowerUI.h"
#include <PealLengthInfo.h>
#include <SearchCommon.h>
#include <DatabaseSearch.h>
#include "ProgressWrapper.h"
#include "PrintPealSearchUtil.h"
#include "PealSearchUI.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::Windows::Forms;

TowerStatsUI::TowerStatsUI(DatabaseManager^ theDatabase)
    : database(theDatabase)
{
    filters = new Duco::StatisticFilters(database->Database());
    InitializeComponent();
}

TowerStatsUI::!TowerStatsUI()
{
    database->RemoveObserver(this);
    delete filters;
}

TowerStatsUI::~TowerStatsUI()
{
    this->!TowerStatsUI();
    backgroundGenerator->CancelAsync();
    if (components)
    {
        delete components;
    }
}

System::Void
TowerStatsUI::TowerStatsUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    StartGenerator(TState::ETowers);
    database->AddObserver(this);
}

void
TowerStatsUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    switch (type)
    {
    case TObjectType::EPeal:
    case TObjectType::ETower:
        towersGenerated = false;
        townsGenerated = false;
        countiesGenerated = false;
        tenorsGenerated = false;
        referencesGenerated = false;
        AutoStartGenerator();
        break;
    default:
        break;
    }
}

System::Void
TowerStatsUI::filtersBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    FiltersUI^ filtersDialog = gcnew FiltersUI(*filters, database);
    System::Windows::Forms::DialogResult returnVal = filtersDialog->ShowDialog(this);
    if (returnVal == System::Windows::Forms::DialogResult::OK)
    {
        towersGenerated = false;
        townsGenerated = false;
        countiesGenerated = false;
        tenorsGenerated = false;
        referencesGenerated = false;
        AutoStartGenerator();
    }
}


System::Void
TowerStatsUI::combineLinkedTowers_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    towersGenerated = false;
    townsGenerated = false;
    countiesGenerated = false;
    tenorsGenerated = false;
    referencesGenerated = false;
    AutoStartGenerator();
}

System::Void
TowerStatsUI::AutoStartGenerator()
{
    switch (tabControl->SelectedIndex)
    {
    default:
    case 0:
        if (towersGenerated)
        {
            countLbl->Text = Convert::ToString(towerData->Rows->Count) + " Towers";
        }
        else
        {
            StartGenerator(TState::ETowers);
        }
        break;
    case 1:
        if (townsGenerated)
            countLbl->Text = Convert::ToString(townsData->Rows->Count) + " Towns";
        else
            StartGenerator(TState::ETowns);
        break;
    case 2:
        if (countiesGenerated)
            countLbl->Text = Convert::ToString(countiesData->Rows->Count) + " Counties";
        else
            StartGenerator(TState::ECounties);
        break;
    case 3:
        if (tenorsGenerated)
            countLbl->Text = Convert::ToString(tenorsData->Rows->Count) + " Tenors";
        else
            StartGenerator(TState::ETenors);
        break;
    case 4:
        if (referencesGenerated)
            countLbl->Text = Convert::ToString(referencesDataview->Rows->Count) + " Towers";
        else
            StartGenerator(TState::EReferences);
        break;
    }
}

System::Void
TowerStatsUI::StartGenerator(TState newState)
{
    if (backgroundGenerator->IsBusy && newState != TState::ENon)
    {
        backgroundGenerator->CancelAsync();
        nextState = newState;
    }
    else if (newState != TState::ENon)
    {
        nextState = TState::ENon;
        state = newState;
        switch (state)
        {
        default:
        case TState::ETowers:
            {
            towerData->Rows->Clear();
            towerData->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::EnableResizing;
            }
            break;
        case TState::ETowns:
            {
            townsData->Rows->Clear();
            townsData->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::EnableResizing;
            }
            break;
        case TState::ECounties:
            {
            countiesData->Rows->Clear();
            countiesData->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::EnableResizing;
            }
            break;
        case TState::ETenors:
            {
            tenorsData->Rows->Clear();
            tenorsData->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::EnableResizing;
            }
            break;
        case TState::EReferences:
            {
            referencesDataview->Rows->Clear();
            referencesDataview->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::EnableResizing;
            }
            break;
        }
        TowerStatsUIArgs^ args = gcnew TowerStatsUIArgs();
        args->state = state;
        args->includeLinked = combineLinkedTowers->Checked;

        backgroundGenerator->RunWorkerAsync(args);
    }
}

System::Void
TowerStatsUI::backgroundGenerator_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
    TowerStatsUIArgs^ currentState = static_cast<TowerStatsUIArgs^>(e->Argument);

    switch (currentState->state)
    {
    case TState::ETowers:
        {
            std::multimap<Duco::PealLengthInfo, Duco::ObjectId> towerData;
            if (currentState->includeLinked)
            {
                filters->SetIncludeLinkedTowers(true);
            }
            database->Database().PealsDatabase().GetTowersAndPealCount(*filters, currentState->includeLinked, towerData);
            double noOfRows = double(towerData.size());
            bool ignoreTowersWithOnePeal = false;
            if (database->Config().DisableFeaturesForPerformance(towerData.size()))
            {
                ignoreTowersWithOnePeal = true;
            }

            double count = 0;
            int displayPosition = 0;
            size_t lastCount = -1;
            std::map<Duco::PealLengthInfo, Duco::ObjectId>::const_reverse_iterator it = towerData.rbegin();
            while (it != towerData.rend())
            {
                if (lastCount != it->first.TotalPeals())
                {
                    displayPosition = int(count) + 1;
                    lastCount = it->first.TotalPeals();
                }
                if (ignoreTowersWithOnePeal && it->second <= 1)
                {
                    ++count;
                }
                else
                {
                    Duco::Tower thisTower;
                    if (database->FindTower(it->second, thisTower, false))
                    {
                        DataGridViewRow^ newRow = gcnew DataGridViewRow();
                        newRow->HeaderCell->Value = Convert::ToString(displayPosition);
                        DataGridViewTextBoxCell^ idCell = gcnew DataGridViewTextBoxCell();
                        idCell->Value = DucoUtils::ConvertString(it->second);
                        newRow->Cells->Add(idCell);
                        DataGridViewTextBoxCell^ nameCell = gcnew DataGridViewTextBoxCell();
                        nameCell->Value = DucoUtils::ConvertString(thisTower.FullName());
                        newRow->Cells->Add(nameCell);
                        DataGridViewTextBoxCell^ newCell = gcnew DataGridViewTextBoxCell();
                        newCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->first.TotalPeals(), true));
                        newRow->Cells->Add(newCell);
                        DataGridViewTextBoxCell^ changesCell = gcnew DataGridViewTextBoxCell();
                        changesCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->first.TotalChanges(), true));
                        newRow->Cells->Add(changesCell);
                        DataGridViewTextBoxCell^ timeCell = gcnew DataGridViewTextBoxCell();
                        timeCell->Value = DucoUtils::PrintPealTime(it->first.TotalMinutes(), false);
                        newRow->Cells->Add(timeCell);
                        backgroundGenerator->ReportProgress(int((++count / noOfRows)*(double)100), newRow);
                    }
                }
                ++it;
            }
        }
        break;
    case TState::ETowns:
        {
            std::map<std::wstring, Duco::PealLengthInfo> sortedTowns;
            database->Database().PealsDatabase().GetTownNamesWithPealCount(*filters, sortedTowns);

            double noOfRows = double(sortedTowns.size());
            double count = 0;
            std::map<std::wstring, Duco::PealLengthInfo>::const_reverse_iterator it = sortedTowns.rbegin();
            while (it != sortedTowns.rend())
            {
                DataGridViewRow^ newRow = gcnew DataGridViewRow();
                newRow->HeaderCell->Value = DucoUtils::ConvertString(it->first);
                DataGridViewTextBoxCell^ performanceCountCell = gcnew DataGridViewTextBoxCell();
                performanceCountCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->second.TotalPeals(), true));
                newRow->Cells->Add(performanceCountCell);
                DataGridViewTextBoxCell^ changesCell = gcnew DataGridViewTextBoxCell();
                changesCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->second.TotalChanges(), true));
                newRow->Cells->Add(changesCell);
                DataGridViewTextBoxCell^ timeCell = gcnew DataGridViewTextBoxCell();
                timeCell->Value = DucoUtils::PrintPealTime(it->second.TotalMinutes(), false);
                newRow->Cells->Add(timeCell);
                backgroundGenerator->ReportProgress(int((++count / noOfRows)*(double)100), newRow);

                ++it;
            }
        }
        break;
    case TState::ECounties:
        {
            std::map<std::wstring, Duco::PealLengthInfo> sortedCounties;
            database->Database().PealsDatabase().GetCountiesWithPealCount(*filters, sortedCounties);

            double noOfRows = double(sortedCounties.size());
            double count = 0;
            std::map<std::wstring, Duco::PealLengthInfo>::const_reverse_iterator it = sortedCounties.rbegin();
            while (it != sortedCounties.rend())
            {
                DataGridViewRow^ newRow = gcnew DataGridViewRow();
                newRow->HeaderCell->Value = DucoUtils::ConvertString(it->first);
                DataGridViewTextBoxCell^ newCell = gcnew DataGridViewTextBoxCell();
                newCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->second.TotalPeals(), true));
                newRow->Cells->Add(newCell);
                DataGridViewTextBoxCell^ changesCell = gcnew DataGridViewTextBoxCell();
                changesCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->second.TotalChanges(), true));
                newRow->Cells->Add(changesCell);
                DataGridViewTextBoxCell^ timeCell = gcnew DataGridViewTextBoxCell();
                timeCell->Value = DucoUtils::PrintPealTime(it->second.TotalMinutes(), false);
                newRow->Cells->Add(timeCell);
                backgroundGenerator->ReportProgress(int((++count / noOfRows)*(double)100), newRow);

                ++it;
            }
        }
        break;
    case TState::ETenors:
    {
        std::map<std::wstring, Duco::PealLengthInfo> sortedTenors;
        database->Database().PealsDatabase().GetTenorKeysWithPealCount(*filters, sortedTenors);

        double noOfRows = double(sortedTenors.size());
        double count = 0;
        std::map<std::wstring, Duco::PealLengthInfo>::const_reverse_iterator it = sortedTenors.rbegin();
        while (it != sortedTenors.rend())
        {
            DataGridViewRow^ newRow = gcnew DataGridViewRow();
            newRow->HeaderCell->Value = DucoUtils::ConvertString(it->first);
            if (it->first.length() == 0)
            {
                newRow->HeaderCell->Value = "Not set";
            }
            DataGridViewTextBoxCell^ newCell = gcnew DataGridViewTextBoxCell();
            newCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->second.TotalPeals(), true));
            newRow->Cells->Add(newCell);
            DataGridViewTextBoxCell^ changesCell = gcnew DataGridViewTextBoxCell();
            changesCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->second.TotalChanges(), true));
            newRow->Cells->Add(changesCell);
            DataGridViewTextBoxCell^ timeCell = gcnew DataGridViewTextBoxCell();
            timeCell->Value = DucoUtils::PrintPealTime(it->second.TotalMinutes(), false);
            newRow->Cells->Add(timeCell);
            backgroundGenerator->ReportProgress(int((++count / noOfRows) * (double)100), newRow);

            ++it;
        }
    }
    break;
    case TState::EReferences:
    {
        std::map<Duco::ObjectId, Duco::ReferenceItem> towers;
        database->Database().PealsDatabase().ReferenceStats(*filters, towers);

        double noOfRows = double(towers.size());
        double count = 0;
        std::map<Duco::ObjectId, Duco::ReferenceItem>::const_reverse_iterator it = towers.rbegin();
        while (it != towers.rend())
        {
            DataGridViewRow^ newRow = gcnew DataGridViewRow();
            newRow->HeaderCell->Value = DucoUtils::ConvertString(it->first);
            DataGridViewTextBoxCell^ newCell = gcnew DataGridViewTextBoxCell();
            newCell->Value = database->TowerName(it->first);
            newRow->Cells->Add(newCell);
            DataGridViewTextBoxCell^ pealCountCell = gcnew DataGridViewTextBoxCell();
            DataGridViewTextBoxCell^ bellboardCell = gcnew DataGridViewTextBoxCell();
            DataGridViewTextBoxCell^ ringingWorldCell = gcnew DataGridViewTextBoxCell();
            pealCountCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->second.NoOfPeals(), true));
            if (it->second.Valid())
            {
                bellboardCell->Value = DucoUtils::FormatFloatToString(it->second.PercentWithBellboardReference());
                ringingWorldCell->Value = DucoUtils::FormatFloatToString(it->second.PercentWithRingingWorldReference());
            }
            newRow->Cells->Add(pealCountCell);
            newRow->Cells->Add(bellboardCell);
            newRow->Cells->Add(ringingWorldCell);
            backgroundGenerator->ReportProgress(int((++count / noOfRows) * (double)100), newRow);

            ++it;
        }
    }
    break;
    default:
        break;
    }
}

System::Void
TowerStatsUI::backgroundGenerator_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    if (backgroundGenerator->CancellationPending)
    {
        return;
    }
    switch (state)
    {
    case TState::ETowers:
        towerData->Rows->Add((DataGridViewRow^)e->UserState);
        break;
    case TState::ETowns:
        townsData->Rows->Add((DataGridViewRow^)e->UserState);
        break;
    case TState::ECounties:
        countiesData->Rows->Add((DataGridViewRow^)e->UserState);
        break;
    case TState::ETenors:
        tenorsData->Rows->Add((DataGridViewRow^)e->UserState);
        break;
    case TState::EReferences:
        referencesDataview->Rows->Add((DataGridViewRow^)e->UserState);
        break;
    default:
        break;
    }
}

System::Void
TowerStatsUI::backgroundGenerator_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    if (e->Cancelled)
    {
        StartGenerator(nextState);
    }
    else
    {
        progressBar->Value = 0;
        switch (state)
        {
        case TState::ETowers:
            {
            countLbl->Text = Convert::ToString(towerData->Rows->Count) + " Towers";
            towerData->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;

            DucoTableSorter^ sorter = gcnew DucoTableSorter(true, EString);
            sorter->AddSortObject(ECount, false, 2);
            sorter->AddSortObject(EString, true, 1);
            towerData->Sort(sorter);

            DataGridViewColumn^ newColumn = towerData->Columns[2];
            newColumn->HeaderCell->SortGlyphDirection = SortOrder::Descending;
            towersGenerated = true;
            }
            break;

        case TState::ETowns:
            {
            countLbl->Text = Convert::ToString(townsData->Rows->Count) + " Towns";
            townsData->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;

            DucoTableSorter^ sorter = gcnew DucoTableSorter(true, EString);
            sorter->AddSortObject(ECount, false, 0);
            townsData->Sort(sorter);

            DataGridViewColumn^ newColumn = townsData->Columns[0];
            newColumn->HeaderCell->SortGlyphDirection = SortOrder::Descending;
            townsGenerated = true;
            }
            break;
        case TState::ECounties:
            {
            countLbl->Text = Convert::ToString(countiesData->Rows->Count) + " Counties";
            countiesData->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;

            DucoTableSorter^ sorter = gcnew DucoTableSorter(true, EString);
            sorter->AddSortObject(ECount, false, 0);
            countiesData->Sort(sorter);

            DataGridViewColumn^ newColumn = countiesData->Columns[0];
            newColumn->HeaderCell->SortGlyphDirection = SortOrder::Descending;
            countiesGenerated = true;
            }
            break;
        case TState::ETenors:
        {
            countLbl->Text = Convert::ToString(countiesData->Rows->Count) + " Tenors";
            tenorsData->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;

            DucoTableSorter^ sorter = gcnew DucoTableSorter(true, EString);
            sorter->AddSortObject(ECount, false, 0);
            tenorsData->Sort(sorter);

            DataGridViewColumn^ newColumn = tenorsData->Columns[0];
            newColumn->HeaderCell->SortGlyphDirection = SortOrder::Descending;
            tenorsGenerated = true;
        }
        break;
        case TState::EReferences:
        {
            countLbl->Text = Convert::ToString(referencesDataview->Rows->Count) + " Towers";
            referencesDataview->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;

            DucoTableSorter^ sorter = gcnew DucoTableSorter(true, EString);
            sorter->AddSortObject(EFloat, false, 1);
            referencesDataview->Sort(sorter);

            DataGridViewColumn^ newColumn = referencesDataview->Columns[0];
            newColumn->HeaderCell->SortGlyphDirection = SortOrder::Descending;
            referencesGenerated = true;
        }
        break;
        default:
            break;
        }
    }
}

System::Void
TowerStatsUI::towerData_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
    DataGridViewColumn^ newColumn = towerData->Columns[e->ColumnIndex];
    if (newColumn->SortMode != DataGridViewColumnSortMode::Programmatic)
    {
        towerData->Columns[2]->HeaderCell->SortGlyphDirection = SortOrder::None;
        return;
    }

    SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;
    if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
        newSortOrder = SortOrder::Ascending;
    else
        newSortOrder = SortOrder::Descending;

    for (int i(0); i < towerData->Columns->Count; ++i)
    {
        towerData->Columns[i]->HeaderCell->SortGlyphDirection = SortOrder::None;
    }
    newColumn->HeaderCell->SortGlyphDirection = newSortOrder;

    DucoTableSorter^ sorter = gcnew DucoTableSorter(newSortOrder == SortOrder::Ascending, EString);
    sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
    towerData->Sort(sorter);
}

System::Void
TowerStatsUI::townsData_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
    DataGridViewColumn^ newColumn = townsData->Columns[e->ColumnIndex];

    SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;
    if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
        newSortOrder = SortOrder::Ascending;
    else
        newSortOrder = SortOrder::Descending;

    for (int i(0); i < townsData->Columns->Count; ++i)
    {
        townsData->Columns[i]->HeaderCell->SortGlyphDirection = SortOrder::None;
    }
    newColumn->HeaderCell->SortGlyphDirection = newSortOrder;

    DucoTableSorter^ sorter = gcnew DucoTableSorter(newSortOrder == SortOrder::Ascending, EString);
    sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
    townsData->Sort(sorter);
}

System::Void
TowerStatsUI::countiesData_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
    DataGridViewColumn^ newColumn = countiesData->Columns[e->ColumnIndex];

    SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;
    if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
        newSortOrder = SortOrder::Ascending;
    else
        newSortOrder = SortOrder::Descending;

    for (int i(0); i < countiesData->Columns->Count; ++i)
    {
        countiesData->Columns[i]->HeaderCell->SortGlyphDirection = SortOrder::None;
    }

    newColumn->HeaderCell->SortGlyphDirection = newSortOrder;
    DucoTableSorter^ sorter = gcnew DucoTableSorter(newSortOrder == SortOrder::Ascending, EString);
    sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
    countiesData->Sort(sorter);
}

System::Void
TowerStatsUI::referencesDataview_ColumnHeaderMouseClick(System::Object^ sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^ e)
{
    DataGridViewColumn^ newColumn = referencesDataview->Columns[e->ColumnIndex];
    if (newColumn->SortMode != DataGridViewColumnSortMode::Programmatic)
    {
        referencesDataview->Columns[2]->HeaderCell->SortGlyphDirection = SortOrder::None;
        return;
    }

    SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;
    if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
        newSortOrder = SortOrder::Ascending;
    else
        newSortOrder = SortOrder::Descending;

    for (int i(0); i < referencesDataview->Columns->Count; ++i)
    {
        referencesDataview->Columns[i]->HeaderCell->SortGlyphDirection = SortOrder::None;
    }

    newColumn->HeaderCell->SortGlyphDirection = newSortOrder;

    DucoTableSorter^ sorter = gcnew DucoTableSorter(newSortOrder == SortOrder::Ascending, EString);
    sorter->AddSortObject(EFloat, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
    referencesDataview->Sort(sorter);
}

System::Void
TowerStatsUI::tabControl_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    AutoStartGenerator();
}

System::Void
TowerStatsUI::towerData_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    try
    {
        unsigned int rowIndex = e->RowIndex;
        DataGridViewRow^ currentRow = towerData->Rows[rowIndex];
        unsigned int towerId = Convert::ToInt16(currentRow->Cells[0]->Value);
        TowerUI::ShowTower(database, this->MdiParent, towerId, KNoId);
    }
    catch (Exception^)
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
TowerStatsUI::referencesDataview_CellClick(System::Object^ sender, System::Windows::Forms::DataGridViewCellEventArgs^ e)
{
    try
    {
        unsigned int rowIndex = e->RowIndex;
        DataGridViewRow^ currentRow = referencesDataview->Rows[rowIndex];
        unsigned int towerId = Convert::ToInt16(currentRow->HeaderCell->Value);
        if (e->ColumnIndex > 0)
        {
            PealSearchUI^ search = gcnew PealSearchUI(database, false);
            search->SearchPealsInTower(this->MdiParent, towerId);
        }
        else
        {
            TowerUI::ShowTower(database, this->MdiParent, towerId, KNoId);
        }
    }
    catch (Exception^)
    {
        SoundUtils::PlayErrorSound();
    }
}

void
TowerStatsUI::InitializeComponent()
{
    System::Windows::Forms::StatusStrip^ statusStrip1;
    System::Windows::Forms::TabPage^ towersPage;
    System::Windows::Forms::TableLayoutPanel^ towersLayoutPanel;
    System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle9 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
    System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle10 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
    System::Windows::Forms::TabPage^ townsPage;
    System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle11 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
    System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle12 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
    System::Windows::Forms::TabPage^ countiesPage;
    System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle13 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
    System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle14 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
    System::Windows::Forms::ToolStrip^ toolStrip1;
    System::Windows::Forms::ToolStripButton^ filtersBtn;
    System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(TowerStatsUI::typeid));
    System::Windows::Forms::TabPage^ tenorsPage;
    System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle15 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
    System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle16 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
    this->progressBar = (gcnew System::Windows::Forms::ToolStripProgressBar());
    this->countLbl = (gcnew System::Windows::Forms::ToolStripStatusLabel());
    this->combineLinkedTowers = (gcnew System::Windows::Forms::CheckBox());
    this->towerData = (gcnew System::Windows::Forms::DataGridView());
    this->IdColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->TowerNameColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->pealCount = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->changesColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->timeColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->townsData = (gcnew System::Windows::Forms::DataGridView());
    this->townPealCount = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->changesColum = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->townsTimeColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->countiesData = (gcnew System::Windows::Forms::DataGridView());
    this->countiesCount = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->countiesChangesColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->countiesTimeColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->tenorsData = (gcnew System::Windows::Forms::DataGridView());
    this->dataGridViewTextBoxColumn1 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->dataGridViewTextBoxColumn2 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->dataGridViewTextBoxColumn3 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->tabControl = (gcnew System::Windows::Forms::TabControl());
    this->referencesPage = (gcnew System::Windows::Forms::TabPage());
    this->referencesDataview = (gcnew System::Windows::Forms::DataGridView());
    this->backgroundGenerator = (gcnew System::ComponentModel::BackgroundWorker());
    this->webTowerColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->referencesPealCount = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->referencesBellboard = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->referencesRingingWorld = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
    towersPage = (gcnew System::Windows::Forms::TabPage());
    towersLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
    townsPage = (gcnew System::Windows::Forms::TabPage());
    countiesPage = (gcnew System::Windows::Forms::TabPage());
    toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
    filtersBtn = (gcnew System::Windows::Forms::ToolStripButton());
    tenorsPage = (gcnew System::Windows::Forms::TabPage());
    statusStrip1->SuspendLayout();
    towersPage->SuspendLayout();
    towersLayoutPanel->SuspendLayout();
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->towerData))->BeginInit();
    townsPage->SuspendLayout();
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->townsData))->BeginInit();
    countiesPage->SuspendLayout();
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->countiesData))->BeginInit();
    toolStrip1->SuspendLayout();
    tenorsPage->SuspendLayout();
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->tenorsData))->BeginInit();
    this->tabControl->SuspendLayout();
    this->referencesPage->SuspendLayout();
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->referencesDataview))->BeginInit();
    this->SuspendLayout();
    // 
    // statusStrip1
    // 
    statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) { this->progressBar, this->countLbl });
    statusStrip1->Location = System::Drawing::Point(0, 375);
    statusStrip1->Name = L"statusStrip1";
    statusStrip1->Size = System::Drawing::Size(813, 22);
    statusStrip1->TabIndex = 1;
    statusStrip1->Text = L"statusStrip1";
    // 
    // progressBar
    // 
    this->progressBar->Name = L"progressBar";
    this->progressBar->Size = System::Drawing::Size(100, 16);
    // 
    // countLbl
    // 
    this->countLbl->Name = L"countLbl";
    this->countLbl->Size = System::Drawing::Size(696, 17);
    this->countLbl->Spring = true;
    this->countLbl->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
    // 
    // towersPage
    // 
    towersPage->Controls->Add(towersLayoutPanel);
    towersPage->Location = System::Drawing::Point(4, 22);
    towersPage->Name = L"towersPage";
    towersPage->Padding = System::Windows::Forms::Padding(3);
    towersPage->Size = System::Drawing::Size(805, 324);
    towersPage->TabIndex = 0;
    towersPage->Text = L"Towers";
    towersPage->UseVisualStyleBackColor = true;
    // 
    // towersLayoutPanel
    // 
    towersLayoutPanel->ColumnCount = 1;
    towersLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
    towersLayoutPanel->Controls->Add(this->combineLinkedTowers, 0, 0);
    towersLayoutPanel->Controls->Add(this->towerData, 0, 1);
    towersLayoutPanel->Dock = System::Windows::Forms::DockStyle::Fill;
    towersLayoutPanel->Location = System::Drawing::Point(3, 3);
    towersLayoutPanel->Name = L"towersLayoutPanel";
    towersLayoutPanel->RowCount = 2;
    towersLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
    towersLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
    towersLayoutPanel->Size = System::Drawing::Size(799, 318);
    towersLayoutPanel->TabIndex = 0;
    // 
    // combineLinkedTowers
    // 
    this->combineLinkedTowers->AutoSize = true;
    this->combineLinkedTowers->Location = System::Drawing::Point(3, 3);
    this->combineLinkedTowers->Name = L"combineLinkedTowers";
    this->combineLinkedTowers->Size = System::Drawing::Size(132, 17);
    this->combineLinkedTowers->TabIndex = 0;
    this->combineLinkedTowers->Text = L"Combine linked towers";
    this->combineLinkedTowers->UseVisualStyleBackColor = true;
    this->combineLinkedTowers->CheckedChanged += gcnew System::EventHandler(this, &TowerStatsUI::combineLinkedTowers_CheckedChanged);
    // 
    // towerData
    // 
    this->towerData->AllowUserToAddRows = false;
    this->towerData->AllowUserToDeleteRows = false;
    this->towerData->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::AllCells;
    this->towerData->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
    this->towerData->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(5) {
        this->IdColumn, this->TowerNameColumn,
            this->pealCount, this->changesColumn, this->timeColumn
    });
    this->towerData->Dock = System::Windows::Forms::DockStyle::Fill;
    this->towerData->Location = System::Drawing::Point(3, 26);
    this->towerData->Name = L"towerData";
    this->towerData->ReadOnly = true;
    this->towerData->Size = System::Drawing::Size(793, 289);
    this->towerData->TabIndex = 1;
    this->towerData->CellContentDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &TowerStatsUI::towerData_CellContentDoubleClick);
    this->towerData->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &TowerStatsUI::towerData_ColumnHeaderMouseClick);
    // 
    // IdColumn
    // 
    this->IdColumn->HeaderText = L"Id";
    this->IdColumn->Name = L"IdColumn";
    this->IdColumn->ReadOnly = true;
    this->IdColumn->Visible = false;
    this->IdColumn->Width = 41;
    // 
    // TowerNameColumn
    // 
    this->TowerNameColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
    this->TowerNameColumn->HeaderText = L"Tower";
    this->TowerNameColumn->Name = L"TowerNameColumn";
    this->TowerNameColumn->ReadOnly = true;
    // 
    // pealCount
    // 
    this->pealCount->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
    this->pealCount->HeaderText = database->PerformanceString(false) + "s";
    this->pealCount->Name = L"pealCount";
    this->pealCount->ReadOnly = true;
    this->pealCount->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
    this->pealCount->ToolTipText = L"Number of performances in this tower";
    this->pealCount->Width = 58;
    // 
    // changesColumn
    // 
    this->changesColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
    dataGridViewCellStyle9->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
    this->changesColumn->DefaultCellStyle = dataGridViewCellStyle9;
    this->changesColumn->HeaderText = L"Changes";
    this->changesColumn->Name = L"changesColumn";
    this->changesColumn->ReadOnly = true;
    this->changesColumn->Width = 74;
    // 
    // timeColumn
    // 
    this->timeColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
    dataGridViewCellStyle10->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
    this->timeColumn->DefaultCellStyle = dataGridViewCellStyle10;
    this->timeColumn->HeaderText = L"Time";
    this->timeColumn->Name = L"timeColumn";
    this->timeColumn->ReadOnly = true;
    this->timeColumn->Width = 55;
    // 
    // townsPage
    // 
    townsPage->Controls->Add(this->townsData);
    townsPage->Location = System::Drawing::Point(4, 22);
    townsPage->Name = L"townsPage";
    townsPage->Padding = System::Windows::Forms::Padding(3);
    townsPage->Size = System::Drawing::Size(805, 324);
    townsPage->TabIndex = 1;
    townsPage->Text = L"Towns";
    townsPage->UseVisualStyleBackColor = true;
    // 
    // townsData
    // 
    this->townsData->AllowUserToAddRows = false;
    this->townsData->AllowUserToDeleteRows = false;
    this->townsData->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
    this->townsData->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(3) {
        this->townPealCount,
            this->changesColum, this->townsTimeColumn
    });
    this->townsData->Dock = System::Windows::Forms::DockStyle::Fill;
    this->townsData->Location = System::Drawing::Point(3, 3);
    this->townsData->Name = L"townsData";
    this->townsData->ReadOnly = true;
    this->townsData->Size = System::Drawing::Size(799, 318);
    this->townsData->TabIndex = 0;
    this->townsData->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &TowerStatsUI::townsData_ColumnHeaderMouseClick);
    // 
    // townPealCount
    // 
    this->townPealCount->HeaderText = L"Performances";
    this->townPealCount->Name = L"townPealCount";
    this->townPealCount->ReadOnly = true;
    this->townPealCount->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
    // 
    // changesColum
    // 
    this->changesColum->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
    dataGridViewCellStyle11->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
    this->changesColum->DefaultCellStyle = dataGridViewCellStyle11;
    this->changesColum->HeaderText = L"Changes";
    this->changesColum->Name = L"changesColum";
    this->changesColum->ReadOnly = true;
    this->changesColum->Width = 74;
    // 
    // townsTimeColumn
    // 
    this->townsTimeColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
    dataGridViewCellStyle12->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
    this->townsTimeColumn->DefaultCellStyle = dataGridViewCellStyle12;
    this->townsTimeColumn->HeaderText = L"Time";
    this->townsTimeColumn->Name = L"townsTimeColumn";
    this->townsTimeColumn->ReadOnly = true;
    this->townsTimeColumn->Width = 55;
    // 
    // countiesPage
    // 
    countiesPage->Controls->Add(this->countiesData);
    countiesPage->Location = System::Drawing::Point(4, 22);
    countiesPage->Name = L"countiesPage";
    countiesPage->Padding = System::Windows::Forms::Padding(3);
    countiesPage->Size = System::Drawing::Size(805, 324);
    countiesPage->TabIndex = 2;
    countiesPage->Text = L"Counties";
    countiesPage->UseVisualStyleBackColor = true;
    // 
    // countiesData
    // 
    this->countiesData->AllowUserToAddRows = false;
    this->countiesData->AllowUserToDeleteRows = false;
    this->countiesData->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
    this->countiesData->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(3) {
        this->countiesCount,
            this->countiesChangesColumn, this->countiesTimeColumn
    });
    this->countiesData->Dock = System::Windows::Forms::DockStyle::Fill;
    this->countiesData->Location = System::Drawing::Point(3, 3);
    this->countiesData->Name = L"countiesData";
    this->countiesData->ReadOnly = true;
    this->countiesData->Size = System::Drawing::Size(799, 318);
    this->countiesData->TabIndex = 0;
    this->countiesData->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &TowerStatsUI::countiesData_ColumnHeaderMouseClick);
    // 
    // countiesCount
    // 
    this->countiesCount->HeaderText = L"Performances";
    this->countiesCount->Name = L"countiesCount";
    this->countiesCount->ReadOnly = true;
    this->countiesCount->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
    // 
    // countiesChangesColumn
    // 
    this->countiesChangesColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
    dataGridViewCellStyle13->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
    this->countiesChangesColumn->DefaultCellStyle = dataGridViewCellStyle13;
    this->countiesChangesColumn->HeaderText = L"Changes";
    this->countiesChangesColumn->Name = L"countiesChangesColumn";
    this->countiesChangesColumn->ReadOnly = true;
    this->countiesChangesColumn->Width = 74;
    // 
    // countiesTimeColumn
    // 
    this->countiesTimeColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
    dataGridViewCellStyle14->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
    this->countiesTimeColumn->DefaultCellStyle = dataGridViewCellStyle14;
    this->countiesTimeColumn->HeaderText = L"Time";
    this->countiesTimeColumn->Name = L"countiesTimeColumn";
    this->countiesTimeColumn->ReadOnly = true;
    this->countiesTimeColumn->Width = 55;
    // 
    // toolStrip1
    // 
    toolStrip1->GripStyle = System::Windows::Forms::ToolStripGripStyle::Hidden;
    toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { filtersBtn });
    toolStrip1->Location = System::Drawing::Point(0, 0);
    toolStrip1->Name = L"toolStrip1";
    toolStrip1->Size = System::Drawing::Size(813, 25);
    toolStrip1->TabIndex = 2;
    toolStrip1->Text = L"toolStrip1";
    // 
    // filtersBtn
    // 
    filtersBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
    filtersBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"filtersBtn.Image")));
    filtersBtn->ImageTransparentColor = System::Drawing::Color::Magenta;
    filtersBtn->Name = L"filtersBtn";
    filtersBtn->Size = System::Drawing::Size(42, 22);
    filtersBtn->Text = L"&Filters";
    filtersBtn->Click += gcnew System::EventHandler(this, &TowerStatsUI::filtersBtn_Click);
    // 
    // tenorsPage
    // 
    tenorsPage->Controls->Add(this->tenorsData);
    tenorsPage->Location = System::Drawing::Point(4, 22);
    tenorsPage->Name = L"tenorsPage";
    tenorsPage->Padding = System::Windows::Forms::Padding(3);
    tenorsPage->Size = System::Drawing::Size(805, 324);
    tenorsPage->TabIndex = 3;
    tenorsPage->Text = L"Tenor keys";
    tenorsPage->UseVisualStyleBackColor = true;
    // 
    // tenorsData
    // 
    this->tenorsData->AllowUserToAddRows = false;
    this->tenorsData->AllowUserToDeleteRows = false;
    this->tenorsData->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
    this->tenorsData->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(3) {
        this->dataGridViewTextBoxColumn1,
            this->dataGridViewTextBoxColumn2, this->dataGridViewTextBoxColumn3
    });
    this->tenorsData->Dock = System::Windows::Forms::DockStyle::Fill;
    this->tenorsData->Location = System::Drawing::Point(3, 3);
    this->tenorsData->Name = L"tenorsData";
    this->tenorsData->ReadOnly = true;
    this->tenorsData->Size = System::Drawing::Size(799, 318);
    this->tenorsData->TabIndex = 1;
    // 
    // dataGridViewTextBoxColumn1
    // 
    this->dataGridViewTextBoxColumn1->HeaderText = L"Performances";
    this->dataGridViewTextBoxColumn1->Name = L"dataGridViewTextBoxColumn1";
    this->dataGridViewTextBoxColumn1->ReadOnly = true;
    this->dataGridViewTextBoxColumn1->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
    // 
    // dataGridViewTextBoxColumn2
    // 
    this->dataGridViewTextBoxColumn2->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
    dataGridViewCellStyle15->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
    this->dataGridViewTextBoxColumn2->DefaultCellStyle = dataGridViewCellStyle15;
    this->dataGridViewTextBoxColumn2->HeaderText = L"Changes";
    this->dataGridViewTextBoxColumn2->Name = L"dataGridViewTextBoxColumn2";
    this->dataGridViewTextBoxColumn2->ReadOnly = true;
    this->dataGridViewTextBoxColumn2->Width = 74;
    // 
    // dataGridViewTextBoxColumn3
    // 
    this->dataGridViewTextBoxColumn3->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
    dataGridViewCellStyle16->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
    this->dataGridViewTextBoxColumn3->DefaultCellStyle = dataGridViewCellStyle16;
    this->dataGridViewTextBoxColumn3->HeaderText = L"Time";
    this->dataGridViewTextBoxColumn3->Name = L"dataGridViewTextBoxColumn3";
    this->dataGridViewTextBoxColumn3->ReadOnly = true;
    this->dataGridViewTextBoxColumn3->Width = 55;
    // 
    // tabControl
    // 
    this->tabControl->Controls->Add(towersPage);
    this->tabControl->Controls->Add(townsPage);
    this->tabControl->Controls->Add(countiesPage);
    this->tabControl->Controls->Add(tenorsPage);
    this->tabControl->Controls->Add(this->referencesPage);
    this->tabControl->Dock = System::Windows::Forms::DockStyle::Fill;
    this->tabControl->Location = System::Drawing::Point(0, 25);
    this->tabControl->Name = L"tabControl";
    this->tabControl->SelectedIndex = 0;
    this->tabControl->Size = System::Drawing::Size(813, 350);
    this->tabControl->TabIndex = 0;
    this->tabControl->SelectedIndexChanged += gcnew System::EventHandler(this, &TowerStatsUI::tabControl_SelectedIndexChanged);
    // 
    // referencesPage
    // 
    this->referencesPage->Controls->Add(this->referencesDataview);
    this->referencesPage->Location = System::Drawing::Point(4, 22);
    this->referencesPage->Name = L"referencesPage";
    this->referencesPage->Padding = System::Windows::Forms::Padding(3);
    this->referencesPage->Size = System::Drawing::Size(805, 324);
    this->referencesPage->TabIndex = 4;
    this->referencesPage->Text = L"Web references";
    this->referencesPage->UseVisualStyleBackColor = true;
    // 
    // referencesDataview
    // 
    this->referencesDataview->AllowUserToAddRows = false;
    this->referencesDataview->AllowUserToDeleteRows = false;
    this->referencesDataview->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::DisplayedCellsExceptHeader;
    this->referencesDataview->AutoSizeRowsMode = System::Windows::Forms::DataGridViewAutoSizeRowsMode::DisplayedCellsExceptHeaders;
    this->referencesDataview->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
    this->referencesDataview->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(4) {
        this->webTowerColumn,
            this->referencesPealCount, this->referencesBellboard, this->referencesRingingWorld
    });
    this->referencesDataview->Dock = System::Windows::Forms::DockStyle::Fill;
    this->referencesDataview->Location = System::Drawing::Point(3, 3);
    this->referencesDataview->Name = L"referencesDataview";
    this->referencesDataview->Size = System::Drawing::Size(799, 318);
    this->referencesDataview->TabIndex = 0;
    this->referencesDataview->CellClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &TowerStatsUI::referencesDataview_CellClick);
    this->referencesDataview->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &TowerStatsUI::referencesDataview_ColumnHeaderMouseClick);
    // 
    // backgroundGenerator
    // 
    this->backgroundGenerator->WorkerReportsProgress = true;
    this->backgroundGenerator->WorkerSupportsCancellation = true;
    this->backgroundGenerator->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &TowerStatsUI::backgroundGenerator_DoWork);
    this->backgroundGenerator->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &TowerStatsUI::backgroundGenerator_ProgressChanged);
    this->backgroundGenerator->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &TowerStatsUI::backgroundGenerator_RunWorkerCompleted);
    // 
    // webTowerColumn
    // 
    this->webTowerColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
    this->webTowerColumn->HeaderText = L"Tower";
    this->webTowerColumn->Name = L"webTowerColumn";
    this->webTowerColumn->Width = 62;
    // 
    // referencesPealCount
    // 
    this->referencesPealCount->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
    this->referencesPealCount->HeaderText = L"Count";
    this->referencesPealCount->Name = L"referencesPealCount";
    this->referencesPealCount->ReadOnly = true;
    this->referencesPealCount->Width = 60;
    // 
    // referencesBellboard
    // 
    this->referencesBellboard->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
    this->referencesBellboard->HeaderText = L"Bell board";
    this->referencesBellboard->Name = L"referencesBellboard";
    this->referencesBellboard->ReadOnly = true;
    this->referencesBellboard->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
    this->referencesBellboard->Width = 79;
    // 
    // referencesRingingWorld
    // 
    this->referencesRingingWorld->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
    this->referencesRingingWorld->HeaderText = L"RW";
    this->referencesRingingWorld->Name = L"referencesRingingWorld";
    this->referencesRingingWorld->ReadOnly = true;
    this->referencesRingingWorld->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
    this->referencesRingingWorld->Width = 51;
    // 
    // TowerStatsUI
    // 
    this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
    this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
    this->ClientSize = System::Drawing::Size(813, 397);
    this->Controls->Add(this->tabControl);
    this->Controls->Add(toolStrip1);
    this->Controls->Add(statusStrip1);
    this->Name = L"TowerStatsUI";
    this->Text = L"Towers";
    this->Load += gcnew System::EventHandler(this, &TowerStatsUI::TowerStatsUI_Load);
    statusStrip1->ResumeLayout(false);
    statusStrip1->PerformLayout();
    towersPage->ResumeLayout(false);
    towersLayoutPanel->ResumeLayout(false);
    towersLayoutPanel->PerformLayout();
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->towerData))->EndInit();
    townsPage->ResumeLayout(false);
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->townsData))->EndInit();
    countiesPage->ResumeLayout(false);
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->countiesData))->EndInit();
    toolStrip1->ResumeLayout(false);
    toolStrip1->PerformLayout();
    tenorsPage->ResumeLayout(false);
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->tenorsData))->EndInit();
    this->tabControl->ResumeLayout(false);
    this->referencesPage->ResumeLayout(false);
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->referencesDataview))->EndInit();
    this->ResumeLayout(false);
    this->PerformLayout();
}
