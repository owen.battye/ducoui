#pragma once

namespace DucoUI {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Windows::Forms;

    public ref class AssociationSearchUI : public System::Windows::Forms::Form,
                                           public DucoUI::ProgressCallbackUI
	{
	public:
        AssociationSearchUI(DucoUI::DatabaseManager^ theDatabase);

        // from ProgressCallbackUI
        virtual System::Void Initialised();
        virtual System::Void Step(int progressPercent);
        virtual System::Void Complete();
    
    protected:
        ~AssociationSearchUI();
        !AssociationSearchUI();
        System::Void AssociationSearchUI_FormClosing(System::Object^ sender, System::Windows::Forms::FormClosingEventArgs^ e);
        System::Boolean CreateSearchParameters(Duco::DatabaseSearch& search);
        System::Void PopulateResults(System::ComponentModel::BackgroundWorker^ bgworker);

    private:
        System::Void clearBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void searchBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void closeBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void AssociationSearchUI_Load(System::Object^ sender, System::EventArgs^ e);
        System::Void dataGridView_CellDoubleClick(System::Object^ sender, System::Windows::Forms::DataGridViewCellEventArgs^ e);

        System::Void backgroundWorker1_DoWork(System::Object^ sender, System::ComponentModel::DoWorkEventArgs^ e);
        System::Void backgroundWorker1_ProgressChanged(System::Object^ sender, System::ComponentModel::ProgressChangedEventArgs^ e);
        System::Void backgroundWorker1_RunWorkerCompleted(System::Object^ sender, System::ComponentModel::RunWorkerCompletedEventArgs^ e);

    private:
        DucoUI::ProgressCallbackWrapper* progressWrapper;
        std::set<Duco::ObjectId>* foundAssociationIds;
    private: System::Windows::Forms::ProgressBar^ progressBar;
    private: System::Windows::Forms::TextBox^ results;
    private: System::Windows::Forms::Button^ clearBtn;
    private: System::Windows::Forms::Button^ searchBtn;
    private: System::Windows::Forms::Button^ closeBtn;
    private: System::Windows::Forms::TextBox^ associationName;
    private: System::Windows::Forms::ComboBox^ linkedAssociation;
    private: System::Windows::Forms::DataGridView^ dataGridView1;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container^ components;
        DucoUI::DatabaseManager^ database;
    private: System::ComponentModel::BackgroundWorker^ backgroundWorker1;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ NameColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ TotalColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ FirstPerformance;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ LastPerformance;
          System::Collections::Generic::List<System::Int16>^ allAssociationIds;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
            System::Windows::Forms::Label^ resultsLbl;
            System::Windows::Forms::Label^ nameLbl;
            System::Windows::Forms::Label^ linkedLbl;
            this->progressBar = (gcnew System::Windows::Forms::ProgressBar());
            this->results = (gcnew System::Windows::Forms::TextBox());
            this->clearBtn = (gcnew System::Windows::Forms::Button());
            this->searchBtn = (gcnew System::Windows::Forms::Button());
            this->closeBtn = (gcnew System::Windows::Forms::Button());
            this->associationName = (gcnew System::Windows::Forms::TextBox());
            this->linkedAssociation = (gcnew System::Windows::Forms::ComboBox());
            this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
            this->NameColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->TotalColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->FirstPerformance = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->LastPerformance = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->backgroundWorker1 = (gcnew System::ComponentModel::BackgroundWorker());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            resultsLbl = (gcnew System::Windows::Forms::Label());
            nameLbl = (gcnew System::Windows::Forms::Label());
            linkedLbl = (gcnew System::Windows::Forms::Label());
            tableLayoutPanel1->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
            this->SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 6;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->Controls->Add(this->progressBar, 0, 3);
            tableLayoutPanel1->Controls->Add(resultsLbl, 1, 3);
            tableLayoutPanel1->Controls->Add(this->results, 2, 3);
            tableLayoutPanel1->Controls->Add(this->clearBtn, 3, 3);
            tableLayoutPanel1->Controls->Add(this->searchBtn, 4, 3);
            tableLayoutPanel1->Controls->Add(this->closeBtn, 5, 3);
            tableLayoutPanel1->Controls->Add(nameLbl, 0, 0);
            tableLayoutPanel1->Controls->Add(linkedLbl, 0, 1);
            tableLayoutPanel1->Controls->Add(this->associationName, 1, 0);
            tableLayoutPanel1->Controls->Add(this->linkedAssociation, 1, 1);
            tableLayoutPanel1->Controls->Add(this->dataGridView1, 0, 2);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 4;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->Size = System::Drawing::Size(642, 243);
            tableLayoutPanel1->TabIndex = 0;
            // 
            // progressBar
            // 
            this->progressBar->Location = System::Drawing::Point(3, 219);
            this->progressBar->Margin = System::Windows::Forms::Padding(3, 5, 3, 3);
            this->progressBar->Name = L"progressBar";
            this->progressBar->Size = System::Drawing::Size(100, 17);
            this->progressBar->TabIndex = 0;
            // 
            // resultsLbl
            // 
            resultsLbl->AutoSize = true;
            resultsLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            resultsLbl->Location = System::Drawing::Point(109, 217);
            resultsLbl->Margin = System::Windows::Forms::Padding(3);
            resultsLbl->Name = L"resultsLbl";
            resultsLbl->Size = System::Drawing::Size(45, 23);
            resultsLbl->TabIndex = 1;
            resultsLbl->Text = L"Results:";
            resultsLbl->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
            // 
            // results
            // 
            this->results->Location = System::Drawing::Point(160, 218);
            this->results->Margin = System::Windows::Forms::Padding(3, 4, 3, 3);
            this->results->Name = L"results";
            this->results->ReadOnly = true;
            this->results->Size = System::Drawing::Size(36, 20);
            this->results->TabIndex = 2;
            // 
            // clearBtn
            // 
            this->clearBtn->Location = System::Drawing::Point(402, 217);
            this->clearBtn->Name = L"clearBtn";
            this->clearBtn->Size = System::Drawing::Size(75, 23);
            this->clearBtn->TabIndex = 3;
            this->clearBtn->Text = L"Clear";
            this->clearBtn->UseVisualStyleBackColor = true;
            this->clearBtn->Click += gcnew System::EventHandler(this, &AssociationSearchUI::clearBtn_Click);
            // 
            // searchBtn
            // 
            this->searchBtn->Location = System::Drawing::Point(483, 217);
            this->searchBtn->Name = L"searchBtn";
            this->searchBtn->Size = System::Drawing::Size(75, 23);
            this->searchBtn->TabIndex = 4;
            this->searchBtn->Text = L"Search";
            this->searchBtn->UseVisualStyleBackColor = true;
            this->searchBtn->Click += gcnew System::EventHandler(this, &AssociationSearchUI::searchBtn_Click);
            // 
            // closeBtn
            // 
            this->closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            this->closeBtn->Location = System::Drawing::Point(564, 217);
            this->closeBtn->Name = L"closeBtn";
            this->closeBtn->Size = System::Drawing::Size(75, 23);
            this->closeBtn->TabIndex = 5;
            this->closeBtn->Text = L"Close";
            this->closeBtn->UseVisualStyleBackColor = true;
            this->closeBtn->Click += gcnew System::EventHandler(this, &AssociationSearchUI::closeBtn_Click);
            // 
            // nameLbl
            // 
            nameLbl->AutoSize = true;
            nameLbl->Dock = System::Windows::Forms::DockStyle::Right;
            nameLbl->Location = System::Drawing::Point(65, 3);
            nameLbl->Margin = System::Windows::Forms::Padding(5, 3, 3, 3);
            nameLbl->Name = L"nameLbl";
            nameLbl->Size = System::Drawing::Size(38, 20);
            nameLbl->TabIndex = 6;
            nameLbl->Text = L"Name:";
            // 
            // linkedLbl
            // 
            linkedLbl->AutoSize = true;
            linkedLbl->Dock = System::Windows::Forms::DockStyle::Right;
            linkedLbl->Location = System::Drawing::Point(49, 31);
            linkedLbl->Margin = System::Windows::Forms::Padding(3, 5, 3, 3);
            linkedLbl->Name = L"linkedLbl";
            linkedLbl->Size = System::Drawing::Size(54, 19);
            linkedLbl->TabIndex = 7;
            linkedLbl->Text = L"Linked to:";
            // 
            // associationName
            // 
            tableLayoutPanel1->SetColumnSpan(this->associationName, 2);
            this->associationName->Dock = System::Windows::Forms::DockStyle::Fill;
            this->associationName->Location = System::Drawing::Point(109, 3);
            this->associationName->Name = L"associationName";
            this->associationName->Size = System::Drawing::Size(287, 20);
            this->associationName->TabIndex = 8;
            // 
            // linkedAssociation
            // 
            tableLayoutPanel1->SetColumnSpan(this->linkedAssociation, 2);
            this->linkedAssociation->Dock = System::Windows::Forms::DockStyle::Fill;
            this->linkedAssociation->FormattingEnabled = true;
            this->linkedAssociation->Location = System::Drawing::Point(109, 29);
            this->linkedAssociation->Name = L"linkedAssociation";
            this->linkedAssociation->Size = System::Drawing::Size(287, 21);
            this->linkedAssociation->TabIndex = 9;
            // 
            // dataGridView1
            // 
            this->dataGridView1->AllowUserToAddRows = false;
            this->dataGridView1->AllowUserToDeleteRows = false;
            this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->dataGridView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(4) {
                this->NameColumn,
                    this->TotalColumn, this->FirstPerformance, this->LastPerformance
            });
            tableLayoutPanel1->SetColumnSpan(this->dataGridView1, 6);
            this->dataGridView1->Dock = System::Windows::Forms::DockStyle::Fill;
            this->dataGridView1->Location = System::Drawing::Point(3, 56);
            this->dataGridView1->Name = L"dataGridView1";
            this->dataGridView1->ReadOnly = true;
            this->dataGridView1->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToDisplayedHeaders;
            this->dataGridView1->Size = System::Drawing::Size(636, 155);
            this->dataGridView1->TabIndex = 10;
            this->dataGridView1->CellDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &AssociationSearchUI::dataGridView_CellDoubleClick);
            // 
            // NameColumn
            // 
            this->NameColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->NameColumn->HeaderText = L"Name";
            this->NameColumn->Name = L"NameColumn";
            this->NameColumn->ReadOnly = true;
            this->NameColumn->Width = 60;
            // 
            // TotalColumn
            // 
            this->TotalColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->TotalColumn->HeaderText = L"Total";
            this->TotalColumn->Name = L"TotalColumn";
            this->TotalColumn->ReadOnly = true;
            this->TotalColumn->Width = 56;
            // 
            // FirstPerformance
            // 
            this->FirstPerformance->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->FirstPerformance->HeaderText = L"First performance";
            this->FirstPerformance->Name = L"FirstPerformance";
            this->FirstPerformance->ReadOnly = true;
            this->FirstPerformance->Width = 104;
            // 
            // LastPerformance
            // 
            this->LastPerformance->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->LastPerformance->HeaderText = L"Last performance";
            this->LastPerformance->Name = L"LastPerformance";
            this->LastPerformance->ReadOnly = true;
            this->LastPerformance->Width = 105;
            // 
            // backgroundWorker1
            // 
            this->backgroundWorker1->WorkerReportsProgress = true;
            this->backgroundWorker1->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &AssociationSearchUI::backgroundWorker1_DoWork);
            this->backgroundWorker1->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &AssociationSearchUI::backgroundWorker1_ProgressChanged);
            this->backgroundWorker1->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &AssociationSearchUI::backgroundWorker1_RunWorkerCompleted);
            // 
            // AssociationSearchUI
            // 
            this->AcceptButton = this->searchBtn;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->CancelButton = this->closeBtn;
            this->ClientSize = System::Drawing::Size(642, 243);
            this->Controls->Add(tableLayoutPanel1);
            this->Name = L"AssociationSearchUI";
            this->Text = L"Search associations";
            this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &AssociationSearchUI::AssociationSearchUI_FormClosing);
            this->Load += gcnew System::EventHandler(this, &AssociationSearchUI::AssociationSearchUI_Load);
            tableLayoutPanel1->ResumeLayout(false);
            tableLayoutPanel1->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
            this->ResumeLayout(false);

        }
#pragma endregion
    };
}
