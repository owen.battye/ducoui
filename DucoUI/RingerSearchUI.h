#pragma once
namespace DucoUI
{
    public ref class RingerSearchUI : public System::Windows::Forms::Form,
                                      public DucoUI::ProgressCallbackUI
    {
    public:
        RingerSearchUI(DucoUI::DatabaseManager^ newDatabase, bool setStartSearchInvalid);

        // from ProgressCallbackUI
        virtual void Initialised();
        virtual void Step(int progressPercent);
        virtual void Complete();

        void SetSimilarRingerId(const Duco::ObjectId& ringerId);

    protected:
        ~RingerSearchUI();
        !RingerSearchUI();
        System::Void RingerSearchUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void ClosingEvent( Object^ /*sender*/, System::ComponentModel::CancelEventArgs^ e );
        System::Void ringersData_CellValueNeeded(System::Object^  sender, System::Windows::Forms::DataGridViewCellValueEventArgs^  e);

        System::Boolean CreateSearchParameters(Duco::DatabaseSearch& search);
        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void searchBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void clearBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void printBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void printTowers(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args);

        System::Void ringersData_CellDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);
        System::Void ringersData_Scroll(System::Object^  sender, System::Windows::Forms::ScrollEventArgs^  e);
        System::Void RingerSearchUI_Scroll(System::Object^  sender, System::Windows::Forms::ScrollEventArgs^  e);

        System::Void MenuStrip_Opening(System::Object^ sender, System::ComponentModel::CancelEventArgs^ e);
        System::Void SetSelectionFemale(System::Object^  sender, System::EventArgs^  e);
        System::Void SetSelectionMale(System::Object^  sender, System::EventArgs^  e);
        System::Void LinkRingers(System::Object^ sender, System::EventArgs^ e);
        System::Void SwapRingers(System::Object^ sender, System::EventArgs^ e);
        System::Void copyFromTableCmd_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void showAll_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void allReferencesBlank_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void nonBlankReferences_CheckedChanged(System::Object^  sender, System::EventArgs^  e);

        System::Void backgroundWorker_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void backgroundWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void backgroundWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);
        System::Void similarRinger_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void invalid_CheckedChanged(System::Object ^ sender, System::EventArgs ^ e);


    private:
        System::Windows::Forms::Button^                     searchBtn;
        System::Windows::Forms::Button^                     clearBtn;
        System::Boolean                                     errorsFound;
        System::ComponentModel::IContainer^                 components;
        System::Windows::Forms::ProgressBar^                progressBar;
        System::Windows::Forms::TextBox^                    resultCount;
        System::Windows::Forms::TextBox^                    surnameEditor;
        System::Windows::Forms::TextBox^                    forenameEditor;
        System::Windows::Forms::CheckBox^                   withAkas;
        System::Windows::Forms::CheckBox^                   showAll;
        System::Windows::Forms::CheckBox^                   invalid;
        System::Windows::Forms::CheckBox^                   male;
        System::Windows::Forms::CheckBox^                   female;
        System::Windows::Forms::CheckBox^                   nonBlankNotes;
        System::Windows::Forms::CheckBox^                   allReferencesBlank;
        System::Windows::Forms::CheckBox^                   nonBlankReferences;
        System::Windows::Forms::TextBox^                    notesEditor;
        System::Windows::Forms::TextBox^                    referencesEditor;
        System::Windows::Forms::DataGridView^               ringersData;
        DucoUI::DatabaseManager^                            database;
        System::Collections::Generic::List<System::UInt64>^ foundRingerIds;
        System::Boolean                                     startSearchInvalid;
        DucoUI::ProgressCallbackWrapper*		            progressWrapper;
        System::ComponentModel::BackgroundWorker^           backgroundWorker;
        DucoUI::RingerDisplayCache^                         ringerCache;
        DucoUI::GenericTablePrintUtil^                      printUtil;

    private: System::Windows::Forms::CheckBox^ nonHuman;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ forenameColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ surnameColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ CurrentnameColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ FirstPealColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ LastPeal;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ PealCount;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ genderColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ invalidColumn;
    private: System::Windows::Forms::ToolStripMenuItem^ linkRingersMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^ swapRingersMenuItem;
    private: System::Windows::Forms::CheckBox^ links;
    private: System::Windows::Forms::ComboBox^ similarRinger;

    protected: void InitializeComponent()
        {
        this->components = (gcnew System::ComponentModel::Container());
        System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
        System::Windows::Forms::TabControl^ tabControl1;
        System::Windows::Forms::TabPage^ namePage;
        System::Windows::Forms::Label^ surnameLbl;
        System::Windows::Forms::Label^ forenameLbl;
        System::Windows::Forms::TabPage^ referencesPage;
        System::Windows::Forms::Label^ referencesLbl;
        System::Windows::Forms::TabPage^ similarPage;
        System::Windows::Forms::Label^ similarRingerLbl;
        System::Windows::Forms::TabPage^ otherPage;
        System::Windows::Forms::GroupBox^ genderGroup;
        System::Windows::Forms::GroupBox^ notesGroup;
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle1 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::ContextMenuStrip^ genderContextMenu;
        System::Windows::Forms::ToolStripMenuItem^ setSelectedAsMale;
        System::Windows::Forms::ToolStripMenuItem^ setSelectedAsFemale;
        System::Windows::Forms::ToolStripSeparator^ dataContextMenuSeperator1;
        System::Windows::Forms::ToolStripSeparator^ dataContextMenuSeperator2;
        System::Windows::Forms::ToolStripMenuItem^ copyFromTableCmd;
        System::Windows::Forms::Panel^ btnPanel;
        System::Windows::Forms::Button^ printBtn;
        System::Windows::Forms::Label^ resultsLbl;
        System::Windows::Forms::Button^ closeBtn;
        this->links = (gcnew System::Windows::Forms::CheckBox());
        this->surnameEditor = (gcnew System::Windows::Forms::TextBox());
        this->forenameEditor = (gcnew System::Windows::Forms::TextBox());
        this->withAkas = (gcnew System::Windows::Forms::CheckBox());
        this->referencesEditor = (gcnew System::Windows::Forms::TextBox());
        this->allReferencesBlank = (gcnew System::Windows::Forms::CheckBox());
        this->nonBlankReferences = (gcnew System::Windows::Forms::CheckBox());
        this->similarRinger = (gcnew System::Windows::Forms::ComboBox());
        this->nonHuman = (gcnew System::Windows::Forms::CheckBox());
        this->invalid = (gcnew System::Windows::Forms::CheckBox());
        this->male = (gcnew System::Windows::Forms::CheckBox());
        this->female = (gcnew System::Windows::Forms::CheckBox());
        this->notesEditor = (gcnew System::Windows::Forms::TextBox());
        this->nonBlankNotes = (gcnew System::Windows::Forms::CheckBox());
        this->showAll = (gcnew System::Windows::Forms::CheckBox());
        this->ringersData = (gcnew System::Windows::Forms::DataGridView());
        this->forenameColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->surnameColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->CurrentnameColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->FirstPealColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->LastPeal = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->PealCount = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->genderColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->invalidColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->linkRingersMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
        this->swapRingersMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
        this->clearBtn = (gcnew System::Windows::Forms::Button());
        this->progressBar = (gcnew System::Windows::Forms::ProgressBar());
        this->resultCount = (gcnew System::Windows::Forms::TextBox());
        this->searchBtn = (gcnew System::Windows::Forms::Button());
        this->backgroundWorker = (gcnew System::ComponentModel::BackgroundWorker());
        tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
        tabControl1 = (gcnew System::Windows::Forms::TabControl());
        namePage = (gcnew System::Windows::Forms::TabPage());
        surnameLbl = (gcnew System::Windows::Forms::Label());
        forenameLbl = (gcnew System::Windows::Forms::Label());
        referencesPage = (gcnew System::Windows::Forms::TabPage());
        referencesLbl = (gcnew System::Windows::Forms::Label());
        similarPage = (gcnew System::Windows::Forms::TabPage());
        similarRingerLbl = (gcnew System::Windows::Forms::Label());
        otherPage = (gcnew System::Windows::Forms::TabPage());
        genderGroup = (gcnew System::Windows::Forms::GroupBox());
        notesGroup = (gcnew System::Windows::Forms::GroupBox());
        genderContextMenu = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
        setSelectedAsMale = (gcnew System::Windows::Forms::ToolStripMenuItem());
        setSelectedAsFemale = (gcnew System::Windows::Forms::ToolStripMenuItem());
        dataContextMenuSeperator1 = (gcnew System::Windows::Forms::ToolStripSeparator());
        dataContextMenuSeperator2 = (gcnew System::Windows::Forms::ToolStripSeparator());
        copyFromTableCmd = (gcnew System::Windows::Forms::ToolStripMenuItem());
        btnPanel = (gcnew System::Windows::Forms::Panel());
        printBtn = (gcnew System::Windows::Forms::Button());
        resultsLbl = (gcnew System::Windows::Forms::Label());
        closeBtn = (gcnew System::Windows::Forms::Button());
        tableLayoutPanel1->SuspendLayout();
        tabControl1->SuspendLayout();
        namePage->SuspendLayout();
        referencesPage->SuspendLayout();
        similarPage->SuspendLayout();
        otherPage->SuspendLayout();
        genderGroup->SuspendLayout();
        notesGroup->SuspendLayout();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ringersData))->BeginInit();
        genderContextMenu->SuspendLayout();
        btnPanel->SuspendLayout();
        this->SuspendLayout();
        // 
        // tableLayoutPanel1
        // 
        tableLayoutPanel1->ColumnCount = 1;
        tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
        tableLayoutPanel1->Controls->Add(tabControl1, 0, 0);
        tableLayoutPanel1->Controls->Add(this->ringersData, 0, 1);
        tableLayoutPanel1->Controls->Add(btnPanel, 0, 2);
        tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
        tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
        tableLayoutPanel1->Name = L"tableLayoutPanel1";
        tableLayoutPanel1->RowCount = 3;
        tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 104)));
        tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
        tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 35)));
        tableLayoutPanel1->Size = System::Drawing::Size(613, 248);
        tableLayoutPanel1->TabIndex = 0;
        // 
        // tabControl1
        // 
        tabControl1->Controls->Add(namePage);
        tabControl1->Controls->Add(referencesPage);
        tabControl1->Controls->Add(similarPage);
        tabControl1->Controls->Add(otherPage);
        tabControl1->Dock = System::Windows::Forms::DockStyle::Fill;
        tabControl1->Location = System::Drawing::Point(3, 3);
        tabControl1->Name = L"tabControl1";
        tabControl1->SelectedIndex = 0;
        tabControl1->Size = System::Drawing::Size(607, 98);
        tabControl1->TabIndex = 0;
        // 
        // namePage
        // 
        namePage->Controls->Add(this->links);
        namePage->Controls->Add(surnameLbl);
        namePage->Controls->Add(this->surnameEditor);
        namePage->Controls->Add(forenameLbl);
        namePage->Controls->Add(this->forenameEditor);
        namePage->Controls->Add(this->withAkas);
        namePage->Location = System::Drawing::Point(4, 22);
        namePage->Name = L"namePage";
        namePage->Padding = System::Windows::Forms::Padding(3);
        namePage->Size = System::Drawing::Size(599, 72);
        namePage->TabIndex = 0;
        namePage->Text = L"Names";
        namePage->UseVisualStyleBackColor = true;
        // 
        // links
        // 
        this->links->AutoSize = true;
        this->links->Location = System::Drawing::Point(190, 47);
        this->links->Name = L"links";
        this->links->Size = System::Drawing::Size(131, 17);
        this->links->TabIndex = 5;
        this->links->Text = L"Linked to other ringers";
        this->links->UseVisualStyleBackColor = true;
        // 
        // surnameLbl
        // 
        surnameLbl->AutoSize = true;
        surnameLbl->Location = System::Drawing::Point(187, 4);
        surnameLbl->Name = L"surnameLbl";
        surnameLbl->Size = System::Drawing::Size(49, 13);
        surnameLbl->TabIndex = 1;
        surnameLbl->Text = L"Surname";
        // 
        // surnameEditor
        // 
        this->surnameEditor->Location = System::Drawing::Point(190, 21);
        this->surnameEditor->Name = L"surnameEditor";
        this->surnameEditor->Size = System::Drawing::Size(180, 20);
        this->surnameEditor->TabIndex = 3;
        // 
        // forenameLbl
        // 
        forenameLbl->AutoSize = true;
        forenameLbl->Location = System::Drawing::Point(4, 4);
        forenameLbl->Name = L"forenameLbl";
        forenameLbl->Size = System::Drawing::Size(54, 13);
        forenameLbl->TabIndex = 0;
        forenameLbl->Text = L"Forename";
        // 
        // forenameEditor
        // 
        this->forenameEditor->Location = System::Drawing::Point(4, 21);
        this->forenameEditor->Name = L"forenameEditor";
        this->forenameEditor->Size = System::Drawing::Size(180, 20);
        this->forenameEditor->TabIndex = 2;
        // 
        // withAkas
        // 
        this->withAkas->AutoSize = true;
        this->withAkas->Location = System::Drawing::Point(4, 48);
        this->withAkas->Name = L"withAkas";
        this->withAkas->Size = System::Drawing::Size(75, 17);
        this->withAkas->TabIndex = 4;
        this->withAkas->Text = L"With Akas";
        this->withAkas->UseVisualStyleBackColor = true;
        // 
        // referencesPage
        // 
        referencesPage->Controls->Add(referencesLbl);
        referencesPage->Controls->Add(this->referencesEditor);
        referencesPage->Controls->Add(this->allReferencesBlank);
        referencesPage->Controls->Add(this->nonBlankReferences);
        referencesPage->Location = System::Drawing::Point(4, 22);
        referencesPage->Name = L"referencesPage";
        referencesPage->Padding = System::Windows::Forms::Padding(3);
        referencesPage->Size = System::Drawing::Size(599, 72);
        referencesPage->TabIndex = 1;
        referencesPage->Text = L"References";
        referencesPage->UseVisualStyleBackColor = true;
        // 
        // referencesLbl
        // 
        referencesLbl->AutoSize = true;
        referencesLbl->Location = System::Drawing::Point(6, 3);
        referencesLbl->Name = L"referencesLbl";
        referencesLbl->Size = System::Drawing::Size(57, 13);
        referencesLbl->TabIndex = 0;
        referencesLbl->Text = L"Reference";
        // 
        // referencesEditor
        // 
        this->referencesEditor->Location = System::Drawing::Point(9, 19);
        this->referencesEditor->Name = L"referencesEditor";
        this->referencesEditor->Size = System::Drawing::Size(180, 20);
        this->referencesEditor->TabIndex = 1;
        // 
        // allReferencesBlank
        // 
        this->allReferencesBlank->AutoSize = true;
        this->allReferencesBlank->Location = System::Drawing::Point(9, 45);
        this->allReferencesBlank->Name = L"allReferencesBlank";
        this->allReferencesBlank->Size = System::Drawing::Size(119, 17);
        this->allReferencesBlank->TabIndex = 2;
        this->allReferencesBlank->Text = L"All references blank";
        this->allReferencesBlank->UseVisualStyleBackColor = true;
        this->allReferencesBlank->CheckedChanged += gcnew System::EventHandler(this, &RingerSearchUI::allReferencesBlank_CheckedChanged);
        // 
        // nonBlankReferences
        // 
        this->nonBlankReferences->AutoSize = true;
        this->nonBlankReferences->Location = System::Drawing::Point(134, 45);
        this->nonBlankReferences->Name = L"nonBlankReferences";
        this->nonBlankReferences->Size = System::Drawing::Size(142, 17);
        this->nonBlankReferences->TabIndex = 3;
        this->nonBlankReferences->Text = L"Any reference non blank";
        this->nonBlankReferences->UseVisualStyleBackColor = true;
        this->nonBlankReferences->CheckedChanged += gcnew System::EventHandler(this, &RingerSearchUI::nonBlankReferences_CheckedChanged);
        // 
        // similarPage
        // 
        similarPage->Controls->Add(similarRingerLbl);
        similarPage->Controls->Add(this->similarRinger);
        similarPage->Location = System::Drawing::Point(4, 22);
        similarPage->Name = L"similarPage";
        similarPage->Padding = System::Windows::Forms::Padding(3);
        similarPage->Size = System::Drawing::Size(599, 72);
        similarPage->TabIndex = 3;
        similarPage->Text = L"Similar";
        similarPage->UseVisualStyleBackColor = true;
        // 
        // similarRingerLbl
        // 
        similarRingerLbl->AutoSize = true;
        similarRingerLbl->Location = System::Drawing::Point(6, 3);
        similarRingerLbl->Name = L"similarRingerLbl";
        similarRingerLbl->Size = System::Drawing::Size(38, 13);
        similarRingerLbl->TabIndex = 1;
        similarRingerLbl->Text = L"Ringer";
        // 
        // similarRinger
        // 
        this->similarRinger->FormattingEnabled = true;
        this->similarRinger->Location = System::Drawing::Point(9, 19);
        this->similarRinger->Name = L"similarRinger";
        this->similarRinger->Size = System::Drawing::Size(354, 21);
        this->similarRinger->TabIndex = 0;
        this->similarRinger->SelectedIndexChanged += gcnew System::EventHandler(this, &RingerSearchUI::similarRinger_SelectedIndexChanged);
        // 
        // otherPage
        // 
        otherPage->Controls->Add(this->nonHuman);
        otherPage->Controls->Add(this->invalid);
        otherPage->Controls->Add(genderGroup);
        otherPage->Controls->Add(notesGroup);
        otherPage->Controls->Add(this->showAll);
        otherPage->Location = System::Drawing::Point(4, 22);
        otherPage->Name = L"otherPage";
        otherPage->Padding = System::Windows::Forms::Padding(3);
        otherPage->Size = System::Drawing::Size(599, 72);
        otherPage->TabIndex = 2;
        otherPage->Text = L"Other";
        otherPage->UseVisualStyleBackColor = true;
        // 
        // nonHuman
        // 
        this->nonHuman->AutoSize = true;
        this->nonHuman->Location = System::Drawing::Point(377, 10);
        this->nonHuman->Name = L"nonHuman";
        this->nonHuman->Size = System::Drawing::Size(140, 17);
        this->nonHuman->TabIndex = 4;
        this->nonHuman->Text = L"Non human or computer";
        this->nonHuman->UseVisualStyleBackColor = true;
        // 
        // invalid
        // 
        this->invalid->AutoSize = true;
        this->invalid->Location = System::Drawing::Point(280, 33);
        this->invalid->Name = L"invalid";
        this->invalid->Size = System::Drawing::Size(91, 17);
        this->invalid->TabIndex = 3;
        this->invalid->Text = L"Contains error";
        this->invalid->UseVisualStyleBackColor = true;
        this->invalid->CheckedChanged += gcnew System::EventHandler(this, &RingerSearchUI::invalid_CheckedChanged);
        // 
        // genderGroup
        // 
        genderGroup->Controls->Add(this->male);
        genderGroup->Controls->Add(this->female);
        genderGroup->Location = System::Drawing::Point(200, 5);
        genderGroup->Name = L"genderGroup";
        genderGroup->Size = System::Drawing::Size(65, 60);
        genderGroup->TabIndex = 1;
        genderGroup->TabStop = false;
        genderGroup->Text = L"Gender";
        // 
        // male
        // 
        this->male->AutoSize = true;
        this->male->Location = System::Drawing::Point(5, 15);
        this->male->Name = L"male";
        this->male->Size = System::Drawing::Size(49, 17);
        this->male->TabIndex = 0;
        this->male->Text = L"Male";
        this->male->UseVisualStyleBackColor = true;
        // 
        // female
        // 
        this->female->AutoSize = true;
        this->female->Location = System::Drawing::Point(5, 38);
        this->female->Name = L"female";
        this->female->Size = System::Drawing::Size(60, 17);
        this->female->TabIndex = 1;
        this->female->Text = L"Female";
        this->female->UseVisualStyleBackColor = true;
        // 
        // notesGroup
        // 
        notesGroup->Controls->Add(this->notesEditor);
        notesGroup->Controls->Add(this->nonBlankNotes);
        notesGroup->Location = System::Drawing::Point(5, 5);
        notesGroup->Name = L"notesGroup";
        notesGroup->Size = System::Drawing::Size(190, 60);
        notesGroup->TabIndex = 0;
        notesGroup->TabStop = false;
        notesGroup->Text = L"Notes";
        // 
        // notesEditor
        // 
        this->notesEditor->Location = System::Drawing::Point(5, 18);
        this->notesEditor->Name = L"notesEditor";
        this->notesEditor->Size = System::Drawing::Size(180, 20);
        this->notesEditor->TabIndex = 0;
        // 
        // nonBlankNotes
        // 
        this->nonBlankNotes->AutoSize = true;
        this->nonBlankNotes->Location = System::Drawing::Point(5, 40);
        this->nonBlankNotes->Name = L"nonBlankNotes";
        this->nonBlankNotes->Size = System::Drawing::Size(75, 17);
        this->nonBlankNotes->TabIndex = 1;
        this->nonBlankNotes->Text = L"Non blank";
        this->nonBlankNotes->UseVisualStyleBackColor = true;
        // 
        // showAll
        // 
        this->showAll->AutoSize = true;
        this->showAll->Location = System::Drawing::Point(280, 10);
        this->showAll->Name = L"showAll";
        this->showAll->Size = System::Drawing::Size(66, 17);
        this->showAll->TabIndex = 2;
        this->showAll->Text = L"Show all";
        this->showAll->UseVisualStyleBackColor = true;
        this->showAll->CheckedChanged += gcnew System::EventHandler(this, &RingerSearchUI::showAll_CheckedChanged);
        // 
        // ringersData
        // 
        this->ringersData->AllowUserToAddRows = false;
        this->ringersData->AllowUserToDeleteRows = false;
        this->ringersData->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::DisplayedCells;
        this->ringersData->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
        this->ringersData->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(8) {
            this->forenameColumn,
                this->surnameColumn, this->CurrentnameColumn, this->FirstPealColumn, this->LastPeal, this->PealCount, this->genderColumn, this->invalidColumn
        });
        this->ringersData->ContextMenuStrip = genderContextMenu;
        this->ringersData->Dock = System::Windows::Forms::DockStyle::Fill;
        this->ringersData->Location = System::Drawing::Point(3, 107);
        this->ringersData->Name = L"ringersData";
        this->ringersData->ReadOnly = true;
        this->ringersData->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToDisplayedHeaders;
        this->ringersData->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
        this->ringersData->Size = System::Drawing::Size(607, 103);
        this->ringersData->TabIndex = 1;
        this->ringersData->VirtualMode = true;
        this->ringersData->CellDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &RingerSearchUI::ringersData_CellDoubleClick);
        this->ringersData->CellValueNeeded += gcnew System::Windows::Forms::DataGridViewCellValueEventHandler(this, &RingerSearchUI::ringersData_CellValueNeeded);
        this->ringersData->Scroll += gcnew System::Windows::Forms::ScrollEventHandler(this, &RingerSearchUI::ringersData_Scroll);
        // 
        // forenameColumn
        // 
        this->forenameColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
        this->forenameColumn->HeaderText = L"Forename";
        this->forenameColumn->Name = L"forenameColumn";
        this->forenameColumn->ReadOnly = true;
        this->forenameColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->forenameColumn->Width = 60;
        // 
        // surnameColumn
        // 
        this->surnameColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
        this->surnameColumn->HeaderText = L"Surname";
        this->surnameColumn->Name = L"surnameColumn";
        this->surnameColumn->ReadOnly = true;
        this->surnameColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->surnameColumn->Width = 55;
        // 
        // CurrentnameColumn
        // 
        this->CurrentnameColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
        this->CurrentnameColumn->HeaderText = L"Current name";
        this->CurrentnameColumn->Name = L"CurrentnameColumn";
        this->CurrentnameColumn->ReadOnly = true;
        this->CurrentnameColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->CurrentnameColumn->Width = 76;
        // 
        // FirstPealColumn
        // 
        this->FirstPealColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
        this->FirstPealColumn->HeaderText = L"First Peal";
        this->FirstPealColumn->Name = L"FirstPealColumn";
        this->FirstPealColumn->ReadOnly = true;
        this->FirstPealColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->FirstPealColumn->Width = 56;
        // 
        // LastPeal
        // 
        this->LastPeal->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
        this->LastPeal->HeaderText = L"Last Peal";
        this->LastPeal->Name = L"LastPeal";
        this->LastPeal->ReadOnly = true;
        this->LastPeal->Width = 76;
        // 
        // PealCount
        // 
        this->PealCount->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
        dataGridViewCellStyle1->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
        this->PealCount->DefaultCellStyle = dataGridViewCellStyle1;
        this->PealCount->HeaderText = L"Perfs";
        this->PealCount->MinimumWidth = 30;
        this->PealCount->Name = L"PealCount";
        this->PealCount->ReadOnly = true;
        this->PealCount->Width = 50;
        // 
        // genderColumn
        // 
        this->genderColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::ColumnHeader;
        this->genderColumn->HeaderText = L"Gender";
        this->genderColumn->Name = L"genderColumn";
        this->genderColumn->ReadOnly = true;
        this->genderColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->genderColumn->Width = 48;
        // 
        // invalidColumn
        // 
        this->invalidColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
        this->invalidColumn->HeaderText = L"Error";
        this->invalidColumn->Name = L"invalidColumn";
        this->invalidColumn->ReadOnly = true;
        this->invalidColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->invalidColumn->Width = 35;
        // 
        // genderContextMenu
        // 
        genderContextMenu->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(7) {
            setSelectedAsMale, setSelectedAsFemale,
                dataContextMenuSeperator1, this->linkRingersMenuItem, this->swapRingersMenuItem, dataContextMenuSeperator2, copyFromTableCmd
        });
        genderContextMenu->Name = L"contextMenuStrip1";
        genderContextMenu->Size = System::Drawing::Size(194, 126);
        genderContextMenu->Opening += gcnew System::ComponentModel::CancelEventHandler(this, &RingerSearchUI::MenuStrip_Opening);
        // 
        // setSelectedAsMale
        // 
        setSelectedAsMale->Name = L"setSelectedAsMale";
        setSelectedAsMale->Size = System::Drawing::Size(193, 22);
        setSelectedAsMale->Text = L"Set selection as male";
        setSelectedAsMale->Click += gcnew System::EventHandler(this, &RingerSearchUI::SetSelectionMale);
        // 
        // setSelectedAsFemale
        // 
        setSelectedAsFemale->Name = L"setSelectedAsFemale";
        setSelectedAsFemale->Size = System::Drawing::Size(193, 22);
        setSelectedAsFemale->Text = L"Set selection as female";
        setSelectedAsFemale->Click += gcnew System::EventHandler(this, &RingerSearchUI::SetSelectionFemale);
        // 
        // dataContextMenuSeperator1
        // 
        dataContextMenuSeperator1->Name = L"dataContextMenuSeperator1";
        dataContextMenuSeperator1->Size = System::Drawing::Size(190, 6);
        // 
        // linkRingersMenuItem
        // 
        this->linkRingersMenuItem->Name = L"linkRingersMenuItem";
        this->linkRingersMenuItem->Size = System::Drawing::Size(193, 22);
        this->linkRingersMenuItem->Text = L"Link ringers";
        this->linkRingersMenuItem->Click += gcnew System::EventHandler(this, &RingerSearchUI::LinkRingers);
        // 
        // swapRingersMenuItem
        // 
        this->swapRingersMenuItem->Name = L"swapRingersMenuItem";
        this->swapRingersMenuItem->Size = System::Drawing::Size(193, 22);
        this->swapRingersMenuItem->Text = L"Swap ringers";
        this->swapRingersMenuItem->Click += gcnew System::EventHandler(this, &RingerSearchUI::SwapRingers);
        // 
        // dataContextMenuSeperator2
        // 
        dataContextMenuSeperator2->Name = L"dataContextMenuSeperator2";
        dataContextMenuSeperator2->Size = System::Drawing::Size(190, 6);
        // 
        // copyFromTableCmd
        // 
        copyFromTableCmd->Name = L"copyFromTableCmd";
        copyFromTableCmd->Size = System::Drawing::Size(193, 22);
        copyFromTableCmd->Text = L"Copy";
        copyFromTableCmd->Click += gcnew System::EventHandler(this, &RingerSearchUI::copyFromTableCmd_Click);
        // 
        // btnPanel
        // 
        btnPanel->Controls->Add(printBtn);
        btnPanel->Controls->Add(this->clearBtn);
        btnPanel->Controls->Add(this->progressBar);
        btnPanel->Controls->Add(this->resultCount);
        btnPanel->Controls->Add(resultsLbl);
        btnPanel->Controls->Add(this->searchBtn);
        btnPanel->Controls->Add(closeBtn);
        btnPanel->Dock = System::Windows::Forms::DockStyle::Fill;
        btnPanel->Location = System::Drawing::Point(3, 216);
        btnPanel->Name = L"btnPanel";
        btnPanel->Size = System::Drawing::Size(607, 29);
        btnPanel->TabIndex = 2;
        // 
        // printBtn
        // 
        printBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
        printBtn->Location = System::Drawing::Point(292, 3);
        printBtn->Name = L"printBtn";
        printBtn->Size = System::Drawing::Size(75, 23);
        printBtn->TabIndex = 13;
        printBtn->Text = L"Print";
        printBtn->UseVisualStyleBackColor = true;
        printBtn->Click += gcnew System::EventHandler(this, &RingerSearchUI::printBtn_Click);
        // 
        // clearBtn
        // 
        this->clearBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
        this->clearBtn->Location = System::Drawing::Point(373, 3);
        this->clearBtn->Margin = System::Windows::Forms::Padding(3, 3, 0, 3);
        this->clearBtn->Name = L"clearBtn";
        this->clearBtn->Size = System::Drawing::Size(75, 23);
        this->clearBtn->TabIndex = 2;
        this->clearBtn->Text = L"Clear";
        this->clearBtn->UseVisualStyleBackColor = true;
        this->clearBtn->Click += gcnew System::EventHandler(this, &RingerSearchUI::clearBtn_Click);
        // 
        // progressBar
        // 
        this->progressBar->Location = System::Drawing::Point(3, 5);
        this->progressBar->Name = L"progressBar";
        this->progressBar->Size = System::Drawing::Size(88, 18);
        this->progressBar->Step = 1;
        this->progressBar->TabIndex = 12;
        // 
        // resultCount
        // 
        this->resultCount->Location = System::Drawing::Point(145, 5);
        this->resultCount->Name = L"resultCount";
        this->resultCount->ReadOnly = true;
        this->resultCount->Size = System::Drawing::Size(43, 20);
        this->resultCount->TabIndex = 1;
        // 
        // resultsLbl
        // 
        resultsLbl->AutoSize = true;
        resultsLbl->Location = System::Drawing::Point(97, 8);
        resultsLbl->Name = L"resultsLbl";
        resultsLbl->Size = System::Drawing::Size(42, 13);
        resultsLbl->TabIndex = 0;
        resultsLbl->Text = L"Results";
        // 
        // searchBtn
        // 
        this->searchBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
        this->searchBtn->Location = System::Drawing::Point(448, 3);
        this->searchBtn->Margin = System::Windows::Forms::Padding(0, 3, 3, 3);
        this->searchBtn->Name = L"searchBtn";
        this->searchBtn->Size = System::Drawing::Size(75, 23);
        this->searchBtn->TabIndex = 3;
        this->searchBtn->Text = L"Search";
        this->searchBtn->UseVisualStyleBackColor = true;
        this->searchBtn->Click += gcnew System::EventHandler(this, &RingerSearchUI::searchBtn_Click);
        // 
        // closeBtn
        // 
        closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
        closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
        closeBtn->Location = System::Drawing::Point(529, 3);
        closeBtn->Name = L"closeBtn";
        closeBtn->Size = System::Drawing::Size(75, 23);
        closeBtn->TabIndex = 4;
        closeBtn->Text = L"Close";
        closeBtn->UseVisualStyleBackColor = true;
        closeBtn->Click += gcnew System::EventHandler(this, &RingerSearchUI::closeBtn_Click);
        // 
        // backgroundWorker
        // 
        this->backgroundWorker->WorkerReportsProgress = true;
        this->backgroundWorker->WorkerSupportsCancellation = true;
        this->backgroundWorker->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &RingerSearchUI::backgroundWorker_DoWork);
        this->backgroundWorker->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &RingerSearchUI::backgroundWorker_ProgressChanged);
        this->backgroundWorker->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &RingerSearchUI::backgroundWorker_RunWorkerCompleted);
        // 
        // RingerSearchUI
        // 
        this->AcceptButton = this->searchBtn;
        this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
        this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
        this->CancelButton = closeBtn;
        this->ClientSize = System::Drawing::Size(613, 248);
        this->Controls->Add(tableLayoutPanel1);
        this->MinimumSize = System::Drawing::Size(532, 287);
        this->Name = L"RingerSearchUI";
        this->Text = L"Search ringers";
        this->Closing += gcnew System::ComponentModel::CancelEventHandler(this, &RingerSearchUI::ClosingEvent);
        this->Load += gcnew System::EventHandler(this, &RingerSearchUI::RingerSearchUI_Load);
        this->Scroll += gcnew System::Windows::Forms::ScrollEventHandler(this, &RingerSearchUI::RingerSearchUI_Scroll);
        tableLayoutPanel1->ResumeLayout(false);
        tabControl1->ResumeLayout(false);
        namePage->ResumeLayout(false);
        namePage->PerformLayout();
        referencesPage->ResumeLayout(false);
        referencesPage->PerformLayout();
        similarPage->ResumeLayout(false);
        similarPage->PerformLayout();
        otherPage->ResumeLayout(false);
        otherPage->PerformLayout();
        genderGroup->ResumeLayout(false);
        genderGroup->PerformLayout();
        notesGroup->ResumeLayout(false);
        notesGroup->PerformLayout();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ringersData))->EndInit();
        genderContextMenu->ResumeLayout(false);
        btnPanel->ResumeLayout(false);
        btnPanel->PerformLayout();
        this->ResumeLayout(false);

    }
    };
}
