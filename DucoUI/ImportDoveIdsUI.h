#pragma once
namespace DucoUI
{
    public ref class ImportDoveIdsUI : public System::Windows::Forms::Form, public DucoUI::ProgressCallbackUI
    {
    public:
        ImportDoveIdsUI(DucoUI::DatabaseManager^ theDatabase);

        // from ProgressCallbackUI
        virtual void Initialised();
        virtual void Step(int progressPercent);
        virtual void Complete();

    protected:
        ~ImportDoveIdsUI();
        !ImportDoveIdsUI();
        System::Void readFileBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void openBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void importbtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void dataGridView_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);
        System::Void selectAllBtn_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void backgroundWorker1_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void backgroundWorker1_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);
        System::Void ImportDoveIdsUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void ImportDoveIdsUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e);
        System::Void StartGeneration(System::Boolean importTowers);

    private:
        System::Windows::Forms::Button^                 closeBtn;
        System::Windows::Forms::Button^                 openBtn;
        System::Windows::Forms::Button^                 importbtn;
        System::Windows::Forms::ToolStripProgressBar^   progressBar;


        System::Windows::Forms::OpenFileDialog^         openFileDialog1;
        System::ComponentModel::BackgroundWorker^       backgroundWorker1;

        DucoUI::ProgressCallbackWrapper*                callback;
        Duco::DoveDatabase*                             doveDatabase;
        DucoUI::DatabaseManager^                        database;
        int                                             numberOfTowersFound;
        int                                             numberOfTowersProcessable;
        System::Boolean                                 importingTowers;

    private: System::Windows::Forms::ToolTip^                   toolTip1;
    private: System::Windows::Forms::DataGridView^              dataGridView;
    private: System::Windows::Forms::ToolStripStatusLabel^      statusLbl;
    private: System::Windows::Forms::Button^                    selectAllBtn;
    private: System::ComponentModel::IContainer^  components;
    private: System::Windows::Forms::ToolStripStatusLabel^  doveFilenameLbl;
    private: System::Windows::Forms::Button^  readFileBtn;
    private: System::Windows::Forms::DataGridViewCheckBoxColumn^ includeColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ towerId;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ towerNameColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ doveColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ doveTowerColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ ExistingTowerId;
    private: System::Windows::Forms::DataGridViewCellStyle^ towerAlreadyUsedCellStyle;

    ref class DoveImportStartUiArgs
    {
    public:
        System::String^     doveFile;
        bool                openDoveFile;
        bool                importDoveIdAndPosition;
    };

#pragma region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        void InitializeComponent(void)
        {
            this->components = (gcnew System::ComponentModel::Container());
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
            System::Windows::Forms::StatusStrip^ statusStrip1;
            this->readFileBtn = (gcnew System::Windows::Forms::Button());
            this->dataGridView = (gcnew System::Windows::Forms::DataGridView());
            this->importbtn = (gcnew System::Windows::Forms::Button());
            this->openBtn = (gcnew System::Windows::Forms::Button());
            this->closeBtn = (gcnew System::Windows::Forms::Button());
            this->selectAllBtn = (gcnew System::Windows::Forms::Button());
            this->progressBar = (gcnew System::Windows::Forms::ToolStripProgressBar());
            this->statusLbl = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->doveFilenameLbl = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
            this->backgroundWorker1 = (gcnew System::ComponentModel::BackgroundWorker());
            this->toolTip1 = (gcnew System::Windows::Forms::ToolTip(this->components));
            this->includeColumn = (gcnew System::Windows::Forms::DataGridViewCheckBoxColumn());
            this->towerId = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->towerNameColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->doveColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->doveTowerColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->ExistingTowerId = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            tableLayoutPanel1->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->BeginInit();
            statusStrip1->SuspendLayout();
            this->SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 6;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->Controls->Add(this->readFileBtn, 2, 1);
            tableLayoutPanel1->Controls->Add(this->dataGridView, 0, 0);
            tableLayoutPanel1->Controls->Add(this->importbtn, 4, 1);
            tableLayoutPanel1->Controls->Add(this->openBtn, 1, 1);
            tableLayoutPanel1->Controls->Add(this->closeBtn, 5, 1);
            tableLayoutPanel1->Controls->Add(this->selectAllBtn, 3, 1);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 2;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 51)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
            tableLayoutPanel1->Size = System::Drawing::Size(896, 392);
            tableLayoutPanel1->TabIndex = 0;
            // 
            // readFileBtn
            // 
            this->readFileBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->readFileBtn->Enabled = false;
            this->readFileBtn->Location = System::Drawing::Point(575, 344);
            this->readFileBtn->Name = L"readFileBtn";
            this->readFileBtn->Size = System::Drawing::Size(75, 23);
            this->readFileBtn->TabIndex = 6;
            this->readFileBtn->Text = L"Read file";
            this->toolTip1->SetToolTip(this->readFileBtn, L"Read the dove data file and search towers");
            this->readFileBtn->UseVisualStyleBackColor = true;
            this->readFileBtn->Click += gcnew System::EventHandler(this, &ImportDoveIdsUI::readFileBtn_Click);
            // 
            // dataGridView
            // 
            this->dataGridView->AllowUserToAddRows = false;
            this->dataGridView->AllowUserToDeleteRows = false;
            this->dataGridView->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::AllCells;
            this->dataGridView->AutoSizeRowsMode = System::Windows::Forms::DataGridViewAutoSizeRowsMode::AllCells;
            this->dataGridView->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->dataGridView->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(6) {
                this->includeColumn,
                    this->towerId, this->towerNameColumn, this->doveColumn, this->doveTowerColumn, this->ExistingTowerId
            });
            tableLayoutPanel1->SetColumnSpan(this->dataGridView, 6);
            this->dataGridView->Dock = System::Windows::Forms::DockStyle::Fill;
            this->dataGridView->Location = System::Drawing::Point(3, 3);
            this->dataGridView->Name = L"dataGridView";
            this->dataGridView->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
            this->dataGridView->Size = System::Drawing::Size(890, 335);
            this->dataGridView->TabIndex = 4;
            this->dataGridView->CellContentDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &ImportDoveIdsUI::dataGridView_CellContentDoubleClick);
            // 
            // importbtn
            // 
            this->importbtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->importbtn->Enabled = false;
            this->importbtn->Location = System::Drawing::Point(737, 344);
            this->importbtn->Name = L"importbtn";
            this->importbtn->Size = System::Drawing::Size(75, 23);
            this->importbtn->TabIndex = 3;
            this->importbtn->Text = L"Import";
            this->toolTip1->SetToolTip(this->importbtn, L"Import duco id and locations for the selected towers");
            this->importbtn->UseVisualStyleBackColor = true;
            this->importbtn->Click += gcnew System::EventHandler(this, &ImportDoveIdsUI::importbtn_Click);
            // 
            // openBtn
            // 
            this->openBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->openBtn->Location = System::Drawing::Point(494, 344);
            this->openBtn->Name = L"openBtn";
            this->openBtn->Size = System::Drawing::Size(75, 23);
            this->openBtn->TabIndex = 1;
            this->openBtn->Text = L"Choose file";
            this->toolTip1->SetToolTip(this->openBtn, L"Download the latest Dove datafile from \"http://dove.cccbr.org.uk/home.php\" and th"
                L"en tell Duco where to find it");
            this->openBtn->UseVisualStyleBackColor = true;
            this->openBtn->Click += gcnew System::EventHandler(this, &ImportDoveIdsUI::openBtn_Click);
            // 
            // closeBtn
            // 
            this->closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            this->closeBtn->Location = System::Drawing::Point(818, 344);
            this->closeBtn->Name = L"closeBtn";
            this->closeBtn->Size = System::Drawing::Size(75, 23);
            this->closeBtn->TabIndex = 0;
            this->closeBtn->Text = L"Close";
            this->closeBtn->UseVisualStyleBackColor = true;
            this->closeBtn->Click += gcnew System::EventHandler(this, &ImportDoveIdsUI::closeBtn_Click);
            // 
            // selectAllBtn
            // 
            this->selectAllBtn->Location = System::Drawing::Point(656, 344);
            this->selectAllBtn->Name = L"selectAllBtn";
            this->selectAllBtn->Size = System::Drawing::Size(75, 23);
            this->selectAllBtn->TabIndex = 5;
            this->selectAllBtn->Text = L"Select all";
            this->selectAllBtn->UseVisualStyleBackColor = true;
            this->selectAllBtn->Click += gcnew System::EventHandler(this, &ImportDoveIdsUI::selectAllBtn_Click);
            // 
            // statusStrip1
            // 
            statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
                this->progressBar, this->statusLbl,
                    this->doveFilenameLbl
            });
            statusStrip1->Location = System::Drawing::Point(0, 370);
            statusStrip1->Name = L"statusStrip1";
            statusStrip1->Size = System::Drawing::Size(896, 22);
            statusStrip1->TabIndex = 1;
            statusStrip1->Text = L"statusStrip1";
            // 
            // progressBar
            // 
            this->progressBar->Name = L"progressBar";
            this->progressBar->Size = System::Drawing::Size(100, 16);
            // 
            // statusLbl
            // 
            this->statusLbl->Name = L"statusLbl";
            this->statusLbl->Size = System::Drawing::Size(729, 17);
            this->statusLbl->Spring = true;
            this->statusLbl->Text = L"status text";
            // 
            // doveFilenameLbl
            // 
            this->doveFilenameLbl->Name = L"doveFilenameLbl";
            this->doveFilenameLbl->Size = System::Drawing::Size(50, 17);
            this->doveFilenameLbl->Text = L"dove.csv";
            // 
            // openFileDialog1
            // 
            this->openFileDialog1->DefaultExt = L"*.csv";
            this->openFileDialog1->Filter = L"CSV files|*.csv|All files|*.*";
            // 
            // backgroundWorker1
            // 
            this->backgroundWorker1->WorkerReportsProgress = true;
            this->backgroundWorker1->WorkerSupportsCancellation = true;
            this->backgroundWorker1->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &ImportDoveIdsUI::backgroundWorker1_DoWork);
            this->backgroundWorker1->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &ImportDoveIdsUI::backgroundWorker1_ProgressChanged);
            this->backgroundWorker1->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &ImportDoveIdsUI::backgroundWorker1_RunWorkerCompleted);
            // 
            // includeColumn
            // 
            this->includeColumn->FalseValue = L"";
            this->includeColumn->HeaderText = L"";
            this->includeColumn->Name = L"includeColumn";
            this->includeColumn->TrueValue = L"";
            this->includeColumn->Width = 5;
            // 
            // towerId
            // 
            this->towerId->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            this->towerId->HeaderText = L"Id";
            this->towerId->Name = L"towerId";
            this->towerId->ReadOnly = true;
            this->towerId->Width = 41;
            // 
            // towerNameColumn
            // 
            this->towerNameColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
            this->towerNameColumn->HeaderText = L"Tower";
            this->towerNameColumn->Name = L"towerNameColumn";
            this->towerNameColumn->ReadOnly = true;
            // 
            // doveColumn
            // 
            this->doveColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            this->doveColumn->HeaderText = L"Dove reference";
            this->doveColumn->Name = L"doveColumn";
            this->doveColumn->ReadOnly = true;
            this->doveColumn->Width = 97;
            // 
            // doveTowerColumn
            // 
            this->doveTowerColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
            this->doveTowerColumn->HeaderText = L"Dove tower";
            this->doveTowerColumn->Name = L"doveTowerColumn";
            this->doveTowerColumn->ReadOnly = true;
            // 
            // ExistingTowerId
            // 
            this->ExistingTowerId->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            this->ExistingTowerId->HeaderText = L"Existing duco tower id";
            this->ExistingTowerId->Name = L"ExistingTowerId";
            this->ExistingTowerId->ReadOnly = true;
            this->ExistingTowerId->Width = 114;
            // 
            // ImportDoveIdsUI
            // 
            this->AcceptButton = this->importbtn;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->CancelButton = this->closeBtn;
            this->ClientSize = System::Drawing::Size(896, 392);
            this->Controls->Add(statusStrip1);
            this->Controls->Add(tableLayoutPanel1);
            this->MinimumSize = System::Drawing::Size(459, 157);
            this->Name = L"ImportDoveIdsUI";
            this->Text = L"Find Dove ids for towers";
            this->toolTip1->SetToolTip(this, L"Finds dove Ids for existing towers, so that the locaations can be imported and he"
                L"lps to prevent duplicate towers.");
            this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &ImportDoveIdsUI::ImportDoveIdsUI_FormClosing);
            this->Load += gcnew System::EventHandler(this, &ImportDoveIdsUI::ImportDoveIdsUI_Load);
            tableLayoutPanel1->ResumeLayout(false);
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->EndInit();
            statusStrip1->ResumeLayout(false);
            statusStrip1->PerformLayout();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion
    };
}
