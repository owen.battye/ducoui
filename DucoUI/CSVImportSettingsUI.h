#pragma once
namespace DucoUI
{

    public ref class CSVImportSettingsUI : public System::Windows::Forms::Form
    {
    public:
        CSVImportSettingsUI(DucoUI::WindowsSettings^ theSettings, System::String^ fileNameOnly);

    protected:
        !CSVImportSettingsUI();
        ~CSVImportSettingsUI();

    private:
        System::Void cancelBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void saveBtn_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void pealBookSettingsBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void resetSettingsBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void defaultsBtn_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void ValueChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void singleDateField_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void singleMethodField_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void singleTimeField_CheckedChanged(System::Object^  sender, System::EventArgs^  e);

        System::Void ClosingEvent(System::Object^ object, System::ComponentModel::CancelEventArgs^ e);
        System::Void CSVImportSettingsUI_Load(System::Object^  sender, System::EventArgs^  e);

        System::Void SetDataChanged();

        System::Void PopulateView();
        System::Void PopulateSetting(System::Windows::Forms::NumericUpDown^ field, const std::wstring& settingId);
        System::Void PopulateSetting(System::Windows::Forms::TextBox^ field, const std::wstring& settingId);

        System::Void SaveSettings();
        static System::Void SaveSetting(System::Windows::Forms::NumericUpDown^ field, const std::wstring& settingId, Duco::SettingsFile& settings);
        static System::Void SaveSetting(System::Windows::Forms::TextBox^ field, const std::wstring& settingId, Duco::SettingsFile& settings);

    private:
        bool                                dataChanged;
        bool                                populatingData;
        System::Windows::Forms::Button^     saveBtn;
        Duco::SettingsFile*                 settings;
    private: System::Windows::Forms::NumericUpDown^  countyField;
    private: System::Windows::Forms::NumericUpDown^  cityField;
    private: System::Windows::Forms::NumericUpDown^  nameField;
    private: System::Windows::Forms::NumericUpDown^  lbs;
    private: System::Windows::Forms::NumericUpDown^  quarters;
    private: System::Windows::Forms::NumericUpDown^  cwt;
    private: System::Windows::Forms::NumericUpDown^  year;
    private: System::Windows::Forms::NumericUpDown^  month;
    private: System::Windows::Forms::NumericUpDown^  day;
    private: System::Windows::Forms::RadioButton^  conductorBellNo;
    private: System::Windows::Forms::RadioButton^  conductorName;
    private: System::Windows::Forms::NumericUpDown^  conductor;
    private: System::Windows::Forms::NumericUpDown^  lastRinger;
    private: System::Windows::Forms::NumericUpDown^  firstRinger;



    private: System::Windows::Forms::NumericUpDown^  minutes;
    private: System::Windows::Forms::NumericUpDown^  hours;
    private: System::Windows::Forms::NumericUpDown^  association;
    private: System::Windows::Forms::NumericUpDown^  changes;
    private: System::Windows::Forms::CheckBox^  handbellAlternate;
    private: System::Windows::Forms::TextBox^  handbellStr;
    private: System::Windows::Forms::NumericUpDown^  handbellField;
    private: System::Windows::Forms::HelpProvider^  helpProvider1;
    private: System::Windows::Forms::ToolTip^  toolTip1;
    private: System::Windows::Forms::NumericUpDown^  footnotes;
    private: System::Windows::Forms::NumericUpDown^  tenorKey;
    private: System::Windows::Forms::NumericUpDown^  composer;
    private: System::Windows::Forms::NumericUpDown^  splicedMethods;
    private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
    private: System::Windows::Forms::CheckBox^  singleDateField;
    private: System::Windows::Forms::Label^  dayLbl;
    private: System::Windows::Forms::CheckBox^  singleMethodField;
    private: System::Windows::Forms::NumericUpDown^  methodStage;
    private: System::Windows::Forms::NumericUpDown^  methodType;
    private: System::Windows::Forms::NumericUpDown^  methodName;
    private: System::Windows::Forms::CheckBox^  singleTimeField;
    private: System::Windows::Forms::Label^  hoursLbl;
    private: System::Windows::Forms::CheckBox^  removeBellNoFromRingers;
    private: System::Windows::Forms::CheckBox^  ignoreFirstLine;
    private: System::Windows::Forms::CheckBox^  enforceQuotes;
    private: System::Windows::Forms::CheckBox^ decodeCheckBox;
    private: System::Windows::Forms::NumericUpDown^ ringingWorldReference;
    private: System::Windows::Forms::NumericUpDown^ bellBoardReference;


    private: System::ComponentModel::IContainer^  components;


#pragma region Windows Form Designer generated code
		void InitializeComponent()
		{
            this->components = (gcnew System::ComponentModel::Container());
            System::Windows::Forms::Button^ cancelBtn;
            System::Windows::Forms::GroupBox^ towerGroup;
            System::Windows::Forms::Label^ countyLbl;
            System::Windows::Forms::Label^ townLbl;
            System::Windows::Forms::Label^ towerNameLbl;
            System::Windows::Forms::GroupBox^ tenorGroup;
            System::Windows::Forms::Label^ lbsLbl;
            System::Windows::Forms::Label^ quartersLbl;
            System::Windows::Forms::Label^ cwtLbl;
            System::Windows::Forms::GroupBox^ dateGroup;
            System::Windows::Forms::Label^ yearLbl;
            System::Windows::Forms::Label^ monthLbl;
            System::Windows::Forms::GroupBox^ ringersGroup;
            System::Windows::Forms::Label^ composerLbl;
            System::Windows::Forms::Label^ ConductorLbl;
            System::Windows::Forms::Label^ lastRingerLbl;
            System::Windows::Forms::Label^ firstRingerLbl;
            System::Windows::Forms::GroupBox^ timeGroup;
            System::Windows::Forms::Label^ minutesLbl;
            System::Windows::Forms::GroupBox^ generalGroup;
            System::Windows::Forms::Label^ splicedMethodsLbl;
            System::Windows::Forms::Label^ tenorKeyLbl;
            System::Windows::Forms::Label^ footnotesLbl;
            System::Windows::Forms::Label^ associationLbl;
            System::Windows::Forms::Label^ changesLbl;
            System::Windows::Forms::GroupBox^ handbellGroup;
            System::Windows::Forms::Label^ handbellStringLbl;
            System::Windows::Forms::Label^ handbellFieldLbl;
            System::Windows::Forms::TabControl^ tabControl1;
            System::Windows::Forms::TabPage^ detailsPage;
            System::Windows::Forms::TabPage^ methodPage;
            System::Windows::Forms::GroupBox^ methodGroup;
            System::Windows::Forms::Label^ stageLbl;
            System::Windows::Forms::Label^ typeLbl;
            System::Windows::Forms::Label^ methodNameLbl;
            System::Windows::Forms::TabPage^ dateAndTimePage;
            System::Windows::Forms::TabPage^ ringersPage;
            System::Windows::Forms::TabPage^ towerPage;
            System::Windows::Forms::TabPage^ defaultsPage;
            System::Windows::Forms::Button^ resetSettingsBtn;
            System::Windows::Forms::Button^ defaultsBtn;
            System::Windows::Forms::Button^ pealBookSettingsBtn;
            System::Windows::Forms::GroupBox^ referencesGroup;
            System::Windows::Forms::Label^ bellboardLabel;
            System::Windows::Forms::Label^ ringingWorldLabel;
            System::Windows::Forms::GroupBox^ otherGroup;
            this->countyField = (gcnew System::Windows::Forms::NumericUpDown());
            this->cityField = (gcnew System::Windows::Forms::NumericUpDown());
            this->nameField = (gcnew System::Windows::Forms::NumericUpDown());
            this->lbs = (gcnew System::Windows::Forms::NumericUpDown());
            this->quarters = (gcnew System::Windows::Forms::NumericUpDown());
            this->cwt = (gcnew System::Windows::Forms::NumericUpDown());
            this->singleDateField = (gcnew System::Windows::Forms::CheckBox());
            this->year = (gcnew System::Windows::Forms::NumericUpDown());
            this->month = (gcnew System::Windows::Forms::NumericUpDown());
            this->day = (gcnew System::Windows::Forms::NumericUpDown());
            this->dayLbl = (gcnew System::Windows::Forms::Label());
            this->removeBellNoFromRingers = (gcnew System::Windows::Forms::CheckBox());
            this->composer = (gcnew System::Windows::Forms::NumericUpDown());
            this->conductorBellNo = (gcnew System::Windows::Forms::RadioButton());
            this->conductorName = (gcnew System::Windows::Forms::RadioButton());
            this->conductor = (gcnew System::Windows::Forms::NumericUpDown());
            this->lastRinger = (gcnew System::Windows::Forms::NumericUpDown());
            this->firstRinger = (gcnew System::Windows::Forms::NumericUpDown());
            this->singleTimeField = (gcnew System::Windows::Forms::CheckBox());
            this->minutes = (gcnew System::Windows::Forms::NumericUpDown());
            this->hours = (gcnew System::Windows::Forms::NumericUpDown());
            this->hoursLbl = (gcnew System::Windows::Forms::Label());
            this->splicedMethods = (gcnew System::Windows::Forms::NumericUpDown());
            this->tenorKey = (gcnew System::Windows::Forms::NumericUpDown());
            this->footnotes = (gcnew System::Windows::Forms::NumericUpDown());
            this->association = (gcnew System::Windows::Forms::NumericUpDown());
            this->changes = (gcnew System::Windows::Forms::NumericUpDown());
            this->handbellAlternate = (gcnew System::Windows::Forms::CheckBox());
            this->handbellStr = (gcnew System::Windows::Forms::TextBox());
            this->handbellField = (gcnew System::Windows::Forms::NumericUpDown());
            this->decodeCheckBox = (gcnew System::Windows::Forms::CheckBox());
            this->enforceQuotes = (gcnew System::Windows::Forms::CheckBox());
            this->ignoreFirstLine = (gcnew System::Windows::Forms::CheckBox());
            this->singleMethodField = (gcnew System::Windows::Forms::CheckBox());
            this->methodStage = (gcnew System::Windows::Forms::NumericUpDown());
            this->methodType = (gcnew System::Windows::Forms::NumericUpDown());
            this->methodName = (gcnew System::Windows::Forms::NumericUpDown());
            this->saveBtn = (gcnew System::Windows::Forms::Button());
            this->helpProvider1 = (gcnew System::Windows::Forms::HelpProvider());
            this->toolTip1 = (gcnew System::Windows::Forms::ToolTip(this->components));
            this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            this->bellBoardReference = (gcnew System::Windows::Forms::NumericUpDown());
            this->ringingWorldReference = (gcnew System::Windows::Forms::NumericUpDown());
            cancelBtn = (gcnew System::Windows::Forms::Button());
            towerGroup = (gcnew System::Windows::Forms::GroupBox());
            countyLbl = (gcnew System::Windows::Forms::Label());
            townLbl = (gcnew System::Windows::Forms::Label());
            towerNameLbl = (gcnew System::Windows::Forms::Label());
            tenorGroup = (gcnew System::Windows::Forms::GroupBox());
            lbsLbl = (gcnew System::Windows::Forms::Label());
            quartersLbl = (gcnew System::Windows::Forms::Label());
            cwtLbl = (gcnew System::Windows::Forms::Label());
            dateGroup = (gcnew System::Windows::Forms::GroupBox());
            yearLbl = (gcnew System::Windows::Forms::Label());
            monthLbl = (gcnew System::Windows::Forms::Label());
            ringersGroup = (gcnew System::Windows::Forms::GroupBox());
            composerLbl = (gcnew System::Windows::Forms::Label());
            ConductorLbl = (gcnew System::Windows::Forms::Label());
            lastRingerLbl = (gcnew System::Windows::Forms::Label());
            firstRingerLbl = (gcnew System::Windows::Forms::Label());
            timeGroup = (gcnew System::Windows::Forms::GroupBox());
            minutesLbl = (gcnew System::Windows::Forms::Label());
            generalGroup = (gcnew System::Windows::Forms::GroupBox());
            splicedMethodsLbl = (gcnew System::Windows::Forms::Label());
            tenorKeyLbl = (gcnew System::Windows::Forms::Label());
            footnotesLbl = (gcnew System::Windows::Forms::Label());
            associationLbl = (gcnew System::Windows::Forms::Label());
            changesLbl = (gcnew System::Windows::Forms::Label());
            handbellGroup = (gcnew System::Windows::Forms::GroupBox());
            handbellStringLbl = (gcnew System::Windows::Forms::Label());
            handbellFieldLbl = (gcnew System::Windows::Forms::Label());
            tabControl1 = (gcnew System::Windows::Forms::TabControl());
            detailsPage = (gcnew System::Windows::Forms::TabPage());
            methodPage = (gcnew System::Windows::Forms::TabPage());
            methodGroup = (gcnew System::Windows::Forms::GroupBox());
            stageLbl = (gcnew System::Windows::Forms::Label());
            typeLbl = (gcnew System::Windows::Forms::Label());
            methodNameLbl = (gcnew System::Windows::Forms::Label());
            dateAndTimePage = (gcnew System::Windows::Forms::TabPage());
            ringersPage = (gcnew System::Windows::Forms::TabPage());
            towerPage = (gcnew System::Windows::Forms::TabPage());
            defaultsPage = (gcnew System::Windows::Forms::TabPage());
            resetSettingsBtn = (gcnew System::Windows::Forms::Button());
            defaultsBtn = (gcnew System::Windows::Forms::Button());
            pealBookSettingsBtn = (gcnew System::Windows::Forms::Button());
            referencesGroup = (gcnew System::Windows::Forms::GroupBox());
            bellboardLabel = (gcnew System::Windows::Forms::Label());
            ringingWorldLabel = (gcnew System::Windows::Forms::Label());
            otherGroup = (gcnew System::Windows::Forms::GroupBox());
            towerGroup->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->countyField))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cityField))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->nameField))->BeginInit();
            tenorGroup->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->lbs))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->quarters))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cwt))->BeginInit();
            dateGroup->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->year))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->month))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->day))->BeginInit();
            ringersGroup->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->composer))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->conductor))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->lastRinger))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->firstRinger))->BeginInit();
            timeGroup->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->minutes))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->hours))->BeginInit();
            generalGroup->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splicedMethods))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->tenorKey))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->footnotes))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->association))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->changes))->BeginInit();
            handbellGroup->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->handbellField))->BeginInit();
            tabControl1->SuspendLayout();
            detailsPage->SuspendLayout();
            methodPage->SuspendLayout();
            methodGroup->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->methodStage))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->methodType))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->methodName))->BeginInit();
            dateAndTimePage->SuspendLayout();
            ringersPage->SuspendLayout();
            towerPage->SuspendLayout();
            defaultsPage->SuspendLayout();
            this->tableLayoutPanel1->SuspendLayout();
            referencesGroup->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bellBoardReference))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ringingWorldReference))->BeginInit();
            otherGroup->SuspendLayout();
            this->SuspendLayout();
            // 
            // cancelBtn
            // 
            cancelBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            cancelBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            cancelBtn->Location = System::Drawing::Point(310, 272);
            cancelBtn->Name = L"cancelBtn";
            cancelBtn->Size = System::Drawing::Size(74, 23);
            cancelBtn->TabIndex = 36;
            cancelBtn->Text = L"Cancel";
            cancelBtn->UseVisualStyleBackColor = true;
            cancelBtn->Click += gcnew System::EventHandler(this, &CSVImportSettingsUI::cancelBtn_Click);
            // 
            // towerGroup
            // 
            towerGroup->Controls->Add(this->countyField);
            towerGroup->Controls->Add(this->cityField);
            towerGroup->Controls->Add(countyLbl);
            towerGroup->Controls->Add(townLbl);
            towerGroup->Controls->Add(towerNameLbl);
            towerGroup->Controls->Add(this->nameField);
            towerGroup->Location = System::Drawing::Point(3, 3);
            towerGroup->Name = L"towerGroup";
            towerGroup->Size = System::Drawing::Size(162, 98);
            towerGroup->TabIndex = 5;
            towerGroup->TabStop = false;
            towerGroup->Text = L"Tower";
            // 
            // countyField
            // 
            this->countyField->Location = System::Drawing::Point(113, 69);
            this->countyField->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, System::Int32::MinValue });
            this->countyField->Name = L"countyField";
            this->countyField->Size = System::Drawing::Size(38, 20);
            this->countyField->TabIndex = 8;
            this->countyField->ValueChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::ValueChanged);
            // 
            // cityField
            // 
            this->cityField->Location = System::Drawing::Point(113, 43);
            this->cityField->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, System::Int32::MinValue });
            this->cityField->Name = L"cityField";
            this->cityField->Size = System::Drawing::Size(38, 20);
            this->cityField->TabIndex = 7;
            this->cityField->ValueChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::ValueChanged);
            // 
            // countyLbl
            // 
            countyLbl->AutoSize = true;
            countyLbl->Location = System::Drawing::Point(64, 71);
            countyLbl->Name = L"countyLbl";
            countyLbl->Size = System::Drawing::Size(43, 13);
            countyLbl->TabIndex = 999;
            countyLbl->Text = L"County:";
            // 
            // townLbl
            // 
            townLbl->AutoSize = true;
            townLbl->Location = System::Drawing::Point(42, 45);
            townLbl->Name = L"townLbl";
            townLbl->Size = System::Drawing::Size(65, 13);
            townLbl->TabIndex = 999;
            townLbl->Text = L"Town / City:";
            // 
            // towerNameLbl
            // 
            towerNameLbl->AutoSize = true;
            towerNameLbl->Location = System::Drawing::Point(7, 19);
            towerNameLbl->Name = L"towerNameLbl";
            towerNameLbl->Size = System::Drawing::Size(100, 13);
            towerNameLbl->TabIndex = 999;
            towerNameLbl->Text = L"Name / Dedication:";
            // 
            // nameField
            // 
            this->nameField->Location = System::Drawing::Point(113, 17);
            this->nameField->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, System::Int32::MinValue });
            this->nameField->Name = L"nameField";
            this->nameField->Size = System::Drawing::Size(38, 20);
            this->nameField->TabIndex = 6;
            this->nameField->ValueChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::ValueChanged);
            // 
            // tenorGroup
            // 
            tenorGroup->Controls->Add(this->lbs);
            tenorGroup->Controls->Add(this->quarters);
            tenorGroup->Controls->Add(this->cwt);
            tenorGroup->Controls->Add(lbsLbl);
            tenorGroup->Controls->Add(quartersLbl);
            tenorGroup->Controls->Add(cwtLbl);
            tenorGroup->Location = System::Drawing::Point(171, 3);
            tenorGroup->Name = L"tenorGroup";
            tenorGroup->Size = System::Drawing::Size(95, 98);
            tenorGroup->TabIndex = 9;
            tenorGroup->TabStop = false;
            tenorGroup->Text = L"Tenor";
            // 
            // lbs
            // 
            this->lbs->Location = System::Drawing::Point(44, 69);
            this->lbs->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, System::Int32::MinValue });
            this->lbs->Name = L"lbs";
            this->lbs->Size = System::Drawing::Size(38, 20);
            this->lbs->TabIndex = 12;
            this->lbs->ValueChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::ValueChanged);
            // 
            // quarters
            // 
            this->quarters->Location = System::Drawing::Point(44, 43);
            this->quarters->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, System::Int32::MinValue });
            this->quarters->Name = L"quarters";
            this->quarters->Size = System::Drawing::Size(38, 20);
            this->quarters->TabIndex = 11;
            this->quarters->ValueChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::ValueChanged);
            // 
            // cwt
            // 
            this->cwt->Location = System::Drawing::Point(44, 17);
            this->cwt->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, System::Int32::MinValue });
            this->cwt->Name = L"cwt";
            this->cwt->Size = System::Drawing::Size(38, 20);
            this->cwt->TabIndex = 10;
            this->cwt->ValueChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::ValueChanged);
            // 
            // lbsLbl
            // 
            lbsLbl->AutoSize = true;
            lbsLbl->Location = System::Drawing::Point(14, 71);
            lbsLbl->Name = L"lbsLbl";
            lbsLbl->Size = System::Drawing::Size(24, 13);
            lbsLbl->TabIndex = 999;
            lbsLbl->Text = L"Lbs";
            // 
            // quartersLbl
            // 
            quartersLbl->AutoSize = true;
            quartersLbl->Location = System::Drawing::Point(9, 45);
            quartersLbl->Name = L"quartersLbl";
            quartersLbl->Size = System::Drawing::Size(29, 13);
            quartersLbl->TabIndex = 999;
            quartersLbl->Text = L"Qrtrs";
            // 
            // cwtLbl
            // 
            cwtLbl->AutoSize = true;
            cwtLbl->Location = System::Drawing::Point(13, 19);
            cwtLbl->Name = L"cwtLbl";
            cwtLbl->Size = System::Drawing::Size(25, 13);
            cwtLbl->TabIndex = 999;
            cwtLbl->Text = L"Cwt";
            // 
            // dateGroup
            // 
            dateGroup->Controls->Add(this->singleDateField);
            dateGroup->Controls->Add(this->year);
            dateGroup->Controls->Add(this->month);
            dateGroup->Controls->Add(this->day);
            dateGroup->Controls->Add(yearLbl);
            dateGroup->Controls->Add(monthLbl);
            dateGroup->Controls->Add(this->dayLbl);
            dateGroup->Location = System::Drawing::Point(6, 6);
            dateGroup->Name = L"dateGroup";
            dateGroup->Size = System::Drawing::Size(123, 121);
            dateGroup->TabIndex = 1;
            dateGroup->TabStop = false;
            dateGroup->Text = L"Date";
            // 
            // singleDateField
            // 
            this->singleDateField->AutoSize = true;
            this->singleDateField->Location = System::Drawing::Point(12, 97);
            this->singleDateField->Name = L"singleDateField";
            this->singleDateField->Size = System::Drawing::Size(101, 17);
            this->singleDateField->TabIndex = 1000;
            this->singleDateField->Text = L"Single date field";
            this->toolTip1->SetToolTip(this->singleDateField, L"Use a single field for date, rather than one for day, one for month and one for y"
                L"ear");
            this->singleDateField->UseVisualStyleBackColor = true;
            this->singleDateField->CheckedChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::singleDateField_CheckedChanged);
            // 
            // year
            // 
            this->year->Location = System::Drawing::Point(52, 69);
            this->year->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, System::Int32::MinValue });
            this->year->Name = L"year";
            this->year->Size = System::Drawing::Size(38, 20);
            this->year->TabIndex = 4;
            this->year->ValueChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::ValueChanged);
            // 
            // month
            // 
            this->month->Location = System::Drawing::Point(52, 43);
            this->month->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, System::Int32::MinValue });
            this->month->Name = L"month";
            this->month->Size = System::Drawing::Size(38, 20);
            this->month->TabIndex = 3;
            this->month->ValueChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::ValueChanged);
            // 
            // day
            // 
            this->day->Location = System::Drawing::Point(52, 17);
            this->day->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, System::Int32::MinValue });
            this->day->Name = L"day";
            this->day->Size = System::Drawing::Size(38, 20);
            this->day->TabIndex = 2;
            this->day->ValueChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::ValueChanged);
            // 
            // yearLbl
            // 
            yearLbl->AutoSize = true;
            yearLbl->Location = System::Drawing::Point(17, 71);
            yearLbl->Name = L"yearLbl";
            yearLbl->Size = System::Drawing::Size(29, 13);
            yearLbl->TabIndex = 999;
            yearLbl->Text = L"Year";
            // 
            // monthLbl
            // 
            monthLbl->AutoSize = true;
            monthLbl->Location = System::Drawing::Point(9, 45);
            monthLbl->Name = L"monthLbl";
            monthLbl->Size = System::Drawing::Size(37, 13);
            monthLbl->TabIndex = 999;
            monthLbl->Text = L"Month";
            // 
            // dayLbl
            // 
            this->dayLbl->AutoSize = true;
            this->dayLbl->Location = System::Drawing::Point(20, 19);
            this->dayLbl->Name = L"dayLbl";
            this->dayLbl->Size = System::Drawing::Size(26, 13);
            this->dayLbl->TabIndex = 999;
            this->dayLbl->Text = L"Day";
            // 
            // ringersGroup
            // 
            ringersGroup->Controls->Add(this->removeBellNoFromRingers);
            ringersGroup->Controls->Add(this->composer);
            ringersGroup->Controls->Add(composerLbl);
            ringersGroup->Controls->Add(this->conductorBellNo);
            ringersGroup->Controls->Add(this->conductorName);
            ringersGroup->Controls->Add(this->conductor);
            ringersGroup->Controls->Add(this->lastRinger);
            ringersGroup->Controls->Add(this->firstRinger);
            ringersGroup->Controls->Add(ConductorLbl);
            ringersGroup->Controls->Add(lastRingerLbl);
            ringersGroup->Controls->Add(firstRingerLbl);
            ringersGroup->Location = System::Drawing::Point(4, 3);
            ringersGroup->Name = L"ringersGroup";
            ringersGroup->Size = System::Drawing::Size(252, 123);
            ringersGroup->TabIndex = 17;
            ringersGroup->TabStop = false;
            ringersGroup->Text = L"Ringers";
            // 
            // removeBellNoFromRingers
            // 
            this->removeBellNoFromRingers->AutoSize = true;
            this->removeBellNoFromRingers->Location = System::Drawing::Point(12, 93);
            this->removeBellNoFromRingers->Name = L"removeBellNoFromRingers";
            this->removeBellNoFromRingers->Size = System::Drawing::Size(180, 17);
            this->removeBellNoFromRingers->TabIndex = 1002;
            this->removeBellNoFromRingers->Text = L"Remove bell number from ringers";
            this->toolTip1->SetToolTip(this->removeBellNoFromRingers, L"Remove the bell number from the start of the ringers name, for example on pealboo"
                L"k csv files.");
            this->removeBellNoFromRingers->UseVisualStyleBackColor = true;
            this->removeBellNoFromRingers->CheckedChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::CheckedChanged);
            // 
            // composer
            // 
            this->composer->Location = System::Drawing::Point(176, 43);
            this->composer->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, System::Int32::MinValue });
            this->composer->Name = L"composer";
            this->composer->Size = System::Drawing::Size(38, 20);
            this->composer->TabIndex = 1000;
            this->composer->ValueChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::ValueChanged);
            // 
            // composerLbl
            // 
            composerLbl->AutoSize = true;
            composerLbl->Location = System::Drawing::Point(116, 45);
            composerLbl->Name = L"composerLbl";
            composerLbl->Size = System::Drawing::Size(54, 13);
            composerLbl->TabIndex = 1001;
            composerLbl->Text = L"Composer";
            // 
            // conductorBellNo
            // 
            this->conductorBellNo->AutoSize = true;
            this->conductorBellNo->Location = System::Drawing::Point(135, 69);
            this->conductorBellNo->Name = L"conductorBellNo";
            this->conductorBellNo->Size = System::Drawing::Size(93, 17);
            this->conductorBellNo->TabIndex = 22;
            this->conductorBellNo->TabStop = true;
            this->conductorBellNo->Text = L"by bell number";
            this->conductorBellNo->UseVisualStyleBackColor = true;
            this->conductorBellNo->CheckedChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::CheckedChanged);
            // 
            // conductorName
            // 
            this->conductorName->AutoSize = true;
            this->conductorName->Location = System::Drawing::Point(12, 69);
            this->conductorName->Name = L"conductorName";
            this->conductorName->Size = System::Drawing::Size(117, 17);
            this->conductorName->TabIndex = 21;
            this->conductorName->TabStop = true;
            this->conductorName->Text = L"Conductor by name";
            this->conductorName->UseVisualStyleBackColor = true;
            this->conductorName->CheckedChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::CheckedChanged);
            // 
            // conductor
            // 
            this->conductor->Location = System::Drawing::Point(71, 43);
            this->conductor->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, System::Int32::MinValue });
            this->conductor->Name = L"conductor";
            this->conductor->Size = System::Drawing::Size(38, 20);
            this->conductor->TabIndex = 20;
            this->conductor->ValueChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::ValueChanged);
            // 
            // lastRinger
            // 
            this->lastRinger->Location = System::Drawing::Point(176, 17);
            this->lastRinger->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, System::Int32::MinValue });
            this->lastRinger->Name = L"lastRinger";
            this->lastRinger->Size = System::Drawing::Size(38, 20);
            this->lastRinger->TabIndex = 19;
            this->lastRinger->ValueChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::ValueChanged);
            // 
            // firstRinger
            // 
            this->firstRinger->Location = System::Drawing::Point(71, 17);
            this->firstRinger->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, System::Int32::MinValue });
            this->firstRinger->Name = L"firstRinger";
            this->firstRinger->Size = System::Drawing::Size(38, 20);
            this->firstRinger->TabIndex = 18;
            this->firstRinger->ValueChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::ValueChanged);
            // 
            // ConductorLbl
            // 
            ConductorLbl->AutoSize = true;
            ConductorLbl->Location = System::Drawing::Point(9, 45);
            ConductorLbl->Name = L"ConductorLbl";
            ConductorLbl->Size = System::Drawing::Size(56, 13);
            ConductorLbl->TabIndex = 999;
            ConductorLbl->Text = L"Conductor";
            // 
            // lastRingerLbl
            // 
            lastRingerLbl->AutoSize = true;
            lastRingerLbl->Location = System::Drawing::Point(143, 19);
            lastRingerLbl->Name = L"lastRingerLbl";
            lastRingerLbl->Size = System::Drawing::Size(27, 13);
            lastRingerLbl->TabIndex = 999;
            lastRingerLbl->Text = L"Last";
            // 
            // firstRingerLbl
            // 
            firstRingerLbl->AutoSize = true;
            firstRingerLbl->Location = System::Drawing::Point(39, 19);
            firstRingerLbl->Name = L"firstRingerLbl";
            firstRingerLbl->Size = System::Drawing::Size(26, 13);
            firstRingerLbl->TabIndex = 999;
            firstRingerLbl->Text = L"First";
            // 
            // timeGroup
            // 
            timeGroup->Controls->Add(this->singleTimeField);
            timeGroup->Controls->Add(this->minutes);
            timeGroup->Controls->Add(this->hours);
            timeGroup->Controls->Add(minutesLbl);
            timeGroup->Controls->Add(this->hoursLbl);
            timeGroup->Location = System::Drawing::Point(135, 6);
            timeGroup->Name = L"timeGroup";
            timeGroup->Size = System::Drawing::Size(123, 96);
            timeGroup->TabIndex = 28;
            timeGroup->TabStop = false;
            timeGroup->Text = L"Time";
            // 
            // singleTimeField
            // 
            this->singleTimeField->AutoSize = true;
            this->singleTimeField->Location = System::Drawing::Point(13, 70);
            this->singleTimeField->Name = L"singleTimeField";
            this->singleTimeField->Size = System::Drawing::Size(99, 17);
            this->singleTimeField->TabIndex = 1001;
            this->singleTimeField->Text = L"Single time field";
            this->toolTip1->SetToolTip(this->singleTimeField, L"Use a single field for date, rather than one for day, one for month and one for y"
                L"ear");
            this->singleTimeField->UseVisualStyleBackColor = true;
            this->singleTimeField->CheckedChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::singleTimeField_CheckedChanged);
            // 
            // minutes
            // 
            this->minutes->Location = System::Drawing::Point(76, 43);
            this->minutes->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, System::Int32::MinValue });
            this->minutes->Name = L"minutes";
            this->minutes->Size = System::Drawing::Size(38, 20);
            this->minutes->TabIndex = 30;
            this->minutes->ValueChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::ValueChanged);
            // 
            // hours
            // 
            this->hours->Location = System::Drawing::Point(76, 17);
            this->hours->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, System::Int32::MinValue });
            this->hours->Name = L"hours";
            this->hours->Size = System::Drawing::Size(38, 20);
            this->hours->TabIndex = 29;
            this->hours->ValueChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::ValueChanged);
            // 
            // minutesLbl
            // 
            minutesLbl->AutoSize = true;
            minutesLbl->Location = System::Drawing::Point(26, 45);
            minutesLbl->Name = L"minutesLbl";
            minutesLbl->Size = System::Drawing::Size(44, 13);
            minutesLbl->TabIndex = 999;
            minutesLbl->Text = L"Minutes";
            // 
            // hoursLbl
            // 
            this->hoursLbl->AutoSize = true;
            this->hoursLbl->Location = System::Drawing::Point(35, 19);
            this->hoursLbl->Name = L"hoursLbl";
            this->hoursLbl->Size = System::Drawing::Size(35, 13);
            this->hoursLbl->TabIndex = 999;
            this->hoursLbl->Text = L"Hours";
            // 
            // generalGroup
            // 
            generalGroup->Controls->Add(this->splicedMethods);
            generalGroup->Controls->Add(splicedMethodsLbl);
            generalGroup->Controls->Add(this->tenorKey);
            generalGroup->Controls->Add(tenorKeyLbl);
            generalGroup->Controls->Add(this->footnotes);
            generalGroup->Controls->Add(footnotesLbl);
            generalGroup->Controls->Add(this->association);
            generalGroup->Controls->Add(this->changes);
            generalGroup->Controls->Add(associationLbl);
            generalGroup->Controls->Add(changesLbl);
            generalGroup->Location = System::Drawing::Point(8, 6);
            generalGroup->Name = L"generalGroup";
            generalGroup->Size = System::Drawing::Size(147, 148);
            generalGroup->TabIndex = 23;
            generalGroup->TabStop = false;
            generalGroup->Text = L"General";
            // 
            // splicedMethods
            // 
            this->splicedMethods->Location = System::Drawing::Point(103, 121);
            this->splicedMethods->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, System::Int32::MinValue });
            this->splicedMethods->Name = L"splicedMethods";
            this->splicedMethods->Size = System::Drawing::Size(38, 20);
            this->splicedMethods->TabIndex = 1000;
            this->splicedMethods->ValueChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::ValueChanged);
            // 
            // splicedMethodsLbl
            // 
            splicedMethodsLbl->AutoSize = true;
            splicedMethodsLbl->Location = System::Drawing::Point(11, 123);
            splicedMethodsLbl->Name = L"splicedMethodsLbl";
            splicedMethodsLbl->Size = System::Drawing::Size(86, 13);
            splicedMethodsLbl->TabIndex = 1001;
            splicedMethodsLbl->Text = L"Spliced Methods";
            // 
            // tenorKey
            // 
            this->tenorKey->Location = System::Drawing::Point(103, 95);
            this->tenorKey->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, System::Int32::MinValue });
            this->tenorKey->Name = L"tenorKey";
            this->tenorKey->Size = System::Drawing::Size(38, 20);
            this->tenorKey->TabIndex = 27;
            this->tenorKey->ValueChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::ValueChanged);
            // 
            // tenorKeyLbl
            // 
            tenorKeyLbl->AutoSize = true;
            tenorKeyLbl->Location = System::Drawing::Point(42, 97);
            tenorKeyLbl->Name = L"tenorKeyLbl";
            tenorKeyLbl->Size = System::Drawing::Size(55, 13);
            tenorKeyLbl->TabIndex = 999;
            tenorKeyLbl->Text = L"Tenor key";
            // 
            // footnotes
            // 
            this->footnotes->Location = System::Drawing::Point(103, 69);
            this->footnotes->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, System::Int32::MinValue });
            this->footnotes->Name = L"footnotes";
            this->footnotes->Size = System::Drawing::Size(38, 20);
            this->footnotes->TabIndex = 26;
            this->footnotes->ValueChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::ValueChanged);
            // 
            // footnotesLbl
            // 
            footnotesLbl->AutoSize = true;
            footnotesLbl->Location = System::Drawing::Point(43, 71);
            footnotesLbl->Name = L"footnotesLbl";
            footnotesLbl->Size = System::Drawing::Size(54, 13);
            footnotesLbl->TabIndex = 999;
            footnotesLbl->Text = L"Footnotes";
            // 
            // association
            // 
            this->association->Location = System::Drawing::Point(103, 43);
            this->association->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, System::Int32::MinValue });
            this->association->Name = L"association";
            this->association->Size = System::Drawing::Size(38, 20);
            this->association->TabIndex = 25;
            this->association->ValueChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::ValueChanged);
            // 
            // changes
            // 
            this->changes->Location = System::Drawing::Point(103, 17);
            this->changes->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, System::Int32::MinValue });
            this->changes->Name = L"changes";
            this->changes->Size = System::Drawing::Size(38, 20);
            this->changes->TabIndex = 24;
            this->changes->ValueChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::ValueChanged);
            // 
            // associationLbl
            // 
            associationLbl->AutoSize = true;
            associationLbl->Location = System::Drawing::Point(36, 45);
            associationLbl->Name = L"associationLbl";
            associationLbl->Size = System::Drawing::Size(61, 13);
            associationLbl->TabIndex = 999;
            associationLbl->Text = L"Association";
            // 
            // changesLbl
            // 
            changesLbl->AutoSize = true;
            changesLbl->Location = System::Drawing::Point(48, 19);
            changesLbl->Name = L"changesLbl";
            changesLbl->Size = System::Drawing::Size(49, 13);
            changesLbl->TabIndex = 999;
            changesLbl->Text = L"Changes";
            // 
            // handbellGroup
            // 
            handbellGroup->Controls->Add(this->handbellAlternate);
            handbellGroup->Controls->Add(this->handbellStr);
            handbellGroup->Controls->Add(this->handbellField);
            handbellGroup->Controls->Add(handbellStringLbl);
            handbellGroup->Controls->Add(handbellFieldLbl);
            handbellGroup->Location = System::Drawing::Point(163, 6);
            handbellGroup->Margin = System::Windows::Forms::Padding(3, 3, 3, 12);
            handbellGroup->Name = L"handbellGroup";
            handbellGroup->Size = System::Drawing::Size(189, 98);
            handbellGroup->TabIndex = 31;
            handbellGroup->TabStop = false;
            handbellGroup->Text = L"Handbell";
            // 
            // handbellAlternate
            // 
            this->handbellAlternate->AutoSize = true;
            this->helpProvider1->SetHelpString(this->handbellAlternate, L"When checked use everyother bell from input for ringers.");
            this->handbellAlternate->Location = System::Drawing::Point(72, 72);
            this->handbellAlternate->Name = L"handbellAlternate";
            this->helpProvider1->SetShowHelp(this->handbellAlternate, true);
            this->handbellAlternate->Size = System::Drawing::Size(92, 17);
            this->handbellAlternate->TabIndex = 33;
            this->handbellAlternate->Text = L"Alternate bells";
            this->toolTip1->SetToolTip(this->handbellAlternate, L"When checked use everyother bell from input for ringers.");
            this->handbellAlternate->UseVisualStyleBackColor = true;
            this->handbellAlternate->CheckedChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::CheckedChanged);
            // 
            // handbellStr
            // 
            this->helpProvider1->SetHelpString(this->handbellStr, L"The string this field will contain, when this is a handbell peal");
            this->handbellStr->Location = System::Drawing::Point(72, 43);
            this->handbellStr->Name = L"handbellStr";
            this->helpProvider1->SetShowHelp(this->handbellStr, true);
            this->handbellStr->Size = System::Drawing::Size(107, 20);
            this->handbellStr->TabIndex = 34;
            this->toolTip1->SetToolTip(this->handbellStr, L"The string this field will contain, when this is a handbell peal");
            this->handbellStr->TextChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::TextChanged);
            // 
            // handbellField
            // 
            this->handbellField->Location = System::Drawing::Point(72, 17);
            this->handbellField->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, System::Int32::MinValue });
            this->handbellField->Name = L"handbellField";
            this->handbellField->Size = System::Drawing::Size(38, 20);
            this->handbellField->TabIndex = 32;
            this->handbellField->ValueChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::ValueChanged);
            // 
            // handbellStringLbl
            // 
            handbellStringLbl->AutoSize = true;
            handbellStringLbl->Location = System::Drawing::Point(30, 46);
            handbellStringLbl->Name = L"handbellStringLbl";
            handbellStringLbl->Size = System::Drawing::Size(34, 13);
            handbellStringLbl->TabIndex = 999;
            handbellStringLbl->Text = L"String";
            // 
            // handbellFieldLbl
            // 
            handbellFieldLbl->AutoSize = true;
            handbellFieldLbl->Location = System::Drawing::Point(22, 19);
            handbellFieldLbl->Name = L"handbellFieldLbl";
            handbellFieldLbl->Size = System::Drawing::Size(44, 13);
            handbellFieldLbl->TabIndex = 999;
            handbellFieldLbl->Text = L"Field no";
            // 
            // tabControl1
            // 
            this->tableLayoutPanel1->SetColumnSpan(tabControl1, 2);
            tabControl1->Controls->Add(detailsPage);
            tabControl1->Controls->Add(methodPage);
            tabControl1->Controls->Add(dateAndTimePage);
            tabControl1->Controls->Add(ringersPage);
            tabControl1->Controls->Add(towerPage);
            tabControl1->Controls->Add(defaultsPage);
            tabControl1->Dock = System::Windows::Forms::DockStyle::Fill;
            tabControl1->Location = System::Drawing::Point(3, 3);
            tabControl1->Name = L"tabControl1";
            tabControl1->SelectedIndex = 0;
            tabControl1->Size = System::Drawing::Size(381, 263);
            tabControl1->TabIndex = 37;
            // 
            // detailsPage
            // 
            detailsPage->Controls->Add(otherGroup);
            detailsPage->Controls->Add(referencesGroup);
            detailsPage->Controls->Add(generalGroup);
            detailsPage->Controls->Add(handbellGroup);
            detailsPage->Location = System::Drawing::Point(4, 22);
            detailsPage->Name = L"detailsPage";
            detailsPage->Padding = System::Windows::Forms::Padding(3);
            detailsPage->Size = System::Drawing::Size(373, 237);
            detailsPage->TabIndex = 0;
            detailsPage->Text = L"General details";
            detailsPage->UseVisualStyleBackColor = true;
            // 
            // decodeCheckBox
            // 
            this->decodeCheckBox->AutoSize = true;
            this->decodeCheckBox->Location = System::Drawing::Point(6, 65);
            this->decodeCheckBox->Name = L"decodeCheckBox";
            this->decodeCheckBox->Size = System::Drawing::Size(130, 17);
            this->decodeCheckBox->TabIndex = 34;
            this->decodeCheckBox->Text = L"Convert from multibyte";
            this->toolTip1->SetToolTip(this->decodeCheckBox, L"Anything containing a space must have quotes around it. This means you can use co"
                L"mmas in fields. However all fields must have quotes, or spaces will be ignored");
            this->decodeCheckBox->UseVisualStyleBackColor = true;
            this->decodeCheckBox->CheckedChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::CheckedChanged);
            // 
            // enforceQuotes
            // 
            this->enforceQuotes->AutoSize = true;
            this->enforceQuotes->Location = System::Drawing::Point(6, 42);
            this->enforceQuotes->Name = L"enforceQuotes";
            this->enforceQuotes->Size = System::Drawing::Size(98, 17);
            this->enforceQuotes->TabIndex = 33;
            this->enforceQuotes->Text = L"Enforce quotes";
            this->toolTip1->SetToolTip(this->enforceQuotes, L"Anything containing a space must have quotes around it. This means you can use co"
                L"mmas in fields. However all fields must have quotes, or spaces will be ignored");
            this->enforceQuotes->UseVisualStyleBackColor = true;
            this->enforceQuotes->CheckedChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::singleDateField_CheckedChanged);
            // 
            // ignoreFirstLine
            // 
            this->ignoreFirstLine->AutoSize = true;
            this->ignoreFirstLine->Location = System::Drawing::Point(6, 19);
            this->ignoreFirstLine->Name = L"ignoreFirstLine";
            this->ignoreFirstLine->Size = System::Drawing::Size(94, 17);
            this->ignoreFirstLine->TabIndex = 32;
            this->ignoreFirstLine->Text = L"Ignore first line";
            this->toolTip1->SetToolTip(this->ignoreFirstLine, L"Ignore the first line of the input file. The first line is often used for column "
                L"headers");
            this->ignoreFirstLine->UseVisualStyleBackColor = true;
            this->ignoreFirstLine->CheckedChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::CheckedChanged);
            // 
            // methodPage
            // 
            methodPage->Controls->Add(methodGroup);
            methodPage->Location = System::Drawing::Point(4, 22);
            methodPage->Name = L"methodPage";
            methodPage->Size = System::Drawing::Size(373, 181);
            methodPage->TabIndex = 4;
            methodPage->Text = L"Method";
            methodPage->UseVisualStyleBackColor = true;
            // 
            // methodGroup
            // 
            methodGroup->Controls->Add(this->singleMethodField);
            methodGroup->Controls->Add(this->methodStage);
            methodGroup->Controls->Add(this->methodType);
            methodGroup->Controls->Add(this->methodName);
            methodGroup->Controls->Add(stageLbl);
            methodGroup->Controls->Add(typeLbl);
            methodGroup->Controls->Add(methodNameLbl);
            methodGroup->Location = System::Drawing::Point(5, 3);
            methodGroup->Margin = System::Windows::Forms::Padding(3, 3, 12, 3);
            methodGroup->Name = L"methodGroup";
            methodGroup->Size = System::Drawing::Size(137, 125);
            methodGroup->TabIndex = 14;
            methodGroup->TabStop = false;
            methodGroup->Text = L"Method";
            // 
            // singleMethodField
            // 
            this->singleMethodField->AutoSize = true;
            this->singleMethodField->Location = System::Drawing::Point(9, 102);
            this->singleMethodField->Name = L"singleMethodField";
            this->singleMethodField->Size = System::Drawing::Size(115, 17);
            this->singleMethodField->TabIndex = 1000;
            this->singleMethodField->Text = L"Single method field";
            this->singleMethodField->UseVisualStyleBackColor = true;
            this->singleMethodField->CheckedChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::singleMethodField_CheckedChanged);
            // 
            // methodStage
            // 
            this->methodStage->Location = System::Drawing::Point(86, 72);
            this->methodStage->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, System::Int32::MinValue });
            this->methodStage->Name = L"methodStage";
            this->methodStage->Size = System::Drawing::Size(38, 20);
            this->methodStage->TabIndex = 16;
            this->methodStage->ValueChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::ValueChanged);
            // 
            // methodType
            // 
            this->methodType->Location = System::Drawing::Point(86, 46);
            this->methodType->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, System::Int32::MinValue });
            this->methodType->Name = L"methodType";
            this->methodType->Size = System::Drawing::Size(38, 20);
            this->methodType->TabIndex = 15;
            this->methodType->ValueChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::ValueChanged);
            // 
            // methodName
            // 
            this->methodName->Location = System::Drawing::Point(86, 20);
            this->methodName->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, System::Int32::MinValue });
            this->methodName->Name = L"methodName";
            this->methodName->Size = System::Drawing::Size(38, 20);
            this->methodName->TabIndex = 14;
            this->methodName->ValueChanged += gcnew System::EventHandler(this, &CSVImportSettingsUI::ValueChanged);
            // 
            // stageLbl
            // 
            stageLbl->AutoSize = true;
            stageLbl->Location = System::Drawing::Point(45, 74);
            stageLbl->Name = L"stageLbl";
            stageLbl->Size = System::Drawing::Size(35, 13);
            stageLbl->TabIndex = 999;
            stageLbl->Text = L"Stage";
            // 
            // typeLbl
            // 
            typeLbl->AutoSize = true;
            typeLbl->Location = System::Drawing::Point(49, 48);
            typeLbl->Name = L"typeLbl";
            typeLbl->Size = System::Drawing::Size(31, 13);
            typeLbl->TabIndex = 999;
            typeLbl->Text = L"Type";
            // 
            // methodNameLbl
            // 
            methodNameLbl->AutoSize = true;
            methodNameLbl->Location = System::Drawing::Point(45, 22);
            methodNameLbl->Name = L"methodNameLbl";
            methodNameLbl->Size = System::Drawing::Size(35, 13);
            methodNameLbl->TabIndex = 999;
            methodNameLbl->Text = L"Name";
            // 
            // dateAndTimePage
            // 
            dateAndTimePage->Controls->Add(dateGroup);
            dateAndTimePage->Controls->Add(timeGroup);
            dateAndTimePage->Location = System::Drawing::Point(4, 22);
            dateAndTimePage->Name = L"dateAndTimePage";
            dateAndTimePage->Padding = System::Windows::Forms::Padding(3);
            dateAndTimePage->Size = System::Drawing::Size(373, 181);
            dateAndTimePage->TabIndex = 1;
            dateAndTimePage->Text = L"Date and time";
            dateAndTimePage->UseVisualStyleBackColor = true;
            // 
            // ringersPage
            // 
            ringersPage->Controls->Add(ringersGroup);
            ringersPage->Location = System::Drawing::Point(4, 22);
            ringersPage->Name = L"ringersPage";
            ringersPage->Size = System::Drawing::Size(373, 181);
            ringersPage->TabIndex = 2;
            ringersPage->Text = L"Ringers";
            ringersPage->UseVisualStyleBackColor = true;
            // 
            // towerPage
            // 
            towerPage->Controls->Add(towerGroup);
            towerPage->Controls->Add(tenorGroup);
            towerPage->Location = System::Drawing::Point(4, 22);
            towerPage->Name = L"towerPage";
            towerPage->Size = System::Drawing::Size(373, 181);
            towerPage->TabIndex = 3;
            towerPage->Text = L"Tower details";
            towerPage->UseVisualStyleBackColor = true;
            // 
            // defaultsPage
            // 
            defaultsPage->Controls->Add(resetSettingsBtn);
            defaultsPage->Controls->Add(defaultsBtn);
            defaultsPage->Controls->Add(pealBookSettingsBtn);
            defaultsPage->Location = System::Drawing::Point(4, 22);
            defaultsPage->Name = L"defaultsPage";
            defaultsPage->Padding = System::Windows::Forms::Padding(3);
            defaultsPage->Size = System::Drawing::Size(373, 181);
            defaultsPage->TabIndex = 5;
            defaultsPage->Text = L"Presets";
            defaultsPage->UseVisualStyleBackColor = true;
            // 
            // resetSettingsBtn
            // 
            resetSettingsBtn->Location = System::Drawing::Point(7, 67);
            resetSettingsBtn->Name = L"resetSettingsBtn";
            resetSettingsBtn->Size = System::Drawing::Size(196, 23);
            resetSettingsBtn->TabIndex = 2;
            resetSettingsBtn->Text = L"Disable all settings";
            this->toolTip1->SetToolTip(resetSettingsBtn, L"Disable all settings, useful to start again if you\'re trying to work out a format"
                L".");
            resetSettingsBtn->UseVisualStyleBackColor = true;
            resetSettingsBtn->Click += gcnew System::EventHandler(this, &CSVImportSettingsUI::resetSettingsBtn_Click);
            // 
            // defaultsBtn
            // 
            defaultsBtn->Location = System::Drawing::Point(7, 37);
            defaultsBtn->Name = L"defaultsBtn";
            defaultsBtn->Size = System::Drawing::Size(196, 23);
            defaultsBtn->TabIndex = 1;
            defaultsBtn->Text = L"Default settings";
            this->toolTip1->SetToolTip(defaultsBtn, L"Use duco default settings");
            defaultsBtn->UseVisualStyleBackColor = true;
            defaultsBtn->Click += gcnew System::EventHandler(this, &CSVImportSettingsUI::defaultsBtn_Click);
            // 
            // pealBookSettingsBtn
            // 
            pealBookSettingsBtn->Location = System::Drawing::Point(7, 7);
            pealBookSettingsBtn->Name = L"pealBookSettingsBtn";
            pealBookSettingsBtn->Size = System::Drawing::Size(196, 23);
            pealBookSettingsBtn->TabIndex = 0;
            pealBookSettingsBtn->Text = L"Pealbook settings";
            this->toolTip1->SetToolTip(pealBookSettingsBtn, L"Configure to known pealbase settings");
            pealBookSettingsBtn->UseVisualStyleBackColor = true;
            pealBookSettingsBtn->Click += gcnew System::EventHandler(this, &CSVImportSettingsUI::pealBookSettingsBtn_Click);
            // 
            // saveBtn
            // 
            this->saveBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->saveBtn->Enabled = false;
            this->saveBtn->Location = System::Drawing::Point(229, 272);
            this->saveBtn->Name = L"saveBtn";
            this->saveBtn->Size = System::Drawing::Size(75, 23);
            this->saveBtn->TabIndex = 35;
            this->saveBtn->Text = L"Save";
            this->saveBtn->UseVisualStyleBackColor = true;
            this->saveBtn->Click += gcnew System::EventHandler(this, &CSVImportSettingsUI::saveBtn_Click);
            // 
            // tableLayoutPanel1
            // 
            this->tableLayoutPanel1->ColumnCount = 2;
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
                100)));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
                80)));
            this->tableLayoutPanel1->Controls->Add(cancelBtn, 1, 1);
            this->tableLayoutPanel1->Controls->Add(tabControl1, 0, 0);
            this->tableLayoutPanel1->Controls->Add(this->saveBtn, 0, 1);
            this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            this->tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
            this->tableLayoutPanel1->RowCount = 2;
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 30)));
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
            this->tableLayoutPanel1->Size = System::Drawing::Size(387, 299);
            this->tableLayoutPanel1->TabIndex = 38;
            // 
            // referencesGroup
            // 
            referencesGroup->Controls->Add(this->ringingWorldReference);
            referencesGroup->Controls->Add(this->bellBoardReference);
            referencesGroup->Controls->Add(ringingWorldLabel);
            referencesGroup->Controls->Add(bellboardLabel);
            referencesGroup->Location = System::Drawing::Point(8, 161);
            referencesGroup->Name = L"referencesGroup";
            referencesGroup->Size = System::Drawing::Size(147, 70);
            referencesGroup->TabIndex = 35;
            referencesGroup->TabStop = false;
            referencesGroup->Text = L"References";
            // 
            // bellboardLabel
            // 
            bellboardLabel->AutoSize = true;
            bellboardLabel->Location = System::Drawing::Point(42, 16);
            bellboardLabel->Name = L"bellboardLabel";
            bellboardLabel->Size = System::Drawing::Size(55, 13);
            bellboardLabel->TabIndex = 0;
            bellboardLabel->Text = L"Bell Board";
            // 
            // ringingWorldLabel
            // 
            ringingWorldLabel->AutoSize = true;
            ringingWorldLabel->Location = System::Drawing::Point(23, 41);
            ringingWorldLabel->Name = L"ringingWorldLabel";
            ringingWorldLabel->Size = System::Drawing::Size(74, 13);
            ringingWorldLabel->TabIndex = 1;
            ringingWorldLabel->Text = L"Ringing World";
            // 
            // bellBoardReference
            // 
            this->bellBoardReference->Location = System::Drawing::Point(103, 13);
            this->bellBoardReference->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, System::Int32::MinValue });
            this->bellBoardReference->Name = L"bellBoardReference";
            this->bellBoardReference->Size = System::Drawing::Size(38, 20);
            this->bellBoardReference->TabIndex = 25;
            // 
            // ringingWorldReference
            // 
            this->ringingWorldReference->Location = System::Drawing::Point(103, 39);
            this->ringingWorldReference->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, System::Int32::MinValue });
            this->ringingWorldReference->Name = L"ringingWorldReference";
            this->ringingWorldReference->Size = System::Drawing::Size(38, 20);
            this->ringingWorldReference->TabIndex = 26;
            // 
            // otherGroup
            // 
            otherGroup->Controls->Add(this->ignoreFirstLine);
            otherGroup->Controls->Add(this->enforceQuotes);
            otherGroup->Controls->Add(this->decodeCheckBox);
            otherGroup->Location = System::Drawing::Point(163, 115);
            otherGroup->Name = L"otherGroup";
            otherGroup->Size = System::Drawing::Size(189, 87);
            otherGroup->TabIndex = 36;
            otherGroup->TabStop = false;
            otherGroup->Text = L"Other";
            // 
            // CSVImportSettingsUI
            // 
            this->AcceptButton = this->saveBtn;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            this->CancelButton = cancelBtn;
            this->ClientSize = System::Drawing::Size(387, 299);
            this->Controls->Add(this->tableLayoutPanel1);
            this->MaximizeBox = false;
            this->MinimizeBox = false;
            this->Name = L"CSVImportSettingsUI";
            this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
            this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
            this->Text = L"CSV Import settings - field numbers";
            this->TopMost = true;
            this->Load += gcnew System::EventHandler(this, &CSVImportSettingsUI::CSVImportSettingsUI_Load);
            towerGroup->ResumeLayout(false);
            towerGroup->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->countyField))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cityField))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->nameField))->EndInit();
            tenorGroup->ResumeLayout(false);
            tenorGroup->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->lbs))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->quarters))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cwt))->EndInit();
            dateGroup->ResumeLayout(false);
            dateGroup->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->year))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->month))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->day))->EndInit();
            ringersGroup->ResumeLayout(false);
            ringersGroup->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->composer))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->conductor))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->lastRinger))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->firstRinger))->EndInit();
            timeGroup->ResumeLayout(false);
            timeGroup->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->minutes))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->hours))->EndInit();
            generalGroup->ResumeLayout(false);
            generalGroup->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splicedMethods))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->tenorKey))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->footnotes))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->association))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->changes))->EndInit();
            handbellGroup->ResumeLayout(false);
            handbellGroup->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->handbellField))->EndInit();
            tabControl1->ResumeLayout(false);
            detailsPage->ResumeLayout(false);
            methodPage->ResumeLayout(false);
            methodGroup->ResumeLayout(false);
            methodGroup->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->methodStage))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->methodType))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->methodName))->EndInit();
            dateAndTimePage->ResumeLayout(false);
            ringersPage->ResumeLayout(false);
            towerPage->ResumeLayout(false);
            defaultsPage->ResumeLayout(false);
            this->tableLayoutPanel1->ResumeLayout(false);
            referencesGroup->ResumeLayout(false);
            referencesGroup->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bellBoardReference))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ringingWorldReference))->EndInit();
            otherGroup->ResumeLayout(false);
            otherGroup->PerformLayout();
            this->ResumeLayout(false);

        }
#pragma endregion
    };
}
