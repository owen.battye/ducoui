#include "PrintAssociationReportUtil.h"

#include "AssociationReportOptions.h"
#include "DucoUIUtils.h"

#include "DatabaseManager.h"
#include <StatisticFilters.h>
#include <RingingDatabase.h>
#include <PealDatabase.h>

using namespace DucoUI;
using namespace System::Drawing::Printing;
using namespace System;
using namespace System::Windows::Forms;
using namespace System::Drawing::Text;

AssociationReportOptions::AssociationReportOptions(DucoUI::DatabaseManager^ newDatabase)
{
    additionalPealIds = new std::set<Duco::ObjectId>();
    database = newDatabase;
    InitializeComponent();
    SetDefaults();
    document = gcnew PrintDocument;
    Int32 defaultMargin = 6 * 4;
    document->DefaultPageSettings->Margins = gcnew System::Drawing::Printing::Margins(defaultMargin, defaultMargin, defaultMargin, defaultMargin);
}

AssociationReportOptions::!AssociationReportOptions()
{
    delete additionalPealIds;
}

AssociationReportOptions::~AssociationReportOptions()
{
    this->!AssociationReportOptions();
    if (components)
    {
        delete components;
    }
}

System::Void
AssociationReportOptions::closeBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    Close();
}

System::Void
AssociationReportOptions::SetDefaults()
{
    std::set<unsigned int> years;
    Duco::StatisticFilters filters(database->Database());
    database->Database().PealsDatabase().GetYearRange(filters, years);
    std::set<unsigned int>::const_iterator it = years.begin();
    while (it != years.end())
    {
        reportYear->Items->Add(Convert::ToString(*it));
        if (*it == (System::DateTime::Now.Year-1))
        {
            reportYear->Text = Convert::ToString(*it);
        }
        ++it;
    }

    fontSelector->BeginUpdate();
    InstalledFontCollection^ fonts = gcnew InstalledFontCollection();
    String^ defaultFont = System::Drawing::SystemFonts::DefaultFont->Name;
    for (int i = 0; i < fonts->Families->Length; ++i)
    {
        System::Drawing::FontFamily^ family = fonts->Families[i];
        fontSelector->Items->Add(family->Name);
        if (family->Name == "Times New Roman")
        {
            defaultFont = family->Name;
        }
    }
    fontSelector->Text = defaultFont;
    fontSelector->EndUpdate();
}

System::Void
AssociationReportOptions::fieldsUpdated(System::Object^ sender, System::EventArgs^ e)
{
    statusLabel->Text = "Reporting on ";

    Int32 pealYear = KNoId;
    std::set<Duco::ObjectId> pealIds;
    if (Int32::TryParse(reportYear->Text, pealYear))
    {
        database->Database().PealsDatabase().GetPealsForYear(pealYear, pealIds);
        statusLabel->Text += Convert::ToString(pealIds.size()) + " " + database->PerformanceString(true) + " from " + Convert::ToString(pealYear);
    }

    array<Char>^ commaSeperators = gcnew array<Char>{
        ','
    };
    array<Char>^ dashSeperators = gcnew array<Char>{
        '-'
    };
    array<String^>^ result = extraPealIds->Text->Split(commaSeperators, StringSplitOptions::RemoveEmptyEntries);
    additionalPealIds->clear();
    System::Collections::IEnumerator^ myEnum = result->GetEnumerator();
    while (myEnum->MoveNext())
    {
        String^ entry = safe_cast<String^>(myEnum->Current);
        Int64 pealId = KNoId;
        if (Int64::TryParse(entry, pealId))
        {
            if (database->Database().PealsDatabase().ObjectExists(pealId))
                additionalPealIds->insert(pealId);
        }
        else if (entry->IndexOf("-") > -1)
        {
            array<String^>^ rangeResult = entry->Split(dashSeperators, StringSplitOptions::RemoveEmptyEntries);
            if (rangeResult->Length == 2)
            {
                Int64 startPealId = KNoId;
                Int64 endPealId = KNoId;
                if (Int64::TryParse(rangeResult[0], startPealId) && Int64::TryParse(rangeResult[1], endPealId))
                {
                    while (startPealId <= endPealId)
                    {
                        if (database->Database().PealsDatabase().ObjectExists(startPealId))
                            additionalPealIds->insert(startPealId);
                        ++startPealId;
                    }
                }
            }
        }
    }
    if (additionalPealIds->size() > 0)
    {
        if (statusLabel->Text->Length > 13)
        {
            statusLabel->Text += " and ";
        }
        statusLabel->Text += Convert::ToString(additionalPealIds->size()) + " additional peal";
        if (additionalPealIds->size() > 1)
            statusLabel->Text += "s";
    }
}

System::Void 
AssociationReportOptions::printBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    PrintDialog^ printDialog = gcnew PrintDialog();
    printDialog->UseEXDialog = true;
    printDialog->AllowSomePages = true;
    printDialog->Document = document;
    printDialog->Document->PrintPage += gcnew System::Drawing::Printing::PrintPageEventHandler(this, &AssociationReportOptions::printAssociationReport);
    if (printDialog->ShowDialog() == ::DialogResult::OK)
    {
        printDialog->Document->Print();
    }
}

System::Void
AssociationReportOptions::printPreviewBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    PrintPreviewDialog^ printPreviewDialog = gcnew PrintPreviewDialog();
    printPreviewDialog->Icon = this->Icon;
    printPreviewDialog->Document = document;
    printPreviewDialog->Document->PrintPage += gcnew System::Drawing::Printing::PrintPageEventHandler(this, &AssociationReportOptions::printAssociationReport);
    printPreviewDialog->Document->EndPrint += gcnew System::Drawing::Printing::PrintEventHandler(this, &AssociationReportOptions::clearReport);
    printPreviewDialog->ShowDialog();
}

System::Void
AssociationReportOptions::clearReport(System::Object^ sender, System::Drawing::Printing::PrintEventArgs^ args)
{
    printAssociationReportUtil = nullptr;
    PrintDocument^ oldDoc = document;
    document = gcnew PrintDocument;
    document->PrinterSettings = oldDoc->PrinterSettings;
    document->DefaultPageSettings = oldDoc->DefaultPageSettings;
}

System::Void
AssociationReportOptions::printAssociationReport(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args)
{
    PrintDocument^ printDoc = static_cast<PrintDocument^>(sender);
    if (printAssociationReportUtil == nullptr)
    {
        printAssociationReportUtil = gcnew PrintAssociationReportUtil(database, args, Decimal::ToSingle(fontSize->Value), Decimal::ToInt32(columnCount->Value), Decimal::ToSingle(columnSpacing->Value), fontSelector->Text);
        Int32 pealNumbering = KNoId;
        if (numberingStart->Text->Length > 0)
            Int32::TryParse(numberingStart->Text, pealNumbering);
        Int32 pealYear = KNoId;
        if (Int32::TryParse(reportYear->Text, pealYear) && pealYear != KNoId)
        {
            printAssociationReportUtil->FindPeals(pealYear, *additionalPealIds, pealNumbering);
        }
    }
    printAssociationReportUtil->printObject(args, printDoc->PrinterSettings);
}

System::Void
AssociationReportOptions::numberingStart_Validating(System::Object^ sender, System::ComponentModel::CancelEventArgs^ e)
{
    Int64 pealNumbering = KNoId;
    if (numberingStart->Text->Length > 0)
        e->Cancel = !Int64::TryParse(safe_cast<System::Windows::Forms::TextBox^>(sender)->Text, pealNumbering);

    if (e->Cancel)
    {
        this->errorProvider1->SetError(numberingStart, String::Format("Cannot convert {0} to number", numberingStart->Text));
    }
}

System::Void
AssociationReportOptions::numberingStart_Validated(Object^ sender, System::EventArgs^ e)
{
    // If all conditions have been met, clear the ErrorProvider of errors.
    this->errorProvider1->SetError(numberingStart, "");
}

System::Void
AssociationReportOptions::AssociationReportOptions_FormClosing(System::Object^ sender, System::Windows::Forms::FormClosingEventArgs^ e)
{
    e->Cancel = false;
}

System::Void
AssociationReportOptions::pageSetupBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    System::Windows::Forms::PageSetupDialog^ pageSetupDialog = gcnew System::Windows::Forms::PageSetupDialog();
    pageSetupDialog->PageSettings = document->DefaultPageSettings;
    pageSetupDialog->EnableMetric = true;
    pageSetupDialog->ShowNetwork = false;

    pageSetupDialog->ShowDialog();
}
