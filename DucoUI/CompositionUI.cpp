#include <Composition.h>
#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"
#include "PrintCompositionUtil.h"
#include "DucoWindowState.h"
#include "ProgressWrapper.h"

#include "CompositionUI.h"

#include "SoundUtils.h"
#include <RingingDatabase.h>
#include "SystemDefaultIcon.h"
#include "DatabaseGenUtils.h"
#include "DucoUtils.h"
#include "DucoUIUtils.h"
#include <LeadOrder.h>
#include <PlaceNotation.h>
#include "InputIndexQueryUI.h"
#include <Change.h>
#include <Course.h>
#include "WindowsSettings.h"
#include <DucoConfiguration.h>
#include <DatabaseSettings.h>
#include <DucoEngineUtils.h>

using namespace Duco;
using namespace DucoUI;
using namespace System::Drawing::Printing;
using namespace System;
using namespace System::Windows::Forms;

namespace DucoUI
{
    ref class CompositionUIArgs
    {
    public:
        Duco::Composition*  composition;
    };
}

CompositionUI::CompositionUI(DatabaseManager^ theDatabase)
:   database(theDatabase), windowState(DucoWindowState::EViewMode),
    dataChanged(false), populatingView(false), printUtil(nullptr)
{
    startingChangeBells = new std::set<unsigned int>();
    progressWrapper = new ProgressCallbackWrapper(this);
    composition = new Duco::Composition(-1, database->Database().MethodsDatabase());
    InitializeComponent();
    allMethodIds = gcnew System::Collections::Generic::List<System::Int16>();
    ringerCache = DucoUI::RingerDisplayCache::CreateDisplayCacheWithConductors(database, true);
    preNewId = new Duco::ObjectId();
}

CompositionUI::CompositionUI(DatabaseManager^ theDatabase, const Duco::ObjectId& compositionToShowId)
:   database(theDatabase), windowState(DucoWindowState::EViewMode),
    dataChanged(false), populatingView(false), printUtil(nullptr)
{
    startingChangeBells = new std::set<unsigned int>();
    progressWrapper = new ProgressCallbackWrapper(this);
    composition = new Duco::Composition(-1, database->Database().MethodsDatabase());
    database->FindComposition(compositionToShowId, *composition, false);
    InitializeComponent();
    allMethodIds = gcnew System::Collections::Generic::List<System::Int16>();
    ringerCache = DucoUI::RingerDisplayCache::CreateDisplayCacheWithConductors(database, true);
    preNewId = new Duco::ObjectId();
}


CompositionUI::!CompositionUI()
{
    delete preNewId;
    delete composition;
    delete progressWrapper;
    delete startingChangeBells;
}

CompositionUI::~CompositionUI()
{
    this->!CompositionUI();
    if (components)
    {
        delete components;
    }
}

void
CompositionUI::ShowComposition(DatabaseManager^ theDatabase, System::Windows::Forms::Form^ parent, const Duco::ObjectId& compositionId)
{
    if (compositionId.ValidId())
    {
        CompositionUI^ seriesUI = gcnew CompositionUI(theDatabase, compositionId);
        seriesUI->MdiParent = parent;
        seriesUI->Show();
    }
}

System::Void
CompositionUI::ClosingEvent(System::Object^ object, System::ComponentModel::CancelEventArgs^ e)
{
    if (generator->IsBusy)
    {
        generator->CancelAsync();
        e->Cancel = true;
        SoundUtils::PlayErrorSound();
    }
    else if (dataChanged)
    {
        if (DucoUtils::ConfirmLooseChanges("composition", this))
        {
            e->Cancel = true;
        }
    }
    if (!e->Cancel)
    {
        database->CancelEdit(TObjectType::EComposition, composition->Id());
        database->RemoveObserver(this);
    }
}

System::Void
CompositionUI::FormLoad(System::Object^  sender, System::EventArgs^  e)
{
    this->Icon = SystemDefaultIcon::DefaultSystemIcon();

    DatabaseGenUtils::GenerateMethodOptions(allMethodIds, methodSelector, database, -1, -1, true, false, true);
    ringerCache->PopulateControl(composerSelector, false);
    if (composition->Id() != -1)
    {
        PopulateView();
    }
    else
    {
        startBtn_Click(sender, e);
    }
    SetViewState();

    database->AddObserver(this);
    if (allMethodIds->Count < 1)
    {
        MessageBox::Show(this, "No methods have been defined with valid place notation, inc. for a bob and single. You cannot define any compositions until you add the place notation.", "No methods defined");
    }
    else if (allMethodIds->Count < 10)
    {
        MessageBox::Show(this, "Very few  methods have been defined with valid place notation, inc. for a bob and single.", "Few methods defined");
    }
}

System::Void
CompositionUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    switch (windowState)
    {
    case DucoWindowState::EViewMode:
        Close();
        break;

    case DucoWindowState::ENewMode:
        PopulateViewWithId(*preNewId);
        SetViewState();
        break;
    
    case DucoWindowState::EEditMode:
        database->CancelEdit(TObjectType::EComposition, composition->Id());
        // no break on purpose

    default:
        PopulateViewWithId(composition->Id());
        SetViewState();
        break;
    }
}

System::Void
CompositionUI::startBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId objectId;
    if (!composition->ProveInProgress() && database->FirstComposition(objectId, false))
    {
        PopulateViewWithId(objectId);
    }
    else
    {
        ClearView();
        SoundUtils::PlayErrorSound();
    }
}

System::Void
CompositionUI::previousBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId objectId (composition->Id());
    if (!composition->ProveInProgress() && database->NextComposition(objectId, false, false))
    {
        PopulateViewWithId(objectId);
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
CompositionUI::nextBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId objectId (composition->Id());
    if (!composition->ProveInProgress() && database->NextComposition(objectId, true, false))
    {
        PopulateViewWithId(objectId);
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
CompositionUI::forward10Btn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId objectId (composition->Id());
    if (!composition->ProveInProgress() && database->ForwardMultipleCompositions(objectId, true))
    {
        PopulateViewWithId(objectId);
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
CompositionUI::back10Btn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId objectId (composition->Id());
    if (!composition->ProveInProgress() && database->ForwardMultipleCompositions(objectId, false))
    {
        PopulateViewWithId(objectId);
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
CompositionUI::endBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId objectId;
    if (!composition->ProveInProgress() && database->LastComposition(objectId))
    {
        PopulateViewWithId(objectId);
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
CompositionUI::saveBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (dataChanged)
    {
        if (composition->Valid(database->Database(), false, true))
        {
            if (windowState == DucoWindowState::EEditMode)
            {
                if (database->UpdateComposition(*composition, true))
                {
                    PopulateViewWithId(composition->Id());
                    SetViewState();
                }
            }
            else if (windowState == DucoWindowState::ENewMode)
            {
                PopulateViewWithId(database->AddComposition(*composition));
                SetViewState();
            }
        }
        else
        {
            MessageBox::Show(this, DucoUtils::ConvertString(composition->ErrorString(database->Database().Settings(), database->Config().IncludeWarningsInValidation())), "Composition is missing data");
            SoundUtils::PlayErrorSound();
        }
    }
}

System::Void
CompositionUI::editBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (!composition->ProveInProgress() && database->StartEdit(TObjectType::EComposition, composition->Id()))
    {
        PopulateView();
        SetEditState();
    }
    else
        SoundUtils::PlayErrorSound();
}

System::Void
CompositionUI::newBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    *preNewId = composition->Id();
    ClearView();
    SetNewState();
    UpdateIdLabel();
}

System::Void
CompositionUI::proveBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (generator->IsBusy)
    {
        SoundUtils::PlayErrorSound();
    }
    else
    {
        CompositionUIArgs^ args = gcnew CompositionUIArgs();
        args->composition = composition;
        generator->RunWorkerAsync(args);
    }
}

System::Void
CompositionUI::printBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    DucoUIUtils::Print(this, sender, gcnew System::Drawing::Printing::PrintPageEventHandler( this, &CompositionUI::printComposition ), "Composition", database->Database().Settings().UsePrintPreview(), false);
}

System::Void
CompositionUI::printComposition(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args)
{
    PrintDocument^ printDoc = static_cast<PrintDocument^>(sender);

    if (printUtil == nullptr)
    {
        printUtil = gcnew PrintCompositionUtil(database, args);
        printUtil->SetObjects(composition, dataGridView1);
        printUtil->printObject(args, printDoc->PrinterSettings);
    }
    else
    {
        printUtil->printObject(args, printDoc->PrinterSettings);
    }
    if (!args->HasMorePages)
    {
        printUtil = nullptr;
    }
}


System::Void
CompositionUI::dataGridView1_CellEndEdit(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    if (populatingView)
        return;

    composition->Clear();
    changesCount->Text = "";
    DataGridViewCell^ cell = dataGridView1->Rows[e->RowIndex]->Cells[e->ColumnIndex];

    String^ cellVal = Convert::ToString(cell->Value);
    if (cellVal != nullptr)
    {
        String^ newCellVal = "";
        CharEnumerator^ it = cellVal->GetEnumerator();
        while (it->MoveNext())
        {
            switch (it->Current)
            {
            default:
                break;
            case '1':
            case '2':
            case '3':
            case '4':
            case 's':
            case '-':
                newCellVal += it->Current;
                break;
            case 'x':
                newCellVal += "-";
                break;
            }
        }
        if (String::Compare(cellVal, newCellVal) != 0)
        {
            dataChanged = true;
            cell->Value = newCellVal;
            cellVal = newCellVal;
        }
    }

    GenerateComposition();
}

System::Void
CompositionUI::GenerateComposition()
{
    bool callsAdded (true);
    for (int i (0); callsAdded && i < dataGridView1->RowCount; ++i)
    {
        callsAdded = false;
        for (int j (1); j < dataGridView1->ColumnCount; ++j)
        {
            DataGridViewColumn^ column = dataGridView1->Columns[j];
            wchar_t currentChar = Convert::ToString(column->HeaderCell->Value)[0];
            callsAdded |= AddCallToComposition(dataGridView1[j, i], currentChar);
        }
        if (callsAdded)
        {
            dataChanged = true;
            composition->MoveToHome(); // Move to home so the right change is displayed on each row.
            dataGridView1->Rows[i]->Cells[0]->Value = DucoUtils::ConvertString(composition->EndChange(false, false));
        }
    }
    changesCount->Text = DucoUtils::ConvertString(DucoEngineUtils::ToString(composition->NoOfChanges(), true));
    snapFinish->Checked = composition->IsSnapFinish();
}

bool
CompositionUI::AddCallToComposition(System::Windows::Forms::DataGridViewCell^ currentCell, wchar_t position)
{
    try
    {
        if (currentCell->Value != nullptr)
        {
            Object^ obj = currentCell->Value;
            if (Type::GetTypeCode(obj->GetType()) == TypeCode::String)
            {
                String^ objStr = static_cast<String^>(obj);
                if (objStr->Length > 0)
                {
                    std::wstring callingStr;
                    DucoUtils::ConvertString(objStr, callingStr);
                    return composition->AddCalls(position, callingStr);
                }
            }
        }
    }
    catch (Exception^ /*Ex*/)
    {
    }
    return false;
}

System::Void
CompositionUI::methodSelector_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (populatingView)
        return;

    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        dataChanged = true;
        composition->SetMethodId(DatabaseGenUtils::FindId(allMethodIds, methodSelector->SelectedIndex));
        composition->PrepareToProve(database->Database().MethodsDatabase(), database->Database().Settings());
        RenameColumns();
        PopulateComposition();
    }
}

System::Void
CompositionUI::composerSelector_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (populatingView)
        return;

    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        dataChanged = true;
        Duco::ObjectId composerId;
        if (composerSelector->SelectedIndex != -1)
        {
            RingerItem^ composer = (RingerItem^)composerSelector->SelectedItem;
            composerId = composer->Id();
        }

        if (composerId.ValidId())
            composition->SetComposerId(composerId);
        else
        {
            std::wstring composerName;
            DucoUtils::ConvertString(composerSelector->Text, composerName);
            composition->SetComposerName(composerName);
        }
    }
}

System::Void
CompositionUI::RenameColumns()
{
    dataGridView1->Rows->Clear();
    while (dataGridView1->Columns->Count > 1)
    {
        dataGridView1->Columns->RemoveAt(1);
    }
    if (composition->MethodId().ValidId() && (composition->Id().ValidId() || windowState == DucoWindowState::ENewMode) )
    {
        Duco::LeadOrder leadOrder = composition->Notation().LeadOrder();
        DataGridViewTextBoxCell^ cellTemplate = gcnew DataGridViewTextBoxCell;
        DataGridViewCellStyle^ style = gcnew DataGridViewCellStyle;
        style->Alignment = DataGridViewContentAlignment::MiddleCenter;
        std::vector<unsigned int>::const_iterator it = leadOrder.Leads().begin();
        while (it != leadOrder.Leads().end())
        {
            DataGridViewColumn^ newColumn = gcnew DataGridViewColumn();
            newColumn->HeaderText = DucoUtils::ConvertString(composition->Notation().LeadOrder().CheckPosition(*it));
            newColumn->CellTemplate = cellTemplate;
            newColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            newColumn->Width = 35;
            newColumn->HeaderCell->Style = style;
            newColumn->DefaultCellStyle = style;
            dataGridView1->Columns->Add(newColumn);

            ++it;
        }
        leadEndColumn->HeaderCell->Value = DucoUtils::ConvertString(composition->StartingLeadEnd());
    }
}

System::Void
CompositionUI::PopulateComposition()
{
    dataGridView1->Rows->Clear();

    if (composition->Ready())
    {
        CompositionTable table = composition->CreateCompositionTable();

        Duco::LeadOrder leadOrder = table.LeadOrder();

        for (int courseNumber = 0; courseNumber < table.NoOfCourses(); ++courseNumber)
        {
            DataGridViewRow^ newRow = gcnew DataGridViewRow();
            DataGridViewCellCollection^ theCells = newRow->Cells;

            // Course end first
            DataGridViewTextBoxCell^ courseEndCell = gcnew DataGridViewTextBoxCell();
            courseEndCell->Value = DucoUtils::ConvertString(table.Course(courseNumber).CourseEnd().Str());
            theCells->Add(courseEndCell);

            std::vector<unsigned int>::const_iterator it = leadOrder.Leads().begin();
            while (it != leadOrder.Leads().end())
            {
                DataGridViewTextBoxCell^ callingsCell = gcnew DataGridViewTextBoxCell();
                callingsCell->Value = DucoUtils::ConvertString(table.Course(courseNumber).CallingStr(*it));
                theCells->Add(callingsCell);
                ++it;
            }

            dataGridView1->Rows->Add(newRow);
        }
        changesCount->Text = DucoUtils::ConvertString(DucoEngineUtils::ToString(composition->NoOfChanges(), true));
        snapFinish->Checked = composition->IsSnapFinish();
    }
}

void
CompositionUI::SetViewState()
{
    windowState = DucoWindowState::EViewMode;
    HideColumns();
    closeBtn->Text = "Close";

    endBtn->Enabled = true;
    back10Btn->Enabled = true;
    nextBtn->Enabled = true;
    previousBtn->Enabled = true;
    forward10Btn->Enabled = true;
    startBtn->Enabled = true;

    saveBtn->Visible = false;
    editBtn->Visible = true;
    editBtn->Enabled = composition->Id() != -1;
    printBtn->Visible = true;
    printBtn->Enabled = composition->Id() != -1;
    newBtn->Visible = true;
    newBtn->Enabled = true;
    proveBtn->Visible = true;
    proveBtn->Enabled = true;

    dataGridView1->ReadOnly = true;
    notesEditor->ReadOnly = true;
    methodSelector->Enabled = false;
    composerSelector->Enabled = false;
    nameEditor->Enabled = false;
    snapStart->Enabled = false;
    repeatsEditor->Enabled = false;
}

void
CompositionUI::SetEditState()
{
    windowState = DucoWindowState::EEditMode;
    HideColumns();
    closeBtn->Text = "Cancel";

    endBtn->Enabled = false;
    back10Btn->Enabled = false;
    nextBtn->Enabled = false;
    previousBtn->Enabled = false;
    forward10Btn->Enabled = false;
    startBtn->Enabled = false;

    saveBtn->Visible = true;
    saveBtn->Enabled = true;
    editBtn->Visible = false;
    newBtn->Visible = false;
    proveBtn->Visible = false;
    printBtn->Visible = false;

    dataGridView1->ReadOnly = false;
    notesEditor->ReadOnly = false;
    methodSelector->Enabled = true;
    composerSelector->Enabled = true;
    nameEditor->Enabled = true;
    repeatsEditor->Enabled = true;
    snapStart->Enabled = true;
    dataChanged = false;
}

void
CompositionUI::SetNewState()
{
    windowState = DucoWindowState::ENewMode;
    HideColumns();
    closeBtn->Text = "Cancel";

    endBtn->Enabled = false;
    back10Btn->Enabled = false;
    nextBtn->Enabled = false;
    previousBtn->Enabled = false;
    forward10Btn->Enabled = false;
    startBtn->Enabled = false;
    saveBtn->Visible = true;
    saveBtn->Enabled = true;
    editBtn->Visible = false;
    newBtn->Visible = false;
    proveBtn->Visible = false;
    printBtn->Visible = false;

    dataGridView1->ReadOnly = false;
    notesEditor->ReadOnly = false;
    methodSelector->Enabled = true;
    composerSelector->Enabled = true;
    nameEditor->Enabled = true;
    repeatsEditor->Enabled = true;
    snapStart->Enabled = true;
    dataChanged = false;
}

void
CompositionUI::ClearView()
{
    tabControl1->SelectedIndex = 0;
    delete composition;
    composition = new Composition(-1, database->Database().MethodsDatabase());
    notesEditor->Text = "";
    composerSelector->Text = "";
    composerSelector->SelectedIndex = -1;
    methodSelector->Text = "";
    methodSelector->SelectedIndex = -1;
    nameEditor->Text = "";
    changesCount->Text = "";
    repeatsEditor->Value = 0;
    RenameColumns();
    dataChanged = false;
    finishingRoundsLbl->Text = "the lead end.";
    snapStart->Value = 0;
    startingChangeBells->clear();
}

System::Void
CompositionUI::compositionIdLbl_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (populatingView)
        return;
    if (windowState == DucoWindowState::EViewMode)
    {
        System::Int16 id = -1;
        InputIndexQueryUI^ findIdDialog = gcnew InputIndexQueryUI("composition", id);
        System::Windows::Forms::DialogResult returnVal = findIdDialog->ShowDialog();
        if (returnVal == ::DialogResult::OK)
        {
            PopulateViewWithId(id);
        }
    }
}

void
CompositionUI::PopulateView()
{
    populatingView = true;
    tabControl1->SelectedIndex = 0;

    composition->PrepareToProve(database->Database().MethodsDatabase(), database->Database().Settings());
    if (!composition->Ready())
    {
        ClearView();
    }
    else
    {
        notesEditor->Text = DucoUtils::ConvertString(composition->Notes());
        if (composition->ComposerId().ValidId())
        {
            composerSelector->SelectedIndex = ringerCache->FindIndexOrAdd(composition->ComposerId());
        }
        else
        {
            composerSelector->Text = DucoUtils::ConvertString(composition->ComposerName());
        }
        methodSelector->SelectedIndex = DatabaseGenUtils::FindIndex(allMethodIds, composition->MethodId());
        nameEditor->Text = DucoUtils::ConvertString(composition->Name());
        repeatsEditor->Value = composition->Repeats();
        CheckSnapFinish();
        if (composition->SnapStart() > (composition->ChangesPerLead() / 2))
            snapStart->Value = int(composition->SnapStart()) - int(composition->ChangesPerLead());
        else
            snapStart->Value = composition->SnapStart();
        snapStart->Minimum = - int (composition->ChangesPerLead() / 2);
        snapStart->Maximum = composition->ChangesPerLead() / 2;
        RenameColumns();
        PopulateComposition();
    }
    UpdateIdLabel();
    HideColumns();
    populatingView = false;
}

void
CompositionUI::PopulateViewWithId(const Duco::ObjectId& composerId)
{
    ClearAnalysis();
    if (database->FindComposition(composerId, *composition, false))
    {
        PopulateView();
    }
    else
    {
        ClearView();
        SoundUtils::PlayErrorSound();
    }
}

System::Void
CompositionUI::ClearAnalysis()
{
    trueLbl->Visible = false;
    musicInfo->Visible = false;
}

System::Void
CompositionUI::UpdateIdLabel()
{
    if (composition->Id() != -1)
        compositionIdLbl->Text = String::Format("{0:D}/{1:D}", composition->Id().Id(), database->NumberOfObjects(TObjectType::EComposition));
    else
        compositionIdLbl->Text = "New Composition";
}


System::Void
CompositionUI::nameEditor_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (populatingView)
        return;
    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        dataChanged = true;
        std::wstring newName;
        DucoUtils::ConvertString(nameEditor->Text, newName);
        composition->SetName(newName);
    }
}

System::Void
CompositionUI::repeatsEditor_ValueChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (populatingView)
        return;
    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        dataChanged = true;
        composition->SetRepeats(Convert::ToInt16(repeatsEditor->Value));
    }
}

System::Void
CompositionUI::snapStart_ValueChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (populatingView)
        return;
    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        dataChanged = true;
        composition->SetSnapStart(Convert::ToInt16(snapStart->Value));
    }
}

System::Void
CompositionUI::notesEditor_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (populatingView)
        return;
    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        dataChanged = true;
        std::wstring newNotes;
        DucoUtils::ConvertString(notesEditor->Text, newNotes);
        composition->SetNotes(newNotes);
    }
}

System::Void
CompositionUI::backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
    CompositionUIArgs^ args = static_cast<CompositionUIArgs^>(e->Argument);
    args->composition->Prove(*progressWrapper, database->WindowsSettings()->InstallationDir());
}

System::Void
CompositionUI::backgroundWorker1_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    progressBar->Value = e->ProgressPercentage;
}

System::Void
CompositionUI::backgroundWorker1_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    progressBar->Value = 0;
    trueLbl->Visible = true;
    if (composition->Proven())
    {
        if (database->StartEdit(TObjectType::EComposition, composition->Id()))
            database->UpdateComposition(*composition, true);
        if (composition->ProvenTrue())
        {
            if (composition->CurrentEndChange().IsRounds())
                trueLbl->Text = "True";
            else
            {
                trueLbl->Text = "True but doesn't end in rounds";
                trueLbl->Text += ": ";
                trueLbl->Text += DucoUtils::ConvertString(composition->CurrentEndChange().Str(true, true));
            }
            musicInfo->Visible = true;
            musicInfo->Text = DucoUtils::ConvertString(composition->MusicDetail());

            CheckSnapFinish();

        }
        else
        {
            trueLbl->Text = "False";
            musicInfo->Text = DucoUtils::ConvertString(composition->FalseDetails());
            musicInfo->Visible = true;
        }
    }
    else
    {
        trueLbl->Text = "Unproven";
        musicInfo->Text = "";
        musicInfo->Visible = false;
    }
    changesCount->Text = DucoUtils::ConvertString(DucoEngineUtils::ToString(composition->NoOfChanges(), true));
    snapFinish->Checked = composition->IsSnapFinish();
    tabControl1->SelectedIndex = 1;
}

void
CompositionUI::CheckSnapFinish()
{
    snapFinish->Checked = composition->IsSnapFinish();
    if (!composition->IsSnapFinish())
        finishingRoundsLbl->Text = "the lead end.";
    else
    {
        size_t changes(composition->SnapFinish());
        if (changes >= (composition->ChangesPerLead()/2))
        {
            changes = composition->ChangesPerLead() - changes;
            finishingRoundsLbl->Text = DucoUtils::ConvertString(DucoEngineUtils::ToString(changes)) + " changes before the lead end";
        }
        else
        {
            finishingRoundsLbl->Text = DucoUtils::ConvertString(DucoEngineUtils::ToString(changes)) + " changes past the lead end";
        }
    }
}

System::Void
CompositionUI::showAll_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    HideColumns();
}

void
CompositionUI::HideColumns()
{
    std::vector<bool> hideColumns (dataGridView1->ColumnCount);
    std::vector<bool>::iterator hideIt = hideColumns.begin();
    while (hideIt != hideColumns.end())
    {
        if (windowState == DucoWindowState::EViewMode)
            (*hideIt) = !showAll->Checked;
        else
            (*hideIt) = false;
        ++hideIt;
    }

    if (windowState != DucoWindowState::EViewMode)
    {
        for (int l (0); l < dataGridView1->Columns->Count; ++l)
        {
            DataGridViewColumn^ column = dataGridView1->Columns[l];
            if (column->HeaderCell->Value != nullptr)
            {
                String^ headerCellValue = Convert::ToString(column->HeaderCell->Value);
                if (headerCellValue->Length > 0)
                {
                    wchar_t currentChar = headerCellValue[0];
                    if (currentChar == 'H' || currentChar == 'W' || currentChar == 'M')
                    {
                        hideColumns[l] = false;
                    }
                }
            }
        }
    }
    for (int j (0); j < dataGridView1->Rows->Count; ++j)
    {
        DataGridViewRow^ currentRow = dataGridView1->Rows[j];
        for (int i (0); i < currentRow->Cells->Count; ++i)
        {
            if (hideColumns[i])
            {
                DataGridViewCell^ thisCell = currentRow->Cells[i];
            
                if (hideColumns[i] && thisCell->Value != nullptr && static_cast<String^>(thisCell->Value)->Length > 0)
                {
                    hideColumns[i] = false;
                }
            }
        }
    }
    for (int k (0); k < dataGridView1->Columns->Count; ++k)
    {
        dataGridView1->Columns[k]->Visible = !hideColumns[k];
    }
}

//**********************************************************************************************
// Call backs from database
//**********************************************************************************************
void
CompositionUI::Initialised()
{

}

void
CompositionUI::Step(int progressPercent)
{
    generator->ReportProgress(progressPercent);
}

void
CompositionUI::Complete()
{
    generator->ReportProgress(100);
}

void
CompositionUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    switch (type)
    {
    default:
        break;

    case TObjectType::EMethod:
        DatabaseGenUtils::GenerateMethodOptions(allMethodIds, methodSelector, database, -1, -1, true, true, false);
        break;
    }
}

