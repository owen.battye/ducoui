#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"
#include <StatisticFilters.h>

#include "PealSpeedUI.h"

#include "SystemDefaultIcon.h"
#include "DucoTableSorter.h"
#include "FiltersUI.h"
#include "DucoUtils.h"
#include <RingingDatabase.h>
#include "SoundUtils.h"
#include <PealDatabase.h>
#include <Peal.h>
#include <Method.h>
#include <Tower.h>
#include <Ring.h>
#include <DucoEngineUtils.h>
#include "GenericTablePrintUtil.h"
#include "DucoWindowState.h"
#include "OpenObjectCallback.h"
#include "PictureListControl.h"
#include "PictureControl.h"
#include "RingerList.h"
#include "PrintBandSummaryUtil.h"
#include <DownloadedPeal.h>
#include "PealUI.h"
#include "DucoUIUtils.h"

using namespace Duco;
using namespace DucoUI;

using namespace System;
using namespace System::ComponentModel;
using namespace System::Windows::Forms;


PealSpeedUI::PealSpeedUI(DatabaseManager^ theDatabase)
: database(theDatabase), loading(true)
{
    filters = new Duco::StatisticFilters(database->Database());
    InitializeComponent();
}

PealSpeedUI::~PealSpeedUI()
{
    this->!PealSpeedUI();
    if (components)
    {
        delete components;
    }
}

System::Void
PealSpeedUI::PealSpeedUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    loading = false;
    database->AddObserver(this);
    GenerateData();
}

PealSpeedUI::!PealSpeedUI()
{
    delete filters;
    database->RemoveObserver(this);
}

void
PealSpeedUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    if (type == TObjectType::EPeal || eventId == EImportComplete)
    {
        GenerateData();
    }
}

System::Void
PealSpeedUI::fastest_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    GenerateData();
}

System::Void
PealSpeedUI::filtersBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    FiltersUI^ filtersDialog = gcnew FiltersUI(*filters, database);
    System::Windows::Forms::DialogResult returnVal = filtersDialog->ShowDialog(this);
    if (returnVal == System::Windows::Forms::DialogResult::OK)
    {
        GenerateData();
    }
}

System::Void
PealSpeedUI::GenerateData()
{
    if (loading)
        return;

    FastestBellRung->Visible = false;
    pealsDisplay->Rows->Clear();
    std::multimap<float, Duco::ObjectId> pealIds;
    if (fastestBtn->Checked)
    {
        this->Text = "Fastest performances";
    }
    else
    {
        this->Text = "Slowest performances";
    }

    if (database->Database().PealsDatabase().GeneratePealSpeedStats(*filters, fastestBtn->Checked, pealIds))
    {
        PopulateResults(pealIds, fastestBtn->Checked);
        DucoTableSorter^ sorter = gcnew DucoTableSorter(true, EString);
        sorter->AddSortObject(EFloat, !fastestBtn->Checked, 1);
        pealsDisplay->Sort(sorter);
        FastestCpm->HeaderCell->SortGlyphDirection = fastestBtn->Checked ? SortOrder::Descending : SortOrder::Ascending;
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
PealSpeedUI::PopulateResults(const std::multimap<float, Duco::ObjectId>& pealsIds, bool fastest)
{
    double count = 0;
    int position = 0;
    float lastCount = -1;

    pealsDisplay->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::EnableResizing;

    if (fastest)
    {
        std::multimap<float, Duco::ObjectId>::const_reverse_iterator it = pealsIds.rbegin();
        while (it != pealsIds.rend())
        {
            if (lastCount != it->first)
            {
                position = int(count) + 1;
                lastCount = it->first;
            }
            bool draw = DucoEngineUtils::NumberWithSameCount(pealsIds, it->first) > 1;
            PopulateResult(it->second, position, draw);

            ++it;
            ++count;
        }
    }
    else
    {
        std::multimap<float, Duco::ObjectId>::const_iterator it = pealsIds.begin();
        while (it != pealsIds.end())
        {
            if (lastCount != it->first)
            {
                position = int(count) + 1;
                lastCount = it->first;
            }
            bool draw = DucoEngineUtils::NumberWithSameCount(pealsIds, it->first) > 1;
            PopulateResult(it->second, position, draw);

            ++it;
            ++count;
        }
    }
    pealsDisplay->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
}

System::Void
PealSpeedUI::PopulateResult(const Duco::ObjectId& pealId, unsigned int position, bool draw)
{
    Duco::Peal thePeal;
    if (database->FindPeal(pealId, thePeal, false))
    {

        DataGridViewRow^ newRow = gcnew DataGridViewRow();
        if (draw)
        {
            newRow->HeaderCell->Value = Convert::ToString(position) +  "=";
        }
        else
        {
            newRow->HeaderCell->Value = Convert::ToString(position);
        }
        DataGridViewCellCollection^ theCells = newRow->Cells;

        //CpmColumn;
        DataGridViewTextBoxCell^ idCell = gcnew DataGridViewTextBoxCell();
        idCell->Value = DucoUtils::ConvertString(pealId);
        theCells->Add(idCell);
        //CpmColumn;
        DataGridViewTextBoxCell^ cpmCell = gcnew DataGridViewTextBoxCell();
        cpmCell->Value = DucoUtils::FormatFloatToString(thePeal.ChangesPerMinute());
        theCells->Add(cpmCell);
        //dateColumn;
        DataGridViewTextBoxCell^ dateCell = gcnew DataGridViewTextBoxCell();
        dateCell->Value = DucoUtils::ConvertString(thePeal.Date().Str());
        theCells->Add(dateCell);
        //changesColumn;
        DataGridViewTextBoxCell^ changesCell = gcnew DataGridViewTextBoxCell();
        changesCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(thePeal.NoOfChanges(), true));
        theCells->Add(changesCell);
        //methodColumn;
        DataGridViewTextBoxCell^ methodCell = gcnew DataGridViewTextBoxCell();
        Method theMethod;
        if (database->FindMethod(thePeal.MethodId(), theMethod, false))
        {
            methodCell->Value = DucoUtils::ConvertString(theMethod.FullName(database->Database()));
        }
        theCells->Add(methodCell);
        //towerColumn;
        DataGridViewTextBoxCell^ towerCell = gcnew DataGridViewTextBoxCell();
        Tower theTower;
        if (database->FindTower(thePeal.TowerId(), theTower, false))
        {
            towerCell->Value = DucoUtils::ConvertString(theTower.FullName());
        }
        theCells->Add(towerCell);
        //timeColumn;
        DataGridViewTextBoxCell^ timeCell = gcnew DataGridViewTextBoxCell();
        timeCell->Value = DucoUtils::PrintPealTime(thePeal.Time(), false);
        theCells->Add(timeCell);

        DataGridViewTextBoxCell^ bellRungCell = gcnew DataGridViewTextBoxCell();
        DataGridViewTextBoxCell^ tenorCell = gcnew DataGridViewTextBoxCell();
        //Bell Rung & time column
        if (theTower.Id().ValidId())
        {
            const Ring* const theRing = theTower.FindRing(thePeal.RingId());
            if (database->Settings().DefaultRingerSet())
            {
                if (thePeal.Handbell())
                {
                    std::wstring bellNumbersStr;
                    if (thePeal.HandBellsRung(database->Settings().DefaultRinger(), bellNumbersStr))
                    {
                        bellRungCell->Value = DucoUtils::ConvertString(bellNumbersStr);
                        FastestBellRung->Visible = true;
                    }
                }
                else
                {
                    std::set<unsigned int> bellNumbers;
                    bool asStrapper (false);
                    if (theRing != NULL && thePeal.BellRung(database->Settings().DefaultRinger(), bellNumbers, asStrapper))
                    {
                        unsigned int bellNumber (*bellNumbers.begin());
                        if (realBellNumberToolStripMenuItem->Checked)
                        {
                            theRing->RealBellNumber(bellNumber);
                        }
                        bellRungCell->Value = Convert::ToString(bellNumber);
                        FastestBellRung->Visible = true;
                    }
                }
            }
            if (theRing != NULL)
                tenorCell->Value = DucoUtils::ConvertString(theRing->TenorDescription());
        }
        //timeColumn;
        theCells->Add(bellRungCell);
        theCells->Add(tenorCell);

        // Add Row
        pealsDisplay->Rows->Add(newRow);
    }
}

System::Void
PealSpeedUI::fastestPeals_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
    DataGridViewColumn^ newColumn = pealsDisplay->Columns[e->ColumnIndex];
    SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;
    FastestCpm->HeaderCell->SortGlyphDirection = SortOrder::None;
    FastestDateColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
    FastestChanges->HeaderCell->SortGlyphDirection = SortOrder::None;
    FastestTime->HeaderCell->SortGlyphDirection = SortOrder::None;
    FastestBellRung->HeaderCell->SortGlyphDirection = SortOrder::None;
    if (newColumn->SortMode != DataGridViewColumnSortMode::Programmatic)
    {
        return;
    }

    FastestMethod->HeaderCell->SortGlyphDirection = SortOrder::None;
    FastestTower->HeaderCell->SortGlyphDirection = SortOrder::None;

    if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
        newSortOrder = SortOrder::Ascending;
    else
        newSortOrder = SortOrder::Descending;

    newColumn->HeaderCell->SortGlyphDirection = newSortOrder;

    DucoTableSorter^ sorter = gcnew DucoTableSorter(false, EString);

    switch (e->ColumnIndex)
    {
    case 1:
        {
        sorter->AddSortObject(EFloat, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
        }
        break;
    case 2:
        {
        sorter->AddSortObject(EDate, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
        }
        break;
    case 3:
    case 7:
        {
        sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
        sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, 1);
        }
        break;

    case 6:
        {
        sorter->AddSortObject(ETime, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
        }
        break;
    }
    pealsDisplay->Sort(sorter);

}

System::Void
PealSpeedUI::dataGridView_CellDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    try
    {
        System::Windows::Forms::DataGridView^ gridView = static_cast<System::Windows::Forms::DataGridView^>(sender);
        unsigned int rowIndex = e->RowIndex;
        DataGridViewRow^ currentRow = gridView->Rows[rowIndex];
        unsigned int pealId = Convert::ToInt16(currentRow->Cells[0]->Value);
        PealUI::ShowPeal(database, this->MdiParent, pealId);
    }
    catch (Exception^)
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
PealSpeedUI::printBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    DucoUIUtils::Print(this, sender, gcnew System::Drawing::Printing::PrintPageEventHandler( this, &PealSpeedUI::printData ), "peal speed", database->Database().Settings().UsePrintPreview(), true);
}

System::Void
PealSpeedUI::printData(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args)
{
    String^ title = "Slowest " + database->PerformanceString(false) + "s for database ";
    if (fastestBtn->Checked)
        title = "Fastest " + database->PerformanceString(false) + "s for database ";
    title += database->Filename();
    GenericTablePrintUtil^ printUtil = gcnew GenericTablePrintUtil(database, args, title);
    printUtil->SetObjects(pealsDisplay, true);

    System::Drawing::Printing::PrintDocument^ printDoc = static_cast<System::Drawing::Printing::PrintDocument^>(sender);
    printUtil->printObject(args, printDoc->PrinterSettings);
}
