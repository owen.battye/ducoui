#pragma once

namespace DucoUI
{
    public ref class MethodSeriesUI : public System::Windows::Forms::Form, public DucoUI::RingingDatabaseObserver
    {
    public:
        MethodSeriesUI(DucoUI::DatabaseManager^ theDatabase);
        static System::Void ShowSeries(DucoUI::DatabaseManager^ theDatabase, System::Windows::Forms::Form^ parent, const Duco::ObjectId& methodSeriesId);
        static System::Void CreateSeries(DucoUI::DatabaseManager^ theDatabase, System::Windows::Forms::Form^ parent, const std::set<Duco::ObjectId>& methodIds, Duco::ObjectId& methodSeriesId);

        // from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

    protected:
        MethodSeriesUI(DucoUI::DatabaseManager^ theDatabase, const Duco::ObjectId& methodSeriesId);
        MethodSeriesUI(DucoUI::DatabaseManager^ theDatabase, const std::set<Duco::ObjectId>& methodIds);
        !MethodSeriesUI();
        ~MethodSeriesUI();

        System::Void MethodSeriesUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void ClosingEvent(System::Object^ object, System::ComponentModel::CancelEventArgs^ e);
        System::Void StartBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void Back10Btn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void PreviousBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void NextBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void Forward10Btn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void EndBtn_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void AddMethodToSeriesBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void RemoveMethodFromSeriesBtn_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void CreateNewSeriesButton_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void EditBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void SaveBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void StatsBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void PrintBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void CloseBtn_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void SeriesIdLabel_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void SeriesName_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void NoOfMethods_ValueChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void SingleOrder_CheckedChanged(System::Object^  sender, System::EventArgs^  e);

        System::Void SetEditState();
        System::Void SetViewState();
        System::Void SetNewState();

        System::Void PopulateView(const Duco::ObjectId& nextSeriesId);
        System::Void PopulateView(const Duco::MethodSeries& theSeries);
        System::Void PopulateMethods(const Duco::MethodSeries& methodSeries);

        System::Void AddMethod(const Duco::ObjectId& methodId);

        System::Void ClearView();
        System::Void ShowNumberOfActualMethods();

        System::Boolean IsEditState();

        System::Void DataGridView_CellDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);
        System::Void DataGridView_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);
        System::Void ResetAllSortGlyphs();
        System::Void ResetProgrammicSortGlyphs();
        System::Void UpdateIdLabel();
        System::Void PrintMethodSeries(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args);

    private:
        DucoUI::DatabaseManager^            database;
        System::Collections::Generic::List<System::Int16>^ allMethodSeriesIds;
        DucoWindowState                     windowState;
        System::Boolean                     dataChanged;
        System::Boolean                     populatingFromPealUIDialog;
        Duco::ObjectId*                     previousSeriesId;

        Duco::MethodSeries*                 currentMethodSeries;
        System::ComponentModel::Container^  components;
        System::Windows::Forms::Button^     startBtn;
        System::Windows::Forms::Button^     back10Btn;
        System::Windows::Forms::Button^     nextBtn;
        System::Windows::Forms::Button^     previousBtn;
        System::Windows::Forms::Button^     forward10Btn;
        System::Windows::Forms::Button^     endBtn;

        System::Windows::Forms::Button^     addMethodBtn;
        System::Windows::Forms::Button^     removeMethodBtn;

        System::Windows::Forms::Button^     createNewSeriesButton;
        System::Windows::Forms::Button^     editBtn;
        System::Windows::Forms::Button^     saveBtn;
        System::Windows::Forms::Button^     closeBtn;
        System::Windows::Forms::Button^     statsBtn;
        System::Windows::Forms::Button^     printBtn;

        System::Windows::Forms::ComboBox^   seriesName;
        System::Windows::Forms::NumericUpDown^  noOfMethods;
        System::Windows::Forms::DataGridView^  dataGridView;
        System::Windows::Forms::CheckBox^   singleOrder;
        System::Windows::Forms::TextBox^    completedDateDisplay;
        System::Windows::Forms::Label^      completedDateLbl;
        System::Windows::Forms::Label^      noOfActualMethodsLbl;
        System::Windows::Forms::ToolStripStatusLabel^  seriesIdLabel;

        System::Windows::Forms::DataGridViewTextBoxColumn^  methodId;
        System::Windows::Forms::DataGridViewCheckBoxColumn^ deleteColumn;
        System::Windows::Forms::DataGridViewTextBoxColumn^  methodName;
        System::Windows::Forms::DataGridViewTextBoxColumn^  rungDateColumn;
        System::Windows::Forms::DataGridViewTextBoxColumn^  numberRungColumn;

#pragma region Windows Form Designer generated code
        void InitializeComponent(void)
        {
            System::Windows::Forms::Label^ nameLbl;
            System::Windows::Forms::Label^ noOfMethodsLbl;
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
            System::Windows::Forms::Panel^ panel1;
            System::Windows::Forms::Panel^ panel2;
            System::Windows::Forms::Panel^ panel3;
            System::Windows::Forms::StatusStrip^ statusStrip1;
            System::Windows::Forms::GroupBox^ groupBox1;
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel2;
            System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(MethodSeriesUI::typeid));
            this->seriesName = (gcnew System::Windows::Forms::ComboBox());
            this->noOfMethods = (gcnew System::Windows::Forms::NumericUpDown());
            this->completedDateDisplay = (gcnew System::Windows::Forms::TextBox());
            this->singleOrder = (gcnew System::Windows::Forms::CheckBox());
            this->closeBtn = (gcnew System::Windows::Forms::Button());
            this->saveBtn = (gcnew System::Windows::Forms::Button());
            this->statsBtn = (gcnew System::Windows::Forms::Button());
            this->printBtn = (gcnew System::Windows::Forms::Button());
            this->createNewSeriesButton = (gcnew System::Windows::Forms::Button());
            this->editBtn = (gcnew System::Windows::Forms::Button());
            this->startBtn = (gcnew System::Windows::Forms::Button());
            this->back10Btn = (gcnew System::Windows::Forms::Button());
            this->previousBtn = (gcnew System::Windows::Forms::Button());
            this->nextBtn = (gcnew System::Windows::Forms::Button());
            this->forward10Btn = (gcnew System::Windows::Forms::Button());
            this->endBtn = (gcnew System::Windows::Forms::Button());
            this->dataGridView = (gcnew System::Windows::Forms::DataGridView());
            this->methodId = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->deleteColumn = (gcnew System::Windows::Forms::DataGridViewCheckBoxColumn());
            this->methodName = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->rungDateColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->numberRungColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->removeMethodBtn = (gcnew System::Windows::Forms::Button());
            this->addMethodBtn = (gcnew System::Windows::Forms::Button());
            this->seriesIdLabel = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            nameLbl = (gcnew System::Windows::Forms::Label());
            noOfActualMethodsLbl = (gcnew System::Windows::Forms::Label());
            completedDateLbl = (gcnew System::Windows::Forms::Label());
            noOfMethodsLbl = (gcnew System::Windows::Forms::Label());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            panel1 = (gcnew System::Windows::Forms::Panel());
            panel2 = (gcnew System::Windows::Forms::Panel());
            panel3 = (gcnew System::Windows::Forms::Panel());
            statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            groupBox1 = (gcnew System::Windows::Forms::GroupBox());
            tableLayoutPanel2 = (gcnew System::Windows::Forms::TableLayoutPanel());
            tableLayoutPanel1->SuspendLayout();
            panel1->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->noOfMethods))->BeginInit();
            panel2->SuspendLayout();
            panel3->SuspendLayout();
            statusStrip1->SuspendLayout();
            groupBox1->SuspendLayout();
            tableLayoutPanel2->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->BeginInit();
            this->SuspendLayout();
            // 
            // nameLbl
            // 
            nameLbl->AutoSize = true;
            nameLbl->Location = System::Drawing::Point(3, 6);
            nameLbl->Name = L"nameLbl";
            nameLbl->Size = System::Drawing::Size(35, 13);
            nameLbl->Text = L"Name";
            // 
            // noOfActualMethodsLbl
            // 
            noOfActualMethodsLbl->AutoSize = true;
            noOfActualMethodsLbl->Location = System::Drawing::Point(2, 2);
            nameLbl->Name = L"noOfActualMethodsLbl";
            noOfActualMethodsLbl->Size = System::Drawing::Size(35, 13);
            noOfActualMethodsLbl->Text = L"Actual methods";
            noOfActualMethodsLbl->Margin = System::Windows::Forms::Padding(3, 3, 8, 3);
            // 
            // completedDateLbl
            // 
            completedDateLbl->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            completedDateLbl->AutoSize = true;
            completedDateLbl->Location = System::Drawing::Point(262, 5);
            completedDateLbl->Name = L"completedDateLbl";
            completedDateLbl->Size = System::Drawing::Size(57, 13);
            completedDateLbl->Text = L"Completed";
            // 
            // noOfMethodsLbl
            // 
            noOfMethodsLbl->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            noOfMethodsLbl->AutoSize = true;
            noOfMethodsLbl->Location = System::Drawing::Point(298, 5);
            noOfMethodsLbl->Name = L"noOfMethodsLbl";
            noOfMethodsLbl->Size = System::Drawing::Size(76, 13);
            noOfMethodsLbl->Text = L"No of methods";
            // 
            // statusStrip1
            // 
            statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->seriesIdLabel });
            statusStrip1->Location = System::Drawing::Point(0, 124);
            statusStrip1->Name = L"statusStrip1";
            statusStrip1->Size = System::Drawing::Size(502, 22);
            statusStrip1->Text = L"statusStrip1";
            // 
            // seriesIdLabel
            // 
            this->seriesIdLabel->Name = L"seriesIdLabel";
            this->seriesIdLabel->Size = System::Drawing::Size(456, 17);
            this->seriesIdLabel->Spring = true;
            this->seriesIdLabel->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            this->seriesIdLabel->Click += gcnew System::EventHandler(this, &MethodSeriesUI::SeriesIdLabel_Click);
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 1;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->Controls->Add(panel1, 0, 0);
            tableLayoutPanel1->Controls->Add(groupBox1, 0, 1);
            tableLayoutPanel1->Controls->Add(panel2, 0, 2);
            tableLayoutPanel1->Controls->Add(panel3, 0, 3);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 4;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 33)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 30)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 54)));
            tableLayoutPanel1->Size = System::Drawing::Size(434, 273);
            // 
            // panel1
            // 
            panel1->Controls->Add(this->seriesName);
            panel1->Controls->Add(noOfMethodsLbl);
            panel1->Controls->Add(nameLbl);
            panel1->Controls->Add(this->noOfMethods);
            panel1->Dock = System::Windows::Forms::DockStyle::Fill;
            panel1->Location = System::Drawing::Point(3, 3);
            panel1->Name = L"panel1";
            panel1->Size = System::Drawing::Size(428, 27);
            // 
            // seriesName
            // 
            this->seriesName->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
            this->seriesName->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->seriesName->FormattingEnabled = true;
            this->seriesName->Location = System::Drawing::Point(44, 3);
            this->seriesName->Name = L"seriesName";
            this->seriesName->Size = System::Drawing::Size(248, 21);
            this->seriesName->TextChanged += gcnew System::EventHandler(this, &MethodSeriesUI::SeriesName_TextChanged);
            // 
            // noOfMethods
            // 
            this->noOfMethods->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->noOfMethods->Location = System::Drawing::Point(377, 3);
            this->noOfMethods->Name = L"noOfMethods";
            this->noOfMethods->Size = System::Drawing::Size(42, 20);
            this->noOfMethods->ValueChanged += gcnew System::EventHandler(this, &MethodSeriesUI::NoOfMethods_ValueChanged);
            // 
            // singleOrder
            // 
            this->singleOrder->AutoSize = true;
            this->singleOrder->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left));
            this->singleOrder->Location = System::Drawing::Point(80, 3);
            this->singleOrder->Name = L"noOfMethods";
            this->singleOrder->Size = System::Drawing::Size(42, 20);
            this->singleOrder->CheckedChanged += gcnew System::EventHandler(this, &MethodSeriesUI::SingleOrder_CheckedChanged);
            this->singleOrder->Text = "Single stage";
            this->singleOrder->Margin = System::Windows::Forms::Padding(3, 3, 8, 3);
            // 
            // panel2
            // 
            panel2->Controls->Add(this->statsBtn);
            panel2->Controls->Add(this->printBtn);
            panel2->Controls->Add(this->completedDateDisplay);
            panel2->Controls->Add(completedDateLbl);
            panel2->Dock = System::Windows::Forms::DockStyle::Fill;
            panel2->Location = System::Drawing::Point(3, 212);
            panel2->Name = L"panel2";
            panel2->Size = System::Drawing::Size(428, 24);
            // 
            // completedDateDisplay
            // 
            this->completedDateDisplay->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->completedDateDisplay->Location = System::Drawing::Point(325, 2);
            this->completedDateDisplay->Margin = System::Windows::Forms::Padding(3, 5, 3, 3);
            this->completedDateDisplay->Name = L"completedDateDisplay";
            this->completedDateDisplay->ReadOnly = true;
            this->completedDateDisplay->Size = System::Drawing::Size(100, 20);
            // 
            // panel3
            // 
            panel3->Controls->Add(this->closeBtn);
            panel3->Controls->Add(this->saveBtn);
            panel3->Controls->Add(this->createNewSeriesButton);
            panel3->Controls->Add(this->editBtn);
            panel3->Controls->Add(this->startBtn);
            panel3->Controls->Add(this->back10Btn);
            panel3->Controls->Add(this->previousBtn);
            panel3->Controls->Add(this->nextBtn);
            panel3->Controls->Add(this->forward10Btn);
            panel3->Controls->Add(this->endBtn);
            panel3->Dock = System::Windows::Forms::DockStyle::Fill;
            panel3->Location = System::Drawing::Point(3, 242);
            panel3->Name = L"panel3";
            panel3->Size = System::Drawing::Size(428, 28);
            // 
            // closeBtn
            // 
            this->closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            this->closeBtn->Location = System::Drawing::Point(350, 3);
            this->closeBtn->Name = L"closeBtn";
            this->closeBtn->Size = System::Drawing::Size(75, 23);
            this->closeBtn->Text = L"Close";
            this->closeBtn->UseVisualStyleBackColor = true;
            this->closeBtn->Click += gcnew System::EventHandler(this, &MethodSeriesUI::CloseBtn_Click);
            // 
            // saveBtn
            // 
            this->saveBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->saveBtn->Location = System::Drawing::Point(275, 3);
            this->saveBtn->Name = L"saveBtn";
            this->saveBtn->Size = System::Drawing::Size(75, 23);
            this->saveBtn->Text = L"Save";
            this->saveBtn->UseVisualStyleBackColor = true;
            this->saveBtn->Click += gcnew System::EventHandler(this, &MethodSeriesUI::SaveBtn_Click);
            // 
            // startBtn
            // 
            this->startBtn->AutoSize = true;
            this->startBtn->Location = System::Drawing::Point(1, 3);
            this->startBtn->Name = L"startBtn";
            this->startBtn->Size = System::Drawing::Size(31, 23);
            this->startBtn->Text = L"|<";
            this->startBtn->UseVisualStyleBackColor = true;
            this->startBtn->Click += gcnew System::EventHandler(this, &MethodSeriesUI::StartBtn_Click);
            // 
            // back10Btn
            // 
            this->back10Btn->Enabled = false;
            this->back10Btn->Location = System::Drawing::Point(32, 3);
            this->back10Btn->Name = L"back10Btn";
            this->back10Btn->Size = System::Drawing::Size(31, 23);
            this->back10Btn->Text = L"<<";
            this->back10Btn->UseVisualStyleBackColor = true;
            this->back10Btn->Click += gcnew System::EventHandler(this, &MethodSeriesUI::Back10Btn_Click);
            // 
            // previousBtn
            // 
            this->previousBtn->Enabled = false;
            this->previousBtn->Location = System::Drawing::Point(63, 3);
            this->previousBtn->Name = L"previousBtn";
            this->previousBtn->Size = System::Drawing::Size(31, 23);
            this->previousBtn->Text = L"<";
            this->previousBtn->UseVisualStyleBackColor = true;
            this->previousBtn->Click += gcnew System::EventHandler(this, &MethodSeriesUI::PreviousBtn_Click);
            // 
            // nextBtn
            // 
            this->nextBtn->Location = System::Drawing::Point(106, 3);
            this->nextBtn->Name = L"nextBtn";
            this->nextBtn->Size = System::Drawing::Size(31, 23);
            this->nextBtn->Text = L">";
            this->nextBtn->UseVisualStyleBackColor = true;
            this->nextBtn->Click += gcnew System::EventHandler(this, &MethodSeriesUI::NextBtn_Click);
            // 
            // forward10Btn
            // 
            this->forward10Btn->Location = System::Drawing::Point(137, 3);
            this->forward10Btn->Name = L"forward10Btn";
            this->forward10Btn->Size = System::Drawing::Size(31, 23);
            this->forward10Btn->Text = L">>";
            this->forward10Btn->UseVisualStyleBackColor = true;
            this->forward10Btn->Click += gcnew System::EventHandler(this, &MethodSeriesUI::Forward10Btn_Click);
            // 
            // endBtn
            // 
            this->endBtn->AutoSize = true;
            this->endBtn->Location = System::Drawing::Point(168, 3);
            this->endBtn->Name = L"endBtn";
            this->endBtn->Size = System::Drawing::Size(31, 23);
            this->endBtn->Text = L">|";
            this->endBtn->UseVisualStyleBackColor = true;
            this->endBtn->Click += gcnew System::EventHandler(this, &MethodSeriesUI::EndBtn_Click);
            // 
            // statsBtn
            // 
            this->statsBtn->Location = System::Drawing::Point(3, 2);
            this->statsBtn->Name = L"statsBtn";
            this->statsBtn->Size = System::Drawing::Size(75, 23);
            this->statsBtn->Text = L"Ringers";
            this->statsBtn->UseVisualStyleBackColor = true;
            this->statsBtn->Click += gcnew System::EventHandler(this, &MethodSeriesUI::StatsBtn_Click);
            // 
            // printBtn
            // 
            this->printBtn->Location = System::Drawing::Point(83, 2);
            this->printBtn->Name = L"printBtn";
            this->printBtn->Size = System::Drawing::Size(75, 23);
            this->printBtn->Text = L"Print";
            this->printBtn->UseVisualStyleBackColor = true;
            this->printBtn->Click += gcnew System::EventHandler(this, &MethodSeriesUI::PrintBtn_Click);
            // 
            // createNewSeriesButton
            // 
            this->createNewSeriesButton->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->createNewSeriesButton->Location = System::Drawing::Point(200, 3);
            this->createNewSeriesButton->Name = L"createNewSeriesButton";
            this->createNewSeriesButton->Size = System::Drawing::Size(75, 23);
            this->createNewSeriesButton->Text = L"New";
            this->createNewSeriesButton->UseVisualStyleBackColor = true;
            this->createNewSeriesButton->Click += gcnew System::EventHandler(this, &MethodSeriesUI::CreateNewSeriesButton_Click);
            // 
            // editBtn
            // 
            this->editBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->editBtn->Location = System::Drawing::Point(275, 3);
            this->editBtn->Name = L"editBtn";
            this->editBtn->Size = System::Drawing::Size(75, 23);
            this->editBtn->Text = L"Edit";
            this->editBtn->UseVisualStyleBackColor = true;
            this->editBtn->Click += gcnew System::EventHandler(this, &MethodSeriesUI::EditBtn_Click);
            // 
            // groupBox1
            // 
            groupBox1->Controls->Add(tableLayoutPanel2);
            groupBox1->Dock = System::Windows::Forms::DockStyle::Fill;
            groupBox1->Location = System::Drawing::Point(3, 36);
            groupBox1->Name = L"groupBox1";
            groupBox1->Size = System::Drawing::Size(428, 170);
            groupBox1->TabStop = false;
            groupBox1->Text = L"Methods";
            // 
            // tableLayoutPanel2
            // 
            tableLayoutPanel2->ColumnCount = 4;
            tableLayoutPanel2->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel2->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute, 100)));
            tableLayoutPanel2->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute, 80)));
            tableLayoutPanel2->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute, 80)));
            tableLayoutPanel2->Controls->Add(this->dataGridView, 0, 0);
            tableLayoutPanel2->Controls->Add(this->noOfActualMethodsLbl, 0, 1);
            tableLayoutPanel2->Controls->Add(this->singleOrder, 1, 1);
            tableLayoutPanel2->Controls->Add(this->removeMethodBtn, 3, 1);
            tableLayoutPanel2->Controls->Add(this->addMethodBtn, 2, 1);
            tableLayoutPanel2->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel2->Location = System::Drawing::Point(3, 16);
            tableLayoutPanel2->Name = L"tableLayoutPanel2";
            tableLayoutPanel2->RowCount = 2;
            tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 28)));
            tableLayoutPanel2->Size = System::Drawing::Size(422, 151);
            // 
            // dataGridView
            // 
            this->dataGridView->AllowUserToAddRows = false;
            this->dataGridView->AllowUserToDeleteRows = false;
            this->dataGridView->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->dataGridView->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(5) {
                this->methodId,
                    this->deleteColumn, this->methodName, this->rungDateColumn, this->numberRungColumn
            });
            tableLayoutPanel2->SetColumnSpan(this->dataGridView, 4);
            this->dataGridView->Dock = System::Windows::Forms::DockStyle::Fill;
            this->dataGridView->Location = System::Drawing::Point(3, 3);
            this->dataGridView->Name = L"dataGridView";
            this->dataGridView->ReadOnly = true;
            this->dataGridView->Size = System::Drawing::Size(416, 117);
            this->dataGridView->CellDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &MethodSeriesUI::DataGridView_CellDoubleClick);
            this->dataGridView->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &MethodSeriesUI::DataGridView_ColumnHeaderMouseClick);
            // 
            // methodId
            // 
            this->methodId->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
            this->methodId->HeaderText = L"Id";
            this->methodId->Name = L"methodId";
            this->methodId->ReadOnly = true;
            this->methodId->Visible = false;
            // 
            // deleteColumn
            // 
            this->deleteColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            this->deleteColumn->HeaderText = L"Delete";
            this->deleteColumn->Name = L"deleteColumn";
            this->deleteColumn->ReadOnly = true;
            this->deleteColumn->Resizable = System::Windows::Forms::DataGridViewTriState::True;
            this->deleteColumn->Width = 44;
            // 
            // methodName
            // 
            this->methodName->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->methodName->HeaderText = L"Method";
            this->methodName->Name = L"methodName";
            this->methodName->ReadOnly = true;
            this->methodName->Width = 68;
            // 
            // rungDateColumn
            // 
            this->rungDateColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->rungDateColumn->HeaderText = L"Date rung";
            this->rungDateColumn->Name = L"rungDateColumn";
            this->rungDateColumn->ReadOnly = true;
            this->rungDateColumn->Width = 79;
            this->rungDateColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            // 
            // numberRungColumn
            // 
            this->numberRungColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->numberRungColumn->HeaderText = L"Number of peals";
            this->numberRungColumn->Name = L"numberRungColumn";
            this->numberRungColumn->ReadOnly = true;
            this->numberRungColumn->Width = 79;
            this->numberRungColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            // 
            // removeMethodBtn
            // 
            this->removeMethodBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->removeMethodBtn->AutoSize = true;
            this->removeMethodBtn->Location = System::Drawing::Point(347, 126);
            this->removeMethodBtn->Name = L"removeMethodBtn";
            this->removeMethodBtn->Size = System::Drawing::Size(75, 22);
            this->removeMethodBtn->Text = L"Remove";
            this->removeMethodBtn->UseVisualStyleBackColor = true;
            this->removeMethodBtn->Click += gcnew System::EventHandler(this, &MethodSeriesUI::RemoveMethodFromSeriesBtn_Click);
            // 
            // addMethodBtn
            // 
            this->addMethodBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->addMethodBtn->AutoSize = true;
            this->addMethodBtn->Location = System::Drawing::Point(272, 126);
            this->addMethodBtn->Margin = System::Windows::Forms::Padding(3, 3, 0, 3);
            this->addMethodBtn->Name = L"addBtn";
            this->addMethodBtn->Size = System::Drawing::Size(75, 22);
            this->addMethodBtn->Text = L"Add";
            this->addMethodBtn->UseVisualStyleBackColor = true;
            this->addMethodBtn->Click += gcnew System::EventHandler(this, &MethodSeriesUI::AddMethodToSeriesBtn_Click);
            // 
            // MethodSeriesUI
            // 
            this->AcceptButton = this->saveBtn;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->CancelButton = this->closeBtn;
            this->ClientSize = System::Drawing::Size(482, 303);
            this->Controls->Add(statusStrip1);
            this->Controls->Add(tableLayoutPanel1);
            this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
            this->MinimumSize = System::Drawing::Size(482, 303);
            this->Name = L"MethodSeriesUI";
            this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Show;
            this->Text = L"Method Series";
            this->Closing += gcnew System::ComponentModel::CancelEventHandler(this, &MethodSeriesUI::ClosingEvent);
            this->Load += gcnew System::EventHandler(this, &MethodSeriesUI::MethodSeriesUI_Load);
            tableLayoutPanel1->ResumeLayout(false);
            panel1->ResumeLayout(false);
            panel1->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->noOfMethods))->EndInit();
            panel2->ResumeLayout(false);
            statusStrip1->ResumeLayout(false);
            panel2->PerformLayout();
            panel3->ResumeLayout(false);
            groupBox1->ResumeLayout(false);
            tableLayoutPanel2->ResumeLayout(false);
            tableLayoutPanel2->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->EndInit();
            this->ResumeLayout(false);

            this->seriesName->TabIndex = 1;
            this->noOfMethods->TabIndex = 2;
            this->dataGridView->TabIndex = 3;
            this->singleOrder->TabIndex = 4;
            this->addMethodBtn->TabIndex = 5;
            this->removeMethodBtn->TabIndex = 6;

            this->startBtn->TabIndex = 7;
            this->back10Btn->TabIndex = 8;
            this->previousBtn->TabIndex = 9;
            this->nextBtn->TabIndex = 10;
            this->forward10Btn->TabIndex = 11;
            this->endBtn->TabIndex = 12;

            this->editBtn->TabIndex = 13;
            this->createNewSeriesButton->TabIndex = 14;
            this->saveBtn->TabIndex = 15;
            this->closeBtn->TabIndex = 16;

            this->statsBtn->TabIndex = 17;
            this->printBtn->TabIndex = 18;
            this->completedDateDisplay->TabIndex = 19;
            statusStrip1->TabIndex = 20;
        }
#pragma endregion

    };
}
