#pragma once

namespace DucoUI
{
    public ref class TowerUI : public System::Windows::Forms::Form, public DucoUI::RingingDatabaseObserver
    {
    public:
        TowerUI(DucoUI::DatabaseManager^ newDatabase, DucoWindowState newState);
        static Duco::ObjectId CreateNewTower(DucoUI::DatabaseManager^ newDatabase, System::String^ newTowerName, System::String^ newRingName, System::String^ newTenorWeight, System::String^ newTenorKey, System::String^ newTowerbaseId, System::String^ newDoveId, unsigned int noOfBells, System::Boolean handbell);
        System::Void CloneTower(DucoUI::DatabaseManager^ newDatabase, const Duco::Tower& newTower, System::Windows::Forms::Form^ parent);
        static void ShowTower(DucoUI::DatabaseManager^ newDatabase, System::Windows::Forms::Form^ parent, const Duco::ObjectId& towerId, const Duco::ObjectId& ringId);
        const Duco::ObjectId& CurrentTowerId();

        // from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

    protected:
        TowerUI(DucoUI::DatabaseManager^ newDatabase, const Duco::ObjectId& towerId, const Duco::ObjectId& ringId);
        !TowerUI();
        ~TowerUI();
        void InitializePictureControl();
        System::Void TowerUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void ClosingEvent(System::Object^ object, System::ComponentModel::CancelEventArgs^ e);
        System::Void PictureEvent(System::Object^  sender, System::EventArgs^  e);
        System::Void TowerUI_Activated(System::Object^  sender, System::EventArgs^  e);
        System::Void TowerUI_Deactivate(System::Object^  sender, System::EventArgs^  e);
        bool InEditState();

        void UpdateIdLabel();
        void PopulateViewWithId(const Duco::ObjectId& towerId);
        void PopulateView();
        void ClearView();
        void ClearRing();
        void PopulateRing(int ringIndex);
        void PopulateRing(const Duco::ObjectId& ringId);
        void PopulateRing(const Duco::Ring& ring);
        void SetBellsInRing(const Duco::Ring& ring);

        bool NextTower(bool forwards, Duco::ObjectId& towerId);
        void SetViewState();
        void SetEditState();

        System::Void StartBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void Back10Btn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void PreviousBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void NextBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void Forward10Btn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void EndBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void AddRingBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void EditBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void CloseBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void CancelBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void SaveBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void DoveBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void MapBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void openLinkedTowerBtn_Click(System::Object^ sender, System::EventArgs^ e);

        System::Void openPealbasebtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void openFelsteadBtn_Click(System::Object ^ sender, System::EventArgs ^ e);

        System::Void RingSelector_Scroll(System::Object^  sender, System::EventArgs^  e);
        System::Void NameEditor_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void TownEditor_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void CountyEditor_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void RingTenorKeyEditor_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void RingTenorWeightEditor_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void RingNameEditor_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void NumberOfBellsEditor_ValueChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void BellsInRing_ValueChanged(System::Object^  sender, System::Windows::Forms::ItemCheckEventArgs^  e);
        System::Void NoOfBellsInRingEditor_ValueChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void Removed_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void handbells_CheckedChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void TowerIdLabel_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void StatsBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void cloneBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void RenameBellsBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void DoveRef_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void Longitude_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void Latitude_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void TowerBaseRef_TextChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void Notes_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void Notes_LinkClicked(System::Object^  sender, System::Windows::Forms::LinkClickedEventArgs^  e);
        System::Void Aka_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void FirstRungDate_ValueChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void linkedTower_SelectedValueChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void antiClockwise_CheckedChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void SwapBtn_Click(System::Object^ sender, System::EventArgs^ e);

        System::Void SetControlColour(System::Windows::Forms::Control^ cntrl);
        System::Void SetBellSelectorColour(int noToAdd);

    // Member data
    private: System::ComponentModel::IContainer^ components;
    protected:
        DucoUI::DatabaseManager^           database;
        System::Boolean                    databaseUpdated;
        System::Boolean                    delayUpdating;
        DucoWindowState                    state;
        Duco::Tower*                       currentTower;
        System::Boolean                    dataChanged;
        System::Boolean                    autoGeneratingFromPealDialog;
        System::Boolean                    updatingNoOfBellsInTower;
        System::Boolean                    changingRing;
        Duco::ObjectId*                    openWithRingId;
        Duco::ObjectId*                    newPealUITowerId;
        System::Boolean                    newTowerRingNameChanged;
        System::Boolean                    autoUpdatingRingName;
        System::Boolean                    resetLinkedTower;
        System::Boolean                    newTowerRingChanged;
        System::Collections::Generic::List<System::Int16>^  allTowerIds;
        DucoUI::PictureControl^             pictureCtrl;
        System::Windows::Forms::ComboBox^   nameEditor;

        System::Windows::Forms::ComboBox^   countyEditor;
        System::Windows::Forms::NumericUpDown^  numberOfBellsEditor;

        System::Windows::Forms::CheckBox^   removed;
        System::Windows::Forms::TextBox^    ringNameEditor;
        System::Windows::Forms::TextBox^    ringTenorWeightEditor;
        System::Windows::Forms::NumericUpDown^   noOfBellsInRingEditor;
        System::Windows::Forms::CheckedListBox^  bellsInRing;
        System::Windows::Forms::TrackBar^   ringSelector;

        System::Windows::Forms::TextBox^    doveRef;
        System::Windows::Forms::MaskedTextBox^    longitude;
        System::Windows::Forms::MaskedTextBox^    latitude;



        System::Windows::Forms::TextBox^    aka;
        System::Windows::Forms::RichTextBox^    notes;
        System::Windows::Forms::Button^     addRingBtn;
        System::Windows::Forms::Button^     startBtn;
        System::Windows::Forms::Button^     back10Btn;
        System::Windows::Forms::Button^     previousBtn;
        System::Windows::Forms::Button^     nextBtn;

        System::Windows::Forms::Button^     endBtn;
        System::Windows::Forms::Button^     saveBtn;

        System::Windows::Forms::Button^     editBtn;

        System::Windows::Forms::Button^     statsBtn;
        System::Windows::Forms::Button^     renameBellsBtn;
        System::Windows::Forms::Button^     doveBtn;
        System::Windows::Forms::Button^     mapBtn;
        System::Windows::Forms::ToolStripStatusLabel^  towerIdLabel;
        System::Windows::Forms::TabPage^    ringsTab;
        System::Windows::Forms::ComboBox^   linkedTower;

        System::Windows::Forms::TabPage^  picturePage;

        System::Windows::Forms::Label^ ringSelectorLabel;
        System::Windows::Forms::TextBox^ ringTenorKeyEditor;

protected: System::Windows::Forms::Button^ doveToolBarBtn;
private: System::Windows::Forms::Button^ openLinkedTowerBtn;
private: System::Windows::Forms::Button^ openPealbaseBtn;
private: System::Windows::Forms::Button^ openFelsteadBtn;
protected: System::Windows::Forms::TextBox^ towerbaseRef;
protected: System::Windows::Forms::CheckBox^ handbells;
private: System::Windows::Forms::CheckBox^ antiClockwise;

protected: System::Windows::Forms::Button^ cancelBtn;
private:
protected: System::Windows::Forms::Button^ closeBtn;
protected: System::Windows::Forms::ComboBox^ townEditor;
private: System::Windows::Forms::Button^ cloneBtn;
protected:
protected: System::Windows::Forms::Button^ forward10Btn;
private: System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
protected: System::Windows::Forms::Button^ SwapBtn;
protected: System::Windows::Forms::DateTimePicker^ firstRungDate;
protected: System::Windows::Forms::Label^ firstRungLbl;
private: System::Windows::Forms::Label^ linkedTowersLbl;
protected:
private:

private:
protected:
private:
protected:

    System::Windows::Forms::ToolTip^ toolTip1;

        void InitializeComponent()
        {
            this->components = (gcnew System::ComponentModel::Container());
            System::Windows::Forms::Label^ doveRefLbl;
            System::Windows::Forms::Label^ towerLbl;
            System::Windows::Forms::Label^ notesLbl;
            System::Windows::Forms::StatusStrip^ statusStrip1;
            System::Windows::Forms::TabControl^ tabControl1;
            System::Windows::Forms::TableLayoutPanel^ ringLabelPanel;
            System::Windows::Forms::Label^ ringNameLbl;
            System::Windows::Forms::Label^ noOfBellsInRingLbl;
            System::Windows::Forms::Label^ ringTenorWeightLbl;
            System::Windows::Forms::Label^ ringTenorKeyLabel;
            System::Windows::Forms::TabPage^ referencesPage;
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel4;
            System::Windows::Forms::GroupBox^ doveGrp;
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel5;
            System::Windows::Forms::Label^ latitudeLbl;
            System::Windows::Forms::Label^ longitudeLbl;
            System::Windows::Forms::GroupBox^ towerbaseGroup;
            System::Windows::Forms::TabPage^ linksPage;
            System::Windows::Forms::Label^ linkedTowerLbl;
            System::Windows::Forms::Label^ akaLbl;
            System::Windows::Forms::TabPage^ otherPage;
            System::Windows::Forms::TableLayoutPanel^ otherLayoutPanel;
            System::Windows::Forms::Label^ countyLbl;
            System::Windows::Forms::Label^ townLbl;
            System::Windows::Forms::Label^ noOfBellsLbl;
            this->towerIdLabel = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->ringsTab = (gcnew System::Windows::Forms::TabPage());
            this->ringTenorKeyEditor = (gcnew System::Windows::Forms::TextBox());
            this->bellsInRing = (gcnew System::Windows::Forms::CheckedListBox());
            this->ringSelector = (gcnew System::Windows::Forms::TrackBar());
            this->noOfBellsInRingEditor = (gcnew System::Windows::Forms::NumericUpDown());
            this->ringNameEditor = (gcnew System::Windows::Forms::TextBox());
            this->ringTenorWeightEditor = (gcnew System::Windows::Forms::TextBox());
            this->addRingBtn = (gcnew System::Windows::Forms::Button());
            this->ringSelectorLabel = (gcnew System::Windows::Forms::Label());
            this->renameBellsBtn = (gcnew System::Windows::Forms::Button());
            this->antiClockwise = (gcnew System::Windows::Forms::CheckBox());
            this->SwapBtn = (gcnew System::Windows::Forms::Button());
            this->doveBtn = (gcnew System::Windows::Forms::Button());
            this->doveRef = (gcnew System::Windows::Forms::TextBox());
            this->mapBtn = (gcnew System::Windows::Forms::Button());
            this->latitude = (gcnew System::Windows::Forms::MaskedTextBox());
            this->longitude = (gcnew System::Windows::Forms::MaskedTextBox());
            this->openPealbaseBtn = (gcnew System::Windows::Forms::Button());
            this->openFelsteadBtn = (gcnew System::Windows::Forms::Button());
            this->towerbaseRef = (gcnew System::Windows::Forms::TextBox());
            this->openLinkedTowerBtn = (gcnew System::Windows::Forms::Button());
            this->linkedTower = (gcnew System::Windows::Forms::ComboBox());
            this->aka = (gcnew System::Windows::Forms::TextBox());
            this->notes = (gcnew System::Windows::Forms::RichTextBox());
            this->picturePage = (gcnew System::Windows::Forms::TabPage());
            this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            this->doveToolBarBtn = (gcnew System::Windows::Forms::Button());
            this->statsBtn = (gcnew System::Windows::Forms::Button());
            this->cancelBtn = (gcnew System::Windows::Forms::Button());
            this->numberOfBellsEditor = (gcnew System::Windows::Forms::NumericUpDown());
            this->saveBtn = (gcnew System::Windows::Forms::Button());
            this->editBtn = (gcnew System::Windows::Forms::Button());
            this->countyEditor = (gcnew System::Windows::Forms::ComboBox());
            this->nameEditor = (gcnew System::Windows::Forms::ComboBox());
            this->previousBtn = (gcnew System::Windows::Forms::Button());
            this->back10Btn = (gcnew System::Windows::Forms::Button());
            this->startBtn = (gcnew System::Windows::Forms::Button());
            this->removed = (gcnew System::Windows::Forms::CheckBox());
            this->closeBtn = (gcnew System::Windows::Forms::Button());
            this->townEditor = (gcnew System::Windows::Forms::ComboBox());
            this->handbells = (gcnew System::Windows::Forms::CheckBox());
            this->cloneBtn = (gcnew System::Windows::Forms::Button());
            this->nextBtn = (gcnew System::Windows::Forms::Button());
            this->forward10Btn = (gcnew System::Windows::Forms::Button());
            this->endBtn = (gcnew System::Windows::Forms::Button());
            this->toolTip1 = (gcnew System::Windows::Forms::ToolTip(this->components));
            this->firstRungLbl = (gcnew System::Windows::Forms::Label());
            this->firstRungDate = (gcnew System::Windows::Forms::DateTimePicker());
            this->linkedTowersLbl = (gcnew System::Windows::Forms::Label());
            doveRefLbl = (gcnew System::Windows::Forms::Label());
            towerLbl = (gcnew System::Windows::Forms::Label());
            notesLbl = (gcnew System::Windows::Forms::Label());
            statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            tabControl1 = (gcnew System::Windows::Forms::TabControl());
            ringLabelPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
            ringNameLbl = (gcnew System::Windows::Forms::Label());
            noOfBellsInRingLbl = (gcnew System::Windows::Forms::Label());
            ringTenorWeightLbl = (gcnew System::Windows::Forms::Label());
            ringTenorKeyLabel = (gcnew System::Windows::Forms::Label());
            referencesPage = (gcnew System::Windows::Forms::TabPage());
            tableLayoutPanel4 = (gcnew System::Windows::Forms::TableLayoutPanel());
            doveGrp = (gcnew System::Windows::Forms::GroupBox());
            tableLayoutPanel5 = (gcnew System::Windows::Forms::TableLayoutPanel());
            latitudeLbl = (gcnew System::Windows::Forms::Label());
            longitudeLbl = (gcnew System::Windows::Forms::Label());
            towerbaseGroup = (gcnew System::Windows::Forms::GroupBox());
            linksPage = (gcnew System::Windows::Forms::TabPage());
            linkedTowerLbl = (gcnew System::Windows::Forms::Label());
            akaLbl = (gcnew System::Windows::Forms::Label());
            otherPage = (gcnew System::Windows::Forms::TabPage());
            otherLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
            countyLbl = (gcnew System::Windows::Forms::Label());
            townLbl = (gcnew System::Windows::Forms::Label());
            noOfBellsLbl = (gcnew System::Windows::Forms::Label());
            statusStrip1->SuspendLayout();
            tabControl1->SuspendLayout();
            this->ringsTab->SuspendLayout();
            ringLabelPanel->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ringSelector))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->noOfBellsInRingEditor))->BeginInit();
            referencesPage->SuspendLayout();
            tableLayoutPanel4->SuspendLayout();
            doveGrp->SuspendLayout();
            tableLayoutPanel5->SuspendLayout();
            towerbaseGroup->SuspendLayout();
            linksPage->SuspendLayout();
            otherPage->SuspendLayout();
            otherLayoutPanel->SuspendLayout();
            this->tableLayoutPanel1->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numberOfBellsEditor))->BeginInit();
            this->SuspendLayout();
            // 
            // doveRefLbl
            // 
            doveRefLbl->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            doveRefLbl->AutoSize = true;
            doveRefLbl->Location = System::Drawing::Point(22, 7);
            doveRefLbl->Margin = System::Windows::Forms::Padding(3, 7, 3, 3);
            doveRefLbl->Name = L"doveRefLbl";
            doveRefLbl->Size = System::Drawing::Size(81, 13);
            doveRefLbl->TabIndex = 0;
            doveRefLbl->Text = L"Dove reference";
            // 
            // towerLbl
            // 
            towerLbl->AutoSize = true;
            this->tableLayoutPanel1->SetColumnSpan(towerLbl, 3);
            towerLbl->Location = System::Drawing::Point(3, 3);
            towerLbl->Margin = System::Windows::Forms::Padding(3);
            towerLbl->Name = L"towerLbl";
            towerLbl->Size = System::Drawing::Size(91, 13);
            towerLbl->TabIndex = 0;
            towerLbl->Text = L"Name/Dedication";
            // 
            // notesLbl
            // 
            notesLbl->AutoSize = true;
            notesLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            notesLbl->Location = System::Drawing::Point(3, 29);
            notesLbl->Margin = System::Windows::Forms::Padding(3);
            notesLbl->Name = L"notesLbl";
            notesLbl->Size = System::Drawing::Size(74, 13);
            notesLbl->TabIndex = 0;
            notesLbl->Text = L"Notes";
            // 
            // statusStrip1
            // 
            statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->towerIdLabel });
            statusStrip1->Location = System::Drawing::Point(0, 359);
            statusStrip1->Name = L"statusStrip1";
            statusStrip1->Size = System::Drawing::Size(722, 22);
            statusStrip1->TabIndex = 25;
            statusStrip1->Text = L"statusStrip1";
            // 
            // towerIdLabel
            // 
            this->towerIdLabel->Name = L"towerIdLabel";
            this->towerIdLabel->Size = System::Drawing::Size(707, 17);
            this->towerIdLabel->Spring = true;
            this->towerIdLabel->Text = L"toolStripStatusLabel1";
            this->towerIdLabel->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            this->towerIdLabel->Click += gcnew System::EventHandler(this, &TowerUI::TowerIdLabel_Click);
            // 
            // tabControl1
            // 
            this->tableLayoutPanel1->SetColumnSpan(tabControl1, 13);
            tabControl1->Controls->Add(this->ringsTab);
            tabControl1->Controls->Add(referencesPage);
            tabControl1->Controls->Add(linksPage);
            tabControl1->Controls->Add(otherPage);
            tabControl1->Controls->Add(this->picturePage);
            tabControl1->Dock = System::Windows::Forms::DockStyle::Fill;
            tabControl1->Location = System::Drawing::Point(3, 75);
            tabControl1->Name = L"tabControl1";
            tabControl1->SelectedIndex = 0;
            tabControl1->Size = System::Drawing::Size(716, 223);
            tabControl1->TabIndex = 11;
            // 
            // ringsTab
            // 
            this->ringsTab->Controls->Add(ringLabelPanel);
            this->ringsTab->Location = System::Drawing::Point(4, 22);
            this->ringsTab->Name = L"ringsTab";
            this->ringsTab->Padding = System::Windows::Forms::Padding(3);
            this->ringsTab->Size = System::Drawing::Size(708, 197);
            this->ringsTab->TabIndex = 0;
            this->ringsTab->Text = L"Rings";
            this->ringsTab->UseVisualStyleBackColor = true;
            // 
            // ringLabelPanel
            // 
            ringLabelPanel->ColumnCount = 6;
            ringLabelPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            ringLabelPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            ringLabelPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            ringLabelPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            ringLabelPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            ringLabelPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            ringLabelPanel->Controls->Add(this->ringTenorKeyEditor, 2, 3);
            ringLabelPanel->Controls->Add(this->bellsInRing, 4, 1);
            ringLabelPanel->Controls->Add(this->ringSelector, 1, 0);
            ringLabelPanel->Controls->Add(this->noOfBellsInRingEditor, 2, 4);
            ringLabelPanel->Controls->Add(this->ringNameEditor, 1, 1);
            ringLabelPanel->Controls->Add(ringNameLbl, 0, 1);
            ringLabelPanel->Controls->Add(this->ringTenorWeightEditor, 2, 2);
            ringLabelPanel->Controls->Add(noOfBellsInRingLbl, 0, 4);
            ringLabelPanel->Controls->Add(this->addRingBtn, 4, 0);
            ringLabelPanel->Controls->Add(ringTenorWeightLbl, 0, 2);
            ringLabelPanel->Controls->Add(this->ringSelectorLabel, 0, 0);
            ringLabelPanel->Controls->Add(ringTenorKeyLabel, 0, 3);
            ringLabelPanel->Controls->Add(this->renameBellsBtn, 5, 0);
            ringLabelPanel->Controls->Add(this->antiClockwise, 0, 5);
            ringLabelPanel->Dock = System::Windows::Forms::DockStyle::Fill;
            ringLabelPanel->Location = System::Drawing::Point(3, 3);
            ringLabelPanel->Name = L"ringLabelPanel";
            ringLabelPanel->RowCount = 6;
            ringLabelPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 30)));
            ringLabelPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            ringLabelPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            ringLabelPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            ringLabelPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            ringLabelPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            ringLabelPanel->Size = System::Drawing::Size(702, 191);
            ringLabelPanel->TabIndex = 10;
            // 
            // ringTenorKeyEditor
            // 
            this->ringTenorKeyEditor->Dock = System::Windows::Forms::DockStyle::Fill;
            this->ringTenorKeyEditor->Location = System::Drawing::Point(90, 85);
            this->ringTenorKeyEditor->Name = L"ringTenorKeyEditor";
            this->ringTenorKeyEditor->Size = System::Drawing::Size(39, 20);
            this->ringTenorKeyEditor->TabIndex = 6;
            this->ringTenorKeyEditor->TextChanged += gcnew System::EventHandler(this, &TowerUI::RingTenorKeyEditor_TextChanged);
            // 
            // bellsInRing
            // 
            ringLabelPanel->SetColumnSpan(this->bellsInRing, 2);
            this->bellsInRing->ColumnWidth = 80;
            this->bellsInRing->Dock = System::Windows::Forms::DockStyle::Fill;
            this->bellsInRing->FormattingEnabled = true;
            this->bellsInRing->Location = System::Drawing::Point(185, 33);
            this->bellsInRing->MultiColumn = true;
            this->bellsInRing->Name = L"bellsInRing";
            ringLabelPanel->SetRowSpan(this->bellsInRing, 5);
            this->bellsInRing->Size = System::Drawing::Size(514, 155);
            this->bellsInRing->TabIndex = 9;
            this->bellsInRing->ItemCheck += gcnew System::Windows::Forms::ItemCheckEventHandler(this, &TowerUI::BellsInRing_ValueChanged);
            // 
            // ringSelector
            // 
            this->ringSelector->BackColor = System::Drawing::SystemColors::Window;
            ringLabelPanel->SetColumnSpan(this->ringSelector, 3);
            this->ringSelector->Dock = System::Windows::Forms::DockStyle::Fill;
            this->ringSelector->Location = System::Drawing::Point(44, 3);
            this->ringSelector->Name = L"ringSelector";
            this->ringSelector->Size = System::Drawing::Size(135, 24);
            this->ringSelector->TabIndex = 1;
            this->ringSelector->TickStyle = System::Windows::Forms::TickStyle::TopLeft;
            this->ringSelector->Scroll += gcnew System::EventHandler(this, &TowerUI::RingSelector_Scroll);
            // 
            // noOfBellsInRingEditor
            // 
            this->noOfBellsInRingEditor->Dock = System::Windows::Forms::DockStyle::Fill;
            this->noOfBellsInRingEditor->Location = System::Drawing::Point(90, 111);
            this->noOfBellsInRingEditor->Name = L"noOfBellsInRingEditor";
            this->noOfBellsInRingEditor->Size = System::Drawing::Size(39, 20);
            this->noOfBellsInRingEditor->TabIndex = 7;
            this->noOfBellsInRingEditor->ValueChanged += gcnew System::EventHandler(this, &TowerUI::NoOfBellsInRingEditor_ValueChanged);
            // 
            // ringNameEditor
            // 
            ringLabelPanel->SetColumnSpan(this->ringNameEditor, 3);
            this->ringNameEditor->Dock = System::Windows::Forms::DockStyle::Fill;
            this->ringNameEditor->Location = System::Drawing::Point(44, 33);
            this->ringNameEditor->Name = L"ringNameEditor";
            this->ringNameEditor->Size = System::Drawing::Size(135, 20);
            this->ringNameEditor->TabIndex = 4;
            this->ringNameEditor->TextChanged += gcnew System::EventHandler(this, &TowerUI::RingNameEditor_TextChanged);
            // 
            // ringNameLbl
            // 
            ringNameLbl->AutoSize = true;
            ringNameLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            ringNameLbl->Location = System::Drawing::Point(3, 37);
            ringNameLbl->Margin = System::Windows::Forms::Padding(3, 7, 3, 3);
            ringNameLbl->Name = L"ringNameLbl";
            ringNameLbl->Size = System::Drawing::Size(35, 16);
            ringNameLbl->TabIndex = 0;
            ringNameLbl->Text = L"Name";
            // 
            // ringTenorWeightEditor
            // 
            ringLabelPanel->SetColumnSpan(this->ringTenorWeightEditor, 2);
            this->ringTenorWeightEditor->Dock = System::Windows::Forms::DockStyle::Fill;
            this->ringTenorWeightEditor->Location = System::Drawing::Point(90, 59);
            this->ringTenorWeightEditor->Name = L"ringTenorWeightEditor";
            this->ringTenorWeightEditor->Size = System::Drawing::Size(89, 20);
            this->ringTenorWeightEditor->TabIndex = 5;
            this->ringTenorWeightEditor->TextChanged += gcnew System::EventHandler(this, &TowerUI::RingTenorWeightEditor_TextChanged);
            // 
            // noOfBellsInRingLbl
            // 
            noOfBellsInRingLbl->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            noOfBellsInRingLbl->AutoSize = true;
            ringLabelPanel->SetColumnSpan(noOfBellsInRingLbl, 2);
            noOfBellsInRingLbl->Location = System::Drawing::Point(3, 115);
            noOfBellsInRingLbl->Margin = System::Windows::Forms::Padding(3, 7, 3, 3);
            noOfBellsInRingLbl->Name = L"noOfBellsInRingLbl";
            noOfBellsInRingLbl->Size = System::Drawing::Size(81, 13);
            noOfBellsInRingLbl->TabIndex = 4;
            noOfBellsInRingLbl->Text = L"Number of Bells";
            noOfBellsInRingLbl->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // addRingBtn
            // 
            this->addRingBtn->Location = System::Drawing::Point(185, 3);
            this->addRingBtn->Name = L"addRingBtn";
            this->addRingBtn->Size = System::Drawing::Size(75, 23);
            this->addRingBtn->TabIndex = 2;
            this->addRingBtn->Text = L"Add ring";
            this->addRingBtn->UseVisualStyleBackColor = true;
            this->addRingBtn->Click += gcnew System::EventHandler(this, &TowerUI::AddRingBtn_Click);
            // 
            // ringTenorWeightLbl
            // 
            ringTenorWeightLbl->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            ringTenorWeightLbl->AutoSize = true;
            ringLabelPanel->SetColumnSpan(ringTenorWeightLbl, 2);
            ringTenorWeightLbl->Location = System::Drawing::Point(15, 63);
            ringTenorWeightLbl->Margin = System::Windows::Forms::Padding(3, 7, 3, 3);
            ringTenorWeightLbl->Name = L"ringTenorWeightLbl";
            ringTenorWeightLbl->Size = System::Drawing::Size(69, 13);
            ringTenorWeightLbl->TabIndex = 2;
            ringTenorWeightLbl->Text = L"Tenor weight";
            ringTenorWeightLbl->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // ringSelectorLabel
            // 
            this->ringSelectorLabel->AutoSize = true;
            this->ringSelectorLabel->Dock = System::Windows::Forms::DockStyle::Fill;
            this->ringSelectorLabel->Location = System::Drawing::Point(3, 3);
            this->ringSelectorLabel->Margin = System::Windows::Forms::Padding(3);
            this->ringSelectorLabel->Name = L"ringSelectorLabel";
            this->ringSelectorLabel->Size = System::Drawing::Size(35, 24);
            this->ringSelectorLabel->TabIndex = 10;
            this->ringSelectorLabel->Text = L"Ring";
            this->ringSelectorLabel->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // ringTenorKeyLabel
            // 
            ringTenorKeyLabel->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            ringTenorKeyLabel->AutoSize = true;
            ringLabelPanel->SetColumnSpan(ringTenorKeyLabel, 2);
            ringTenorKeyLabel->Location = System::Drawing::Point(29, 89);
            ringTenorKeyLabel->Margin = System::Windows::Forms::Padding(3, 7, 3, 0);
            ringTenorKeyLabel->Name = L"ringTenorKeyLabel";
            ringTenorKeyLabel->Size = System::Drawing::Size(55, 13);
            ringTenorKeyLabel->TabIndex = 12;
            ringTenorKeyLabel->Text = L"Tenor key";
            ringTenorKeyLabel->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // renameBellsBtn
            // 
            this->renameBellsBtn->Location = System::Drawing::Point(596, 3);
            this->renameBellsBtn->Name = L"renameBellsBtn";
            this->renameBellsBtn->Size = System::Drawing::Size(103, 23);
            this->renameBellsBtn->TabIndex = 3;
            this->renameBellsBtn->Text = L"Rename Bells";
            this->renameBellsBtn->UseVisualStyleBackColor = true;
            this->renameBellsBtn->Click += gcnew System::EventHandler(this, &TowerUI::RenameBellsBtn_Click);
            // 
            // antiClockwise
            // 
            this->antiClockwise->AutoSize = true;
            ringLabelPanel->SetColumnSpan(this->antiClockwise, 4);
            this->antiClockwise->Location = System::Drawing::Point(3, 171);
            this->antiClockwise->Name = L"antiClockwise";
            this->antiClockwise->Size = System::Drawing::Size(95, 17);
            this->antiClockwise->TabIndex = 13;
            this->antiClockwise->Text = L"Anti Clockwise";
            this->antiClockwise->UseVisualStyleBackColor = true;
            this->antiClockwise->CheckedChanged += gcnew System::EventHandler(this, &TowerUI::antiClockwise_CheckedChanged);
            // 
            // referencesPage
            // 
            referencesPage->Controls->Add(tableLayoutPanel4);
            referencesPage->Location = System::Drawing::Point(4, 22);
            referencesPage->Name = L"referencesPage";
            referencesPage->Padding = System::Windows::Forms::Padding(3);
            referencesPage->Size = System::Drawing::Size(708, 197);
            referencesPage->TabIndex = 1;
            referencesPage->Text = L"Web links";
            referencesPage->UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            tableLayoutPanel4->AutoSize = true;
            tableLayoutPanel4->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            tableLayoutPanel4->ColumnCount = 2;
            tableLayoutPanel4->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 50)));
            tableLayoutPanel4->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 50)));
            tableLayoutPanel4->Controls->Add(doveGrp, 0, 0);
            tableLayoutPanel4->Controls->Add(towerbaseGroup, 1, 0);
            tableLayoutPanel4->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel4->Location = System::Drawing::Point(3, 3);
            tableLayoutPanel4->Name = L"tableLayoutPanel4";
            tableLayoutPanel4->RowCount = 2;
            tableLayoutPanel4->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel4->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel4->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
            tableLayoutPanel4->Size = System::Drawing::Size(702, 191);
            tableLayoutPanel4->TabIndex = 4;
            // 
            // doveGrp
            // 
            doveGrp->AutoSize = true;
            doveGrp->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            doveGrp->Controls->Add(tableLayoutPanel5);
            doveGrp->Dock = System::Windows::Forms::DockStyle::Fill;
            doveGrp->Location = System::Drawing::Point(3, 3);
            doveGrp->Name = L"doveGrp";
            tableLayoutPanel4->SetRowSpan(doveGrp, 2);
            doveGrp->Size = System::Drawing::Size(345, 185);
            doveGrp->TabIndex = 0;
            doveGrp->TabStop = false;
            doveGrp->Text = L"Dove";
            // 
            // tableLayoutPanel5
            // 
            tableLayoutPanel5->AutoSize = true;
            tableLayoutPanel5->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            tableLayoutPanel5->ColumnCount = 3;
            tableLayoutPanel5->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel5->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel5->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel5->Controls->Add(this->SwapBtn, 0, 3);
            tableLayoutPanel5->Controls->Add(this->doveBtn, 0, 3);
            tableLayoutPanel5->Controls->Add(doveRefLbl, 0, 0);
            tableLayoutPanel5->Controls->Add(this->doveRef, 1, 0);
            tableLayoutPanel5->Controls->Add(this->mapBtn, 2, 3);
            tableLayoutPanel5->Controls->Add(latitudeLbl, 0, 1);
            tableLayoutPanel5->Controls->Add(this->latitude, 1, 1);
            tableLayoutPanel5->Controls->Add(longitudeLbl, 0, 2);
            tableLayoutPanel5->Controls->Add(this->longitude, 1, 2);
            tableLayoutPanel5->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel5->Location = System::Drawing::Point(3, 16);
            tableLayoutPanel5->Name = L"tableLayoutPanel5";
            tableLayoutPanel5->RowCount = 4;
            tableLayoutPanel5->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel5->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel5->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel5->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 35)));
            tableLayoutPanel5->Size = System::Drawing::Size(339, 166);
            tableLayoutPanel5->TabIndex = 8;
            // 
            // SwapBtn
            // 
            this->SwapBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
            this->SwapBtn->Location = System::Drawing::Point(109, 141);
            this->SwapBtn->Name = L"SwapBtn";
            this->SwapBtn->Size = System::Drawing::Size(75, 22);
            this->SwapBtn->TabIndex = 10;
            this->SwapBtn->Text = L"Swap";
            this->toolTip1->SetToolTip(this->SwapBtn, L"Swap Latitude and longitude");
            this->SwapBtn->UseVisualStyleBackColor = true;
            this->SwapBtn->Click += gcnew System::EventHandler(this, &TowerUI::SwapBtn_Click);
            // 
            // doveBtn
            // 
            this->doveBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
            this->doveBtn->Location = System::Drawing::Point(28, 141);
            this->doveBtn->Name = L"doveBtn";
            this->doveBtn->Size = System::Drawing::Size(75, 22);
            this->doveBtn->TabIndex = 2;
            this->doveBtn->Text = L"Dove";
            this->doveBtn->UseVisualStyleBackColor = true;
            this->doveBtn->Click += gcnew System::EventHandler(this, &TowerUI::DoveBtn_Click);
            // 
            // doveRef
            // 
            tableLayoutPanel5->SetColumnSpan(this->doveRef, 2);
            this->doveRef->Dock = System::Windows::Forms::DockStyle::Fill;
            this->doveRef->Location = System::Drawing::Point(109, 3);
            this->doveRef->Name = L"doveRef";
            this->doveRef->Size = System::Drawing::Size(227, 20);
            this->doveRef->TabIndex = 1;
            this->doveRef->TextChanged += gcnew System::EventHandler(this, &TowerUI::DoveRef_TextChanged);
            // 
            // mapBtn
            // 
            this->mapBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
            this->mapBtn->Location = System::Drawing::Point(261, 141);
            this->mapBtn->Name = L"mapBtn";
            this->mapBtn->Size = System::Drawing::Size(75, 22);
            this->mapBtn->TabIndex = 7;
            this->mapBtn->Text = L"Map";
            this->mapBtn->UseVisualStyleBackColor = true;
            this->mapBtn->Click += gcnew System::EventHandler(this, &TowerUI::MapBtn_Click);
            // 
            // latitudeLbl
            // 
            latitudeLbl->Location = System::Drawing::Point(3, 26);
            latitudeLbl->Name = L"latitudeLbl";
            latitudeLbl->Size = System::Drawing::Size(100, 23);
            latitudeLbl->TabIndex = 8;
            latitudeLbl->Text = L"Latitude";
            latitudeLbl->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // latitude
            // 
            tableLayoutPanel5->SetColumnSpan(this->latitude, 2);
            this->latitude->Dock = System::Windows::Forms::DockStyle::Fill;
            this->latitude->Location = System::Drawing::Point(109, 29);
            this->latitude->Mask = L"##9.99999";
            this->latitude->Name = L"latitude";
            this->latitude->ReadOnly = true;
            this->latitude->Size = System::Drawing::Size(227, 20);
            this->latitude->TabIndex = 6;
            this->toolTip1->SetToolTip(this->latitude, L"Import location from Dove from the main menu");
            this->latitude->TextChanged += gcnew System::EventHandler(this, &TowerUI::Latitude_TextChanged);
            // 
            // longitudeLbl
            // 
            longitudeLbl->AutoSize = true;
            longitudeLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            longitudeLbl->Location = System::Drawing::Point(3, 52);
            longitudeLbl->Name = L"longitudeLbl";
            longitudeLbl->Size = System::Drawing::Size(100, 26);
            longitudeLbl->TabIndex = 9;
            longitudeLbl->Text = L"Longitude";
            longitudeLbl->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // longitude
            // 
            tableLayoutPanel5->SetColumnSpan(this->longitude, 2);
            this->longitude->Dock = System::Windows::Forms::DockStyle::Fill;
            this->longitude->Location = System::Drawing::Point(109, 55);
            this->longitude->Mask = L"##9.99999";
            this->longitude->Name = L"longitude";
            this->longitude->ReadOnly = true;
            this->longitude->Size = System::Drawing::Size(227, 20);
            this->longitude->TabIndex = 4;
            this->toolTip1->SetToolTip(this->longitude, L"Import location from Dove from the main menu");
            this->longitude->TextChanged += gcnew System::EventHandler(this, &TowerUI::Longitude_TextChanged);
            // 
            // towerbaseGroup
            // 
            towerbaseGroup->AutoSize = true;
            towerbaseGroup->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            towerbaseGroup->Controls->Add(this->openPealbaseBtn);
            towerbaseGroup->Controls->Add(this->openFelsteadBtn);
            towerbaseGroup->Controls->Add(this->towerbaseRef);
            towerbaseGroup->Dock = System::Windows::Forms::DockStyle::Fill;
            towerbaseGroup->Location = System::Drawing::Point(354, 3);
            towerbaseGroup->Name = L"towerbaseGroup";
            towerbaseGroup->Size = System::Drawing::Size(345, 60);
            towerbaseGroup->TabIndex = 4;
            towerbaseGroup->TabStop = false;
            towerbaseGroup->Text = L"Tower base";
            // 
            // openPealbaseBtn
            // 
            this->openPealbaseBtn->Location = System::Drawing::Point(180, 18);
            this->openPealbaseBtn->Name = L"openPealbaseBtn";
            this->openPealbaseBtn->Size = System::Drawing::Size(75, 23);
            this->openPealbaseBtn->TabIndex = 1;
            this->openPealbaseBtn->Text = L"Pealbase";
            this->openPealbaseBtn->UseVisualStyleBackColor = true;
            this->openPealbaseBtn->Click += gcnew System::EventHandler(this, &TowerUI::openPealbasebtn_Click);
            // 
            // openFelsteadBtn
            // 
            this->openFelsteadBtn->Location = System::Drawing::Point(99, 18);
            this->openFelsteadBtn->Name = L"openFelsteadBtn";
            this->openFelsteadBtn->Size = System::Drawing::Size(75, 23);
            this->openFelsteadBtn->TabIndex = 1;
            this->openFelsteadBtn->Text = L"Felstead";
            this->openFelsteadBtn->UseVisualStyleBackColor = true;
            this->openFelsteadBtn->Click += gcnew System::EventHandler(this, &TowerUI::openFelsteadBtn_Click);
            // 
            // towerbaseRef
            // 
            this->towerbaseRef->Location = System::Drawing::Point(9, 19);
            this->towerbaseRef->Margin = System::Windows::Forms::Padding(6, 3, 6, 3);
            this->towerbaseRef->Name = L"towerbaseRef";
            this->towerbaseRef->Size = System::Drawing::Size(81, 20);
            this->towerbaseRef->TabIndex = 2;
            this->toolTip1->SetToolTip(this->towerbaseRef, L"Link to felstead, click to open");
            this->towerbaseRef->TextChanged += gcnew System::EventHandler(this, &TowerUI::TowerBaseRef_TextChanged);
            // 
            // linksPage
            // 
            linksPage->Controls->Add(this->linkedTowersLbl);
            linksPage->Controls->Add(this->openLinkedTowerBtn);
            linksPage->Controls->Add(this->linkedTower);
            linksPage->Controls->Add(linkedTowerLbl);
            linksPage->Controls->Add(akaLbl);
            linksPage->Controls->Add(this->aka);
            linksPage->Location = System::Drawing::Point(4, 22);
            linksPage->Name = L"linksPage";
            linksPage->Padding = System::Windows::Forms::Padding(3);
            linksPage->Size = System::Drawing::Size(708, 197);
            linksPage->TabIndex = 2;
            linksPage->Text = L"Links";
            linksPage->UseVisualStyleBackColor = true;
            // 
            // openLinkedTowerBtn
            // 
            this->openLinkedTowerBtn->Location = System::Drawing::Point(497, 32);
            this->openLinkedTowerBtn->Name = L"openLinkedTowerBtn";
            this->openLinkedTowerBtn->Size = System::Drawing::Size(75, 23);
            this->openLinkedTowerBtn->TabIndex = 3;
            this->openLinkedTowerBtn->Text = L"Open";
            this->openLinkedTowerBtn->UseVisualStyleBackColor = true;
            this->openLinkedTowerBtn->Click += gcnew System::EventHandler(this, &TowerUI::openLinkedTowerBtn_Click);
            // 
            // linkedTower
            // 
            this->linkedTower->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
            this->linkedTower->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->linkedTower->FormattingEnabled = true;
            this->linkedTower->Location = System::Drawing::Point(86, 32);
            this->linkedTower->Name = L"linkedTower";
            this->linkedTower->Size = System::Drawing::Size(405, 21);
            this->linkedTower->TabIndex = 2;
            this->toolTip1->SetToolTip(this->linkedTower, L"Link to another tower, for example when a set of bells is replaced, so that some "
                L"stastics can be combined.");
            this->linkedTower->SelectedValueChanged += gcnew System::EventHandler(this, &TowerUI::linkedTower_SelectedValueChanged);
            // 
            // linkedTowerLbl
            // 
            linkedTowerLbl->AutoSize = true;
            linkedTowerLbl->Location = System::Drawing::Point(14, 35);
            linkedTowerLbl->Name = L"linkedTowerLbl";
            linkedTowerLbl->Size = System::Drawing::Size(68, 13);
            linkedTowerLbl->TabIndex = 4;
            linkedTowerLbl->Text = L"Linked tower";
            // 
            // akaLbl
            // 
            akaLbl->AutoSize = true;
            akaLbl->Location = System::Drawing::Point(6, 10);
            akaLbl->Name = L"akaLbl";
            akaLbl->Size = System::Drawing::Size(76, 13);
            akaLbl->TabIndex = 0;
            akaLbl->Text = L"Also known as";
            // 
            // aka
            // 
            this->aka->Location = System::Drawing::Point(86, 6);
            this->aka->Name = L"aka";
            this->aka->Size = System::Drawing::Size(405, 20);
            this->aka->TabIndex = 1;
            this->toolTip1->SetToolTip(this->aka, L"A second name the tower can be known by. Entering some details here, can help to "
                L"determine which tower should be used when downloading peals.");
            this->aka->TextChanged += gcnew System::EventHandler(this, &TowerUI::Aka_TextChanged);
            // 
            // otherPage
            // 
            otherPage->Controls->Add(otherLayoutPanel);
            otherPage->Location = System::Drawing::Point(4, 22);
            otherPage->Name = L"otherPage";
            otherPage->Padding = System::Windows::Forms::Padding(3);
            otherPage->Size = System::Drawing::Size(708, 197);
            otherPage->TabIndex = 3;
            otherPage->Text = L"Other";
            otherPage->UseVisualStyleBackColor = true;
            // 
            // otherLayoutPanel
            // 
            otherLayoutPanel->ColumnCount = 2;
            otherLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            otherLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            otherLayoutPanel->Controls->Add(this->firstRungDate, 1, 0);
            otherLayoutPanel->Controls->Add(this->firstRungLbl, 0, 0);
            otherLayoutPanel->Controls->Add(notesLbl, 0, 1);
            otherLayoutPanel->Controls->Add(this->notes, 0, 2);
            otherLayoutPanel->Dock = System::Windows::Forms::DockStyle::Fill;
            otherLayoutPanel->Location = System::Drawing::Point(3, 3);
            otherLayoutPanel->Name = L"otherLayoutPanel";
            otherLayoutPanel->RowCount = 3;
            otherLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            otherLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            otherLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            otherLayoutPanel->Size = System::Drawing::Size(702, 191);
            otherLayoutPanel->TabIndex = 2;
            // 
            // notes
            // 
            this->notes->AcceptsTab = true;
            otherLayoutPanel->SetColumnSpan(this->notes, 2);
            this->notes->Dock = System::Windows::Forms::DockStyle::Fill;
            this->notes->Location = System::Drawing::Point(3, 48);
            this->notes->Name = L"notes";
            this->notes->Size = System::Drawing::Size(696, 140);
            this->notes->TabIndex = 2;
            this->notes->Text = L"";
            this->notes->LinkClicked += gcnew System::Windows::Forms::LinkClickedEventHandler(this, &TowerUI::Notes_LinkClicked);
            this->notes->TextChanged += gcnew System::EventHandler(this, &TowerUI::Notes_TextChanged);
            // 
            // picturePage
            // 
            this->picturePage->Location = System::Drawing::Point(4, 22);
            this->picturePage->Name = L"picturePage";
            this->picturePage->Padding = System::Windows::Forms::Padding(3);
            this->picturePage->Size = System::Drawing::Size(708, 197);
            this->picturePage->TabIndex = 4;
            this->picturePage->Text = L"Picture";
            this->picturePage->UseVisualStyleBackColor = true;
            // 
            // countyLbl
            // 
            countyLbl->AutoSize = true;
            this->tableLayoutPanel1->SetColumnSpan(countyLbl, 3);
            countyLbl->Location = System::Drawing::Point(529, 3);
            countyLbl->Margin = System::Windows::Forms::Padding(3);
            countyLbl->Name = L"countyLbl";
            countyLbl->Size = System::Drawing::Size(40, 13);
            countyLbl->TabIndex = 2;
            countyLbl->Text = L"County";
            // 
            // townLbl
            // 
            townLbl->AutoSize = true;
            this->tableLayoutPanel1->SetColumnSpan(townLbl, 2);
            townLbl->Location = System::Drawing::Point(264, 3);
            townLbl->Margin = System::Windows::Forms::Padding(3);
            townLbl->Name = L"townLbl";
            townLbl->Size = System::Drawing::Size(34, 13);
            townLbl->TabIndex = 1;
            townLbl->Text = L"Town";
            // 
            // noOfBellsLbl
            // 
            noOfBellsLbl->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            noOfBellsLbl->AutoSize = true;
            this->tableLayoutPanel1->SetColumnSpan(noOfBellsLbl, 2);
            noOfBellsLbl->Location = System::Drawing::Point(264, 53);
            noOfBellsLbl->Margin = System::Windows::Forms::Padding(3, 7, 3, 3);
            noOfBellsLbl->Name = L"noOfBellsLbl";
            noOfBellsLbl->Size = System::Drawing::Size(145, 13);
            noOfBellsLbl->TabIndex = 7;
            noOfBellsLbl->Text = L"Total number of bells in tower";
            // 
            // tableLayoutPanel1
            // 
            this->tableLayoutPanel1->ColumnCount = 13;
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
                19.56522F)));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
                25.56391F)));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
                42.85714F)));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
                12.14312F)));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->Controls->Add(this->doveToolBarBtn, 8, 4);
            this->tableLayoutPanel1->Controls->Add(towerLbl, 0, 0);
            this->tableLayoutPanel1->Controls->Add(this->statsBtn, 9, 4);
            this->tableLayoutPanel1->Controls->Add(townLbl, 7, 0);
            this->tableLayoutPanel1->Controls->Add(noOfBellsLbl, 7, 2);
            this->tableLayoutPanel1->Controls->Add(this->cancelBtn, 12, 5);
            this->tableLayoutPanel1->Controls->Add(this->numberOfBellsEditor, 9, 2);
            this->tableLayoutPanel1->Controls->Add(countyLbl, 10, 0);
            this->tableLayoutPanel1->Controls->Add(this->saveBtn, 11, 5);
            this->tableLayoutPanel1->Controls->Add(tabControl1, 0, 3);
            this->tableLayoutPanel1->Controls->Add(this->editBtn, 11, 4);
            this->tableLayoutPanel1->Controls->Add(this->countyEditor, 10, 1);
            this->tableLayoutPanel1->Controls->Add(this->nameEditor, 0, 1);
            this->tableLayoutPanel1->Controls->Add(this->previousBtn, 2, 4);
            this->tableLayoutPanel1->Controls->Add(this->back10Btn, 1, 4);
            this->tableLayoutPanel1->Controls->Add(this->startBtn, 0, 4);
            this->tableLayoutPanel1->Controls->Add(this->removed, 0, 2);
            this->tableLayoutPanel1->Controls->Add(this->closeBtn, 12, 4);
            this->tableLayoutPanel1->Controls->Add(this->townEditor, 7, 1);
            this->tableLayoutPanel1->Controls->Add(this->handbells, 10, 2);
            this->tableLayoutPanel1->Controls->Add(this->cloneBtn, 7, 4);
            this->tableLayoutPanel1->Controls->Add(this->nextBtn, 3, 4);
            this->tableLayoutPanel1->Controls->Add(this->forward10Btn, 4, 4);
            this->tableLayoutPanel1->Controls->Add(this->endBtn, 5, 4);
            this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            this->tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
            this->tableLayoutPanel1->RowCount = 6;
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
            this->tableLayoutPanel1->Size = System::Drawing::Size(722, 359);
            this->tableLayoutPanel1->TabIndex = 26;
            // 
            // doveToolBarBtn
            // 
            this->doveToolBarBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
            this->doveToolBarBtn->AutoSize = true;
            this->doveToolBarBtn->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            this->doveToolBarBtn->Location = System::Drawing::Point(366, 304);
            this->doveToolBarBtn->Name = L"doveToolBarBtn";
            this->doveToolBarBtn->Size = System::Drawing::Size(43, 23);
            this->doveToolBarBtn->TabIndex = 18;
            this->doveToolBarBtn->Text = L"Dove";
            this->doveToolBarBtn->UseVisualStyleBackColor = true;
            this->doveToolBarBtn->Click += gcnew System::EventHandler(this, &TowerUI::DoveBtn_Click);
            // 
            // statsBtn
            // 
            this->tableLayoutPanel1->SetColumnSpan(this->statsBtn, 2);
            this->statsBtn->Location = System::Drawing::Point(415, 304);
            this->statsBtn->Margin = System::Windows::Forms::Padding(3, 3, 10, 3);
            this->statsBtn->Name = L"statsBtn";
            this->statsBtn->Size = System::Drawing::Size(75, 23);
            this->statsBtn->TabIndex = 19;
            this->statsBtn->Text = L"Statistics";
            this->statsBtn->UseVisualStyleBackColor = true;
            this->statsBtn->Click += gcnew System::EventHandler(this, &TowerUI::StatsBtn_Click);
            // 
            // cancelBtn
            // 
            this->cancelBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->cancelBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            this->cancelBtn->Location = System::Drawing::Point(644, 333);
            this->cancelBtn->Name = L"cancelBtn";
            this->cancelBtn->Size = System::Drawing::Size(75, 23);
            this->cancelBtn->TabIndex = 24;
            this->cancelBtn->Text = L"Cancel";
            this->cancelBtn->UseVisualStyleBackColor = true;
            this->cancelBtn->Click += gcnew System::EventHandler(this, &TowerUI::CancelBtn_Click);
            // 
            // numberOfBellsEditor
            // 
            this->numberOfBellsEditor->Location = System::Drawing::Point(415, 49);
            this->numberOfBellsEditor->Name = L"numberOfBellsEditor";
            this->numberOfBellsEditor->Size = System::Drawing::Size(44, 20);
            this->numberOfBellsEditor->TabIndex = 8;
            this->numberOfBellsEditor->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 5, 0, 0, 0 });
            this->numberOfBellsEditor->ValueChanged += gcnew System::EventHandler(this, &TowerUI::NumberOfBellsEditor_ValueChanged);
            // 
            // saveBtn
            // 
            this->saveBtn->Location = System::Drawing::Point(561, 333);
            this->saveBtn->Name = L"saveBtn";
            this->saveBtn->Size = System::Drawing::Size(75, 23);
            this->saveBtn->TabIndex = 21;
            this->saveBtn->Text = L"Save";
            this->saveBtn->UseVisualStyleBackColor = true;
            this->saveBtn->Click += gcnew System::EventHandler(this, &TowerUI::SaveBtn_Click);
            // 
            // editBtn
            // 
            this->editBtn->Location = System::Drawing::Point(561, 304);
            this->editBtn->Name = L"editBtn";
            this->editBtn->Size = System::Drawing::Size(75, 23);
            this->editBtn->TabIndex = 20;
            this->editBtn->Text = L"Edit";
            this->editBtn->UseVisualStyleBackColor = true;
            this->editBtn->Click += gcnew System::EventHandler(this, &TowerUI::EditBtn_Click);
            // 
            // countyEditor
            // 
            this->countyEditor->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
            this->countyEditor->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->tableLayoutPanel1->SetColumnSpan(this->countyEditor, 3);
            this->countyEditor->Dock = System::Windows::Forms::DockStyle::Fill;
            this->countyEditor->FormattingEnabled = true;
            this->countyEditor->Location = System::Drawing::Point(529, 22);
            this->countyEditor->Name = L"countyEditor";
            this->countyEditor->Size = System::Drawing::Size(190, 21);
            this->countyEditor->TabIndex = 5;
            this->countyEditor->TextChanged += gcnew System::EventHandler(this, &TowerUI::CountyEditor_TextChanged);
            // 
            // nameEditor
            // 
            this->nameEditor->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
            this->nameEditor->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->tableLayoutPanel1->SetColumnSpan(this->nameEditor, 6);
            this->nameEditor->Dock = System::Windows::Forms::DockStyle::Fill;
            this->nameEditor->FormattingEnabled = true;
            this->nameEditor->Location = System::Drawing::Point(3, 22);
            this->nameEditor->Name = L"nameEditor";
            this->nameEditor->Size = System::Drawing::Size(255, 21);
            this->nameEditor->TabIndex = 3;
            this->nameEditor->TextChanged += gcnew System::EventHandler(this, &TowerUI::NameEditor_TextChanged);
            // 
            // previousBtn
            // 
            this->previousBtn->AutoSize = true;
            this->previousBtn->Location = System::Drawing::Point(71, 304);
            this->previousBtn->Margin = System::Windows::Forms::Padding(0, 3, 6, 3);
            this->previousBtn->Name = L"previousBtn";
            this->previousBtn->Size = System::Drawing::Size(31, 23);
            this->previousBtn->TabIndex = 14;
            this->previousBtn->Text = L"<";
            this->previousBtn->UseVisualStyleBackColor = true;
            this->previousBtn->Click += gcnew System::EventHandler(this, &TowerUI::PreviousBtn_Click);
            // 
            // back10Btn
            // 
            this->back10Btn->AutoSize = true;
            this->back10Btn->Location = System::Drawing::Point(37, 304);
            this->back10Btn->Margin = System::Windows::Forms::Padding(0, 3, 3, 3);
            this->back10Btn->Name = L"back10Btn";
            this->back10Btn->Size = System::Drawing::Size(31, 23);
            this->back10Btn->TabIndex = 13;
            this->back10Btn->Text = L"<<";
            this->back10Btn->UseVisualStyleBackColor = true;
            this->back10Btn->Click += gcnew System::EventHandler(this, &TowerUI::Back10Btn_Click);
            // 
            // startBtn
            // 
            this->startBtn->AutoSize = true;
            this->startBtn->Location = System::Drawing::Point(3, 304);
            this->startBtn->Name = L"startBtn";
            this->startBtn->Size = System::Drawing::Size(31, 23);
            this->startBtn->TabIndex = 12;
            this->startBtn->Text = L"|<";
            this->startBtn->UseVisualStyleBackColor = true;
            this->startBtn->Click += gcnew System::EventHandler(this, &TowerUI::StartBtn_Click);
            // 
            // removed
            // 
            this->removed->AutoSize = true;
            this->tableLayoutPanel1->SetColumnSpan(this->removed, 3);
            this->removed->Location = System::Drawing::Point(3, 51);
            this->removed->Margin = System::Windows::Forms::Padding(3, 5, 3, 3);
            this->removed->Name = L"removed";
            this->removed->Size = System::Drawing::Size(72, 17);
            this->removed->TabIndex = 6;
            this->removed->Text = L"Removed";
            this->removed->UseVisualStyleBackColor = true;
            this->removed->CheckedChanged += gcnew System::EventHandler(this, &TowerUI::Removed_CheckedChanged);
            // 
            // closeBtn
            // 
            this->closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->closeBtn->Location = System::Drawing::Point(644, 304);
            this->closeBtn->Name = L"closeBtn";
            this->closeBtn->Size = System::Drawing::Size(75, 23);
            this->closeBtn->TabIndex = 23;
            this->closeBtn->Text = L"Close";
            this->closeBtn->UseVisualStyleBackColor = true;
            this->closeBtn->Click += gcnew System::EventHandler(this, &TowerUI::CloseBtn_Click);
            // 
            // townEditor
            // 
            this->townEditor->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
            this->townEditor->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->tableLayoutPanel1->SetColumnSpan(this->townEditor, 3);
            this->townEditor->Dock = System::Windows::Forms::DockStyle::Fill;
            this->townEditor->FormattingEnabled = true;
            this->townEditor->Location = System::Drawing::Point(264, 22);
            this->townEditor->Name = L"townEditor";
            this->townEditor->Size = System::Drawing::Size(259, 21);
            this->townEditor->TabIndex = 4;
            this->townEditor->TextChanged += gcnew System::EventHandler(this, &TowerUI::TownEditor_TextChanged);
            // 
            // handbells
            // 
            this->handbells->AutoSize = true;
            this->tableLayoutPanel1->SetColumnSpan(this->handbells, 3);
            this->handbells->Location = System::Drawing::Point(529, 51);
            this->handbells->Margin = System::Windows::Forms::Padding(3, 5, 3, 3);
            this->handbells->Name = L"handbells";
            this->handbells->Size = System::Drawing::Size(76, 17);
            this->handbells->TabIndex = 25;
            this->handbells->Text = L"Hand bells";
            this->handbells->UseVisualStyleBackColor = true;
            this->handbells->CheckedChanged += gcnew System::EventHandler(this, &TowerUI::handbells_CheckedChanged);
            // 
            // cloneBtn
            // 
            this->cloneBtn->Location = System::Drawing::Point(264, 304);
            this->cloneBtn->Name = L"cloneBtn";
            this->cloneBtn->Size = System::Drawing::Size(75, 23);
            this->cloneBtn->TabIndex = 26;
            this->cloneBtn->Text = L"Clone";
            this->cloneBtn->UseVisualStyleBackColor = true;
            this->cloneBtn->Click += gcnew System::EventHandler(this, &TowerUI::cloneBtn_Click);
            // 
            // nextBtn
            // 
            this->nextBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->nextBtn->AutoSize = true;
            this->nextBtn->Location = System::Drawing::Point(128, 304);
            this->nextBtn->Margin = System::Windows::Forms::Padding(5, 3, 0, 3);
            this->nextBtn->Name = L"nextBtn";
            this->nextBtn->Size = System::Drawing::Size(31, 23);
            this->nextBtn->TabIndex = 15;
            this->nextBtn->Text = L">";
            this->nextBtn->UseVisualStyleBackColor = true;
            this->nextBtn->Click += gcnew System::EventHandler(this, &TowerUI::NextBtn_Click);
            // 
            // forward10Btn
            // 
            this->forward10Btn->AutoSize = true;
            this->forward10Btn->Location = System::Drawing::Point(162, 304);
            this->forward10Btn->Margin = System::Windows::Forms::Padding(3, 3, 0, 3);
            this->forward10Btn->Name = L"forward10Btn";
            this->forward10Btn->Size = System::Drawing::Size(31, 23);
            this->forward10Btn->TabIndex = 16;
            this->forward10Btn->Text = L">>";
            this->forward10Btn->UseVisualStyleBackColor = true;
            this->forward10Btn->Click += gcnew System::EventHandler(this, &TowerUI::Forward10Btn_Click);
            // 
            // endBtn
            // 
            this->endBtn->AutoSize = true;
            this->endBtn->Location = System::Drawing::Point(196, 304);
            this->endBtn->Margin = System::Windows::Forms::Padding(3, 3, 25, 3);
            this->endBtn->Name = L"endBtn";
            this->endBtn->Size = System::Drawing::Size(31, 23);
            this->endBtn->TabIndex = 17;
            this->endBtn->Text = L">|";
            this->endBtn->UseVisualStyleBackColor = true;
            this->endBtn->Click += gcnew System::EventHandler(this, &TowerUI::EndBtn_Click);
            // 
            // firstRungLbl
            // 
            this->firstRungLbl->AutoSize = true;
            this->firstRungLbl->Location = System::Drawing::Point(3, 4);
            this->firstRungLbl->Margin = System::Windows::Forms::Padding(3, 4, 3, 3);
            this->firstRungLbl->Name = L"firstRungLbl";
            this->firstRungLbl->Size = System::Drawing::Size(74, 13);
            this->firstRungLbl->TabIndex = 3;
            this->firstRungLbl->Text = L"First rung date";
            // 
            // firstRungDate
            // 
            this->firstRungDate->CustomFormat = L"ddd, d MMMM yyyy";
            this->firstRungDate->Format = System::Windows::Forms::DateTimePickerFormat::Custom;
            this->firstRungDate->Location = System::Drawing::Point(83, 3);
            this->firstRungDate->Name = L"firstRungDate";
            this->firstRungDate->Size = System::Drawing::Size(200, 20);
            this->firstRungDate->TabIndex = 1;
            this->toolTip1->SetToolTip(this->firstRungDate, L"Specify the date you first rang here. If none present, the first performance date"
                L" is used.");
            // 
            // linkedTowersLbl
            // 
            this->linkedTowersLbl->AutoEllipsis = true;
            this->linkedTowersLbl->AutoSize = true;
            this->linkedTowersLbl->Location = System::Drawing::Point(86, 60);
            this->linkedTowersLbl->Name = L"linkedTowersLbl";
            this->linkedTowersLbl->Size = System::Drawing::Size(86, 13);
            this->linkedTowersLbl->TabIndex = 5;
            this->linkedTowersLbl->Text = L"No linked towers";
            // 
            // TowerUI
            // 
            this->AcceptButton = this->saveBtn;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->CancelButton = this->cancelBtn;
            this->ClientSize = System::Drawing::Size(722, 381);
            this->Controls->Add(this->tableLayoutPanel1);
            this->Controls->Add(statusStrip1);
            this->MaximizeBox = false;
            this->MinimumSize = System::Drawing::Size(540, 340);
            this->Name = L"TowerUI";
            this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Show;
            this->Text = L"Tower";
            this->Activated += gcnew System::EventHandler(this, &TowerUI::TowerUI_Activated);
            this->Deactivate += gcnew System::EventHandler(this, &TowerUI::TowerUI_Deactivate);
            this->Load += gcnew System::EventHandler(this, &TowerUI::TowerUI_Load);
            statusStrip1->ResumeLayout(false);
            statusStrip1->PerformLayout();
            tabControl1->ResumeLayout(false);
            this->ringsTab->ResumeLayout(false);
            ringLabelPanel->ResumeLayout(false);
            ringLabelPanel->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ringSelector))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->noOfBellsInRingEditor))->EndInit();
            referencesPage->ResumeLayout(false);
            referencesPage->PerformLayout();
            tableLayoutPanel4->ResumeLayout(false);
            tableLayoutPanel4->PerformLayout();
            doveGrp->ResumeLayout(false);
            doveGrp->PerformLayout();
            tableLayoutPanel5->ResumeLayout(false);
            tableLayoutPanel5->PerformLayout();
            towerbaseGroup->ResumeLayout(false);
            towerbaseGroup->PerformLayout();
            linksPage->ResumeLayout(false);
            linksPage->PerformLayout();
            otherPage->ResumeLayout(false);
            otherLayoutPanel->ResumeLayout(false);
            otherLayoutPanel->PerformLayout();
            this->tableLayoutPanel1->ResumeLayout(false);
            this->tableLayoutPanel1->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numberOfBellsEditor))->EndInit();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
    };
}
