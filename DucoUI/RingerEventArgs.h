#pragma once

namespace DucoUI
{
    enum class TRingerChangeEventType
    {
        ERingerNameChanged,
        ERingerIdChanged,
        EConductor,
        EStrapper
    };

    public ref class RingerEventArgs: public System::EventArgs
    {
    private:
        TRingerChangeEventType  eventType;
        Duco::ObjectId*         ringerId;
        bool                    conducted;

    public:
        explicit RingerEventArgs( TRingerChangeEventType newType, const Duco::ObjectId& id, bool newConducted)
            :    eventType(newType), conducted (newConducted)
        {
            ringerId = new Duco::ObjectId(id);
        }

        !RingerEventArgs()
        {
            delete ringerId;
        }

        ~RingerEventArgs()
        {
            this->!RingerEventArgs();
        }

       property TRingerChangeEventType Type
       {
          TRingerChangeEventType get()
          { return eventType; }

          void set( TRingerChangeEventType value )
          { eventType = value; }
       }
       property Duco::ObjectId Id
       {
          Duco::ObjectId get()
          { return *ringerId; }

          void set( Duco::ObjectId value )
          { *ringerId = value; }
       }
       property bool IsConductor
       {
          bool get()
          { return conducted; }

          void set( bool value )
          { conducted = value; }
       }
    };
}