#include "PealFeesUI.h"
#include <Peal.h>
#include "DatabaseManager.h"
#include "SystemDefaultIcon.h"
#include "PealFeeList.h"
#include "DucoCommon.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::Windows::Forms;

PealFeesUI::PealFeesUI(DucoUI::DatabaseManager^ theManager, Duco::ObjectId pealId)
:   database(theManager), dataChanged(false), currentPeal(NULL)
{
    InitializeComponent();
    currentPeal = new Duco::Peal();
    database->FindPeal(pealId, *currentPeal, false);
}

PealFeesUI::!PealFeesUI()
{
    delete currentPeal;
}

PealFeesUI::~PealFeesUI()
{
    this->!PealFeesUI();
    if (components)
    {
        delete components;
    }
}

System::Void
PealFeesUI::PealFeesUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    if (!currentPeal->Id().ValidId() || !database->StartEdit(TObjectType::EPeal, currentPeal->Id()))
    {
        Close();
    }
    else
    {
        CreateRingersList();
    }
}

System::Void
PealFeesUI::PealFeesUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e)
{
    if ( dataChanged )
    {
        if ( MessageBox::Show(this, "Do you want to continue and lose changes?", "Duco - Unsaved changes to peal", MessageBoxButtons::YesNo, MessageBoxIcon::Warning ) == ::DialogResult::No )
        {
            e->Cancel = true;
        }
    }
    database->CancelEdit(TObjectType::EPeal, currentPeal->Id());
}

void
PealFeesUI::CreateRingersList()
{
    ringerList = gcnew PealFeeList(database, currentPeal);
    this->Controls->Add(this->ringerList);
    this->ringerList->AutoSize = true;
    this->ringerList->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
    this->ringerList->Location = System::Drawing::Point(5, 5);
    this->ringerList->Name = L"ringerList";
    this->ringerList->Size = System::Drawing::Size(367, KRingerLineHeight*10);
    this->ringerList->TabIndex = 1;
    this->ringerList->Text = L"Ringers";
    this->ringerList->AutoScroll = true;
    this->ringerList->Margin = System::Windows::Forms::Padding(1, 1, 1, 38);
    this->ringerList->dataChangedEventHandler += gcnew System::EventHandler(this, &PealFeesUI::RingerObjectChanged);
}

System::Void
PealFeesUI::RingerObjectChanged(System::Object^  sender, System::EventArgs^  e)
{
    dataChanged = true;
}

System::Void
PealFeesUI::SaveBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (dataChanged)
    {
        database->UpdatePeal(*currentPeal, true);
        dataChanged = false;
        Close();
    }
}

System::Void
PealFeesUI::CloseBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}
