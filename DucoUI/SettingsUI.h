#pragma once
namespace DucoUI
{
    public ref class SettingsUI : public System::Windows::Forms::Form
    {
    public:
        SettingsUI(DucoUI::DatabaseManager^ theDatabase);

    protected:
        ~SettingsUI();

        System::Void SettingsUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void ClosingEvent( Object^ /*sender*/, System::ComponentModel::CancelEventArgs^ e );

        System::Void defaultRingerSelector_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void allBells_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void longLine_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void methodGrid_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void showGrid_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void googleMapsKey_TextChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void mapBoxKey_TextChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void googleMapsBtn_CheckedChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void mapBoxBtn_CheckedChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void ChangedDatabaseSetting(System::Object^  sender, System::EventArgs^  e);
        System::Void ChangedWindowsSetting(System::Object^  sender, System::EventArgs^  e);

        System::Void websitesBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void cancelBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void saveBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void resetBtn_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void PopulateSettings();
        System::Void PopulateSettingsFrom(const Duco::DatabaseSettings& settings, DucoUI::WindowsSettings^ windowsSettings);
        System::Void SaveSettings();
        void SetDataChanged(bool windows, bool database);

    private:
        System::Boolean                             windowsSettingsChanged;
        System::Boolean                             databaseSettingsChanged;
        System::Boolean                             dataChangedCallObservers;
        System::Boolean                             populatingData;
        System::ComponentModel::IContainer^         components;
        System::Windows::Forms::CheckBox^           surnamesFirst;
        System::Windows::Forms::ComboBox^           defaultRingerSelector;
        DucoUI::DatabaseManager^                    database;
        DucoUI::RingerDisplayCache^                 ringerCache;
        System::Windows::Forms::RadioButton^        alphabeticRenumber;
        System::Windows::Forms::CheckBox^           showTreble;
        System::Windows::Forms::CheckBox^           showGrid;
        System::Windows::Forms::RadioButton^        visMethSelector;
        System::Windows::Forms::Button^             saveBtn;
        System::Windows::Forms::Button^             resetBtn;
        System::Windows::Forms::CheckBox^           printPreview;
        System::Windows::Forms::CheckBox^           disablePerformance;
        System::Windows::Forms::CheckBox^           methodGrid;
        System::Windows::Forms::CheckBox^           showCallings;
        System::Windows::Forms::RadioButton^        longLine;
        System::Windows::Forms::RadioButton^        allBells;
        System::Windows::Forms::RadioButton^        normalLine;
        System::Windows::Forms::CheckBox^           allowErrors;
        System::Windows::Forms::RadioButton^        pealBoardFormat;
        System::Windows::Forms::RadioButton^        rwFormat;
        System::Windows::Forms::CheckBox^           printSurnameFirst;
        System::Windows::Forms::RadioButton^        fullPeals;
        System::Windows::Forms::RadioButton^        quarterPeals;
        System::Windows::Forms::TextBox^            doveURLEditor;
        System::Windows::Forms::CheckBox^           keepTowersWithDate;
        System::Windows::Forms::ToolTip^            toolTip;
        System::Windows::Forms::RadioButton^        createNewMethod;
        System::Windows::Forms::RadioButton^        importNewMethod;
    private: System::Windows::Forms::Label^  doveTowerLbl;
    private: System::Windows::Forms::Label^  pealsCoUkTowerLbl;

    private: System::Windows::Forms::CheckBox^  exportToXmlIncludesIds;


    private: System::Windows::Forms::TextBox^ mapBoxKey;

    private: System::Windows::Forms::Button^ websitesBtn;
    private: System::Windows::Forms::CheckBox^ missingCallingNotation;
    private: System::Windows::Forms::CheckBox^ seriesMissingWarning;
    private: System::Windows::Forms::CheckBox^ missingAssociationsWarning;
    private: System::Windows::Forms::TextBox^ warningsDescriptionLbl;
    private: System::Windows::Forms::CheckBox^ ignoreMissingTime;
    private: System::Windows::Forms::CheckBox^ enableSetRWPageOnUpload;


        void InitializeComponent()
        {
            this->components = (gcnew System::ComponentModel::Container());
            System::Windows::Forms::Label^ defaultRingerlbl;
            System::Windows::Forms::GroupBox^ ringersSettingsGrp;
            System::Windows::Forms::GroupBox^ doveTowerSettingsGrp;
            System::Windows::Forms::GroupBox^ reorderGrp;
            System::Windows::Forms::RadioButton^ sortPealCount;
            System::Windows::Forms::GroupBox^ methodOptionsBox;
            System::Windows::Forms::RadioButton^ bluelineSelector;
            System::Windows::Forms::GroupBox^ methodPealUIGrp;
            System::Windows::Forms::GroupBox^ printPreviewGrp;
            System::Windows::Forms::GroupBox^ printFormatGroup;
            System::Windows::Forms::GroupBox^ databaseTypeGroup;
            System::Windows::Forms::GroupBox^ methodLineTypeGroup;
            System::Windows::Forms::GroupBox^ databaseGrp;
            System::Windows::Forms::Button^ cancelBtn;
            System::Windows::Forms::TabControl^ settingsTabCtrl;
            System::Windows::Forms::TabPage^ generalPage;
            System::Windows::Forms::TabPage^ pealsPage;
            System::Windows::Forms::GroupBox^ bellboard;
            System::Windows::Forms::TabPage^ towersPage;
            System::Windows::Forms::TabPage^ ringersPage;
            System::Windows::Forms::TabPage^ methodsPage;
            System::Windows::Forms::TabPage^ mapPage;
            System::Windows::Forms::Label^ mapBoxLabel;
            System::Windows::Forms::TabPage^ warningsPage;
            System::Windows::Forms::TableLayoutPanel^ masterLayoutPanel;
            this->defaultRingerSelector = (gcnew System::Windows::Forms::ComboBox());
            this->surnamesFirst = (gcnew System::Windows::Forms::CheckBox());
            this->doveTowerLbl = (gcnew System::Windows::Forms::Label());
            this->doveURLEditor = (gcnew System::Windows::Forms::TextBox());
            this->alphabeticRenumber = (gcnew System::Windows::Forms::RadioButton());
            this->showGrid = (gcnew System::Windows::Forms::CheckBox());
            this->visMethSelector = (gcnew System::Windows::Forms::RadioButton());
            this->methodGrid = (gcnew System::Windows::Forms::CheckBox());
            this->showCallings = (gcnew System::Windows::Forms::CheckBox());
            this->showTreble = (gcnew System::Windows::Forms::CheckBox());
            this->importNewMethod = (gcnew System::Windows::Forms::RadioButton());
            this->createNewMethod = (gcnew System::Windows::Forms::RadioButton());
            this->printPreview = (gcnew System::Windows::Forms::CheckBox());
            this->printSurnameFirst = (gcnew System::Windows::Forms::CheckBox());
            this->pealBoardFormat = (gcnew System::Windows::Forms::RadioButton());
            this->rwFormat = (gcnew System::Windows::Forms::RadioButton());
            this->quarterPeals = (gcnew System::Windows::Forms::RadioButton());
            this->fullPeals = (gcnew System::Windows::Forms::RadioButton());
            this->longLine = (gcnew System::Windows::Forms::RadioButton());
            this->normalLine = (gcnew System::Windows::Forms::RadioButton());
            this->allBells = (gcnew System::Windows::Forms::RadioButton());
            this->allowErrors = (gcnew System::Windows::Forms::CheckBox());
            this->websitesBtn = (gcnew System::Windows::Forms::Button());
            this->exportToXmlIncludesIds = (gcnew System::Windows::Forms::CheckBox());
            this->disablePerformance = (gcnew System::Windows::Forms::CheckBox());
            this->enableSetRWPageOnUpload = (gcnew System::Windows::Forms::CheckBox());
            this->keepTowersWithDate = (gcnew System::Windows::Forms::CheckBox());
            this->mapBoxKey = (gcnew System::Windows::Forms::TextBox());
            this->ignoreMissingTime = (gcnew System::Windows::Forms::CheckBox());
            this->missingCallingNotation = (gcnew System::Windows::Forms::CheckBox());
            this->seriesMissingWarning = (gcnew System::Windows::Forms::CheckBox());
            this->missingAssociationsWarning = (gcnew System::Windows::Forms::CheckBox());
            this->warningsDescriptionLbl = (gcnew System::Windows::Forms::TextBox());
            this->saveBtn = (gcnew System::Windows::Forms::Button());
            this->resetBtn = (gcnew System::Windows::Forms::Button());
            this->pealsCoUkTowerLbl = (gcnew System::Windows::Forms::Label());
            this->toolTip = (gcnew System::Windows::Forms::ToolTip(this->components));
            defaultRingerlbl = (gcnew System::Windows::Forms::Label());
            ringersSettingsGrp = (gcnew System::Windows::Forms::GroupBox());
            doveTowerSettingsGrp = (gcnew System::Windows::Forms::GroupBox());
            reorderGrp = (gcnew System::Windows::Forms::GroupBox());
            sortPealCount = (gcnew System::Windows::Forms::RadioButton());
            methodOptionsBox = (gcnew System::Windows::Forms::GroupBox());
            bluelineSelector = (gcnew System::Windows::Forms::RadioButton());
            methodPealUIGrp = (gcnew System::Windows::Forms::GroupBox());
            printPreviewGrp = (gcnew System::Windows::Forms::GroupBox());
            printFormatGroup = (gcnew System::Windows::Forms::GroupBox());
            databaseTypeGroup = (gcnew System::Windows::Forms::GroupBox());
            methodLineTypeGroup = (gcnew System::Windows::Forms::GroupBox());
            databaseGrp = (gcnew System::Windows::Forms::GroupBox());
            cancelBtn = (gcnew System::Windows::Forms::Button());
            settingsTabCtrl = (gcnew System::Windows::Forms::TabControl());
            generalPage = (gcnew System::Windows::Forms::TabPage());
            pealsPage = (gcnew System::Windows::Forms::TabPage());
            bellboard = (gcnew System::Windows::Forms::GroupBox());
            towersPage = (gcnew System::Windows::Forms::TabPage());
            ringersPage = (gcnew System::Windows::Forms::TabPage());
            methodsPage = (gcnew System::Windows::Forms::TabPage());
            mapPage = (gcnew System::Windows::Forms::TabPage());
            mapBoxLabel = (gcnew System::Windows::Forms::Label());
            warningsPage = (gcnew System::Windows::Forms::TabPage());
            masterLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
            ringersSettingsGrp->SuspendLayout();
            doveTowerSettingsGrp->SuspendLayout();
            reorderGrp->SuspendLayout();
            methodOptionsBox->SuspendLayout();
            methodPealUIGrp->SuspendLayout();
            printPreviewGrp->SuspendLayout();
            printFormatGroup->SuspendLayout();
            databaseTypeGroup->SuspendLayout();
            methodLineTypeGroup->SuspendLayout();
            databaseGrp->SuspendLayout();
            settingsTabCtrl->SuspendLayout();
            generalPage->SuspendLayout();
            pealsPage->SuspendLayout();
            bellboard->SuspendLayout();
            towersPage->SuspendLayout();
            ringersPage->SuspendLayout();
            methodsPage->SuspendLayout();
            mapPage->SuspendLayout();
            warningsPage->SuspendLayout();
            masterLayoutPanel->SuspendLayout();
            this->SuspendLayout();
            // 
            // defaultRingerlbl
            // 
            defaultRingerlbl->AutoSize = true;
            defaultRingerlbl->Location = System::Drawing::Point(6, 22);
            defaultRingerlbl->Name = L"defaultRingerlbl";
            defaultRingerlbl->Size = System::Drawing::Size(41, 13);
            defaultRingerlbl->TabIndex = 0;
            defaultRingerlbl->Text = L"Default";
            // 
            // ringersSettingsGrp
            // 
            ringersSettingsGrp->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            ringersSettingsGrp->Controls->Add(this->defaultRingerSelector);
            ringersSettingsGrp->Controls->Add(defaultRingerlbl);
            ringersSettingsGrp->Controls->Add(this->surnamesFirst);
            ringersSettingsGrp->Location = System::Drawing::Point(6, 6);
            ringersSettingsGrp->Name = L"ringersSettingsGrp";
            ringersSettingsGrp->Size = System::Drawing::Size(280, 71);
            ringersSettingsGrp->TabIndex = 0;
            ringersSettingsGrp->TabStop = false;
            ringersSettingsGrp->Text = L"Ringers";
            // 
            // defaultRingerSelector
            // 
            this->defaultRingerSelector->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::CustomSource;
            this->defaultRingerSelector->FormattingEnabled = true;
            this->defaultRingerSelector->Location = System::Drawing::Point(53, 19);
            this->defaultRingerSelector->Name = L"defaultRingerSelector";
            this->defaultRingerSelector->Size = System::Drawing::Size(221, 21);
            this->defaultRingerSelector->TabIndex = 1;
            this->toolTip->SetToolTip(this->defaultRingerSelector, L"Specifies the default ringer, who must be in all peals.");
            this->defaultRingerSelector->SelectedIndexChanged += gcnew System::EventHandler(this, &SettingsUI::defaultRingerSelector_SelectedIndexChanged);
            // 
            // surnamesFirst
            // 
            this->surnamesFirst->AutoSize = true;
            this->surnamesFirst->Location = System::Drawing::Point(53, 46);
            this->surnamesFirst->Name = L"surnamesFirst";
            this->surnamesFirst->Size = System::Drawing::Size(87, 17);
            this->surnamesFirst->TabIndex = 2;
            this->surnamesFirst->Text = L"Surname first";
            this->surnamesFirst->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            this->toolTip->SetToolTip(this->surnamesFirst, L"If checked, ringers are shown surname first, else forenames first.");
            this->surnamesFirst->UseVisualStyleBackColor = true;
            this->surnamesFirst->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::ChangedDatabaseSetting);
            // 
            // doveTowerSettingsGrp
            // 
            doveTowerSettingsGrp->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            doveTowerSettingsGrp->Controls->Add(this->doveTowerLbl);
            doveTowerSettingsGrp->Controls->Add(this->doveURLEditor);
            doveTowerSettingsGrp->Location = System::Drawing::Point(6, 6);
            doveTowerSettingsGrp->Name = L"doveTowerSettingsGrp";
            doveTowerSettingsGrp->Size = System::Drawing::Size(280, 45);
            doveTowerSettingsGrp->TabIndex = 0;
            doveTowerSettingsGrp->TabStop = false;
            doveTowerSettingsGrp->Text = L"Dove";
            // 
            // doveTowerLbl
            // 
            this->doveTowerLbl->AutoSize = true;
            this->doveTowerLbl->Location = System::Drawing::Point(3, 20);
            this->doveTowerLbl->Name = L"doveTowerLbl";
            this->doveTowerLbl->Size = System::Drawing::Size(29, 13);
            this->doveTowerLbl->TabIndex = 0;
            this->doveTowerLbl->Text = L"URL";
            // 
            // doveURLEditor
            // 
            this->doveURLEditor->Location = System::Drawing::Point(37, 17);
            this->doveURLEditor->Name = L"doveURLEditor";
            this->doveURLEditor->Size = System::Drawing::Size(230, 20);
            this->doveURLEditor->TabIndex = 1;
            this->doveURLEditor->TextChanged += gcnew System::EventHandler(this, &SettingsUI::ChangedDatabaseSetting);
            // 
            // reorderGrp
            // 
            reorderGrp->Controls->Add(sortPealCount);
            reorderGrp->Controls->Add(this->alphabeticRenumber);
            reorderGrp->Location = System::Drawing::Point(6, 6);
            reorderGrp->Name = L"reorderGrp";
            reorderGrp->Size = System::Drawing::Size(280, 43);
            reorderGrp->TabIndex = 1;
            reorderGrp->TabStop = false;
            reorderGrp->Text = L"Reordering";
            // 
            // sortPealCount
            // 
            sortPealCount->AutoSize = true;
            sortPealCount->Checked = true;
            sortPealCount->Location = System::Drawing::Point(143, 19);
            sortPealCount->Name = L"sortPealCount";
            sortPealCount->Size = System::Drawing::Size(115, 17);
            sortPealCount->TabIndex = 2;
            sortPealCount->TabStop = true;
            sortPealCount->Text = L"Performance count";
            this->toolTip->SetToolTip(sortPealCount, L"When rebuilding the database, reorder objects by peal count.");
            sortPealCount->UseVisualStyleBackColor = true;
            sortPealCount->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::ChangedDatabaseSetting);
            // 
            // alphabeticRenumber
            // 
            this->alphabeticRenumber->AutoSize = true;
            this->alphabeticRenumber->Location = System::Drawing::Point(7, 19);
            this->alphabeticRenumber->Name = L"alphabeticRenumber";
            this->alphabeticRenumber->Size = System::Drawing::Size(83, 17);
            this->alphabeticRenumber->TabIndex = 1;
            this->alphabeticRenumber->Text = L"Alphabetical";
            this->toolTip->SetToolTip(this->alphabeticRenumber, L"When rebuilding the database, sort all objects alphabetically.");
            this->alphabeticRenumber->UseVisualStyleBackColor = true;
            this->alphabeticRenumber->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::ChangedDatabaseSetting);
            // 
            // methodOptionsBox
            // 
            methodOptionsBox->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            methodOptionsBox->Controls->Add(bluelineSelector);
            methodOptionsBox->Controls->Add(this->showGrid);
            methodOptionsBox->Controls->Add(this->visMethSelector);
            methodOptionsBox->Controls->Add(this->methodGrid);
            methodOptionsBox->Controls->Add(this->showCallings);
            methodOptionsBox->Controls->Add(this->showTreble);
            methodOptionsBox->Location = System::Drawing::Point(6, 100);
            methodOptionsBox->Name = L"methodOptionsBox";
            methodOptionsBox->Size = System::Drawing::Size(280, 90);
            methodOptionsBox->TabIndex = 1;
            methodOptionsBox->TabStop = false;
            methodOptionsBox->Text = L"Options";
            // 
            // bluelineSelector
            // 
            bluelineSelector->AutoSize = true;
            bluelineSelector->Checked = true;
            bluelineSelector->Location = System::Drawing::Point(140, 66);
            bluelineSelector->Name = L"bluelineSelector";
            bluelineSelector->Size = System::Drawing::Size(66, 17);
            bluelineSelector->TabIndex = 5;
            bluelineSelector->TabStop = true;
            bluelineSelector->Text = L"BlueLine";
            this->toolTip->SetToolTip(bluelineSelector, L"Use Paul Graupner\\\'s website to show BlueLines");
            bluelineSelector->UseVisualStyleBackColor = true;
            bluelineSelector->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::ChangedDatabaseSetting);
            // 
            // showGrid
            // 
            this->showGrid->AutoSize = true;
            this->showGrid->Location = System::Drawing::Point(140, 20);
            this->showGrid->Name = L"showGrid";
            this->showGrid->Size = System::Drawing::Size(137, 17);
            this->showGrid->TabIndex = 1;
            this->showGrid->Text = L"Show background lines";
            this->toolTip->SetToolTip(this->showGrid, L"Displays a background grid to make method lines easier to read");
            this->showGrid->UseVisualStyleBackColor = true;
            this->showGrid->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::showGrid_CheckedChanged);
            // 
            // visMethSelector
            // 
            this->visMethSelector->AutoSize = true;
            this->visMethSelector->Location = System::Drawing::Point(7, 66);
            this->visMethSelector->Name = L"visMethSelector";
            this->visMethSelector->Size = System::Drawing::Size(130, 17);
            this->visMethSelector->TabIndex = 4;
            this->visMethSelector->Text = L"Visual Method archive";
            this->toolTip->SetToolTip(this->visMethSelector, L"Use Adam J Beers website to show method lines");
            this->visMethSelector->UseVisualStyleBackColor = true;
            this->visMethSelector->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::ChangedDatabaseSetting);
            // 
            // methodGrid
            // 
            this->methodGrid->AutoSize = true;
            this->methodGrid->Location = System::Drawing::Point(7, 43);
            this->methodGrid->Name = L"methodGrid";
            this->methodGrid->Size = System::Drawing::Size(111, 17);
            this->methodGrid->TabIndex = 2;
            this->methodGrid->Text = L"Show method grid";
            this->toolTip->SetToolTip(this->methodGrid, L"Displays all leads together in a single grid");
            this->methodGrid->UseVisualStyleBackColor = true;
            this->methodGrid->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::methodGrid_CheckedChanged);
            // 
            // showCallings
            // 
            this->showCallings->AutoSize = true;
            this->showCallings->Location = System::Drawing::Point(140, 43);
            this->showCallings->Name = L"showCallings";
            this->showCallings->Size = System::Drawing::Size(91, 17);
            this->showCallings->TabIndex = 3;
            this->showCallings->Text = L"Show callings";
            this->toolTip->SetToolTip(this->showCallings, L"Displays the bobs and singles in the method drawing");
            this->showCallings->UseVisualStyleBackColor = true;
            this->showCallings->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::ChangedDatabaseSetting);
            // 
            // showTreble
            // 
            this->showTreble->AutoSize = true;
            this->showTreble->Location = System::Drawing::Point(7, 20);
            this->showTreble->Name = L"showTreble";
            this->showTreble->Size = System::Drawing::Size(82, 17);
            this->showTreble->TabIndex = 0;
            this->showTreble->Text = L"Show treble";
            this->toolTip->SetToolTip(this->showTreble, L"Shows the treble when drawing method lines");
            this->showTreble->UseVisualStyleBackColor = true;
            this->showTreble->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::ChangedDatabaseSetting);
            // 
            // methodPealUIGrp
            // 
            methodPealUIGrp->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            methodPealUIGrp->Controls->Add(this->importNewMethod);
            methodPealUIGrp->Controls->Add(this->createNewMethod);
            methodPealUIGrp->Location = System::Drawing::Point(6, 195);
            methodPealUIGrp->Name = L"methodPealUIGrp";
            methodPealUIGrp->Size = System::Drawing::Size(280, 45);
            methodPealUIGrp->TabIndex = 2;
            methodPealUIGrp->TabStop = false;
            methodPealUIGrp->Text = L"Missing methods in peals";
            // 
            // importNewMethod
            // 
            this->importNewMethod->AutoSize = true;
            this->importNewMethod->Location = System::Drawing::Point(140, 20);
            this->importNewMethod->Name = L"importNewMethod";
            this->importNewMethod->Size = System::Drawing::Size(129, 17);
            this->importNewMethod->TabIndex = 1;
            this->importNewMethod->Text = L"Import missing method";
            this->toolTip->SetToolTip(this->importNewMethod, L"When creating a new peal, if the method doesn\'t exist, Import one from the Centra"
                L"l Council collection.");
            this->importNewMethod->UseVisualStyleBackColor = true;
            this->importNewMethod->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::ChangedWindowsSetting);
            // 
            // createNewMethod
            // 
            this->createNewMethod->AutoSize = true;
            this->createNewMethod->Location = System::Drawing::Point(7, 20);
            this->createNewMethod->Name = L"createNewMethod";
            this->createNewMethod->Size = System::Drawing::Size(117, 17);
            this->createNewMethod->TabIndex = 0;
            this->createNewMethod->Text = L"Create new method";
            this->toolTip->SetToolTip(this->createNewMethod, L"When creating a new peal, if the method doesn\'t exist, Automatically create a new"
                L" one.");
            this->createNewMethod->UseVisualStyleBackColor = true;
            this->createNewMethod->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::ChangedWindowsSetting);
            // 
            // printPreviewGrp
            // 
            printPreviewGrp->Controls->Add(this->printPreview);
            printPreviewGrp->Location = System::Drawing::Point(6, 55);
            printPreviewGrp->Name = L"printPreviewGrp";
            printPreviewGrp->Size = System::Drawing::Size(280, 48);
            printPreviewGrp->TabIndex = 2;
            printPreviewGrp->TabStop = false;
            printPreviewGrp->Text = L"Printing";
            // 
            // printPreview
            // 
            this->printPreview->AutoSize = true;
            this->printPreview->Location = System::Drawing::Point(7, 20);
            this->printPreview->Name = L"printPreview";
            this->printPreview->Size = System::Drawing::Size(109, 17);
            this->printPreview->TabIndex = 1;
            this->printPreview->Text = L"Use Print preview";
            this->toolTip->SetToolTip(this->printPreview, L"Before printing display the print preview dialog.");
            this->printPreview->UseVisualStyleBackColor = true;
            this->printPreview->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::ChangedDatabaseSetting);
            // 
            // printFormatGroup
            // 
            printFormatGroup->Controls->Add(this->printSurnameFirst);
            printFormatGroup->Controls->Add(this->pealBoardFormat);
            printFormatGroup->Controls->Add(this->rwFormat);
            printFormatGroup->Location = System::Drawing::Point(6, 55);
            printFormatGroup->Name = L"printFormatGroup";
            printFormatGroup->Size = System::Drawing::Size(279, 68);
            printFormatGroup->TabIndex = 4;
            printFormatGroup->TabStop = false;
            printFormatGroup->Text = L"Print format";
            // 
            // printSurnameFirst
            // 
            this->printSurnameFirst->AutoSize = true;
            this->printSurnameFirst->Location = System::Drawing::Point(6, 43);
            this->printSurnameFirst->Name = L"printSurnameFirst";
            this->printSurnameFirst->Size = System::Drawing::Size(143, 17);
            this->printSurnameFirst->TabIndex = 2;
            this->printSurnameFirst->Text = L"Print ringers surname first";
            this->toolTip->SetToolTip(this->printSurnameFirst, L"Print ringers names, surname first.");
            this->printSurnameFirst->UseVisualStyleBackColor = true;
            this->printSurnameFirst->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::ChangedDatabaseSetting);
            // 
            // pealBoardFormat
            // 
            this->pealBoardFormat->AutoSize = true;
            this->pealBoardFormat->Location = System::Drawing::Point(139, 19);
            this->pealBoardFormat->Name = L"pealBoardFormat";
            this->pealBoardFormat->Size = System::Drawing::Size(76, 17);
            this->pealBoardFormat->TabIndex = 1;
            this->pealBoardFormat->TabStop = true;
            this->pealBoardFormat->Text = L"Peal board";
            this->pealBoardFormat->UseVisualStyleBackColor = true;
            this->pealBoardFormat->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::ChangedDatabaseSetting);
            // 
            // rwFormat
            // 
            this->rwFormat->AutoSize = true;
            this->rwFormat->Location = System::Drawing::Point(6, 19);
            this->rwFormat->Name = L"rwFormat";
            this->rwFormat->Size = System::Drawing::Size(89, 17);
            this->rwFormat->TabIndex = 0;
            this->rwFormat->TabStop = true;
            this->rwFormat->Text = L"Ringing world";
            this->rwFormat->UseVisualStyleBackColor = true;
            this->rwFormat->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::ChangedDatabaseSetting);
            // 
            // databaseTypeGroup
            // 
            databaseTypeGroup->Controls->Add(this->quarterPeals);
            databaseTypeGroup->Controls->Add(this->fullPeals);
            databaseTypeGroup->Location = System::Drawing::Point(6, 110);
            databaseTypeGroup->Name = L"databaseTypeGroup";
            databaseTypeGroup->Size = System::Drawing::Size(280, 49);
            databaseTypeGroup->TabIndex = 3;
            databaseTypeGroup->TabStop = false;
            databaseTypeGroup->Text = L"Database type";
            // 
            // quarterPeals
            // 
            this->quarterPeals->AutoSize = true;
            this->quarterPeals->Location = System::Drawing::Point(143, 20);
            this->quarterPeals->Name = L"quarterPeals";
            this->quarterPeals->Size = System::Drawing::Size(88, 17);
            this->quarterPeals->TabIndex = 2;
            this->quarterPeals->TabStop = true;
            this->quarterPeals->Text = L"Quarter peals";
            this->quarterPeals->UseVisualStyleBackColor = true;
            // 
            // fullPeals
            // 
            this->fullPeals->AutoSize = true;
            this->fullPeals->Location = System::Drawing::Point(7, 20);
            this->fullPeals->Name = L"fullPeals";
            this->fullPeals->Size = System::Drawing::Size(69, 17);
            this->fullPeals->TabIndex = 1;
            this->fullPeals->TabStop = true;
            this->fullPeals->Text = L"Full peals";
            this->fullPeals->UseVisualStyleBackColor = true;
            this->fullPeals->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::ChangedDatabaseSetting);
            // 
            // methodLineTypeGroup
            // 
            methodLineTypeGroup->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            methodLineTypeGroup->Controls->Add(this->longLine);
            methodLineTypeGroup->Controls->Add(this->normalLine);
            methodLineTypeGroup->Controls->Add(this->allBells);
            methodLineTypeGroup->Location = System::Drawing::Point(6, 6);
            methodLineTypeGroup->Name = L"methodLineTypeGroup";
            methodLineTypeGroup->Size = System::Drawing::Size(280, 91);
            methodLineTypeGroup->TabIndex = 0;
            methodLineTypeGroup->TabStop = false;
            methodLineTypeGroup->Text = L"Method line type";
            // 
            // longLine
            // 
            this->longLine->AutoSize = true;
            this->longLine->Location = System::Drawing::Point(7, 43);
            this->longLine->Name = L"longLine";
            this->longLine->Size = System::Drawing::Size(87, 17);
            this->longLine->TabIndex = 1;
            this->longLine->Text = L"One long line";
            this->toolTip->SetToolTip(this->longLine, L"Shows the method line as one long continious line, not seperated into leads.");
            this->longLine->UseVisualStyleBackColor = true;
            this->longLine->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::longLine_CheckedChanged);
            // 
            // normalLine
            // 
            this->normalLine->AutoSize = true;
            this->normalLine->Checked = true;
            this->normalLine->Location = System::Drawing::Point(7, 20);
            this->normalLine->Name = L"normalLine";
            this->normalLine->Size = System::Drawing::Size(77, 17);
            this->normalLine->TabIndex = 0;
            this->normalLine->TabStop = true;
            this->normalLine->Text = L"Normal line";
            this->toolTip->SetToolTip(this->normalLine, L"Shows the method as a series of blue lines, one per lead.");
            this->normalLine->UseVisualStyleBackColor = true;
            this->normalLine->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::ChangedDatabaseSetting);
            // 
            // allBells
            // 
            this->allBells->AutoSize = true;
            this->allBells->Location = System::Drawing::Point(7, 66);
            this->allBells->Name = L"allBells";
            this->allBells->Size = System::Drawing::Size(89, 17);
            this->allBells->TabIndex = 2;
            this->allBells->Text = L"Show all bells";
            this->toolTip->SetToolTip(this->allBells, L"Shows the full method grid seperated into leads.");
            this->allBells->UseVisualStyleBackColor = true;
            this->allBells->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::allBells_CheckedChanged);
            // 
            // databaseGrp
            // 
            databaseGrp->Controls->Add(this->allowErrors);
            databaseGrp->Location = System::Drawing::Point(6, 6);
            databaseGrp->Name = L"databaseGrp";
            databaseGrp->Size = System::Drawing::Size(279, 43);
            databaseGrp->TabIndex = 3;
            databaseGrp->TabStop = false;
            databaseGrp->Text = L"Database";
            // 
            // allowErrors
            // 
            this->allowErrors->AutoSize = true;
            this->allowErrors->Location = System::Drawing::Point(6, 19);
            this->allowErrors->Name = L"allowErrors";
            this->allowErrors->Size = System::Drawing::Size(228, 17);
            this->allowErrors->TabIndex = 0;
            this->allowErrors->Text = L"Allow performances with errors to be saved";
            this->toolTip->SetToolTip(this->allowErrors, L"When saving peals is errors are detected, warn but don\\\'t prevent save.");
            this->allowErrors->UseVisualStyleBackColor = true;
            this->allowErrors->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::ChangedDatabaseSetting);
            // 
            // cancelBtn
            // 
            cancelBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            cancelBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            cancelBtn->Location = System::Drawing::Point(228, 298);
            cancelBtn->Name = L"cancelBtn";
            cancelBtn->Size = System::Drawing::Size(75, 23);
            cancelBtn->TabIndex = 3;
            cancelBtn->Text = L"&Cancel";
            cancelBtn->UseVisualStyleBackColor = true;
            cancelBtn->Click += gcnew System::EventHandler(this, &SettingsUI::cancelBtn_Click);
            // 
            // settingsTabCtrl
            // 
            masterLayoutPanel->SetColumnSpan(settingsTabCtrl, 3);
            settingsTabCtrl->Controls->Add(generalPage);
            settingsTabCtrl->Controls->Add(pealsPage);
            settingsTabCtrl->Controls->Add(towersPage);
            settingsTabCtrl->Controls->Add(ringersPage);
            settingsTabCtrl->Controls->Add(methodsPage);
            settingsTabCtrl->Controls->Add(mapPage);
            settingsTabCtrl->Controls->Add(warningsPage);
            settingsTabCtrl->Dock = System::Windows::Forms::DockStyle::Fill;
            settingsTabCtrl->Location = System::Drawing::Point(3, 3);
            settingsTabCtrl->Multiline = true;
            settingsTabCtrl->Name = L"settingsTabCtrl";
            settingsTabCtrl->SelectedIndex = 0;
            settingsTabCtrl->Size = System::Drawing::Size(300, 289);
            settingsTabCtrl->TabIndex = 0;
            // 
            // generalPage
            // 
            generalPage->Controls->Add(this->websitesBtn);
            generalPage->Controls->Add(this->exportToXmlIncludesIds);
            generalPage->Controls->Add(databaseTypeGroup);
            generalPage->Controls->Add(reorderGrp);
            generalPage->Controls->Add(printPreviewGrp);
            generalPage->Controls->Add(this->disablePerformance);
            generalPage->Location = System::Drawing::Point(4, 40);
            generalPage->Name = L"generalPage";
            generalPage->Padding = System::Windows::Forms::Padding(3);
            generalPage->Size = System::Drawing::Size(292, 245);
            generalPage->TabIndex = 0;
            generalPage->Text = L"General";
            generalPage->UseVisualStyleBackColor = true;
            // 
            // websitesBtn
            // 
            this->websitesBtn->AutoSize = true;
            this->websitesBtn->Location = System::Drawing::Point(200, 217);
            this->websitesBtn->Margin = System::Windows::Forms::Padding(12, 3, 12, 12);
            this->websitesBtn->Name = L"websitesBtn";
            this->websitesBtn->Size = System::Drawing::Size(80, 23);
            this->websitesBtn->TabIndex = 6;
            this->websitesBtn->Text = L"&Website links";
            this->websitesBtn->UseVisualStyleBackColor = true;
            this->websitesBtn->Click += gcnew System::EventHandler(this, &SettingsUI::websitesBtn_Click);
            // 
            // exportToXmlIncludesIds
            // 
            this->exportToXmlIncludesIds->AutoSize = true;
            this->exportToXmlIncludesIds->Location = System::Drawing::Point(7, 187);
            this->exportToXmlIncludesIds->Name = L"exportToXmlIncludesIds";
            this->exportToXmlIncludesIds->Size = System::Drawing::Size(184, 17);
            this->exportToXmlIncludesIds->TabIndex = 5;
            this->exportToXmlIncludesIds->Text = L"On export to xml include Duco ids";
            this->toolTip->SetToolTip(this->exportToXmlIncludesIds, L"When saving a database as an XML, include all duco ids and objects. It can be hel"
                L"pful to disable all this extra info, for example: when generating exports for co"
                L"mparisions.");
            this->exportToXmlIncludesIds->UseVisualStyleBackColor = true;
            this->exportToXmlIncludesIds->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::ChangedWindowsSetting);
            // 
            // disablePerformance
            // 
            this->disablePerformance->AutoSize = true;
            this->disablePerformance->Location = System::Drawing::Point(7, 164);
            this->disablePerformance->Name = L"disablePerformance";
            this->disablePerformance->Size = System::Drawing::Size(190, 17);
            this->disablePerformance->TabIndex = 4;
            this->disablePerformance->Text = L"Slow computer or Large databases";
            this->toolTip->SetToolTip(this->disablePerformance, L"Disable some features which require faster computers. Enable this if Duco is slow"
                L" at some operations.");
            this->disablePerformance->UseVisualStyleBackColor = true;
            this->disablePerformance->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::ChangedWindowsSetting);
            // 
            // pealsPage
            // 
            pealsPage->Controls->Add(bellboard);
            pealsPage->Controls->Add(printFormatGroup);
            pealsPage->Controls->Add(databaseGrp);
            pealsPage->Location = System::Drawing::Point(4, 40);
            pealsPage->Name = L"pealsPage";
            pealsPage->Padding = System::Windows::Forms::Padding(3);
            pealsPage->Size = System::Drawing::Size(292, 245);
            pealsPage->TabIndex = 1;
            pealsPage->Text = L"Performances";
            pealsPage->UseVisualStyleBackColor = true;
            // 
            // bellboard
            // 
            bellboard->Controls->Add(this->enableSetRWPageOnUpload);
            bellboard->Location = System::Drawing::Point(6, 129);
            bellboard->Name = L"bellboard";
            bellboard->Size = System::Drawing::Size(279, 53);
            bellboard->TabIndex = 5;
            bellboard->TabStop = false;
            bellboard->Text = L"Bellboard";
            // 
            // enableSetRWPageOnUpload
            // 
            this->enableSetRWPageOnUpload->AutoSize = true;
            this->enableSetRWPageOnUpload->Location = System::Drawing::Point(6, 19);
            this->enableSetRWPageOnUpload->Name = L"enableSetRWPageOnUpload";
            this->enableSetRWPageOnUpload->Size = System::Drawing::Size(187, 17);
            this->enableSetRWPageOnUpload->TabIndex = 0;
            this->enableSetRWPageOnUpload->Text = L"On bellboard upload set RW page";
            this->enableSetRWPageOnUpload->UseVisualStyleBackColor = true;
            this->enableSetRWPageOnUpload->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::ChangedDatabaseSetting);
            // 
            // towersPage
            // 
            towersPage->Controls->Add(doveTowerSettingsGrp);
            towersPage->Controls->Add(this->keepTowersWithDate);
            towersPage->Location = System::Drawing::Point(4, 40);
            towersPage->Name = L"towersPage";
            towersPage->Padding = System::Windows::Forms::Padding(3);
            towersPage->Size = System::Drawing::Size(292, 245);
            towersPage->TabIndex = 2;
            towersPage->Text = L"Towers";
            towersPage->UseVisualStyleBackColor = true;
            // 
            // keepTowersWithDate
            // 
            this->keepTowersWithDate->AutoSize = true;
            this->keepTowersWithDate->Location = System::Drawing::Point(6, 57);
            this->keepTowersWithDate->Name = L"keepTowersWithDate";
            this->keepTowersWithDate->Size = System::Drawing::Size(174, 17);
            this->keepTowersWithDate->TabIndex = 2;
            this->keepTowersWithDate->Text = L"Save towers with first rung date";
            this->toolTip->SetToolTip(this->keepTowersWithDate, L"Delete unused dialog should not delete towers which haven\'t been pealed, but have"
                L" a first rung date set.");
            this->keepTowersWithDate->UseVisualStyleBackColor = true;
            this->keepTowersWithDate->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::ChangedDatabaseSetting);
            // 
            // ringersPage
            // 
            ringersPage->Controls->Add(ringersSettingsGrp);
            ringersPage->Location = System::Drawing::Point(4, 40);
            ringersPage->Name = L"ringersPage";
            ringersPage->Padding = System::Windows::Forms::Padding(3);
            ringersPage->Size = System::Drawing::Size(292, 245);
            ringersPage->TabIndex = 3;
            ringersPage->Text = L"Ringers";
            ringersPage->UseVisualStyleBackColor = true;
            // 
            // methodsPage
            // 
            methodsPage->Controls->Add(methodLineTypeGroup);
            methodsPage->Controls->Add(methodOptionsBox);
            methodsPage->Controls->Add(methodPealUIGrp);
            methodsPage->Location = System::Drawing::Point(4, 40);
            methodsPage->Name = L"methodsPage";
            methodsPage->Padding = System::Windows::Forms::Padding(3);
            methodsPage->Size = System::Drawing::Size(292, 245);
            methodsPage->TabIndex = 4;
            methodsPage->Text = L"Methods";
            methodsPage->UseVisualStyleBackColor = true;
            // 
            // mapPage
            // 
            mapPage->Controls->Add(this->mapBoxKey);
            mapPage->Controls->Add(mapBoxLabel);
            mapPage->Location = System::Drawing::Point(4, 40);
            mapPage->Name = L"mapPage";
            mapPage->Padding = System::Windows::Forms::Padding(3);
            mapPage->Size = System::Drawing::Size(292, 245);
            mapPage->TabIndex = 5;
            mapPage->Text = L"Maps";
            mapPage->UseVisualStyleBackColor = true;
            // 
            // mapBoxKey
            // 
            this->mapBoxKey->Location = System::Drawing::Point(5, 27);
            this->mapBoxKey->Multiline = true;
            this->mapBoxKey->Name = L"mapBoxKey";
            this->mapBoxKey->Size = System::Drawing::Size(276, 55);
            this->mapBoxKey->TabIndex = 4;
            this->mapBoxKey->TextChanged += gcnew System::EventHandler(this, &SettingsUI::mapBoxKey_TextChanged);
            // 
            // mapBoxLabel
            // 
            mapBoxLabel->AutoSize = true;
            mapBoxLabel->Location = System::Drawing::Point(6, 11);
            mapBoxLabel->Name = L"mapBoxLabel";
            mapBoxLabel->Size = System::Drawing::Size(68, 13);
            mapBoxLabel->TabIndex = 3;
            mapBoxLabel->Text = L"Map box key";
            // 
            // warningsPage
            // 
            warningsPage->Controls->Add(this->ignoreMissingTime);
            warningsPage->Controls->Add(this->missingCallingNotation);
            warningsPage->Controls->Add(this->seriesMissingWarning);
            warningsPage->Controls->Add(this->missingAssociationsWarning);
            warningsPage->Controls->Add(this->warningsDescriptionLbl);
            warningsPage->Location = System::Drawing::Point(4, 40);
            warningsPage->Name = L"warningsPage";
            warningsPage->Padding = System::Windows::Forms::Padding(3);
            warningsPage->Size = System::Drawing::Size(292, 245);
            warningsPage->TabIndex = 6;
            warningsPage->Text = L"Warnings";
            warningsPage->UseVisualStyleBackColor = true;
            // 
            // ignoreMissingTime
            // 
            this->ignoreMissingTime->AutoSize = true;
            this->ignoreMissingTime->Location = System::Drawing::Point(7, 115);
            this->ignoreMissingTime->Name = L"ignoreMissingTime";
            this->ignoreMissingTime->Size = System::Drawing::Size(115, 17);
            this->ignoreMissingTime->TabIndex = 4;
            this->ignoreMissingTime->Text = L"Ignore missing time";
            this->ignoreMissingTime->UseVisualStyleBackColor = true;
            this->ignoreMissingTime->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::ChangedDatabaseSetting);
            // 
            // missingCallingNotation
            // 
            this->missingCallingNotation->AutoSize = true;
            this->missingCallingNotation->Location = System::Drawing::Point(7, 92);
            this->missingCallingNotation->Name = L"missingCallingNotation";
            this->missingCallingNotation->Size = System::Drawing::Size(164, 17);
            this->missingCallingNotation->TabIndex = 3;
            this->missingCallingNotation->Text = L"Missing calling place notation";
            this->missingCallingNotation->UseVisualStyleBackColor = true;
            this->missingCallingNotation->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::ChangedDatabaseSetting);
            // 
            // seriesMissingWarning
            // 
            this->seriesMissingWarning->AutoSize = true;
            this->seriesMissingWarning->Location = System::Drawing::Point(7, 68);
            this->seriesMissingWarning->Name = L"seriesMissingWarning";
            this->seriesMissingWarning->Size = System::Drawing::Size(113, 17);
            this->seriesMissingWarning->TabIndex = 2;
            this->seriesMissingWarning->Text = L"Add method series";
            this->seriesMissingWarning->UseVisualStyleBackColor = true;
            this->seriesMissingWarning->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::ChangedDatabaseSetting);
            // 
            // missingAssociationsWarning
            // 
            this->missingAssociationsWarning->AutoSize = true;
            this->missingAssociationsWarning->Location = System::Drawing::Point(7, 44);
            this->missingAssociationsWarning->Name = L"missingAssociationsWarning";
            this->missingAssociationsWarning->Size = System::Drawing::Size(122, 17);
            this->missingAssociationsWarning->TabIndex = 1;
            this->missingAssociationsWarning->Text = L"Missing associations";
            this->missingAssociationsWarning->UseVisualStyleBackColor = true;
            this->missingAssociationsWarning->CheckedChanged += gcnew System::EventHandler(this, &SettingsUI::ChangedDatabaseSetting);
            // 
            // warningsDescriptionLbl
            // 
            this->warningsDescriptionLbl->BorderStyle = System::Windows::Forms::BorderStyle::None;
            this->warningsDescriptionLbl->Location = System::Drawing::Point(6, 6);
            this->warningsDescriptionLbl->Multiline = true;
            this->warningsDescriptionLbl->Name = L"warningsDescriptionLbl";
            this->warningsDescriptionLbl->Size = System::Drawing::Size(281, 31);
            this->warningsDescriptionLbl->TabIndex = 0;
            this->warningsDescriptionLbl->Text = L"If any of the validation warnings annoy you, turn them off here. Let me know whic"
                L"h you\'d like to be able to disable.";
            // 
            // masterLayoutPanel
            // 
            masterLayoutPanel->ColumnCount = 3;
            masterLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            masterLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute, 81)));
            masterLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute, 81)));
            masterLayoutPanel->Controls->Add(cancelBtn, 2, 1);
            masterLayoutPanel->Controls->Add(settingsTabCtrl, 0, 0);
            masterLayoutPanel->Controls->Add(this->saveBtn, 1, 1);
            masterLayoutPanel->Controls->Add(this->resetBtn, 0, 1);
            masterLayoutPanel->Dock = System::Windows::Forms::DockStyle::Fill;
            masterLayoutPanel->Location = System::Drawing::Point(0, 0);
            masterLayoutPanel->Name = L"masterLayoutPanel";
            masterLayoutPanel->RowCount = 2;
            masterLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            masterLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 29)));
            masterLayoutPanel->Size = System::Drawing::Size(306, 324);
            masterLayoutPanel->TabIndex = 4;
            // 
            // saveBtn
            // 
            this->saveBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->saveBtn->Enabled = false;
            this->saveBtn->Location = System::Drawing::Point(147, 298);
            this->saveBtn->Name = L"saveBtn";
            this->saveBtn->Size = System::Drawing::Size(75, 23);
            this->saveBtn->TabIndex = 2;
            this->saveBtn->Text = L"&Save";
            this->saveBtn->UseVisualStyleBackColor = true;
            this->saveBtn->Click += gcnew System::EventHandler(this, &SettingsUI::saveBtn_Click);
            // 
            // resetBtn
            // 
            this->resetBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->resetBtn->Location = System::Drawing::Point(66, 298);
            this->resetBtn->Name = L"resetBtn";
            this->resetBtn->Size = System::Drawing::Size(75, 23);
            this->resetBtn->TabIndex = 1;
            this->resetBtn->Text = L"&Reset";
            this->resetBtn->UseVisualStyleBackColor = true;
            this->resetBtn->Click += gcnew System::EventHandler(this, &SettingsUI::resetBtn_Click);
            // 
            // pealsCoUkTowerLbl
            // 
            this->pealsCoUkTowerLbl->Location = System::Drawing::Point(0, 0);
            this->pealsCoUkTowerLbl->Name = L"pealsCoUkTowerLbl";
            this->pealsCoUkTowerLbl->Size = System::Drawing::Size(100, 23);
            this->pealsCoUkTowerLbl->TabIndex = 0;
            // 
            // SettingsUI
            // 
            this->AcceptButton = this->saveBtn;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            this->CancelButton = cancelBtn;
            this->ClientSize = System::Drawing::Size(306, 324);
            this->Controls->Add(masterLayoutPanel);
            this->MaximizeBox = false;
            this->MinimizeBox = false;
            this->Name = L"SettingsUI";
            this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
            this->Text = L"Database Preferences";
            this->Load += gcnew System::EventHandler(this, &SettingsUI::SettingsUI_Load);
            ringersSettingsGrp->ResumeLayout(false);
            ringersSettingsGrp->PerformLayout();
            doveTowerSettingsGrp->ResumeLayout(false);
            doveTowerSettingsGrp->PerformLayout();
            reorderGrp->ResumeLayout(false);
            reorderGrp->PerformLayout();
            methodOptionsBox->ResumeLayout(false);
            methodOptionsBox->PerformLayout();
            methodPealUIGrp->ResumeLayout(false);
            methodPealUIGrp->PerformLayout();
            printPreviewGrp->ResumeLayout(false);
            printPreviewGrp->PerformLayout();
            printFormatGroup->ResumeLayout(false);
            printFormatGroup->PerformLayout();
            databaseTypeGroup->ResumeLayout(false);
            databaseTypeGroup->PerformLayout();
            methodLineTypeGroup->ResumeLayout(false);
            methodLineTypeGroup->PerformLayout();
            databaseGrp->ResumeLayout(false);
            databaseGrp->PerformLayout();
            settingsTabCtrl->ResumeLayout(false);
            generalPage->ResumeLayout(false);
            generalPage->PerformLayout();
            pealsPage->ResumeLayout(false);
            bellboard->ResumeLayout(false);
            bellboard->PerformLayout();
            towersPage->ResumeLayout(false);
            towersPage->PerformLayout();
            ringersPage->ResumeLayout(false);
            methodsPage->ResumeLayout(false);
            mapPage->ResumeLayout(false);
            mapPage->PerformLayout();
            warningsPage->ResumeLayout(false);
            warningsPage->PerformLayout();
            masterLayoutPanel->ResumeLayout(false);
            this->ResumeLayout(false);

        }
    };
}
