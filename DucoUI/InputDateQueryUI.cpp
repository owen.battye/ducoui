#include "DatabaseManager.h"

#include "InputDateQueryUI.h"

#include "DucoUtils.h"
#include "SystemDefaultIcon.h"

using namespace DucoUI;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;

InputDateQueryUI::InputDateQueryUI(DatabaseManager^ theDatabase, Duco::ObjectId& newId, System::DateTime^ currentDate)
:   database(theDatabase), id (newId)
{
    InitializeComponent();
    if (currentDate != DateTime::MinValue)
    {
        dateTimePicker1->Value = *currentDate;
    }
}

InputDateQueryUI::~InputDateQueryUI()
{
    if (components)
    {
        delete components;
    }
}

System::Void
InputDateQueryUI::cancelBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}   

System::Void
InputDateQueryUI::okBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    id = database->FindPeal(DucoUtils::ConvertDate(dateTimePicker1->Value));
    this->DialogResult = ::DialogResult::OK;
    Close();
}

System::Void
InputDateQueryUI::InputDateQueryUI_Load(System::Object^  sender, System::EventArgs^  e) 
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
}

void
InputDateQueryUI::InitializeComponent()
{
    System::Windows::Forms::Button^  cancelBtn;
    System::Windows::Forms::Button^  okBtn;
    System::Windows::Forms::Label^  label1;
    System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
    this->dateTimePicker1 = (gcnew System::Windows::Forms::DateTimePicker());
    cancelBtn = (gcnew System::Windows::Forms::Button());
    okBtn = (gcnew System::Windows::Forms::Button());
    label1 = (gcnew System::Windows::Forms::Label());
    tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
    tableLayoutPanel1->SuspendLayout();
    this->SuspendLayout();
    // 
    // cancelBtn
    // 
    cancelBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
    cancelBtn->Location = System::Drawing::Point(157, 29);
    cancelBtn->Name = L"cancelBtn";
    cancelBtn->Size = System::Drawing::Size(75, 23);
    cancelBtn->TabIndex = 4;
    cancelBtn->Text = L"Cancel";
    cancelBtn->UseVisualStyleBackColor = true;
    cancelBtn->Click += gcnew System::EventHandler(this, &InputDateQueryUI::cancelBtn_Click);
    // 
    // okBtn
    // 
    okBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
    okBtn->Location = System::Drawing::Point(77, 29);
    okBtn->Name = L"okBtn";
    okBtn->Size = System::Drawing::Size(74, 23);
    okBtn->TabIndex = 3;
    okBtn->Text = L"OK";
    okBtn->UseVisualStyleBackColor = true;
    okBtn->Click += gcnew System::EventHandler(this, &InputDateQueryUI::okBtn_Click);
    // 
    // label1
    // 
    label1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
    label1->AutoSize = true;
    label1->Location = System::Drawing::Point(3, 6);
    label1->Margin = System::Windows::Forms::Padding(3, 6, 3, 3);
    label1->Name = L"label1";
    label1->Size = System::Drawing::Size(68, 13);
    label1->TabIndex = 5;
    label1->Text = L"Search date:";
    // 
    // dateTimePicker1
    // 
    tableLayoutPanel1->SetColumnSpan(this->dateTimePicker1, 2);
    this->dateTimePicker1->Dock = System::Windows::Forms::DockStyle::Fill;
    this->dateTimePicker1->Location = System::Drawing::Point(77, 3);
    this->dateTimePicker1->Name = L"dateTimePicker1";
    this->dateTimePicker1->Size = System::Drawing::Size(155, 20);
    this->dateTimePicker1->TabIndex = 0;
    // 
    // tableLayoutPanel1
    // 
    tableLayoutPanel1->ColumnCount = 3;
    tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
    tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
    tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
    tableLayoutPanel1->Controls->Add(label1, 0, 0);
    tableLayoutPanel1->Controls->Add(this->dateTimePicker1, 1, 0);
    tableLayoutPanel1->Controls->Add(cancelBtn, 2, 1);
    tableLayoutPanel1->Controls->Add(okBtn, 1, 1);
    tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
    tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
    tableLayoutPanel1->Name = L"tableLayoutPanel1";
    tableLayoutPanel1->RowCount = 2;
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
    tableLayoutPanel1->Size = System::Drawing::Size(235, 56);
    tableLayoutPanel1->TabIndex = 6;
    // 
    // InputDateQueryUI
    // 
    this->AcceptButton = okBtn;
    this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
    this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
    this->CancelButton = cancelBtn;
    this->ClientSize = System::Drawing::Size(235, 56);
    this->Controls->Add(tableLayoutPanel1);
    this->MaximizeBox = false;
    this->MaximumSize = System::Drawing::Size(251, 95);
    this->MinimizeBox = false;
    this->MinimumSize = System::Drawing::Size(251, 95);
    this->Name = L"InputDateQueryUI";
    this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
    this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
    this->Text = L"Find peal by date";
    this->Load += gcnew System::EventHandler(this, &InputDateQueryUI::InputDateQueryUI_Load);
    tableLayoutPanel1->ResumeLayout(false);
    tableLayoutPanel1->PerformLayout();
    this->ResumeLayout(false);

}
