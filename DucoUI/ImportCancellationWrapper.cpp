#include "ImportCancellationWrapper.h"

using namespace DucoUI;

ImportCancellationWrapper::ImportCancellationWrapper(DucoUI::ImportCancellationCallbackUI^ theUiCallback)
    : uiCallback(theUiCallback)
{

}

System::Boolean
ImportCancellationWrapper::ContinueImport()
{
    return uiCallback->ContinueImport();
}

