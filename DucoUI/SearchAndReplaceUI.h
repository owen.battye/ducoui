#pragma once
#include "ProgressWrapper.h"
#include <set>
#include <ObjectType.h>
#include <ObjectId.h>

namespace DucoUI
{
    ref class DatabaseManager;

    /// <summary>
    /// Summary for UpdateObjectsUI1
    /// </summary>
    public ref class SearchAndReplaceUI : public System::Windows::Forms::Form
    {
    public:
        SearchAndReplaceUI(DucoUI::DatabaseManager^ theDatabase, DucoUI::ProgressCallbackWrapper* theCallBack, const std::set<Duco::ObjectId>& pealIdsToUpdate, Duco::TObjectType objectIdTypesSupplied);

    protected:
        ~SearchAndReplaceUI();
        System::Void updateBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void capitalise_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void SearchAndReplaceUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void CreateOptions();

    private:
        DucoUI::DatabaseManager^ database;
        DucoUI::ProgressCallbackWrapper* callBack;
        std::set<Duco::ObjectId>* objectIdsToUpdate;
        Duco::TObjectType objectIdTypesSupplied;
        System::Windows::Forms::Button^  closeBtn;
        System::Windows::Forms::Button^  updateBtn;
        System::Windows::Forms::ComboBox^  fieldChooser;
        System::Windows::Forms::CheckBox^  capitalise;
        System::ComponentModel::Container ^components;
        System::Windows::Forms::TextBox^  replaceWith;
        System::Windows::Forms::TextBox^  searchString;
        System::Windows::Forms::CheckBox^  exactMatch;

#pragma region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        void InitializeComponent(void)
        {
            System::Windows::Forms::TableLayoutPanel^  mainLayoutPanel;
            System::Windows::Forms::GroupBox^  replaceWithFrp;
            System::Windows::Forms::GroupBox^  SearchForGrp;
            this->replaceWith = (gcnew System::Windows::Forms::TextBox());
            this->closeBtn = (gcnew System::Windows::Forms::Button());
            this->updateBtn = (gcnew System::Windows::Forms::Button());
            this->fieldChooser = (gcnew System::Windows::Forms::ComboBox());
            this->capitalise = (gcnew System::Windows::Forms::CheckBox());
            this->searchString = (gcnew System::Windows::Forms::TextBox());
            this->exactMatch = (gcnew System::Windows::Forms::CheckBox());
            mainLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
            replaceWithFrp = (gcnew System::Windows::Forms::GroupBox());
            SearchForGrp = (gcnew System::Windows::Forms::GroupBox());
            mainLayoutPanel->SuspendLayout();
            replaceWithFrp->SuspendLayout();
            SearchForGrp->SuspendLayout();
            this->SuspendLayout();
            // 
            // mainLayoutPanel
            // 
            mainLayoutPanel->ColumnCount = 2;
            mainLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            mainLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute, 82)));
            mainLayoutPanel->Controls->Add(replaceWithFrp, 0, 3);
            mainLayoutPanel->Controls->Add(this->closeBtn, 1, 4);
            mainLayoutPanel->Controls->Add(this->updateBtn, 0, 4);
            mainLayoutPanel->Controls->Add(this->fieldChooser, 0, 0);
            mainLayoutPanel->Controls->Add(this->capitalise, 0, 1);
            mainLayoutPanel->Controls->Add(SearchForGrp, 0, 2);
            mainLayoutPanel->Dock = System::Windows::Forms::DockStyle::Fill;
            mainLayoutPanel->Location = System::Drawing::Point(0, 0);
            mainLayoutPanel->Name = L"mainLayoutPanel";
            mainLayoutPanel->RowCount = 5;
            mainLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 28)));
            mainLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 24)));
            mainLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 71)));
            mainLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            mainLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 30)));
            mainLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
            mainLayoutPanel->Size = System::Drawing::Size(308, 197);
            mainLayoutPanel->TabIndex = 1;
            // 
            // replaceWithFrp
            // 
            mainLayoutPanel->SetColumnSpan(replaceWithFrp, 2);
            replaceWithFrp->Controls->Add(this->replaceWith);
            replaceWithFrp->Dock = System::Windows::Forms::DockStyle::Fill;
            replaceWithFrp->Location = System::Drawing::Point(3, 123);
            replaceWithFrp->Margin = System::Windows::Forms::Padding(3, 0, 3, 0);
            replaceWithFrp->Name = L"replaceWithFrp";
            replaceWithFrp->Size = System::Drawing::Size(302, 44);
            replaceWithFrp->TabIndex = 4;
            replaceWithFrp->TabStop = false;
            replaceWithFrp->Text = L"Replace with";
            // 
            // replaceWith
            // 
            this->replaceWith->Location = System::Drawing::Point(10, 19);
            this->replaceWith->Name = L"replaceWith";
            this->replaceWith->Size = System::Drawing::Size(284, 20);
            this->replaceWith->TabIndex = 1;
            // 
            // closeBtn
            // 
            this->closeBtn->Location = System::Drawing::Point(229, 170);
            this->closeBtn->Name = L"closeBtn";
            this->closeBtn->Size = System::Drawing::Size(75, 23);
            this->closeBtn->TabIndex = 6;
            this->closeBtn->Text = L"Close";
            this->closeBtn->UseVisualStyleBackColor = true;
            this->closeBtn->Click += gcnew System::EventHandler(this, &SearchAndReplaceUI::closeBtn_Click);
            // 
            // updateBtn
            // 
            this->updateBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->updateBtn->Location = System::Drawing::Point(148, 170);
            this->updateBtn->Name = L"updateBtn";
            this->updateBtn->Size = System::Drawing::Size(75, 23);
            this->updateBtn->TabIndex = 5;
            this->updateBtn->Text = L"Update";
            this->updateBtn->UseVisualStyleBackColor = true;
            this->updateBtn->Click += gcnew System::EventHandler(this, &SearchAndReplaceUI::updateBtn_Click);
            // 
            // fieldChooser
            // 
            this->fieldChooser->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
            this->fieldChooser->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            mainLayoutPanel->SetColumnSpan(this->fieldChooser, 2);
            this->fieldChooser->FormattingEnabled = true;
            this->fieldChooser->Location = System::Drawing::Point(3, 3);
            this->fieldChooser->Name = L"fieldChooser";
            this->fieldChooser->Size = System::Drawing::Size(294, 21);
            this->fieldChooser->TabIndex = 1;
            // 
            // capitalise
            // 
            this->capitalise->AutoSize = true;
            this->capitalise->Location = System::Drawing::Point(3, 31);
            this->capitalise->Name = L"capitalise";
            this->capitalise->Size = System::Drawing::Size(93, 17);
            this->capitalise->TabIndex = 2;
            this->capitalise->Text = L"Capitalise only";
            this->capitalise->UseVisualStyleBackColor = true;
            this->capitalise->CheckedChanged += gcnew System::EventHandler(this, &SearchAndReplaceUI::capitalise_CheckedChanged);
            // 
            // SearchForGrp
            // 
            mainLayoutPanel->SetColumnSpan(SearchForGrp, 2);
            SearchForGrp->Controls->Add(this->searchString);
            SearchForGrp->Controls->Add(this->exactMatch);
            SearchForGrp->Dock = System::Windows::Forms::DockStyle::Fill;
            SearchForGrp->Location = System::Drawing::Point(3, 52);
            SearchForGrp->Margin = System::Windows::Forms::Padding(3, 0, 3, 0);
            SearchForGrp->Name = L"SearchForGrp";
            SearchForGrp->Size = System::Drawing::Size(302, 71);
            SearchForGrp->TabIndex = 3;
            SearchForGrp->TabStop = false;
            SearchForGrp->Text = L"Search for";
            // 
            // searchString
            // 
            this->searchString->Location = System::Drawing::Point(10, 44);
            this->searchString->Name = L"searchString";
            this->searchString->Size = System::Drawing::Size(284, 20);
            this->searchString->TabIndex = 1;
            // 
            // exactMatch
            // 
            this->exactMatch->AutoSize = true;
            this->exactMatch->Location = System::Drawing::Point(10, 20);
            this->exactMatch->Name = L"exactMatch";
            this->exactMatch->Size = System::Drawing::Size(85, 17);
            this->exactMatch->TabIndex = 0;
            this->exactMatch->Text = L"Exact match";
            this->exactMatch->UseVisualStyleBackColor = true;
            // 
            // SearchAndReplaceUI
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(308, 197);
            this->Controls->Add(mainLayoutPanel);
            this->Name = L"SearchAndReplaceUI";
            this->Text = L"Search and update";
            this->Load += gcnew System::EventHandler(this, &SearchAndReplaceUI::SearchAndReplaceUI_Load);
            mainLayoutPanel->ResumeLayout(false);
            mainLayoutPanel->PerformLayout();
            replaceWithFrp->ResumeLayout(false);
            replaceWithFrp->PerformLayout();
            SearchForGrp->ResumeLayout(false);
            SearchForGrp->PerformLayout();
            this->ResumeLayout(false);
        }
#pragma endregion
    };
}
