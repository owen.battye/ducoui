#pragma once
namespace DucoUI
{
    public ref class MergeUI : public System::Windows::Forms::Form, public DucoUI::ProgressCallbackUI, public DucoUI::RingingDatabaseObserver
    {
    public:
        MergeUI(DucoUI::DatabaseManager^ theDatabase);

    // from ProgressCallbackUI
    virtual void Initialised();
    virtual void Step(int progressPercent);
    virtual void Complete();

    // from RingingDatabaseObserver
    virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

    protected:
        !MergeUI();
        ~MergeUI();
        void InitializeComponent();

        System::Void MergeUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void MergeUI_Shown(System::Object^  sender, System::EventArgs^  e);
        System::Void defaultRinger_CheckedChanged(System::Object^  sender, System::EventArgs^  e);

        System::Void fileNameEditor_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void readBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void mergeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void dataGridView1_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);

        bool OpenFileSelectionDlg();
        System::Void PopulateView();

    private:
        DucoUI::ProgressCallbackWrapper*	progressWrapper;
        DucoUI::DatabaseManager^            database;
        Duco::MergeDatabase*                externalDatabase;
        bool                                asyncCancel;
        bool                                mergeStarted;

        std::set<Duco::ObjectId>*           pealIdsToImport;

        System::Windows::Forms::TextBox^    fileNameEditor;
    private: System::ComponentModel::IContainer^ components;


        System::Windows::Forms::Button^		readBtn;
        System::Windows::Forms::Button^		mergeBtn;
        System::Windows::Forms::CheckBox^	defaultRinger;
        System::Windows::Forms::ToolStripProgressBar^  progressBar;
        System::Windows::Forms::Label^		defaultRingerLbl;

    private: System::Windows::Forms::DataGridViewCheckBoxColumn^ importColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  associationColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  dateColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  changesColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  methodColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  towerColumn;
    private: System::Windows::Forms::ToolTip^ toolTip1;
    private: System::Windows::Forms::TextBox^ towerbaseId;
    private: System::Windows::Forms::Label^ towerLbl;
    private: System::Windows::Forms::Label^ warningLbl;




        System::Windows::Forms::DataGridView^  dataGridView1;
    };
}
