#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"
#include <StatisticFilters.h>
#include "GenericTablePrintUtil.h"

#include "PeopleStatsUI.h"

#include <DucoConfiguration.h>
#include <DucoEngineUtils.h>
#include "DucoTableSorter.h"
#include "DucoUIUtils.h"
#include "DucoUtils.h"
#include "DucoWindowState.h"
#include "FiltersUI.h"
#include <PealDatabase.h>
#include <Ringer.h>
#include "ProgressWrapper.h"
#include "RingerUI.h"
#include <RingingDatabase.h>
#include <StatisticFilters.h>
#include "SystemDefaultIcon.h"
#include "SoundUtils.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Windows::Forms;

PeopleStatsUI::PeopleStatsUI(DatabaseManager^ theDatabase)
    :   database(theDatabase), conductorsGenerated(false),
        ringersGenerated(false), state(PeopleStatsUI::TState::ENon)
{
    filters = new Duco::StatisticFilters(database->Database());
    InitializeComponent();
}

PeopleStatsUI::!PeopleStatsUI()
{
    delete filters;
    database->RemoveObserver(this);
}

PeopleStatsUI::~PeopleStatsUI()
{
    this->!PeopleStatsUI();
    backgroundWorker->CancelAsync();
    if (components)
    {
        delete components;
    }
}

System::Void 
PeopleStatsUI::PeopleStatsUI_Load(System::Object^ sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    database->AddObserver(this);
    StartGenerateRingers();
}

void 
PeopleStatsUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    switch (type)
    {
    case TObjectType::EPeal:
        conductorsGenerated = false;
        ringersGenerated = false;
        composersGenerated = false;
        break;
    case TObjectType::ERinger:
        conductorsGenerated = false;
        ringersGenerated = false;
        break;
    case TObjectType::EOrderName:
    case TObjectType::EMethod:
    default:
        break;
    }
}

System::Void 
PeopleStatsUI::backgroundWorker_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^ e)
{
    PeopleStatsUIArgs^ args = (PeopleStatsUIArgs^)e->Argument;
    BackgroundWorker^ worker = dynamic_cast<BackgroundWorker^>(sender);

    switch (args->newState)
    {
    case PeopleStatsUI::TState::ERingers:
        GenerateRingerCounts(worker, e);
        break;
    case PeopleStatsUI::TState::EConductors:
        GenerateConductorCounts(worker, e);
        break;
    case PeopleStatsUI::TState::EComposers:
        GenerateComposerCounts(worker, e);
        break;
    default:
        break;
    }
    if (worker->CancellationPending)
        e->Cancel = true;
}

System::Void 
PeopleStatsUI::backgroundWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    if (backgroundWorker->CancellationPending)
        return;

    progressBar->Value = e->ProgressPercentage;
    if (e->UserState != nullptr)
    {
        switch (state)
        {
        case PeopleStatsUI::TState::ERingers:
            ringerDataCount->Rows->Add((DataGridViewRow^)e->UserState);
            break;
        case PeopleStatsUI::TState::EConductors:
            conductorsData->Rows->Add((DataGridViewRow^)e->UserState);
            break;
        case PeopleStatsUI::TState::EComposers:
            composersData->Rows->Add((DataGridViewRow^)e->UserState);
            break;
        default:
            break;
        }
    }
}

System::Void 
PeopleStatsUI::backgroundWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    if (e->Cancelled)
    {
        TState newState = nextState;
        nextState = TState::ENon;
        Restart(newState);
    }
    else
    {
        switch (state)
        {
        case PeopleStatsUI::TState::ERingers:
            {
            DucoTableSorter^ sorter = gcnew DucoTableSorter(false, EString);
            sorter->AddSortObject(ECount, false, 2);
            sorter->AddSortObject(EString, true, 1);
            ringerDataCount->Sort(sorter);

            ringerDataCount->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
            DataGridViewColumn^ newColumn = ringerDataCount->Columns[2];
            newColumn->HeaderCell->SortGlyphDirection = SortOrder::Descending;
            peopleCountLbl->Text = "";
            ringersGenerated = true;
            }
            break;
        case PeopleStatsUI::TState::EConductors:
            {
            DucoTableSorter^ sorter = gcnew DucoTableSorter(false, EString);
            sorter->AddSortObject(ECount, false, 2);
            sorter->AddSortObject(EString, true, 1);
            conductorsData->Sort(sorter);

            conductorsData->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
            DataGridViewColumn^ newColumn = conductorsData->Columns[2];
            newColumn->HeaderCell->SortGlyphDirection = SortOrder::Descending;
            peopleCountLbl->Text = Convert::ToString(conductorsData->Rows->Count) + " Conductors";
            conductorsGenerated = true;
            }
            break;
        case PeopleStatsUI::TState::EComposers:
            {
            DucoTableSorter^ sorter = gcnew DucoTableSorter(false, EString);
            sorter->AddSortObject(ECount, false, 1);
            composersData->Sort(sorter);

            composersData->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
            DataGridViewColumn^ newColumn = composersData->Columns[1];
            newColumn->HeaderCell->SortGlyphDirection = SortOrder::Descending;
            peopleCountLbl->Text = Convert::ToString(composersData->Rows->Count) + " Composers";
            composersGenerated = true;
            }
            break;
        default:
            break;
        }
        state = PeopleStatsUI::TState::ENon;
        progressBar->Value = 0;
    }
    this->Cursor = Cursors::Arrow;
}

System::Void 
PeopleStatsUI::GenerateRingerCounts(BackgroundWorker^ worker, System::ComponentModel::DoWorkEventArgs^ e)
{
    std::multimap<Duco::PealLengthInfo, Duco::ObjectId> sortedRingerIds;
    {
        std::map<Duco::ObjectId, Duco::PealLengthInfo> ringerIdsAndPealCount;
        database->Database().PealsDatabase().GetRingersPealCount(*filters, combineRingers->Checked, ringerIdsAndPealCount);
        DucoEngineUtils::SwapNumbers(ringerIdsAndPealCount, sortedRingerIds);
    }

    double noOfRows = double(sortedRingerIds.size());
    double count (0);
    int position (0);
    size_t lastCount (-1);
    bool ignorePeopleWithOnePeal (false);
    if (database->Config().DisableFeaturesForPerformance(sortedRingerIds.size()))
    {
        ignorePeopleWithOnePeal = true;
    }

    std::multimap<Duco::PealLengthInfo, Duco::ObjectId>::const_reverse_iterator it = sortedRingerIds.rbegin();
    while (it != sortedRingerIds.rend() && !worker->CancellationPending)
    {
        if (lastCount != it->first.TotalPeals())
        {
            position = int(count) + 1;
            lastCount = it->first.TotalPeals();
        }
        int percentComplete = int((++count / noOfRows)*(double)100);
        if (ignorePeopleWithOnePeal && it->first.TotalPeals() <= 1)
        {
            backgroundWorker->ReportProgress(percentComplete);
        }
        else
        {
            Duco::Ringer thisRinger;
            if (database->FindRinger(it->second, thisRinger, false))
            {
                DataGridViewRow^ newRow = gcnew DataGridViewRow();
                newRow->HeaderCell->Value = Convert::ToString(position);
                DataGridViewTextBoxCell^ idCell = gcnew DataGridViewTextBoxCell();
                idCell->Value = DucoUtils::ConvertString(it->second);
                newRow->Cells->Add(idCell);
                DataGridViewTextBoxCell^ nameCell = gcnew DataGridViewTextBoxCell();
                nameCell->Value = DucoUtils::ConvertString(thisRinger.FullName(database->Settings().LastNameFirst()));
                newRow->Cells->Add(nameCell);
                DataGridViewTextBoxCell^ countCell = gcnew DataGridViewTextBoxCell();
                countCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->first.TotalPeals(), true));
                newRow->Cells->Add(countCell);
                DataGridViewTextBoxCell^ changesCell = gcnew DataGridViewTextBoxCell();
                changesCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->first.TotalChanges(), true));
                newRow->Cells->Add(changesCell);
                DataGridViewTextBoxCell^ timeCell = gcnew DataGridViewTextBoxCell();
                timeCell->Value = DucoUtils::PrintPealTime(it->first.TotalMinutes(), false);
                newRow->Cells->Add(timeCell);
                DataGridViewTextBoxCell^ durationCell = gcnew DataGridViewTextBoxCell();
                durationCell->Value = DucoUtils::ConvertString(it->first.DurationBetweenFirstAndLastPealString());
                if (it->first.DaysBetweenFirstAndLastPeal() > (365 * 75))
                {
                    durationCell->Style->BackColor = System::Drawing::SystemColors::Highlight;
                    durationCell->Style->ForeColor = System::Drawing::SystemColors::HighlightText;
                }
                else if (it->first.DaysBetweenFirstAndLastPeal() > (365 * 50))
                {
                    durationCell->Style->BackColor = System::Drawing::SystemColors::ActiveCaption;
                    durationCell->Style->ForeColor = System::Drawing::SystemColors::ActiveCaptionText;
                }
                newRow->Cells->Add(durationCell);
                backgroundWorker->ReportProgress(percentComplete, newRow);
            }
        }
        ++it;
    }
}

System::Void 
PeopleStatsUI::GenerateConductorCounts(BackgroundWorker^ worker, System::ComponentModel::DoWorkEventArgs^ e)
{
    std::multimap<Duco::PealLengthInfo, Duco::ObjectId> sortedPealCounts;
    database->Database().PealsDatabase().GetAllConductorsByPealCount(*filters, combineRingers->Checked, sortedPealCounts);

    double noOfRows = double(sortedPealCounts.size());
    double count (0);
    int position (0);
    size_t lastCount (-1);

    std::multimap<Duco::PealLengthInfo, Duco::ObjectId>::const_reverse_iterator it = sortedPealCounts.rbegin();
    while (it != sortedPealCounts.rend() && !worker->CancellationPending)
    {
        if (lastCount != it->first.TotalPeals())
        {
            position = int(count) + 1;
            lastCount = it->first.TotalPeals();
        }

        DataGridViewRow^ newRow = gcnew DataGridViewRow();
        if (DucoEngineUtils::NumberWithSameCount(sortedPealCounts, it->first) > 1)
        {
            newRow->HeaderCell->Value = Convert::ToString(position) +  "=";
        }
        else
        {
            newRow->HeaderCell->Value = Convert::ToString(position);
        }
        DataGridViewTextBoxCell^ idCell = gcnew DataGridViewTextBoxCell();
        if (it->second != -1)
            idCell->Value = DucoUtils::ConvertString(it->second);
        newRow->Cells->Add(idCell);

        DataGridViewTextBoxCell^ nameCell = gcnew DataGridViewTextBoxCell();
        Duco::Ringer nextConductor;
        if (database->FindRinger(it->second, nextConductor, false))
        {
            nameCell->Value = DucoUtils::ConvertString(nextConductor.FullName(database->Settings().LastNameFirst()));
        }
        else if (it->second == -1)
        {
            nameCell->Value = gcnew String("Silent and non conducted");
        }
        newRow->Cells->Add(nameCell);
        DataGridViewTextBoxCell^ countCell = gcnew DataGridViewTextBoxCell();
        countCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->first.TotalPeals(), true));
        newRow->Cells->Add(countCell);
        DataGridViewTextBoxCell^ changesCell = gcnew DataGridViewTextBoxCell();
        changesCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->first.TotalChanges(), true));
        newRow->Cells->Add(changesCell);
        DataGridViewTextBoxCell^ timeCell = gcnew DataGridViewTextBoxCell();
        timeCell->Value = DucoUtils::PrintPealTime(it->first.TotalMinutes(), false);
        newRow->Cells->Add(timeCell);
        backgroundWorker->ReportProgress(int((++count / noOfRows)*(double)100), newRow);
        ++it;
    }
}

System::Void 
PeopleStatsUI::GenerateComposerCounts(BackgroundWorker^ worker, System::ComponentModel::DoWorkEventArgs^ e)
{
    PeopleStatsUIArgs^ args = (PeopleStatsUIArgs^)e->Argument;
    bool ignoreBracketed = args->ignoreBracketed;
    state = PeopleStatsUI::TState::EComposers;
    std::map<std::wstring, Duco::PealLengthInfo> sortedPealCounts;
    database->GetAllComposersByPealCount(sortedPealCounts, ignoreBracketed, *filters);

    double noOfRows = double(sortedPealCounts.size());
    double count (0);
    std::map<std::wstring, Duco::PealLengthInfo>::const_reverse_iterator it = sortedPealCounts.rbegin();
    while (it != sortedPealCounts.rend() && !worker->CancellationPending)
    {
        DataGridViewRow^ newRow = gcnew DataGridViewRow();
        DataGridViewTextBoxCell^ nameCell = gcnew DataGridViewTextBoxCell();
        nameCell->Value = DucoUtils::ConvertString(it->first);
        newRow->Cells->Add(nameCell);
        DataGridViewTextBoxCell^ countCell = gcnew DataGridViewTextBoxCell();
        countCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->second.TotalPeals(), true));
        newRow->Cells->Add(countCell);
        DataGridViewTextBoxCell^ changesCell = gcnew DataGridViewTextBoxCell();
        changesCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->second.TotalChanges(), true));
        newRow->Cells->Add(changesCell);
        DataGridViewTextBoxCell^ timeCell = gcnew DataGridViewTextBoxCell();
        timeCell->Value = DucoUtils::PrintPealTime(it->second.TotalMinutes(), false);
        newRow->Cells->Add(timeCell);
        backgroundWorker->ReportProgress(int((++count / noOfRows)*(double)100), newRow);
        ++it;
    }
}

System::Void
PeopleStatsUI::StartGenerateRingers()
{
    if (ringersGenerated)
        peopleCountLbl->Text = "";
    else
    {
        ringerDataCount->Rows->Clear();
        ringerDataCount->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::EnableResizing;
        Restart(PeopleStatsUI::TState::ERingers);
    }
}

System::Void
PeopleStatsUI::tabbedControl1_Selected(System::Object^ /*sender*/, System::Windows::Forms::TabControlEventArgs^  e)
{
    startGeneration(e->TabPageIndex);
}

System::Void
PeopleStatsUI::startGeneration(int startPage)
{
    switch (startPage)
    {
    case 0:
        StartGenerateRingers();
        break;
    case 1:
        {
            if (conductorsGenerated)
                    peopleCountLbl->Text = Convert::ToString(conductorsData->Rows->Count) + " Conductors";
            else
            {
                conductorsData->Rows->Clear();
                conductorsData->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::EnableResizing;
                Restart(PeopleStatsUI::TState::EConductors);
            }
        }
        break;
    case 2:
        {
            if (composersGenerated)
                peopleCountLbl->Text = Convert::ToString(composersData->Rows->Count) + " Composers";
            else
            {
                composersData->Rows->Clear();
                composersData->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::EnableResizing;
                Restart(PeopleStatsUI::TState::EComposers);
            }
        }
        break;
    }
}

System::Void
PeopleStatsUI::Restart(PeopleStatsUI::TState newState)
{
    if (state == newState || newState == TState::ENon)
        return;
    if (backgroundWorker->IsBusy)
    {
        backgroundWorker->CancelAsync();
        nextState = newState;
    }
    else
    {
        state = newState;
        PeopleStatsUIArgs^ args = gcnew PeopleStatsUIArgs;
        args->newState = newState;
        args->ignoreBracketed = ignoreBracketed->Checked;

        backgroundWorker->RunWorkerAsync(args);
        this->Cursor = Cursors::WaitCursor;
    }
}

System::Void
PeopleStatsUI::ignoreBracketed_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    composersGenerated = false;
    composersData->Rows->Clear();
    Restart(PeopleStatsUI::TState::EComposers);
}

System::Void
PeopleStatsUI::ringerDataCount_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
    DataGridViewColumn^ newColumn = ringerDataCount->Columns[e->ColumnIndex];
    if (newColumn->SortMode != DataGridViewColumnSortMode::Programmatic)
    {
        ringerDataCount->Columns[2]->HeaderCell->SortGlyphDirection = SortOrder::None;
        return;
    }

    SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;
    if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
        newSortOrder = SortOrder::Ascending;
    else
        newSortOrder = SortOrder::Descending;

    newColumn->HeaderCell->SortGlyphDirection = newSortOrder;

    DucoTableSorter^ sorter = gcnew DucoTableSorter(newSortOrder == SortOrder::Ascending, EString);
    sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, 2);
    sorter->AddSortObject(EString, true, 1);
    ringerDataCount->Sort(sorter);
}

System::Void
PeopleStatsUI::conductorsData_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
    DataGridViewColumn^ newColumn = conductorsData->Columns[e->ColumnIndex];
    if (newColumn->SortMode != DataGridViewColumnSortMode::Programmatic)
    {
        conductorsData->Columns[2]->HeaderCell->SortGlyphDirection = SortOrder::None;
        return;
    }

    SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;
    if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
        newSortOrder = SortOrder::Ascending;
    else
        newSortOrder = SortOrder::Descending;

    newColumn->HeaderCell->SortGlyphDirection = newSortOrder;

    DucoTableSorter^ sorter = gcnew DucoTableSorter(newSortOrder == SortOrder::Ascending, EString);
    sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, 2);
    sorter->AddSortObject(EString, true, 1);
    conductorsData->Sort(sorter);
}

System::Void
PeopleStatsUI::composersData_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
    DataGridViewColumn^ newColumn = composersData->Columns[e->ColumnIndex];

    SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;
    if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
        newSortOrder = SortOrder::Ascending;
    else
        newSortOrder = SortOrder::Descending;

    newColumn->HeaderCell->SortGlyphDirection = newSortOrder;

    DucoTableSorter^ sorter = gcnew DucoTableSorter(true, EString);
    switch (e->ColumnIndex)
    {
    case 0:
        composersData->Columns[1]->HeaderCell->SortGlyphDirection = SortOrder::None;
        sorter->AddSortObject(EString, newSortOrder == SortOrder::Ascending, 0);
        sorter->AddSortObject(ECount, false, 1);
        break;
    default:
        composersData->Columns[0]->HeaderCell->SortGlyphDirection = SortOrder::None;
        sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, 1);
        break;
    }

    composersData->Sort(sorter);
}

System::Void
PeopleStatsUI::data_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    try
    {
        System::Windows::Forms::DataGridView^ theDataView = static_cast<System::Windows::Forms::DataGridView^>(sender);
        unsigned int rowIndex = e->RowIndex;
        DataGridViewRow^ currentRow = theDataView->Rows[rowIndex];
        unsigned int ringerId = Convert::ToInt16(currentRow->Cells[0]->Value);
        RingerUI::ShowRinger(database, this->MdiParent, ringerId);
    }
    catch (Exception^)
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
PeopleStatsUI::filtersButton_Click(System::Object^  sender, System::EventArgs^  e)
{
    FiltersUI^ filtersDialog = gcnew FiltersUI(*filters, database);
    System::Windows::Forms::DialogResult returnVal = filtersDialog->ShowDialog(this);
    if (returnVal == System::Windows::Forms::DialogResult::OK)
    {
        conductorsGenerated = false;
        ringersGenerated = false;
        composersGenerated = false;
        startGeneration(tabbedCtrl->SelectedIndex);
    }
}

System::Void
PeopleStatsUI::printBtn_Click(System::Object^ sender, System::EventArgs^  e)
{
    DucoUIUtils::Print(this, sender, gcnew System::Drawing::Printing::PrintPageEventHandler(this, &PeopleStatsUI::printData), "people", database->Database().Settings().UsePrintPreview(), false);
}

System::Void
PeopleStatsUI::printData(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args)
{
    System::Drawing::Printing::PrintDocument^ printDoc = static_cast<System::Drawing::Printing::PrintDocument^>(sender);
    if (printUtil == nullptr)
    {
        String^ title = "People who have run " + database->PerformanceString(false) + "s in database " + database->Filename();
        if (tabbedCtrl->SelectedIndex == 1)
        {
            title = "People who have conducted " + database->PerformanceString(false) + "s in database " + database->Filename();
        }
        else if (tabbedCtrl->SelectedIndex == 2)
        {
            title = "People who have composed " + database->PerformanceString(false) + "s in database " + database->Filename();
        }
        printUtil = gcnew GenericTablePrintUtil(database, args, title);
        printUtil->SetSecondaryTitle(DucoUtils::ConvertString(filters->Description()));
        printUtil->SetObjects(ringerDataCount, true);
    }
    printUtil->printObject(args, printDoc->PrinterSettings);
    if (!args->HasMorePages)
    {
        printUtil = nullptr;
    }
}
