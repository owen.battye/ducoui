#pragma once
namespace DucoUI
{
    public ref class SearchDoveUI :    public System::Windows::Forms::Form,
                                       public DucoUI::DoveSearchCallbackUI
    {
    public:
        SearchDoveUI(DucoUI::DatabaseManager^ theDatabase);

        // from DoveSearchCallbackUI
        virtual void Initialised();
        virtual void Step(int progressPercent);
        virtual void Complete();
        virtual void TowerFound(const Duco::DoveObject& newTower);

    protected:
        ~SearchDoveUI();
        !SearchDoveUI();
        System::Void SearchDoveUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void SearchDoveUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e);

        System::Void valueChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void ignoreTowersRung_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void allBells_CheckedChanged(System::Object^  sender, System::EventArgs^  e);

        System::Void doveTowers_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);

        System::Void StartGeneration();
        System::Void backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void backgroundWorker1_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void backgroundWorker1_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);

        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void importBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void openBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void uncheckBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void printBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void printData(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args);

        System::Void ResetAllSortGlyphs();
        System::Void ResetAllProgrammaticGlyphs();

    private: System::Windows::Forms::Button^  importBtn;
    private: System::Windows::Forms::Button^  closeBtn;
    private: System::Windows::Forms::Button^  openBtn;
    private: System::Windows::Forms::Button^  printBtn;
    private: System::Windows::Forms::NumericUpDown^  noOfBells;
    private: System::Windows::Forms::StatusStrip^  statusStrip1;
    private: System::Windows::Forms::ToolStripProgressBar^  progress;
    private: System::ComponentModel::BackgroundWorker^  backgroundWorker1;
    private:
        System::Collections::Generic::List<System::Int16>^ allTowerNames;
        System::ComponentModel::IContainer^  components;

        bool                                        restart;
        bool                                           populating;
        DucoUI::DoveSearchWrapper*            callback;
        Duco::DoveSearch*                           doveSearch;
        DucoUI::DatabaseManager^              database;
        Duco::ObjectId*                             distanceTowerId;
    private: System::Windows::Forms::Label^         countLbl;
    private: System::Windows::Forms::DataGridView^  doveTowers;
    private: System::Windows::Forms::GroupBox^  filtersGroup;
    private: System::Windows::Forms::ComboBox^  distanceFrom;
    private: System::Windows::Forms::CheckBox^  ignoreTowersRung;
    private: System::Windows::Forms::CheckBox^  cathedralsOnly;
        System::Windows::Forms::DataGridViewCellStyle^  unringableTowerStyle;
        System::Windows::Forms::DataGridViewCellStyle^  unringableTowerStyleNumbers;
    private: System::Windows::Forms::CheckBox^              atLeast;
             System::Windows::Forms::OpenFileDialog^        openFileDialog1;
    private: System::Windows::Forms::CheckBox^              allBells;
             DucoUI::GenericTablePrintUtil^                 printUtil;
    private: System::Windows::Forms::CheckBox^  unringable;
    private: System::Windows::Forms::TextBox^  searchString;
    private: System::Windows::Forms::ToolStripStatusLabel^  dummyLbl;
    private: System::Windows::Forms::ToolStripStatusLabel^  towerWarningsLbl;
    private: System::Windows::Forms::CheckBox^  ukOrIrelandOnly;
    private: System::Windows::Forms::DataGridViewCheckBoxColumn^  importColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  towerName;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  countryColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  bellsColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  tenorColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  distanceColumn;
    private: System::Windows::Forms::TextBox^ currentDoveDatabase;
    private: System::Windows::Forms::CheckBox^ antiClockwise;

    private: System::Windows::Forms::DataGridViewTextBoxColumn^ doveIdColumn;

#pragma region Windows Form Designer generated code
        void InitializeComponent(void)
        {
            this->components = (gcnew System::ComponentModel::Container());
            System::Windows::Forms::GroupBox^ detailsGroup;
            System::Windows::Forms::GroupBox^ optionsGroup;
            System::Windows::Forms::GroupBox^ searchStringGroup;
            System::Windows::Forms::GroupBox^ bellsGrp;
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle1 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::Label^ sortByByLbl;
            System::Windows::Forms::Button^ uncheckBtn;
            System::Windows::Forms::ToolTip^ toolTip1;
            this->antiClockwise = (gcnew System::Windows::Forms::CheckBox());
            this->ukOrIrelandOnly = (gcnew System::Windows::Forms::CheckBox());
            this->cathedralsOnly = (gcnew System::Windows::Forms::CheckBox());
            this->unringable = (gcnew System::Windows::Forms::CheckBox());
            this->searchString = (gcnew System::Windows::Forms::TextBox());
            this->atLeast = (gcnew System::Windows::Forms::CheckBox());
            this->noOfBells = (gcnew System::Windows::Forms::NumericUpDown());
            this->countLbl = (gcnew System::Windows::Forms::Label());
            this->doveTowers = (gcnew System::Windows::Forms::DataGridView());
            this->importColumn = (gcnew System::Windows::Forms::DataGridViewCheckBoxColumn());
            this->towerName = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->countryColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->bellsColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->tenorColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->distanceColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->doveIdColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->filtersGroup = (gcnew System::Windows::Forms::GroupBox());
            this->allBells = (gcnew System::Windows::Forms::CheckBox());
            this->distanceFrom = (gcnew System::Windows::Forms::ComboBox());
            this->ignoreTowersRung = (gcnew System::Windows::Forms::CheckBox());
            this->printBtn = (gcnew System::Windows::Forms::Button());
            this->currentDoveDatabase = (gcnew System::Windows::Forms::TextBox());
            this->openBtn = (gcnew System::Windows::Forms::Button());
            this->importBtn = (gcnew System::Windows::Forms::Button());
            this->closeBtn = (gcnew System::Windows::Forms::Button());
            this->statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            this->progress = (gcnew System::Windows::Forms::ToolStripProgressBar());
            this->dummyLbl = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->towerWarningsLbl = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->backgroundWorker1 = (gcnew System::ComponentModel::BackgroundWorker());
            this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
            detailsGroup = (gcnew System::Windows::Forms::GroupBox());
            optionsGroup = (gcnew System::Windows::Forms::GroupBox());
            searchStringGroup = (gcnew System::Windows::Forms::GroupBox());
            bellsGrp = (gcnew System::Windows::Forms::GroupBox());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            sortByByLbl = (gcnew System::Windows::Forms::Label());
            uncheckBtn = (gcnew System::Windows::Forms::Button());
            toolTip1 = (gcnew System::Windows::Forms::ToolTip(this->components));
            detailsGroup->SuspendLayout();
            optionsGroup->SuspendLayout();
            searchStringGroup->SuspendLayout();
            bellsGrp->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->noOfBells))->BeginInit();
            tableLayoutPanel1->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->doveTowers))->BeginInit();
            this->filtersGroup->SuspendLayout();
            this->statusStrip1->SuspendLayout();
            this->SuspendLayout();
            // 
            // detailsGroup
            // 
            detailsGroup->AutoSize = true;
            detailsGroup->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            tableLayoutPanel1->SetColumnSpan(detailsGroup, 5);
            detailsGroup->Controls->Add(optionsGroup);
            detailsGroup->Controls->Add(searchStringGroup);
            detailsGroup->Controls->Add(bellsGrp);
            detailsGroup->Dock = System::Windows::Forms::DockStyle::Fill;
            detailsGroup->Location = System::Drawing::Point(3, 32);
            detailsGroup->Name = L"detailsGroup";
            detailsGroup->Size = System::Drawing::Size(1045, 77);
            detailsGroup->TabIndex = 3;
            detailsGroup->TabStop = false;
            detailsGroup->Text = L"Tower search details";
            // 
            // optionsGroup
            // 
            optionsGroup->Controls->Add(this->antiClockwise);
            optionsGroup->Controls->Add(this->ukOrIrelandOnly);
            optionsGroup->Controls->Add(this->cathedralsOnly);
            optionsGroup->Controls->Add(this->unringable);
            optionsGroup->Location = System::Drawing::Point(346, 19);
            optionsGroup->Name = L"optionsGroup";
            optionsGroup->Size = System::Drawing::Size(473, 53);
            optionsGroup->TabIndex = 13;
            optionsGroup->TabStop = false;
            optionsGroup->Text = L"Options";
            // 
            // antiClockwise
            // 
            this->antiClockwise->AutoSize = true;
            this->antiClockwise->Location = System::Drawing::Point(353, 20);
            this->antiClockwise->Name = L"antiClockwise";
            this->antiClockwise->Size = System::Drawing::Size(116, 17);
            this->antiClockwise->TabIndex = 12;
            this->antiClockwise->Text = L"Anti clockwise only";
            this->antiClockwise->UseVisualStyleBackColor = true;
            this->antiClockwise->CheckedChanged += gcnew System::EventHandler(this, &SearchDoveUI::valueChanged);
            // 
            // ukOrIrelandOnly
            // 
            this->ukOrIrelandOnly->AutoSize = true;
            this->ukOrIrelandOnly->Checked = true;
            this->ukOrIrelandOnly->CheckState = System::Windows::Forms::CheckState::Checked;
            this->ukOrIrelandOnly->Location = System::Drawing::Point(18, 20);
            this->ukOrIrelandOnly->Name = L"ukOrIrelandOnly";
            this->ukOrIrelandOnly->Size = System::Drawing::Size(110, 17);
            this->ukOrIrelandOnly->TabIndex = 11;
            this->ukOrIrelandOnly->Text = L"UK or Ireland only";
            this->ukOrIrelandOnly->UseVisualStyleBackColor = true;
            this->ukOrIrelandOnly->CheckedChanged += gcnew System::EventHandler(this, &SearchDoveUI::valueChanged);
            // 
            // cathedralsOnly
            // 
            this->cathedralsOnly->AutoSize = true;
            this->cathedralsOnly->Location = System::Drawing::Point(134, 20);
            this->cathedralsOnly->Name = L"cathedralsOnly";
            this->cathedralsOnly->Size = System::Drawing::Size(98, 17);
            this->cathedralsOnly->TabIndex = 7;
            this->cathedralsOnly->Text = L"Cathedrals only";
            this->cathedralsOnly->UseVisualStyleBackColor = true;
            this->cathedralsOnly->CheckedChanged += gcnew System::EventHandler(this, &SearchDoveUI::valueChanged);
            // 
            // unringable
            // 
            this->unringable->AutoSize = true;
            this->unringable->Checked = true;
            this->unringable->CheckState = System::Windows::Forms::CheckState::Checked;
            this->unringable->Location = System::Drawing::Point(238, 20);
            this->unringable->Name = L"unringable";
            this->unringable->Size = System::Drawing::Size(108, 17);
            this->unringable->TabIndex = 8;
            this->unringable->Text = L"Ignore unringable";
            this->unringable->UseVisualStyleBackColor = true;
            this->unringable->CheckedChanged += gcnew System::EventHandler(this, &SearchDoveUI::valueChanged);
            // 
            // searchStringGroup
            // 
            searchStringGroup->Controls->Add(this->searchString);
            searchStringGroup->Location = System::Drawing::Point(139, 19);
            searchStringGroup->Name = L"searchStringGroup";
            searchStringGroup->Size = System::Drawing::Size(201, 53);
            searchStringGroup->TabIndex = 12;
            searchStringGroup->TabStop = false;
            searchStringGroup->Text = L"Search string";
            // 
            // searchString
            // 
            this->searchString->Location = System::Drawing::Point(6, 17);
            this->searchString->Name = L"searchString";
            this->searchString->Size = System::Drawing::Size(182, 20);
            this->searchString->TabIndex = 10;
            this->searchString->TextChanged += gcnew System::EventHandler(this, &SearchDoveUI::valueChanged);
            // 
            // bellsGrp
            // 
            bellsGrp->Controls->Add(this->atLeast);
            bellsGrp->Controls->Add(this->noOfBells);
            bellsGrp->Location = System::Drawing::Point(9, 19);
            bellsGrp->Name = L"bellsGrp";
            bellsGrp->Size = System::Drawing::Size(124, 53);
            bellsGrp->TabIndex = 4;
            bellsGrp->TabStop = false;
            bellsGrp->Text = L"Bells";
            // 
            // atLeast
            // 
            this->atLeast->AutoSize = true;
            this->atLeast->Checked = true;
            this->atLeast->CheckState = System::Windows::Forms::CheckState::Checked;
            this->atLeast->Location = System::Drawing::Point(57, 20);
            this->atLeast->Name = L"atLeast";
            this->atLeast->Size = System::Drawing::Size(61, 17);
            this->atLeast->TabIndex = 6;
            this->atLeast->Text = L"At least";
            this->atLeast->UseVisualStyleBackColor = true;
            this->atLeast->CheckedChanged += gcnew System::EventHandler(this, &SearchDoveUI::valueChanged);
            // 
            // noOfBells
            // 
            this->noOfBells->Location = System::Drawing::Point(6, 19);
            this->noOfBells->Name = L"noOfBells";
            this->noOfBells->Size = System::Drawing::Size(45, 20);
            this->noOfBells->TabIndex = 5;
            this->noOfBells->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10, 0, 0, 0 });
            this->noOfBells->ValueChanged += gcnew System::EventHandler(this, &SearchDoveUI::valueChanged);
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            tableLayoutPanel1->ColumnCount = 5;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute, 80)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute, 80)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute, 80)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute, 80)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute, 20)));
            tableLayoutPanel1->Controls->Add(detailsGroup, 0, 1);
            tableLayoutPanel1->Controls->Add(this->countLbl, 0, 4);
            tableLayoutPanel1->Controls->Add(this->doveTowers, 0, 3);
            tableLayoutPanel1->Controls->Add(this->filtersGroup, 0, 2);
            tableLayoutPanel1->Controls->Add(this->printBtn, 2, 4);
            tableLayoutPanel1->Controls->Add(uncheckBtn, 1, 4);
            tableLayoutPanel1->Controls->Add(this->currentDoveDatabase, 0, 0);
            tableLayoutPanel1->Controls->Add(this->openBtn, 4, 0);
            tableLayoutPanel1->Controls->Add(this->importBtn, 3, 4);
            tableLayoutPanel1->Controls->Add(this->closeBtn, 4, 4);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 5;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 83)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 80)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 49)));
            tableLayoutPanel1->Size = System::Drawing::Size(1051, 522);
            tableLayoutPanel1->TabIndex = 0;
            // 
            // countLbl
            // 
            this->countLbl->AutoSize = true;
            this->countLbl->Location = System::Drawing::Point(3, 480);
            this->countLbl->Margin = System::Windows::Forms::Padding(3, 7, 3, 0);
            this->countLbl->Name = L"countLbl";
            this->countLbl->Size = System::Drawing::Size(48, 13);
            this->countLbl->TabIndex = 15;
            this->countLbl->Text = L"countLbl";
            // 
            // doveTowers
            // 
            this->doveTowers->AllowUserToAddRows = false;
            this->doveTowers->AllowUserToDeleteRows = false;
            this->doveTowers->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->doveTowers->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(7) {
                this->importColumn,
                    this->towerName, this->countryColumn, this->bellsColumn, this->tenorColumn, this->distanceColumn, this->doveIdColumn
            });
            tableLayoutPanel1->SetColumnSpan(this->doveTowers, 5);
            this->doveTowers->Dock = System::Windows::Forms::DockStyle::Fill;
            this->doveTowers->Location = System::Drawing::Point(3, 195);
            this->doveTowers->Name = L"doveTowers";
            this->doveTowers->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
            this->doveTowers->Size = System::Drawing::Size(1045, 275);
            this->doveTowers->TabIndex = 14;
            this->doveTowers->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &SearchDoveUI::doveTowers_ColumnHeaderMouseClick);
            // 
            // importColumn
            // 
            this->importColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            this->importColumn->FalseValue = L"FALSE";
            this->importColumn->HeaderText = L"";
            this->importColumn->Name = L"importColumn";
            this->importColumn->ToolTipText = L"Import this tower into Duco";
            this->importColumn->TrueValue = L"TRUE";
            this->importColumn->Width = 5;
            // 
            // towerName
            // 
            this->towerName->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->towerName->HeaderText = L"Tower";
            this->towerName->Name = L"towerName";
            this->towerName->ReadOnly = true;
            this->towerName->Resizable = System::Windows::Forms::DataGridViewTriState::True;
            this->towerName->Width = 62;
            // 
            // countryColumn
            // 
            this->countryColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->countryColumn->HeaderText = L"Country";
            this->countryColumn->Name = L"countryColumn";
            this->countryColumn->ReadOnly = true;
            this->countryColumn->Width = 68;
            // 
            // bellsColumn
            // 
            this->bellsColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            dataGridViewCellStyle1->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->bellsColumn->DefaultCellStyle = dataGridViewCellStyle1;
            this->bellsColumn->HeaderText = L"Bells";
            this->bellsColumn->Name = L"bellsColumn";
            this->bellsColumn->ReadOnly = true;
            this->bellsColumn->Resizable = System::Windows::Forms::DataGridViewTriState::True;
            this->bellsColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->bellsColumn->Width = 54;
            // 
            // tenorColumn
            // 
            this->tenorColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->tenorColumn->HeaderText = L"Tenor";
            this->tenorColumn->Name = L"tenorColumn";
            this->tenorColumn->ReadOnly = true;
            this->tenorColumn->Resizable = System::Windows::Forms::DataGridViewTriState::True;
            this->tenorColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->tenorColumn->Width = 60;
            // 
            // distanceColumn
            // 
            this->distanceColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->distanceColumn->HeaderText = L"Distance";
            this->distanceColumn->Name = L"distanceColumn";
            this->distanceColumn->ReadOnly = true;
            this->distanceColumn->Resizable = System::Windows::Forms::DataGridViewTriState::True;
            this->distanceColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->distanceColumn->ToolTipText = L"Distance from the selected tower";
            this->distanceColumn->Width = 74;
            // 
            // doveIdColumn
            // 
            this->doveIdColumn->HeaderText = L"Dove ID";
            this->doveIdColumn->Name = L"doveIdColumn";
            this->doveIdColumn->ReadOnly = true;
            // 
            // filtersGroup
            // 
            tableLayoutPanel1->SetColumnSpan(this->filtersGroup, 5);
            this->filtersGroup->Controls->Add(this->allBells);
            this->filtersGroup->Controls->Add(sortByByLbl);
            this->filtersGroup->Controls->Add(this->distanceFrom);
            this->filtersGroup->Controls->Add(this->ignoreTowersRung);
            this->filtersGroup->Dock = System::Windows::Forms::DockStyle::Fill;
            this->filtersGroup->Location = System::Drawing::Point(3, 115);
            this->filtersGroup->Name = L"filtersGroup";
            this->filtersGroup->Size = System::Drawing::Size(1045, 74);
            this->filtersGroup->TabIndex = 11;
            this->filtersGroup->TabStop = false;
            this->filtersGroup->Text = L"Filters";
            // 
            // allBells
            // 
            this->allBells->AutoSize = true;
            this->allBells->Location = System::Drawing::Point(164, 19);
            this->allBells->Name = L"allBells";
            this->allBells->Size = System::Drawing::Size(130, 17);
            this->allBells->TabIndex = 13;
            this->allBells->Text = L"All bells in rung towers";
            toolTip1->SetToolTip(this->allBells, L"A peal must have been rung on all bells in the tower, e.g. on 12, not the back 10"
                L".");
            this->allBells->UseVisualStyleBackColor = true;
            this->allBells->CheckedChanged += gcnew System::EventHandler(this, &SearchDoveUI::allBells_CheckedChanged);
            // 
            // sortByByLbl
            // 
            sortByByLbl->AutoSize = true;
            sortByByLbl->Location = System::Drawing::Point(9, 45);
            sortByByLbl->Name = L"sortByByLbl";
            sortByByLbl->Size = System::Drawing::Size(106, 13);
            sortByByLbl->TabIndex = 1;
            sortByByLbl->Text = L"Sort by distance from";
            // 
            // distanceFrom
            // 
            this->distanceFrom->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
            this->distanceFrom->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->distanceFrom->FormattingEnabled = true;
            this->distanceFrom->Location = System::Drawing::Point(121, 42);
            this->distanceFrom->Name = L"distanceFrom";
            this->distanceFrom->Size = System::Drawing::Size(437, 21);
            this->distanceFrom->TabIndex = 14;
            this->distanceFrom->SelectedIndexChanged += gcnew System::EventHandler(this, &SearchDoveUI::valueChanged);
            // 
            // ignoreTowersRung
            // 
            this->ignoreTowersRung->AutoSize = true;
            this->ignoreTowersRung->Checked = true;
            this->ignoreTowersRung->CheckState = System::Windows::Forms::CheckState::Checked;
            this->ignoreTowersRung->Location = System::Drawing::Point(12, 19);
            this->ignoreTowersRung->Name = L"ignoreTowersRung";
            this->ignoreTowersRung->Size = System::Drawing::Size(146, 17);
            this->ignoreTowersRung->TabIndex = 12;
            this->ignoreTowersRung->Text = L"Ignore tower already rung";
            toolTip1->SetToolTip(this->ignoreTowersRung, L"Dont show towers which already exist in this database");
            this->ignoreTowersRung->UseVisualStyleBackColor = true;
            this->ignoreTowersRung->CheckedChanged += gcnew System::EventHandler(this, &SearchDoveUI::ignoreTowersRung_CheckedChanged);
            // 
            // printBtn
            // 
            this->printBtn->Location = System::Drawing::Point(814, 476);
            this->printBtn->Name = L"printBtn";
            this->printBtn->Size = System::Drawing::Size(74, 23);
            this->printBtn->TabIndex = 16;
            this->printBtn->Text = L"Print";
            this->printBtn->UseVisualStyleBackColor = true;
            this->printBtn->Click += gcnew System::EventHandler(this, &SearchDoveUI::printBtn_Click);
            // 
            // uncheckBtn
            // 
            uncheckBtn->Location = System::Drawing::Point(734, 476);
            uncheckBtn->Name = L"uncheckBtn";
            uncheckBtn->Size = System::Drawing::Size(74, 23);
            uncheckBtn->TabIndex = 20;
            uncheckBtn->Text = L"Uncheck all";
            uncheckBtn->UseVisualStyleBackColor = true;
            uncheckBtn->Click += gcnew System::EventHandler(this, &SearchDoveUI::uncheckBtn_Click);
            // 
            // currentDoveDatabase
            // 
            tableLayoutPanel1->SetColumnSpan(this->currentDoveDatabase, 4);
            this->currentDoveDatabase->Dock = System::Windows::Forms::DockStyle::Fill;
            this->currentDoveDatabase->Location = System::Drawing::Point(3, 4);
            this->currentDoveDatabase->Margin = System::Windows::Forms::Padding(3, 4, 3, 3);
            this->currentDoveDatabase->Name = L"currentDoveDatabase";
            this->currentDoveDatabase->ReadOnly = true;
            this->currentDoveDatabase->Size = System::Drawing::Size(965, 20);
            this->currentDoveDatabase->TabIndex = 21;
            this->currentDoveDatabase->Text = L"No file selected";
            // 
            // openBtn
            // 
            this->openBtn->Location = System::Drawing::Point(974, 3);
            this->openBtn->Name = L"openBtn";
            this->openBtn->Size = System::Drawing::Size(74, 23);
            this->openBtn->TabIndex = 17;
            this->openBtn->Text = L"Choose file";
            toolTip1->SetToolTip(this->openBtn, L"Choose Dove database file, which can be downloaded from:  http://http://dove.cccb"
                L"r.org.uk/");
            this->openBtn->UseVisualStyleBackColor = true;
            this->openBtn->Click += gcnew System::EventHandler(this, &SearchDoveUI::openBtn_Click);
            // 
            // importBtn
            // 
            this->importBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->importBtn->Location = System::Drawing::Point(894, 476);
            this->importBtn->Name = L"importBtn";
            this->importBtn->Size = System::Drawing::Size(74, 23);
            this->importBtn->TabIndex = 18;
            this->importBtn->Text = L"Import";
            toolTip1->SetToolTip(this->importBtn, L"Imports the selected towers");
            this->importBtn->UseVisualStyleBackColor = true;
            this->importBtn->Click += gcnew System::EventHandler(this, &SearchDoveUI::importBtn_Click);
            // 
            // closeBtn
            // 
            this->closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            this->closeBtn->Location = System::Drawing::Point(974, 476);
            this->closeBtn->Name = L"closeBtn";
            this->closeBtn->Size = System::Drawing::Size(74, 23);
            this->closeBtn->TabIndex = 19;
            this->closeBtn->Text = L"Close";
            this->closeBtn->UseVisualStyleBackColor = true;
            this->closeBtn->Click += gcnew System::EventHandler(this, &SearchDoveUI::closeBtn_Click);
            // 
            // statusStrip1
            // 
            this->statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
                this->progress, this->dummyLbl,
                    this->towerWarningsLbl
            });
            this->statusStrip1->Location = System::Drawing::Point(0, 500);
            this->statusStrip1->Name = L"statusStrip1";
            this->statusStrip1->ShowItemToolTips = true;
            this->statusStrip1->Size = System::Drawing::Size(1051, 22);
            this->statusStrip1->TabIndex = 20;
            this->statusStrip1->Text = L"statusStrip1";
            // 
            // progress
            // 
            this->progress->Name = L"progress";
            this->progress->Size = System::Drawing::Size(100, 16);
            // 
            // dummyLbl
            // 
            this->dummyLbl->Name = L"dummyLbl";
            this->dummyLbl->Size = System::Drawing::Size(934, 17);
            this->dummyLbl->Spring = true;
            this->dummyLbl->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // towerWarningsLbl
            // 
            this->towerWarningsLbl->Name = L"towerWarningsLbl";
            this->towerWarningsLbl->Size = System::Drawing::Size(0, 17);
            this->towerWarningsLbl->ToolTipText = L"If towers don\'t have a specified Dove ID then this seach dialog maybe inable to r"
                L"esolve them during a dove search and may list them again, allowing you to duplic"
                L"ate towers.";
            // 
            // backgroundWorker1
            // 
            this->backgroundWorker1->WorkerReportsProgress = true;
            this->backgroundWorker1->WorkerSupportsCancellation = true;
            this->backgroundWorker1->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &SearchDoveUI::backgroundWorker1_DoWork);
            this->backgroundWorker1->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &SearchDoveUI::backgroundWorker1_ProgressChanged);
            this->backgroundWorker1->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &SearchDoveUI::backgroundWorker1_RunWorkerCompleted);
            // 
            // openFileDialog1
            // 
            this->openFileDialog1->DefaultExt = L"csv";
            this->openFileDialog1->FileName = L"dove.csv";
            this->openFileDialog1->Filter = L"CSV files|*.csv|All files|*.*";
            this->openFileDialog1->RestoreDirectory = true;
            // 
            // SearchDoveUI
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->CancelButton = this->closeBtn;
            this->ClientSize = System::Drawing::Size(1051, 522);
            this->Controls->Add(this->statusStrip1);
            this->Controls->Add(tableLayoutPanel1);
            this->Name = L"SearchDoveUI";
            this->Text = L"Search Dove database and import towers";
            this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &SearchDoveUI::SearchDoveUI_FormClosing);
            this->Load += gcnew System::EventHandler(this, &SearchDoveUI::SearchDoveUI_Load);
            detailsGroup->ResumeLayout(false);
            optionsGroup->ResumeLayout(false);
            optionsGroup->PerformLayout();
            searchStringGroup->ResumeLayout(false);
            searchStringGroup->PerformLayout();
            bellsGrp->ResumeLayout(false);
            bellsGrp->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->noOfBells))->EndInit();
            tableLayoutPanel1->ResumeLayout(false);
            tableLayoutPanel1->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->doveTowers))->EndInit();
            this->filtersGroup->ResumeLayout(false);
            this->filtersGroup->PerformLayout();
            this->statusStrip1->ResumeLayout(false);
            this->statusStrip1->PerformLayout();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion
};
}
