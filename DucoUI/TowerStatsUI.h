#pragma once
namespace DucoUI
{
   public ref class TowerStatsUI : public System::Windows::Forms::Form, public DucoUI::RingingDatabaseObserver
    {
    public:
        TowerStatsUI(DucoUI::DatabaseManager^ theDatabase);

        // from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

    protected:
        !TowerStatsUI();
        ~TowerStatsUI();
        void InitializeComponent();
        System::Void combineLinkedTowers_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void TowerStatsUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void filtersBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void referencesDataview_CellClick(System::Object^ sender, System::Windows::Forms::DataGridViewCellEventArgs^ e);

        System::Windows::Forms::CheckBox^  combineLinkedTowers;
        Duco::StatisticFilters* filters;

    private: System::Windows::Forms::DataGridView^ tenorsData;
    private: System::Windows::Forms::TabPage^ referencesPage;
    private: System::Windows::Forms::DataGridView^ referencesDataview;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ IdColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ TowerNameColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ pealCount;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ changesColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ timeColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ townPealCount;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ changesColum;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ townsTimeColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ countiesCount;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ countiesChangesColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ countiesTimeColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ dataGridViewTextBoxColumn1;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ dataGridViewTextBoxColumn2;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ dataGridViewTextBoxColumn3;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ webTowerColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ referencesPealCount;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ referencesBellboard;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ referencesRingingWorld;

    protected:

        enum class TState
        {
            ENon = 0,
            ETowers,
            ETowns,
            ECounties,
            ETenors,
            EReferences
        } state;
        ref class TowerStatsUIArgs
        {
        public:
            TState state;
            System::Boolean includeLinked;
        };
        System::Void StartGenerator(TowerStatsUI::TState newState);
        System::Void AutoStartGenerator();

        System::Void backgroundGenerator_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void backgroundGenerator_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void backgroundGenerator_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);

        System::Void towerData_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);
        System::Void townsData_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);
        System::Void countiesData_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);
        System::Void referencesDataview_ColumnHeaderMouseClick(System::Object^ sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^ e);

        System::Void tabControl_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);

        System::Void towerData_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);

    private:
        DucoUI::DatabaseManager^                            database;
        TState                                              nextState;
        System::ComponentModel::Container^                  components;
    private: System::Windows::Forms::TabControl^ tabControl;
        System::Windows::Forms::DataGridView^               towerData;
        System::Windows::Forms::DataGridView^               townsData;
        System::Windows::Forms::DataGridView^               countiesData;
        System::ComponentModel::BackgroundWorker^           backgroundGenerator;
        System::Windows::Forms::ToolStripProgressBar^       progressBar;
        System::Windows::Forms::ToolStripStatusLabel^       countLbl;

        bool                                                towersGenerated;
        bool                                                townsGenerated;
        bool                                                countiesGenerated;
        bool                                                tenorsGenerated;
        bool                                                referencesGenerated;
    };
}
