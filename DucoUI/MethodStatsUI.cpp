#include "RingingDatabaseObserver.h"
#include "DucoWindowState.h"
#include "DatabaseManager.h"
#include "RingerDisplayCache.h"
#include <Method.h>

#include "MethodStatsUI.h"

#include <StatisticFilters.h>
#include "DucoTableSorter.h"
#include "SystemDefaultIcon.h"
#include <RingingDatabase.h>
#include <PealDatabase.h>
#include <DucoEngineUtils.h>
#include "DucoUtils.h"
#include "MethodUI.h"
#include "SoundUtils.h"
#include <Method.h>
#include "FiltersUI.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::Windows::Forms;

MethodStatsUI::MethodStatsUI(DatabaseManager^ theDatabase)
:   database(theDatabase), methodsGenerated(false),
    typesGenerated(false), stagesGenerated(false)
{
    filters = new Duco::StatisticFilters(database->Database());
    InitializeComponent();
}

MethodStatsUI::MethodStatsUI(DucoUI::DatabaseManager^ theDatabase, const Duco::StatisticFilters& newFilters)
    : database(theDatabase), methodsGenerated(false),
    typesGenerated(false), stagesGenerated(false)
{
    filters = new Duco::StatisticFilters(database->Database());
    (*filters) = newFilters;
    InitializeComponent();
}

MethodStatsUI::!MethodStatsUI()
{
    database->RemoveObserver(this);
    delete filters;
}

MethodStatsUI::~MethodStatsUI()
{
    this->!MethodStatsUI();
    if (components)
    {
        delete components;
    }
}

System::Void
MethodStatsUI::MethodStatsUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    StartGenerator();
    database->AddObserver(this);
}

void
MethodStatsUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    switch (type)
    {
    case TObjectType::EPeal:
    case TObjectType::EMethod:
        methodsGenerated = false;
        typesGenerated = false;
        namesGenerated = false;
        // no break on purpose
    case TObjectType::EOrderName:
        stagesGenerated = false;
        StartGenerator();
        break;
    default:
        break;
    }
}

System::Void
MethodStatsUI::tabbedControl_Selected(System::Object^  sender, System::Windows::Forms::TabControlEventArgs^  e)
{
    StartGenerator();
}

System::Void
MethodStatsUI::StartGenerator()
{
    switch (tabbedControl->SelectedIndex)
    {
    case 0:
        if (!methodsGenerated)
        {
            methodData->Rows->Clear();
            methodData->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::EnableResizing;
            Restart(MethodStatsUI::TState::EMethods);
        }
        break;
    case 1:
        if (!namesGenerated)
        {
            nameData->Rows->Clear();
            nameData->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::EnableResizing;
            Restart(MethodStatsUI::TState::ENames);
        }
        break;
    case 2:
        if (!typesGenerated)
        {
            typesData->Rows->Clear();
            typesData->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::EnableResizing;
            Restart(MethodStatsUI::TState::ETypes);
        }
        break;
    case 3:
        if (!stagesGenerated)
        {
            stagesData->Rows->Clear();
            stagesData->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::EnableResizing;
            Restart(MethodStatsUI::TState::EStages);
        }
        break;
    default:
        break;
    }
}

System::Void
MethodStatsUI::backgroundGenerator_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
    MethodStatsUIArgs^ args = (MethodStatsUIArgs^)e->Argument;

    switch (args->newState)
    {
    case MethodStatsUI::TState::EMethods:
        GenerateMethodCounts(e);
        break;
    case MethodStatsUI::TState::ETypes:
        GenerateTypes(e);
        break;
    case MethodStatsUI::TState::EStages:
        GenerateStages(e);
        break;
    case MethodStatsUI::TState::ENames:
        GenerateNames(e);
        break;
    default:
        break;
    }
}

System::Void
MethodStatsUI::backgroundGenerator_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    progressBar->Value = e->ProgressPercentage;
    switch (state)
    {
    case MethodStatsUI::TState::EMethods:
        methodData->Rows->Add((DataGridViewRow^)e->UserState);
        break;
    case MethodStatsUI::TState::ETypes:
        typesData->Rows->Add((DataGridViewRow^)e->UserState);
        break;
    case MethodStatsUI::TState::EStages:
        stagesData->Rows->Add((DataGridViewRow^)e->UserState);
        break;
    case MethodStatsUI::TState::ENames:
        nameData->Rows->Add((DataGridViewRow^)e->UserState);
        break;
    default:
        break;
    }
}

System::Void
MethodStatsUI::backgroundGenerator_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    if (e->Cancelled)
    {
        TState newState = nextState;
        nextState = TState::ENon;
        Restart(newState);
    }
    else
    {
        switch (state)
        {
        case MethodStatsUI::TState::EMethods:
            {
            DucoTableSorter^ sorter = gcnew DucoTableSorter(true, EString);
            sorter->AddSortObject(ECount, false, 2);
            sorter->AddSortObject(EString, true, 1);
            methodData->Sort(sorter);

            methodData->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
            DataGridViewColumn^ newColumn = methodData->Columns[2];
            newColumn->HeaderCell->SortGlyphDirection = SortOrder::Descending;
            methodsGenerated = true;
            }
            break;
        case MethodStatsUI::TState::ENames:
            {
            DucoTableSorter^ sorter = gcnew DucoTableSorter(true, EString);
            sorter->AddSortObject(ECount, false, 0);
            nameData->Sort(sorter);
            nameData->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
            DataGridViewColumn^ newColumn = nameData->Columns[0];
            newColumn->HeaderCell->SortGlyphDirection = SortOrder::Descending;
            namesGenerated = true;
            }
            break;
        case MethodStatsUI::TState::ETypes:
            {
            DucoTableSorter^ sorter = gcnew DucoTableSorter(true, EString);
            sorter->AddSortObject(ECount, false, 0);
            typesData->Sort(sorter);

            typesData->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
            DataGridViewColumn^ newColumn = typesData->Columns[0];
            newColumn->HeaderCell->SortGlyphDirection = SortOrder::Descending;
            typesGenerated = true;
            }
            break;
        case MethodStatsUI::TState::EStages:
            {
            stagesData->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
            stagesGenerated = true;
            }
            break;
        default:
            break;
        }
        state = MethodStatsUI::TState::ENon;
        this->Cursor = Cursors::Arrow;
        progressBar->Value = 0;
    }

}

System::Void
MethodStatsUI::GenerateMethodCounts(System::ComponentModel::DoWorkEventArgs^ e)
{
    std::multimap<Duco::PealLengthInfo, Duco::ObjectId> sortedMethodIds;
    {
        std::map<Duco::ObjectId, Duco::PealLengthInfo> methodIdsAndPealCount;
        database->Database().PealsDatabase().GetMethodsPealCount(*filters, methodIdsAndPealCount);
        DucoEngineUtils::SwapNumbers(methodIdsAndPealCount, sortedMethodIds);
    }

    double noOfRows = double(sortedMethodIds.size());
    double count = 0;
    int position = 0;
    size_t lastCount = -1;

    std::multimap<Duco::PealLengthInfo, Duco::ObjectId>::const_reverse_iterator it = sortedMethodIds.rbegin();
    while (it != sortedMethodIds.rend())
    {
        if (lastCount != it->first.TotalPeals())
        {
            position = int(count) + 1;
            lastCount = it->first.TotalPeals();
        }

        Duco::Method thisMethod;
        if (database->FindMethod(it->second, thisMethod, false))
        {
            DataGridViewRow^ newRow = gcnew DataGridViewRow();
            if (DucoEngineUtils::NumberWithSameCount(sortedMethodIds, it->first) > 1)
            {
                newRow->HeaderCell->Value = Convert::ToString(position) +  "=";
            }
            else
            {
                newRow->HeaderCell->Value = Convert::ToString(position);
            }

            DataGridViewTextBoxCell^ idCell = gcnew DataGridViewTextBoxCell();
            idCell->Value = DucoUtils::ConvertString(it->second);
            newRow->Cells->Add(idCell);
            DataGridViewTextBoxCell^ nameCell = gcnew DataGridViewTextBoxCell();
            nameCell->Value = DucoUtils::ConvertString(thisMethod.FullName(database->Database()));
            newRow->Cells->Add(nameCell);
            DataGridViewTextBoxCell^ totalCell = gcnew DataGridViewTextBoxCell();
            totalCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->first.TotalPeals(), true));
            newRow->Cells->Add(totalCell);
            DataGridViewTextBoxCell^ changesCell = gcnew DataGridViewTextBoxCell();
            changesCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->first.TotalChanges(), true));
            newRow->Cells->Add(changesCell);
            DataGridViewTextBoxCell^ timeCell = gcnew DataGridViewTextBoxCell();
            timeCell->Value = DucoUtils::PrintPealTime(it->first.TotalMinutes(), false);
            newRow->Cells->Add(timeCell);
            backgroundGenerator->ReportProgress(int((++count / noOfRows)*(double)100), newRow);
        }
        ++it;
    }
}

System::Void
MethodStatsUI::GenerateStages(System::ComponentModel::DoWorkEventArgs^ e)
{
    std::map<unsigned int, Duco::PealLengthInfo> sortedPealCounts;
    std::map<unsigned int, Duco::PealLengthInfo> sortedSplicedPealCounts;
    database->Database().PealsDatabase().GetAllStagesByPealCount(*filters, sortedPealCounts, sortedSplicedPealCounts);

    double noOfRows = double(sortedPealCounts.size() + sortedSplicedPealCounts.size());
    double count = 0;
    std::map<unsigned int, Duco::PealLengthInfo>::const_reverse_iterator it = sortedPealCounts.rbegin();
    while (it != sortedPealCounts.rend())
    {
        AddStage(*it, int((++count / noOfRows)*(double)100), false);
        ++it;
    }
    std::map<unsigned int, Duco::PealLengthInfo>::const_reverse_iterator it2 = sortedSplicedPealCounts.rbegin();
    while (it2 != sortedSplicedPealCounts.rend())
    {
        AddStage(*it2, int((++count / noOfRows)*(double)100), true);
        ++it2;
    }
}

System::Void
MethodStatsUI::AddStage(const std::pair<unsigned int, Duco::PealLengthInfo>& object, int percentProgress, bool spliced)
{
    DataGridViewRow^ newRow = gcnew DataGridViewRow();
    System::String^ orderName;
    if (!database->FindOrderName(object.first, orderName))
    {
        orderName = "Unknown";
    }
    if (spliced)
    {
        System::String^ fullOrderName ("Spliced " + orderName + " and ");
        if (database->FindOrderName(object.first - 1, orderName))
        {
            fullOrderName += orderName;
        }
        else
        {
            fullOrderName += Convert::ToString(object.first);
        }
        orderName = fullOrderName;
    }
    newRow->HeaderCell->Value = orderName;
    DataGridViewTextBoxCell^ valueCell = gcnew DataGridViewTextBoxCell();
    valueCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(object.second.TotalPeals(), true));
    newRow->Cells->Add(valueCell);
    DataGridViewTextBoxCell^ changesCell = gcnew DataGridViewTextBoxCell();
    changesCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(object.second.TotalChanges(), true));
    newRow->Cells->Add(changesCell);
    DataGridViewTextBoxCell^ timeCell = gcnew DataGridViewTextBoxCell();
    timeCell->Value = DucoUtils::PrintPealTime(object.second.TotalMinutes(), false);
    newRow->Cells->Add(timeCell);
    backgroundGenerator->ReportProgress(percentProgress, newRow);
}

System::Void
MethodStatsUI::GenerateTypes(System::ComponentModel::DoWorkEventArgs^ e)
{
    std::map<std::wstring, Duco::PealLengthInfo> sortedPealCounts;
    database->Database().PealsDatabase().GetAllMethodTypesByPealCount(*filters, sortedPealCounts);

    double noOfRows = double(sortedPealCounts.size());
    double count = 0;
    std::map<std::wstring, Duco::PealLengthInfo>::const_reverse_iterator it = sortedPealCounts.rbegin();
    while (it != sortedPealCounts.rend())
    {
        DataGridViewRow^ newRow = gcnew DataGridViewRow();
        newRow->HeaderCell->Value = DucoUtils::ConvertString(it->first);
        DataGridViewTextBoxCell^ newCell = gcnew DataGridViewTextBoxCell();
        newCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->second.TotalPeals(), true));
        newRow->Cells->Add(newCell);
        DataGridViewTextBoxCell^ changesCell = gcnew DataGridViewTextBoxCell();
        changesCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->second.TotalChanges(), true));
        newRow->Cells->Add(changesCell);
        DataGridViewTextBoxCell^ timeCell = gcnew DataGridViewTextBoxCell();
        timeCell->Value = DucoUtils::PrintPealTime(it->second.TotalMinutes(), false);
        newRow->Cells->Add(timeCell);
        backgroundGenerator->ReportProgress(int((++count / noOfRows)*(double)100), newRow);
        ++it;
    }
}

System::Void
MethodStatsUI::GenerateNames(System::ComponentModel::DoWorkEventArgs^ e)
{
    std::map<std::wstring, Duco::PealLengthInfo> sortedPealCounts;
    database->Database().PealsDatabase().GetAllMethodNamesByPealCount(sortedPealCounts);

    double noOfRows = double(sortedPealCounts.size());
    double count = 0;
    std::map<std::wstring, Duco::PealLengthInfo>::const_iterator it = sortedPealCounts.begin();
    while (it != sortedPealCounts.end())
    {
        DataGridViewRow^ newRow = gcnew DataGridViewRow();
        newRow->HeaderCell->Value = DucoUtils::ConvertString(it->first);
        DataGridViewTextBoxCell^ newCell = gcnew DataGridViewTextBoxCell();
        newCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->second.TotalPeals(), true));
        newRow->Cells->Add(newCell);
        DataGridViewTextBoxCell^ changesCell = gcnew DataGridViewTextBoxCell();
        changesCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->second.TotalChanges(), true));
        newRow->Cells->Add(changesCell);
        DataGridViewTextBoxCell^ timeCell = gcnew DataGridViewTextBoxCell();
        timeCell->Value = DucoUtils::PrintPealTime(it->second.TotalMinutes(), false);
        newRow->Cells->Add(timeCell);
        backgroundGenerator->ReportProgress(int((++count / noOfRows)*(double)100), newRow);
        ++it;
    }
}

System::Void 
MethodStatsUI::Restart(MethodStatsUI::TState newState)
{
    if (state == newState)
        return;
    if (backgroundGenerator->IsBusy)
    {
        backgroundGenerator->CancelAsync();
        nextState = newState;
    }
    else
    {
        state = newState;
        MethodStatsUIArgs^ args = gcnew MethodStatsUIArgs;
        args->newState = newState;

        backgroundGenerator->RunWorkerAsync(args);
        this->Cursor = Cursors::WaitCursor;
    }
}

System::Void 
MethodStatsUI::methodData_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
    DataGridViewColumn^ newColumn = methodData->Columns[e->ColumnIndex];
    if (newColumn->SortMode != DataGridViewColumnSortMode::Programmatic)
    {
        methodData->Columns[2]->HeaderCell->SortGlyphDirection = SortOrder::None;
        return;
    }

    SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;
    if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
        newSortOrder = SortOrder::Ascending;
    else
        newSortOrder = SortOrder::Descending;

    newColumn->HeaderCell->SortGlyphDirection = newSortOrder;

    DucoTableSorter^ sorter = gcnew DucoTableSorter(newSortOrder == SortOrder::Ascending, ECount);
    sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
    sorter->AddSortObject(EString, true, 1);
    methodData->Sort(sorter);
}

System::Void 
MethodStatsUI::typesData_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
    DataGridViewColumn^ newColumn = typesData->Columns[e->ColumnIndex];

    SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;
    if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
        newSortOrder = SortOrder::Ascending;
    else
        newSortOrder = SortOrder::Descending;

    newColumn->HeaderCell->SortGlyphDirection = newSortOrder;

    DucoTableSorter^ sorter = gcnew DucoTableSorter(true, EString);
    sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
    typesData->Sort(sorter);
}

System::Void 
MethodStatsUI::stagesData_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
    DataGridViewColumn^ newColumn = stagesData->Columns[e->ColumnIndex];

    SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;
    if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
        newSortOrder = SortOrder::Ascending;
    else
        newSortOrder = SortOrder::Descending;

    newColumn->HeaderCell->SortGlyphDirection = newSortOrder;

    DucoTableSorter^ sorter = gcnew DucoTableSorter(true, EString);
    sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
    stagesData->Sort(sorter);
}


System::Void
MethodStatsUI::nameData_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
    DataGridViewColumn^ newColumn = nameData->Columns[e->ColumnIndex];

    SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;
    if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
        newSortOrder = SortOrder::Ascending;
    else
        newSortOrder = SortOrder::Descending;

    newColumn->HeaderCell->SortGlyphDirection = newSortOrder;

    DucoTableSorter^ sorter = gcnew DucoTableSorter(true, EString);
    sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
    nameData->Sort(sorter);

}

System::Void
MethodStatsUI::methodData_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    try
    {
        unsigned int rowIndex = e->RowIndex;
        DataGridViewRow^ currentRow = methodData->Rows[rowIndex];
        unsigned int towerId = Convert::ToInt16(currentRow->Cells[0]->Value);
        MethodUI::ShowMethod(database, this->MdiParent, towerId);
    }
    catch (Exception^)
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
MethodStatsUI::filtersBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    FiltersUI^ filtersDialog = gcnew FiltersUI(*filters, database);
    System::Windows::Forms::DialogResult returnVal = filtersDialog->ShowDialog(this);
    if (returnVal == System::Windows::Forms::DialogResult::OK)
    {
        methodsGenerated = false;
        namesGenerated = false;
        typesGenerated = false;
        stagesGenerated = false;
        StartGenerator();
    }
}
