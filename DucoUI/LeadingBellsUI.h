#pragma once

namespace DucoUI {
	/// <summary>
	/// Summary for LeadingBellsUI
	/// </summary>
	public ref class LeadingBellsUI : public System::Windows::Forms::Form
	{
	public:
        LeadingBellsUI(DucoUI::DatabaseManager^ theDatabase, const Duco::ObjectId& ringerId);
        ~LeadingBellsUI();

    private: System::Windows::Forms::Label^  ringerLbl;
    private: DucoUI::DatabaseManager^ database;
    private: System::Windows::Forms::DataGridView^  dataGridView;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  towerColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  bellNumberColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  pealCountColumn;
    private: Duco::ObjectId* ringerId;
    private: System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
    private: System::Void printBtn_Click(System::Object^  sender, System::EventArgs^  e);
    private: System::Void LeadingBellsUI_Load(System::Object^  sender, System::EventArgs^  e);
             System::Void printData(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args);

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent()
		{
            System::Windows::Forms::Button^  closeBtn;
            System::Windows::Forms::Button^  printBtn;
            System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
            System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(LeadingBellsUI::typeid));
            this->ringerLbl = (gcnew System::Windows::Forms::Label());
            this->dataGridView = (gcnew System::Windows::Forms::DataGridView());
            this->towerColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->bellNumberColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->pealCountColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            closeBtn = (gcnew System::Windows::Forms::Button());
            printBtn = (gcnew System::Windows::Forms::Button());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            tableLayoutPanel1->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->BeginInit();
            this->SuspendLayout();
            // 
            // closeBtn
            // 
            closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            closeBtn->Location = System::Drawing::Point(459, 459);
            closeBtn->Name = L"closeBtn";
            closeBtn->Size = System::Drawing::Size(75, 23);
            closeBtn->TabIndex = 0;
            closeBtn->Text = L"Close";
            closeBtn->UseVisualStyleBackColor = true;
            closeBtn->Click += gcnew System::EventHandler(this, &LeadingBellsUI::closeBtn_Click);
            // 
            // printBtn
            // 
            printBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            printBtn->Location = System::Drawing::Point(378, 459);
            printBtn->Name = L"printBtn";
            printBtn->Size = System::Drawing::Size(75, 23);
            printBtn->TabIndex = 3;
            printBtn->Text = L"Print";
            printBtn->UseVisualStyleBackColor = true;
            printBtn->Click += gcnew System::EventHandler(this, &LeadingBellsUI::printBtn_Click);
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 2;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->Controls->Add(closeBtn, 1, 2);
            tableLayoutPanel1->Controls->Add(this->ringerLbl, 0, 0);
            tableLayoutPanel1->Controls->Add(this->dataGridView, 0, 1);
            tableLayoutPanel1->Controls->Add(printBtn, 0, 2);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 3;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->Size = System::Drawing::Size(537, 485);
            tableLayoutPanel1->TabIndex = 0;
            // 
            // ringerLbl
            // 
            this->ringerLbl->AutoSize = true;
            this->ringerLbl->Location = System::Drawing::Point(3, 3);
            this->ringerLbl->Margin = System::Windows::Forms::Padding(3);
            this->ringerLbl->Name = L"ringerLbl";
            this->ringerLbl->Size = System::Drawing::Size(67, 13);
            this->ringerLbl->TabIndex = 1;
            this->ringerLbl->Text = L"Ringer name";
            // 
            // dataGridView
            // 
            this->dataGridView->AllowUserToAddRows = false;
            this->dataGridView->AllowUserToDeleteRows = false;
            this->dataGridView->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->dataGridView->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(3) {
                this->towerColumn,
                    this->bellNumberColumn, this->pealCountColumn
            });
            tableLayoutPanel1->SetColumnSpan(this->dataGridView, 2);
            this->dataGridView->Dock = System::Windows::Forms::DockStyle::Fill;
            this->dataGridView->Location = System::Drawing::Point(3, 22);
            this->dataGridView->Name = L"dataGridView";
            this->dataGridView->ReadOnly = true;
            this->dataGridView->Size = System::Drawing::Size(531, 431);
            this->dataGridView->TabIndex = 2;
            // 
            // towerColumn
            // 
            this->towerColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            this->towerColumn->HeaderText = L"Tower";
            this->towerColumn->Name = L"towerColumn";
            this->towerColumn->ReadOnly = true;
            this->towerColumn->Width = 62;
            // 
            // bellNumberColumn
            // 
            this->bellNumberColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            this->bellNumberColumn->HeaderText = L"Bell";
            this->bellNumberColumn->Name = L"bellNumberColumn";
            this->bellNumberColumn->ReadOnly = true;
            this->bellNumberColumn->Width = 49;
            // 
            // pealCountColumn
            // 
            this->pealCountColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            this->pealCountColumn->HeaderText = L"No. of peals";
            this->pealCountColumn->Name = L"pealCountColumn";
            this->pealCountColumn->ReadOnly = true;
            this->pealCountColumn->Width = 89;
            // 
            // LeadingBellsUI
            // 
            this->CancelButton = closeBtn;
            this->ClientSize = System::Drawing::Size(537, 485);
            this->Controls->Add(tableLayoutPanel1);
            this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
            this->Name = L"LeadingBellsUI";
            this->Text = L"Leading bells";
            this->Load += gcnew System::EventHandler(this, &LeadingBellsUI::LeadingBellsUI_Load);
            tableLayoutPanel1->ResumeLayout(false);
            tableLayoutPanel1->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->EndInit();
            this->ResumeLayout(false);

        }
#pragma endregion
    };
}
