#pragma once
namespace DucoUI
{
    public ref class CompletedSeriesUI : public System::Windows::Forms::Form, public DucoUI::RingingDatabaseObserver
    {
    public:
        CompletedSeriesUI(DucoUI::DatabaseManager^ theDatabase);

        // from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

    protected:
        ~CompletedSeriesUI();
        !CompletedSeriesUI();

        System::Void dataGridView_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);
        System::Void CompletedSeriesUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void backgroundWorker_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void backgroundWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void backgroundWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);
        System::Void StartGenerator();

        System::Void dataGridView_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);
        System::Void ResetAllSortGlyphs();
        System::Void ResetProgrammicSortGlyphs();

    private:
        DucoUI::DatabaseManager^          database;
        bool                                    restartGenerator;
        System::Windows::Forms::DataGridView^   dataGridView;
        System::ComponentModel::BackgroundWorker^           backgroundWorker;
        System::Windows::Forms::ToolStripProgressBar^       progressBar;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ IdColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ Seriesname;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ NoOfMethods;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ PealCount;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ FirstPeal;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ LastPealColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ Remaining;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ CompletedDate;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ DayeTakenColumn;
































        System::ComponentModel::Container^                  components;

#pragma region Windows Form Designer generated code
        void InitializeComponent()
        {
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
            System::Windows::Forms::StatusStrip^ statusStrip1;
            this->dataGridView = (gcnew System::Windows::Forms::DataGridView());
            this->progressBar = (gcnew System::Windows::Forms::ToolStripProgressBar());
            this->backgroundWorker = (gcnew System::ComponentModel::BackgroundWorker());
            this->IdColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->Seriesname = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->NoOfMethods = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->PealCount = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->FirstPeal = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->LastPealColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->Remaining = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->CompletedDate = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->DayeTakenColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            tableLayoutPanel1->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->BeginInit();
            statusStrip1->SuspendLayout();
            this->SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 1;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->Controls->Add(this->dataGridView, 0, 0);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 2;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 22)));
            tableLayoutPanel1->Size = System::Drawing::Size(845, 370);
            tableLayoutPanel1->TabIndex = 1;
            // 
            // dataGridView
            // 
            this->dataGridView->AllowUserToAddRows = false;
            this->dataGridView->AllowUserToDeleteRows = false;
            this->dataGridView->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::AllCells;
            this->dataGridView->AutoSizeRowsMode = System::Windows::Forms::DataGridViewAutoSizeRowsMode::AllCells;
            this->dataGridView->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->dataGridView->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(9) {
                this->IdColumn,
                    this->Seriesname, this->NoOfMethods, this->PealCount, this->FirstPeal, this->LastPealColumn, this->Remaining, this->CompletedDate,
                    this->DayeTakenColumn
            });
            this->dataGridView->Dock = System::Windows::Forms::DockStyle::Fill;
            this->dataGridView->Location = System::Drawing::Point(3, 3);
            this->dataGridView->Name = L"dataGridView";
            this->dataGridView->ReadOnly = true;
            this->dataGridView->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
            this->dataGridView->Size = System::Drawing::Size(839, 342);
            this->dataGridView->TabIndex = 1;
            this->dataGridView->CellContentDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &CompletedSeriesUI::dataGridView_CellContentDoubleClick);
            this->dataGridView->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &CompletedSeriesUI::dataGridView_ColumnHeaderMouseClick);
            // 
            // statusStrip1
            // 
            statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->progressBar });
            statusStrip1->Location = System::Drawing::Point(0, 348);
            statusStrip1->Name = L"statusStrip1";
            statusStrip1->Size = System::Drawing::Size(845, 22);
            statusStrip1->TabIndex = 2;
            statusStrip1->Text = L"statusStrip1";
            // 
            // progressBar
            // 
            this->progressBar->Name = L"progressBar";
            this->progressBar->Size = System::Drawing::Size(100, 16);
            // 
            // backgroundWorker
            // 
            this->backgroundWorker->WorkerReportsProgress = true;
            this->backgroundWorker->WorkerSupportsCancellation = true;
            this->backgroundWorker->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &CompletedSeriesUI::backgroundWorker_DoWork);
            this->backgroundWorker->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &CompletedSeriesUI::backgroundWorker_ProgressChanged);
            this->backgroundWorker->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &CompletedSeriesUI::backgroundWorker_RunWorkerCompleted);
            // 
            // IdColumn
            // 
            this->IdColumn->HeaderText = L"Id";
            this->IdColumn->Name = L"IdColumn";
            this->IdColumn->ReadOnly = true;
            this->IdColumn->Visible = false;
            this->IdColumn->Width = 41;
            // 
            // Seriesname
            // 
            this->Seriesname->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
            this->Seriesname->HeaderText = L"Series";
            this->Seriesname->Name = L"Seriesname";
            this->Seriesname->ReadOnly = true;
            // 
            // NoOfMethods
            // 
            this->NoOfMethods->DividerWidth = 2;
            this->NoOfMethods->HeaderText = L"Methods";
            this->NoOfMethods->Name = L"NoOfMethods";
            this->NoOfMethods->ReadOnly = true;
            this->NoOfMethods->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->NoOfMethods->Width = 75;
            // 
            // PealCount
            // 
            this->PealCount->HeaderText = L"No of peals";
            this->PealCount->Name = L"PealCount";
            this->PealCount->ReadOnly = true;
            this->PealCount->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->PealCount->Width = 86;
            // 
            // FirstPeal
            // 
            this->FirstPeal->HeaderText = L"First peal";
            this->FirstPeal->Name = L"FirstPeal";
            this->FirstPeal->ReadOnly = true;
            this->FirstPeal->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->FirstPeal->Width = 74;
            // 
            // LastPealColumn
            // 
            this->LastPealColumn->DividerWidth = 2;
            this->LastPealColumn->HeaderText = L"Last peal";
            this->LastPealColumn->Name = L"LastPealColumn";
            this->LastPealColumn->ReadOnly = true;
            this->LastPealColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->LastPealColumn->Width = 77;
            // 
            // Remaining
            // 
            this->Remaining->DividerWidth = 2;
            this->Remaining->HeaderText = L"Remaining";
            this->Remaining->Name = L"Remaining";
            this->Remaining->ReadOnly = true;
            this->Remaining->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->Remaining->Width = 84;
            // 
            // CompletedDate
            // 
            this->CompletedDate->HeaderText = L"Completed";
            this->CompletedDate->Name = L"CompletedDate";
            this->CompletedDate->ReadOnly = true;
            this->CompletedDate->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->CompletedDate->Width = 82;
            // 
            // DayeTakenColumn
            // 
            this->DayeTakenColumn->HeaderText = L"Days taken";
            this->DayeTakenColumn->Name = L"DayeTakenColumn";
            this->DayeTakenColumn->ReadOnly = true;
            this->DayeTakenColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->DayeTakenColumn->Width = 86;
            // 
            // CompletedSeriesUI
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(845, 370);
            this->Controls->Add(statusStrip1);
            this->Controls->Add(tableLayoutPanel1);
            this->Name = L"CompletedSeriesUI";
            this->Text = L"Peals in methods in Method Series";
            this->Load += gcnew System::EventHandler(this, &CompletedSeriesUI::CompletedSeriesUI_Load);
            tableLayoutPanel1->ResumeLayout(false);
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->EndInit();
            statusStrip1->ResumeLayout(false);
            statusStrip1->PerformLayout();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion
    };
}
