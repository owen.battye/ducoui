#include "WindowsSettings.h"
#include "DucoUtils.h"
#include "DucoUILog.h"
#include "SoundUtils.h"

using namespace DucoUI;
using namespace Microsoft::Win32;
using namespace std;
using namespace System;
using namespace System::IO;
using namespace System::Diagnostics;
using namespace System::Windows::Forms;

#define KRocketpoleSettingName "Rocketpole"
#define KDucoSettingName "Duco"
#define KDucoInstallDirSettingName "InstallDir"
#define KDucoHistorySettingName "History"
#define KDucoReindexSettingName "DisableAutoReindex"

WindowsSettings::WindowsSettings(System::Windows::Forms::IWin32Window^ newOwner)
:  owner(newOwner), autoRenumberDisabled(false)
{
    installationDir = new std::string();
    historyFile1 = gcnew String("");
    historyFile2 = gcnew String("");
    historyFile3 = gcnew String("");
    historyFile4 = gcnew String("");
    historyFile5 = gcnew String("");
    LoadSettings();
}

WindowsSettings::~WindowsSettings()
{
    this->!WindowsSettings();
}

WindowsSettings::!WindowsSettings()
{
    delete installationDir;
    installationDir = NULL;
}

System::Boolean
WindowsSettings::LoadSettings()
{
    try
    {
#ifdef _WIN64
        RegistryKey^ localMachine = RegistryKey::OpenBaseKey(RegistryHive::LocalMachine, RegistryView::Registry64);
#else
        RegistryKey^ localMachine = RegistryKey::OpenBaseKey(RegistryHive::LocalMachine, RegistryView::Registry32);
#endif
        RegistryKey^ localMachineSoftware = localMachine->OpenSubKey("SOFTWARE");
        localMachine->Close();
        RegistryKey^ rocketpoleLMSettings = localMachineSoftware->OpenSubKey(KRocketpoleSettingName);
        localMachineSoftware->Close();
        String^ installDir = "";
        if (rocketpoleLMSettings != nullptr)
        {
            RegistryKey^ ducoCurrentUserSettings = rocketpoleLMSettings->OpenSubKey(KDucoSettingName);
            rocketpoleLMSettings->Close();
            ReadSetting(ducoCurrentUserSettings, KDucoInstallDirSettingName, installDir);
            DucoUtils::ConvertString(installDir, *installationDir);
            ducoCurrentUserSettings->Close();
        }

#ifdef _WIN64
        RegistryKey^ currentUser = RegistryKey::OpenBaseKey(RegistryHive::CurrentUser, RegistryView::Registry64);
#else
        RegistryKey^ currentUser = RegistryKey::OpenBaseKey(RegistryHive::CurrentUser, RegistryView::Registry32);
#endif
        RegistryKey^ currentUserSoftware = currentUser->OpenSubKey("SOFTWARE", true);
        currentUser->Close();
        RegistryKey^ rocketpoleSettings = currentUserSoftware->OpenSubKey(KRocketpoleSettingName, true);
        if (rocketpoleSettings == nullptr)
        {
            rocketpoleSettings = currentUserSoftware->CreateSubKey(KRocketpoleSettingName);
        }
        currentUserSoftware->Close();
        RegistryKey^ ducoSettings = rocketpoleSettings->OpenSubKey(KDucoSettingName, true);
        if (ducoSettings == nullptr)
        {
            ducoSettings = rocketpoleSettings->CreateSubKey(KDucoSettingName);
        }

        RegistryKey^ history = ducoSettings->OpenSubKey(KDucoHistorySettingName);
        if (history == nullptr)
        {
            history = ducoSettings->CreateSubKey(KDucoHistorySettingName);
        }
        ReadHistorySetting(history, "1", historyFile1);
        ReadHistorySetting(history, "2", historyFile2);
        ReadHistorySetting(history, "3", historyFile3);
        ReadHistorySetting(history, "4", historyFile4);
        ReadHistorySetting(history, "5", historyFile5);
        history->Close();

        System::Object^ reindex = ducoSettings->GetValue(KDucoReindexSettingName);
        if (reindex == nullptr)
        {
            ducoSettings->SetValue(KDucoReindexSettingName, "false");
        }
        else
        {
            autoRenumberDisabled = Convert::ToBoolean(reindex);
        }

        rocketpoleSettings->Close();
        ducoSettings->Close();
    }
    catch (Exception^ ex)
    {
        MessageBox::Show("Please reinstall duco to try to repair the settings.\nYou can continue but Duco may not remember some settings, like which files were opened last time.", "Error loading default settings", MessageBoxButtons::OK, MessageBoxIcon::Warning);
        DucoUILog::PrintError("Error loading settings");
        DucoUILog::PrintError(ex->ToString());
        return false;
    }
    return true;
}

System::Void
WindowsSettings::GetHistory(System::Windows::Forms::ToolStripMenuItem^  fileHistoryMenu,
                              System::Windows::Forms::ToolStripMenuItem^  fileHistory1,
                              System::Windows::Forms::ToolStripMenuItem^  fileHistory2,
                              System::Windows::Forms::ToolStripMenuItem^  fileHistory3,
                              System::Windows::Forms::ToolStripMenuItem^  fileHistory4,
                              System::Windows::Forms::ToolStripMenuItem^  fileHistory5)
{
    bool showMenuCommand = SetHistoryMenuCommand(fileHistory1, historyFile1);
    showMenuCommand |= SetHistoryMenuCommand(fileHistory2, historyFile2);
    showMenuCommand |= SetHistoryMenuCommand(fileHistory3, historyFile3);
    showMenuCommand |= SetHistoryMenuCommand(fileHistory4, historyFile4);
    showMenuCommand |= SetHistoryMenuCommand(fileHistory5, historyFile5);
    fileHistoryMenu->Visible = showMenuCommand;
}

System::Boolean
WindowsSettings::ReadSetting(Microsoft::Win32::RegistryKey^ ducoSettings, System::String^ settingName, System::String^% settingValue)
{
    settingValue = "";
    try
    {
        Object^ settingObj = ducoSettings->GetValue(settingName);
        if (settingObj != nullptr)
        {
            String^ settingStr = settingObj->ToString();
            if (settingStr != nullptr)
            {
                settingValue = settingStr->Trim();
                return true;
            }
        }
    }
#ifdef _DEBUG
    catch (Exception^ ex)
    {
        MessageBox::Show(ex->ToString(),"Error reading setting");
#else
    catch (Exception^)
    {
#endif
        settingValue = "";
    }
    return false;
}

System::Void
WindowsSettings::ReadSetting(Microsoft::Win32::RegistryKey^ regKey, System::String^ settingName, System::String^% settingValue, System::String^ defaultValue)
{
    if (!ReadSetting(regKey, settingName, settingValue))
    {
        settingValue = defaultValue;
    }
}

System::Void
WindowsSettings::ReadHistorySetting(RegistryKey^ regKey, String^ settingName, String^% settingValue)
{
    settingValue = "";
    Object^ settingObj = regKey->GetValue(settingName);
    if (settingObj != nullptr)
    {
        String^ settingStr = settingObj->ToString();
        if (settingStr != nullptr)
        {
            settingValue = settingStr->Trim();
        }
    }
}

System::Void
WindowsSettings::AddToHistory(String^ newFileName,
                              System::Windows::Forms::ToolStripMenuItem^  fileHistoryMenu,
                              System::Windows::Forms::ToolStripMenuItem^  fileHistory1,
                              System::Windows::Forms::ToolStripMenuItem^  fileHistory2,
                              System::Windows::Forms::ToolStripMenuItem^  fileHistory3,
                              System::Windows::Forms::ToolStripMenuItem^  fileHistory4,
                              System::Windows::Forms::ToolStripMenuItem^  fileHistory5)
{
    String^ theNewFileName = Path::GetFullPath(newFileName)->Trim();
    String^ previousHistoryItem = theNewFileName;
    bool changeMade (false);
    if (MoveHistoryItem(theNewFileName, previousHistoryItem, historyFile1))
    {
        changeMade = true;
        if (MoveHistoryItem(theNewFileName, previousHistoryItem, historyFile2))
            if (MoveHistoryItem(theNewFileName, previousHistoryItem, historyFile3))
                if (MoveHistoryItem(theNewFileName, previousHistoryItem, historyFile4))
                    MoveHistoryItem(theNewFileName, previousHistoryItem, historyFile5);
    }

    if (changeMade)
    {
        bool showMenuCommand = SetHistoryMenuCommand(fileHistory1, historyFile1);
        showMenuCommand |= SetHistoryMenuCommand(fileHistory2, historyFile2);
        showMenuCommand |= SetHistoryMenuCommand(fileHistory3, historyFile3);
        showMenuCommand |= SetHistoryMenuCommand(fileHistory4, historyFile4);
        showMenuCommand |= SetHistoryMenuCommand(fileHistory5, historyFile5);
        fileHistoryMenu->Visible = showMenuCommand;
        SaveSettings();
    }
}

System::Boolean
WindowsSettings::MoveHistoryItem(String^ newFileName, String^% currentHistoryItem, String^% nextHistoryItem)
{
    bool keepProcessing = true;
    if (nextHistoryItem->Length == 0 || String::Compare(newFileName,nextHistoryItem) == 0)
        keepProcessing = false;

    String^ tempHistoryItem = nextHistoryItem;

    nextHistoryItem = currentHistoryItem;
    if (keepProcessing)
        currentHistoryItem = tempHistoryItem;

    return keepProcessing;
}

System::Boolean
WindowsSettings::RemoveHistoryItem(unsigned int index)
{
    bool changeMade = false;
    if (index <= 1)
    {
        historyFile1 = historyFile2;
        changeMade = true;
    }
    if (index <= 2)
    {
        historyFile2 = historyFile3;
        changeMade = true;
    }
    if (index <= 3)
    {
        historyFile3 = historyFile4;
        changeMade = true;
    }
    if (index <= 4)
    {
        historyFile4 = historyFile5;
        changeMade = true;
    }
    if (index <= 5)
    {
        historyFile5 = "";
        changeMade = true;
    }
    return changeMade;
}

System::Boolean
WindowsSettings::AutoRenumberDisabled()
{
    return autoRenumberDisabled;
}

System::Void
WindowsSettings::SaveSettings()
{
    RegistryKey^ ducosettings = nullptr;
    try
    {
#ifdef _WIN64
        RegistryKey^ machine = RegistryKey::OpenBaseKey(RegistryHive::CurrentUser, RegistryView::Registry64);
#else
        RegistryKey^ machine = RegistryKey::OpenBaseKey(RegistryHive::CurrentUser, RegistryView::Registry32);
#endif
        RegistryKey^ software = machine->OpenSubKey("SOFTWARE");
        machine->Close();
        RegistryKey^ rocketpoleSettings = software->OpenSubKey(KRocketpoleSettingName);
        software->Close();
        ducosettings = rocketpoleSettings->OpenSubKey(KDucoSettingName, RegistryKeyPermissionCheck::ReadWriteSubTree, System::Security::AccessControl::RegistryRights::WriteKey);
        rocketpoleSettings->Close();
    }
    catch (System::Security::SecurityException^ ex)
    {
        MessageBox::Show("Please rerun Duco as Administrator, otherwise settings, inc. recent file history, will not be saved.\n" + ex->ToString(),"Cannot save Duco settings, please rerun as Administrator.");
        return;
    }
    catch (Exception^ ex)
    {
        MessageBox::Show(ex->ToString(),"Cannot save Duco settings.");
        return;
    }

    if (ducosettings != nullptr)
    {
        RegistryKey^ history = ducosettings->OpenSubKey(KDucoHistorySettingName, true);
        if (history != nullptr)
        {
            if (historyFile1 != nullptr)
                history->SetValue("1", historyFile1);
            if (historyFile2 != nullptr)
                history->SetValue("2", historyFile2);
            if (historyFile3 != nullptr)
                history->SetValue("3", historyFile3);
            if (historyFile4 != nullptr)
                history->SetValue("4", historyFile4);
            if (historyFile5 != nullptr)
                history->SetValue("5", historyFile5);
            history->Close();
        }
        ducosettings->Close();
    }
}

System::String^
WindowsSettings::HistoryItem(unsigned int index)
{
    switch (index)
    {
    case 1:
        return historyFile1;
        break;
    case 2:
        return historyFile2;
        break;
    case 3:
        return historyFile3;
        break;
    case 4:
        return historyFile4;
        break;
    case 5:
        return historyFile5;
        break;
    default:
        break;
    }

    return "";
}

System::Boolean
WindowsSettings::SetHistoryMenuCommand(System::Windows::Forms::ToolStripMenuItem^ fileHistoryItem, String^ historyFileName)
{
    bool commandDisplayed = true;
    if (historyFileName->Length > 0)
    {
        fileHistoryItem->Text = DucoUtils::FormatFilename(historyFileName);
        fileHistoryItem->ToolTipText = historyFileName;
        fileHistoryItem->Enabled = true;
        fileHistoryItem->Visible = true;
    }
    else
    {
        commandDisplayed = false;
        fileHistoryItem->Visible = false;
        fileHistoryItem->ToolTipText = "";
    }
    return commandDisplayed;
}

const std::string&
WindowsSettings::InstallationDir()
{
    return *installationDir;
}

std::string
WindowsSettings::ConfigDir()
{
    System::String^ dataPath = Environment::GetFolderPath(Environment::SpecialFolder::CommonApplicationData, Environment::SpecialFolderOption::Create);
    dataPath += "\\";
    dataPath += KRocketpoleSettingName;
    dataPath += "\\";
    dataPath += KDucoSettingName;
    dataPath += "\\";
    Directory::CreateDirectory(dataPath);
    std::string dataPathReturn;
    DucoUtils::ConvertString(dataPath, dataPathReturn);
    return dataPathReturn;
}

System::Boolean
WindowsSettings::SetValue(RegistryKey^ rocketpoleSettings, String^ settingName, String^ settingValue)
{
    try
    {
        RegistryKey^ duco = rocketpoleSettings->OpenSubKey(KDucoSettingName, true);
        duco->SetValue(settingName, settingValue);
        duco->Close();
        return true;
    }
    catch (Exception^ ex)
    {
        SoundUtils::PlayErrorSound();
        MessageBox::Show(owner, "Please run Duco as Administrator to enable all features." + ex->ToString(), "Error reading settings");
    }
    return false;
}
