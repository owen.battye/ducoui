#include "ProgressWrapper.h"
#include <DoveDatabase.h>
#include "DatabaseManager.h"

#include "ImportDoveTowerTenorUI.h"
#include "DucoUtils.h"
#include "SoundUtils.h"
#include <DoveDatabase.h>
#include <DoveObject.h>
#include <Tower.h>
#include <TowerDatabase.h>
#include <RingingDatabase.h>
#include "WindowsSettings.h"
#include "SystemDefaultIcon.h"
#include "RingingDatabaseObserver.h"
#include "DucoWindowState.h"
#include "OpenObjectCallback.h"
#include "PictureListControl.h"
#include "PictureControl.h"
#include "TowerUI.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Windows::Forms;

ImportDoveTowerTenorUI::ImportDoveTowerTenorUI(DucoUI::DatabaseManager^ theDatabase)
    : database(theDatabase), doveDatabase(NULL)
{
    callback = new ProgressCallbackWrapper(this);
    InitializeComponent();
    towerAlreadyUsedCellStyle = (gcnew System::Windows::Forms::DataGridViewCellStyle(dataGridView->DefaultCellStyle));
    towerAlreadyUsedCellStyle->ForeColor = System::Drawing::SystemColors::GrayText;

}

ImportDoveTowerTenorUI::!ImportDoveTowerTenorUI()
{
    delete callback;
    delete doveDatabase;
}

ImportDoveTowerTenorUI::~ImportDoveTowerTenorUI()
{
    this->!ImportDoveTowerTenorUI();
    if (components)
    {
        delete components;
    }
}

System::Void
ImportDoveTowerTenorUI::closeBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (backgroundWorker1->IsBusy)
    {
        backgroundWorker1->CancelAsync();
    }
    else
    {
        Close();
    }
}

System::Void
ImportDoveTowerTenorUI::findTowersBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    dataGridView->Rows->Clear();
    StartGeneration(false);
}

System::Void
ImportDoveTowerTenorUI::chooseFileBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (backgroundWorker1->IsBusy)
    {
        SoundUtils::PlayErrorSound();
    }
    else
    {
        if (openFileDialog1->ShowDialog() == ::System::Windows::Forms::DialogResult::OK)
        {
            dataGridView->Rows->Clear();
            if (doveDatabase != NULL)
            {
                doveDatabase->Clear();
            }
            database->SetDoveDataFile(openFileDialog1->FileName);

            StartGeneration(false);
        }
    }
}

System::Void
ImportDoveTowerTenorUI::importbtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (backgroundWorker1->IsBusy || dataGridView->Rows->Count <= 0)
    {
        SoundUtils::PlayErrorSound();
    }
    else
    {
        statusLbl->Text = "";
        findTowersBtn->Enabled = false;
        chooseFileBtn->Enabled = false;
        importbtn->Enabled = false;
        closeBtn->Enabled = false;
        StartGeneration(true);
    }
}

System::Void
ImportDoveTowerTenorUI::StartGeneration(System::Boolean importTenorsLocal)
{
    statusLbl->Text = "";
    importTenors = importTenorsLocal;

    if (System::IO::File::Exists(database->DoveDataFile()))
    {
        database->SetDoveDataFile(openFileDialog1->FileName);
        DoveUpdateTenorUiArgs^ args = gcnew DoveUpdateTenorUiArgs();
        args->doveFile = database->DoveDataFile();
        args->importTenors = importTenors;

        findTowersBtn->Enabled = false;
        chooseFileBtn->Enabled = false;
        importbtn->Enabled = false;
        closeBtn->Enabled = false;
        backgroundWorker1->RunWorkerAsync(args);
    }
}

System::Void
ImportDoveTowerTenorUI::backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
    bool cancelled = false;
    BackgroundWorker^ worker = dynamic_cast<BackgroundWorker^>(sender);
    DoveUpdateTenorUiArgs^ startArgs = static_cast<DoveUpdateTenorUiArgs^>(e->Argument);
    if (startArgs->importTenors)
    {
        std::string filename;
        DucoUtils::ConvertString(startArgs->doveFile, filename);
        if (doveDatabase != NULL)
        {
            delete doveDatabase;
        }
        doveDatabase = new DoveDatabase(filename, callback, database->WindowsSettings()->InstallationDir(), true);
        doveDatabase->Import();

        database->ImportMissingTenorKeys(*doveDatabase);
    }
    else
    {

        float total = (float)database->Database().TowersDatabase().NumberOfObjects();
        float count = 0;
        ObjectId objectId;
        bool foundObject = database->Database().TowersDatabase().FirstObject(objectId);
        while (foundObject)
        {
            ++count;
            Duco::Tower theTower;
            if (database->FindTower(objectId, theTower, false))
            {
                if (theTower.DoveRefSet() && theTower.TowerKey().length() == 0)
                {
                    DataGridViewRow^ newRow = gcnew DataGridViewRow();
                    DataGridViewCellCollection^ theCells = newRow->Cells;
                    //id Column;
                    DataGridViewTextBoxCell^ ducoIdCell = gcnew DataGridViewTextBoxCell();
                    ducoIdCell->Value = DucoUtils::ConvertString(theTower.Id().Str());
                    theCells->Add(ducoIdCell);
                    //tower name Column;
                    DataGridViewTextBoxCell^ ducoTowerCell = gcnew DataGridViewTextBoxCell();
                    ducoTowerCell->Value = DucoUtils::ConvertString(theTower.FullName());
                    theCells->Add(ducoTowerCell);
                    backgroundWorker1->ReportProgress((int)(100 * (count / total)), newRow);
                }
            }
            if (worker->CancellationPending)
            {
                e->Cancel = true;
                cancelled = true;
                break;
            }
            foundObject = database->Database().TowersDatabase().NextObject(objectId, true);
        }
    }
}

System::Void
ImportDoveTowerTenorUI::backgroundWorker1_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    progressBar->Value = e->ProgressPercentage;
    closeBtn->Text = "Cancel";
    if (e->UserState != nullptr)
    {
        DataGridViewRow^ newResultRow = static_cast<DataGridViewRow^>(e->UserState);
        if (newResultRow != nullptr)
        {
            dataGridView->Rows->Add(newResultRow);
        }
        statusLbl->Text = "Found " + Convert::ToString(dataGridView->Rows) + " towers with no tenor key";
    }
}

System::Void
ImportDoveTowerTenorUI::backgroundWorker1_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    progressBar->Value = 0;
    if (e == nullptr || e->Cancelled)
    {
        dataGridView->Rows->Clear();
        if (doveDatabase != NULL)
        {
            doveDatabase->Clear();
        }
    }
    else if (importTenors)
    {
        dataGridView->Rows->Clear();
        importTenors = false;
    }
    closeBtn->Text = "Close";
    findTowersBtn->Enabled = true;
    chooseFileBtn->Enabled = true;
    importbtn->Enabled = true;
    closeBtn->Enabled = true;
    statusLbl->Text = "Found " + Convert::ToString(dataGridView->Rows->Count) + " towers";
}

void
ImportDoveTowerTenorUI::Initialised()
{
    backgroundWorker1->ReportProgress(0);
}

void
ImportDoveTowerTenorUI::Step(int progressPercent)
{
    backgroundWorker1->ReportProgress(progressPercent);
}

void
ImportDoveTowerTenorUI::Complete()
{

}

System::Void
ImportDoveTowerTenorUI::dataGridView_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    try
    {
        unsigned int rowIndex = e->RowIndex;
        DataGridViewRow^ currentRow = dataGridView->Rows[rowIndex];
        if (currentRow->Cells[0]->Value != nullptr)
        {
            unsigned int towerId = Convert::ToInt16(currentRow->Cells[0]->Value);
            if (towerId > 0)
            {
                TowerUI::ShowTower(database, this->MdiParent, towerId, -1);
            }
        }
    }
    catch (Exception^)
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
ImportDoveTowerTenorUI::ImportDoveTowerTenorUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e)
{
    if (backgroundWorker1->IsBusy)
    {
        backgroundWorker1->CancelAsync();
        e->Cancel = true;
        SoundUtils::PlayErrorSound();
    }
}

System::Void
ImportDoveTowerTenorUI::ImportDoveTowerTenorUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    doveFilenameLbl->Text = "";
    Icon = SystemDefaultIcon::DefaultSystemIcon();

    try
    {
        this->openFileDialog1->InitialDirectory = System::IO::Path::GetDirectoryName(database->DoveDataFile());
        this->openFileDialog1->FileName = System::IO::Path::GetFileName(database->DoveDataFile());
    }
    catch (System::Exception^)
    {
    }
    if (System::IO::File::Exists(database->DoveDataFile()))
    {
        openFileDialog1->InitialDirectory = System::IO::Path::GetDirectoryName(database->DoveDataFile());
        openFileDialog1->FileName = database->DoveDataFile();
        int index = database->DoveDataFile()->LastIndexOf("\\") + 1;
        doveFilenameLbl->Text = database->DoveDataFile()->Substring(index);
        doveFilenameLbl->ToolTipText = database->DoveDataFile();
        StartGeneration(false);
    }
    else
    {
        doveFilenameLbl->Text = "Choose dove data file";
    }
}
