#include "RingingDatabaseObserver.h"
#include "DatabaseManager.h"
#include "DucoWindowState.h"

#include "RingerUI.h"

#include <Ringer.h>
#include <RingerDatabase.h>
#include "SoundUtils.h"
#include "DucoUtils.h"
#include "SystemDefaultIcon.h"
#include <RingingDatabase.h>
#include "InputIndexQueryUI.h"
#include <PealLengthInfo.h>
#include "SingleObjectStatsUI.h"
#include <DucoEngineUtils.h>
#include <RingerNameChange.h>
#include "LeadingBellsUI.h"
#include <DucoConfiguration.h>
#include "ProgressWrapper.h"
#include "RingerDisplayCache.h"
#include <DatabaseSearch.h>
#include "GenericTablePrintUtil.h"
#include "RingerSearchUI.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::Windows::Forms;

RingerUI::RingerUI(DatabaseManager^ theDatabase, DucoWindowState newState)
:   database(theDatabase), windowState(newState)
{
    InitializeComponent();
    currentRingerLinkedIds = new std::vector<Duco::ObjectId>();
}

RingerUI::RingerUI(DatabaseManager^ newDatabase, const Duco::ObjectId& ringerId)
: database(newDatabase), windowState(DucoWindowState::EViewMode)
{
    InitializeComponent();
    currentRingerLinkedIds = new std::vector<Duco::ObjectId>();
    currentRinger = new Ringer();
    database->FindRinger(ringerId, *currentRinger, false);
}


RingerUI::~RingerUI()
{
    this->!RingerUI();
    if (components)
    {
        delete components;
    }
}

RingerUI::!RingerUI()
{
    delete currentRingerLinkedIds;
    delete currentRinger;
}


System::Void
RingerUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    switch (type)
    {
    case TObjectType::ERinger:
        if (windowState == DucoWindowState::EViewMode)
        {
            PopulateView();
        }
        UpdateIdLabel();
        break;

    default:
        break;
    }
}

System::Void 
RingerUI::ShowRinger(DatabaseManager^ theDatabase, System::Windows::Forms::Form^ parent, const Duco::ObjectId& ringerId)
{
    if (ringerId.ValidId())
    {
        RingerUI^ ringerUI = gcnew RingerUI(theDatabase, ringerId);
        ringerUI->MdiParent = parent;
        ringerUI->Show();
    }
}

System::Void
RingerUI::ClosingEvent(System::Object^ object, System::ComponentModel::CancelEventArgs^ e)
{
    if (dataChanged)
    {
        if (DucoUtils::ConfirmLooseChanges("ringer", this))
        {
            e->Cancel = true;
        }
    }
    if (!e->Cancel)
    {
        database->CancelEdit(TObjectType::ERinger, currentRinger->Id());
        database->RemoveObserver(this);
    }
}

System::Void
RingerUI::RingerUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    this->Icon = SystemDefaultIcon::DefaultSystemIcon();
    this->Closing += gcnew System::ComponentModel::CancelEventHandler(this, &RingerUI::ClosingEvent);

    akaChangedDate->MinDate = DateTime(1900,1,1);
    DateTime maxDate = DateTime::Today;
    TimeSpan oneMonth(31,0,0,0);
    akaChangedDate->MaxDate = maxDate + oneMonth;
    Boolean viewRinger = false;
    if (currentRinger == NULL)
    {
        currentRinger = new Ringer();
        viewRinger = true;
    }

    switch (windowState)
    {
    case DucoWindowState::EViewMode:
        {
        if (viewRinger)
        {
            Duco::ObjectId firstRingerId;
            if (database->FirstRinger(firstRingerId, false))
            {
                PopulateViewWithId(firstRingerId);
            }
        }
        else
        {
            PopulateView();
        }
        SetViewState();
        }
        break;
    case DucoWindowState::EEditMode:
    case DucoWindowState::ENewMode:
        SetNewState();
        break;
    }
    database->AddObserver(this);
}

System::Void 
RingerUI::InitializeComponent()
{
    System::Windows::Forms::GroupBox^ genderGroup;
    System::Windows::Forms::Label^ surnameLbl;
    System::Windows::Forms::Label^ forenameLbl;
    System::Windows::Forms::StatusStrip^ statusStrip1;
    System::Windows::Forms::Panel^ panel1;
    System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
    System::Windows::Forms::TabControl^ tabControl1;
    System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel2;
    System::Windows::Forms::TextBox^ linkedRingersDescription;
    System::Windows::Forms::GroupBox^ akaGroup;
    System::Windows::Forms::GroupBox^ pealbaseGroup;
    System::Windows::Forms::TabPage^ deceasedPage;
    this->male = (gcnew System::Windows::Forms::RadioButton());
    this->female = (gcnew System::Windows::Forms::RadioButton());
    this->ringerIdLabel = (gcnew System::Windows::Forms::ToolStripStatusLabel());
    this->back10Btn = (gcnew System::Windows::Forms::Button());
    this->saveBtn = (gcnew System::Windows::Forms::Button());
    this->cancelBtn = (gcnew System::Windows::Forms::Button());
    this->editBtn = (gcnew System::Windows::Forms::Button());
    this->closeBtn = (gcnew System::Windows::Forms::Button());
    this->endBtn = (gcnew System::Windows::Forms::Button());
    this->startBtn = (gcnew System::Windows::Forms::Button());
    this->forward10Btn = (gcnew System::Windows::Forms::Button());
    this->nextBtn = (gcnew System::Windows::Forms::Button());
    this->previousBtn = (gcnew System::Windows::Forms::Button());
    this->namePage = (gcnew System::Windows::Forms::TabPage());
    this->ringerNameChanges = (gcnew System::Windows::Forms::TrackBar());
    this->surnameEditor = (gcnew System::Windows::Forms::TextBox());
    this->forenameEditor = (gcnew System::Windows::Forms::TextBox());
    this->deleteAkaBtn = (gcnew System::Windows::Forms::Button());
    this->addAkaBtn = (gcnew System::Windows::Forms::Button());
    this->fromDateLbl = (gcnew System::Windows::Forms::Label());
    this->akaChangedDate = (gcnew System::Windows::Forms::DateTimePicker());
    this->linkedRingersPage = (gcnew System::Windows::Forms::TabPage());
    this->linkedRingers = (gcnew System::Windows::Forms::CheckedListBox());
    this->similarBtn = (gcnew System::Windows::Forms::Button());
    this->otherPage = (gcnew System::Windows::Forms::TabPage());
    this->nonHuman = (gcnew System::Windows::Forms::CheckBox());
    this->akaEditor = (gcnew System::Windows::Forms::TextBox());
    this->referencesPage = (gcnew System::Windows::Forms::TabPage());
    this->pealbaseRefEditor = (gcnew System::Windows::Forms::TextBox());
    this->pealbaseBtn = (gcnew System::Windows::Forms::Button());
    this->notesPage = (gcnew System::Windows::Forms::TabPage());
    this->notesEditor = (gcnew System::Windows::Forms::TextBox());
    this->deceasedDate = (gcnew System::Windows::Forms::DateTimePicker());
    this->deceased = (gcnew System::Windows::Forms::CheckBox());
    this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
    this->statsBtn = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->bellsBtn = (gcnew System::Windows::Forms::ToolStripMenuItem());
    genderGroup = (gcnew System::Windows::Forms::GroupBox());
    surnameLbl = (gcnew System::Windows::Forms::Label());
    forenameLbl = (gcnew System::Windows::Forms::Label());
    statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
    panel1 = (gcnew System::Windows::Forms::Panel());
    tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
    tabControl1 = (gcnew System::Windows::Forms::TabControl());
    tableLayoutPanel2 = (gcnew System::Windows::Forms::TableLayoutPanel());
    linkedRingersDescription = (gcnew System::Windows::Forms::TextBox());
    akaGroup = (gcnew System::Windows::Forms::GroupBox());
    pealbaseGroup = (gcnew System::Windows::Forms::GroupBox());
    deceasedPage = (gcnew System::Windows::Forms::TabPage());
    genderGroup->SuspendLayout();
    statusStrip1->SuspendLayout();
    panel1->SuspendLayout();
    tableLayoutPanel1->SuspendLayout();
    tabControl1->SuspendLayout();
    this->namePage->SuspendLayout();
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ringerNameChanges))->BeginInit();
    this->linkedRingersPage->SuspendLayout();
    tableLayoutPanel2->SuspendLayout();
    this->otherPage->SuspendLayout();
    akaGroup->SuspendLayout();
    this->referencesPage->SuspendLayout();
    pealbaseGroup->SuspendLayout();
    this->notesPage->SuspendLayout();
    deceasedPage->SuspendLayout();
    this->menuStrip1->SuspendLayout();
    this->SuspendLayout();
    // 
    // genderGroup
    // 
    genderGroup->Controls->Add(this->male);
    genderGroup->Controls->Add(this->female);
    genderGroup->Location = System::Drawing::Point(6, 5);
    genderGroup->Name = L"genderGroup";
    genderGroup->Size = System::Drawing::Size(150, 38);
    genderGroup->TabIndex = 0;
    genderGroup->TabStop = false;
    genderGroup->Text = L"Gender";
    // 
    // male
    // 
    this->male->AutoSize = true;
    this->male->Location = System::Drawing::Point(5, 13);
    this->male->Name = L"male";
    this->male->Size = System::Drawing::Size(48, 17);
    this->male->TabIndex = 0;
    this->male->Text = L"Male";
    this->male->UseVisualStyleBackColor = true;
    this->male->CheckedChanged += gcnew System::EventHandler(this, &RingerUI::Male_CheckedChanged);
    // 
    // female
    // 
    this->female->AutoSize = true;
    this->female->Location = System::Drawing::Point(77, 13);
    this->female->Name = L"female";
    this->female->Size = System::Drawing::Size(59, 17);
    this->female->TabIndex = 1;
    this->female->Text = L"Female";
    this->female->UseVisualStyleBackColor = true;
    this->female->CheckedChanged += gcnew System::EventHandler(this, &RingerUI::Female_CheckedChanged);
    // 
    // surnameLbl
    // 
    surnameLbl->AutoSize = true;
    surnameLbl->Location = System::Drawing::Point(10, 36);
    surnameLbl->Name = L"surnameLbl";
    surnameLbl->Size = System::Drawing::Size(52, 13);
    surnameLbl->TabIndex = 2;
    surnameLbl->Text = L"Surname:";
    // 
    // forenameLbl
    // 
    forenameLbl->AutoSize = true;
    forenameLbl->Location = System::Drawing::Point(5, 10);
    forenameLbl->Name = L"forenameLbl";
    forenameLbl->Size = System::Drawing::Size(57, 13);
    forenameLbl->TabIndex = 0;
    forenameLbl->Text = L"Forename:";
    // 
    // statusStrip1
    // 
    statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->ringerIdLabel });
    statusStrip1->Location = System::Drawing::Point(0, 217);
    statusStrip1->Name = L"statusStrip1";
    statusStrip1->Size = System::Drawing::Size(444, 22);
    statusStrip1->TabIndex = 14;
    statusStrip1->Text = L"statusStrip1";
    // 
    // ringerIdLabel
    // 
    this->ringerIdLabel->Name = L"ringerIdLabel";
    this->ringerIdLabel->Size = System::Drawing::Size(429, 17);
    this->ringerIdLabel->Spring = true;
    this->ringerIdLabel->Text = L"toolStripStatusLabel1";
    this->ringerIdLabel->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
    this->ringerIdLabel->Click += gcnew System::EventHandler(this, &RingerUI::RingerIdLabel_Click);
    // 
    // panel1
    // 
    panel1->Controls->Add(this->back10Btn);
    panel1->Controls->Add(this->saveBtn);
    panel1->Controls->Add(this->cancelBtn);
    panel1->Controls->Add(this->editBtn);
    panel1->Controls->Add(this->closeBtn);
    panel1->Controls->Add(this->endBtn);
    panel1->Controls->Add(this->startBtn);
    panel1->Controls->Add(this->forward10Btn);
    panel1->Controls->Add(this->nextBtn);
    panel1->Controls->Add(this->previousBtn);
    panel1->Dock = System::Windows::Forms::DockStyle::Fill;
    panel1->Location = System::Drawing::Point(3, 159);
    panel1->Name = L"panel1";
    panel1->Size = System::Drawing::Size(438, 31);
    panel1->TabIndex = 16;
    // 
    // back10Btn
    // 
    this->back10Btn->Location = System::Drawing::Point(34, 3);
    this->back10Btn->Name = L"back10Btn";
    this->back10Btn->Size = System::Drawing::Size(31, 23);
    this->back10Btn->TabIndex = 5;
    this->back10Btn->Text = L"<<";
    this->back10Btn->UseVisualStyleBackColor = true;
    this->back10Btn->Click += gcnew System::EventHandler(this, &RingerUI::Back10Btn_Click);
    // 
    // saveBtn
    // 
    this->saveBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
    this->saveBtn->Location = System::Drawing::Point(285, 3);
    this->saveBtn->Name = L"saveBtn";
    this->saveBtn->Size = System::Drawing::Size(75, 23);
    this->saveBtn->TabIndex = 12;
    this->saveBtn->Text = L"&Save";
    this->saveBtn->UseVisualStyleBackColor = true;
    this->saveBtn->Visible = false;
    this->saveBtn->Click += gcnew System::EventHandler(this, &RingerUI::SaveBtn_Click);
    // 
    // cancelBtn
    // 
    this->cancelBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
    this->cancelBtn->Location = System::Drawing::Point(360, 3);
    this->cancelBtn->Name = L"cancelBtn";
    this->cancelBtn->Size = System::Drawing::Size(75, 23);
    this->cancelBtn->TabIndex = 13;
    this->cancelBtn->Text = L"&Cancel";
    this->cancelBtn->UseVisualStyleBackColor = true;
    this->cancelBtn->Visible = false;
    this->cancelBtn->Click += gcnew System::EventHandler(this, &RingerUI::CancelBtn_Click);
    // 
    // editBtn
    // 
    this->editBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
    this->editBtn->Location = System::Drawing::Point(285, 3);
    this->editBtn->Name = L"editBtn";
    this->editBtn->Size = System::Drawing::Size(75, 23);
    this->editBtn->TabIndex = 10;
    this->editBtn->Text = L"&Edit";
    this->editBtn->UseVisualStyleBackColor = true;
    this->editBtn->Click += gcnew System::EventHandler(this, &RingerUI::EditBtn_Click);
    // 
    // closeBtn
    // 
    this->closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
    this->closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
    this->closeBtn->Location = System::Drawing::Point(360, 3);
    this->closeBtn->Name = L"closeBtn";
    this->closeBtn->Size = System::Drawing::Size(75, 23);
    this->closeBtn->TabIndex = 11;
    this->closeBtn->Text = L"Cl&ose";
    this->closeBtn->UseVisualStyleBackColor = true;
    this->closeBtn->Click += gcnew System::EventHandler(this, &RingerUI::CloseBtn_Click);
    // 
    // endBtn
    // 
    this->endBtn->AutoSize = true;
    this->endBtn->Location = System::Drawing::Point(170, 3);
    this->endBtn->Name = L"endBtn";
    this->endBtn->Size = System::Drawing::Size(31, 23);
    this->endBtn->TabIndex = 9;
    this->endBtn->Text = L">|";
    this->endBtn->UseVisualStyleBackColor = true;
    this->endBtn->Click += gcnew System::EventHandler(this, &RingerUI::EndBtn_Click);
    // 
    // startBtn
    // 
    this->startBtn->Location = System::Drawing::Point(3, 3);
    this->startBtn->Name = L"startBtn";
    this->startBtn->Size = System::Drawing::Size(31, 23);
    this->startBtn->TabIndex = 4;
    this->startBtn->Text = L"|<";
    this->startBtn->UseVisualStyleBackColor = true;
    this->startBtn->Click += gcnew System::EventHandler(this, &RingerUI::StartBtn_Click);
    // 
    // forward10Btn
    // 
    this->forward10Btn->Location = System::Drawing::Point(139, 3);
    this->forward10Btn->Name = L"forward10Btn";
    this->forward10Btn->Size = System::Drawing::Size(31, 23);
    this->forward10Btn->TabIndex = 8;
    this->forward10Btn->Text = L">>";
    this->forward10Btn->UseVisualStyleBackColor = true;
    this->forward10Btn->Click += gcnew System::EventHandler(this, &RingerUI::Forward10Btn_Click);
    // 
    // nextBtn
    // 
    this->nextBtn->Location = System::Drawing::Point(108, 3);
    this->nextBtn->Name = L"nextBtn";
    this->nextBtn->Size = System::Drawing::Size(31, 23);
    this->nextBtn->TabIndex = 7;
    this->nextBtn->Text = L">";
    this->nextBtn->UseVisualStyleBackColor = true;
    this->nextBtn->Click += gcnew System::EventHandler(this, &RingerUI::NextBtn_Click);
    // 
    // previousBtn
    // 
    this->previousBtn->Location = System::Drawing::Point(65, 3);
    this->previousBtn->Name = L"previousBtn";
    this->previousBtn->Size = System::Drawing::Size(31, 23);
    this->previousBtn->TabIndex = 6;
    this->previousBtn->Text = L"<";
    this->previousBtn->UseVisualStyleBackColor = true;
    this->previousBtn->Click += gcnew System::EventHandler(this, &RingerUI::PreviousBtn_Click);
    // 
    // tableLayoutPanel1
    // 
    tableLayoutPanel1->ColumnCount = 1;
    tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
    tableLayoutPanel1->Controls->Add(panel1, 0, 1);
    tableLayoutPanel1->Controls->Add(tabControl1, 0, 0);
    tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
    tableLayoutPanel1->Location = System::Drawing::Point(0, 24);
    tableLayoutPanel1->Name = L"tableLayoutPanel1";
    tableLayoutPanel1->RowCount = 2;
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
    tableLayoutPanel1->Size = System::Drawing::Size(444, 193);
    tableLayoutPanel1->TabIndex = 17;
    // 
    // tabControl1
    // 
    tabControl1->Controls->Add(this->namePage);
    tabControl1->Controls->Add(this->linkedRingersPage);
    tabControl1->Controls->Add(this->otherPage);
    tabControl1->Controls->Add(this->referencesPage);
    tabControl1->Controls->Add(this->notesPage);
    tabControl1->Controls->Add(deceasedPage);
    tabControl1->Dock = System::Windows::Forms::DockStyle::Fill;
    tabControl1->Location = System::Drawing::Point(3, 3);
    tabControl1->Name = L"tabControl1";
    tabControl1->SelectedIndex = 0;
    tabControl1->Size = System::Drawing::Size(438, 150);
    tabControl1->TabIndex = 0;
    // 
    // namePage
    // 
    this->namePage->Controls->Add(this->ringerNameChanges);
    this->namePage->Controls->Add(surnameLbl);
    this->namePage->Controls->Add(this->surnameEditor);
    this->namePage->Controls->Add(this->forenameEditor);
    this->namePage->Controls->Add(this->deleteAkaBtn);
    this->namePage->Controls->Add(forenameLbl);
    this->namePage->Controls->Add(this->addAkaBtn);
    this->namePage->Controls->Add(this->fromDateLbl);
    this->namePage->Controls->Add(this->akaChangedDate);
    this->namePage->Location = System::Drawing::Point(4, 22);
    this->namePage->Name = L"namePage";
    this->namePage->Padding = System::Windows::Forms::Padding(3);
    this->namePage->Size = System::Drawing::Size(430, 124);
    this->namePage->TabIndex = 1;
    this->namePage->Text = L"Names";
    this->namePage->UseVisualStyleBackColor = true;
    // 
    // ringerNameChanges
    // 
    this->ringerNameChanges->BackColor = System::Drawing::SystemColors::ControlLightLight;
    this->ringerNameChanges->Location = System::Drawing::Point(190, 60);
    this->ringerNameChanges->Name = L"ringerNameChanges";
    this->ringerNameChanges->Size = System::Drawing::Size(95, 45);
    this->ringerNameChanges->TabIndex = 6;
    this->ringerNameChanges->ValueChanged += gcnew System::EventHandler(this, &RingerUI::ActiveNameChanged);
    // 
    // surnameEditor
    // 
    this->surnameEditor->Location = System::Drawing::Point(68, 33);
    this->surnameEditor->Name = L"surnameEditor";
    this->surnameEditor->Size = System::Drawing::Size(345, 20);
    this->surnameEditor->TabIndex = 3;
    this->surnameEditor->TextChanged += gcnew System::EventHandler(this, &RingerUI::RingerDataChanged);
    // 
    // forenameEditor
    // 
    this->forenameEditor->Location = System::Drawing::Point(68, 7);
    this->forenameEditor->Name = L"forenameEditor";
    this->forenameEditor->Size = System::Drawing::Size(345, 20);
    this->forenameEditor->TabIndex = 1;
    this->forenameEditor->TextChanged += gcnew System::EventHandler(this, &RingerUI::RingerDataChanged);
    // 
    // deleteAkaBtn
    // 
    this->deleteAkaBtn->Location = System::Drawing::Point(338, 91);
    this->deleteAkaBtn->Name = L"deleteAkaBtn";
    this->deleteAkaBtn->Size = System::Drawing::Size(75, 23);
    this->deleteAkaBtn->TabIndex = 7;
    this->deleteAkaBtn->Text = L"Delete Aka";
    this->deleteAkaBtn->UseVisualStyleBackColor = true;
    this->deleteAkaBtn->Visible = false;
    this->deleteAkaBtn->Click += gcnew System::EventHandler(this, &RingerUI::DeleteAkaBtn_Click);
    // 
    // addAkaBtn
    // 
    this->addAkaBtn->Location = System::Drawing::Point(338, 62);
    this->addAkaBtn->Name = L"addAkaBtn";
    this->addAkaBtn->Size = System::Drawing::Size(75, 23);
    this->addAkaBtn->TabIndex = 6;
    this->addAkaBtn->Text = L"Add Aka";
    this->addAkaBtn->UseVisualStyleBackColor = true;
    this->addAkaBtn->Click += gcnew System::EventHandler(this, &RingerUI::AddAkaBtn_Click);
    // 
    // fromDateLbl
    // 
    this->fromDateLbl->AutoSize = true;
    this->fromDateLbl->Location = System::Drawing::Point(30, 62);
    this->fromDateLbl->Name = L"fromDateLbl";
    this->fromDateLbl->Size = System::Drawing::Size(33, 13);
    this->fromDateLbl->TabIndex = 4;
    this->fromDateLbl->Text = L"From:";
    // 
    // akaChangedDate
    // 
    this->akaChangedDate->Location = System::Drawing::Point(68, 59);
    this->akaChangedDate->Name = L"akaChangedDate";
    this->akaChangedDate->Size = System::Drawing::Size(118, 20);
    this->akaChangedDate->TabIndex = 5;
    this->akaChangedDate->TextChanged += gcnew System::EventHandler(this, &RingerUI::RingerDataChanged);
    // 
    // linkedRingersPage
    // 
    this->linkedRingersPage->Controls->Add(tableLayoutPanel2);
    this->linkedRingersPage->Location = System::Drawing::Point(4, 22);
    this->linkedRingersPage->Name = L"linkedRingersPage";
    this->linkedRingersPage->Padding = System::Windows::Forms::Padding(3);
    this->linkedRingersPage->Size = System::Drawing::Size(430, 124);
    this->linkedRingersPage->TabIndex = 23;
    this->linkedRingersPage->Text = L"Linked ringers";
    this->linkedRingersPage->UseVisualStyleBackColor = true;
    // 
    // tableLayoutPanel2
    // 
    tableLayoutPanel2->ColumnCount = 2;
    tableLayoutPanel2->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
    tableLayoutPanel2->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
    tableLayoutPanel2->Controls->Add(this->linkedRingers, 0, 0);
    tableLayoutPanel2->Controls->Add(linkedRingersDescription, 0, 1);
    tableLayoutPanel2->Controls->Add(this->similarBtn, 1, 0);
    tableLayoutPanel2->Dock = System::Windows::Forms::DockStyle::Fill;
    tableLayoutPanel2->Location = System::Drawing::Point(3, 3);
    tableLayoutPanel2->Name = L"tableLayoutPanel2";
    tableLayoutPanel2->RowCount = 2;
    tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
    tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
    tableLayoutPanel2->Size = System::Drawing::Size(424, 118);
    tableLayoutPanel2->TabIndex = 1;
    // 
    // linkedRingers
    // 
    this->linkedRingers->CheckOnClick = true;
    this->linkedRingers->Dock = System::Windows::Forms::DockStyle::Fill;
    this->linkedRingers->FormattingEnabled = true;
    this->linkedRingers->Location = System::Drawing::Point(3, 3);
    this->linkedRingers->Name = L"linkedRingers";
    this->linkedRingers->Size = System::Drawing::Size(337, 76);
    this->linkedRingers->TabIndex = 0;
    this->linkedRingers->SelectedValueChanged += gcnew System::EventHandler(this, &RingerUI::linkedRingers_SelectedValueChanged);
    // 
    // linkedRingersDescription
    // 
    linkedRingersDescription->BackColor = System::Drawing::SystemColors::ControlLightLight;
    linkedRingersDescription->BorderStyle = System::Windows::Forms::BorderStyle::None;
    linkedRingersDescription->CausesValidation = false;
    tableLayoutPanel2->SetColumnSpan(linkedRingersDescription, 2);
    linkedRingersDescription->Dock = System::Windows::Forms::DockStyle::Fill;
    linkedRingersDescription->Location = System::Drawing::Point(3, 85);
    linkedRingersDescription->Multiline = true;
    linkedRingersDescription->Name = L"linkedRingersDescription";
    linkedRingersDescription->ReadOnly = true;
    linkedRingersDescription->Size = System::Drawing::Size(418, 30);
    linkedRingersDescription->TabIndex = 0;
    linkedRingersDescription->TabStop = false;
    linkedRingersDescription->Text = L"Ringers linked to this one, can be seen and removed from here. To remove links, c"
        L"heck them and save. To add links, go to ringer search, select two ringers and ri"
        L"ght click.";
    // 
    // similarBtn
    // 
    this->similarBtn->Location = System::Drawing::Point(346, 3);
    this->similarBtn->Name = L"similarBtn";
    this->similarBtn->Size = System::Drawing::Size(75, 23);
    this->similarBtn->TabIndex = 1;
    this->similarBtn->Text = L"Similar";
    this->similarBtn->UseVisualStyleBackColor = true;
    this->similarBtn->Click += gcnew System::EventHandler(this, &RingerUI::similarBtn_Click);
    // 
    // otherPage
    // 
    this->otherPage->Controls->Add(this->nonHuman);
    this->otherPage->Controls->Add(akaGroup);
    this->otherPage->Controls->Add(genderGroup);
    this->otherPage->Location = System::Drawing::Point(4, 22);
    this->otherPage->Name = L"otherPage";
    this->otherPage->Padding = System::Windows::Forms::Padding(3);
    this->otherPage->Size = System::Drawing::Size(430, 124);
    this->otherPage->TabIndex = 16;
    this->otherPage->Text = L"Other";
    this->otherPage->UseVisualStyleBackColor = true;
    // 
    // nonHuman
    // 
    this->nonHuman->AutoSize = true;
    this->nonHuman->Location = System::Drawing::Point(166, 19);
    this->nonHuman->Name = L"nonHuman";
    this->nonHuman->Size = System::Drawing::Size(169, 17);
    this->nonHuman->TabIndex = 4;
    this->nonHuman->Text = L"Non human or computer ringer";
    this->nonHuman->UseVisualStyleBackColor = true;
    this->nonHuman->CheckedChanged += gcnew System::EventHandler(this, &RingerUI::nonHuman_CheckedChanged);
    // 
    // akaGroup
    // 
    akaGroup->Controls->Add(this->akaEditor);
    akaGroup->Location = System::Drawing::Point(6, 49);
    akaGroup->Name = L"akaGroup";
    akaGroup->Size = System::Drawing::Size(407, 52);
    akaGroup->TabIndex = 3;
    akaGroup->TabStop = false;
    akaGroup->Text = L"Also known as";
    // 
    // akaEditor
    // 
    this->akaEditor->Location = System::Drawing::Point(6, 19);
    this->akaEditor->Name = L"akaEditor";
    this->akaEditor->Size = System::Drawing::Size(395, 20);
    this->akaEditor->TabIndex = 2;
    this->akaEditor->TextChanged += gcnew System::EventHandler(this, &RingerUI::AkaEditor_TextChanged);
    // 
    // referencesPage
    // 
    this->referencesPage->Controls->Add(pealbaseGroup);
    this->referencesPage->Location = System::Drawing::Point(4, 22);
    this->referencesPage->Name = L"referencesPage";
    this->referencesPage->Padding = System::Windows::Forms::Padding(3);
    this->referencesPage->Size = System::Drawing::Size(430, 124);
    this->referencesPage->TabIndex = 9;
    this->referencesPage->Text = L"Web links";
    this->referencesPage->UseVisualStyleBackColor = true;
    // 
    // pealbaseGroup
    // 
    pealbaseGroup->AutoSize = true;
    pealbaseGroup->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
    pealbaseGroup->Controls->Add(this->pealbaseRefEditor);
    pealbaseGroup->Controls->Add(this->pealbaseBtn);
    pealbaseGroup->Location = System::Drawing::Point(6, 6);
    pealbaseGroup->Name = L"pealbaseGroup";
    pealbaseGroup->Size = System::Drawing::Size(243, 58);
    pealbaseGroup->TabIndex = 7;
    pealbaseGroup->TabStop = false;
    pealbaseGroup->Text = L"Pealbase";
    // 
    // pealbaseRefEditor
    // 
    this->pealbaseRefEditor->Location = System::Drawing::Point(6, 19);
    this->pealbaseRefEditor->Name = L"pealbaseRefEditor";
    this->pealbaseRefEditor->Size = System::Drawing::Size(150, 20);
    this->pealbaseRefEditor->TabIndex = 5;
    this->pealbaseRefEditor->TextChanged += gcnew System::EventHandler(this, &RingerUI::PealbaseRefEditor_TextChanged);
    // 
    // pealbaseBtn
    // 
    this->pealbaseBtn->AutoSize = true;
    this->pealbaseBtn->Location = System::Drawing::Point(162, 16);
    this->pealbaseBtn->Name = L"pealbaseBtn";
    this->pealbaseBtn->Size = System::Drawing::Size(75, 23);
    this->pealbaseBtn->TabIndex = 6;
    this->pealbaseBtn->Text = L"Open";
    this->pealbaseBtn->UseVisualStyleBackColor = true;
    this->pealbaseBtn->Click += gcnew System::EventHandler(this, &RingerUI::PealbaseBtn_Click);
    // 
    // notesPage
    // 
    this->notesPage->Controls->Add(this->notesEditor);
    this->notesPage->Location = System::Drawing::Point(4, 22);
    this->notesPage->Name = L"notesPage";
    this->notesPage->Padding = System::Windows::Forms::Padding(3);
    this->notesPage->Size = System::Drawing::Size(430, 124);
    this->notesPage->TabIndex = 22;
    this->notesPage->Text = L"Notes";
    this->notesPage->UseVisualStyleBackColor = true;
    // 
    // notesEditor
    // 
    this->notesEditor->AcceptsReturn = true;
    this->notesEditor->Dock = System::Windows::Forms::DockStyle::Fill;
    this->notesEditor->Location = System::Drawing::Point(3, 3);
    this->notesEditor->Multiline = true;
    this->notesEditor->Name = L"notesEditor";
    this->notesEditor->Size = System::Drawing::Size(424, 118);
    this->notesEditor->TabIndex = 0;
    this->notesEditor->TextChanged += gcnew System::EventHandler(this, &RingerUI::NotesEditor_TextChanged);
    // 
    // deceasedPage
    // 
    deceasedPage->Controls->Add(this->deceasedDate);
    deceasedPage->Controls->Add(this->deceased);
    deceasedPage->Location = System::Drawing::Point(4, 22);
    deceasedPage->Name = L"deceasedPage";
    deceasedPage->Padding = System::Windows::Forms::Padding(3);
    deceasedPage->Size = System::Drawing::Size(430, 124);
    deceasedPage->TabIndex = 24;
    deceasedPage->Text = L"Deceased";
    deceasedPage->UseVisualStyleBackColor = true;
    // 
    // deceasedDate
    // 
    this->deceasedDate->Location = System::Drawing::Point(7, 31);
    this->deceasedDate->Name = L"deceasedDate";
    this->deceasedDate->Size = System::Drawing::Size(200, 20);
    this->deceasedDate->TabIndex = 1;
    this->deceasedDate->ValueChanged += gcnew System::EventHandler(this, &RingerUI::deceasedDate_ValueChanged);
    // 
    // deceased
    // 
    this->deceased->AutoSize = true;
    this->deceased->Location = System::Drawing::Point(7, 7);
    this->deceased->Name = L"deceased";
    this->deceased->Size = System::Drawing::Size(75, 17);
    this->deceased->TabIndex = 0;
    this->deceased->Text = L"Deceased";
    this->deceased->UseVisualStyleBackColor = true;
    this->deceased->CheckedChanged += gcnew System::EventHandler(this, &RingerUI::deceased_CheckedChanged);
    // 
    // menuStrip1
    // 
    this->menuStrip1->AllowMerge = false;
    this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) { this->statsBtn, this->bellsBtn });
    this->menuStrip1->Location = System::Drawing::Point(0, 0);
    this->menuStrip1->Name = L"menuStrip1";
    this->menuStrip1->Size = System::Drawing::Size(444, 24);
    this->menuStrip1->TabIndex = 15;
    this->menuStrip1->Text = L"ringerMenuStrip";
    // 
    // statsBtn
    // 
    this->statsBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
    this->statsBtn->Name = L"statsBtn";
    this->statsBtn->Size = System::Drawing::Size(65, 20);
    this->statsBtn->Text = L"Statistics";
    this->statsBtn->Click += gcnew System::EventHandler(this, &RingerUI::StatsBtn_Click);
    // 
    // bellsBtn
    // 
    this->bellsBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
    this->bellsBtn->Name = L"bellsBtn";
    this->bellsBtn->Size = System::Drawing::Size(88, 20);
    this->bellsBtn->Text = L"Leading bells";
    this->bellsBtn->Click += gcnew System::EventHandler(this, &RingerUI::bellsBtn_Click);
    // 
    // RingerUI
    // 
    this->AcceptButton = this->saveBtn;
    this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
    this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
    this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
    this->CancelButton = this->closeBtn;
    this->ClientSize = System::Drawing::Size(444, 239);
    this->Controls->Add(tableLayoutPanel1);
    this->Controls->Add(statusStrip1);
    this->Controls->Add(this->menuStrip1);
    this->MaximizeBox = false;
    this->Name = L"RingerUI";
    this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
    this->Text = L"Ringer";
    this->TopMost = true;
    this->Load += gcnew System::EventHandler(this, &RingerUI::RingerUI_Load);
    genderGroup->ResumeLayout(false);
    genderGroup->PerformLayout();
    statusStrip1->ResumeLayout(false);
    statusStrip1->PerformLayout();
    panel1->ResumeLayout(false);
    panel1->PerformLayout();
    tableLayoutPanel1->ResumeLayout(false);
    tabControl1->ResumeLayout(false);
    this->namePage->ResumeLayout(false);
    this->namePage->PerformLayout();
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ringerNameChanges))->EndInit();
    this->linkedRingersPage->ResumeLayout(false);
    tableLayoutPanel2->ResumeLayout(false);
    tableLayoutPanel2->PerformLayout();
    this->otherPage->ResumeLayout(false);
    this->otherPage->PerformLayout();
    akaGroup->ResumeLayout(false);
    akaGroup->PerformLayout();
    this->referencesPage->ResumeLayout(false);
    this->referencesPage->PerformLayout();
    pealbaseGroup->ResumeLayout(false);
    pealbaseGroup->PerformLayout();
    this->notesPage->ResumeLayout(false);
    this->notesPage->PerformLayout();
    deceasedPage->ResumeLayout(false);
    deceasedPage->PerformLayout();
    this->menuStrip1->ResumeLayout(false);
    this->menuStrip1->PerformLayout();
    this->ResumeLayout(false);
    this->PerformLayout();

}

System::Void 
RingerUI::Back10Btn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId nextRingerId (currentRinger->Id());
    if (database->ForwardMultipleRingers(nextRingerId, false))
    {
        PopulateViewWithId(nextRingerId);
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void 
RingerUI::Forward10Btn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId nextRingerId (currentRinger->Id());
    if (database->ForwardMultipleRingers(nextRingerId, true))
    {
        PopulateViewWithId(nextRingerId);
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void 
RingerUI::NextBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId nextRingerId (currentRinger->Id());
    if (database->NextRinger(nextRingerId, true, false))
    {
        PopulateViewWithId(nextRingerId);
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void 
RingerUI::PreviousBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId nextRingerId (currentRinger->Id());
    if (database->NextRinger(nextRingerId, false, false))
    {
        PopulateViewWithId(nextRingerId);
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void 
RingerUI::EndBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId nextRingerId;
    if (database->LastRinger(nextRingerId))
    {
        PopulateViewWithId(nextRingerId);
    }
}

System::Void 
RingerUI::StartBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId nextRingerId;
    if (database->FirstRinger(nextRingerId, false))
    {
        PopulateViewWithId(nextRingerId);
    }
}

System::Void 
RingerUI::AddAkaBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    dataChanged = true;
    DateTime today = DateTime::Today;
    std::wstring tempForeNameStr;
    DucoUtils::ConvertString(forenameEditor->Text, tempForeNameStr);
    currentRinger->AddNameChange(tempForeNameStr, L"", DucoUtils::ConvertDate(today));

    fromDateLbl->Visible = true;
    akaChangedDate->Visible = true;
    ringerNameChanges->Visible = true;
    deleteAkaBtn->Visible = true;

    ringerNameChanges->Maximum = int(currentRinger->NoOfNameChanges() + 1);
    ringerNameChanges->Minimum = 1;
    ringerNameChanges->Value = ringerNameChanges->Maximum;
    ringerNameChanges->Enabled = true;
    akaChangedDate->Value = today;

    surnameEditor->Text = "";
    akaChangedDate->Enabled = true;
}

System::Void
RingerUI::DeleteAkaBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    dataChanged = true;
    changingName = true;
    currentRinger->DeleteNameChange(ringerNameChanges->Value - 2);

    size_t noOfAkas = currentRinger->NoOfNameChanges();
    if (noOfAkas == 0)
    {
        ringerNameChanges->Visible = false;
        deleteAkaBtn->Visible = false;
        SetActiveName(1);
    }
    else
    {
        ringerNameChanges->Maximum -= 1;
        ringerNameChanges->Value -= 1;
        SetActiveName(ringerNameChanges->Value);
    }
    changingName = false;
}

System::Void 
RingerUI::EditBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->StartEdit(TObjectType::ERinger, currentRinger->Id()))
        SetEditState();
    else
        SoundUtils::PlayErrorSound();
}

System::Void
RingerUI::CloseBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    switch (windowState)
    {
    case DucoWindowState::ENewMode:
        {
            SetViewState();
            Duco::ObjectId nextRingerId;
            if (database->FirstRinger(nextRingerId, false))
            {
                PopulateViewWithId(nextRingerId);
            }
        }
        break;

    case DucoWindowState::EEditMode:
        {
            SetViewState();
            if (database->FindRinger(currentRinger->Id(), *currentRinger, false))
            {
                PopulateView();
            }
        }
        break;

    default:
        Close();
        break;
    }
}

System::Void
RingerUI::CancelBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (dataChanged && (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode || windowState == DucoWindowState::EAutoNewMode))
    {
        if (DucoUtils::ConfirmLooseChanges("Ringer", this))
        {
            return;
        }
        // set data changed to false, so the closing event will not prompt the user again
        dataChanged = false;
    }

    switch (windowState)
    {
    case DucoWindowState::ENewMode:
    {
        SetViewState();
        Duco::ObjectId nextRingerId;
        if (database->FirstRinger(nextRingerId, false))
        {
            PopulateViewWithId(nextRingerId);
        }
        Close();
    }
    break;

    case DucoWindowState::EEditMode:
    {
        database->CancelEdit(TObjectType::ERinger, currentRinger->Id());
        SetViewState();
        if (database->FindRinger(currentRinger->Id(), *currentRinger, false))
        {
            PopulateView();
        }
    }
    break;

    default:
        break;
    }
}

System::Void
RingerUI::SaveBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (!currentRinger->Valid(database->Database(), false, true))
    {
        MessageBox::Show(this, DucoUtils::ConvertString(currentRinger->ErrorString(database->Database().Settings(), database->Config().IncludeWarningsInValidation())), "Ringer missing essential data");
        return;
    }

    if (dataChanged)
    {
        if (windowState == DucoWindowState::ENewMode)
        {
            Duco::ObjectId newRingerId = database->AddRinger(*currentRinger);
            if (newRingerId.ValidId())
            {
                PopulateViewWithId(newRingerId);
                SetViewState();
            }
            return;

        }
        else
        {
            database->UpdateRinger(*currentRinger, true);
            System::Collections::IEnumerator^ linksToRemove = linkedRingers->SelectedIndices->GetEnumerator();
            while (linksToRemove->MoveNext())
            {
                int index = safe_cast<int>(linksToRemove->Current);
                Duco::ObjectId linkedRingerId = currentRingerLinkedIds->operator[](index);
                database->Database().RingersDatabase().RemoveLinkedRinger(linkedRingerId);
            
            }
        }
        dataChanged = false;
    }
    else
    {
        database->CancelEdit(TObjectType::ERinger, currentRinger->Id());
    }

    switch (windowState)
    {
    case DucoWindowState::ENewMode:
        break;
    default:
        SetViewState();
        if (database->FindRinger(currentRinger->Id(), *currentRinger, false))
        {
            PopulateView();
        }
        break;
    }
}

System::Void 
RingerUI::ActiveNameChanged(System::Object^  sender, System::EventArgs^  e)
{
    changingName = true;
    int nameNumber = ringerNameChanges->Value;
    SetActiveName(nameNumber);
    changingName = false;
    if (windowState == DucoWindowState::EEditMode && nameNumber != 1)
    {
        deleteAkaBtn->Visible = true;
    }
    else
    {
        deleteAkaBtn->Visible = false;
    }
}

System::Void 
RingerUI::NotesEditor_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        std::wstring newNotes;
        DucoUtils::ConvertString(notesEditor->Text, newNotes);
        currentRinger->SetNotes(newNotes);
        dataChanged = true;
    }
}

System::Void 
RingerUI::AkaEditor_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        std::wstring newAka;
        DucoUtils::ConvertString(akaEditor->Text, newAka);
        currentRinger->SetAlsoKnownAs(newAka);
        dataChanged = true;
    }
}

System::Void 
RingerUI::PealbaseRefEditor_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        std::wstring newRef;
        DucoUtils::ConvertString(pealbaseRefEditor->Text, newRef);
        currentRinger->SetPealbaseReference(newRef);
        dataChanged = true;
    }
}

System::Void
RingerUI::nonHuman_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
{
    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        currentRinger->SetNonHuman(nonHuman->Checked);
        dataChanged = true;
    }
    male->Enabled = !nonHuman->Checked;
    female->Enabled = !nonHuman->Checked;
}


System::Void 
RingerUI::SetActiveName(unsigned int nameNumber)
{
    if (nameNumber > 1)
    {
        const Duco::RingerNameChange& lastAka = currentRinger->NameChange(nameNumber-2);
        forenameEditor->Text = DucoUtils::ConvertString(lastAka.forename);
        surnameEditor->Text = DucoUtils::ConvertString(lastAka.surname);
        System::DateTime^ akaDate = DucoUtils::ConvertDate(lastAka.date);
        akaChangedDate->Value = *akaDate;
        fromDateLbl->Visible = true;
        akaChangedDate->Visible = true;
    }
    else if (nameNumber == 1)
    {
        forenameEditor->Text = DucoUtils::ConvertString(currentRinger->Forename());
        surnameEditor->Text = DucoUtils::ConvertString(currentRinger->Surname());
        fromDateLbl->Visible = false;
        akaChangedDate->Visible = false;
    }
}

System::Void 
RingerUI::RingerDataChanged(System::Object^ sender, System::EventArgs^ e)
{
    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        if (!changingName == true)
        {
            unsigned int nameId = 0;
            if (ringerNameChanges->Visible)
            {
                nameId = ringerNameChanges->Value-1;
            }
            dataChanged = true;
            if (sender == surnameEditor)
            {
                std::wstring newsurname;
                DucoUtils::ConvertString(surnameEditor->Text, newsurname);
                currentRinger->SetSurname(nameId, newsurname);
            }
            else if (sender == forenameEditor)
            {
                std::wstring newforname;
                DucoUtils::ConvertString(forenameEditor->Text, newforname);
                currentRinger->SetForename(nameId, newforname);
            }
            else if (sender == akaChangedDate)
            {
                PerformanceDate newDate = DucoUtils::ConvertDate(akaChangedDate->Value);
                currentRinger->SetDateChanged(nameId, newDate);
            }
        }
    }
}

System::Void
RingerUI::SetNewState()
{
    forenameEditor->ReadOnly = false;
    forenameEditor->Text = "";
    surnameEditor->ReadOnly = false;
    surnameEditor->Text = "";
    fromDateLbl->Visible = false;
    akaChangedDate->Visible = false;
    ringerNameChanges->Visible = false;
    male->Enabled = true;
    female->Enabled = true;
    nonHuman->Enabled = true;
    notesEditor->Enabled = true;
    akaEditor->Enabled = true;
    pealbaseRefEditor->Enabled = true;

    editBtn->Visible = false;
    closeBtn->Visible = false;
    saveBtn->Visible = true;
    cancelBtn->Visible = true;
    addAkaBtn->Visible = true;
    deleteAkaBtn->Visible = false;
    forward10Btn->Visible = false;
    back10Btn->Visible = false;
    nextBtn->Visible = false;
    previousBtn->Visible = false;
    startBtn->Visible = false;
    endBtn->Visible = false;
    statsBtn->Enabled = false;
    bellsBtn->Enabled = false;
    pealbaseBtn->Visible = false;

    ringerIdLabel->Text = "New ringer";

    windowState = DucoWindowState::ENewMode;
}

System::Void 
RingerUI::SetViewState()
{
    dataChanged = false;
    forenameEditor->ReadOnly = true;
    surnameEditor->ReadOnly = true;
    akaChangedDate->Enabled = false;
    male->Enabled = false;
    female->Enabled = false;
    nonHuman->Enabled = false;
    ringerNameChanges->Visible = currentRinger->NoOfNameChanges() > 0;
    notesEditor->Enabled = false;
    akaEditor->Enabled = false;
    pealbaseRefEditor->Enabled = false;
    bellsBtn->Enabled = true;

    editBtn->Visible = currentRinger->Id().ValidId();
    closeBtn->Visible = true;
    saveBtn->Visible = false;
    cancelBtn->Visible = false;
    addAkaBtn->Visible = false;
    deleteAkaBtn->Visible = false;
    forward10Btn->Visible = true;
    back10Btn->Visible = true;
    nextBtn->Visible = true;
    previousBtn->Visible = true;
    startBtn->Visible = true;
    endBtn->Visible = true;
    statsBtn->Enabled = true;
    pealbaseBtn->Visible = true;
    closeBtn->Text = "Close";
    linkedRingers->Enabled = false;
    deceased->Enabled = false;
    deceasedDate->Enabled = false;
    PerformanceDate dataOfDeath;
    if (!currentRinger->Deceased() || !currentRinger->DateOfDeath(dataOfDeath))
    {
        deceasedDate->Visible = false;
    }
    else
    {
        deceasedDate->Visible = true;
    }

    windowState = DucoWindowState::EViewMode;
}

System::Void
RingerUI::SetEditState()
{
    forenameEditor->ReadOnly = false;
    surnameEditor->ReadOnly = false;
    male->Enabled = true;
    female->Enabled = true;
    nonHuman->Enabled = true;
    notesEditor->Enabled = true;
    akaEditor->Enabled = true;
    pealbaseRefEditor->Enabled = true;
    bellsBtn->Enabled = false;

    Boolean zeroAkas (currentRinger->NoOfNameChanges() == 0);
    fromDateLbl->Visible = !zeroAkas;
    akaChangedDate->Visible = !zeroAkas && ringerNameChanges->Value != 1;
    akaChangedDate->Enabled = !zeroAkas;
    ringerNameChanges->Visible = !zeroAkas;
    ringerNameChanges->Enabled = !zeroAkas;
    deleteAkaBtn->Visible = !zeroAkas && ringerNameChanges->Value != 1;

    editBtn->Visible = false;
    closeBtn->Visible = false;
    saveBtn->Visible = true;
    cancelBtn->Visible = true;
    addAkaBtn->Visible = true;
    forward10Btn->Visible = false;
    back10Btn->Visible = false;
    nextBtn->Visible = false;
    startBtn->Visible = false;
    endBtn->Visible = false;
    previousBtn->Visible = false;
    statsBtn->Enabled = false;
    pealbaseBtn->Visible = false;
    linkedRingers->Enabled = true;
    deceased->Enabled = true;
    deceasedDate->Enabled = true;

    windowState = DucoWindowState::EEditMode;
}

System::Void 
RingerUI::PopulateViewWithId(const Duco::ObjectId& ringerId)
{
    if (database->FindRinger(ringerId, *currentRinger, false))
    {
        PopulateView();
    }
    else
    {
        SoundUtils::PlayErrorSound();
        ringerNameChanges->Visible = false;
        akaChangedDate->Visible = false;
    }
}

System::Void 
RingerUI::UpdateIdLabel()
{
    if (currentRinger->Id() != -1)
        ringerIdLabel->Text = String::Format("{0:D}/{1:D}", currentRinger->Id().Id(), database->NumberOfObjects(TObjectType::ERinger));
    else
        ringerIdLabel->Text = "";
}

System::Void 
RingerUI::PopulateView()
{
    UpdateIdLabel();

    size_t noOfAkas (currentRinger->NoOfNameChanges());

    if (noOfAkas > 0)
    {
        const Duco::RingerNameChange& lastAka = currentRinger->NameChange(noOfAkas-1);
        forenameEditor->Text = DucoUtils::ConvertString(lastAka.forename);
        surnameEditor->Text = DucoUtils::ConvertString(lastAka.surname);
        ringerNameChanges->Maximum = noOfAkas + 1;
        ringerNameChanges->Minimum = 1;
        ringerNameChanges->Value = noOfAkas + 1;
        System::DateTime^ akaDate = DucoUtils::ConvertDate(lastAka.date);
        akaChangedDate->Value = *akaDate;

        fromDateLbl->Visible = true;
        akaChangedDate->Visible = true;
        akaChangedDate->Enabled = false;
        ringerNameChanges->Visible = true;
    }
    else
    {
        forenameEditor->Text = DucoUtils::ConvertString(currentRinger->Forename());
        surnameEditor->Text = DucoUtils::ConvertString(currentRinger->Surname());
        fromDateLbl->Visible = false;
        akaChangedDate->Visible = false;
        ringerNameChanges->Visible = false;
    }
    male->Checked = currentRinger->Male();
    female->Checked = currentRinger->Female();
    nonHuman->Checked = currentRinger->NonHuman();
    notesEditor->Text = DucoUtils::ConvertString(currentRinger->Notes());
    akaEditor->Text = DucoUtils::ConvertString(currentRinger->AlsoKnownAs());
    pealbaseRefEditor->Text = DucoUtils::ConvertString(currentRinger->PealbaseReference());
    linkedRingers->Items->Clear();
    std::set<Duco::ObjectId> templinks;
    if (database->Database().RingersDatabase().LinkedRingers(currentRinger->Id(), templinks, false ))
    {
        currentRingerLinkedIds->clear();
        for (const auto& linkedRingerId : templinks)
        {
            Duco::Ringer linkedRinger;
            if (currentRinger->Id() != linkedRingerId && database->FindRinger(linkedRingerId, linkedRinger, false))
            {
                linkedRingers->Items->Add(DucoUtils::ConvertString(linkedRinger.Forename() + L" " + linkedRinger.Surname()));
                currentRingerLinkedIds->push_back(linkedRingerId);
            }
        }
    }
    deceased->Checked = currentRinger->Deceased();
    if (!currentRinger->Deceased())
    {
        deceasedDate->Visible = false;
    }
    else
    {
        deceasedDate->Visible = true;
        PerformanceDate dateOfDeath;
        if (currentRinger->DateOfDeath(dateOfDeath))
        {
            deceasedDate->Value = *DucoUtils::ConvertDate(dateOfDeath);
        }
        else
        {
            if (windowState == DucoWindowState::EViewMode)
            {
                deceasedDate->Visible = false;
            }
        }
    }
}

System::Void
RingerUI::RingerIdLabel_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (windowState == DucoWindowState::EViewMode)
    {
        System::Int16 id = -1;
        InputIndexQueryUI^ findIdDialog = gcnew InputIndexQueryUI("ringer", id);
        System::Windows::Forms::DialogResult returnVal = findIdDialog->ShowDialog();
        if (returnVal == ::DialogResult::OK)
        {
            PopulateViewWithId(id);
        }
    }
}

System::Void
RingerUI::StatsBtn_Click(System::Object^ sender, System::EventArgs^  e)
{
    SingleObjectStatsUI^ statsUI = gcnew SingleObjectStatsUI(database, currentRinger->Id(), TObjectType::ERinger);
    statsUI->MdiParent = this->MdiParent;
    statsUI->Show();
}

System::Void
RingerUI::PealbaseBtn_Click(System::Object^ sender, System::EventArgs^  e)
{
    String^ website = gcnew String("http://www.pealbase.co.uk/pealbase");
    if (currentRinger->PealbaseReference().length() > 0)
    {
        website += "/ringer.php?ringer=";
        website += DucoUtils::ConvertString(currentRinger->PealbaseReference());
    }
    System::Diagnostics::Process::Start( website );
}

System::Void
RingerUI::Male_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        RadioButton^ maleSender = static_cast<RadioButton^>(sender);
        if (maleSender->Checked)
        {
            dataChanged = true;
            currentRinger->SetMale();
        }
    }
}

System::Void
RingerUI::Female_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        RadioButton^ femaleSender = static_cast<RadioButton^>(sender);
        if (femaleSender->Checked)
        {
            dataChanged = true;
            currentRinger->SetFemale();
        }
    }
}

System::Void
RingerUI::deceased_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
{
    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        dataChanged = true;
        currentRinger->SetDeceased(deceased->Checked);
        if (deceased->Checked)
        {
            deceasedDate->Visible = true;
        }
        else
        {
            deceasedDate->Visible = false;
        }
    }
}

System::Void
RingerUI::deceasedDate_ValueChanged(System::Object^ sender, System::EventArgs^ e)
{
    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        dataChanged = true;
        Duco::PerformanceDate newDoD = DucoUtils::ConvertDate(deceasedDate->Value);
        if (newDoD.Valid())
        {
            currentRinger->SetDateOfDeath(newDoD);
        }
        else
        {
            SoundUtils::PlayErrorSound();
        }
    }
}


System::Void
RingerUI::bellsBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    LeadingBellsUI^ ringerStats = gcnew LeadingBellsUI(database, currentRinger->Id());
    ringerStats->MdiParent = this->MdiParent;
    ringerStats->Show();
}


System::Void
RingerUI::linkedRingers_SelectedValueChanged(System::Object^ sender, System::EventArgs^ e)
{
    dataChanged = true;
}

System::Void
RingerUI::similarBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    RingerSearchUI^ validateUI = gcnew RingerSearchUI(database, false);
    validateUI->SetSimilarRingerId(currentRinger->Id());
    validateUI->MdiParent = this->MdiParent;
    validateUI->Show();
}
