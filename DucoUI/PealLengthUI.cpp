#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"
#include <PealLengthInfo.h>
#include <StatisticFilters.h>

#include "PealLengthUI.h"

#include "DucoWindowState.h"
#include "OpenObjectCallback.h"
#include "PictureListControl.h"
#include "PictureControl.h"
#include "RingerList.h"
#include <Tower.h>
#include "PrintBandSummaryUtil.h"
#include <DownloadedPeal.h>
#include "PealUI.h"
#include "SystemDefaultIcon.h"
#include <Peal.h>
#include "DucoUtils.h"
#include "FiltersUI.h"
#include <RingingDatabase.h>
#include <PealDatabase.h>
#include <DucoEngineUtils.h>

using namespace Duco;
using namespace DucoUI;

PealLengthUI::PealLengthUI(DatabaseManager^ theDatabase)
    : database (theDatabase)
{
    InitializeComponent();
    pealInfo = new PealLengthInfo;
    filters = new Duco::StatisticFilters(database->Database());
    filters->SetExcludeInvalidTimeOrChanges(true);
}

PealLengthUI::!PealLengthUI()
{
    database->RemoveObserver(this);
    delete filters;
}

 PealLengthUI::~PealLengthUI()
{
    this->!PealLengthUI();
    if (components)
    {
        delete components;
    }
    delete pealInfo;
}

void
PealLengthUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    switch (type)
    {
    case TObjectType::EPeal:
        GenerateGeneral();
        break;
    default:
        break;
    }
}

System::Void
PealLengthUI::PealLengthUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    GenerateGeneral();
    database->AddObserver(this);
}

System::Void
PealLengthUI::filtersBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    FiltersUI^ filtersDialog = gcnew FiltersUI(*filters, database);
    System::Windows::Forms::DialogResult returnVal = filtersDialog->ShowDialog(this);
    if (returnVal == System::Windows::Forms::DialogResult::OK)
    {
        GenerateGeneral();
    }
}

System::Void
PealLengthUI::GenerateGeneral()
{
    *pealInfo = database->Database().PealsDatabase().PerformanceInfo(*filters);

    Duco::Peal longestChangesPeal;
    if (database->FindPeal(pealInfo->LongestChangesId(), longestChangesPeal, false))
    {
        longestChanges->Text = DucoUtils::ConvertString(DucoEngineUtils::ToString(longestChangesPeal.NoOfChanges(), true));
    }
    else
    {
        longestChanges->Text = "--";
    }
    Duco::Peal shortestChangesPeal;
    if (database->FindPeal(pealInfo->ShortestChangesId(), shortestChangesPeal, false))
    {
        shortestChanges->Text = DucoUtils::ConvertString(DucoEngineUtils::ToString(shortestChangesPeal.NoOfChanges(), true));
    }
    else
    {
        shortestChanges->Text = "--";
    }
    Duco::Peal longestMinutesPeal;
    if (database->FindPeal(pealInfo->LongestMinutesId(), longestMinutesPeal, false))
    {
        longestTime->Text = DucoUtils::PrintPealTime(longestMinutesPeal.Time(), false);
    }
    else
    {
        longestTime->Text = "--";
    }
    Duco::Peal shortestMinutesPeal;
    if (database->FindPeal(pealInfo->ShortestMinutesId(), shortestMinutesPeal, false))
    {
        shortestTime->Text = DucoUtils::PrintPealTime(shortestMinutesPeal.Time(), false);
    }
    else
    {
        shortestTime->Text = "--";
    }
    Duco::Peal fastestCpmPeal;
    if (database->FindPeal(pealInfo->SlowestCpmId(), fastestCpmPeal, false))
    {
        longestCPM->Text = DucoUtils::FormatFloatToString(fastestCpmPeal.ChangesPerMinute());
    }
    else
    {
        longestCPM->Text = "--";
    }
    Duco::Peal slowestCpmPeal;
    if (database->FindPeal(pealInfo->FastestCpmId(), slowestCpmPeal, false))
    {
        shortestCPM->Text = DucoUtils::FormatFloatToString(slowestCpmPeal.ChangesPerMinute());
    }
    else
    {
        shortestCPM->Text = "--";
    }

    averageChanges->Text = DucoUtils::ConvertString(DucoEngineUtils::ToString(pealInfo->AverageChanges(), true));
    averageTime->Text = DucoUtils::PrintPealTime(pealInfo->AverageMinutes(), false);
    totalChanges->Text = DucoUtils::ConvertString(DucoEngineUtils::ToString(pealInfo->TotalChanges(), true));
    totalTime->Text = DucoUtils::PrintPealTime(pealInfo->TotalMinutes(), false);
    if (pealInfo->TotalChanges() > 0 && pealInfo->TotalMinutes() > 0)
        averageCPM->Text = DucoUtils::FormatDoubleToString(pealInfo->AveragePealSpeed(), 2);
    else
        averageCPM->Text = "--";
}

System::Void
PealLengthUI::longestTime_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
    PealUI::ShowPeal(database, this->MdiParent, pealInfo->LongestMinutesId());
}

System::Void
PealLengthUI::longestChanges_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
    PealUI::ShowPeal(database, this->MdiParent, pealInfo->LongestChangesId());
}

System::Void
PealLengthUI::longestCPM_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
    PealUI::ShowPeal(database, this->MdiParent, pealInfo->SlowestCpmId());
}

System::Void
PealLengthUI::shortestTime_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
    PealUI::ShowPeal(database, this->MdiParent, pealInfo->ShortestMinutesId());
}

System::Void
PealLengthUI::shortestChanges_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
    PealUI::ShowPeal(database, this->MdiParent, pealInfo->ShortestChangesId());
}

System::Void
PealLengthUI::shortestCPM_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
    PealUI::ShowPeal(database, this->MdiParent, pealInfo->FastestCpmId());
}

System::Void 
PealLengthUI::averageTime_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
    unsigned int averageNumberOfMinutes = pealInfo->AverageMinutes();
    Duco::ObjectId objectId = database->Database().PealsDatabase().FindPealWithAverageTime(averageNumberOfMinutes);
    if (objectId.ValidId())
    {
        PealUI::ShowPeal(database, this->MdiParent, objectId);
    }
}

System::Void 
PealLengthUI::averageChanges_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
    unsigned int averageNumberOfMinutes = pealInfo->AverageChanges();
    Duco::ObjectId objectId = database->Database().PealsDatabase().FindPealWithAverageChanges(averageNumberOfMinutes);
    if (objectId.ValidId())
    {
        PealUI::ShowPeal(database, this->MdiParent, objectId);
    }
}

System::Void 
PealLengthUI::averageCPM_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
    double averageNumberOfChanges = pealInfo->AveragePealSpeed();
    Duco::ObjectId objectId = database->Database().PealsDatabase().FindPealWithAverageChangesPerMinute(averageNumberOfChanges);
    if (objectId.ValidId())
    {
        PealUI::ShowPeal(database, this->MdiParent, objectId);
    }
}
