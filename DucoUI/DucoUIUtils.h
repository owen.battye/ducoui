#pragma once
namespace Duco
{
    class DatabaseSearch;
}

namespace DucoUI
{
    ref class DucoUIUtils
    {
        public:
            static bool NumericKey(System::Windows::Forms::KeyEventArgs^% keys, System::Windows::Forms::Keys currentModifiers);
            static bool NotationKey(System::Windows::Forms::KeyEventArgs^% keys, System::Windows::Forms::Keys currentModifiers, bool fullNotation, unsigned int noOfBells);

            static System::Void Print(System::Windows::Forms::Form^ owner, System::Object^ sender,
                                      System::Drawing::Printing::PrintPageEventHandler^ printHandler,
                                      System::String^ objectName,
                                      System::Boolean usePrintPreview,
                                      bool landscape);
    };

    ref class SearchUIArgs
    {
        public:
            Duco::DatabaseSearch*  search;
    };
}