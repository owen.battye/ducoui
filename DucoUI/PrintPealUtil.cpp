#include "PrintPealUtil.h"
#include "DatabaseManager.h"
#include <Peal.h>
#include <Tower.h>
#include <Ring.h>
#include <Method.h>
#include <Ringer.h>
#include "DucoUtils.h"
#include <DatabaseSettings.h>
#include <DucoEngineUtils.h>

using namespace System;
using namespace System::Drawing;
using namespace Duco;
using namespace DucoUI;

PrintPealUtil::PrintPealUtil(DatabaseManager^ theDatabase, System::Drawing::Printing::PrintPageEventArgs^ args)
:	PrintUtilBase(theDatabase, 12, args, false)
{

}

void
PrintPealUtil::SetObjects(const Duco::Peal* thePeal, const Duco::Tower* theTower)
{
    currentPeal = thePeal;
    currentTower = theTower;
}

System::Void
PrintPealUtil::printObject(System::Drawing::Printing::PrintPageEventArgs^ printArgs, System::Drawing::Printing::PrinterSettings^ printSettings)
{
    if (database->Settings().PealPrintFormat() == DatabaseSettings::ERingingWorldStyle)
    {
        printPealRWFormat(*currentPeal, *currentTower, printArgs);
    }
    else
    {
        printPealPealBoardFormat(*currentPeal, *currentTower, printArgs);
    }
}

System::Void
PrintPealUtil::printPealPealBoardFormat(const Duco::Peal& currentPeal, const Duco::Tower& currentTower, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
{
    StringFormat^ stringFormat = gcnew StringFormat;
    addDucoFooter(smallFont, gcnew StringFormat, printArgs);

    array<Single>^ tabStops = {20.0f};
    Single firstTab (0.0f);
    stringFormat->SetTabStops(firstTab, tabStops);

    // Association line
    PrintStringCentrally(DucoUtils::ConvertString(currentPeal.AssociationName(database->Database())), boldFont, printArgs);
    yPos += lineHeight * float(1.5);

    // Location and tenor weight line
    System::String^ locationLine = DucoUtils::ConvertString(currentTower.City());
    bool printCounty (currentTower.County().length() > 0);
    if (printCounty)
        locationLine += DucoUtils::ConvertString(L", " + currentTower.County());
    locationLine += DucoUtils::ConvertString(L" At " + currentTower.Name());
    const Ring* const theRing = currentTower.FindRing(currentPeal.RingId());
    if (theRing != NULL)
    {
        locationLine += DucoUtils::ConvertString(L" " + theRing->TenorWeight());
        if (theRing->TenorKey().length() > 0)
        {
            if (theRing->TenorWeight().length() > 0)
            {
                locationLine += " ";
            }
            locationLine += "in ";
            locationLine += DucoUtils::ConvertString(theRing->TenorKey());
        }
    }
    PrintStringCentrally(locationLine, normalFont, printArgs);
    yPos += lineHeight * float(1.5);

    // date and time line
    std::wstring date;
    DucoUtils::ConvertString(DucoUtils::ConvertString(currentPeal.Date(), true), date);
    std::wstring time;
    DucoUtils::ConvertString(DucoUtils::PrintPealTime(currentPeal.Time(), true), time);
    System::String^ dateLine = "On " + DucoUtils::ConvertString(date) + ". In " + DucoUtils::ConvertString(time);
    PrintStringCentrally(dateLine, normalFont, printArgs);
    yPos += lineHeight * 1.5f;

    // method line
    std::wstring methodLine;
    DucoUtils::ConvertString(DucoUtils::ConvertString(DucoEngineUtils::ToString(currentPeal.NoOfChanges(), true)), methodLine);
    Method theMethod;
    if (database->FindMethod(currentPeal.MethodId(), theMethod, false))
    {
        methodLine += L" " + theMethod.FullName(database->Database());
    }
    PrintStringCentrally(DucoUtils::ConvertString(methodLine), normalFont, printArgs);
    yPos += lineHeight * 1.5f;

    // Composer line
    if (currentPeal.ContainsComposer())
    {
        System::String^ composerLine = "Composed by: ";
        composerLine += DucoUtils::ConvertString(currentPeal.Composer());
        PrintStringCentrally(composerLine, normalFont, printArgs);
        yPos += lineHeight * 1.5f;
    }

    if (currentPeal.ContainsMultiMethods())
    {
        PrintLongString(currentPeal.MultiMethods(), normalFont, printArgs);
        yPos += lineHeight;
    }

    yPos += lineHeight * 0.5f;

    array<Single>^ ringerTabStops = {80.0f};
    StringFormat^ ringerStringFormat = gcnew StringFormat();
    ringerStringFormat->SetTabStops(firstTab, ringerTabStops);

    for (unsigned int bellNo (1); bellNo <= currentPeal.NoOfBellsRung(database->Database()); ++bellNo)
    {
        Duco::ObjectId ringerId;
        if (currentPeal.RingerId(bellNo, false, ringerId))
        {
            String^ ringerLine = "";
            std::wstring bellNumbers;
            if (currentPeal.Handbell() && currentPeal.HandBellsRung(ringerId, bellNumbers))
            {
                ringerLine += DucoUtils::ConvertString(bellNumbers);
            }
            else
            {
                ringerLine = Convert::ToString(bellNo);
            }
            ringerLine += "\t";
            Duco::Ringer theRinger;
            if (database->FindRinger(ringerId, theRinger, false))
            {
                ringerLine += DucoUtils::ConvertString(theRinger.FullName(currentPeal.Date(), database->Settings().PrintSurnameFirst()));
                printArgs->Graphics->DrawString(ringerLine, normalFont, Brushes::Black, leftMargin, yPos, ringerStringFormat );
                yPos += lineHeight * float(1.5);
            }
        }
        if (currentPeal.RingerId(bellNo, true, ringerId))
        {
            String^ ringerLine = "Strapper\t";
            Duco::Ringer theRinger;
            if (database->FindRinger(ringerId, theRinger, false))
            {
                ringerLine += DucoUtils::ConvertString(theRinger.FullName(currentPeal.Date(), database->Settings().PrintSurnameFirst()));
                printArgs->Graphics->DrawString(ringerLine, normalFont, Brushes::Black, leftMargin, yPos, ringerStringFormat );
                yPos += lineHeight * float(1.5);
            }
        }
    }
    yPos += lineHeight;

    switch (currentPeal.ConductedType())
    {
    case ESilentAndNonConducted:
        {
        String^ silentLine = "Silent and Non-Conducted";
        printArgs->Graphics->DrawString(silentLine, normalFont, Brushes::Black, leftMargin, yPos, stringFormat);
        yPos += lineHeight;
        }
        break;

    default:
        {
            bool printConductors (false);
            std::wstring conductedLine = L"Conducted by: ";
            std::set<Duco::ObjectId>::const_iterator conductors = currentPeal.Conductors().begin();
            while (conductors != currentPeal.Conductors().end())
            {
                Duco::Ringer nextRinger;
                if (database->FindRinger(*conductors, nextRinger, false))
                {
                    printConductors = true;
                    conductedLine += nextRinger.FullName(currentPeal.Date(), database->Settings().PrintSurnameFirst()) + L" ";
                }
                ++conductors;
            }
            if (printConductors)
                PrintLongString(conductedLine, normalFont, printArgs);
        }
        break;

    }
    yPos += lineHeight;

    if (currentPeal.ContainsFootnotes())
    {
        PrintLongString(currentPeal.Footnotes(), normalFont, printArgs);
    }

    yPos += lineHeight * 2;

    String^ pealNumberLine = "Peal number " + DucoUtils::ConvertString(currentPeal.Id());
    SizeF statsStrSize = printArgs->Graphics->MeasureString(pealNumberLine, smallFont);
    float statsXPos = float(printArgs->MarginBounds.Width) - statsStrSize.Width + leftMargin;
    float statsYPos = float(printArgs->MarginBounds.Height + printArgs->MarginBounds.Top) - (2 * lineHeight);
    printArgs->Graphics->DrawString(pealNumberLine, smallFont, Brushes::Black, statsXPos, statsYPos, ringerStringFormat );

    printArgs->HasMorePages = false;
}

System::Void
PrintPealUtil::printPealRWFormat(const Duco::Peal& currentPeal, const Duco::Tower& currentTower, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
{
    StringFormat^ stringFormat = gcnew StringFormat;
    addDucoFooter(smallFont, stringFormat, printArgs);

    array<Single>^ tabStops = {};
    Single firstTab (0.0f);
    stringFormat->SetTabStops(firstTab, tabStops);

    String^ printBuffer = DucoUtils::ConvertString(currentPeal.AssociationName(database->Database()));
    printArgs->Graphics->DrawString(printBuffer, boldFont, Brushes::Black, leftMargin, yPos, stringFormat );
    yPos += float(lineHeight * 1.5);

    printBuffer = DucoUtils::ConvertString(currentTower.City());
    bool printCounty (currentTower.County().length() > 0);
    if (!printCounty)
        printBuffer += ".";
    else
        printBuffer += ", ";
    printArgs->Graphics->DrawString(printBuffer, boldFont, Brushes::Black, leftMargin, yPos, stringFormat );
    if (printCounty)
    {
        yPos += boldFont->GetHeight(printArgs->Graphics) - normalFont->GetHeight(printArgs->Graphics);;
        SizeF countyStrSize = printArgs->Graphics->MeasureString(printBuffer, boldFont);
        printBuffer = DucoUtils::ConvertString(currentTower.County());
        printBuffer += ".";
        printArgs->Graphics->DrawString(printBuffer, normalFont, Brushes::Black, leftMargin+countyStrSize.Width, yPos, stringFormat );
    }
    yPos += lineHeight;
    printBuffer = DucoUtils::ConvertString(currentTower.Name());
    const Ring* const theRing = currentTower.FindRing(currentPeal.RingId());
    if (theRing != NULL)
    {
        printBuffer += " (";
        printBuffer += DucoUtils::ConvertString(theRing->TenorWeight());
        if (theRing->TenorKey().length() > 0)
        {
            if (theRing->TenorWeight().length() > 0)
            {
                printBuffer += " ";
            }
            printBuffer += "in ";
            printBuffer += DucoUtils::ConvertString(theRing->TenorKey());
        }
        printBuffer += ")";
    }
    printArgs->Graphics->DrawString(printBuffer, normalFont, Brushes::Black, leftMargin, yPos, stringFormat );
    yPos += lineHeight;
    printBuffer = "On " + DucoUtils::ConvertString(currentPeal.Date(), true);
    printBuffer += " in ";
    printBuffer += DucoUtils::PrintPealTime(currentPeal.Time(), true);
    printArgs->Graphics->DrawString(printBuffer, normalFont, Brushes::Black, leftMargin, yPos, stringFormat );
    yPos += float(lineHeight * 1.5);
    printBuffer = DucoUtils::ConvertString(DucoEngineUtils::ToString(currentPeal.NoOfChanges(), true));
    Duco::Method theMethod;
    if (database->FindMethod(currentPeal.MethodId(), theMethod, false))
    {
        printBuffer += " ";
        printBuffer += DucoUtils::ConvertString(theMethod.FullName(database->Database()));
    }
    printArgs->Graphics->DrawString(printBuffer, boldFont, Brushes::Black, leftMargin, yPos, stringFormat );
    yPos += lineHeight;
    if (currentPeal.ContainsComposer())
    {
        printBuffer = "Composed by: ";
        printBuffer += DucoUtils::ConvertString(currentPeal.Composer());
        printArgs->Graphics->DrawString(printBuffer, normalFont, Brushes::Black, leftMargin, yPos, stringFormat );
        yPos += lineHeight;
    }
    if (currentPeal.ContainsMultiMethods())
    {
        PrintLongString(currentPeal.MultiMethods(), normalFont, printArgs);
    }
    yPos += float(lineHeight * 0.5);

    array<Single>^ ringerTabStops = {80.0f};
    StringFormat^ ringerStringFormat = gcnew StringFormat();
    ringerStringFormat->SetTabStops(firstTab, ringerTabStops);
    for (unsigned int bellNo (1); bellNo <= currentPeal.NoOfBellsRung(database->Database()); ++bellNo)
    {
        printBuffer = "";
        Duco::ObjectId ringerId;
        if (currentPeal.RingerId(bellNo, false, ringerId))
        {
            std::wstring bellNumbers;
            if (currentPeal.Handbell() && currentPeal.HandBellsRung(ringerId, bellNumbers))
            {
                printBuffer += DucoUtils::ConvertString(bellNumbers);
                printBuffer += "\t";
            }
            else
            {
                if (bellNo == 1)
                {
                    printBuffer += "Treble\t";
                }
                else if (bellNo == currentPeal.NoOfBellsRung(database->Database()))
                {
                    printBuffer += "Tenor\t";
                }
                else
                {
                    printBuffer += Convert::ToString(bellNo);
                    printBuffer += "\t";
                }
            }
            Duco::Ringer theRinger;
            if (database->FindRinger(ringerId, theRinger, false))
            {
                printBuffer += DucoUtils::ConvertString(theRinger.FullName(currentPeal.Date(), database->Settings().PrintSurnameFirst()));
            }

            if (currentPeal.ContainsConductor(ringerId))
            {
                printBuffer += " (C)";
            }
            if (theRinger.Id().ValidId())
            {
                printArgs->Graphics->DrawString(printBuffer, normalFont, Brushes::Black, leftMargin, yPos, ringerStringFormat );
                yPos += lineHeight;
            }

            if (currentPeal.RingerId(bellNo, true, ringerId))
            {
                printBuffer = "Strapper\t";
                Ringer theStrapper;
                if (database->FindRinger(ringerId, theStrapper, false))
                {
                    printBuffer += DucoUtils::ConvertString(theStrapper.FullName(currentPeal.Date(), database->Settings().PrintSurnameFirst()));
                }

                if (currentPeal.ContainsConductor(ringerId))
                {
                    printBuffer += " (C)";
                }
                if (theStrapper.Id().ValidId())
                {
                    printArgs->Graphics->DrawString(printBuffer, normalFont, Brushes::Black, leftMargin, yPos, ringerStringFormat );
                    yPos += lineHeight;
                }
            }
        }
    }
    yPos += lineHeight;
    if (currentPeal.ContainsFootnotes())
    {
        PrintLongString(currentPeal.Footnotes(), normalFont, printArgs);
    }

    yPos += lineHeight;

    printBuffer = "Peal number ";
    printBuffer += DucoUtils::ConvertString(currentPeal.Id());
    if (currentPeal.ContainsConductor(database->Settings().DefaultRinger()))
    {
        printBuffer += " (";
        printBuffer += DucoUtils::ConvertString(DucoEngineUtils::ToString(database->NoOfPealsAsConductor(database->Settings().DefaultRinger(), currentPeal.Date()), true));
        printBuffer += ")";
    }
    printBuffer += "; " + database->PerformanceString(false) + "s in this tower: ";
    printBuffer += DucoUtils::ConvertString(DucoEngineUtils::ToString(database->NoOfPealsInTower(currentPeal.TowerId(), currentPeal.Id()), true));
    SizeF statsStrSize = printArgs->Graphics->MeasureString(printBuffer, smallFont);

    float statsXPos = float(printArgs->MarginBounds.Width) - statsStrSize.Width + leftMargin;
    float statsYPos = float(printArgs->MarginBounds.Height + printArgs->MarginBounds.Top) - (2 * lineHeight);

    printArgs->Graphics->DrawString(printBuffer, smallFont, Brushes::Black, statsXPos, statsYPos, ringerStringFormat );
    statsYPos -= lineHeight;

    Boolean foundLink = false;
    if (currentPeal.ValidRingingWorldLink())
    {
        foundLink = true;
        printBuffer = "Ringing world: " + DucoUtils::ConvertString(currentPeal.RingingWorldReference());
        printArgs->Graphics->DrawString(printBuffer, smallFont, Brushes::Black, statsXPos, statsYPos, ringerStringFormat);
        statsYPos -= lineHeight;
    }
    if (currentPeal.ValidBellBoardId())
    {
        foundLink = true;
        printBuffer = "Bell board id: " + DucoUtils::ConvertString(currentPeal.BellBoardId());
        printArgs->Graphics->DrawString(printBuffer, smallFont, Brushes::Black, statsXPos, statsYPos, ringerStringFormat);
        statsYPos -= lineHeight;
    }
    if (!foundLink)
    {
        printBuffer = DucoUtils::ConvertString(currentPeal.FindReference());
        if (printBuffer->Length > 0)
        {
            printArgs->Graphics->DrawString(printBuffer, smallFont, Brushes::Black, statsXPos, statsYPos, ringerStringFormat);
            statsYPos -= lineHeight;
        }
    }

    printArgs->HasMorePages = false;
}
