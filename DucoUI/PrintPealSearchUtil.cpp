#include "PrintPealSearchUtil.h"

using namespace System;
using namespace System::Drawing;
using namespace System::Collections::Generic;
using namespace System::Windows::Forms;
using namespace DucoUI;

PrintPealSearchUtil::PrintPealSearchUtil(DucoUI::DatabaseManager^ theDatabase, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
:	PrintUtilBase(theDatabase, 6, printArgs, false), tabStops(nullptr)
{

}

void
PrintPealSearchUtil::SetObjects(System::Windows::Forms::DataGridView^ theDataGridView)
{
    dataGridView = theDataGridView;
    printingResultNumber = 0;
}

System::Void
PrintPealSearchUtil::printObject(System::Drawing::Printing::PrintPageEventArgs^ args, System::Drawing::Printing::PrinterSettings^ printSettings)
{
    StringFormat^ stringFormat = gcnew StringFormat;
    Boolean stringTrimmed;
    addDucoFooter(normalFont, stringFormat, args);
    array<UInt32^>^ columsToPrint = {1u, 2u, 3u, 4u, 5u, 6u, 9u, 10u};
    if (tabStops == nullptr)
    {
        tabStops = gcnew List<Single>();
        while (printingResultNumber < dataGridView->Rows->Count)
        {
            DataGridViewRow^ currentRow = dataGridView->Rows[printingResultNumber];
            UInt32 columnCount = 0;
            for each (UInt32 ^ columnNumber in columsToPrint)
            {
                Single^ newSize = (args->Graphics->MeasureString(currentRow->Cells[*columnNumber]->Value->ToString(), normalFont).Width + 20);
                if (tabStops->Count <= columnCount)
                {
                    tabStops->Add(*newSize);
                }
                else
                {
                    if (*newSize > tabStops[columnCount])
                    {
                        tabStops->RemoveAt(columnCount);
                        tabStops->Insert(columnCount, *newSize);
                    }
                }
                ++columnCount;
            }
            printingResultNumber++;
        }
        printingResultNumber = 0;
    }
    String^ printBuffer = "Id\tAssociation\tDate\tChanges\tMethod\tTower\tConductor\tTime";
    stringFormat->SetTabStops(0.0f, tabStops->ToArray());
    args->Graphics->DrawString(printBuffer, boldFont, Brushes::Black, leftMargin, yPos, stringFormat);
    while (noOfLines > 0 && printingResultNumber < dataGridView->Rows->Count)
    {
        yPos += lineHeight;
        printBuffer = "";
        DataGridViewRow^ currentRow = dataGridView->Rows[printingResultNumber];
        UInt32 columnCount = 0;
        for each (UInt32 ^ columnNumber in columsToPrint)
        {
            printBuffer += TrimString(currentRow->Cells[*columnNumber], stringFormat, normalFont, tabStops[columnCount], args, "\t", stringTrimmed);
            ++columnCount;
        }
        /*printBuffer += currentRow->Cells[1]->Value->ToString() + "\t";
        printBuffer += TrimString(currentRow->Cells[2], stringFormat, normalFont, tabStops[1], args, "\t", stringTrimmed);
        printBuffer += currentRow->Cells[3]->Value->ToString() + "\t";
        printBuffer += currentRow->Cells[4]->Value->ToString() + "\t";
        printBuffer += TrimString(currentRow->Cells[5], stringFormat, normalFont, tabStops[4], args, "\t", stringTrimmed);
        printBuffer += TrimString(currentRow->Cells[6], stringFormat, normalFont, tabStops[5], args, "\t", stringTrimmed);
        printBuffer += currentRow->Cells[7]->Value->ToString() + "\t";*/

        args->Graphics->DrawString(printBuffer, normalFont, Brushes::Black, leftMargin, yPos, stringFormat);

        noOfLines--;
        printingResultNumber++;
    }

    if ( printingResultNumber < dataGridView->Rows->Count && dataGridView->Rows->Count > 0)
    {
        ResetPosition(args);
        args->HasMorePages = true;
    }
    else
    {
        printingResultNumber = 0;
    }
}

