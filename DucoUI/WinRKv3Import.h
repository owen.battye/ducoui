#pragma once

#include "DatabaseImportUtil.h"

namespace DucoUI
{
    ref class DatabaseManager;

ref class WinRKv3Import : public DatabaseImportUtil
{
public:
    WinRKv3Import(DucoUI::DatabaseManager^ theDatabase, DucoUI::WinRkImportProgressCallback^ newCallback, System::String^ theFileName);
    ~WinRKv3Import();
    virtual System::String^ FileType() override;
    virtual double TotalNoOfStages() override;
    virtual double StageNumber(DucoUI::WinRkImportProgressCallback::TImportStage currentStage) override;

protected:
    virtual void Import(System::ComponentModel::BackgroundWorker^ worker, System::ComponentModel::DoWorkEventArgs^  e) override;

    void ImportPeals(System::Data::Odbc::OdbcCommand^ command,
                     std::map<Duco::ObjectId, Duco::ObjectId>& oldAKAs, 
                     const std::map<unsigned int, std::wstring>& multiMethods,
                     const std::map<unsigned int, unsigned int>& oldRingIdsToTowerIds,
                     const std::map<unsigned int, Duco::ObjectId>& oldRingIdsToNewRingIds,
                     System::ComponentModel::BackgroundWorker^ worker);
    void ImportRingers(System::Data::Odbc::OdbcCommand^ command,
                     System::ComponentModel::BackgroundWorker^ worker);
    void ImportRingerAkas(System::Data::Odbc::OdbcCommand^ command,
                     std::map<Duco::ObjectId, Duco::ObjectId>& oldAKAs,
                     std::map<unsigned int, unsigned int>& oldAKAsWithoutDates,
                     System::ComponentModel::BackgroundWorker^ worker);
    void ImportTowers(System::Data::Odbc::OdbcCommand^ command,
                     System::ComponentModel::BackgroundWorker^ worker);
    void ImportRings(System::Data::Odbc::OdbcCommand^ command,
                     std::map<unsigned int, unsigned int>& oldRingIdsToTowerIds,
                     std::map<unsigned int, Duco::ObjectId>& oldRingIdsToNewRingIds,
                     System::ComponentModel::BackgroundWorker^ worker);
    void ImportTenors(System::Data::Odbc::OdbcCommand^ command,
                     std::map<unsigned int,std::wstring>& tenors,
                     System::ComponentModel::BackgroundWorker^ worker);
    void ImportMethods(System::Data::Odbc::OdbcCommand^ command,
                     const std::map<unsigned int, std::wstring>& extraOrders,
                     std::map<unsigned int, unsigned int>& oldOrderNamesToNew,
                     System::ComponentModel::BackgroundWorker^ worker);
    void ImportOrderNames(System::Data::Odbc::OdbcCommand^ command,
                     std::map<unsigned int, std::wstring>& extraOrders,
                     std::map<unsigned int, unsigned int>& oldOrderNamesToNew,
                     System::ComponentModel::BackgroundWorker^ worker);

    void RepairRingerAkasWithoutDates(const std::map<unsigned int, unsigned int>& oldAKAsWithoutDates,
                     System::ComponentModel::BackgroundWorker^ worker);
    bool RepairRingerAkasWithoutDates(unsigned int ringerId, unsigned int ringerIdToMergeAndDelete, const Duco::PerformanceDate& dateChanged);
    bool FindRingingWorldPublicationId(System::Data::Odbc::OdbcCommand^ command);
    Duco::ObjectId FindRingOrCreate(const Duco::ObjectId& towerId, const Duco::ObjectId& ringId, unsigned int noOfBellsRung);

protected:
    int ringingWorldPublicationId;
};

}