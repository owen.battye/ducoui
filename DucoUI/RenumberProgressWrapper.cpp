#include "RenumberProgressWrapper.h"

using namespace DucoUI;

RenumberProgressWrapper::RenumberProgressWrapper(DucoUI::RenumberProgressCallbackUI^ theCallback)
	: uiCallback(theCallback)
{

}

void
RenumberProgressWrapper::RenumberInitialised(RenumberProgressCallback::TRenumberStage newStage)
{
	uiCallback->RenumberInitialised(newStage);
}

void
RenumberProgressWrapper::RenumberStep(size_t objectId, size_t total)
{
	uiCallback->RenumberStep(objectId, total);
}

void
RenumberProgressWrapper::RenumberComplete()
{
	uiCallback->RenumberComplete();
}
