#pragma once

#include "PrintUtilBase.h"

namespace Duco
{
    class Method;
}

namespace DucoUI
{
public ref class PrintMethodUtil : public PrintUtilBase
{
public:
    PrintMethodUtil(DucoUI::DatabaseManager^ theDatabase, System::Drawing::Printing::PrintPageEventArgs^ printArgs);
    void SetObjects(const Duco::Method* theMethod, System::Drawing::Image^ theImage);
    virtual System::Void printObject(System::Drawing::Printing::PrintPageEventArgs^ args, System::Drawing::Printing::PrinterSettings^ printSettings) override;
    System::Void PrintMethod(System::Drawing::Rectangle^ marginBounds, System::Drawing::Graphics^ grphcs);

protected:
    const Duco::Method*         currentMethod;
    System::Drawing::Image^     methodImage;
};

}