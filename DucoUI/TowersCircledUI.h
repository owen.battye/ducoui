#pragma once
namespace DucoUI
{
    public ref class TowersCircledUI : public System::Windows::Forms::Form, public DucoUI::RingingDatabaseObserver
    {
    public:
        TowersCircledUI(DucoUI::DatabaseManager^ theDatabase);

        // from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

    protected:
        ~TowersCircledUI();
        !TowersCircledUI();

        System::Void CheckEnoughColumnsExist(unsigned int noOfBells);
        System::Void GeneratePealData(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void TowersCircledUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void backgroundWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void backgroundWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);
        System::Void circledTowerData_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);
        System::Void circledTowerData_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);
        System::Windows::Forms::DataGridViewRow^ GenerateNextTower(const Duco::StatisticFilters& filters, const Duco::Tower& tower, const Duco::PealLengthInfo& pealCounts);

        System::Void printBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void filtersBtn_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void printData(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args);

        System::Void StartGenerator();
        System::Void ResetAllSortGlyphs();

    protected: 
        DucoUI::DatabaseManager^                            database;
        Duco::StatisticFilters*                             filters;
        unsigned int                                        columnCount;
        unsigned int                                        totalNumberOfTowersCircled;
        bool                                                restartGenerator;
        bool                                                loading;
        GenericTablePrintUtil^                              printUtil;

    private:
        System::ComponentModel::Container^                  components;
        System::ComponentModel::BackgroundWorker^           backgroundWorker;

        System::Windows::Forms::ToolStripStatusLabel^       towersCircled;
        System::Windows::Forms::ToolStripProgressBar^       progressBar;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ TowerId;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ TowerColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ CircledColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ Peals;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ dateColumn;
           System::Windows::Forms::DataGridView^ circledTowerData;






        void InitializeComponent()
        {
            System::Windows::Forms::StatusStrip^ statusStrip1;
            System::Windows::Forms::ToolStripButton^ printBtn;
            System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(TowersCircledUI::typeid));
            System::Windows::Forms::ToolStrip^ toolStrip1;
            System::Windows::Forms::ToolStripButton^ filtersBtn;
            this->progressBar = (gcnew System::Windows::Forms::ToolStripProgressBar());
            this->towersCircled = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->circledTowerData = (gcnew System::Windows::Forms::DataGridView());
            this->backgroundWorker = (gcnew System::ComponentModel::BackgroundWorker());
            this->TowerId = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->TowerColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->CircledColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->Peals = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->dateColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            printBtn = (gcnew System::Windows::Forms::ToolStripButton());
            toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
            filtersBtn = (gcnew System::Windows::Forms::ToolStripButton());
            statusStrip1->SuspendLayout();
            toolStrip1->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->circledTowerData))->BeginInit();
            this->SuspendLayout();
            // 
            // statusStrip1
            // 
            statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) { this->progressBar, this->towersCircled });
            statusStrip1->Location = System::Drawing::Point(0, 396);
            statusStrip1->Name = L"statusStrip1";
            statusStrip1->Size = System::Drawing::Size(1202, 22);
            statusStrip1->TabIndex = 2;
            statusStrip1->Text = L"statusStrip1";
            // 
            // progressBar
            // 
            this->progressBar->Name = L"progressBar";
            this->progressBar->Size = System::Drawing::Size(100, 16);
            // 
            // towersCircled
            // 
            this->towersCircled->Name = L"towersCircled";
            this->towersCircled->Size = System::Drawing::Size(1085, 17);
            this->towersCircled->Spring = true;
            this->towersCircled->Text = L"Towers circled: \?";
            this->towersCircled->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // printBtn
            // 
            printBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
            printBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"printBtn.Image")));
            printBtn->ImageTransparentColor = System::Drawing::Color::Magenta;
            printBtn->Name = L"printBtn";
            printBtn->Size = System::Drawing::Size(36, 22);
            printBtn->Text = L"&Print";
            printBtn->Click += gcnew System::EventHandler(this, &TowersCircledUI::printBtn_Click);
            // 
            // toolStrip1
            // 
            toolStrip1->GripStyle = System::Windows::Forms::ToolStripGripStyle::Hidden;
            toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) { filtersBtn, printBtn });
            toolStrip1->Location = System::Drawing::Point(0, 0);
            toolStrip1->Name = L"toolStrip1";
            toolStrip1->Size = System::Drawing::Size(1202, 25);
            toolStrip1->TabIndex = 5;
            toolStrip1->Text = L"toolStrip1";
            // 
            // filtersBtn
            // 
            filtersBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
            filtersBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"filtersBtn.Image")));
            filtersBtn->ImageTransparentColor = System::Drawing::Color::Magenta;
            filtersBtn->Name = L"filtersBtn";
            filtersBtn->Size = System::Drawing::Size(42, 22);
            filtersBtn->Text = L"&Filters";
            filtersBtn->Click += gcnew System::EventHandler(this, &TowersCircledUI::filtersBtn_Click);
            // 
            // circledTowerData
            // 
            this->circledTowerData->AllowUserToAddRows = false;
            this->circledTowerData->AllowUserToDeleteRows = false;
            this->circledTowerData->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::AllCells;
            this->circledTowerData->AutoSizeRowsMode = System::Windows::Forms::DataGridViewAutoSizeRowsMode::AllCells;
            this->circledTowerData->ClipboardCopyMode = System::Windows::Forms::DataGridViewClipboardCopyMode::EnableAlwaysIncludeHeaderText;
            this->circledTowerData->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->circledTowerData->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(5) {
                this->TowerId,
                    this->TowerColumn, this->CircledColumn, this->Peals, this->dateColumn
            });
            this->circledTowerData->Dock = System::Windows::Forms::DockStyle::Fill;
            this->circledTowerData->Location = System::Drawing::Point(0, 25);
            this->circledTowerData->Name = L"circledTowerData";
            this->circledTowerData->ReadOnly = true;
            this->circledTowerData->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToDisplayedHeaders;
            this->circledTowerData->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::CellSelect;
            this->circledTowerData->Size = System::Drawing::Size(1202, 393);
            this->circledTowerData->TabIndex = 0;
            this->circledTowerData->CellContentDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &TowersCircledUI::circledTowerData_CellContentDoubleClick);
            this->circledTowerData->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &TowersCircledUI::circledTowerData_ColumnHeaderMouseClick);
            // 
            // backgroundWorker
            // 
            this->backgroundWorker->WorkerReportsProgress = true;
            this->backgroundWorker->WorkerSupportsCancellation = true;
            this->backgroundWorker->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &TowersCircledUI::GeneratePealData);
            this->backgroundWorker->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &TowersCircledUI::backgroundWorker_ProgressChanged);
            this->backgroundWorker->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &TowersCircledUI::backgroundWorker_RunWorkerCompleted);
            // 
            // TowerId
            // 
            this->TowerId->Frozen = true;
            this->TowerId->HeaderText = L"Id";
            this->TowerId->Name = L"TowerId";
            this->TowerId->ReadOnly = true;
            this->TowerId->Visible = false;
            this->TowerId->Width = 41;
            // 
            // TowerColumn
            // 
            this->TowerColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
            this->TowerColumn->HeaderText = L"Tower";
            this->TowerColumn->Name = L"TowerColumn";
            this->TowerColumn->ReadOnly = true;
            // 
            // CircledColumn
            // 
            this->CircledColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
            this->CircledColumn->HeaderText = L"Circled";
            this->CircledColumn->Name = L"CircledColumn";
            this->CircledColumn->ReadOnly = true;
            this->CircledColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->CircledColumn->ToolTipText = L"Number of times this tower circled";
            this->CircledColumn->Width = 30;
            // 
            // Peals
            // 
            this->Peals->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
            this->Peals->HeaderText = L"Peals";
            this->Peals->Name = L"Peals";
            this->Peals->ReadOnly = true;
            this->Peals->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->Peals->ToolTipText = L"Number of peals in each tower";
            this->Peals->Width = 30;
            // 
            // dateColumn
            // 
            this->dateColumn->HeaderText = L"Date";
            this->dateColumn->Name = L"dateColumn";
            this->dateColumn->ReadOnly = true;
            this->dateColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->dateColumn->ToolTipText = L"Date of first circle";
            this->dateColumn->Width = 55;
            // 
            // TowersCircledUI
            // 
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Inherit;
            this->ClientSize = System::Drawing::Size(1202, 418);
            this->Controls->Add(statusStrip1);
            this->Controls->Add(this->circledTowerData);
            this->Controls->Add(toolStrip1);
            this->MinimumSize = System::Drawing::Size(431, 242);
            this->Name = L"TowersCircledUI";
            this->Text = L"Tower circling";
            this->Load += gcnew System::EventHandler(this, &TowersCircledUI::TowersCircledUI_Load);
            statusStrip1->ResumeLayout(false);
            statusStrip1->PerformLayout();
            toolStrip1->ResumeLayout(false);
            toolStrip1->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->circledTowerData))->EndInit();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
    };
}
