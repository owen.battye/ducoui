#include "ProgressWrapper.h"

using namespace DucoUI;

ProgressCallbackWrapper::ProgressCallbackWrapper(DucoUI::ProgressCallbackUI^ theUiCallback)
: uiCallback(theUiCallback)
{

}

void
ProgressCallbackWrapper::Initialised()
{
	uiCallback->Initialised();
}

void
ProgressCallbackWrapper::Step(int progressPercent)
{
	uiCallback->Step(progressPercent);
}

void
ProgressCallbackWrapper::Complete(bool error)
{
	uiCallback->Complete();
}
