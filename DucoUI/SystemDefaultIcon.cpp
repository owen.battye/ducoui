#include "SystemDefaultIcon.h"
#include "WindowsSettings.h"
#include "DucoUtils.h"

using namespace DucoUI;

System::Drawing::Icon^
SystemDefaultIcon::DefaultSystemIcon()
{
    if (defaultIcon == nullptr)
    {
        if (System::IO::File::Exists("app.ico"))
        {
            defaultIcon = gcnew System::Drawing::Icon("app.ico");
        }
    }
    return defaultIcon;
}

System::Drawing::Icon^
SystemDefaultIcon::DefaultSystemIcon(DucoUI::WindowsSettings^ settings)
{
    if (!System::IO::File::Exists("app.ico"))
    {
        System::IO::Directory::SetCurrentDirectory(DucoUI::DucoUtils::ConvertString(settings->InstallationDir()));
    }
    return DefaultSystemIcon();
}
