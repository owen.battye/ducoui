#pragma once

namespace DucoUI
{
    public ref class FiltersUI : public System::Windows::Forms::Form
    {
    public:
        FiltersUI(Duco::StatisticFilters& newFilters, DucoUI::DatabaseManager^ theDatabase);
        System::Void RequireConductor();

    protected:
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        ~FiltersUI();
        System::Void Populate();
        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void cancelBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void clearBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void associationEditor_SelectionChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void ringerSelector_SelectionChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void conductorSelector_SelectionChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void towerSelector_SelectionChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void methodSelector_SelectionChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void typeSelector_SelectionChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void stageSelector_SelectionChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void startDate_ValueChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void endDate_ValueChanged(System::Object^ sender, System::EventArgs^ e);

        System::Void ringerSelector_Validated(System::Object^ sender, System::EventArgs^ e);
        System::Void RingerChangedToUnknown(System::String^ newRingerName);

        System::Void thisYearButton_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void lastYearButton_Click(System::Object^ sender, System::EventArgs^ e);

        Duco::StatisticFilters& filters;
        DucoUI::RingerDisplayCache^ ringerCache;
        DucoUI::RingerDisplayCache^ conductorCache;
        System::Collections::Generic::List<System::Int16>^  allTowerIds;
        System::Collections::Generic::List<System::Int16>^  allMethodIds;
        System::Collections::Generic::List<System::Int16>^  allAssocaitionIds;

        System::Windows::Forms::CheckBox^  includeWithdrawn;
        System::Windows::Forms::CheckBox^  associationCheckbox;
        System::Windows::Forms::ComboBox^  associationEditor;
        System::Windows::Forms::CheckBox^  ringerCheckbox;
        System::Windows::Forms::ComboBox^  ringerSelector;
        System::Windows::Forms::CheckBox^  conductorCheckbox;
        System::Windows::Forms::ComboBox^  conductorSelector;
        System::Windows::Forms::ComboBox^  stageSelector;
        System::Windows::Forms::CheckBox^  stageCheckbox;
        System::Windows::Forms::ComboBox^  methodSelector;
        System::Windows::Forms::CheckBox^  methodCheckbox;
        System::Windows::Forms::ComboBox^  typeSelector;
        System::Windows::Forms::CheckBox^  typeCheckbox;

    protected: System::Windows::Forms::CheckBox^ includeLinked;
    protected: System::Windows::Forms::ComboBox^ towerSelector;
    protected: System::Windows::Forms::CheckBox^ towerCheckbox;
    protected: System::Windows::Forms::CheckBox^ towerBells;
    protected: System::Windows::Forms::CheckBox^ handBells;
    protected: System::Windows::Forms::CheckBox^ excludeValid;
    protected: System::Windows::Forms::Button^ closeBtn;
    protected: System::Windows::Forms::CheckBox^ linkedRingers;

    private: System::Windows::Forms::Button^ lastYearBtn;
    private: System::Windows::Forms::CheckBox^ endCheckBox;
    private: System::Windows::Forms::CheckBox^ startCheckBox;
    private: System::Windows::Forms::DateTimePicker^ endDate;
    private: System::Windows::Forms::DateTimePicker^ startDate;
    private: System::Windows::Forms::CheckBox^ excludeInvalidTimeOrChanges;
    private: System::Windows::Forms::Button^ thisYearBtn;
           DucoUI::DatabaseManager^ database;
           System::Boolean                    requireConductor;
           System::Boolean                    populating;

    protected:
        /// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
            System::Windows::Forms::GroupBox^ towerGroupBox;
            System::Windows::Forms::GroupBox^ methodGroupBox;
            System::Windows::Forms::GroupBox^ peopleGroupBox;
            System::Windows::Forms::Button^ clearBtn;
            System::Windows::Forms::Button^ cancelBtn;
            System::Windows::Forms::GroupBox^ associationGroupBox;
            System::Windows::Forms::GroupBox^ otherGroup;
            System::Windows::Forms::GroupBox^ dateGroup;
            this->includeLinked = (gcnew System::Windows::Forms::CheckBox());
            this->towerSelector = (gcnew System::Windows::Forms::ComboBox());
            this->towerCheckbox = (gcnew System::Windows::Forms::CheckBox());
            this->towerBells = (gcnew System::Windows::Forms::CheckBox());
            this->handBells = (gcnew System::Windows::Forms::CheckBox());
            this->stageSelector = (gcnew System::Windows::Forms::ComboBox());
            this->typeSelector = (gcnew System::Windows::Forms::ComboBox());
            this->methodSelector = (gcnew System::Windows::Forms::ComboBox());
            this->typeCheckbox = (gcnew System::Windows::Forms::CheckBox());
            this->stageCheckbox = (gcnew System::Windows::Forms::CheckBox());
            this->methodCheckbox = (gcnew System::Windows::Forms::CheckBox());
            this->conductorCheckbox = (gcnew System::Windows::Forms::CheckBox());
            this->conductorSelector = (gcnew System::Windows::Forms::ComboBox());
            this->ringerSelector = (gcnew System::Windows::Forms::ComboBox());
            this->ringerCheckbox = (gcnew System::Windows::Forms::CheckBox());
            this->closeBtn = (gcnew System::Windows::Forms::Button());
            this->associationEditor = (gcnew System::Windows::Forms::ComboBox());
            this->associationCheckbox = (gcnew System::Windows::Forms::CheckBox());
            this->linkedRingers = (gcnew System::Windows::Forms::CheckBox());
            this->excludeInvalidTimeOrChanges = (gcnew System::Windows::Forms::CheckBox());
            this->excludeValid = (gcnew System::Windows::Forms::CheckBox());
            this->includeWithdrawn = (gcnew System::Windows::Forms::CheckBox());
            this->thisYearBtn = (gcnew System::Windows::Forms::Button());
            this->lastYearBtn = (gcnew System::Windows::Forms::Button());
            this->endCheckBox = (gcnew System::Windows::Forms::CheckBox());
            this->startCheckBox = (gcnew System::Windows::Forms::CheckBox());
            this->endDate = (gcnew System::Windows::Forms::DateTimePicker());
            this->startDate = (gcnew System::Windows::Forms::DateTimePicker());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            towerGroupBox = (gcnew System::Windows::Forms::GroupBox());
            methodGroupBox = (gcnew System::Windows::Forms::GroupBox());
            peopleGroupBox = (gcnew System::Windows::Forms::GroupBox());
            clearBtn = (gcnew System::Windows::Forms::Button());
            cancelBtn = (gcnew System::Windows::Forms::Button());
            associationGroupBox = (gcnew System::Windows::Forms::GroupBox());
            otherGroup = (gcnew System::Windows::Forms::GroupBox());
            dateGroup = (gcnew System::Windows::Forms::GroupBox());
            tableLayoutPanel1->SuspendLayout();
            towerGroupBox->SuspendLayout();
            methodGroupBox->SuspendLayout();
            peopleGroupBox->SuspendLayout();
            associationGroupBox->SuspendLayout();
            otherGroup->SuspendLayout();
            dateGroup->SuspendLayout();
            this->SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 3;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->Controls->Add(towerGroupBox, 1, 2);
            tableLayoutPanel1->Controls->Add(methodGroupBox, 0, 3);
            tableLayoutPanel1->Controls->Add(peopleGroupBox, 0, 4);
            tableLayoutPanel1->Controls->Add(clearBtn, 0, 6);
            tableLayoutPanel1->Controls->Add(cancelBtn, 2, 6);
            tableLayoutPanel1->Controls->Add(this->closeBtn, 1, 6);
            tableLayoutPanel1->Controls->Add(associationGroupBox, 0, 1);
            tableLayoutPanel1->Controls->Add(otherGroup, 0, 5);
            tableLayoutPanel1->Controls->Add(dateGroup, 0, 0);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 6;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->Size = System::Drawing::Size(394, 560);
            tableLayoutPanel1->TabIndex = 0;
            // 
            // towerGroupBox
            // 
            towerGroupBox->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            tableLayoutPanel1->SetColumnSpan(towerGroupBox, 3);
            towerGroupBox->Controls->Add(this->includeLinked);
            towerGroupBox->Controls->Add(this->towerSelector);
            towerGroupBox->Controls->Add(this->towerCheckbox);
            towerGroupBox->Controls->Add(this->towerBells);
            towerGroupBox->Controls->Add(this->handBells);
            towerGroupBox->Dock = System::Windows::Forms::DockStyle::Fill;
            towerGroupBox->Location = System::Drawing::Point(3, 149);
            towerGroupBox->Name = L"towerGroupBox";
            towerGroupBox->Size = System::Drawing::Size(388, 100);
            towerGroupBox->TabIndex = 2;
            towerGroupBox->TabStop = false;
            towerGroupBox->Text = L"Tower";
            // 
            // includeLinked
            // 
            this->includeLinked->AutoSize = true;
            this->includeLinked->Location = System::Drawing::Point(6, 46);
            this->includeLinked->Name = L"includeLinked";
            this->includeLinked->Padding = System::Windows::Forms::Padding(0, 4, 0, 0);
            this->includeLinked->Size = System::Drawing::Size(126, 21);
            this->includeLinked->TabIndex = 3;
            this->includeLinked->Text = L"Include linked towers";
            this->includeLinked->UseVisualStyleBackColor = true;
            // 
            // towerSelector
            // 
            this->towerSelector->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
            this->towerSelector->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->towerSelector->FormattingEnabled = true;
            this->towerSelector->Location = System::Drawing::Point(6, 19);
            this->towerSelector->Name = L"towerSelector";
            this->towerSelector->Size = System::Drawing::Size(296, 21);
            this->towerSelector->TabIndex = 1;
            this->towerSelector->SelectedIndexChanged += gcnew System::EventHandler(this, &FiltersUI::towerSelector_SelectionChanged);
            this->towerSelector->TextUpdate += gcnew System::EventHandler(this, &FiltersUI::towerSelector_SelectionChanged);
            // 
            // towerCheckbox
            // 
            this->towerCheckbox->AutoSize = true;
            this->towerCheckbox->Location = System::Drawing::Point(308, 19);
            this->towerCheckbox->Name = L"towerCheckbox";
            this->towerCheckbox->Padding = System::Windows::Forms::Padding(0, 4, 0, 0);
            this->towerCheckbox->Size = System::Drawing::Size(59, 21);
            this->towerCheckbox->TabIndex = 2;
            this->towerCheckbox->Text = L"Enable";
            this->towerCheckbox->UseVisualStyleBackColor = true;
            // 
            // towerBells
            // 
            this->towerBells->AutoSize = true;
            this->towerBells->Location = System::Drawing::Point(6, 73);
            this->towerBells->Name = L"towerBells";
            this->towerBells->Padding = System::Windows::Forms::Padding(0, 4, 0, 0);
            this->towerBells->Size = System::Drawing::Size(80, 21);
            this->towerBells->TabIndex = 4;
            this->towerBells->Text = L"Tower bells";
            this->towerBells->UseVisualStyleBackColor = true;
            // 
            // handBells
            // 
            this->handBells->AutoSize = true;
            this->handBells->Location = System::Drawing::Point(92, 73);
            this->handBells->Name = L"handBells";
            this->handBells->Padding = System::Windows::Forms::Padding(0, 4, 0, 0);
            this->handBells->Size = System::Drawing::Size(76, 21);
            this->handBells->TabIndex = 5;
            this->handBells->Text = L"Hand bells";
            this->handBells->UseVisualStyleBackColor = true;
            // 
            // methodGroupBox
            // 
            methodGroupBox->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            tableLayoutPanel1->SetColumnSpan(methodGroupBox, 3);
            methodGroupBox->Controls->Add(this->stageSelector);
            methodGroupBox->Controls->Add(this->typeSelector);
            methodGroupBox->Controls->Add(this->methodSelector);
            methodGroupBox->Controls->Add(this->typeCheckbox);
            methodGroupBox->Controls->Add(this->stageCheckbox);
            methodGroupBox->Controls->Add(this->methodCheckbox);
            methodGroupBox->Dock = System::Windows::Forms::DockStyle::Fill;
            methodGroupBox->Location = System::Drawing::Point(3, 255);
            methodGroupBox->Name = L"methodGroupBox";
            methodGroupBox->Size = System::Drawing::Size(388, 109);
            methodGroupBox->TabIndex = 3;
            methodGroupBox->TabStop = false;
            methodGroupBox->Text = L"Method";
            // 
            // stageSelector
            // 
            this->stageSelector->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
            this->stageSelector->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->stageSelector->FormattingEnabled = true;
            this->stageSelector->Location = System::Drawing::Point(6, 73);
            this->stageSelector->Name = L"stageSelector";
            this->stageSelector->Size = System::Drawing::Size(296, 21);
            this->stageSelector->TabIndex = 5;
            this->stageSelector->SelectedIndexChanged += gcnew System::EventHandler(this, &FiltersUI::stageSelector_SelectionChanged);
            this->stageSelector->TextUpdate += gcnew System::EventHandler(this, &FiltersUI::stageSelector_SelectionChanged);
            // 
            // typeSelector
            // 
            this->typeSelector->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
            this->typeSelector->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->typeSelector->FormattingEnabled = true;
            this->typeSelector->ItemHeight = 13;
            this->typeSelector->Location = System::Drawing::Point(6, 46);
            this->typeSelector->Name = L"typeSelector";
            this->typeSelector->Size = System::Drawing::Size(296, 21);
            this->typeSelector->TabIndex = 3;
            this->typeSelector->SelectedIndexChanged += gcnew System::EventHandler(this, &FiltersUI::typeSelector_SelectionChanged);
            this->typeSelector->TextUpdate += gcnew System::EventHandler(this, &FiltersUI::typeSelector_SelectionChanged);
            // 
            // methodSelector
            // 
            this->methodSelector->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
            this->methodSelector->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->methodSelector->FormattingEnabled = true;
            this->methodSelector->Location = System::Drawing::Point(6, 19);
            this->methodSelector->Name = L"methodSelector";
            this->methodSelector->Size = System::Drawing::Size(296, 21);
            this->methodSelector->TabIndex = 1;
            this->methodSelector->SelectedIndexChanged += gcnew System::EventHandler(this, &FiltersUI::methodSelector_SelectionChanged);
            this->methodSelector->TextUpdate += gcnew System::EventHandler(this, &FiltersUI::methodSelector_SelectionChanged);
            // 
            // typeCheckbox
            // 
            this->typeCheckbox->AutoSize = true;
            this->typeCheckbox->Location = System::Drawing::Point(308, 46);
            this->typeCheckbox->Name = L"typeCheckbox";
            this->typeCheckbox->Padding = System::Windows::Forms::Padding(0, 4, 0, 0);
            this->typeCheckbox->Size = System::Drawing::Size(50, 21);
            this->typeCheckbox->TabIndex = 4;
            this->typeCheckbox->Text = L"Type";
            this->typeCheckbox->UseVisualStyleBackColor = true;
            // 
            // stageCheckbox
            // 
            this->stageCheckbox->AutoSize = true;
            this->stageCheckbox->Location = System::Drawing::Point(308, 73);
            this->stageCheckbox->Name = L"stageCheckbox";
            this->stageCheckbox->Padding = System::Windows::Forms::Padding(0, 4, 0, 0);
            this->stageCheckbox->Size = System::Drawing::Size(54, 21);
            this->stageCheckbox->TabIndex = 6;
            this->stageCheckbox->Text = L"Stage";
            this->stageCheckbox->UseVisualStyleBackColor = true;
            // 
            // methodCheckbox
            // 
            this->methodCheckbox->AutoSize = true;
            this->methodCheckbox->Location = System::Drawing::Point(308, 19);
            this->methodCheckbox->Name = L"methodCheckbox";
            this->methodCheckbox->Padding = System::Windows::Forms::Padding(0, 4, 0, 0);
            this->methodCheckbox->Size = System::Drawing::Size(62, 21);
            this->methodCheckbox->TabIndex = 2;
            this->methodCheckbox->Text = L"Method";
            this->methodCheckbox->UseVisualStyleBackColor = true;
            // 
            // peopleGroupBox
            // 
            peopleGroupBox->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            tableLayoutPanel1->SetColumnSpan(peopleGroupBox, 3);
            peopleGroupBox->Controls->Add(this->conductorCheckbox);
            peopleGroupBox->Controls->Add(this->conductorSelector);
            peopleGroupBox->Controls->Add(this->ringerSelector);
            peopleGroupBox->Controls->Add(this->ringerCheckbox);
            peopleGroupBox->Dock = System::Windows::Forms::DockStyle::Fill;
            peopleGroupBox->Location = System::Drawing::Point(3, 370);
            peopleGroupBox->Name = L"peopleGroupBox";
            peopleGroupBox->Size = System::Drawing::Size(388, 81);
            peopleGroupBox->TabIndex = 4;
            peopleGroupBox->TabStop = false;
            peopleGroupBox->Text = L"People";
            // 
            // conductorCheckbox
            // 
            this->conductorCheckbox->AutoSize = true;
            this->conductorCheckbox->Location = System::Drawing::Point(308, 46);
            this->conductorCheckbox->Name = L"conductorCheckbox";
            this->conductorCheckbox->Padding = System::Windows::Forms::Padding(0, 4, 0, 0);
            this->conductorCheckbox->Size = System::Drawing::Size(75, 21);
            this->conductorCheckbox->TabIndex = 4;
            this->conductorCheckbox->Text = L"Conductor";
            this->conductorCheckbox->UseVisualStyleBackColor = true;
            // 
            // conductorSelector
            // 
            this->conductorSelector->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
            this->conductorSelector->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->conductorSelector->FormattingEnabled = true;
            this->conductorSelector->Location = System::Drawing::Point(6, 46);
            this->conductorSelector->Name = L"conductorSelector";
            this->conductorSelector->Size = System::Drawing::Size(296, 21);
            this->conductorSelector->TabIndex = 3;
            this->conductorSelector->SelectedIndexChanged += gcnew System::EventHandler(this, &FiltersUI::conductorSelector_SelectionChanged);
            this->conductorSelector->TextUpdate += gcnew System::EventHandler(this, &FiltersUI::conductorSelector_SelectionChanged);
            // 
            // ringerSelector
            // 
            this->ringerSelector->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
            this->ringerSelector->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->ringerSelector->FormattingEnabled = true;
            this->ringerSelector->Location = System::Drawing::Point(6, 19);
            this->ringerSelector->Name = L"ringerSelector";
            this->ringerSelector->Size = System::Drawing::Size(296, 21);
            this->ringerSelector->TabIndex = 1;
            this->ringerSelector->SelectedIndexChanged += gcnew System::EventHandler(this, &FiltersUI::ringerSelector_SelectionChanged);
            this->ringerSelector->TextUpdate += gcnew System::EventHandler(this, &FiltersUI::ringerSelector_SelectionChanged);
            this->ringerSelector->Validated += gcnew System::EventHandler(this, &FiltersUI::ringerSelector_Validated);
            // 
            // ringerCheckbox
            // 
            this->ringerCheckbox->AutoSize = true;
            this->ringerCheckbox->Location = System::Drawing::Point(308, 19);
            this->ringerCheckbox->Name = L"ringerCheckbox";
            this->ringerCheckbox->Padding = System::Windows::Forms::Padding(0, 4, 0, 0);
            this->ringerCheckbox->Size = System::Drawing::Size(57, 21);
            this->ringerCheckbox->TabIndex = 2;
            this->ringerCheckbox->Text = L"Ringer";
            this->ringerCheckbox->UseVisualStyleBackColor = true;
            // 
            // clearBtn
            // 
            clearBtn->Location = System::Drawing::Point(3, 533);
            clearBtn->Name = L"clearBtn";
            clearBtn->Size = System::Drawing::Size(75, 21);
            clearBtn->TabIndex = 7;
            clearBtn->Text = L"Clear";
            clearBtn->UseVisualStyleBackColor = true;
            clearBtn->Click += gcnew System::EventHandler(this, &FiltersUI::clearBtn_Click);
            // 
            // cancelBtn
            // 
            cancelBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            cancelBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            cancelBtn->Location = System::Drawing::Point(316, 533);
            cancelBtn->Name = L"cancelBtn";
            cancelBtn->Size = System::Drawing::Size(75, 21);
            cancelBtn->TabIndex = 8;
            cancelBtn->Text = L"Cancel";
            cancelBtn->UseVisualStyleBackColor = true;
            cancelBtn->Click += gcnew System::EventHandler(this, &FiltersUI::cancelBtn_Click);
            // 
            // closeBtn
            // 
            this->closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->closeBtn->DialogResult = System::Windows::Forms::DialogResult::OK;
            this->closeBtn->Location = System::Drawing::Point(235, 533);
            this->closeBtn->Name = L"closeBtn";
            this->closeBtn->Size = System::Drawing::Size(75, 22);
            this->closeBtn->TabIndex = 9;
            this->closeBtn->Text = L"Save";
            this->closeBtn->UseVisualStyleBackColor = true;
            this->closeBtn->Click += gcnew System::EventHandler(this, &FiltersUI::closeBtn_Click);
            // 
            // associationGroupBox
            // 
            tableLayoutPanel1->SetColumnSpan(associationGroupBox, 3);
            associationGroupBox->Controls->Add(this->associationEditor);
            associationGroupBox->Controls->Add(this->associationCheckbox);
            associationGroupBox->Dock = System::Windows::Forms::DockStyle::Fill;
            associationGroupBox->Location = System::Drawing::Point(3, 89);
            associationGroupBox->Name = L"associationGroupBox";
            associationGroupBox->Size = System::Drawing::Size(388, 54);
            associationGroupBox->TabIndex = 1;
            associationGroupBox->TabStop = false;
            associationGroupBox->Text = L"Association";
            // 
            // associationEditor
            // 
            this->associationEditor->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
            this->associationEditor->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->associationEditor->FormattingEnabled = true;
            this->associationEditor->Location = System::Drawing::Point(6, 19);
            this->associationEditor->Margin = System::Windows::Forms::Padding(10, 3, 3, 3);
            this->associationEditor->Name = L"associationEditor";
            this->associationEditor->Size = System::Drawing::Size(296, 21);
            this->associationEditor->TabIndex = 1;
            this->associationEditor->SelectedIndexChanged += gcnew System::EventHandler(this, &FiltersUI::associationEditor_SelectionChanged);
            this->associationEditor->TextUpdate += gcnew System::EventHandler(this, &FiltersUI::associationEditor_SelectionChanged);
            // 
            // associationCheckbox
            // 
            this->associationCheckbox->AutoSize = true;
            this->associationCheckbox->Location = System::Drawing::Point(308, 19);
            this->associationCheckbox->Name = L"associationCheckbox";
            this->associationCheckbox->Padding = System::Windows::Forms::Padding(0, 4, 0, 0);
            this->associationCheckbox->Size = System::Drawing::Size(59, 21);
            this->associationCheckbox->TabIndex = 2;
            this->associationCheckbox->Text = L"Enable";
            this->associationCheckbox->UseVisualStyleBackColor = true;
            // 
            // otherGroup
            // 
            otherGroup->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            otherGroup->CausesValidation = false;
            tableLayoutPanel1->SetColumnSpan(otherGroup, 3);
            otherGroup->Controls->Add(this->linkedRingers);
            otherGroup->Controls->Add(this->excludeInvalidTimeOrChanges);
            otherGroup->Controls->Add(this->excludeValid);
            otherGroup->Controls->Add(this->includeWithdrawn);
            otherGroup->Dock = System::Windows::Forms::DockStyle::Fill;
            otherGroup->Location = System::Drawing::Point(3, 457);
            otherGroup->Name = L"otherGroup";
            otherGroup->Size = System::Drawing::Size(388, 70);
            otherGroup->TabIndex = 10;
            otherGroup->TabStop = false;
            otherGroup->Text = L"Other";
            // 
            // linkedRingers
            // 
            this->linkedRingers->AutoSize = true;
            this->linkedRingers->Location = System::Drawing::Point(203, 43);
            this->linkedRingers->Name = L"linkedRingers";
            this->linkedRingers->Padding = System::Windows::Forms::Padding(4);
            this->linkedRingers->Size = System::Drawing::Size(134, 25);
            this->linkedRingers->TabIndex = 8;
            this->linkedRingers->Text = L"Include linked ringers";
            this->linkedRingers->UseVisualStyleBackColor = true;
            // 
            // excludeInvalidTimeOrChanges
            // 
            this->excludeInvalidTimeOrChanges->AutoSize = true;
            this->excludeInvalidTimeOrChanges->Location = System::Drawing::Point(6, 47);
            this->excludeInvalidTimeOrChanges->Name = L"excludeInvalidTimeOrChanges";
            this->excludeInvalidTimeOrChanges->Size = System::Drawing::Size(175, 17);
            this->excludeInvalidTimeOrChanges->TabIndex = 7;
            this->excludeInvalidTimeOrChanges->Text = L"Exclude invalid time or changes";
            this->excludeInvalidTimeOrChanges->UseVisualStyleBackColor = true;
            // 
            // excludeValid
            // 
            this->excludeValid->AutoSize = true;
            this->excludeValid->Location = System::Drawing::Point(203, 16);
            this->excludeValid->Name = L"excludeValid";
            this->excludeValid->Padding = System::Windows::Forms::Padding(4);
            this->excludeValid->Size = System::Drawing::Size(164, 25);
            this->excludeValid->TabIndex = 6;
            this->excludeValid->Text = L"Exclude valid performances";
            this->excludeValid->UseVisualStyleBackColor = true;
            // 
            // includeWithdrawn
            // 
            this->includeWithdrawn->AutoSize = true;
            this->includeWithdrawn->Location = System::Drawing::Point(3, 16);
            this->includeWithdrawn->Name = L"includeWithdrawn";
            this->includeWithdrawn->Padding = System::Windows::Forms::Padding(4);
            this->includeWithdrawn->Size = System::Drawing::Size(187, 25);
            this->includeWithdrawn->TabIndex = 5;
            this->includeWithdrawn->Text = L"Include withdrawn performances";
            this->includeWithdrawn->UseVisualStyleBackColor = true;
            // 
            // dateGroup
            // 
            dateGroup->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            tableLayoutPanel1->SetColumnSpan(dateGroup, 3);
            dateGroup->Controls->Add(this->thisYearBtn);
            dateGroup->Controls->Add(this->lastYearBtn);
            dateGroup->Controls->Add(this->endCheckBox);
            dateGroup->Controls->Add(this->startCheckBox);
            dateGroup->Controls->Add(this->endDate);
            dateGroup->Controls->Add(this->startDate);
            dateGroup->Dock = System::Windows::Forms::DockStyle::Fill;
            dateGroup->Location = System::Drawing::Point(3, 3);
            dateGroup->Name = L"dateGroup";
            dateGroup->Size = System::Drawing::Size(388, 80);
            dateGroup->TabIndex = 11;
            dateGroup->TabStop = false;
            dateGroup->Text = L"Date";
            // 
            // thisYearBtn
            // 
            this->thisYearBtn->Location = System::Drawing::Point(304, 15);
            this->thisYearBtn->Name = L"thisYearBtn";
            this->thisYearBtn->Size = System::Drawing::Size(75, 23);
            this->thisYearBtn->TabIndex = 5;
            this->thisYearBtn->Text = L"This year";
            this->thisYearBtn->UseVisualStyleBackColor = true;
            this->thisYearBtn->Click += gcnew System::EventHandler(this, &FiltersUI::thisYearButton_Click);
            // 
            // lastYearBtn
            // 
            this->lastYearBtn->Location = System::Drawing::Point(304, 44);
            this->lastYearBtn->Name = L"lastYearBtn";
            this->lastYearBtn->Size = System::Drawing::Size(75, 23);
            this->lastYearBtn->TabIndex = 4;
            this->lastYearBtn->Text = L"Last year";
            this->lastYearBtn->UseVisualStyleBackColor = true;
            this->lastYearBtn->Click += gcnew System::EventHandler(this, &FiltersUI::lastYearButton_Click);
            // 
            // endCheckBox
            // 
            this->endCheckBox->AutoSize = true;
            this->endCheckBox->Location = System::Drawing::Point(139, 50);
            this->endCheckBox->Name = L"endCheckBox";
            this->endCheckBox->Size = System::Drawing::Size(39, 17);
            this->endCheckBox->TabIndex = 3;
            this->endCheckBox->Text = L"To";
            this->endCheckBox->UseVisualStyleBackColor = true;
            // 
            // startCheckBox
            // 
            this->startCheckBox->AutoSize = true;
            this->startCheckBox->Location = System::Drawing::Point(139, 23);
            this->startCheckBox->Name = L"startCheckBox";
            this->startCheckBox->Size = System::Drawing::Size(49, 17);
            this->startCheckBox->TabIndex = 2;
            this->startCheckBox->Text = L"From";
            this->startCheckBox->UseVisualStyleBackColor = true;
            // 
            // endDate
            // 
            this->endDate->Location = System::Drawing::Point(6, 47);
            this->endDate->Name = L"endDate";
            this->endDate->Size = System::Drawing::Size(122, 20);
            this->endDate->TabIndex = 1;
            this->endDate->ValueChanged += gcnew System::EventHandler(this, &FiltersUI::endDate_ValueChanged);
            // 
            // startDate
            // 
            this->startDate->Location = System::Drawing::Point(6, 20);
            this->startDate->Name = L"startDate";
            this->startDate->Size = System::Drawing::Size(122, 20);
            this->startDate->TabIndex = 0;
            this->startDate->ValueChanged += gcnew System::EventHandler(this, &FiltersUI::startDate_ValueChanged);
            // 
            // FiltersUI
            // 
            this->AcceptButton = this->closeBtn;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            this->CancelButton = cancelBtn;
            this->ClientSize = System::Drawing::Size(394, 560);
            this->Controls->Add(tableLayoutPanel1);
            this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedDialog;
            this->MaximizeBox = false;
            this->Name = L"FiltersUI";
            this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
            this->Text = L"Filters";
            tableLayoutPanel1->ResumeLayout(false);
            towerGroupBox->ResumeLayout(false);
            towerGroupBox->PerformLayout();
            methodGroupBox->ResumeLayout(false);
            methodGroupBox->PerformLayout();
            peopleGroupBox->ResumeLayout(false);
            peopleGroupBox->PerformLayout();
            associationGroupBox->ResumeLayout(false);
            associationGroupBox->PerformLayout();
            otherGroup->ResumeLayout(false);
            otherGroup->PerformLayout();
            dateGroup->ResumeLayout(false);
            dateGroup->PerformLayout();
            this->ResumeLayout(false);

        }
#pragma endregion
    };
}
