#include "TableSorterByTime.h"
#include "DucoUtils.h"

using namespace DucoUI;
using namespace System::Windows::Forms;
using namespace System;

TableSorterByTime::TableSorterByTime(bool newAscending, int newColumnId)
: ascending(newAscending), columnId(newColumnId)
{
}

TableSorterByTime::~TableSorterByTime()
{
}

int
TableSorterByTime::Compare(System::Object^ firstObject, System::Object^ secondObject)
{
    DataGridViewRow^ row1 = static_cast<DataGridViewRow^>(firstObject);
    DataGridViewRow^ row2 = static_cast<DataGridViewRow^>(secondObject);

    if (!ascending)
        return Compare(row1, row2);

    return Compare(row2, row1);
}


int
TableSorterByTime::Compare(DataGridViewRow^ firstRow, DataGridViewRow^ secondRow)
{
    String^ row1Value = Convert::ToString(firstRow->Cells[columnId]->Value);
    String^ row2Value = Convert::ToString(secondRow->Cells[columnId]->Value);

    Duco::PerformanceTime row1Time = DucoUtils::ToTime(row1Value);
    Duco::PerformanceTime row2Time = DucoUtils::ToTime(row2Value);

    return row2Time - row1Time;
}