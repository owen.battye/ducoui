#include "DatabaseManager.h"
#include <Tower.h>

#include "RenameBellsUI.h"

#include "DucoUtils.h"
#include "SystemDefaultIcon.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;

RenameBellsUI::RenameBellsUI(DatabaseManager^ theDatabase, Duco::Tower& theCurrentTower, bool& newChangesMade)
: database(theDatabase), currentTower(theCurrentTower), changesMade(newChangesMade)
{
    InitializeComponent();
    renamedBells = new std::map<unsigned int, Duco::TRenameBellType>();
}

RenameBellsUI::~RenameBellsUI()
{
    if (components)
    {
        delete components;
    }
    delete renamedBells;
}

System::Void
RenameBellsUI::saveBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (dataChanged)
    {
        changesMade = true;
        currentTower.SetRenamedBells(*renamedBells);
        dataChanged = false;
    }
    Close();
}

System::Void
RenameBellsUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (dataChanged && DucoUtils::ConfirmLooseChanges(" bell names", this))
    {
        Close();
    }
}

System::Void
RenameBellsUI::RenameBellsUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    currentTower.RenamedBells(*renamedBells);

    std::vector<std::wstring> bellNames;
    currentTower.AllBellNames(bellNames, KNoId);
    for (unsigned int i = 1; i <= currentTower.Bells(); ++i)
    {
        bellList->Items->Add(Convert::ToString(i) + L":\t " + DucoUtils::ConvertString(bellNames[i-1]));
    }
    bellList->SelectedIndex = 0;
    dataChanged = false;
}

System::Void
RenameBellsUI::RenameBellsUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e)
{
    if ( dataChanged )
    {
        if (DucoUtils::ConfirmLooseChanges("Rename bells", this))
        {
            e->Cancel = true;
        }
    }
}

System::Void 
RenameBellsUI::extraTrebleBtn_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (!switchingBell)
    {
        RadioButton^ btn = safe_cast<RadioButton^>(sender);
        if (btn->Checked)
        {
            SetBellNameType(bellList->SelectedIndex + 1, Duco::EExtraTreble);
            dataChanged = true;
        }
    }
}

System::Void
RenameBellsUI::sharpBtn_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (!switchingBell)
    {
        RadioButton^ btn = safe_cast<RadioButton^>(sender);
        if (btn->Checked)
        {
            SetBellNameType(bellList->SelectedIndex + 1, Duco::ESharp);
            dataChanged = true;
        }
    }
}

System::Void
RenameBellsUI::flatBtn_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (!switchingBell)
    {
        RadioButton^ btn = safe_cast<RadioButton^>(sender);
        if (btn->Checked)
        {
            SetBellNameType(bellList->SelectedIndex + 1, Duco::EFlat);
            dataChanged = true;
        }
    }
}

System::Void
RenameBellsUI::normalBtn_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (!switchingBell)
    {
        RadioButton^ btn = safe_cast<RadioButton^>(sender);
        if (btn->Checked)
        {
            SetBellNameType(bellList->SelectedIndex + 1, Duco::ENormalBell);
            dataChanged = true;
        }
    }
}

System::Void
RenameBellsUI::SetBellNameType(unsigned int bellNumber, Duco::TRenameBellType newType)
{
    std::map<unsigned int, Duco::TRenameBellType>::iterator it = renamedBells->find(bellNumber);
    if (it == renamedBells->end())
    {
        switch (newType)
        {
        case Duco::ENormalBell:
            break;
        default:
            {
            std::pair<unsigned int, Duco::TRenameBellType> newObject (bellNumber, newType);
            renamedBells->insert(newObject);
            }
            break;
        }
    }
    else
    {
        switch (newType)
        {
        case Duco::ENormalBell:
            renamedBells->erase(it);
            break;

        default:
            it->second = newType;
            break;
        }
    }

    Tower newTower = currentTower;
    newTower.SetRenamedBells(*renamedBells);
    std::vector<std::wstring> bellNames;
    bellList->Items->Clear();
    newTower.AllBellNames(bellNames, KNoId);
    for (unsigned int i = 1; i <= currentTower.Bells(); ++i)
    {
        bellList->Items->Add(Convert::ToString(i) + L":\t " + DucoUtils::ConvertString(bellNames[i - 1]));
    }
    bellList->SelectedIndex = bellNumber - 1;
}

System::Void
RenameBellsUI::bellList_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    std::map<unsigned int, Duco::TRenameBellType>::const_iterator it = renamedBells->find(bellList->SelectedIndex+1);
    switchingBell = true;

    if (bellList->SelectedIndex == 0)
    {
        extraTrebleBtn->Visible = true;
    }
    else
    {
        extraTrebleBtn->Visible = false;
    }
    if (it == renamedBells->end())
    {
        normalBtn->Checked = true;
    }
    else
    {
        switch (it->second)
        {
        default:
            normalBtn->Checked = true;
            break;
        case Duco::EExtraTreble:
            extraTrebleBtn->Checked = true;
            break;
        case Duco::EFlat:
            flatBtn->Checked = true;
            break;
        case Duco::ESharp:
            sharpBtn->Checked = true;
            break;
        }
    }
    switchingBell = false;
}

#pragma region Windows Form Designer generated code
void RenameBellsUI::InitializeComponent(void)
{
    System::Windows::Forms::GroupBox^ groupBox1;
    System::Windows::Forms::Button^ closeBtn;
    System::Windows::Forms::Button^ saveBtn;
    System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
    System::Windows::Forms::Label^ label1;
    this->extraTrebleBtn = (gcnew System::Windows::Forms::RadioButton());
    this->normalBtn = (gcnew System::Windows::Forms::RadioButton());
    this->sharpBtn = (gcnew System::Windows::Forms::RadioButton());
    this->flatBtn = (gcnew System::Windows::Forms::RadioButton());
    this->bellList = (gcnew System::Windows::Forms::ComboBox());
    groupBox1 = (gcnew System::Windows::Forms::GroupBox());
    closeBtn = (gcnew System::Windows::Forms::Button());
    saveBtn = (gcnew System::Windows::Forms::Button());
    tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
    label1 = (gcnew System::Windows::Forms::Label());
    groupBox1->SuspendLayout();
    tableLayoutPanel1->SuspendLayout();
    this->SuspendLayout();
    // 
    // groupBox1
    // 
    tableLayoutPanel1->SetColumnSpan(groupBox1, 3);
    groupBox1->Controls->Add(this->extraTrebleBtn);
    groupBox1->Controls->Add(this->normalBtn);
    groupBox1->Controls->Add(this->sharpBtn);
    groupBox1->Controls->Add(this->flatBtn);
    groupBox1->Dock = System::Windows::Forms::DockStyle::Fill;
    groupBox1->Location = System::Drawing::Point(3, 29);
    groupBox1->Name = L"groupBox1";
    groupBox1->Size = System::Drawing::Size(292, 49);
    groupBox1->TabIndex = 1;
    groupBox1->TabStop = false;
    groupBox1->Text = L"Details";
    // 
    // extraTrebleBtn
    // 
    this->extraTrebleBtn->AutoSize = true;
    this->extraTrebleBtn->Location = System::Drawing::Point(182, 19);
    this->extraTrebleBtn->Name = L"extraTrebleBtn";
    this->extraTrebleBtn->Size = System::Drawing::Size(82, 17);
    this->extraTrebleBtn->TabIndex = 3;
    this->extraTrebleBtn->TabStop = true;
    this->extraTrebleBtn->Text = L"Extra Treble";
    this->extraTrebleBtn->UseVisualStyleBackColor = true;
    this->extraTrebleBtn->CheckedChanged += gcnew System::EventHandler(this, &RenameBellsUI::extraTrebleBtn_CheckedChanged);
    // 
    // normalBtn
    // 
    this->normalBtn->AutoSize = true;
    this->normalBtn->Location = System::Drawing::Point(11, 19);
    this->normalBtn->Name = L"normalBtn";
    this->normalBtn->Size = System::Drawing::Size(58, 17);
    this->normalBtn->TabIndex = 0;
    this->normalBtn->TabStop = true;
    this->normalBtn->Text = L"Normal";
    this->normalBtn->UseVisualStyleBackColor = true;
    this->normalBtn->CheckedChanged += gcnew System::EventHandler(this, &RenameBellsUI::normalBtn_CheckedChanged);
    // 
    // sharpBtn
    // 
    this->sharpBtn->AutoSize = true;
    this->sharpBtn->Location = System::Drawing::Point(123, 19);
    this->sharpBtn->Name = L"sharpBtn";
    this->sharpBtn->Size = System::Drawing::Size(53, 17);
    this->sharpBtn->TabIndex = 2;
    this->sharpBtn->TabStop = true;
    this->sharpBtn->Text = L"Sharp";
    this->sharpBtn->UseVisualStyleBackColor = true;
    this->sharpBtn->CheckedChanged += gcnew System::EventHandler(this, &RenameBellsUI::sharpBtn_CheckedChanged);
    // 
    // flatBtn
    // 
    this->flatBtn->AutoSize = true;
    this->flatBtn->Location = System::Drawing::Point(75, 19);
    this->flatBtn->Name = L"flatBtn";
    this->flatBtn->Size = System::Drawing::Size(42, 17);
    this->flatBtn->TabIndex = 1;
    this->flatBtn->TabStop = true;
    this->flatBtn->Text = L"Flat";
    this->flatBtn->UseVisualStyleBackColor = true;
    this->flatBtn->CheckedChanged += gcnew System::EventHandler(this, &RenameBellsUI::flatBtn_CheckedChanged);
    // 
    // closeBtn
    // 
    closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
    closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
    closeBtn->Location = System::Drawing::Point(220, 84);
    closeBtn->Name = L"closeBtn";
    closeBtn->Size = System::Drawing::Size(75, 23);
    closeBtn->TabIndex = 3;
    closeBtn->Text = L"Close";
    closeBtn->UseVisualStyleBackColor = true;
    // 
    // saveBtn
    // 
    saveBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
    saveBtn->Location = System::Drawing::Point(139, 84);
    saveBtn->Name = L"saveBtn";
    saveBtn->Size = System::Drawing::Size(75, 23);
    saveBtn->TabIndex = 2;
    saveBtn->Text = L"Save";
    saveBtn->UseVisualStyleBackColor = true;
    saveBtn->Click += gcnew System::EventHandler(this, &RenameBellsUI::saveBtn_Click);
    // 
    // tableLayoutPanel1
    // 
    tableLayoutPanel1->ColumnCount = 3;
    tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
    tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
    tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
    tableLayoutPanel1->Controls->Add(closeBtn, 2, 2);
    tableLayoutPanel1->Controls->Add(groupBox1, 0, 1);
    tableLayoutPanel1->Controls->Add(saveBtn, 1, 2);
    tableLayoutPanel1->Controls->Add(label1, 0, 0);
    tableLayoutPanel1->Controls->Add(this->bellList, 1, 0);
    tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
    tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
    tableLayoutPanel1->Name = L"tableLayoutPanel1";
    tableLayoutPanel1->RowCount = 3;
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 26)));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 29)));
    tableLayoutPanel1->Size = System::Drawing::Size(298, 110);
    tableLayoutPanel1->TabIndex = 4;
    // 
    // label1
    // 
    label1->AutoSize = true;
    label1->Location = System::Drawing::Point(3, 6);
    label1->Margin = System::Windows::Forms::Padding(3, 6, 3, 0);
    label1->Name = L"label1";
    label1->Size = System::Drawing::Size(68, 13);
    label1->TabIndex = 4;
    label1->Text = L"Bell number: ";
    // 
    // bellList
    // 
    tableLayoutPanel1->SetColumnSpan(this->bellList, 2);
    this->bellList->FormattingEnabled = true;
    this->bellList->Location = System::Drawing::Point(77, 3);
    this->bellList->Name = L"bellList";
    this->bellList->Size = System::Drawing::Size(102, 21);
    this->bellList->TabIndex = 0;
    this->bellList->SelectedIndexChanged += gcnew System::EventHandler(this, &RenameBellsUI::bellList_SelectedIndexChanged);
    // 
    // RenameBellsUI
    // 
    this->AcceptButton = saveBtn;
    this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
    this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
    this->CancelButton = closeBtn;
    this->ClientSize = System::Drawing::Size(298, 110);
    this->Controls->Add(tableLayoutPanel1);
    this->MaximizeBox = false;
    this->MinimizeBox = false;
    this->MinimumSize = System::Drawing::Size(314, 149);
    this->Name = L"RenameBellsUI";
    this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
    this->Text = L"Rename bell";
    this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &RenameBellsUI::RenameBellsUI_FormClosing);
    this->Load += gcnew System::EventHandler(this, &RenameBellsUI::RenameBellsUI_Load);
    groupBox1->ResumeLayout(false);
    groupBox1->PerformLayout();
    tableLayoutPanel1->ResumeLayout(false);
    tableLayoutPanel1->PerformLayout();
    this->ResumeLayout(false);

}
#pragma endregion
