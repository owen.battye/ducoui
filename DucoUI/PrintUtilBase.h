#pragma once

#include <string>
#include <vector>

#define KPrintFooter  L"Produced by Duco - download from www.rocketpole.co.uk"
const System::Int16 KFooterSizeLines = 2;

namespace DucoUI
{
    ref class DatabaseManager;

public ref class PrintUtilBase abstract
{
public:
    PrintUtilBase(DucoUI::DatabaseManager^ theDatabase, float fontSize, System::Drawing::Printing::PrintPageEventArgs^ newArgs, bool newRotateHeaderCells);
    virtual System::Void printObject(System::Drawing::Printing::PrintPageEventArgs^ newArgs, System::Drawing::Printing::PrinterSettings^ printSettings) = 0;

protected:
    System::Void SetFont(System::String^ fontName, float fontSize, System::Drawing::Printing::PrintPageEventArgs^ newArgs);
    System::Void ResetPosition(System::Drawing::Printing::PrintPageEventArgs^ newArgs);
    System::Void PrintLine(System::Int32 left, System::UInt32 right, System::UInt32 yPos, System::Drawing::Printing::PrintPageEventArgs^ printArgs);

    System::Void PrintLongString(const std::wstring& stringToPrint, System::Drawing::Font^ font, System::Drawing::Printing::PrintPageEventArgs^ newArgs);
    System::Void PrintLongString(System::String^ stringToPrint, System::Drawing::Font^ font, System::Drawing::Printing::PrintPageEventArgs^ newArgs);
    System::Void PrintLongString(System::String^ stringToPrint, System::Drawing::Font^ font, float leftPosition, System::Drawing::Printing::PrintPageEventArgs^ newArgs);
    System::Void PrintLongString(System::String^ stringToPrint, System::Drawing::StringFormat^ stringFormat, System::Drawing::Font^ font, float leftPosition, float maxWidth, System::Drawing::Printing::PrintPageEventArgs^ printArgs);
    System::Void PrintStringCentrally(System::String^ stringToPrint, System::Drawing::Font^ font, System::Drawing::Printing::PrintPageEventArgs^ newArgs);
    System::Void PrintStringMaxLength(System::String^ stringToPrint, System::Drawing::Font^ font, float& xPosition, System::Single maxLength, System::Drawing::Printing::PrintPageEventArgs^ newArgs);
    System::Void PrintStringRightJustified(System::String^ stringToPrint, System::Drawing::Font^ font, float& xPosition, System::Single maxLength, System::Drawing::Printing::PrintPageEventArgs^ printArgs);

    System::Void addDucoFooter(System::Drawing::Font^ printFont, System::Drawing::StringFormat^ ringerStringFormat, System::Drawing::Printing::PrintPageEventArgs^ newArgs);
    System::Void addDucoFooter(System::Drawing::Font^ printFont, System::Drawing::StringFormat^ ringerStringFormat, System::Drawing::Printing::PrintPageEventArgs^ newArgs, System::Int16 pageNo);

    float ColumnHeaderSize(System::Windows::Forms::DataGridViewColumn^ theColumn, System::Drawing::Printing::PrintPageEventArgs^ printArgs, bool rotate);
    virtual System::Void GenerateColumnWidths(std::vector<float>& columnWidths, System::Drawing::Printing::PrintPageEventArgs^ printArgs, System::Windows::Forms::DataGridView^ dataGridView);

    unsigned int MeasureLongStringHeight(System::String^ stringToPrint, System::Drawing::StringFormat^ stringFormat, System::Drawing::Font^ font, float maxWidth, System::Drawing::Printing::PrintPageEventArgs^ printArgs);
    System::String^ TrimString(System::Windows::Forms::DataGridViewCell^ nextCell, System::Drawing::StringFormat^ stringFormat, System::Drawing::Font^ font, System::Single maxLength, System::Drawing::Printing::PrintPageEventArgs^ newArgs, System::String^ charToAppend, System::Boolean% trimmed);
    System::String^ TrimString(System::String^ stringToPrint, System::Drawing::StringFormat^ stringFormat, System::Drawing::Font^ font, System::Single maxLength, System::Drawing::Printing::PrintPageEventArgs^ printArgs, System::String^ charToAppend, System::Boolean% trimmed);

protected:
    DucoUI::DatabaseManager^        database;
    System::Boolean                 rotateHeaderCells;
    System::Boolean                 printThisPage;
    System::Drawing::Font^          smallFont;
    System::Drawing::Font^          normalFont;
    System::Drawing::Font^          largeFont;
    System::Drawing::Font^          boldFont;
    float                           lineHeight;
    float                           smallLineHeight;
    float                           leftMargin;
    float                           yPos;
    float                           maxHeaderRowHeight;
    unsigned int                    noOfLines;
    System::Int16                   printingResultNumber;
};

}