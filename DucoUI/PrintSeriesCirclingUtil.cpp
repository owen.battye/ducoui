#include "PrintSeriesCirclingUtil.h"
#include <MethodSeries.h>
#include "DucoUtils.h"
#include "DatabaseManager.h"

using namespace System;
using namespace System::Drawing;
using namespace System::Drawing::Drawing2D;
using namespace System::Windows::Forms;
using namespace Duco;
using namespace DucoUI;

const float KColumnSpacing = 5;

PrintSeriesCirclingUtil::PrintSeriesCirclingUtil(DatabaseManager^ theDatabase, System::Drawing::Printing::PrintPageEventArgs^ args)
:	PrintUtilBase(theDatabase, 7, args, true)
{
    seriesId = new Duco::ObjectId();
}

PrintSeriesCirclingUtil::!PrintSeriesCirclingUtil()
{
    delete seriesId;
}

PrintSeriesCirclingUtil::~PrintSeriesCirclingUtil()
{
    this->!PrintSeriesCirclingUtil();
}


System::Void
PrintSeriesCirclingUtil::SetObjects(System::Windows::Forms::DataGridView^ theDataGridView, const Duco::ObjectId& newSeriesId)
{
    *seriesId = newSeriesId;
    dataGridView = theDataGridView;
    printingResultNumber = 0;
}

System::Void
PrintSeriesCirclingUtil::printObject(System::Drawing::Printing::PrintPageEventArgs^ printArgs, System::Drawing::Printing::PrinterSettings^ printSettings)
{
    StringFormat^ stringFormat = gcnew StringFormat;
    addDucoFooter(normalFont, stringFormat, printArgs);

    MethodSeries theSeries;
    if (database->FindMethodSeries(*seriesId, theSeries, false))
    {
        String^ titlePrintBuffer = "Method series: " + DucoUtils::ConvertString(theSeries.Name());
        printArgs->Graphics->DrawString(titlePrintBuffer, boldFont, Brushes::Black, leftMargin, yPos, stringFormat);
        yPos += lineHeight;
        noOfLines -= 1;

        Duco::PerformanceDate lastPealDate;
        if (database->LastPealInMethodSeries(*seriesId, lastPealDate))
        {
            titlePrintBuffer = "Up to and including last peal on: " + DucoUtils::ConvertString(lastPealDate, true);
            printArgs->Graphics->DrawString(titlePrintBuffer, normalFont, Brushes::Black, leftMargin, yPos, stringFormat);
            noOfLines -= 1;
        }
    }

    std::vector<float> columnWidths;
    GenerateColumnWidths(columnWidths, printArgs, dataGridView);

    maxHeaderRowHeight *= (2.0F / 3.0F);
    int noOfLinesInHeader = int(maxHeaderRowHeight / lineHeight);
    noOfLines -= noOfLinesInHeader;

    float xPos = leftMargin;
    //Print header row
    for (int columnNo (0); columnNo < dataGridView->Columns->Count; ++columnNo)
    {
        String^ headerCellText = dataGridView->Columns[columnNo]->HeaderCell->Value->ToString();
        SizeF stringSize = printArgs->Graphics->MeasureString(headerCellText, normalFont, printArgs->PageBounds.Width);
        DataGridViewColumn^ currentColumn = dataGridView->Columns[columnNo];
        float thisYPos (yPos);
        if (columnNo == 0)
        {
            thisYPos += (maxHeaderRowHeight - stringSize.Height);
            printArgs->Graphics->DrawString(headerCellText, boldFont, Brushes::Black, xPos, thisYPos, stringFormat);
            xPos += columnWidths[columnNo];
            xPos += KColumnSpacing;
        }
        else if (columnNo == 1 && columnWidths[1] > 0)
        {
            thisYPos += (maxHeaderRowHeight - stringSize.Height);
            printArgs->Graphics->DrawString(headerCellText, normalFont, Brushes::Black, xPos, thisYPos, stringFormat);
            xPos += columnWidths[columnNo];
            xPos += KColumnSpacing;
        }
        else if (columnNo > 1)
        {
            GraphicsPath^ textPath = gcnew GraphicsPath();
            textPath->AddString(headerCellText, largeFont->FontFamily, 0, largeFont->Size, PointF(0, 0), stringFormat);
            Matrix^ transformMatrix = gcnew Matrix();
            transformMatrix->Rotate(292.5F);
            textPath->Transform(transformMatrix);
            transformMatrix->Reset();
            transformMatrix->Translate(xPos, thisYPos + maxHeaderRowHeight);
            textPath->Transform(transformMatrix);
            printArgs->Graphics->FillPath(Brushes::Black, textPath);
            xPos += columnWidths[columnNo];
            xPos += KColumnSpacing;
        }
    }
    xPos  = leftMargin;
    yPos += maxHeaderRowHeight;

    while (noOfLines > 0 && printingResultNumber < dataGridView->Rows->Count)
    {
        DataGridViewRow^ currentRow = dataGridView->Rows[printingResultNumber];
        xPos = leftMargin;
        yPos += lineHeight;
        String^ printBuffer = "";

        unsigned int columnNo (0);
        std::vector<float>::const_iterator it = columnWidths.begin();
        while (it != columnWidths.end())
        {
            if ((*it) > 0)
            {
                printBuffer = DucoUtils::ConvertString(currentRow->Cells[columnNo]);
                printArgs->Graphics->DrawString(printBuffer, normalFont, Brushes::Black, xPos, yPos, stringFormat);
                xPos += KColumnSpacing;
                xPos += *it;
            }
            ++columnNo;
            ++it;
        }

        noOfLines--;
        printingResultNumber++;
    }
}
