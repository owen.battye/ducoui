#include "RingingDatabaseObserver.h"
#include "DatabaseManager.h"
#include "RingerDisplayCache.h"
#include <StatisticFilters.h>
#include "DucoWindowState.h"

#include "AssociationStatsUI.h"

#include <AssociationDatabase.h>
#include "SystemDefaultIcon.h"
#include "FiltersUI.h"
#include "DucoTableSorter.h"
#include <RingingDatabase.h>
#include <DucoEngineUtils.h>
#include "DucoUtils.h"
#include <PealDatabase.h>
#include "SoundUtils.h"
#include "AssociationsUI.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Windows::Forms;

AssociationStatsUI::AssociationStatsUI(DatabaseManager^ theDatabase)
:   database(theDatabase)
{
    filters = new Duco::StatisticFilters(database->Database());
    InitializeComponent();
}

AssociationStatsUI::~AssociationStatsUI()
{
    this->!AssociationStatsUI();
    if (components)
    {
        delete components;
    }
}

AssociationStatsUI::!AssociationStatsUI()
{
    delete filters;
}

System::Void
AssociationStatsUI::AssociationStatsUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    backgroundGenerator->RunWorkerAsync(this);
}

System::Void
AssociationStatsUI::combineBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    dataGridView->Rows->Clear();
    backgroundGenerator->RunWorkerAsync(this);
}

System::Void
AssociationStatsUI::filtersBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    FiltersUI^ filtersDialog = gcnew FiltersUI(*filters, database);
    System::Windows::Forms::DialogResult returnVal = filtersDialog->ShowDialog(this);
    if (returnVal == System::Windows::Forms::DialogResult::OK)
    {
        dataGridView->Rows->Clear();
        backgroundGenerator->RunWorkerAsync(this);
    }
}

System::Void
AssociationStatsUI::dataGridView_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
    DataGridViewColumn^ newColumn = dataGridView->Columns[e->ColumnIndex];
    SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;

    societyCount->HeaderCell->SortGlyphDirection = SortOrder::None;

    if (newColumn->SortMode != System::Windows::Forms::DataGridViewColumnSortMode::Programmatic)
        return;

    if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
        newSortOrder = SortOrder::Ascending;
    else
        newSortOrder = SortOrder::Descending;

    associationColumn->HeaderCell->SortGlyphDirection = SortOrder::None;

    newColumn->HeaderCell->SortGlyphDirection = newSortOrder;
    DucoTableSorter^ sorter = gcnew DucoTableSorter(newSortOrder == SortOrder::Ascending, EString);
    switch (e->ColumnIndex)
    {
    case 2:
        sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
        break;
    case 1:
        sorter->AddSortObject(EString, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
        break;
    default:
        break;
    }
    dataGridView->Sort(sorter);
}

System::Void
AssociationStatsUI::dataGridView_CellContentClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    try
    {
        System::Windows::Forms::DataGridView^ theDataView = static_cast<System::Windows::Forms::DataGridView^>(sender);
        unsigned int rowIndex = e->RowIndex;
        DataGridViewRow^ currentRow = theDataView->Rows[rowIndex];
        unsigned int associationId = Convert::ToInt16(currentRow->Cells[0]->Value);
        AssociationsUI::ShowAssociation(database, this->MdiParent, associationId);
    }
    catch (Exception^)
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
AssociationStatsUI::backgroundGenerator_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
    BackgroundWorker^ worker = dynamic_cast<BackgroundWorker^>(sender);

    worker->ReportProgress(0);
    std::map<Duco::PealLengthInfo, Duco::ObjectId> sortedPealCounts;

    {
        std::map<Duco::ObjectId, Duco::PealLengthInfo> associationCounts;
        database->Database().PealsDatabase().GetAssociationsPealCount(*filters, associationCounts, combineBtn->Checked);

        std::map<Duco::ObjectId, Duco::PealLengthInfo>::const_iterator convIt = associationCounts.begin();
        while (convIt != associationCounts.end())
        {
            std::pair<Duco::PealLengthInfo, Duco::ObjectId> newObject(convIt->second, convIt->first);
            sortedPealCounts.insert(newObject);
            ++convIt;
        }
    }

    double noOfRows = double(sortedPealCounts.size());
    double count = 0;
    size_t position = 0;
    size_t lastCount = -1;

    std::map<Duco::PealLengthInfo, Duco::ObjectId>::const_reverse_iterator it = sortedPealCounts.rbegin();
    while (it != sortedPealCounts.rend() && !worker->CancellationPending)
    {
        if (lastCount != it->first.TotalPeals())
        {
            position = size_t(count) + 1;
            lastCount = it->first.TotalPeals();
        }

        DataGridViewRow^ newRow = gcnew DataGridViewRow();
        if (DucoEngineUtils::NumberWithSameCount(sortedPealCounts, it->first) > 1)
        {
            newRow->HeaderCell->Value = Convert::ToString(position) +  "=";
        }
        else
        {
            newRow->HeaderCell->Value = Convert::ToString(position);
        }

        DataGridViewTextBoxCell^ idCell = gcnew DataGridViewTextBoxCell();
        idCell->Value = DucoUtils::ConvertString(it->second);
        newRow->Cells->Add(idCell);
        DataGridViewTextBoxCell^ assocCell = gcnew DataGridViewTextBoxCell();
        assocCell->Value = DucoUtils::ConvertString(database->Database().AssociationsDatabase().FindAssociationName(it->second));
        newRow->Cells->Add(assocCell);
        DataGridViewTextBoxCell^ countCell = gcnew DataGridViewTextBoxCell();
        countCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->first.TotalPeals(), true));
        newRow->Cells->Add(countCell);
        DataGridViewTextBoxCell^ changesCell = gcnew DataGridViewTextBoxCell();
        changesCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->first.TotalChanges(), true));
        newRow->Cells->Add(changesCell);
        DataGridViewTextBoxCell^ timeCell = gcnew DataGridViewTextBoxCell();
        timeCell->Value = DucoUtils::PrintPealTime(it->first.TotalMinutes(), false);
        newRow->Cells->Add(timeCell);
        backgroundGenerator->ReportProgress(int((++count / noOfRows)*(double)100), newRow);
        ++it;
    }

    if (worker->CancellationPending)
        e->Cancel = true;
}

System::Void
AssociationStatsUI::backgroundGenerator_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    progressBar->Value = e->ProgressPercentage;
    if (e->ProgressPercentage == 0)
    {
        dataGridView->Rows->Clear();
        dataGridView->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::EnableResizing;
    }
    else
    {
        dataGridView->Rows->Add((DataGridViewRow^)e->UserState);
    }
}

System::Void
AssociationStatsUI::backgroundGenerator_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    progressBar->Value = 0;
    unsigned int noOfassociations = dataGridView->Rows->Count;
    if (noOfassociations == 1)
        noOfAssociations->Text = "1 Association";
    else
        noOfAssociations->Text = Convert::ToString(noOfassociations) + " Associations";
}
