#pragma once

#include <ProgressCallback.h>
#include <vcclr.h>

namespace DucoUI
{
interface class ProgressCallbackUI
{
public:
    virtual System::Void Initialised() = 0;
    virtual System::Void Step(int progressPercent) = 0;
    virtual System::Void Complete() = 0;
};

class ProgressCallbackWrapper : public Duco::ProgressCallback
{
public:
    ProgressCallbackWrapper(DucoUI::ProgressCallbackUI^ theCallback);

    virtual void Initialised();
    virtual void Step(int progressPercent);
    virtual void Complete(bool error);

protected:
    gcroot<ProgressCallbackUI^> uiCallback;
};

} // end namespace DucoUI

