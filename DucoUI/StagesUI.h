#pragma once
#include "RingingDatabaseObserver.h"

namespace DucoUI
{
    ref class DatabaseManager;
    public ref class StagesUI : public System::Windows::Forms::Form, public DucoUI::RingingDatabaseObserver
    {
    public:
        StagesUI(DucoUI::DatabaseManager^ theDatabase);

        // from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

    protected:
        ~StagesUI();
        !StagesUI();

        System::Void StagesUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void addBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void deleteBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void ordersDisplay_CellValueChanged(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);
        System::Void ordersDisplay_SelectionChanged(System::Object^  sender, System::EventArgs^  e);

        System::Void GenerateOrders();

    private:
        DucoUI::DatabaseManager^                    database;
        bool                                        loadingComplete;
        System::Windows::Forms::Button^             closeBtn;
        System::Windows::Forms::Button^             addBtn;
        System::Windows::Forms::Button^             deleteBtn;
        System::Windows::Forms::TableLayoutPanel^   tableLayoutPanel1;
        System::Windows::Forms::DataGridView^       ordersDisplay;
        System::Windows::Forms::DataGridViewTextBoxColumn^  nameColumn;
        System::ComponentModel::Container^          components;

        void InitializeComponent()
        {
            System::Windows::Forms::Panel^  panel1;
            this->closeBtn = (gcnew System::Windows::Forms::Button());
            this->deleteBtn = (gcnew System::Windows::Forms::Button());
            this->addBtn = (gcnew System::Windows::Forms::Button());
            this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            this->ordersDisplay = (gcnew System::Windows::Forms::DataGridView());
            this->nameColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            panel1 = (gcnew System::Windows::Forms::Panel());
            panel1->SuspendLayout();
            this->tableLayoutPanel1->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ordersDisplay))->BeginInit();
            this->SuspendLayout();
            // 
            // panel1
            // 
            panel1->Controls->Add(this->closeBtn);
            panel1->Controls->Add(this->deleteBtn);
            panel1->Controls->Add(this->addBtn);
            panel1->Dock = System::Windows::Forms::DockStyle::Fill;
            panel1->Location = System::Drawing::Point(3, 318);
            panel1->Name = L"panel1";
            panel1->Size = System::Drawing::Size(239, 25);
            panel1->TabIndex = 2;
            // 
            // closeBtn
            // 
            this->closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
            this->closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            this->closeBtn->Location = System::Drawing::Point(164, 2);
            this->closeBtn->Name = L"closeBtn";
            this->closeBtn->Size = System::Drawing::Size(75, 23);
            this->closeBtn->TabIndex = 2;
            this->closeBtn->Text = L"Close";
            this->closeBtn->UseVisualStyleBackColor = true;
            this->closeBtn->Click += gcnew System::EventHandler(this, &StagesUI::closeBtn_Click);
            // 
            // deleteBtn
            // 
            this->deleteBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
            this->deleteBtn->Enabled = false;
            this->deleteBtn->Location = System::Drawing::Point(2, 2);
            this->deleteBtn->Name = L"deleteBtn";
            this->deleteBtn->Size = System::Drawing::Size(75, 23);
            this->deleteBtn->TabIndex = 0;
            this->deleteBtn->Text = L"Delete unused";
            this->deleteBtn->UseVisualStyleBackColor = true;
            this->deleteBtn->Click += gcnew System::EventHandler(this, &StagesUI::deleteBtn_Click);
            // 
            // addBtn
            // 
            this->addBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
            this->addBtn->Location = System::Drawing::Point(83, 2);
            this->addBtn->Name = L"addBtn";
            this->addBtn->Size = System::Drawing::Size(75, 23);
            this->addBtn->TabIndex = 1;
            this->addBtn->Text = L"Add";
            this->addBtn->UseVisualStyleBackColor = true;
            this->addBtn->Click += gcnew System::EventHandler(this, &StagesUI::addBtn_Click);
            // 
            // tableLayoutPanel1
            // 
            this->tableLayoutPanel1->ColumnCount = 1;
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
                100)));
            this->tableLayoutPanel1->Controls->Add(panel1, 0, 1);
            this->tableLayoutPanel1->Controls->Add(this->ordersDisplay, 0, 0);
            this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            this->tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
            this->tableLayoutPanel1->RowCount = 2;
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 31)));
            this->tableLayoutPanel1->Size = System::Drawing::Size(245, 346);
            this->tableLayoutPanel1->TabIndex = 0;
            // 
            // ordersDisplay
            // 
            this->ordersDisplay->AllowUserToAddRows = false;
            this->ordersDisplay->AllowUserToDeleteRows = false;
            this->ordersDisplay->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->ordersDisplay->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(1) { this->nameColumn });
            this->ordersDisplay->Dock = System::Windows::Forms::DockStyle::Fill;
            this->ordersDisplay->Location = System::Drawing::Point(3, 3);
            this->ordersDisplay->Name = L"ordersDisplay";
            this->ordersDisplay->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToDisplayedHeaders;
            this->ordersDisplay->Size = System::Drawing::Size(239, 309);
            this->ordersDisplay->TabIndex = 1;
            this->ordersDisplay->CellValueChanged += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &StagesUI::ordersDisplay_CellValueChanged);
            this->ordersDisplay->SelectionChanged += gcnew System::EventHandler(this, &StagesUI::ordersDisplay_SelectionChanged);
            // 
            // nameColumn
            // 
            this->nameColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
            this->nameColumn->HeaderText = L"Stage name";
            this->nameColumn->Name = L"nameColumn";
            this->nameColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->nameColumn->Width = 150;
            // 
            // StagesUI
            // 
            this->AcceptButton = this->deleteBtn;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->CancelButton = this->closeBtn;
            this->ClientSize = System::Drawing::Size(245, 346);
            this->Controls->Add(this->tableLayoutPanel1);
            this->MinimumSize = System::Drawing::Size(253, 373);
            this->Name = L"StagesUI";
            this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
            this->Text = L"Stages";
            this->Load += gcnew System::EventHandler(this, &StagesUI::StagesUI_Load);
            panel1->ResumeLayout(false);
            this->tableLayoutPanel1->ResumeLayout(false);
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ordersDisplay))->EndInit();
            this->ResumeLayout(false);

        }

    };
}
