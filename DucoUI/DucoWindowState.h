#pragma once

namespace DucoUI
{
    enum class DucoWindowState
    {
        EViewMode = 0,
        EEditMode,
        ENewMode,
        EAutoNewMode
    };
} // end namespace Duco
