#include "TableSorterByFloat.h"

#include <math.h>
#include <wchar.h>

using namespace DucoUI;
using namespace System;
using namespace System::Windows::Forms;

const float KFloatingPointComparisonPower = 10;

TableSorterByFloat::TableSorterByFloat(bool newAscending, int newColumnId)
: ascending(newAscending), columnId(newColumnId), accuracy(3), simpleString(true)
{
}

TableSorterByFloat::~TableSorterByFloat()
{
}

void
TableSorterByFloat::SetAccuracy(int newAccuracy)
{
    accuracy = newAccuracy;
}

void 
TableSorterByFloat::SetSimpleString(bool newSimpleString)
{
    simpleString = newSimpleString;
}

int 
TableSorterByFloat::Compare(System::Object^ firstObject, System::Object^ secondObject)
{
    DataGridViewRow^ row1 = static_cast<DataGridViewRow^>(firstObject);
    DataGridViewRow^ row2 = static_cast<DataGridViewRow^>(secondObject);
    if (row1 == row2)
        return 0;

    float row1Value = ConvertValueToFloat(row1,columnId);
    float row2Value = ConvertValueToFloat(row2,columnId);

    // Round up to 3 (KFloatingPointComparisonAccuracy) decimal places for comparision.
    long int row1IntValue = long int(row1Value * pow(KFloatingPointComparisonPower, accuracy));
    long int row2IntValue = long int(row2Value * pow(KFloatingPointComparisonPower, accuracy));

    return ascending ? row1IntValue - row2IntValue : row2IntValue - row1IntValue;
}

float
TableSorterByFloat::ConvertValueToFloat(DataGridViewRow^ row, int cellNumber)
{
    float returnValue (0);
    try
    {
        String^ object = nullptr;
        if (cellNumber != -1)
        {
            object = static_cast<String^>(row->Cells[cellNumber]->Value);
        }
        else
        {
            object = static_cast<String^>(row->HeaderCell->Value);
        }
        if (object != nullptr)
        {
            if (simpleString)
            {
                returnValue = Convert::ToSingle(object);
            }
            else
            {
                String^ numberOnlyChars = "";
                bool allNumbers (true);
                System::CharEnumerator^ it = object->GetEnumerator();
                while (it->MoveNext() && allNumbers)
                {
                    if (iswdigit (it->Current) || it->Current == '.')
                    {
                        numberOnlyChars += it->Current;
                    }
                    else
                    {
                        allNumbers = false;
                    }
                }
                returnValue = Convert::ToSingle(numberOnlyChars);
            }
        }
    }
    catch (Exception^)
    {
        returnValue = 0;
    }
    return returnValue;
}
