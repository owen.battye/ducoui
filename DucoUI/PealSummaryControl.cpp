#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"
#include "PealSummaryControl.h"
#include <Peal.h>
#include "DucoUtils.h"

#include "RingingDatabaseObserver.h"
#include "DucoWindowState.h"
#include "OpenObjectCallback.h"
#include "PictureListControl.h"
#include "PictureControl.h"
#include "RingerList.h"
#include "PrintBandSummaryUtil.h"
#include <DownloadedPeal.h>
#include "PealUI.h"

using namespace DucoUI;
int expandedHeight = 300;
int collapsedHeight = 30;

System::Void
PealSummaryControl::SetPealDetails(System::UInt32 thePealId, System::Boolean firstOrOnlyPeal)
{
    pealId = thePealId;
    Duco::Peal thePeal;
    if (database->FindPeal(Duco::ObjectId(pealId), thePeal, false))
    {
        pealSummary->Text = DucoUtils::ConvertString(thePeal.OnThisDayTitle(database->Database()));
        std::wstring text;
        thePeal.Title(text, database->Database());
        pealDetails->Text = DucoUtils::ConvertString(thePeal.Details(database->Database()));
    }
    if (firstOrOnlyPeal)
    {
        this->Height = expandedHeight;
        pealDetails->Visible = true;
        expandBtn->Text = "�";
    }
    else
    {
        expandBtn->Text = "�";
        this->Height = collapsedHeight;
        pealDetails->Visible = false;
    }
}

System::Void
PealSummaryControl::expandBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    System::Drawing::Size newSize = FindForm()->Size;
    pealDetails->Visible = !pealDetails->Visible;
    if (pealDetails->Visible)
    {
        newSize.Height += expandedHeight - collapsedHeight;
        expandBtn->Text = "�";
        this->Height = expandedHeight;
    }
    else
    {
        newSize.Height -= expandedHeight - collapsedHeight;
        expandBtn->Text = "�";
        this->Height = collapsedHeight;
    }

   
    FindForm()->Size = newSize;
}

System::Void 
PealSummaryControl::openBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    System::Windows::Forms::Form^ parentForm = FindForm();
    PealUI::ShowPeal(database, parentForm->MdiParent, Duco::ObjectId(pealId));
}
