#pragma once

namespace DucoUI
{

    public ref class AboutUI : public System::Windows::Forms::Form
    {
    public:
        AboutUI(DucoUI::DatabaseManager^ theDatabase);

    protected:
        ~AboutUI();
        !AboutUI();
        System::Void CloseButton_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void AboutUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void AboutBox_LinkClicked(System::Object^  sender, System::Windows::Forms::LinkClickedEventArgs^  e);
        System::Void AboutUI_Activated(System::Object^  sender, System::EventArgs^  e);

    protected: 
        DucoUI::DatabaseManager^      database;
        System::ComponentModel::Container^  components;
        System::Windows::Forms::Label^      versionLabel;
        System::Windows::Forms::RichTextBox^  aboutBox;

        System::Windows::Forms::Label^  fileNameLbl;
        System::Windows::Forms::Label^  databaseCreatedDateLbl;
        System::Windows::Forms::Label^  dbCreatedDateLbl;
        System::Windows::Forms::Label^  databaseVersionLbl;
        System::Windows::Forms::Label^  label3;



    protected: 
        Duco::DucoVersionNumber*        versionNo;

        System::Void InitializeComponent()
        {
            System::Windows::Forms::Label^ copyrightLabel;
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
            System::Windows::Forms::Label^ currentDatabaseLbl;
            this->databaseVersionLbl = (gcnew System::Windows::Forms::Label());
            this->aboutBox = (gcnew System::Windows::Forms::RichTextBox());
            this->label3 = (gcnew System::Windows::Forms::Label());
            this->databaseCreatedDateLbl = (gcnew System::Windows::Forms::Label());
            this->versionLabel = (gcnew System::Windows::Forms::Label());
            this->dbCreatedDateLbl = (gcnew System::Windows::Forms::Label());
            this->fileNameLbl = (gcnew System::Windows::Forms::Label());
            copyrightLabel = (gcnew System::Windows::Forms::Label());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            currentDatabaseLbl = (gcnew System::Windows::Forms::Label());
            tableLayoutPanel1->SuspendLayout();
            this->SuspendLayout();
            // 
            // copyrightLabel
            // 
            copyrightLabel->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            copyrightLabel->AutoSize = true;
            tableLayoutPanel1->SetColumnSpan(copyrightLabel, 2);
            copyrightLabel->Location = System::Drawing::Point(293, 3);
            copyrightLabel->Margin = System::Windows::Forms::Padding(3);
            copyrightLabel->Name = L"copyrightLabel";
            copyrightLabel->Size = System::Drawing::Size(159, 13);
            copyrightLabel->TabIndex = 1;
            copyrightLabel->Text = L"Rocketpole software 2009-2024";
            copyrightLabel->Click += gcnew System::EventHandler(this, &AboutUI::copyrightLabel_Click);
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 4;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 50)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 50)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->Controls->Add(this->databaseVersionLbl, 3, 4);
            tableLayoutPanel1->Controls->Add(this->aboutBox, 0, 1);
            tableLayoutPanel1->Controls->Add(this->label3, 2, 4);
            tableLayoutPanel1->Controls->Add(copyrightLabel, 2, 0);
            tableLayoutPanel1->Controls->Add(this->databaseCreatedDateLbl, 1, 4);
            tableLayoutPanel1->Controls->Add(this->versionLabel, 0, 0);
            tableLayoutPanel1->Controls->Add(this->dbCreatedDateLbl, 0, 4);
            tableLayoutPanel1->Controls->Add(currentDatabaseLbl, 0, 2);
            tableLayoutPanel1->Controls->Add(this->fileNameLbl, 0, 3);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Margin = System::Windows::Forms::Padding(3, 25, 3, 40);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 5;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
            tableLayoutPanel1->Size = System::Drawing::Size(455, 336);
            tableLayoutPanel1->TabIndex = 4;
            tableLayoutPanel1->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &AboutUI::tableLayoutPanel1_Paint);
            // 
            // databaseVersionLbl
            // 
            this->databaseVersionLbl->AutoSize = true;
            this->databaseVersionLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            this->databaseVersionLbl->Location = System::Drawing::Point(414, 319);
            this->databaseVersionLbl->Margin = System::Windows::Forms::Padding(3);
            this->databaseVersionLbl->Name = L"databaseVersionLbl";
            this->databaseVersionLbl->Size = System::Drawing::Size(38, 14);
            this->databaseVersionLbl->TabIndex = 10;
            this->databaseVersionLbl->Text = L"eeeee";
            // 
            // aboutBox
            // 
            tableLayoutPanel1->SetColumnSpan(this->aboutBox, 4);
            this->aboutBox->Dock = System::Windows::Forms::DockStyle::Fill;
            this->aboutBox->Location = System::Drawing::Point(3, 23);
            this->aboutBox->Name = L"aboutBox";
            this->aboutBox->ReadOnly = true;
            this->aboutBox->Size = System::Drawing::Size(449, 251);
            this->aboutBox->TabIndex = 2;
            this->aboutBox->Text = L"About text is generated";
            this->aboutBox->LinkClicked += gcnew System::Windows::Forms::LinkClickedEventHandler(this, &AboutUI::AboutBox_LinkClicked);
            this->aboutBox->TextChanged += gcnew System::EventHandler(this, &AboutUI::aboutBox_TextChanged);
            // 
            // label3
            // 
            this->label3->AutoSize = true;
            this->label3->Dock = System::Windows::Forms::DockStyle::Fill;
            this->label3->Location = System::Drawing::Point(247, 319);
            this->label3->Margin = System::Windows::Forms::Padding(3);
            this->label3->Name = L"label3";
            this->label3->Size = System::Drawing::Size(161, 14);
            this->label3->TabIndex = 9;
            this->label3->Text = L"Database version:";
            this->label3->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // databaseCreatedDateLbl
            // 
            this->databaseCreatedDateLbl->AutoSize = true;
            this->databaseCreatedDateLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            this->databaseCreatedDateLbl->Location = System::Drawing::Point(80, 319);
            this->databaseCreatedDateLbl->Margin = System::Windows::Forms::Padding(3);
            this->databaseCreatedDateLbl->Name = L"databaseCreatedDateLbl";
            this->databaseCreatedDateLbl->Size = System::Drawing::Size(161, 14);
            this->databaseCreatedDateLbl->TabIndex = 8;
            this->databaseCreatedDateLbl->Text = L"dddddd";
            // 
            // versionLabel
            // 
            this->versionLabel->AutoSize = true;
            tableLayoutPanel1->SetColumnSpan(this->versionLabel, 2);
            this->versionLabel->Dock = System::Windows::Forms::DockStyle::Fill;
            this->versionLabel->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
            this->versionLabel->Location = System::Drawing::Point(3, 3);
            this->versionLabel->Margin = System::Windows::Forms::Padding(3);
            this->versionLabel->Name = L"versionLabel";
            this->versionLabel->Size = System::Drawing::Size(238, 14);
            this->versionLabel->TabIndex = 0;
            this->versionLabel->Text = L"Duco version:";
            this->versionLabel->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
            // 
            // dbCreatedDateLbl
            // 
            this->dbCreatedDateLbl->AutoSize = true;
            this->dbCreatedDateLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            this->dbCreatedDateLbl->Location = System::Drawing::Point(3, 319);
            this->dbCreatedDateLbl->Margin = System::Windows::Forms::Padding(3);
            this->dbCreatedDateLbl->Name = L"dbCreatedDateLbl";
            this->dbCreatedDateLbl->Size = System::Drawing::Size(71, 14);
            this->dbCreatedDateLbl->TabIndex = 7;
            this->dbCreatedDateLbl->Text = L"Created date:";
            // 
            // currentDatabaseLbl
            // 
            currentDatabaseLbl->AutoSize = true;
            tableLayoutPanel1->SetColumnSpan(currentDatabaseLbl, 3);
            currentDatabaseLbl->Location = System::Drawing::Point(3, 280);
            currentDatabaseLbl->Margin = System::Windows::Forms::Padding(3);
            currentDatabaseLbl->Name = L"currentDatabaseLbl";
            currentDatabaseLbl->Size = System::Drawing::Size(111, 13);
            currentDatabaseLbl->TabIndex = 2;
            currentDatabaseLbl->Text = L"Current database info:";
            // 
            // fileNameLbl
            // 
            this->fileNameLbl->AutoSize = true;
            tableLayoutPanel1->SetColumnSpan(this->fileNameLbl, 4);
            this->fileNameLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            this->fileNameLbl->Location = System::Drawing::Point(3, 299);
            this->fileNameLbl->Margin = System::Windows::Forms::Padding(3);
            this->fileNameLbl->Name = L"fileNameLbl";
            this->fileNameLbl->Size = System::Drawing::Size(449, 14);
            this->fileNameLbl->TabIndex = 1;
            this->fileNameLbl->Text = L"replaced with filename";
            // 
            // AboutUI
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(455, 336);
            this->Controls->Add(tableLayoutPanel1);
            this->MaximizeBox = false;
            this->Name = L"AboutUI";
            this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
            this->Text = L"About Duco";
            this->Activated += gcnew System::EventHandler(this, &AboutUI::AboutUI_Activated);
            this->Load += gcnew System::EventHandler(this, &AboutUI::AboutUI_Load);
            tableLayoutPanel1->ResumeLayout(false);
            tableLayoutPanel1->PerformLayout();
            this->ResumeLayout(false);

        }
private: System::Void aboutBox_TextChanged(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void copyrightLabel_Click(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void tableLayoutPanel1_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
}
};
}
