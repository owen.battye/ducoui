#pragma once

namespace Duco
{
    class Peal;
}

namespace DucoUI
{
    ref class PealFeeObject;
    ref class DatabaseManager;
    ref class RingerEventArgs;

    public ref class PealFeeList : public System::Windows::Forms::Panel
	{
	public:
        PealFeeList(DucoUI::DatabaseManager^ theManager, Duco::Peal*const thePeal);

        System::Void SetPeal();

        // current peal must have been updated first for these to work.
        System::Void UpdateNoOfBells(unsigned int noOfBells);

        event System::EventHandler^ dataChangedEventHandler;
        System::Void PealFeeObjectChanged(System::Object^  sender, System::EventArgs^  e);

    protected:
        virtual void OnRingerDataChanged(RingerEventArgs^ e);

        ~PealFeeList();
        !PealFeeList();
		void InitializeComponent();

        PealFeeObject^ CreateRingerControl(unsigned int bellnumber);

	private:
        DucoUI::DatabaseManager^      database;
		System::ComponentModel::Container^  components;

        Duco::Peal* const                   currentPeal;
	};
}
