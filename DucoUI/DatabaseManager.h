#pragma once

#include "ImportExportProgressWrapper.h"
#include "EventType.h"
#include <map>
#include <vector>
#include <set>
#include <RingerPealDates.h>

namespace Duco
{
    class Association;
    class Composition;
    class DatabaseSettings;
    class DoveObject;
    class DoveDatabase;
    class DucoConfiguration;
    class PerformanceDate;
    class Method;
    class MethodSeries;
    class Peal;
    class Picture;
    class ProgressCallback;
    class RenumberProgressCallback;
    class Ringer;
    class RingingDatabase;
    class RingingObject;
    class StatisticFilters;
    class Tower;
    class UnpaidPealFeeData;
    class PealLengthInfo;
}

namespace DucoUI
{
    interface class RingingDatabaseObserver;
    ref class WindowsSettings;
    ref class Form1;

    public ref class CompletedEventArgs : public System::EventArgs
    {
    public:
        property System::Boolean Import;
    };

    public ref class DatabaseManager : public System::Object, public DucoUI::ImportExportProgressCallbackUI
    {
    public:
        // Constructors
        DatabaseManager(DucoUI::Form1^ observer, System::Windows::Forms::IWin32Window^ newOwner);

        // from ImportExportProgressCallback
        virtual System::Void InitialisingImportExport(System::Boolean internalising, unsigned int versionNumber, size_t totalNumberOfObjects);
        virtual System::Void ObjectProcessed(System::Boolean internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase);
        virtual System::Void ImportExportComplete(System::Boolean internalising);
        virtual System::Void ImportExportFailed(System::Boolean internalising, int errorCode);

        // New functions
        System::Void RemoveObserver(RingingDatabaseObserver^ obs);
        System::Void AddObserver(RingingDatabaseObserver^ obs);
        System::Void NotifyImportComplete();
        inline System::Void ConvertStarted();
        System::Void CallObservers(DucoUI::TEventType eventId, Duco::TObjectType type, Duco::ObjectId id, Duco::ObjectId id2);
        System::Boolean CheckRebuildRecommended();

        Duco::RingingDatabase& Database();
        Duco::DatabaseSettings& Settings();
        Duco::DucoConfiguration& Config();
        DucoUI::WindowsSettings^ WindowsSettings();

        System::String^ DoveDataFile();
        System::Void SetDoveDataFile(System::String^ newValue);
        System::String^ XmlMethodCollectionFile();
        System::Void SetXmlMethodCollectionFile(System::String^ newValue);
        System::DateTime^ DatabaseCreatedDate();

        System::Void CreateDatabase(System::Boolean pealDatabase);
        System::Void DeleteDatabase();
        inline System::Boolean DatabaseOpen();
        System::Void ClearDatabase(System::Boolean quarterPealDb, System::Boolean createDefaults);
        System::Void Reset();

        System::Boolean DataChanged();
        System::String^ PerformanceString(System::Boolean includePlurality);

        inline System::String^ Filename();
        System::String^ BackupFileName();

        System::Void NoDefaultRinger();
        System::Boolean CheckDefaultRinger();

        System::Boolean UppercaseAssociations(Duco::ProgressCallback* const callback);
        System::Boolean UppercaseCities(Duco::ProgressCallback* const callback);
        System::Boolean CapitaliseFields(Duco::ProgressCallback* const callback, bool methodName, bool MethodType, bool county, bool dedication, bool ringerNames);
        System::Boolean RemoveDuplicateRings(Duco::ProgressCallback* const callback);
        System::Boolean ReplaceMethodTypes(Duco::ProgressCallback* const callback);
        System::Boolean RemoveDuplicateMethods(Duco::ProgressCallback* const callback);
        System::Boolean ImportMissingTenorKeys(const Duco::DoveDatabase& doveDatabase);

        System::Boolean Externalise(Duco::ImportExportProgressCallback* newCallback, bool& fileNameValid);
        System::Boolean Externalise(System::String^ filename, Duco::ImportExportProgressCallback* newCallback, bool& fileNameValid);
        System::Boolean Internalise(Duco::ImportExportProgressCallback* newCallback, System::String^ fileName);
        inline bool InternaliseExternaliseInProgress();
        inline bool ExternaliseInProgress();

        // General database functions
        System::Boolean StartEdit(Duco::TObjectType objectType, const Duco::ObjectId& objectId);
        System::Void CancelEdit(Duco::TObjectType objectType, const Duco::ObjectId& objectId);
        System::Boolean NothingInEditMode();
        System::String^ FullName(Duco::TObjectType objectType, const Duco::ObjectId& objectId);

        Duco::ObjectId AddRinger(Duco::Ringer& newRinger);
        Duco::ObjectId AddRinger(System::String^ newRingerName);
        System::Boolean DeleteRinger(const Duco::ObjectId& ringerId, bool suspendObservers);
        System::Boolean UpdateRinger(const Duco::Ringer& newRinger, bool notifyObservers);
        System::Boolean SetRingerGender(const Duco::ObjectId& ringerId, bool male);
        Duco::ObjectId SuggestRingers(System::String^ newRingerName, const Duco::PerformanceDate& performanceDate, std::set<Duco::RingerPealDates>& nearMatches);
        System::Collections::Generic::List<System::Int64>^ SuggestAllRingers(System::String^ newRingerName);
        unsigned int FindOrderNumber(System::String^% methodName);
        System::String^ RingerName(const Duco::ObjectId& ringerId);
        System::String^ RingerName(const Duco::ObjectId& ringerId, const Duco::PerformanceDate& pealDate);

        System::Boolean ReplaceRinger(const Duco::ObjectId& oldRingerId, const Duco::ObjectId& newRingerId, bool deleteOldRinger);
        System::Boolean LinkRingers(const Duco::ObjectId& ringerIdOne, const Duco::ObjectId& ringerIdTwo);
        System::Boolean FirstRinger(Duco::ObjectId& objectId, System::Boolean setIndex);
        System::Boolean LastRinger(Duco::ObjectId& objectId);
        System::Boolean FindRinger(const Duco::ObjectId& objectId, Duco::Ringer& returnRinger, System::Boolean setIndex);
        System::Boolean NextRinger(Duco::ObjectId& objectId, System::Boolean forwards, System::Boolean setIndex);
        System::Boolean ForwardMultipleRingers(Duco::ObjectId& objectId, System::Boolean forwards);
        System::Boolean HasConducted(const Duco::ObjectId& objectId);
        System::Boolean FindRingerIdDuringDownload(System::String^ ringerName, Duco::ObjectId& ringerId);

        System::Boolean AddTower(Duco::Tower& newTower);
        System::Boolean AddTower(const Duco::DoveObject& newTower);
        Duco::ObjectId AddNewTower(Duco::Tower& newTower);
        System::Boolean DeleteTower(const Duco::ObjectId& towerId, System::Boolean suspendObservers);
        System::Boolean DeleteRing(const Duco::ObjectId& towerId, const Duco::ObjectId& ringId, System::Boolean suspendObservers);
        System::Boolean UpdateTower(const Duco::Tower& tower, System::Boolean notifyObservers);
        System::Boolean FirstTower(Duco::ObjectId& objectId, System::Boolean setIndex);
        System::Boolean LastTower(Duco::ObjectId& objectId);
        System::Boolean FindTower(const Duco::ObjectId& objectId, Duco::Tower& theTower, System::Boolean setIndex);
        Duco::ObjectId SuggestTower(System::String^ title, unsigned int noOfBells);
        System::Boolean NextTower(Duco::ObjectId& objectId, System::Boolean forwards, System::Boolean setIndex);
        System::Boolean ForwardMultipleTowers(Duco::ObjectId& objectId, System::Boolean forwards);
        unsigned int NumberOfBellsRungInTower(const Duco::ObjectId& towerId);
        unsigned int NumberOfPealsRungOnRing(const Duco::ObjectId& towerId, const Duco::ObjectId& ringId);
        System::Boolean MergeTowerRings(const Duco::ObjectId& towerId, const Duco::Tower& otherTower);
        System::Boolean ReplaceTower(const Duco::ObjectId& oldTowerId, const Duco::ObjectId& newTowerId);
        System::String^ TowerName(const Duco::ObjectId& towerId);

        Duco::ObjectId AddMethod(Duco::Method& newMethod);
        Duco::ObjectId AddMethod(System::String^ fullMethodName);
        System::Boolean UpdateMethod(const Duco::Method& method, System::Boolean notifyObservers);
        System::Boolean DeleteMethod(const Duco::ObjectId& methodId, System::Boolean suspendObservers);
        System::Boolean FirstMethod(Duco::ObjectId& objectId, System::Boolean setIndex);
        System::Boolean LastMethod(Duco::ObjectId& objectId);
        System::Boolean FindMethod(const Duco::ObjectId& objectId, Duco::Method& theMethod, System::Boolean setIndex);
        System::Boolean NextMethod(Duco::ObjectId& objectId, System::Boolean forwards, System::Boolean setIndex);
        System::Boolean ForwardMultipleMethods(Duco::ObjectId& objectId, System::Boolean forwards);
        System::Boolean FirstPealInMethod(const Duco::ObjectId& methodId, Duco::PerformanceDate& pealDate);
        System::Collections::Generic::List<System::UInt64>^ PealsInMethod(const Duco::ObjectId& methodId, Duco::PerformanceDate& pealDate);
        System::Boolean UpdateNotation(const Duco::ObjectId& methodId, const std::wstring& newFullNotation);
        System::Boolean SetNumberOfLeads(const Duco::ObjectId& methodId, unsigned int noOfLeads);
        unsigned int ImportMethodPlaceNotations(Duco::ProgressCallback* const callback, System::String^ methodFileName, const std::map<std::wstring, Duco::ObjectId>& methodsToUpdate);
        System::Boolean FindMethod(unsigned int noOfBells, const std::wstring& placeNotation, Duco::Method& theMethod);
        Duco::ObjectId SuggestMethod(System::String^ methodName);
        System::Boolean MethodNameMatchesOrder(System::String^ fullMethodName, unsigned int noOfBells);

        System::Boolean FindOrderNumber(System::String^ name, unsigned int& order, bool& dualOrder);
        System::Boolean FindOrderName(unsigned int order, System::String^% newName);
        unsigned int HighestValidOrderNumber();
        unsigned int LowestValidOrderNumber();
        unsigned int LowestValidOrderNumber(const Duco::ObjectId& towerId, const Duco::ObjectId& ringId);
        System::Boolean AddOrderName(unsigned int order, System::String^% newName);
        System::Void UpdateOrderName(unsigned int order, System::String^ newName, System::Boolean notifyObservers);
        System::Boolean RemoveOrderName(unsigned int order);
        System::Void GetAllMethodOrders(std::set<unsigned int>& methodOrders);
        System::Void GetAllOrders(std::vector<unsigned int>& orders);
        System::Boolean IsDualOrder(System::String^% orderNameToFind);

        Duco::ObjectId AddPeal(Duco::Peal& newPeal);
        System::Void DeletePeal(const Duco::ObjectId& pealId, System::Boolean suspendObservers);
        System::Boolean UpdatePeal(const Duco::Peal& newPeal, System::Boolean notifyObservers);
        System::Boolean FirstPeal(Duco::ObjectId& objectId, System::Boolean setIndex);
        System::Boolean LastPeal(Duco::ObjectId& objectId);
        System::String^ LastDateString();
        System::Boolean FindPeal(const Duco::ObjectId& objectId, Duco::Peal& thePeal, System::Boolean setIndex);
        Duco::ObjectId FindPeal(const Duco::PerformanceDate& pealDate);
        System::Boolean NextPeal(Duco::ObjectId& objectId, System::Boolean forwards, System::Boolean setIndex);
        System::Boolean ForwardMultiplePeals(Duco::ObjectId& objectId, System::Boolean forwards);
        System::Void GetAllComposersByPealCount(std::map<std::wstring, Duco::PealLengthInfo>& sortedComposerCounts, bool ignoreBracketed, const Duco::StatisticFilters& filters);
        size_t NoOfPealsAsConductor(const Duco::ObjectId& ringerId, const Duco::PerformanceDate& dateUntil);
        size_t NoOfPealsInTower(const Duco::ObjectId& towerId, const Duco::ObjectId& upToAndIncludePealId);
        System::Boolean FindPealsWithUnpaidFees(const Duco::ObjectId& feePayerId, const Duco::ObjectId& associationId, unsigned int year, std::set<Duco::UnpaidPealFeeData>& pealIds);
        System::Void GetDateRange(System::DateTime^% startDate, System::DateTime^% endDate);
        Duco::ObjectId FindLastPealBeforeDate(System::DateTime^ lastPealDate);
        System::Collections::Generic::HashSet<System::String^>^ RemoveExistingBellBoardPealIds(System::Collections::Generic::Queue<System::String^>^ allPealIds); // returns known peal Ids and passed list becomes the unknown ones
        System::Boolean AnyOnThisDay();
        System::Void OnThisDay(System::Collections::Generic::List<System::UInt32>^ pealIds);
        System::String^ PealMap(size_t& numberOfTowersPlotted, size_t& numberOfTowersNotPlotted, System::Boolean zeroPerformances, System::Boolean onePerformance, System::Boolean multiplePerformances, System::Boolean europeOnly, System::Boolean combinedLinked, System::Boolean useFelsteadPealCount, System::DateTime^ startDate, System::DateTime^ endDate, System::String^ county);
        System::String^ SummaryTitle(const Duco::ObjectId& pealId);
        System::String^ BellboardId(const Duco::ObjectId& pealId);
        System::Boolean AddWebReferencesToPeal(const Duco::ObjectId& similarId, System::String^ bellboardId, const std::wstring& ringingWorldReference);
        System::Void ClearInvalidTenorKeys(Duco::ProgressCallback* callback);
        System::Void SetTowersWithHandbellPealsAsHandbells();

        Duco::ObjectId AddMethodSeries(Duco::MethodSeries& newMethodSeries);
        System::Boolean UpdateMethodSeries(const Duco::MethodSeries& methodSeries, System::Boolean notifyObservers);
        System::Boolean DeleteMethodSeries(const Duco::ObjectId& methodIdSeries, System::Boolean suspendObservers);
        System::Boolean FirstMethodSeries(Duco::ObjectId& objectId, System::Boolean setIndex);
        System::Boolean LastMethodSeries(Duco::ObjectId& objectId);
        System::Boolean FindMethodSeries(const Duco::ObjectId& objectId, Duco::MethodSeries& theSeries, System::Boolean setIndex);
        System::Boolean NextMethodSeries(Duco::ObjectId& objectId, System::Boolean forwards, System::Boolean setIndex);
        System::Boolean ForwardMultipleMethodSeries(Duco::ObjectId& objectId, System::Boolean forwards);
        System::Collections::Generic::List<System::UInt64>^ PealsInSeries(const Duco::ObjectId& seriesId, Duco::PerformanceDate& pealDate);
        System::Boolean LastPealInMethodSeries(const Duco::ObjectId& seriesId, Duco::PerformanceDate& pealDate);
        System::Boolean SuggestMethodSeries(System::String^ methods, unsigned int order, Duco::ObjectId& methodSeriesId, std::set<Duco::ObjectId>& methodIds);

        Duco::ObjectId AddComposition(Duco::Composition& newComposition);
        System::Boolean UpdateComposition(const Duco::Composition& composition, System::Boolean notifyObservers);
        System::Boolean DeleteComposition(const Duco::ObjectId& compositionId, System::Boolean suspendObservers);
        System::Boolean FindComposition(const Duco::ObjectId& objectId, Duco::Composition& theComposition, System::Boolean setIndex);
        System::Boolean FirstComposition(Duco::ObjectId& objectId, System::Boolean setIndex);
        System::Boolean LastComposition(Duco::ObjectId& objectId);
        System::Boolean NextComposition(Duco::ObjectId& objectId, System::Boolean forwards, System::Boolean setIndex);
        System::Boolean ForwardMultipleCompositions(Duco::ObjectId& objectId, System::Boolean forwards);
        System::String^ FindCompositionFullName(const Duco::ObjectId& objectId);

        Duco::ObjectId AddAssociation(Duco::Association& newAssociation);
        Duco::ObjectId AddAssociation(System::String^ newAssociationName);
        System::Boolean DeleteAssociation(const Duco::ObjectId& associationId, System::Boolean suspendObservers);
        System::Boolean UpdateAssociation(const Duco::Association& association, System::Boolean notifyObservers);
        System::String^ FindAssociationName(const Duco::ObjectId& objectId);
        System::Boolean FindAssociation(const Duco::ObjectId& objectId, Duco::Association& theAssociation, System::Boolean setIndex);
        System::Boolean FirstAssociation(Duco::ObjectId& objectId, System::Boolean setIndex);
        System::Boolean LastAssociation(Duco::ObjectId& objectId);
        System::Boolean NextAssociation(Duco::ObjectId& objectId, System::Boolean forwards, System::Boolean setIndex);
        System::Boolean ForwardMultipleAssociation(Duco::ObjectId& objectId, System::Boolean forwards);
        Duco::ObjectId SuggestAssociation(System::String^ name);
        System::Boolean RemoveDuplicateAssociations();
        size_t NumberOfPealsByAssociation(const Duco::ObjectId& associationId);
        System::Boolean ReplaceAssociation(Duco::ProgressCallback* callback, const Duco::ObjectId& oldAssociation, const Duco::ObjectId& newAssociation);

        System::Boolean DeletePictures(const std::set<Duco::ObjectId>& pictureIds, System::Boolean suspendObservers);
        System::Boolean RemoveAndDeletePictures(const std::set<Duco::ObjectId>& pictureIds);
        Duco::ObjectId AddPicture(Duco::Picture& newPicture);
        System::Boolean FindPicture(const Duco::ObjectId& objectId, Duco::Picture& thePicture, System::Boolean setIndex);
        System::Boolean FindPicture(const Duco::ObjectId& pictureId, System::Collections::Generic::List<System::UInt64>^% pealIds, System::Collections::Generic::List<System::UInt64>^% towerIds);
        System::Boolean FirstPicture(Duco::ObjectId& objectId, System::Boolean setIndex);
        System::Boolean LastPicture(Duco::ObjectId& objectId);
        System::Boolean NextPicture(Duco::ObjectId& objectId, System::Boolean forwards, System::Boolean setIndex);
        System::Boolean ForwardMultiplePictures(Duco::ObjectId& objectId, System::Boolean forwards);
        System::Boolean UpdatePicture(const Duco::Picture& updatedPicture, bool notifyObservers);

        System::Boolean UniqueObject(const Duco::RingingObject& object, Duco::TObjectType objectType);
        Duco::ObjectId FindDuplicateObject(const Duco::RingingObject& object, Duco::TObjectType objectType);
        size_t NumberOfObjects(Duco::TObjectType objectType);
        size_t NumberOfValidObjects(Duco::TObjectType objectType);
        System::Boolean ObjectExists(Duco::TObjectType objectType, const Duco::ObjectId& id);
        size_t CheckBellBoardForUpdatedReferences(Duco::ProgressCallback* callBack, bool rwPagesOnly, bool& cancellationFlag);

        void RebuildDatabase(Duco::RenumberProgressCallback* callback);
        void SwapPeals(const Duco::ObjectId& pealId1, const Duco::ObjectId& pealId2, unsigned int selectedIndex);

    protected:
        !DatabaseManager();
        ~DatabaseManager();
        System::Void ProcessData(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void ProcessDataInternalise(System::ComponentModel::BackgroundWorker^ worker, System::ComponentModel::DoWorkEventArgs^ e);
        System::Void ProcessDataExternalise(System::ComponentModel::BackgroundWorker^ worker, System::ComponentModel::DoWorkEventArgs^ e);
        System::Void backgroundWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void backgroundWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);

        void HideBackupFile();

    private:
        System::Windows::Forms::IWin32Window^       owner;
        DucoUI::ImportExportProgressWrapper*        progressWrapper;
        DucoUI::WindowsSettings^                    settings;
        Duco::DucoConfiguration*                    config;
        DucoUI::Form1^                              mainForm;
        System::Collections::ArrayList^             observerList;
        Duco::RingingDatabase*                      database;
        System::String^                             databaseFilename;

        System::ComponentModel::BackgroundWorker^   backgroundWorker;
        Duco::ImportExportProgressCallback*         importExportCallback;
        System::Boolean                             import;
        System::Boolean                             importFromOtherFormatInProgress;

        Duco::ObjectId*                             editingPealNo;
        Duco::ObjectId*                             editingMethodNo;
        Duco::ObjectId*                             editingMethodSeriesNo;
        Duco::ObjectId*                             editingTowerNo;
        Duco::ObjectId*                             editingRingerNo;
        Duco::ObjectId*                             editingCompositionNo;
        Duco::ObjectId*                             editingAssociationNo;
    };

System::String^
DatabaseManager::Filename()
{
    return databaseFilename;
}

bool 
DatabaseManager::DatabaseOpen()
{
    return database != NULL;
}

bool
DatabaseManager::InternaliseExternaliseInProgress()
{
    return backgroundWorker->IsBusy || importFromOtherFormatInProgress;
}

bool
DatabaseManager::ExternaliseInProgress()
{
    return backgroundWorker->IsBusy && !import;
}

void 
DatabaseManager::ConvertStarted()
{
    importFromOtherFormatInProgress = true;
}

}

