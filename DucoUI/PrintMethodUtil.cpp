#include "PrintMethodUtil.h"

#include "DucoUtils.h"
#include <Method.h>
#include "DatabaseManager.h"

using namespace System;
using namespace System::Drawing;
using namespace Duco;
using namespace DucoUI;

PrintMethodUtil::PrintMethodUtil(DatabaseManager^ theDatabase, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
:	PrintUtilBase(theDatabase, 12, printArgs, false)
{

}

void
PrintMethodUtil::SetObjects(const Duco::Method* theMethod, System::Drawing::Image^ theImage)
{
    currentMethod = theMethod;
    methodImage = theImage;
}

System::Void
PrintMethodUtil::printObject(System::Drawing::Printing::PrintPageEventArgs^ args, System::Drawing::Printing::PrinterSettings^ printSettings)
{
    // Print title of method.
    String^ printBuffer = DucoUtils::ConvertString(currentMethod->FullName(database->Database()));
    args->Graphics->DrawString(printBuffer, boldFont, Brushes::Black, leftMargin, yPos, gcnew StringFormat );
    yPos += lineHeight;
    printBuffer = DucoUtils::ConvertString(currentMethod->PlaceNotation());
    args->Graphics->DrawString(printBuffer, normalFont, Brushes::Black, leftMargin, yPos, gcnew StringFormat );
    yPos += 2*lineHeight;

    PrintMethod(args->MarginBounds, args->Graphics);

    // Add Duco footer.
    addDucoFooter(smallFont, gcnew StringFormat, args);
}

System::Void
PrintMethodUtil::PrintMethod(System::Drawing::Rectangle^ marginBounds, System::Drawing::Graphics^ grphcs)
{
    float maxPrintHeight = float(marginBounds->Height) - (yPos - float(marginBounds->Top)) - lineHeight;
    float maxPrintWidth = float(marginBounds->Width);
    float imagePrintHeight = 0.0F;
    float imagePrintWidth = 0.0F;

    SizeF imageSize = methodImage->Size;
    if (imageSize.Width > maxPrintWidth || imageSize.Height > maxPrintHeight) // Fill page
    {
        // Calculate image ratio
        float imageRatioHW = float(methodImage->Size.Height) / float(methodImage->Size.Width);
        float imageRatioWH = float(methodImage->Size.Width) / float(methodImage->Size.Height);

        imagePrintHeight = maxPrintHeight;
        imagePrintWidth = maxPrintWidth;
        if (imagePrintHeight > (imagePrintWidth * imageRatioHW))
        {
            imagePrintHeight = imagePrintWidth * imageRatioHW;
        }
        if (imagePrintWidth > (imagePrintHeight * imageRatioWH))
        {
            imagePrintWidth = imagePrintHeight * imageRatioWH;
        }
    }
    else
    {
        imagePrintHeight = float(methodImage->Size.Height);
        imagePrintWidth = float(methodImage->Size.Width);
        while ((imagePrintHeight*2 < maxPrintHeight) && (imagePrintWidth*2 < maxPrintWidth))
        {
            if ((imagePrintHeight*2) < maxPrintHeight)
                imagePrintHeight *= 2;
            if ((imagePrintWidth*2) < maxPrintWidth)
                imagePrintWidth *= 2;
        }
    }
    RectangleF imageRectangle (leftMargin, yPos, imagePrintWidth, imagePrintHeight);

    //Print Method
    grphcs->DrawImage(methodImage,imageRectangle);
}