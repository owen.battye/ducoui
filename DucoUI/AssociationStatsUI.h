#pragma once
namespace DucoUI
{
    public ref class AssociationStatsUI : public System::Windows::Forms::Form
    {
    public:
        AssociationStatsUI(DucoUI::DatabaseManager^ theDatabase);

    protected:
        ~AssociationStatsUI();
        !AssociationStatsUI();

        System::Void AssociationStatsUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void filtersBtn_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void backgroundGenerator_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void backgroundGenerator_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void backgroundGenerator_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);
        System::Void dataGridView_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);
        System::Void dataGridView_CellContentClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);

        System::Void combineBtn_Click(System::Object^ sender, System::EventArgs^ e);

    private:
        DucoUI::DatabaseManager^                        database;
        Duco::StatisticFilters*                         filters;
        System::ComponentModel::Container^              components;
        System::Windows::Forms::ToolStripProgressBar^   progressBar;
        System::Windows::Forms::ToolStripStatusLabel^   noOfAssociations;
        System::ComponentModel::BackgroundWorker^       backgroundGenerator;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ idColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ associationColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ societyCount;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ changesColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ timeColumn;
    private: System::Windows::Forms::ToolStripButton^ combineBtn;
        System::Windows::Forms::DataGridView^           dataGridView;

        void InitializeComponent()
        {
            System::Windows::Forms::StatusStrip^ statusStrip1;
            System::Windows::Forms::ToolStrip^ toolStrip1;
            System::Windows::Forms::ToolStripButton^ filtersBtn;
            System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(AssociationStatsUI::typeid));
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle3 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle4 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            this->progressBar = (gcnew System::Windows::Forms::ToolStripProgressBar());
            this->noOfAssociations = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->dataGridView = (gcnew System::Windows::Forms::DataGridView());
            this->idColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->associationColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->societyCount = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->changesColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->timeColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->backgroundGenerator = (gcnew System::ComponentModel::BackgroundWorker());
            this->combineBtn = (gcnew System::Windows::Forms::ToolStripButton());
            statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
            filtersBtn = (gcnew System::Windows::Forms::ToolStripButton());
            statusStrip1->SuspendLayout();
            toolStrip1->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->BeginInit();
            this->SuspendLayout();
            // 
            // statusStrip1
            // 
            statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) { this->progressBar, this->noOfAssociations });
            statusStrip1->Location = System::Drawing::Point(0, 365);
            statusStrip1->Name = L"statusStrip1";
            statusStrip1->Size = System::Drawing::Size(715, 22);
            statusStrip1->TabIndex = 1;
            statusStrip1->Text = L"statusStrip1";
            // 
            // progressBar
            // 
            this->progressBar->Name = L"progressBar";
            this->progressBar->Size = System::Drawing::Size(100, 16);
            // 
            // noOfAssociations
            // 
            this->noOfAssociations->Name = L"noOfAssociations";
            this->noOfAssociations->Size = System::Drawing::Size(598, 17);
            this->noOfAssociations->Spring = true;
            this->noOfAssociations->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // toolStrip1
            // 
            toolStrip1->GripStyle = System::Windows::Forms::ToolStripGripStyle::Hidden;
            toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) { filtersBtn, this->combineBtn });
            toolStrip1->Location = System::Drawing::Point(0, 0);
            toolStrip1->Name = L"toolStrip1";
            toolStrip1->Size = System::Drawing::Size(715, 25);
            toolStrip1->TabIndex = 3;
            toolStrip1->Text = L"toolStrip1";
            // 
            // filtersBtn
            // 
            filtersBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
            filtersBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"filtersBtn.Image")));
            filtersBtn->ImageTransparentColor = System::Drawing::Color::Magenta;
            filtersBtn->Name = L"filtersBtn";
            filtersBtn->Size = System::Drawing::Size(42, 22);
            filtersBtn->Text = L"&Filters";
            filtersBtn->Click += gcnew System::EventHandler(this, &AssociationStatsUI::filtersBtn_Click);
            // 
            // dataGridView
            // 
            this->dataGridView->AllowUserToAddRows = false;
            this->dataGridView->AllowUserToDeleteRows = false;
            this->dataGridView->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(5) {
                this->idColumn,
                    this->associationColumn, this->societyCount, this->changesColumn, this->timeColumn
            });
            this->dataGridView->Dock = System::Windows::Forms::DockStyle::Fill;
            this->dataGridView->Location = System::Drawing::Point(0, 25);
            this->dataGridView->Name = L"dataGridView";
            this->dataGridView->ReadOnly = true;
            this->dataGridView->RowHeadersWidth = 55;
            this->dataGridView->Size = System::Drawing::Size(715, 340);
            this->dataGridView->TabIndex = 2;
            this->dataGridView->CellContentClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &AssociationStatsUI::dataGridView_CellContentClick);
            this->dataGridView->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &AssociationStatsUI::dataGridView_ColumnHeaderMouseClick);
            // 
            // idColumn
            // 
            this->idColumn->HeaderText = L"ID";
            this->idColumn->Name = L"idColumn";
            this->idColumn->ReadOnly = true;
            this->idColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->idColumn->Visible = false;
            // 
            // associationColumn
            // 
            this->associationColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
            this->associationColumn->HeaderText = L"Association";
            this->associationColumn->Name = L"associationColumn";
            this->associationColumn->ReadOnly = true;
            this->associationColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            // 
            // societyCount
            // 
            this->societyCount->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->societyCount->HeaderText = L"Performances";
            this->societyCount->Name = L"societyCount";
            this->societyCount->ReadOnly = true;
            this->societyCount->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->societyCount->Width = 97;
            // 
            // changesColumn
            // 
            this->changesColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            dataGridViewCellStyle3->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->changesColumn->DefaultCellStyle = dataGridViewCellStyle3;
            this->changesColumn->HeaderText = L"Changes";
            this->changesColumn->Name = L"changesColumn";
            this->changesColumn->ReadOnly = true;
            this->changesColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->changesColumn->Width = 74;
            // 
            // timeColumn
            // 
            this->timeColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            dataGridViewCellStyle4->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->timeColumn->DefaultCellStyle = dataGridViewCellStyle4;
            this->timeColumn->HeaderText = L"Time";
            this->timeColumn->Name = L"timeColumn";
            this->timeColumn->ReadOnly = true;
            this->timeColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->timeColumn->Width = 55;
            // 
            // backgroundGenerator
            // 
            this->backgroundGenerator->WorkerReportsProgress = true;
            this->backgroundGenerator->WorkerSupportsCancellation = true;
            this->backgroundGenerator->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &AssociationStatsUI::backgroundGenerator_DoWork);
            this->backgroundGenerator->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &AssociationStatsUI::backgroundGenerator_ProgressChanged);
            this->backgroundGenerator->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &AssociationStatsUI::backgroundGenerator_RunWorkerCompleted);
            // 
            // combineBtn
            // 
            this->combineBtn->Checked = true;
            this->combineBtn->CheckOnClick = true;
            this->combineBtn->CheckState = System::Windows::Forms::CheckState::Checked;
            this->combineBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
            this->combineBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"combineBtn.Image")));
            this->combineBtn->ImageTransparentColor = System::Drawing::Color::Magenta;
            this->combineBtn->Name = L"combineBtn";
            this->combineBtn->Size = System::Drawing::Size(60, 22);
            this->combineBtn->Text = L"Combine";
            this->combineBtn->Click += gcnew System::EventHandler(this, &AssociationStatsUI::combineBtn_Click);
            // 
            // AssociationStatsUI
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->AutoSize = true;
            this->ClientSize = System::Drawing::Size(715, 387);
            this->Controls->Add(this->dataGridView);
            this->Controls->Add(toolStrip1);
            this->Controls->Add(statusStrip1);
            this->Name = L"AssociationStatsUI";
            this->Text = L"Associations";
            this->Load += gcnew System::EventHandler(this, &AssociationStatsUI::AssociationStatsUI_Load);
            statusStrip1->ResumeLayout(false);
            statusStrip1->PerformLayout();
            toolStrip1->ResumeLayout(false);
            toolStrip1->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->EndInit();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
    };
}
