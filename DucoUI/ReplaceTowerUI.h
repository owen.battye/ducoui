#pragma once

namespace DucoUI
{
    public ref class ReplaceTowerUI : public System::Windows::Forms::Form
    {
    public:
        ReplaceTowerUI(DucoUI::DatabaseManager^ theDatabase);

    protected:
        ~ReplaceTowerUI();
        System::Void ReplaceTowerUI_Load(System::Object^  sender, System::EventArgs^  e);

        System::Void OldTower_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void NewTower_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);

        System::Void ReplaceBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void CloseBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void swapBtn_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void EnableButtons(const Duco::ObjectId& removeTowerId, const Duco::ObjectId& newTowerId);

    private:
        DucoUI::DatabaseManager^                     database;
        System::Collections::Generic::List<System::Int16>^ allTowerIds;

        System::ComponentModel::Container^  components;
        System::Windows::Forms::ComboBox^   oldTower;
        System::Windows::Forms::ComboBox^   newTower;
        System::Windows::Forms::Label^      oldTowerLbl;
        System::Windows::Forms::Label^      newTowerLbl;

    private: System::Windows::Forms::Button^  swapBtn;
    private: System::Windows::Forms::ToolStripStatusLabel^  statusLabel;

             System::Windows::Forms::Button^     replaceBtn;

        void InitializeComponent()
        {
            System::Windows::Forms::Button^  closeBtn;
            System::Windows::Forms::Label^  label1;
            System::Windows::Forms::Label^  label2;
            System::Windows::Forms::StatusStrip^  statusStrip1;
            System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
            this->statusLabel = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->replaceBtn = (gcnew System::Windows::Forms::Button());
            this->oldTowerLbl = (gcnew System::Windows::Forms::Label());
            this->newTowerLbl = (gcnew System::Windows::Forms::Label());
            this->oldTower = (gcnew System::Windows::Forms::ComboBox());
            this->newTower = (gcnew System::Windows::Forms::ComboBox());
            this->swapBtn = (gcnew System::Windows::Forms::Button());
            closeBtn = (gcnew System::Windows::Forms::Button());
            label1 = (gcnew System::Windows::Forms::Label());
            label2 = (gcnew System::Windows::Forms::Label());
            statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            statusStrip1->SuspendLayout();
            tableLayoutPanel1->SuspendLayout();
            this->SuspendLayout();
            // 
            // closeBtn
            // 
            closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            closeBtn->Location = System::Drawing::Point(471, 72);
            closeBtn->Name = L"closeBtn";
            closeBtn->Size = System::Drawing::Size(75, 23);
            closeBtn->TabIndex = 10;
            closeBtn->Text = L"Close";
            closeBtn->UseVisualStyleBackColor = true;
            closeBtn->Click += gcnew System::EventHandler(this, &ReplaceTowerUI::CloseBtn_Click);
            // 
            // label1
            // 
            label1->AutoSize = true;
            label1->Dock = System::Windows::Forms::DockStyle::Fill;
            label1->Location = System::Drawing::Point(3, 0);
            label1->Name = L"label1";
            label1->Size = System::Drawing::Size(98, 27);
            label1->TabIndex = 1;
            label1->Text = L"Remove tower";
            label1->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // label2
            // 
            label2->AutoSize = true;
            label2->Dock = System::Windows::Forms::DockStyle::Fill;
            label2->Location = System::Drawing::Point(3, 27);
            label2->Name = L"label2";
            label2->Size = System::Drawing::Size(98, 27);
            label2->TabIndex = 4;
            label2->Text = L"Replace with tower";
            label2->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // statusStrip1
            // 
            statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->statusLabel });
            statusStrip1->Location = System::Drawing::Point(0, 98);
            statusStrip1->Name = L"statusStrip1";
            statusStrip1->Size = System::Drawing::Size(549, 22);
            statusStrip1->TabIndex = 0;
            statusStrip1->Text = L"statusStrip1";
            // 
            // statusLabel
            // 
            this->statusLabel->Name = L"statusLabel";
            this->statusLabel->Size = System::Drawing::Size(643, 17);
            this->statusLabel->Spring = true;
            this->statusLabel->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 3;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->Controls->Add(label1, 0, 0);
            tableLayoutPanel1->Controls->Add(label2, 0, 1);
            tableLayoutPanel1->Controls->Add(this->replaceBtn, 1, 3);
            tableLayoutPanel1->Controls->Add(this->oldTowerLbl, 2, 0);
            tableLayoutPanel1->Controls->Add(closeBtn, 2, 3);
            tableLayoutPanel1->Controls->Add(this->newTowerLbl, 2, 1);
            tableLayoutPanel1->Controls->Add(this->oldTower, 1, 0);
            tableLayoutPanel1->Controls->Add(this->newTower, 1, 1);
            tableLayoutPanel1->Controls->Add(this->swapBtn, 0, 3);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->Padding = System::Windows::Forms::Padding(0, 0, 0, 22);
            tableLayoutPanel1->RowCount = 4;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->Size = System::Drawing::Size(549, 120);
            tableLayoutPanel1->TabIndex = 1;
            // 
            // replaceBtn
            // 
            this->replaceBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->replaceBtn->Enabled = false;
            this->replaceBtn->Location = System::Drawing::Point(390, 72);
            this->replaceBtn->Name = L"replaceBtn";
            this->replaceBtn->Size = System::Drawing::Size(75, 23);
            this->replaceBtn->TabIndex = 9;
            this->replaceBtn->Text = L"Replace";
            this->replaceBtn->UseVisualStyleBackColor = true;
            this->replaceBtn->Click += gcnew System::EventHandler(this, &ReplaceTowerUI::ReplaceBtn_Click);
            // 
            // oldTowerLbl
            // 
            this->oldTowerLbl->AutoSize = true;
            this->oldTowerLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            this->oldTowerLbl->Location = System::Drawing::Point(471, 0);
            this->oldTowerLbl->Name = L"oldTowerLbl";
            this->oldTowerLbl->Size = System::Drawing::Size(75, 27);
            this->oldTowerLbl->TabIndex = 3;
            this->oldTowerLbl->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
            // 
            // newTowerLbl
            // 
            this->newTowerLbl->AutoSize = true;
            this->newTowerLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            this->newTowerLbl->Location = System::Drawing::Point(471, 27);
            this->newTowerLbl->Name = L"newTowerLbl";
            this->newTowerLbl->Size = System::Drawing::Size(75, 27);
            this->newTowerLbl->TabIndex = 6;
            this->newTowerLbl->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
            // 
            // oldTower
            // 
            this->oldTower->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
            this->oldTower->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->oldTower->Dock = System::Windows::Forms::DockStyle::Fill;
            this->oldTower->FormattingEnabled = true;
            this->oldTower->Location = System::Drawing::Point(107, 3);
            this->oldTower->Name = L"oldTower";
            this->oldTower->Size = System::Drawing::Size(358, 21);
            this->oldTower->TabIndex = 2;
            this->oldTower->SelectedIndexChanged += gcnew System::EventHandler(this, &ReplaceTowerUI::OldTower_SelectedIndexChanged);
            // 
            // newTower
            // 
            this->newTower->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
            this->newTower->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->newTower->Dock = System::Windows::Forms::DockStyle::Fill;
            this->newTower->FormattingEnabled = true;
            this->newTower->Location = System::Drawing::Point(107, 30);
            this->newTower->Name = L"newTower";
            this->newTower->Size = System::Drawing::Size(358, 21);
            this->newTower->TabIndex = 5;
            this->newTower->SelectedIndexChanged += gcnew System::EventHandler(this, &ReplaceTowerUI::NewTower_SelectedIndexChanged);
            // 
            // swapBtn
            // 
            this->swapBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->swapBtn->Location = System::Drawing::Point(26, 72);
            this->swapBtn->Name = L"swapBtn";
            this->swapBtn->Size = System::Drawing::Size(75, 23);
            this->swapBtn->TabIndex = 11;
            this->swapBtn->Text = L"Swap";
            this->swapBtn->UseVisualStyleBackColor = true;
            this->swapBtn->Click += gcnew System::EventHandler(this, &ReplaceTowerUI::swapBtn_Click);
            // 
            // ReplaceTowerUI
            // 
            this->AcceptButton = this->replaceBtn;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->CancelButton = closeBtn;
            this->ClientSize = System::Drawing::Size(549, 120);
            this->Controls->Add(statusStrip1);
            this->Controls->Add(tableLayoutPanel1);
            this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
            this->MinimumSize = System::Drawing::Size(565, 159);
            this->Name = L"ReplaceTowerUI";
            this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
            this->Text = L"Replace tower";
            this->Load += gcnew System::EventHandler(this, &ReplaceTowerUI::ReplaceTowerUI_Load);
            statusStrip1->ResumeLayout(false);
            statusStrip1->PerformLayout();
            tableLayoutPanel1->ResumeLayout(false);
            tableLayoutPanel1->PerformLayout();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
    };

}
