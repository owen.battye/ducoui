#pragma once

namespace DucoUI
{
    public ref class DeleteUnusedUI : public System::Windows::Forms::Form
    {
    public:
        DeleteUnusedUI(DucoUI::DatabaseManager^ theDatabase, bool& newChangesMade);
    protected:
        ~DeleteUnusedUI();
        !DeleteUnusedUI();

    protected:
        System::Void Find();
        System::Void SetLabels();

        System::Void findBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void Delete_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void selectAllBtn_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void DeleteUnusedUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void DeleteUnusedUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e);

        System::Void list_ItemCheck(System::Object^  sender, System::Windows::Forms::ItemCheckEventArgs^  e);
        System::Void EnableDeleteButton(bool checked);
        void InitializeComponent();

    private:
        System::ComponentModel::Container^  components;
        System::Windows::Forms::Button^     deleteBtn;
        DucoUI::DatabaseManager^      database;
        bool                                renumberInProgress;

    private: System::Windows::Forms::TextBox^  ringersEditor;
    private: System::Windows::Forms::TextBox^  towersEditor;
    private: System::Windows::Forms::TextBox^  methodsEditor;
    private: System::Windows::Forms::TextBox^  ringsEditor;
    private: System::Windows::Forms::TextBox^  seriesEditor;
    private: System::Windows::Forms::TextBox^  compositionsEditor;
    private: System::Windows::Forms::Label^  totalRingersLbl;

    private: System::Windows::Forms::Label^  totalTowersLbl;
    private: System::Windows::Forms::Label^  totalMethodsLbl;
    private: System::Windows::Forms::Label^  totalRingsLbl;
    private: System::Windows::Forms::Label^  totalCompositionsLbl;
    private: System::Windows::Forms::Label^  totalSeriesLbl;
    private: System::Windows::Forms::CheckedListBox^  ringerList;
    private: System::Windows::Forms::CheckedListBox^  towerList;
    private: System::Windows::Forms::CheckedListBox^  methodList;
    private: System::Windows::Forms::CheckedListBox^  ringsList;
    private: System::Windows::Forms::CheckedListBox^  seriesList;
    private: System::Windows::Forms::CheckedListBox^  compositionsList;
    private: System::Windows::Forms::CheckedListBox^  associationsList;

        std::set<Duco::ObjectId>*   unusedMethods;
        std::set<Duco::ObjectId>*   unusedRingers;
        std::multimap<Duco::ObjectId, Duco::ObjectId>* unusedRings;
        std::set<Duco::ObjectId>*   unusedTowers;
        std::set<Duco::ObjectId>*   unusedMethodSeries;
        std::set<Duco::ObjectId>*   unusedCompositions;
        std::set<Duco::ObjectId>*   unusedAssociations;

    private: System::Windows::Forms::TableLayoutPanel^  ringersLayoutPanel;
    private: System::Windows::Forms::Panel^  ringersPanel;

    private: System::Windows::Forms::TableLayoutPanel^  towersLayoutPanel;
    private: System::Windows::Forms::Panel^  towersPanel;

    private: System::Windows::Forms::TableLayoutPanel^  methodsLayoutPanel;
    private: System::Windows::Forms::Panel^  methodsPanel;

    private: System::Windows::Forms::TableLayoutPanel^  ringsLayoutPanel;
    private: System::Windows::Forms::Panel^  ringsPanel;

    private: System::Windows::Forms::TableLayoutPanel^  seriesLayoutPanel;
    private: System::Windows::Forms::Panel^  seriesPanel;

    private: System::Windows::Forms::TableLayoutPanel^  compositionsLayoutPanel;
    private: System::Windows::Forms::Panel^  compositionsPanel;
    private: System::Windows::Forms::TableLayoutPanel^  masterLayoutPanel;
    private: System::Windows::Forms::Panel^  btnPanel;
    private: System::Windows::Forms::Button^  findBtn;
    private: System::Windows::Forms::Button^  closeBtn;
    private: System::Windows::Forms::Button^  selectAllBtn;
    private: System::Windows::Forms::TextBox^  associationsEditor;
    private: System::Windows::Forms::Label^  totalAssociationsLbl;
    bool&                       changesMade;
    };
}
