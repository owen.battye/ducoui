#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"
#include "BellboardParserWrapper.h"
#include <MethodNotationDatabaseSingleImporter.h>
#include "OpenObjectCallback.h"
#include "RingingDatabaseObserver.h"
#include "PictureListControl.h"
#include "PictureControl.h"
#include "DucoWindowState.h"
#include "PrintBandSummaryUtil.h"
#include "RingerList.h"
#include <DownloadedPeal.h>
#include "PealUI.h"
#include <BellboardPerformanceParser.h>

#include "DownloadPealUI.h"

#include "WindowsSettings.h"
#include <RingingDatabase.h>
#include <DatabaseSettings.h>
#include "DucoUtils.h"
#include "DucoUILog.h"
#include "SoundUtils.h"
#include "SystemDefaultIcon.h"
#include <Tower.h>
#include <Ringer.h>
#include <Tower.h>
#include <PealDatabase.h>
#include <TowerDatabase.h>
#include "DoveSearchWrapper.h"
#include <DucoConfiguration.h>
#include <DoveDatabase.h>
#include <DoveSearch.h>
#include <DoveObject.h>
#include <StatisticFilters.h>
#include "BellBoardConfirmIdUpdate.h"
#include "ChooseOrGenerateRinger.h"

using namespace CefSharp::WinForms;
using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::Collections::Generic;
using namespace System::IO;
using namespace System::Threading::Tasks;
using namespace System::Net;
using namespace System::Windows;
using namespace System::Windows::Forms;
using namespace System::Xml::Linq;

#define KZoomIncrement 0.5
#define KPealsInProgressColour "#f2ef9d"
#define KPealsAlreadyInDatabaseColour "#d7fcba"
#define KDownloadedPealColour "#b1d198"

DownloadPealUI::DownloadPealUI(DatabaseManager^ theDatabase)
    : database(theDatabase)
{
    callbackWrapper = new BellboardParserWrapper(this);
    ringerCallbackWrapper = new BellboardRingerParserWrapper(this);
    firstLoad = true;
    downloadHandler = gcnew System::Net::Http::HttpClient();
    parser = new BellboardPerformanceParser(database->Database(), *callbackWrapper, ringerCallbackWrapper);
    loadDelegate = gcnew LoadStatusDelegate(this, &DownloadPealUI::LoadingStatusChanged);
    missingPealIds = gcnew System::Collections::Generic::Queue<System::String^>();
    InitializeComponent();
    InitializeCEFSharp();

    if (!database->Config().DisableFeaturesForPerformance())
    {
        if (File::Exists(database->XmlMethodCollectionFile()))
        {
            std::string methodsFileName;
            DucoUtils::ConvertString(database->XmlMethodCollectionFile(), methodsFileName);

            parser->EnableMethodsDatabase(database->Database().MethodsDatabase(), methodsFileName);
        }

        if (File::Exists(DucoUtils::ConvertString(database->Config().DoveDataFile())))
        {
            parser->EnableDoveDatabase(database->Config().DoveDataFile(), database->WindowsSettings()->InstallationDir());
        }
    }
}

DownloadPealUI::!DownloadPealUI()
{
    delete parser;
    delete callbackWrapper;
}

DownloadPealUI::~DownloadPealUI()
{
    this->!DownloadPealUI();
    if (components)
    {
        delete components;
    }
}

System::Void
DownloadPealUI::LoadingStatusChanged(CefSharp::LoadingStateChangedEventArgs ^  e)
{
    DUCOUILOGDEBUG("DownloadPealUI::LoadingStatusChanged: loading:" + Convert::ToString(e->IsLoading));
    Boolean enableButtons = !e->IsLoading;
    Boolean enableDownloadButton = false;
    Boolean enableDownloadAllButton = false;
    if (enableButtons)
    {
        System::String^ currentUrl = chromeBrowser->Address;
        enableDownloadButton = currentUrl->Contains("view.php");
        enableDownloadAllButton = currentUrl->Contains("search.php") && !currentUrl->EndsWith("search.php");
        DUCOUILOGDEBUG("DownloadPealUI::LoadingStatusChanged: enableDownloadAllButton:" + Convert::ToString(enableDownloadAllButton));
        if (enableDownloadAllButton)
        {
            HighlightKnownPealsAndSetMissingIds(true);
        }
        double value = database->Config().ChromeZoomLevel();
        CefSharp::WebBrowserExtensions::SetZoomLevel(chromeBrowser, value);
    }
    downloadBtn->Enabled = enableDownloadButton;
    highlightKnownPealsBtn->Enabled = enableDownloadAllButton;
    downloadAllBtn->Enabled = enableDownloadAllButton;
}

System::Void
DownloadPealUI::BrowserLoadingStateChanged(System::Object^ object, CefSharp::LoadingStateChangedEventArgs ^  e)
{
    if (e->IsLoading == false)
    {
        CefSharp::WinForms::ChromiumWebBrowser^ browser = static_cast<CefSharp::WinForms::ChromiumWebBrowser^>(object);
        DUCOUILOGDEBUG("DownloadPealUI::BrowserLoadingStateChanged: " + browser->Address);
        DownloadPealUI^ parentForm = static_cast<DownloadPealUI^>(browser->FindForm());
        parentForm->numberDownloaded = 0;
        if (browser->Address->EndsWith("search.php"))
        {
            parentForm->SetSearchParameters();
        }
        else if (browser->Address->EndsWith(".co.uk/"))
        {
            parentForm->GoToSearch();
            // Disabled for now, as this doesnt seem to work any longer!
        }
        else if (browser->Address->Contains("search.php"))
        {
            String^ scrollToBottom = "document.getElementById(\"page-footer\").scrollIntoView();";
            CefSharp::WebBrowserExtensions::ExecuteScriptAsync(browser, scrollToBottom);
        }
        if (!parentForm->Disposing && !parentForm->IsDisposed)
        {
            array<Object^>^ loadStatusArray = { e };
            parentForm->Invoke(parentForm->loadDelegate, loadStatusArray);
        }
    }
}

System::Void
DownloadPealUI::GoToSearch()
{
    String^ startSearchJScript = "var elements = document.getElementsByTagName('a'); for (var i = 0; i < elements.length; i++) { if (elements[i].innerText.includes('Search')) { elements[i].click(); } }";
    CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, startSearchJScript);
}

System::Void
DownloadPealUI::useBellboardIds_Click(System::Object^  sender, System::EventArgs^  e)
{
    firstLoad = true;
    SetSearchParameters();
}

System::Void
DownloadPealUI::SetSearchParameters()
{
    if (!firstLoad)
        return;

    firstLoad = false;

    const Duco::ObjectId defaultRingerId = database->Database().Settings().DefaultRinger();
    Duco::Ringer defaultRinger;
    Duco::StatisticFilters filters(database->Database());
    if (database->FindRinger(defaultRingerId, defaultRinger, false))
    {
        std::wstring defaultRingerName = defaultRinger.FullName(false);
        String^ setRingerJavaScript = "var x = document.getElementById('ringer'); x.value ='";
        setRingerJavaScript += DucoUtils::ConvertString(defaultRingerName);
        setRingerJavaScript += "';";
        CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, setRingerJavaScript);
    }
    else if (database->Database().TowersDatabase().NumberOfUniqueTowers() == 1 && database->Database().PerformanceInfo(filters).TotalPeals() > 1)
    {
        Duco::ObjectId firstTowerId;
        if (database->FirstTower(firstTowerId, false))
        {
            bool towerFound = true;
            Duco::Tower defaultTower;
            if (database->FindTower(firstTowerId, defaultTower, false))
            {
                while (defaultTower.Removed())
                {
                    if (database->NextTower(firstTowerId, true, false))
                    {
                        if (!database->FindTower(firstTowerId, defaultTower, false))
                        {
                            towerFound = false;
                        }
                    }
                    else
                    {
                        towerFound = false;
                    }
                }
                if (towerFound)
                {
                    std::wstring towerCity = defaultTower.City();
                    String^ setTowerJavaScript = "var x = document.getElementById('place'); x.value ='";
                    setTowerJavaScript += DucoUtils::ConvertString(towerCity);
                    setTowerJavaScript += "';";
                    CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, setTowerJavaScript);
                }
            }
        }
    }
    if (database->Database().Settings().PealDatabase())
        CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, "document.querySelector('#length').value='peal'");
    else
        CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, "document.querySelector('#length').value='quarter'");

    bool found = false;
    PerformanceDate lastPealDate;
    if (useBellboardIds->Checked)
    {
        if (database->Database().PealsDatabase().FindFirstPealWithoutBellboardId(lastPealDate).ValidId())
        {
            found = true;
        }
    }
    else
    {
        PerformanceDate today;
        Duco::StatisticFilters filters(database->Database());
        filters.SetEndDate(true, today);
        Duco::PealLengthInfo info = database->Database().PealsDatabase().PerformanceInfo(filters);
        if (info.LastPealId().ValidId())
        {
            lastPealDate = info.DateOfLastPeal();
            found = true;
        }
    }

    if (found)
    {
        String^ setLastDateJavaScript = "var x = document.getElementById('from'); x.value ='";
        setLastDateJavaScript += DucoUtils::ConvertString(lastPealDate.Str());
        setLastDateJavaScript += "';";
        CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, setLastDateJavaScript);
    }
}

System::Void
DownloadPealUI::DownloadPeal_Load(System::Object^  sender, System::EventArgs^ eventArgs)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    this->chromeBrowser->LoadingStateChanged += gcnew System::EventHandler<CefSharp::LoadingStateChangedEventArgs ^>(BrowserLoadingStateChanged);

    GoToWebSite();
}

System::Void
DownloadPealUI::DownloadPealUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e)
{
}

System::Void
DownloadPealUI::GoToWebSite()
{
    try
    {
        String^ downloadString = DucoUtils::ConvertString(database->Settings().PreferedDownloadWebsiteUrl());
        chromeBrowser->Load(downloadString);
    }
    catch (Exception^ ex) 
    {
        SoundUtils::PlayErrorSound();
        DucoUILog::PrintError("Error going to download website: " + ex->ToString());
        MessageBox::Show(this, "Please check the peal download URL in settings\n" + ex->ToString(), "Bad download URL", MessageBoxButtons::OK, MessageBoxIcon::Warning );
    }
}

System::Void
DownloadPealUI::downloadBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    statusLabel->Text = "";
    if (database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
    }
    else
    {
        System::String^ currentUrl = chromeBrowser->Address;
        int startOfPealNumber = currentUrl->LastIndexOf('=') + 1;
        String^ pealNumber = currentUrl->Substring(startOfPealNumber);
        missingPealIds->Clear();
        missingPealIds->Enqueue(pealNumber);
        StartNextDownload();
    }
}

System::Void
DownloadPealUI::highlightBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    statusLabel->Text = "";
    HighlightKnownPealsAndSetMissingIds(true);
}

System::Void
DownloadPealUI::HighlightKnownPealsAndSetMissingIds(Boolean updateMissingPealIds)
{
    numberDownloaded = 0;
    ClearPealFlags();
    Task<System::String^>^ taskResponse = CefSharp::WebBrowserExtensions::GetSourceAsync(chromeBrowser);
    taskResponse->Wait(1000);
    if (updateMissingPealIds)
        missingPealIds->Clear();

    DUCOUILOGDEBUG("DownloadPealUI::HighlightKnownPealsAndSetMissingIds: IsCompleted:" + Convert::ToString(taskResponse->IsCompleted));
    if (taskResponse->IsCompleted)
    {
        String^ html = (String^)taskResponse->Result;
        int index = html->IndexOf("name=\"ids\"");
        if (index != -1)
        {
            String^ valueConst = "value=\"";
            int index2 = html->IndexOf("value=\"", index);
            int index3 = html->IndexOf("\">", index2);

            if (updateMissingPealIds)
            {
                String^ ids = html->Substring(index2 + valueConst->Length, index3 - (index2 + valueConst->Length));
                array<String^>^ idArray = ids->Split(',');
                for each (System::String ^ id in idArray)
                {
                    missingPealIds->Enqueue(id);
                }
            }

            System::Collections::Generic::HashSet<System::String^>^ existingPealIds = database->RemoveExistingBellBoardPealIds(missingPealIds);
            System::Collections::Generic::HashSet<System::String^>::Enumerator it = existingPealIds->GetEnumerator();
            while (it.MoveNext())
            {
                DUCOUILOGDEBUG("DownloadPealUI::HighlightKnownPeal: " + Convert::ToString(it.Current));
                HighlightKnownPeal(it.Current, KPealsAlreadyInDatabaseColour);
            }
        }
    }
}

System::Void
DownloadPealUI::clearHighlightsToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e)
{
    ClearPealFlags();
}

System::Void
DownloadPealUI::ClearPealFlags()
{
    String^ highlightKnownPeal = "var x = document.getElementsByClassName(\"place\");var i;for (i = 0; i < x.length; i++) {if (x[i].innerHTML.includes(\"/view.php?id=";
    highlightKnownPeal += "\")) { x[i].style.backgroundColor = \"\"; x[i].scrollIntoView(); } }";
    CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, highlightKnownPeal);
}

System::Void
DownloadPealUI::HighlightKnownPeal(System::String^ pealId, String^ colour)
{
    String^ highlightKnownPeal = "var x = document.getElementsByClassName(\"place\");var i;for (i = 0; i < x.length; i++) {if (x[i].innerHTML.includes(\"/view.php?id=";
    highlightKnownPeal += pealId;
    highlightKnownPeal += "\")) { x[i].style.backgroundColor = \"" + colour + "\"; x[i].scrollIntoView(); } }";
    CefSharp::WebBrowserExtensions::ExecuteScriptAsync(chromeBrowser, highlightKnownPeal);
}

System::Void
DownloadPealUI::downloadAllBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    statusLabel->Text = "";
    if (database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
    }
    else
    {
        HighlightKnownPealsAndSetMissingIds(true);
        StartNextDownload();
    }
}

System::Void
DownloadPealUI::backButton_Click(System::Object^  sender, System::EventArgs^  e)
{
    CefSharp::WebBrowserExtensions::Back(chromeBrowser);
}

System::Void
DownloadPealUI::bellBoardBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    GoToWebSite();
}

System::Void
DownloadPealUI::InitializeCEFSharp()
{
    this->chromeBrowser = gcnew CefSharp::WinForms::ChromiumWebBrowser();
    this->tableLayoutPanel1->Controls->Add(this->chromeBrowser, 0, 2);
    // 
    // chromeBrowser
    // 
    this->tableLayoutPanel1->SetColumnSpan(this->chromeBrowser, 5);
    this->tableLayoutPanel1->SetRowSpan(this->chromeBrowser, 1);
    this->chromeBrowser->Dock = System::Windows::Forms::DockStyle::Fill;
    this->chromeBrowser->Location = System::Drawing::Point(3, 50);
    this->chromeBrowser->Margin = System::Windows::Forms::Padding(3, 0, 3, 0);
    this->chromeBrowser->Name = L"chromeBrowser";
    this->chromeBrowser->TabIndex = 5;
}

System::Void 
DownloadPealUI::InitializeComponent()
{
    System::Windows::Forms::Button^ backButton;
    System::Windows::Forms::StatusStrip^ statusStrip1;
    System::Windows::Forms::MenuStrip^ menuStrip;
    this->statusLabel = (gcnew System::Windows::Forms::ToolStripStatusLabel());
    this->highlightKnownPealsBtn = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->useBellboardIds = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->clearHighlightsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
    this->resetZoomBtn = (gcnew System::Windows::Forms::Button());
    this->zoomInBtn = (gcnew System::Windows::Forms::Button());
    this->bellBoardBtn = (gcnew System::Windows::Forms::Button());
    this->zoomOutBtn = (gcnew System::Windows::Forms::Button());
    this->downloadBtn = (gcnew System::Windows::Forms::Button());
    this->downloadAllBtn = (gcnew System::Windows::Forms::Button());
    backButton = (gcnew System::Windows::Forms::Button());
    statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
    menuStrip = (gcnew System::Windows::Forms::MenuStrip());
    statusStrip1->SuspendLayout();
    menuStrip->SuspendLayout();
    this->tableLayoutPanel1->SuspendLayout();
    this->SuspendLayout();
    // 
    // backButton
    // 
    backButton->Dock = System::Windows::Forms::DockStyle::Fill;
    backButton->Location = System::Drawing::Point(3, 27);
    backButton->Name = L"backButton";
    this->tableLayoutPanel1->SetRowSpan(backButton, 2);
    backButton->Size = System::Drawing::Size(48, 48);
    backButton->TabIndex = 0;
    backButton->Text = L"<<";
    backButton->UseVisualStyleBackColor = true;
    backButton->Click += gcnew System::EventHandler(this, &DownloadPealUI::backButton_Click);
    // 
    // statusStrip1
    // 
    statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->statusLabel });
    statusStrip1->Location = System::Drawing::Point(0, 634);
    statusStrip1->Name = L"statusStrip1";
    statusStrip1->Size = System::Drawing::Size(1150, 22);
    statusStrip1->TabIndex = 1;
    statusStrip1->Text = L"statusStrip1";
    // 
    // statusLabel
    // 
    this->statusLabel->Name = L"statusLabel";
    this->statusLabel->Size = System::Drawing::Size(1135, 17);
    this->statusLabel->Spring = true;
    this->statusLabel->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
    // 
    // menuStrip
    // 
    menuStrip->AllowMerge = false;
    menuStrip->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
        this->highlightKnownPealsBtn,
            this->useBellboardIds, this->clearHighlightsToolStripMenuItem
    });
    menuStrip->Location = System::Drawing::Point(0, 0);
    menuStrip->Name = L"menuStrip";
    menuStrip->Size = System::Drawing::Size(1150, 24);
    menuStrip->TabIndex = 0;
    menuStrip->Text = L"menuStrip1";
    // 
    // highlightKnownPealsBtn
    // 
    this->highlightKnownPealsBtn->Name = L"highlightKnownPealsBtn";
    this->highlightKnownPealsBtn->Size = System::Drawing::Size(108, 20);
    this->highlightKnownPealsBtn->Text = L"Highlight known";
    this->highlightKnownPealsBtn->Click += gcnew System::EventHandler(this, &DownloadPealUI::highlightBtn_Click);
    // 
    // useBellboardIds
    // 
    this->useBellboardIds->Name = L"useBellboardIds";
    this->useBellboardIds->Size = System::Drawing::Size(111, 20);
    this->useBellboardIds->Text = L"Set search criteria";
    this->useBellboardIds->Click += gcnew System::EventHandler(this, &DownloadPealUI::useBellboardIds_Click);
    // 
    // clearHighlightsToolStripMenuItem
    // 
    this->clearHighlightsToolStripMenuItem->Alignment = System::Windows::Forms::ToolStripItemAlignment::Right;
    this->clearHighlightsToolStripMenuItem->Name = L"clearHighlightsToolStripMenuItem";
    this->clearHighlightsToolStripMenuItem->Size = System::Drawing::Size(102, 20);
    this->clearHighlightsToolStripMenuItem->Text = L"Clear highlights";
    this->clearHighlightsToolStripMenuItem->Click += gcnew System::EventHandler(this, &DownloadPealUI::clearHighlightsToolStripMenuItem_Click);
    // 
    // tableLayoutPanel1
    // 
    this->tableLayoutPanel1->ColumnCount = 5;
    this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
        54)));
    this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
        100)));
    this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
        74)));
    this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
        50)));
    this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
        50)));
    this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
        20)));
    this->tableLayoutPanel1->Controls->Add(this->resetZoomBtn, 3, 1);
    this->tableLayoutPanel1->Controls->Add(this->zoomInBtn, 4, 0);
    this->tableLayoutPanel1->Controls->Add(this->bellBoardBtn, 1, 0);
    this->tableLayoutPanel1->Controls->Add(backButton, 0, 0);
    this->tableLayoutPanel1->Controls->Add(this->zoomOutBtn, 3, 0);
    this->tableLayoutPanel1->Controls->Add(this->downloadBtn, 0, 3);
    this->tableLayoutPanel1->Controls->Add(this->downloadAllBtn, 2, 3);
    this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
    this->tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
    this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
    this->tableLayoutPanel1->Padding = System::Windows::Forms::Padding(0, 24, 0, 0);
    this->tableLayoutPanel1->RowCount = 5;
    this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 30)));
    this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 24)));
    this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
    this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 50)));
    this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
    this->tableLayoutPanel1->Size = System::Drawing::Size(1150, 656);
    this->tableLayoutPanel1->TabIndex = 1;
    // 
    // resetZoomBtn
    // 
    this->tableLayoutPanel1->SetColumnSpan(this->resetZoomBtn, 2);
    this->resetZoomBtn->Dock = System::Windows::Forms::DockStyle::Fill;
    this->resetZoomBtn->Location = System::Drawing::Point(1053, 55);
    this->resetZoomBtn->Margin = System::Windows::Forms::Padding(3, 1, 3, 3);
    this->resetZoomBtn->Name = L"resetZoomBtn";
    this->resetZoomBtn->Size = System::Drawing::Size(94, 20);
    this->resetZoomBtn->TabIndex = 4;
    this->resetZoomBtn->Text = L"Reset";
    this->resetZoomBtn->UseVisualStyleBackColor = true;
    this->resetZoomBtn->Click += gcnew System::EventHandler(this, &DownloadPealUI::clearZoom_Click);
    // 
    // zoomInBtn
    // 
    this->zoomInBtn->Dock = System::Windows::Forms::DockStyle::Fill;
    this->zoomInBtn->Location = System::Drawing::Point(1103, 27);
    this->zoomInBtn->Margin = System::Windows::Forms::Padding(3, 3, 3, 2);
    this->zoomInBtn->Name = L"zoomInBtn";
    this->zoomInBtn->Size = System::Drawing::Size(44, 25);
    this->zoomInBtn->TabIndex = 3;
    this->zoomInBtn->Text = L"+";
    this->zoomInBtn->UseVisualStyleBackColor = true;
    this->zoomInBtn->Click += gcnew System::EventHandler(this, &DownloadPealUI::zoomInBtn_Click);
    // 
    // bellBoardBtn
    // 
    this->tableLayoutPanel1->SetColumnSpan(this->bellBoardBtn, 2);
    this->bellBoardBtn->Dock = System::Windows::Forms::DockStyle::Fill;
    this->bellBoardBtn->Location = System::Drawing::Point(57, 27);
    this->bellBoardBtn->Name = L"bellBoardBtn";
    this->tableLayoutPanel1->SetRowSpan(this->bellBoardBtn, 2);
    this->bellBoardBtn->Size = System::Drawing::Size(990, 48);
    this->bellBoardBtn->TabIndex = 1;
    this->bellBoardBtn->Text = L"BellBoard home";
    this->bellBoardBtn->UseVisualStyleBackColor = true;
    this->bellBoardBtn->Click += gcnew System::EventHandler(this, &DownloadPealUI::bellBoardBtn_Click);
    // 
    // zoomOutBtn
    // 
    this->zoomOutBtn->Dock = System::Windows::Forms::DockStyle::Fill;
    this->zoomOutBtn->Location = System::Drawing::Point(1053, 27);
    this->zoomOutBtn->Margin = System::Windows::Forms::Padding(3, 3, 3, 2);
    this->zoomOutBtn->Name = L"zoomOutBtn";
    this->zoomOutBtn->Size = System::Drawing::Size(44, 25);
    this->zoomOutBtn->TabIndex = 2;
    this->zoomOutBtn->Text = L"-";
    this->zoomOutBtn->UseVisualStyleBackColor = true;
    this->zoomOutBtn->Click += gcnew System::EventHandler(this, &DownloadPealUI::zoomOutBtn_Click);
    // 
    // downloadBtn
    // 
    this->tableLayoutPanel1->SetColumnSpan(this->downloadBtn, 2);
    this->downloadBtn->DialogResult = System::Windows::Forms::DialogResult::OK;
    this->downloadBtn->Dock = System::Windows::Forms::DockStyle::Fill;
    this->downloadBtn->Enabled = false;
    this->downloadBtn->Location = System::Drawing::Point(3, 589);
    this->downloadBtn->Name = L"downloadBtn";
    this->downloadBtn->Size = System::Drawing::Size(970, 44);
    this->downloadBtn->TabIndex = 6;
    this->downloadBtn->Text = L"Download one";
    this->downloadBtn->UseVisualStyleBackColor = true;
    this->downloadBtn->Click += gcnew System::EventHandler(this, &DownloadPealUI::downloadBtn_Click);
    // 
    // downloadAllBtn
    // 
    this->tableLayoutPanel1->SetColumnSpan(this->downloadAllBtn, 3);
    this->downloadAllBtn->DialogResult = System::Windows::Forms::DialogResult::OK;
    this->downloadAllBtn->Dock = System::Windows::Forms::DockStyle::Fill;
    this->downloadAllBtn->Enabled = false;
    this->downloadAllBtn->Location = System::Drawing::Point(979, 589);
    this->downloadAllBtn->Name = L"downloadAllBtn";
    this->downloadAllBtn->Size = System::Drawing::Size(168, 44);
    this->downloadAllBtn->TabIndex = 7;
    this->downloadAllBtn->Text = L"Download all";
    this->downloadAllBtn->UseVisualStyleBackColor = true;
    this->downloadAllBtn->Click += gcnew System::EventHandler(this, &DownloadPealUI::downloadAllBtn_Click);
    // 
    // DownloadPealUI
    // 
    this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
    this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
    this->ClientSize = System::Drawing::Size(1150, 656);
    this->Controls->Add(statusStrip1);
    this->Controls->Add(menuStrip);
    this->Controls->Add(this->tableLayoutPanel1);
    this->MainMenuStrip = menuStrip;
    this->Name = L"DownloadPealUI";
    this->ShowInTaskbar = false;
    this->Text = L"Download from Website";
    this->TopMost = true;
    this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &DownloadPealUI::DownloadPealUI_FormClosing);
    this->Load += gcnew System::EventHandler(this, &DownloadPealUI::DownloadPeal_Load);
    statusStrip1->ResumeLayout(false);
    statusStrip1->PerformLayout();
    menuStrip->ResumeLayout(false);
    menuStrip->PerformLayout();
    this->tableLayoutPanel1->ResumeLayout(false);
    this->ResumeLayout(false);
    this->PerformLayout();

}

void
DownloadPealUI::StartNextDownload()
{
    if (missingPealIds->Count > 0)
    {
        ++numberDownloaded;
        System::String^ pealId = missingPealIds->Dequeue();
        std::wstring pealIdStr;
        HighlightKnownPeal(pealId, KPealsInProgressColour);
        DucoUtils::ConvertString(pealId, pealIdStr);
        statusLabel->Text = "";
        if (parser->DownloadPerformance(pealIdStr))
        {
            StartNextDownload();
        }
        else
        {
            SoundUtils::PlayErrorSound();
        }
    }
}

System::Void
DownloadPealUI::Cancelled(System::String^ bellboardId, System::String^ errorMessage)
{
    SoundUtils::PlayErrorSound();
    DucoUILog::PrintError(errorMessage);
    statusLabel->Text = "Download of performance " + bellboardId + " cancelled due to error";
    downloadBtn->Enabled = false;
}

System::Void
DownloadPealUI::Completed(System::String^ bellboardId, System::Boolean errors)
{
    bool showingPealUI = false;
    Duco::ObjectId similarId = database->FindDuplicateObject(static_cast<const Duco::RingingObject&>(parser->CurrentPerformance()), TObjectType::EPeal);
    bool pealDone = false;
    if (similarId.ValidId())
    {
        System::String^ existingBellBoardId = database->BellboardId(similarId);
        if (String::Compare(existingBellBoardId, bellboardId) != 0)
        {
            statusLabel->Text = "";
            BellBoardConfirmIdUpdate^ confirmDialog = gcnew BellBoardConfirmIdUpdate(database);

            Duco::Peal existingPeal;
            database->FindPeal(similarId, existingPeal, false);
            confirmDialog->PopulateView(parser->CurrentPerformance(), existingPeal);

            System::Windows::Forms::DialogResult result = confirmDialog->ShowDialog(this->MdiParent);

            switch (result)
            {
            case System::Windows::Forms::DialogResult::Cancel:
                break;

            case System::Windows::Forms::DialogResult::Yes:
                if (!database->AddWebReferencesToPeal(similarId, bellboardId, parser->CurrentPerformance().RingingWorldReference()))
                {
                    SoundUtils::PlayErrorSound();
                    missingPealIds->Clear();
                }
                else
                {
                    pealDone = true;
                }
                break;
            case System::Windows::Forms::DialogResult::No:
                break;
            }
        }
        else if (existingBellBoardId->Length > 0)
        {
            // Cant check BB id is identical - because it can change
            PealUI::ShowPeal(database, this->MdiParent, similarId);
            MessageBox::Show(this, "A similar performance with a matching Bellboard ID has been found", "Matching performance found", MessageBoxButtons::OK, MessageBoxIcon::Information);
            SoundUtils::PlayErrorSound();
            missingPealIds->Clear();
            pealDone = true;
        }
    }
    if (!pealDone)
    {
        if (errors)
        {
            PealUI^ displayPealUI = PealUI::PreparePealUI(database, MdiParent, parser->CurrentPerformance());
            displayPealUI->Show();
            showingPealUI = true;
            if (!parser->CurrentPerformance().AssociationId().ValidId())
            {
                displayPealUI->SetMissingAssociationFromDownload(DucoUtils::ConvertString(parser->CurrentPerformance().MissingAssociation()));
            }
            //Missing Tower
            if (parser->CurrentPerformance().MissingRingOnly())
            {
                displayPealUI->SetMissingRingFromDownload(DucoUtils::ConvertString(parser->CurrentPerformance().MissingRing(database->Database())));
            }
            else if (!parser->CurrentPerformance().TowerId().ValidId())
            {
                displayPealUI->SetMissingTowerFromDownload(DucoUtils::ConvertString(parser->CurrentPerformance().MissingTower()),
                    DucoUtils::ConvertString(parser->CurrentPerformance().MissingTenorWeight()),
                    DucoUtils::ConvertString(parser->CurrentPerformance().MissingTenorKey()),
                    DucoUtils::ConvertString(parser->CurrentPerformance().MissingTowerbaseId()),
                    DucoUtils::ConvertString(parser->CurrentPerformance().MissingDoveId()));
            }
            //Missing method
            if (!parser->CurrentPerformance().MethodId().ValidId())
            {
                displayPealUI->SetMissingMethodFromDownload(DucoUtils::ConvertString(parser->CurrentPerformance().MissingMethod()));
            }

            for (unsigned int bellNumber = 0; bellNumber <= parser->CurrentPerformance().NoOfBellsRung(database->Database()); ++bellNumber)
            {
                if (parser->CurrentPerformance().IsMissingRinger(bellNumber, false))
                {
                    displayPealUI->SetMissingRingerFromDownload(bellNumber, DucoUtils::ConvertString(parser->CurrentPerformance().MissingRinger(bellNumber, false, database->Database().Settings())), false, parser->CurrentPerformance().IsMissingRingerConductor(bellNumber, false));
                }
                if (parser->CurrentPerformance().IsMissingRinger(bellNumber, true))
                {
                    displayPealUI->SetMissingRingerFromDownload(bellNumber, DucoUtils::ConvertString(parser->CurrentPerformance().MissingRinger(bellNumber, true, database->Database().Settings())), true, parser->CurrentPerformance().IsMissingRingerConductor(bellNumber, false));
                }
            }
            displayPealUI->DownloadComplete();
            SetStatusLabelText();
            missingPealIds->Clear();
        }
        else
        {
            Peal* newPeal = new Peal(parser->CurrentPerformance());
            if (database->AddPeal(*newPeal).ValidId())
            {
                pealDone = true;
            }
            else
            {
                statusLabel->Text = "";
                SoundUtils::PlayErrorSound();
                missingPealIds->Clear();
            }
        }
    }

    if (parser->ImportedTower())
    {
        database->CallObservers(DucoUI::TEventType::EAdded, TObjectType::ETower, KNoId, KNoId);
    }
    if (parser->ImportedMethod())
    {
        database->CallObservers(DucoUI::TEventType::EAdded, TObjectType::EMethod, KNoId, KNoId);
    }
    if (pealDone)
    {
        HighlightKnownPeal(bellboardId, KDownloadedPealColour);
    }
}

Duco::ObjectId
DownloadPealUI::ChooseRinger(System::String^ missingRingerName, const std::set<Duco::RingerPealDates>& nearMatches)
{
    Duco::ObjectId id;
    DucoUI::ChooseOrGenerateRinger^ chooseRingerDialog = gcnew DucoUI::ChooseOrGenerateRinger(database, nearMatches, parser->CurrentPerformance(), missingRingerName, false);
    if (chooseRingerDialog->ShowDialog() != ::DialogResult::OK)
    {
        id.ClearId();
    }
    else
    {
        id = chooseRingerDialog->SelectedId();
    }
    return id;
}

System::Void
DownloadPealUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    if (eventId == DucoUI::TEventType::EAdded && type == Duco::TObjectType::EPeal && id.ValidId())
    {
        HighlightKnownPealsAndSetMissingIds(false);
    }
}

System::Void
DownloadPealUI::zoomOutBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Task<Double>^ zoomTask = CefSharp::WebBrowserExtensions::GetZoomLevelAsync(chromeBrowser);
    zoomTask->Wait(1000);

    Double value = zoomTask->Result;
    value -= KZoomIncrement;
    database->Config().SetChromeZoomLevel(value);

    CefSharp::WebBrowserExtensions::SetZoomLevel(chromeBrowser, value);
}

System::Void
DownloadPealUI::zoomInBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Task<Double>^ zoomTask = CefSharp::WebBrowserExtensions::GetZoomLevelAsync(chromeBrowser);
    zoomTask->Wait(1000);

    Double value = zoomTask->Result;
    value += KZoomIncrement;
    database->Config().SetChromeZoomLevel(value);

    CefSharp::WebBrowserExtensions::SetZoomLevel(chromeBrowser, value);
}

System::Void
DownloadPealUI::clearZoom_Click(System::Object^  sender, System::EventArgs^  e)
{
    database->Config().SetChromeZoomLevel(0.0);
    CefSharp::WebBrowserExtensions::SetZoomLevel(chromeBrowser, 0.0);
}

void
DownloadPealUI::SetStatusLabelText()
{
    statusLabel->Text = "";
    if (parser->ImportedTower())
    {
        Tower theTower;
        if (database->FindTower(parser->CurrentPerformance().TowerId(), theTower, false))
        {
            AppendStatusLabelText("Imported new tower (" + DucoUtils::ConvertString(theTower.City()) + ")");
        }
        else
        {
            AppendStatusLabelText("Imported new tower");
        }
    }
    if (parser->ImportedMethod())
    {
        AppendStatusLabelText("Imported new method");
    }
    if (!parser->CurrentPerformance().AssociationId().ValidId())
    {
        AppendStatusLabelText("Unknown association");
    }
    if (!parser->CurrentPerformance().TowerId().ValidId())
    {
        AppendStatusLabelText("Unknown tower");
    }
    else if (!parser->CurrentPerformance().RingId().ValidId())
    {
        AppendStatusLabelText("Unknown ring in tower (" + DucoUtils::ConvertString(parser->CurrentPerformance().MissingTenorWeight()) + ")");
    }
    if (parser->CurrentPerformance().HasMissingRingers())
    {
        AppendStatusLabelText("Unknown ringers: "+ DucoUtils::ConvertString(parser->CurrentPerformance().MissingRingers(database->Database().Settings())));
    }
}

void
DownloadPealUI::AppendStatusLabelText(System::String^ nextText)
{
    if (statusLabel->Text->Length > 0 && nextText->Length > 0)
    {
        statusLabel->Text = statusLabel->Text + "; " + nextText;
    }
    else if (nextText->Length > 0)
    {
        statusLabel->Text = nextText->Trim();
    }
}
