#pragma once

#include "PrintUtilBase.h"
#include <set>
#include <map>

namespace Duco
{
    class ObjectId;
    class PealLengthInfo;
    class PerformanceDate;
}

namespace DucoUI
{
    public ref class PrintBandSummaryUtil : public PrintUtilBase
    {
    public:
        PrintBandSummaryUtil(DucoUI::DatabaseManager^ theDatabase, System::Drawing::Printing::PrintPageEventArgs^ printArgs, const Duco::ObjectId& newPealId);
        PrintBandSummaryUtil(DucoUI::DatabaseManager^ theDatabase, System::Drawing::Printing::PrintPageEventArgs^ printArgs, const Duco::ObjectId& newTowerId, const std::set<Duco::ObjectId>& ringerIds);
        ~PrintBandSummaryUtil();
        virtual System::Void printObject(System::Drawing::Printing::PrintPageEventArgs^ args, System::Drawing::Printing::PrinterSettings^ printSettings) override;

        System::Boolean             morePages;

    protected:
        Duco::ObjectId printChangesCount(System::Drawing::Printing::PrintPageEventArgs^ args, const Duco::ObjectId& pealId);
        System::String^ PrintPeal();
        float FindLongestRingerName(const std::map<Duco::ObjectId, std::map<unsigned int, Duco::PealLengthInfo>>& ringers, System::Drawing::Printing::PrintPageEventArgs^ printArgs);

        System::Drawing::Font^      smallBoldFont;
        Duco::ObjectId*             pealId;
        Duco::ObjectId*             towerId;
        std::set<Duco::ObjectId>*   ringerIds;
        Duco::PerformanceDate*      performanceDate;

    };
}