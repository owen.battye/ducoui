// DucoUI.cpp : main project file.
#include "ProgressWrapper.h"
#include "ImportExportProgressWrapper.h"
#include "RingingDatabaseObserver.h"
#include "DucoWindowState.h"
#include "DatabaseManager.h"
#include "CheckLatestVersion.h"
#include "PrintAssociationReportUtil.h"
#include "Form1.h"
#include "DucoUILog.h"

#include <stdlib.h>
#include <assert.h>
using namespace CefSharp;
using namespace CefSharp::WinForms;
using namespace DucoUI;
using namespace System;
using namespace System::Windows::Forms;

[STAThreadAttribute]
int main(array<System::String ^> ^args)
{
    // Enabling Windows XP visual effects before any controls are created
    Application::EnableVisualStyles();
    Application::SetCompatibleTextRenderingDefault(false); 
    CefSettings^ settings = gcnew CefSettings();
    settings->CachePath = System::IO::Path::Combine(Environment::GetFolderPath(Environment::SpecialFolder::LocalApplicationData), "CefSharp\\Cache");
    Cef::Initialize(settings);
    Cef::EnableHighDPISupport();

    // Create the main window and run it
    try
    {
        Form1^ mainForm = gcnew Form1(args);
        Application::Run(mainForm);
        delete mainForm;
    }
    catch (Exception^ ex)
    {
#if defined(_DEBUG)
        System::Diagnostics::Debug::Assert(false);
#endif
        DucoUI::DucoUILog::PrintError(ex->ToString());
        Console::Error->WriteLine("Uncaught exception running DucoUI");
        Console::Error->WriteLine(ex->ToString());
        MessageBox::Show(ex->ToString(), "Uncaught exception running DucoUI");
    }
    Cef::Shutdown();
    DucoUI::DucoUILog::Close();
#ifdef _DEBUG
    GC::Collect();
    GC::WaitForPendingFinalizers();
#endif
    return 0;
}
