#include "RingingDatabaseObserver.h"
#include <StatisticFilters.h>
#include "DatabaseManager.h"
#include "RingerDisplayCache.h"
#include "GenericTablePrintUtil.h"

#include "TowersCircledUI.h"

#include "DucoUtils.h"
#include "SystemDefaultIcon.h"
#include "DucoUIUtils.h"
#include "SoundUtils.h"
#include "RingingDatabaseObserver.h"
#include "DucoWindowState.h"
#include "OpenObjectCallback.h"
#include "PictureListControl.h"
#include "PictureControl.h"
#include <Ring.h>
#include "TowerUI.h"
#include "FiltersUI.h"
#include <RingingDatabase.h>
#include <DucoConfiguration.h>
#include <PealDatabase.h>
#include "DatabaseManager.h"
#include "DucoTableSorter.h"
#include <Tower.h>
#include <DatabaseSettings.h>
#include <DucoEngineUtils.h>
#include "DucoUILog.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Drawing::Printing;
using namespace System::Windows::Forms;
using namespace System::Diagnostics;


TowersCircledUI::TowersCircledUI(DucoUI::DatabaseManager^ theDatabase)
: database(theDatabase), columnCount(0), totalNumberOfTowersCircled(0), restartGenerator(false), loading(true)
{
    filters = new Duco::StatisticFilters(database->Database());
    filters->SetIncludeLinkedTowers(false);
    filters->SetDefaultRinger();
    InitializeComponent();
}

TowersCircledUI::!TowersCircledUI()
{
    delete filters;
    database->RemoveObserver(this);
}

TowersCircledUI::~TowersCircledUI()
{
    this->!TowersCircledUI();
    if (components)
    {
        delete components;
    }
}

void 
TowersCircledUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    switch (type)
    {
        case TObjectType::EPeal:
        case TObjectType::ETower:
            StartGenerator();
            break;

        default:
            break;
    }
}

System::Void 
TowersCircledUI::TowersCircledUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    loading = false;
    StartGenerator();
    database->AddObserver(this);
}

System::Void 
TowersCircledUI::GeneratePealData(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
    try
    {
        totalNumberOfTowersCircled = 0;
        BackgroundWorker^ worker = dynamic_cast<BackgroundWorker^>(sender);
        if ( worker->CancellationPending )
        {
            e->Cancel = true;
            return;
        }
        std::map<Duco::ObjectId, Duco::PealLengthInfo> sortedTowerIds;
        database->Database().PealsDatabase().GetTowersPealCount(*filters, sortedTowerIds);

        double noOfPeals = double(sortedTowerIds.size());
        double count (0);

        bool ignoreTowersWithOnePeal(false);
        if (database->Config().DisableFeaturesForPerformance(sortedTowerIds.size()) || sortedTowerIds.size() > 250)
        {
            ignoreTowersWithOnePeal = true;
        }
        // Populate tower names
        std::map<Duco::ObjectId, Duco::PealLengthInfo>::const_iterator it = sortedTowerIds.begin();
        while (it != sortedTowerIds.end() && !worker->CancellationPending)
        {
            DUCOUILOGDEBUG("Start: " + DucoUtils::ConvertString(it->first.Str()));
            if (ignoreTowersWithOnePeal && it->second.TotalPeals() <= 1)
            {
                ++count;
            }
            else
            {
                Duco::Tower nextTower;
                if (database->FindTower(it->first, nextTower, true))
                {
                    DataGridViewRow^ newRow = GenerateNextTower(*filters, nextTower, it->second);
                    if (newRow != nullptr)
                        worker->ReportProgress(int((++count / noOfPeals) * double(100)), newRow);
                    else
                        worker->ReportProgress(int((++count / noOfPeals) * double(100)));
                }
                else
                {
                    ++count;
                }
            }
            DUCOUILOGDEBUG("\tFinished: " + DucoUtils::ConvertString(it->first.Str()));
            ++it;
        }
    }
    catch (Exception^)
    {
        DucoUILog::PrintError("Error generating tower circling data");
        SoundUtils::PlayErrorSound();
    }
}

System::Windows::Forms::DataGridViewRow^
TowersCircledUI::GenerateNextTower(const Duco::StatisticFilters& filters, const Duco::Tower& tower, const Duco::PealLengthInfo& pealCounts)
{
    size_t numberOfTimesCircled(0);
    Duco::PealLengthInfo numberOfPeals;
    std::map<unsigned int, Duco::CirclingData> bellPealCount;
    PerformanceDate firstCircledDate;
    size_t numberOfDaysToFirstCircle(0);
    StatisticFilters localFilters(filters);
    localFilters.SetTower(true, tower.Id());

    database->Database().GetTowerCircling(localFilters, bellPealCount, numberOfPeals, numberOfTimesCircled, firstCircledDate, numberOfDaysToFirstCircle);
    DUCOUILOGDEBUG("\tGot: " + DucoUtils::ConvertString(tower.Id().Str()));

    // Add tower name, and peal totals here.
    DataGridViewRow^ newRow = gcnew DataGridViewRow();
    newRow->HeaderCell->Value = DucoUtils::ConvertString(tower.Id().Str());
    DataGridViewCellCollection^ theCells = newRow->Cells;

    DataGridViewTextBoxCell^ idCell = gcnew DataGridViewTextBoxCell();
    idCell->Value = DucoUtils::ConvertString(tower.Id());
    theCells->Add(idCell);

    DataGridViewTextBoxCell^ towerNameCell = gcnew DataGridViewTextBoxCell();
    towerNameCell->Value = DucoUtils::ConvertString(tower.FullName());
    theCells->Add(towerNameCell);

    DataGridViewTextBoxCell^ towerCircledCell = gcnew DataGridViewTextBoxCell();
    towerCircledCell->Value = Convert::ToString(numberOfTimesCircled);
    theCells->Add(towerCircledCell);
    if (numberOfTimesCircled >= 1)
        ++totalNumberOfTowersCircled;

    DataGridViewTextBoxCell^ towerPealCountCell = gcnew DataGridViewTextBoxCell();
    towerPealCountCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(pealCounts.TotalPeals(), true));
    theCells->Add(towerPealCountCell);

    DataGridViewTextBoxCell^ dateCell = gcnew DataGridViewTextBoxCell();
    if (numberOfTimesCircled > 0)
    {
        dateCell->Value = DucoUtils::ConvertString(firstCircledDate.Str());
        dateCell->ToolTipText = "It took " + Convert::ToString(numberOfDaysToFirstCircle) + " for first circle from first peal";
    }
    theCells->Add(dateCell);

    bool addTower = true;
    std::map<unsigned int, Duco::CirclingData>::const_iterator it2 = bellPealCount.begin();
    while (it2 != bellPealCount.end())
    {
        DataGridViewTextBoxCell^ nextBellCell = gcnew DataGridViewTextBoxCell();

        if (it2->second.PealCount() > 0)
            addTower = true;
        String^ newBellPealCountString = Convert::ToString(it2->second.PealCount());
        nextBellCell->Value = newBellPealCountString;
        theCells->Add(nextBellCell);
        ++it2;
    }
    if (!addTower)
    {
        return nullptr;
    }
    return newRow;
}

System::Void 
TowersCircledUI::backgroundWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    if (restartGenerator)
    {
        StartGenerator();
    }
    else
    {
        circledTowerData->Refresh();
        towersCircled->Text = Convert::ToString(totalNumberOfTowersCircled) + " towers circled";

        DucoTableSorter^ sorter = gcnew DucoTableSorter(true, EString);
        sorter->AddSortObject(ECount, false, 3);
        sorter->AddSortObject(ECount, false, 2);
        sorter->AddSortObject(EString, true, 1);
        circledTowerData->Sort(sorter);

        DataGridViewColumn^ newColumn = circledTowerData->Columns[3];
        newColumn->HeaderCell->SortGlyphDirection = SortOrder::Descending;
        progressBar->Value = 0;
    }
}

System::Void 
TowersCircledUI::CheckEnoughColumnsExist(unsigned int noOfBells)
{
    if (columnCount < noOfBells)
    {
        for (; columnCount < noOfBells; ++columnCount)
        {
            String^ newColumnName = String::Format("Bell{0:D}", columnCount+1);
            String^ newColumnTitle = Convert::ToString(columnCount+1);
            int columnId = circledTowerData->Columns->Add(newColumnName, newColumnTitle);
            circledTowerData->Columns[columnId]->Width=30;
            circledTowerData->Columns[columnId]->AutoSizeMode = DataGridViewAutoSizeColumnMode::None;
            circledTowerData->Columns[columnId]->SortMode = DataGridViewColumnSortMode::Programmatic;
        }
    }
}

System::Void 
TowersCircledUI::backgroundWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    progressBar->Value = e->ProgressPercentage;

    if (e->UserState != nullptr)
    {
        DataGridViewRow^ newRow = (DataGridViewRow^)e->UserState;
        CheckEnoughColumnsExist(newRow->Cells->Count-5);
        circledTowerData->Rows->Add(newRow);
    }
}

System::Void
TowersCircledUI::ResetAllSortGlyphs()
{
    CircledColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
    Peals->HeaderCell->SortGlyphDirection = SortOrder::None;
    dateColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
    for (int i = 3; i < circledTowerData->ColumnCount; ++i)
    {
        circledTowerData->Columns[i]->HeaderCell->SortGlyphDirection = SortOrder::None;
    }
}

System::Void
TowersCircledUI::circledTowerData_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
    DataGridViewColumn^ newColumn = circledTowerData->Columns[e->ColumnIndex];
    if (newColumn->SortMode == System::Windows::Forms::DataGridViewColumnSortMode::Automatic)
    {
        ResetAllSortGlyphs();
        return;
    }
    DataGridViewColumn^ oldColumn = circledTowerData->SortedColumn;
    if (oldColumn != nullptr && oldColumn != newColumn)
    {
        oldColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
    }

    SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;
    if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
        newSortOrder = SortOrder::Ascending;
    else
        newSortOrder = SortOrder::Descending;
    ResetAllSortGlyphs();

    newColumn->HeaderCell->SortGlyphDirection = newSortOrder;

    int secondColumnId = 3;
    if (e->ColumnIndex == 3)
        secondColumnId = 2;

    DucoTableSorter^ sorter = gcnew DucoTableSorter(newSortOrder == SortOrder::Ascending, EString);

    if (e->ColumnIndex == 4)
    {
        sorter->AddSortObject(EDate, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
    }
    else
    {
        sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
        sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, secondColumnId);
        sorter->AddSortObject(EString, newSortOrder == SortOrder::Ascending, 1);
    }
    circledTowerData->Sort(sorter);
}

System::Void 
TowersCircledUI::StartGenerator()
{
    if (loading)
        return;

    if (backgroundWorker->IsBusy)
    {
        restartGenerator = true;
        backgroundWorker->CancelAsync();
        return;
    }

    ResetAllSortGlyphs();
    towersCircled->Text = "";
    circledTowerData->Rows->Clear();
    if (circledTowerData->Columns->Count > 4)
    {
        while (columnCount > 0)
        {
            String^ newColumnName = String::Format("Bell{0:D}", columnCount--);
            circledTowerData->Columns->Remove(newColumnName);
        }
    }
    restartGenerator = false;

    backgroundWorker->RunWorkerAsync();
}

System::Void
TowersCircledUI::circledTowerData_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    try
    {
        unsigned int rowIndex = e->RowIndex;
        DataGridViewRow^ currentRow = circledTowerData->Rows[rowIndex];
        unsigned int towerId = Convert::ToInt16(currentRow->Cells[0]->Value);
        TowerUI::ShowTower(database, this->MdiParent, towerId, KNoId);
    }
    catch (Exception^)
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
TowersCircledUI::filtersBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    FiltersUI^ filtersDialog = gcnew FiltersUI(*filters, database);
    System::Windows::Forms::DialogResult returnVal = filtersDialog->ShowDialog(this);
    if (returnVal == System::Windows::Forms::DialogResult::OK)
    {
        StartGenerator();
    }
}

System::Void 
TowersCircledUI::printBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    DucoUIUtils::Print(this, sender, gcnew System::Drawing::Printing::PrintPageEventHandler( this, &TowersCircledUI::printData ), "tower circling", database->Database().Settings().UsePrintPreview(), true);
}

System::Void
TowersCircledUI::printData(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args)
{
    if (printUtil == nullptr)
    {
        printUtil = gcnew GenericTablePrintUtil(database, args, "Towers circled by ");
        printUtil->SetObjects(circledTowerData, filters->RingerId(), TObjectType::ERinger, "", 5, circledTowerData->Columns->Count);
    }
    PrintDocument^ printDoc = static_cast<PrintDocument^>(sender);
    printUtil->printObject(args, printDoc->PrinterSettings);
    if (!args->HasMorePages)
    {
        printUtil = nullptr;
    }
}