#pragma once
namespace DucoUI
{

    /// <summary>
    /// Summary for SelectPictureUI
    /// </summary>
    public ref class SelectPictureUI : public System::Windows::Forms::Form
    {
    public:
        SelectPictureUI(DucoUI::DatabaseManager^ theDatabase);
        System::Void ShowPicture(Duco::ObjectId& pictureId);

        event System::EventHandler^         pictureEventHandler;

    protected:
        ~SelectPictureUI();
        System::Void SelectPictureUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void pictureListCancelled(System::Object^  sender, System::EventArgs^  e);
        System::Void pictureSelected(System::Object^  sender, System::EventArgs^  e);

    private:
        DucoUI::DatabaseManager^  database;
        DucoUI::PictureListControl^     pictureList;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        void InitializeComponent(void)
        {
            this->SuspendLayout();
            // 
            // SelectPictureUI
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(959, 496);
            this->KeyPreview = true;
            this->Name = L"SelectPictureUI";
            this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
            this->Text = L"Select picture";
            this->Load += gcnew System::EventHandler(this, &SelectPictureUI::SelectPictureUI_Load);
            this->ResumeLayout(false);

        }
#pragma endregion
    };
}
