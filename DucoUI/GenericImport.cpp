#include "GenericImport.h"
#include "DucoUtils.h"
#include <PealBaseTsvImporter.h>
#include <PealBasePbrImporter.h>
#include "DatabaseManager.h"
#include <CsvImporter.h>
#include <WinRkVersion1Importer.h>
#include "DucoCommon.h"

using namespace Duco;
using namespace DucoUI;

GenericImport::GenericImport(DucoUI::DatabaseManager^ theDatabase,
    DucoUI::WinRkImportProgressCallback^ newCallback, System::String^ theFileName,
    const std::string& theInstallDir, TFileType newFileType)
: DatabaseImportUtil(theDatabase, newCallback, theFileName), fileType(newFileType)
{
    installDir = new std::string(theInstallDir);
    engineCallback = new ProgressCallbackWrapper(this);
}

GenericImport::!GenericImport()
{
    delete installDir;
    delete engineCallback;
    delete fileImporter;
}

GenericImport::~GenericImport()
{
    this->!GenericImport();
}

void
GenericImport::Import(System::ComponentModel::BackgroundWorker^ worker, System::ComponentModel::DoWorkEventArgs^ e)
{
    bgWorker = worker;
    std::string theFileName;
    DucoUtils::ConvertString(fileName, theFileName);
    delete fileImporter;
    fileImporter = NULL;

    switch (fileType)
    {
    case EPealBaseTSV:
        {
        fileImporter = new PealBaseTsvImporter(*engineCallback, database->Database());
        }
        break;

    case EPealBasePBR:
        {
        fileImporter = new PealBasePbrImporter(*engineCallback, database->Database());
        }
        break;

    case ECsv:
        {
        fileImporter = new CsvImporter(*engineCallback, database->Database(), *installDir, KDefaultCsvImportSettings);
        }
        break;

    case EWinRkVersion1:
        {
        fileImporter = new WinRkVersion1Importer(*engineCallback, database->Database());
        }
        break;

    default:
        break;
    }
    if (fileImporter != NULL)
    {
        fileImporter->Import(theFileName);
    }
}

System::String^
GenericImport::FileType()
{
    switch (fileType)
    {
    case EWinRkVersion1:
        return "WinRk (version 1)";

    case EPealBaseTSV:
        return "PealBase - TSV";

    case EPealBasePBR:
        return "PealBase - PBR";

    case ECsv:
        return "CSV File";

    default:
        break;
    }
    return "Unknown file type";
}

void
GenericImport::Initialised()
{
    ReportProgress(DucoUI::WinRkImportProgressCallback::TImportStage::EImportingPeals, 100, 0);
}

void
GenericImport::Step(int progressPercent)
{
    ReportProgress(DucoUI::WinRkImportProgressCallback::TImportStage::EImportingPeals, 100, progressPercent);
    if (bgWorker != nullptr && bgWorker->CancellationPending && fileImporter != NULL)
    {
        fileImporter->Cancel();
    }
}

void
GenericImport::Complete()
{
}

double 
GenericImport::StageNumber(WinRkImportProgressCallback::TImportStage currentStage)
{
    return (double)0;
}

double
GenericImport::TotalNoOfStages()
{
    return (double)1;
}