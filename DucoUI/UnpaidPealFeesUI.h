#pragma once

namespace DucoUI
{
    public ref class UnpaidPealFeesUI : public System::Windows::Forms::Form
    {
    public:
        UnpaidPealFeesUI(DucoUI::DatabaseManager^ theDatabase);

    protected:
        ~UnpaidPealFeesUI();

        System::Void CloseBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void CopyBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void ItemChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void Association_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);

        System::Void UnpaidPealFeesUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void Regenerate();
        System::Void DataGridView1_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);
        System::Void dataGridView1_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);

    protected:
        System::Windows::Forms::ComboBox^       feePayers;
        System::ComponentModel::Container^      components;
        System::Windows::Forms::DataGridView^   dataGridView1;
        System::Windows::Forms::ComboBox^       association;
        DucoUI::RingerDisplayCache^             ringerCache;
        System::Boolean                         generating;
        System::Collections::Generic::List<System::Int16>^ allAssociationIds;

    protected: 
        DucoUI::DatabaseManager^ database;
        System::Windows::Forms::DataGridViewTextBoxColumn^  idColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  pealNameColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  dateColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  FeePayerColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  paidColumn;
    private: System::Windows::Forms::NumericUpDown^ yearSelector;

    private: System::Windows::Forms::CheckBox^ enableYear;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ owingColumn;

#pragma region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        void InitializeComponent(void)
        {
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
            System::Windows::Forms::Panel^ settingsPanel;
            System::Windows::Forms::Label^ yearLbl;
            System::Windows::Forms::Label^ feePayerLbl;
            System::Windows::Forms::Label^ associationLbl;
            System::Windows::Forms::Button^ copyBtn;
            System::Windows::Forms::Button^ closeBtn;
            this->enableYear = (gcnew System::Windows::Forms::CheckBox());
            this->yearSelector = (gcnew System::Windows::Forms::NumericUpDown());
            this->association = (gcnew System::Windows::Forms::ComboBox());
            this->feePayers = (gcnew System::Windows::Forms::ComboBox());
            this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
            this->idColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->pealNameColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->dateColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->FeePayerColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->paidColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->owingColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            settingsPanel = (gcnew System::Windows::Forms::Panel());
            yearLbl = (gcnew System::Windows::Forms::Label());
            feePayerLbl = (gcnew System::Windows::Forms::Label());
            associationLbl = (gcnew System::Windows::Forms::Label());
            copyBtn = (gcnew System::Windows::Forms::Button());
            closeBtn = (gcnew System::Windows::Forms::Button());
            tableLayoutPanel1->SuspendLayout();
            settingsPanel->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->yearSelector))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
            this->SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 2;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute, 84)));
            tableLayoutPanel1->Controls->Add(settingsPanel, 0, 0);
            tableLayoutPanel1->Controls->Add(copyBtn, 0, 2);
            tableLayoutPanel1->Controls->Add(closeBtn, 1, 2);
            tableLayoutPanel1->Controls->Add(this->dataGridView1, 0, 1);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 3;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 29)));
            tableLayoutPanel1->Size = System::Drawing::Size(677, 436);
            tableLayoutPanel1->TabIndex = 0;
            // 
            // settingsPanel
            // 
            settingsPanel->AutoSize = true;
            settingsPanel->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            tableLayoutPanel1->SetColumnSpan(settingsPanel, 2);
            settingsPanel->Controls->Add(this->enableYear);
            settingsPanel->Controls->Add(this->yearSelector);
            settingsPanel->Controls->Add(yearLbl);
            settingsPanel->Controls->Add(this->association);
            settingsPanel->Controls->Add(feePayerLbl);
            settingsPanel->Controls->Add(associationLbl);
            settingsPanel->Controls->Add(this->feePayers);
            settingsPanel->Dock = System::Windows::Forms::DockStyle::Fill;
            settingsPanel->Location = System::Drawing::Point(0, 0);
            settingsPanel->Margin = System::Windows::Forms::Padding(0);
            settingsPanel->Name = L"settingsPanel";
            settingsPanel->Size = System::Drawing::Size(677, 83);
            settingsPanel->TabIndex = 0;
            // 
            // enableYear
            // 
            this->enableYear->AutoSize = true;
            this->enableYear->Location = System::Drawing::Point(134, 62);
            this->enableYear->Name = L"enableYear";
            this->enableYear->Size = System::Drawing::Size(82, 17);
            this->enableYear->TabIndex = 7;
            this->enableYear->Text = L"Enable year";
            this->enableYear->UseVisualStyleBackColor = true;
            this->enableYear->CheckedChanged += gcnew System::EventHandler(this, &UnpaidPealFeesUI::ItemChanged);
            // 
            // yearSelector
            // 
            this->yearSelector->Location = System::Drawing::Point(70, 60);
            this->yearSelector->Name = L"yearSelector";
            this->yearSelector->Size = System::Drawing::Size(58, 20);
            this->yearSelector->TabIndex = 6;
            this->yearSelector->ValueChanged += gcnew System::EventHandler(this, &UnpaidPealFeesUI::ItemChanged);
            // 
            // yearLbl
            // 
            yearLbl->AutoSize = true;
            yearLbl->Location = System::Drawing::Point(35, 62);
            yearLbl->Name = L"yearLbl";
            yearLbl->Size = System::Drawing::Size(29, 13);
            yearLbl->TabIndex = 5;
            yearLbl->Text = L"Year";
            // 
            // association
            // 
            this->association->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
            this->association->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->association->FormattingEnabled = true;
            this->association->Location = System::Drawing::Point(70, 6);
            this->association->Name = L"association";
            this->association->Size = System::Drawing::Size(379, 21);
            this->association->TabIndex = 4;
            this->association->SelectedIndexChanged += gcnew System::EventHandler(this, &UnpaidPealFeesUI::Association_SelectedIndexChanged);
            // 
            // feePayerLbl
            // 
            feePayerLbl->AutoSize = true;
            feePayerLbl->Location = System::Drawing::Point(10, 36);
            feePayerLbl->Name = L"feePayerLbl";
            feePayerLbl->Size = System::Drawing::Size(54, 13);
            feePayerLbl->TabIndex = 3;
            feePayerLbl->Text = L"Fee payer";
            // 
            // associationLbl
            // 
            associationLbl->AutoSize = true;
            associationLbl->Location = System::Drawing::Point(3, 9);
            associationLbl->Name = L"associationLbl";
            associationLbl->Size = System::Drawing::Size(61, 13);
            associationLbl->TabIndex = 2;
            associationLbl->Text = L"Association";
            // 
            // feePayers
            // 
            this->feePayers->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
            this->feePayers->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->feePayers->FormattingEnabled = true;
            this->feePayers->Location = System::Drawing::Point(70, 33);
            this->feePayers->Name = L"feePayers";
            this->feePayers->Size = System::Drawing::Size(379, 21);
            this->feePayers->TabIndex = 1;
            this->feePayers->SelectedIndexChanged += gcnew System::EventHandler(this, &UnpaidPealFeesUI::ItemChanged);
            // 
            // copyBtn
            // 
            copyBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            copyBtn->Location = System::Drawing::Point(515, 410);
            copyBtn->Name = L"copyBtn";
            copyBtn->Size = System::Drawing::Size(75, 23);
            copyBtn->TabIndex = 9;
            copyBtn->Text = L"Copy";
            copyBtn->UseVisualStyleBackColor = true;
            copyBtn->Click += gcnew System::EventHandler(this, &UnpaidPealFeesUI::CopyBtn_Click);
            // 
            // closeBtn
            // 
            closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            closeBtn->Location = System::Drawing::Point(599, 410);
            closeBtn->Name = L"closeBtn";
            closeBtn->Size = System::Drawing::Size(75, 23);
            closeBtn->TabIndex = 10;
            closeBtn->Text = L"Close";
            closeBtn->UseVisualStyleBackColor = true;
            closeBtn->Click += gcnew System::EventHandler(this, &UnpaidPealFeesUI::CloseBtn_Click);
            // 
            // dataGridView1
            // 
            this->dataGridView1->AllowUserToAddRows = false;
            this->dataGridView1->AllowUserToDeleteRows = false;
            this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->dataGridView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(6) {
                this->idColumn,
                    this->pealNameColumn, this->dateColumn, this->FeePayerColumn, this->paidColumn, this->owingColumn
            });
            tableLayoutPanel1->SetColumnSpan(this->dataGridView1, 2);
            this->dataGridView1->Dock = System::Windows::Forms::DockStyle::Fill;
            this->dataGridView1->Location = System::Drawing::Point(3, 86);
            this->dataGridView1->Name = L"dataGridView1";
            this->dataGridView1->ReadOnly = true;
            this->dataGridView1->Size = System::Drawing::Size(671, 318);
            this->dataGridView1->TabIndex = 11;
            this->dataGridView1->CellContentDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &UnpaidPealFeesUI::DataGridView1_CellContentDoubleClick);
            this->dataGridView1->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &UnpaidPealFeesUI::dataGridView1_ColumnHeaderMouseClick);
            // 
            // idColumn
            // 
            this->idColumn->HeaderText = L"Id";
            this->idColumn->Name = L"idColumn";
            this->idColumn->ReadOnly = true;
            this->idColumn->Visible = false;
            // 
            // pealNameColumn
            // 
            this->pealNameColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->pealNameColumn->HeaderText = L"Peal";
            this->pealNameColumn->Name = L"pealNameColumn";
            this->pealNameColumn->ReadOnly = true;
            this->pealNameColumn->Width = 53;
            // 
            // dateColumn
            // 
            this->dateColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->dateColumn->HeaderText = L"Date";
            this->dateColumn->Name = L"dateColumn";
            this->dateColumn->ReadOnly = true;
            this->dateColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->dateColumn->Width = 55;
            // 
            // FeePayerColumn
            // 
            this->FeePayerColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->FeePayerColumn->HeaderText = L"Fee Payer";
            this->FeePayerColumn->Name = L"FeePayerColumn";
            this->FeePayerColumn->ReadOnly = true;
            this->FeePayerColumn->Width = 80;
            // 
            // paidColumn
            // 
            this->paidColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->paidColumn->HeaderText = L"Paid";
            this->paidColumn->Name = L"paidColumn";
            this->paidColumn->ReadOnly = true;
            this->paidColumn->Width = 53;
            // 
            // owingColumn
            // 
            this->owingColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->owingColumn->HeaderText = L"Owing";
            this->owingColumn->Name = L"owingColumn";
            this->owingColumn->ReadOnly = true;
            this->owingColumn->Width = 62;
            // 
            // UnpaidPealFeesUI
            // 
            this->AcceptButton = closeBtn;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->CancelButton = closeBtn;
            this->ClientSize = System::Drawing::Size(677, 436);
            this->Controls->Add(tableLayoutPanel1);
            this->Name = L"UnpaidPealFeesUI";
            this->Text = L"Unpaid peal fees";
            this->Load += gcnew System::EventHandler(this, &UnpaidPealFeesUI::UnpaidPealFeesUI_Load);
            tableLayoutPanel1->ResumeLayout(false);
            tableLayoutPanel1->PerformLayout();
            settingsPanel->ResumeLayout(false);
            settingsPanel->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->yearSelector))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
            this->ResumeLayout(false);

        }
#pragma endregion
    };
}
