#pragma once
#include "PrintUtilBase.h"
#include "ProgressWrapper.h"
namespace Duco
{
    class Composition;
    class LeadEndCollection;
}

namespace DucoUI
{
    ref class DatabaseManager;

    public ref class PrintCompositionUtil : public DucoUI::PrintUtilBase, public DucoUI::ProgressCallbackUI
    {
    public:
        PrintCompositionUtil(DucoUI::DatabaseManager^ theDatabase, System::Drawing::Printing::PrintPageEventArgs^ printArgs);
        void SetObjects(Duco::Composition* theMethod, System::Windows::Forms::DataGridView^ theDataGridView);
        virtual System::Void printObject(System::Drawing::Printing::PrintPageEventArgs^ args, System::Drawing::Printing::PrinterSettings^ printSettings) override;

        !PrintCompositionUtil();
        ~PrintCompositionUtil();

    public: // ProgressCallbackUI
        virtual void Initialised();
        virtual void Step(int progressPercent);
        virtual void Complete();

    protected:
        System::Void PrintFirstPage(System::Drawing::Printing::PrintPageEventArgs^ args);
        System::Void PrintLeadEnds(System::Drawing::Printing::PrintPageEventArgs^ args);

    protected:
        Duco::Composition*          currentComposition;
        System::Drawing::Pen^       courseEndPen;
        System::Drawing::Pen^       partEndPen;
        System::Windows::Forms::DataGridView^ dataGridView;
        Duco::LeadEndCollection*    leadEnds;

        float                       leadEndWidth;
    };
}
