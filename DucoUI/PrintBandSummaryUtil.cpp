#include "PrintBandSummaryUtil.h"
#include <RingerCirclingData.h>
#include "DucoUtils.h"
#include "DatabaseManager.h"
#include <RingingDatabase.h>
#include <Ringer.h>
#include <Peal.h>
#include <PealDatabase.h>
#include <Tower.h>
#include "DucoCommon.h"
#include <StatisticFilters.h>

using namespace System;
using namespace System::Drawing;
using namespace Duco;
using namespace DucoUI;

const float KFontSize = 10;
const float KMarginOffset = 30;

PrintBandSummaryUtil::PrintBandSummaryUtil(DatabaseManager^ theDatabase,
                                           System::Drawing::Printing::PrintPageEventArgs^ printArgs,
                                           const Duco::ObjectId& newPealId)
    : PrintUtilBase(theDatabase, KFontSize, printArgs, false), morePages(false)
{
    smallBoldFont = gcnew System::Drawing::Font( KDefaultPrintFont, KFontSize-2, FontStyle::Bold);
    pealId = new Duco::ObjectId(newPealId);
    ringerIds = new std::set<Duco::ObjectId>();
    performanceDate = new Duco::PerformanceDate();
    Duco::Peal peal;
    if (database->FindPeal(*pealId, peal, false))
    {
        peal.RingerIds(*ringerIds);
        towerId = new Duco::ObjectId(peal.TowerId());
        (*performanceDate) = peal.Date();
    }
}

PrintBandSummaryUtil::PrintBandSummaryUtil(DucoUI::DatabaseManager^ theDatabase,
                                           System::Drawing::Printing::PrintPageEventArgs^ printArgs,
                                           const Duco::ObjectId& newTowerId,
                                           const std::set<Duco::ObjectId>& newRingerIds)
    : PrintUtilBase(theDatabase, KFontSize, printArgs, false), morePages(false)
{
    smallBoldFont = gcnew System::Drawing::Font(KDefaultPrintFont, KFontSize - 2, FontStyle::Bold);
    towerId = new Duco::ObjectId(newTowerId);
    ringerIds = new std::set<Duco::ObjectId>(newRingerIds);
    pealId = new Duco::ObjectId();
    performanceDate = new Duco::PerformanceDate();
}

PrintBandSummaryUtil::~PrintBandSummaryUtil()
{
    delete pealId;
    delete towerId;
    delete ringerIds;
    delete performanceDate;
}

System::Void
PrintBandSummaryUtil::printObject(System::Drawing::Printing::PrintPageEventArgs^ args, System::Drawing::Printing::PrinterSettings^ printSettings)
{
    addDucoFooter(smallFont, gcnew StringFormat, args);

    StringFormat^ defaultStringFormat = gcnew StringFormat();
    array<Single>^ tabStops = { KMarginOffset };
    defaultStringFormat->SetTabStops(KMarginOffset, tabStops);

    String^ printBuffer = "Tower circling info from database: ";
    printBuffer += DucoUtils::FormatFilename(database->Filename());
    args->Graphics->DrawString(printBuffer, boldFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
    yPos += lineHeight;
    printBuffer = "At " + database->TowerName(*towerId);
    args->Graphics->DrawString(printBuffer, normalFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
    yPos += lineHeight;
    if (pealId->ValidId())
    {
        printBuffer = "For the band in " + PrintPeal();
        args->Graphics->DrawString(printBuffer, smallFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
        yPos += smallLineHeight;
        printBuffer = "Up to and including peal number " + DucoUtils::ConvertString(pealId->Str());
        args->Graphics->DrawString(printBuffer, smallFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
        yPos += smallLineHeight;
    }
    yPos += smallLineHeight;

    StatisticFilters filters(database->Database());
    filters.SetTower(true, *towerId);
    std::map<Duco::ObjectId, std::map<unsigned int, Duco::PealLengthInfo>> circlingData;
    database->Database().PealsDatabase().GetRingersTowerCircling(filters, *pealId, *ringerIds, circlingData);

    System::Collections::Generic::List<Single>^ tabStopsForRingers = gcnew System::Collections::Generic::List<Single>();
    tabStopsForRingers->Add(FindLongestRingerName(circlingData, args));
    String^ printBufferForBellNumbers = "";
    if (circlingData.size() > 0)
    {
        for (int count = 1; count <= circlingData.begin()->second.size(); ++count)
        {
            printBufferForBellNumbers += "\t" + Convert::ToString(count);
            tabStopsForRingers->Add(20.0f);
        }
    }
    defaultStringFormat->SetTabStops(KMarginOffset, tabStopsForRingers->ToArray());
    args->Graphics->DrawString(printBufferForBellNumbers, boldFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
    yPos += lineHeight;

    if (circlingData.size() > 0)
    {
        std::map<Duco::ObjectId, std::map<unsigned int, Duco::PealLengthInfo>>::const_iterator ringerId = circlingData.begin();
        while (ringerId != circlingData.end())
        {
            Duco::Ringer ringer;
            if (database->FindRinger(ringerId->first, ringer, false))
            {
                String^ printBuffer = "";
                printBuffer += DucoUtils::ConvertString(ringer.FullName(false));
                std::map<unsigned int, Duco::PealLengthInfo>::const_iterator bells = ringerId->second.begin();
                while (bells != ringerId->second.end())
                {
                    printBuffer += "\t" + Convert::ToString(bells->second.TotalPeals());

                    ++bells;
                }
                args->Graphics->DrawString(printBuffer, smallFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
                yPos += smallLineHeight;
            }
            ++ringerId;
        }
    }
    Duco::ObjectId lastPealId = printChangesCount(args, KNoId);
    if (lastPealId != *pealId && pealId->ValidId())
        printChangesCount(args, *pealId);
}

Duco::ObjectId
PrintBandSummaryUtil::printChangesCount(System::Drawing::Printing::PrintPageEventArgs^ args, const Duco::ObjectId& lastPealId)
{
    StringFormat^ defaultStringFormat = gcnew StringFormat();
    System::String^ printBuffer = "";
    Duco::PercentageStatistics stats = database->Database().PealsDatabase().GetRingersPercentages(*towerId, *ringerIds, lastPealId);
    if (stats.ContainsResults())
    {
        yPos += 2 * lineHeight;
        args->Graphics->DrawString("Percentage of changes and performances", boldFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
        yPos += lineHeight;
        args->Graphics->DrawString("From a total of " + stats.NoOfPeals().ToString("n0") + " " + database->PerformanceString(false) + "s and " + stats.NoOfChanges().ToString("n0") + " changes", smallFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
        yPos += smallLineHeight;
        if (lastPealId.ValidId())
            printBuffer = "Performances in tower up to and including peal number " + DucoUtils::ConvertString(lastPealId.Str());
        else
            printBuffer = "Including all " + database->PerformanceString(false) + "s in tower";
        args->Graphics->DrawString(printBuffer, smallFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
        yPos += smallLineHeight;

        std::multiset<Duco::RingerCount> counts = stats.ChangeCounts();
        std::multiset<Duco::RingerCount>::const_reverse_iterator it2 = counts.rbegin();
        while (it2 != counts.rend())
        {
            double percentage = it2->PercentageOfChanges();
            if (percentage < 0.095)
                printBuffer = String::Format("{0} has rung in {1:n0} changes, thats {2:P1}", database->RingerName(it2->RingerId(), *performanceDate), it2->NumberOfChanges(), percentage);
            else
                printBuffer = String::Format("{0} has rung in {1:n0} changes, thats {2:P0}", database->RingerName(it2->RingerId(), *performanceDate), it2->NumberOfChanges(), percentage);
            args->Graphics->DrawString(printBuffer, normalFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
            yPos += lineHeight;
            printBuffer = String::Format("and has rung in {1:n0} peals", database->RingerName(it2->RingerId(), *performanceDate), it2->NumberOfPeals());
            args->Graphics->DrawString(printBuffer, smallFont, Brushes::Black, 2*leftMargin, yPos, defaultStringFormat);
            yPos += smallLineHeight;
            ++it2;
        }
    }
    return stats.LastPealId();
}

System::String^
PrintBandSummaryUtil::PrintPeal()
{
    String^ printBuffer = "";
    Duco::Peal peal;
    if (database->FindPeal(*pealId, peal, false))
    {
        printBuffer += DucoUtils::ConvertString(peal.ShortTitleWithDate());
    }
    return printBuffer;
}

float
PrintBandSummaryUtil::FindLongestRingerName(const std::map<Duco::ObjectId, std::map<unsigned int, Duco::PealLengthInfo>>& ringers, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
{
    float longestName = 0.0F;
    std::map<Duco::ObjectId, std::map<unsigned int, Duco::PealLengthInfo>>::const_iterator it = ringers.begin();
    while (it != ringers.end())
    {
        SizeF stringSize = printArgs->Graphics->MeasureString(database->RingerName(it->first, *performanceDate), normalFont, printArgs->PageBounds.Width);
        longestName = Math::Max(longestName, stringSize.Width);
        ++it;
    }
    return longestName;
}
