#pragma once
namespace DucoUI
{

    public ref class AlphabetsUI : public System::Windows::Forms::Form
    {
    public:
        AlphabetsUI(DucoUI::DatabaseManager^ theDatabase);

    protected:
        !AlphabetsUI();
        ~AlphabetsUI();
        System::Void AlphabetsUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void AlphabetsUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e);
        System::Void filterBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void settingsUpdated(System::Object^  sender, System::EventArgs^  e);

        System::Void dataView_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);
        System::Void dataView_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);

        System::Void StartGeneration();
        System::Void backgroundGenerator_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void backgroundGenerator_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);

    private:
        DucoUI::DatabaseManager^                    database;
        Duco::StatisticFilters*                     filters;
        System::ComponentModel::BackgroundWorker^   backgroundGenerator;
        System::ComponentModel::IContainer^         components;
        System::Windows::Forms::DataGridView^       dataView;
        System::Boolean                             lastDisplayedDates;
        System::Boolean                             autoRestart;
        System::Windows::Forms::ToolStripMenuItem^  uniqueMethods;
        System::Windows::Forms::ToolStripMenuItem^  insideBellOnly;


#pragma region Windows Form Designer generated code
    protected:
        void InitializeComponent()
        {
            System::Windows::Forms::ToolStrip^  toolStrip1;
            System::Windows::Forms::ToolStripButton^  filterBtn;
            System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(AlphabetsUI::typeid));
            System::Windows::Forms::ToolStripDropDownButton^  settingsBtn;
            this->uniqueMethods = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->insideBellOnly = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->dataView = (gcnew System::Windows::Forms::DataGridView());
            this->backgroundGenerator = (gcnew System::ComponentModel::BackgroundWorker());
            toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
            filterBtn = (gcnew System::Windows::Forms::ToolStripButton());
            settingsBtn = (gcnew System::Windows::Forms::ToolStripDropDownButton());
            toolStrip1->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataView))->BeginInit();
            this->SuspendLayout();
            // 
            // toolStrip1
            // 
            toolStrip1->GripStyle = System::Windows::Forms::ToolStripGripStyle::Hidden;
            toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) { filterBtn, settingsBtn });
            toolStrip1->Location = System::Drawing::Point(0, 0);
            toolStrip1->Name = L"toolStrip1";
            toolStrip1->Size = System::Drawing::Size(458, 25);
            toolStrip1->TabIndex = 1;
            toolStrip1->Text = L"toolStrip1";
            // 
            // filterBtn
            // 
            filterBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
            filterBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"filterBtn.Image")));
            filterBtn->ImageTransparentColor = System::Drawing::Color::Magenta;
            filterBtn->Name = L"filterBtn";
            filterBtn->Size = System::Drawing::Size(42, 22);
            filterBtn->Text = L"&Filters";
            filterBtn->Click += gcnew System::EventHandler(this, &AlphabetsUI::filterBtn_Click);
            // 
            // settingsBtn
            // 
            settingsBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
            settingsBtn->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
                this->uniqueMethods,
                    this->insideBellOnly
            });
            settingsBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"settingsBtn.Image")));
            settingsBtn->ImageTransparentColor = System::Drawing::Color::Magenta;
            settingsBtn->Name = L"settingsBtn";
            settingsBtn->Size = System::Drawing::Size(62, 22);
            settingsBtn->Text = L"&Settings";
            // 
            // uniqueMethods
            // 
            this->uniqueMethods->CheckOnClick = true;
            this->uniqueMethods->Name = L"uniqueMethods";
            this->uniqueMethods->Size = System::Drawing::Size(162, 22);
            this->uniqueMethods->Text = L"&Unique methods";
            this->uniqueMethods->ToolTipText = L"Only use a method once - ie no method appears in two alphabets.";
            this->uniqueMethods->Click += gcnew System::EventHandler(this, &AlphabetsUI::settingsUpdated);
            // 
            // insideBellOnly
            // 
            this->insideBellOnly->CheckOnClick = true;
            this->insideBellOnly->Name = L"insideBellOnly";
            this->insideBellOnly->Size = System::Drawing::Size(162, 22);
            this->insideBellOnly->Text = L"&Inside bell only";
            this->insideBellOnly->ToolTipText = L"Only show peals where the chosen ringer is not ringing the treble or the method i"
                L"s a principle";
            this->insideBellOnly->Click += gcnew System::EventHandler(this, &AlphabetsUI::settingsUpdated);
            // 
            // dataView
            // 
            this->dataView->AllowUserToAddRows = false;
            this->dataView->AllowUserToDeleteRows = false;
            this->dataView->ClipboardCopyMode = System::Windows::Forms::DataGridViewClipboardCopyMode::EnableAlwaysIncludeHeaderText;
            this->dataView->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->dataView->Dock = System::Windows::Forms::DockStyle::Fill;
            this->dataView->Location = System::Drawing::Point(0, 25);
            this->dataView->Name = L"dataView";
            this->dataView->ReadOnly = true;
            this->dataView->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
            this->dataView->Size = System::Drawing::Size(458, 647);
            this->dataView->TabIndex = 4;
            this->dataView->CellDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &AlphabetsUI::dataView_CellContentDoubleClick);
            this->dataView->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &AlphabetsUI::dataView_ColumnHeaderMouseClick);
            // 
            // backgroundGenerator
            // 
            this->backgroundGenerator->WorkerSupportsCancellation = true;
            this->backgroundGenerator->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &AlphabetsUI::backgroundGenerator_DoWork);
            this->backgroundGenerator->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &AlphabetsUI::backgroundGenerator_RunWorkerCompleted);
            // 
            // AlphabetsUI
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(458, 661);
            this->Controls->Add(this->dataView);
            this->Controls->Add(toolStrip1);
            this->MinimumSize = System::Drawing::Size(395, 501);
            this->Name = L"AlphabetsUI";
            this->Text = L"Alphabet completions";
            this->Load += gcnew System::EventHandler(this, &AlphabetsUI::AlphabetsUI_Load);
            toolStrip1->ResumeLayout(false);
            toolStrip1->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataView))->EndInit();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion
    };
}
