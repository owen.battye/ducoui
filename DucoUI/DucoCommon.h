#pragma once

namespace DucoUI
{
#define KErrorColour System::Drawing::Color::LightPink

#define KWebSite          L"www.rocketpole.co.uk"
#define KDucoOnline       L"http://rocketpole.synology.me:60251/upload.html"
#define KDefaultPrintFont L"Tahoma"
#define KUIVersionNumber  L"1.21.0"

// Sizes for laying out the ringer list
const int KLabelXLocation = 0;
const int KEditorXLocation = 50;
const int KConductorXLocation = 275;
const int KStrapperXLocation = 312;
const int KYPosition = 1;
const int KYLabelPosition = 4;
const int KLabelHeight = 13;
const int KLabelWidth = 48;
const int KEditorHeight = 21;
const int KEditorWidth = 215;
const int KCheckBoxHeight = 17;
const int KCheckBoxWidth = 14;
const int KRingerLineHeight = 25;
const int KRingerAndConductorLineHeight = 45;
const int KRingerWidth = 315;
const int KFirstControlYOffSet = 21;
// END Sizes for laying out the ringer list

#define KDefaultCsvImportSettings "csvimportsettings.config"
#define KRingNameSuffix L" bell peal"

} // end namespace Duco
