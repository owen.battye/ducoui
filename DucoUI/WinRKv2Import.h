#pragma once
#include "DatabaseImportUtil.h"
#include <set>

namespace Duco
{
    class Ringer;
    struct OldRingerNameChange;
}

namespace DucoUI
{
    ref class DatabaseManager;
    interface class WinRkImportProgressCallback;

ref class WinRKv2Import : public DatabaseImportUtil
{
public:
    WinRKv2Import(DucoUI::DatabaseManager^ theDatabase, DucoUI::WinRkImportProgressCallback^ newCallback, System::String^ theFileName);
    virtual ~WinRKv2Import();
    virtual System::String^ FileType() override;

    virtual double TotalNoOfStages() override;
    virtual double StageNumber(DucoUI::WinRkImportProgressCallback::TImportStage currentStage) override;

protected:
    virtual void Import(System::ComponentModel::BackgroundWorker^ worker, System::ComponentModel::DoWorkEventArgs^  e) override;

    void ImportPeals(System::Data::Odbc::OdbcCommand^ command,
                     const std::map<unsigned int, Duco::ObjectId>& societies, 
                     const std::map<unsigned int, std::wstring>& composers, 
                     const std::map<Duco::ObjectId, Duco::ObjectId>& ringerAkas,
                     const std::map<unsigned int, std::wstring>& towerTenors,
                     System::ComponentModel::BackgroundWorker^ worker);
    void ImportRingers(System::Data::Odbc::OdbcCommand^ command, std::map<Duco::ObjectId, Duco::ObjectId>& ringerAkas, System::ComponentModel::BackgroundWorker^ worker);
    void ImportTowers(System::Data::Odbc::OdbcCommand^ command, std::map<unsigned int, std::wstring>& towerTenors,
                     System::ComponentModel::BackgroundWorker^ worker);

    Duco::ObjectId SuggestMethodId(const std::wstring& methodName, const std::wstring& methodType, unsigned int order);
    Duco::ObjectId FindOrAddRing(unsigned int towerId, unsigned int noOfBells, const std::map<unsigned int, std::wstring>& towerTenors);
    void GenerateBells(unsigned int noOfBellsInRing, unsigned int noOfBellsInTower, std::set<unsigned int>& newBells);
    void CheckForOldNameChanges(Duco::Ringer& ringer, const Duco::ObjectId& otherRingerId, std::map<Duco::ObjectId, Duco::OldRingerNameChange*>& unprocessedRingerAkas, std::map<Duco::ObjectId, Duco::ObjectId>& ringerAkas);
};

}