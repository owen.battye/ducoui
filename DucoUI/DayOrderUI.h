#pragma once

namespace DucoUI
{

    public ref class DayOrderUI : public System::Windows::Forms::Form, public DucoUI::RingingDatabaseObserver
    {
    public:
        DayOrderUI(DucoUI::DatabaseManager^ theDatabase);

        // from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

    protected:
        ~DayOrderUI();

        System::Void downBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void upBtn_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void DayOrderUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void dateSelector_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void pealList_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);

        System::Void GenerateData(int dateIndex);
        System::Void EnableButtons(int position, int count);
        unsigned int SelectedPealId(int index);
        int SelectedPealIndex();
        System::Void SelectLastPeal();

    protected:
        DucoUI::DatabaseManager^            database;
        Duco::ObjectId*                     lastSelectedPealId;

    private:
        System::Windows::Forms::ListView^   pealList;
        System::Windows::Forms::ComboBox^   dateSelector;
        System::Windows::Forms::Button^     upBtn;
        System::Windows::Forms::Button^     downBtn;

        System::ComponentModel::Container^  components;

        void InitializeComponent()
        {
            System::Windows::Forms::Button^  closeBtn;
            System::Windows::Forms::Label^  datelbl;
            System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
            this->pealList = (gcnew System::Windows::Forms::ListView());
            this->dateSelector = (gcnew System::Windows::Forms::ComboBox());
            this->upBtn = (gcnew System::Windows::Forms::Button());
            this->downBtn = (gcnew System::Windows::Forms::Button());
            closeBtn = (gcnew System::Windows::Forms::Button());
            datelbl = (gcnew System::Windows::Forms::Label());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            tableLayoutPanel1->SuspendLayout();
            this->SuspendLayout();
            // 
            // closeBtn
            // 
            closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            closeBtn->Location = System::Drawing::Point(348, 143);
            closeBtn->Name = L"closeBtn";
            closeBtn->Size = System::Drawing::Size(75, 23);
            closeBtn->TabIndex = 7;
            closeBtn->Text = L"Close";
            closeBtn->UseVisualStyleBackColor = true;
            closeBtn->Click += gcnew System::EventHandler(this, &DayOrderUI::closeBtn_Click);
            // 
            // datelbl
            // 
            datelbl->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            datelbl->AutoSize = true;
            datelbl->Location = System::Drawing::Point(48, 7);
            datelbl->Margin = System::Windows::Forms::Padding(3, 7, 3, 3);
            datelbl->Name = L"datelbl";
            datelbl->Size = System::Drawing::Size(30, 13);
            datelbl->TabIndex = 1;
            datelbl->Text = L"Date";
            // 
            // pealList
            // 
            tableLayoutPanel1->SetColumnSpan(this->pealList, 3);
            this->pealList->Dock = System::Windows::Forms::DockStyle::Fill;
            this->pealList->HideSelection = false;
            this->pealList->Location = System::Drawing::Point(3, 30);
            this->pealList->MultiSelect = false;
            this->pealList->Name = L"pealList";
            this->pealList->Size = System::Drawing::Size(420, 107);
            this->pealList->TabIndex = 3;
            this->pealList->UseCompatibleStateImageBehavior = false;
            this->pealList->View = System::Windows::Forms::View::List;
            this->pealList->SelectedIndexChanged += gcnew System::EventHandler(this, &DayOrderUI::pealList_SelectedIndexChanged);
            // 
            // dateSelector
            // 
            tableLayoutPanel1->SetColumnSpan(this->dateSelector, 2);
            this->dateSelector->Dock = System::Windows::Forms::DockStyle::Fill;
            this->dateSelector->FormattingEnabled = true;
            this->dateSelector->Location = System::Drawing::Point(84, 3);
            this->dateSelector->Name = L"dateSelector";
            this->dateSelector->Size = System::Drawing::Size(339, 21);
            this->dateSelector->TabIndex = 2;
            this->dateSelector->SelectedIndexChanged += gcnew System::EventHandler(this, &DayOrderUI::dateSelector_SelectedIndexChanged);
            // 
            // upBtn
            // 
            this->upBtn->Enabled = false;
            this->upBtn->Location = System::Drawing::Point(3, 143);
            this->upBtn->Name = L"upBtn";
            this->upBtn->Size = System::Drawing::Size(75, 23);
            this->upBtn->TabIndex = 5;
            this->upBtn->Text = L"Up";
            this->upBtn->UseVisualStyleBackColor = true;
            this->upBtn->Click += gcnew System::EventHandler(this, &DayOrderUI::upBtn_Click);
            // 
            // downBtn
            // 
            this->downBtn->Enabled = false;
            this->downBtn->Location = System::Drawing::Point(84, 143);
            this->downBtn->Name = L"downBtn";
            this->downBtn->Size = System::Drawing::Size(75, 23);
            this->downBtn->TabIndex = 5;
            this->downBtn->Text = L"Down";
            this->downBtn->UseVisualStyleBackColor = true;
            this->downBtn->Click += gcnew System::EventHandler(this, &DayOrderUI::downBtn_Click);
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 3;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute, 81)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute, 81)));
            tableLayoutPanel1->Controls->Add(closeBtn, 2, 2);
            tableLayoutPanel1->Controls->Add(this->dateSelector, 1, 0);
            tableLayoutPanel1->Controls->Add(this->upBtn, 0, 2);
            tableLayoutPanel1->Controls->Add(this->downBtn, 1, 2);
            tableLayoutPanel1->Controls->Add(datelbl, 0, 0);
            tableLayoutPanel1->Controls->Add(this->pealList, 0, 1);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 3;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 27)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 29)));
            tableLayoutPanel1->Size = System::Drawing::Size(426, 169);
            tableLayoutPanel1->TabIndex = 8;
            // 
            // DayOrderUI
            // 
            this->AcceptButton = closeBtn;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->CancelButton = closeBtn;
            this->ClientSize = System::Drawing::Size(426, 169);
            this->Controls->Add(tableLayoutPanel1);
            this->MaximizeBox = false;
            this->MinimizeBox = false;
            this->Name = L"DayOrderUI";
            this->Text = L"Day ordering";
            this->Load += gcnew System::EventHandler(this, &DayOrderUI::DayOrderUI_Load);
            tableLayoutPanel1->ResumeLayout(false);
            tableLayoutPanel1->PerformLayout();
            this->ResumeLayout(false);

        }
    };
}
