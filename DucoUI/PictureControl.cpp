#include "RingingDatabaseObserver.h"
#include "OpenObjectCallback.h"
#include "DatabaseManager.h"
#include "RingerDisplayCache.h"
#include "PictureListControl.h"
#include "ProgressWrapper.h"
#include <DatabaseSearch.h>
#include "SearchUtils.h"
#include "PrintPealSearchUtil.h"

#include "PictureControl.h"

#include <Picture.h>
#include "DucoUtils.h"
#include <DucoConfiguration.h>
#include "SoundUtils.h"
#include "OpenObjectCallback.h"
#include "PealSearchUI.h"
#include "GenericTablePrintUtil.h"
#include "TowerSearchUI.h"
#include "SelectPictureUI.h"
#include <Peal.h>
#include <PealDatabase.h>
#include <PictureDatabase.h>
#include "PictureEventArgs.h"
#include <RingingDatabase.h>

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace System::IO;
using namespace System::Runtime::InteropServices;

PictureControl::PictureControl(DucoUI::DatabaseManager^ theDatabase)
    : database(theDatabase), enableSelection(false), selected(false), parent(nullptr), currentPeal(NULL)
{
    currentPicture = new Duco::Picture();
    InitializeComponent();
}

PictureControl::PictureControl(DucoUI::DatabaseManager^ theDatabase, DucoUI::PictureListControl^ theParent)
    : database(theDatabase), enableSelection(false), selected(false), parent(theParent), currentPeal(NULL)
{
    currentPicture = new Duco::Picture();
    InitializeComponent();
}

PictureControl::PictureControl(DucoUI::DatabaseManager^ theDatabase, DucoUI::PictureListControl^ theParent, Duco::Picture& newPicture)
    : database(theDatabase), enableSelection(false), selected(false), parent(theParent), currentPeal(NULL)
{
    currentPicture = new Duco::Picture(newPicture);
    InitializeComponent();

    UpdatePicture(true, false);
}

PictureControl::!PictureControl()
{
    delete currentPicture;
}

PictureControl::~PictureControl()
{
    this->!PictureControl();
    if (components)
    {
        delete components;
    }
}

System::Void
PictureControl::SetPicture(const Duco::ObjectId& pictureId, System::Boolean showSummary)
{
    if (!database->FindPicture(pictureId, *currentPicture, false))
    {
        delete currentPicture;
        currentPicture = new Duco::Picture();
    }
    UpdatePicture(showSummary, newBtn->Visible);
}

System::Void
PictureControl::SetPeal(Duco::Peal* thePeal)
{
    currentPeal = thePeal;
}

System::Void
PictureControl::SetViewMode()
{
    newBtn->Visible = false;
    removeBtn->Visible = false;
    existingBtn->Visible = false;
    pasteBtn->Visible = false;
    resizeBtn->Visible = currentPicture->ContainsValidPicture() && ImageRequiresScaling();
}

System::Void
PictureControl::SetEditMode(System::Boolean containsPicture)
{
    newBtn->Visible = true;
    removeBtn->Visible = containsPicture;
    existingBtn->Visible = true;
    pasteBtn->Visible = true;
    resizeBtn->Visible = false;
}

System::Void
PictureControl::UpdatePicture(System::Boolean showSummary, System::Boolean editMode)
{
    bool showPicture = false;
    if (currentPicture != NULL && currentPicture->ContainsValidPicture())
    {
        const std::string& pictureBuffer = currentPicture->GetPictureBuffer();
        try
        {
            array<Byte>^ data = gcnew array<Byte>(pictureBuffer.size());
            IntPtr test = IntPtr((void*)&pictureBuffer[0]);
            System::Runtime::InteropServices::Marshal::Copy(test, data, 0, pictureBuffer.size());

            MemoryStream^ memoryStream = gcnew MemoryStream(data);
            Image^ newImage = Image::FromStream(memoryStream);
            pictureBox->Image = newImage;
            memoryStream->Close();
            showPicture = true;

            System::Collections::Generic::List<System::UInt64>^ pealIds = gcnew System::Collections::Generic::List<System::UInt64>();
            System::Collections::Generic::List<System::UInt64>^ towerIds = gcnew System::Collections::Generic::List<System::UInt64>();
            if (database->FindPicture(currentPicture->Id(), pealIds, towerIds))
            {
                if (pealIds->Count == 1 && towerIds->Count == 0)
                {
                    associatedLbl->Text = "Peal number: " + Convert::ToString(pealIds[0]);
                }
                else if (towerIds->Count == 1 && pealIds->Count == 0)
                {
                    associatedLbl->Text = "Tower: " + Convert::ToString(towerIds[0]);
                }
                else if (pealIds->Count > 1 && towerIds->Count == 0)
                {
                    associatedLbl->Text = "Peal numbers: " + DucoUtils::ConvertString(pealIds);
                }
                else if (towerIds->Count > 1 && pealIds->Count == 0)
                {
                    associatedLbl->Text = "Towers:" + DucoUtils::ConvertString(pealIds);
                }
                else
                {
                    associatedLbl->Text = "Multiple associations";
                }
            }
            else
            {
                associatedLbl->Text = "Associated with no performances or towers";
            }

            sizeLbl->Text = DucoUtils::ConvertString(currentPicture->Id().Str());
        }
        catch (Exception^)
        {
            showPicture = false;
        }
    }
    if (showPicture)
    {
        newBtn->Visible = editMode;
        removeBtn->Visible = editMode;
        existingBtn->Visible = editMode;
        pasteBtn->Visible = editMode;
        resizeBtn->Visible = !editMode && ImageRequiresScaling();
    }
    else
    {
        resizeBtn->Visible = false;
        pictureBox->Image = nullptr;
        associatedLbl->Text = "";
        sizeLbl->Text = "";
    }
    if (!showSummary && currentPicture->Id().ValidId())
    {
        associatedLbl->Text = "Picture id: " + DucoUtils::ConvertString(currentPicture->Id().Str());
    }
    associatedLbl->Visible = true;
}

System::Void
PictureControl::pictureBox_DoubleClick(System::Object^  sender, System::EventArgs^  e)
{
    try
    {
        if (currentPicture->ContainsValidPicture())
        {
            String^ temporaryFilePath = Path::GetTempPath();
            String^ temporaryFileName = Path::GetFileNameWithoutExtension(Path::GetRandomFileName());

            std::string picturePath;
            DucoUtils::ConvertString(temporaryFilePath + temporaryFileName, picturePath);
            currentPicture->SavePicture(picturePath);
            System::Diagnostics::Process::Start(DucoUtils::ConvertString(picturePath));
        }
    }
    catch (Exception^ ex)
    {
        MessageBox::Show(ex->ToString(), "Error displaying picture");
    }
}

System::Void
PictureControl::AssociatedLbl_DoubleClick(System::Object^  sender, System::EventArgs^  e)
{
    if (callback != nullptr)
    {
        System::Collections::Generic::List<System::UInt64>^ pealIds = gcnew System::Collections::Generic::List<System::UInt64>();
        System::Collections::Generic::List<System::UInt64>^ towerIds = gcnew System::Collections::Generic::List<System::UInt64>();
        if (database->FindPicture(currentPicture->Id(), pealIds, towerIds))
        {
            if (pealIds->Count == 1)
            {
                Duco::ObjectId firstPealId (pealIds[0]);
                callback->OpenObject(firstPealId, TObjectType::EPeal);
            }
            else if (pealIds->Count > 1)
            {
                PealSearchUI^ pealSearchUI = gcnew PealSearchUI(database, false);
                pealSearchUI->Show(ParentForm->MdiParent, pealIds);
            }
            if (towerIds->Count == 1)
            {
                Duco::ObjectId firstTowerId (towerIds[0]);
                callback->OpenObject(firstTowerId, TObjectType::ETower);
            }
            else if (pealIds->Count > 1)
            {
                TowerSearchUI^ towerSearchUI = gcnew TowerSearchUI(database, false);
                towerSearchUI->ShowTowers(ParentForm->MdiParent, towerIds);
            }
        }
    }
}

System::Void
PictureControl::existingBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId pictureId = KNoId;
    
    SelectPictureUI^ dialog = gcnew SelectPictureUI(database);
    dialog->pictureEventHandler += gcnew System::EventHandler(this, &PictureControl::PictureSelected);

    if (currentPeal != NULL)
    {
        if (database->Database().PealsDatabase().FindPictureForTower(currentPeal->TowerId(), pictureId))
        {
            dialog->ShowPicture(pictureId);
        }
        else
        {
            SoundUtils::PlayErrorSound();
        }
    }
    dialog->ShowDialog();
}

System::Void
PictureControl::PictureSelected(System::Object^  sender, System::EventArgs^  e)
{
    pictureEventHandler(this, e);
}

System::Void
PictureControl::resizeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    System::String^ newFileName = "";
    if (!database->NothingInEditMode())
    {
        SoundUtils::PlayErrorSound();
    }
    else
    {
        System::IO::Stream^ pictureStream = GetPictureMemoryStream();

        if (Resize(pictureStream, newFileName, false))
        {
            std::string scaledPictureFilename;
            DucoUtils::ConvertString(newFileName, scaledPictureFilename);
            currentPicture->ReplacePicture(scaledPictureFilename);
            if (database->UpdatePicture(*currentPicture, true))
            {
                UpdatePicture(true, false);
            }
            else
            {
                SoundUtils::PlayErrorSound();
            }

            if (File::Exists(newFileName))
            {
                File::Delete(newFileName);
            }
        }
        pictureStream->Close();
    }
}

System::IO::Stream^
PictureControl::GetPictureMemoryStream()
{
    System::IO::MemoryStream^ pictureStream = gcnew System::IO::MemoryStream(currentPicture->GetPictureBuffer().size());
    std::string::const_iterator it = currentPicture->GetPictureBuffer().begin();
    while (it != currentPicture->GetPictureBuffer().end())
    {
        pictureStream->WriteByte(*it);
        ++it;
    }
    return pictureStream;
}

System::Drawing::Image^
PictureControl::ImageDimensions(System::IO::Stream^ pictureStream, System::Drawing::Size^% imageSize)
{
    Image^ original = nullptr;
    try
    {
        original = Image::FromStream(pictureStream);
        pictureStream->Close();
        imageSize = original->Size;
    }
    catch (Exception^ ex)
    {
        MessageBox::Show(Parent, "Cannot determine image dimensions: " + ex->ToString(), "Error - opening picture", MessageBoxButtons::OK, MessageBoxIcon::Warning);
    }
    return original;
}

System::Boolean
PictureControl::ImageRequiresScaling()
{
    if (!currentPicture->ContainsValidPicture())
        return false;

    System::Drawing::Size^ actualSize = nullptr;
    ImageDimensions(GetPictureMemoryStream(), actualSize);

    return actualSize->Width > database->Config().ImageMaxDimension() || actualSize->Height > database->Config().ImageMaxDimension();
}


System::Boolean
PictureControl::Resize(System::IO::Stream^ fileStream, System::String^% newFileName, System::Boolean promptForConfirmation)
{
    bool resized(false);

    try
    {
        System::Drawing::Size^ originalSize = nullptr;
        Image^ original = ImageDimensions(fileStream, originalSize);

        if (originalSize->Width > database->Config().ImageMaxDimension() || originalSize->Height > database->Config().ImageMaxDimension())
        {
            Double verticalRatio = originalSize->Height / database->Config().ImageMaxDimension();
            Double horitontalRatio = originalSize->Width / database->Config().ImageMaxDimension();
            System::Drawing::Size^ newSize = gcnew System::Drawing::Size();
            if (verticalRatio > horitontalRatio)
            {
                newSize->Height = int(Double(originalSize->Height) / verticalRatio);
                newSize->Width = int(Double(originalSize->Width) / verticalRatio);
            }
            else
            {
                newSize->Height = int(Double(originalSize->Height) / horitontalRatio);
                newSize->Width = int(Double(originalSize->Width) / horitontalRatio);
            }

            if (newSize->Width < originalSize->Width && newSize->Height < originalSize->Height)
            {
                if (promptForConfirmation)
                {
                    MessageBox::Show(Parent, "This picture is " + originalSize->Width + " by " + originalSize->Height + ", and will be resized to: " + newSize->Width + " by " + newSize->Height + ".", "Warning - picture size", MessageBoxButtons::OK, MessageBoxIcon::Warning);
                }

                Bitmap^ newBitmap = gcnew Bitmap(newSize->Width, newSize->Height, System::Drawing::Imaging::PixelFormat::Format24bppRgb);
                Graphics^ g = Graphics::FromImage(newBitmap);
                g->DrawImage(original, 0, 0, newSize->Width, newSize->Height);

                System::Drawing::Imaging::ImageCodecInfo^ jpgEncoder = GetEncoder(System::Drawing::Imaging::ImageFormat::Jpeg);
                System::Drawing::Imaging::Encoder^ encoder = System::Drawing::Imaging::Encoder::ColorDepth;
                System::Drawing::Imaging::EncoderParameters^ myEncoderParameters = gcnew System::Drawing::Imaging::EncoderParameters(1);
                System::Drawing::Imaging::EncoderParameter^ myEncoderParameter = gcnew System::Drawing::Imaging::EncoderParameter(encoder, __int64(database->Config().ImageColourDepth()));
                myEncoderParameters->Param[0] = myEncoderParameter;
                String^ tempFileName = Path::GetTempFileName();
                tempFileName = Path::ChangeExtension(tempFileName, ".jpg");
                newBitmap->Save(tempFileName, jpgEncoder, myEncoderParameters);
                newFileName = tempFileName;
                resized = true;
            }
        }
    }
    catch (Exception^ ex)
    {
        MessageBox::Show(Parent, ex->ToString(), "File resize failed", MessageBoxButtons::OK, MessageBoxIcon::Error);
        resized = false;
    }

    return resized;
}

System::Boolean
PictureControl::ClipboardContainsSupportedImage()
{
    if (!Clipboard::ContainsImage())
        return false;

    System::Drawing::Image^ image = Clipboard::GetImage();
    if (image == nullptr)
        return false;

    System::Drawing::Imaging::ImageFormat^ format = image->RawFormat;
    System::Drawing::Imaging::ImageFormat^ memoryBmp = System::Drawing::Imaging::ImageFormat::MemoryBmp;

    if (format->Equals(System::Drawing::Imaging::ImageFormat::Bmp)
        || format->Equals(System::Drawing::Imaging::ImageFormat::Jpeg)
        || format->Equals(System::Drawing::Imaging::ImageFormat::Gif)
        || format->Equals(System::Drawing::Imaging::ImageFormat::Png)
        || format->Equals(System::Drawing::Imaging::ImageFormat::MemoryBmp))
        return true;

    if (format->Guid.Equals(System::Drawing::Imaging::ImageFormat::Emf))
        return false;
    if (format == memoryBmp)
        return false;

    if (format->Equals(System::Drawing::Imaging::ImageFormat::MemoryBmp))
        return false;
    if (format->Guid.Equals(System::Drawing::Imaging::ImageFormat::Emf))
        return false;
    if (format->Guid.Equals(System::Drawing::Imaging::ImageFormat::Exif))
        return false;
    if (format->Guid.Equals(System::Drawing::Imaging::ImageFormat::Icon))
        return false;
    if (format->Guid.Equals(System::Drawing::Imaging::ImageFormat::Wmf))
        return false;
    if (format->Guid.Equals(System::Drawing::Imaging::ImageFormat::Tiff))
        return false;

    return false;
}

System::String^
PictureControl::TempFilename()
{
    String^ tempFileName = Path::GetTempFileName();
    Int32 lastIndex = tempFileName->LastIndexOf(".");
    tempFileName = tempFileName->Substring(0, lastIndex);

    
    System::Drawing::Imaging::ImageFormat^ format = Clipboard::GetImage()->RawFormat;
    if (format->Equals(System::Drawing::Imaging::ImageFormat::Bmp))
    {
        tempFileName += ".bmp";
    }
    else if (format->Equals(System::Drawing::Imaging::ImageFormat::Jpeg))
    {
        tempFileName += ".jpg";
    }
    else if (format->Equals(System::Drawing::Imaging::ImageFormat::Gif))
    {
        tempFileName += ".gif";
    }
    else if (format->Equals(System::Drawing::Imaging::ImageFormat::Png))
    {
        tempFileName += ".png";
    }
    else if (format->Equals(System::Drawing::Imaging::ImageFormat::MemoryBmp))
    {
        tempFileName += ".bmp";
    }

    return tempFileName;
}

System::Void
PictureControl::pasteBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (!ClipboardContainsSupportedImage())
    {
        SoundUtils::PlayErrorSound();
        return;
    }
    System::Drawing::Image^ image = Clipboard::GetImage();
    String^ tempFileName = TempFilename();
    image->Save(tempFileName);
    std::string fileName;
    DucoUtils::ConvertString(tempFileName, fileName);

    Duco::Picture newPicture(fileName);
    if (newPicture.PictureSize() < database->Config().MaximumImageSize() || MessageBox::Show(Parent, "Warning, This is a very large picture, large picture databases could run very slowly. Are you sure you want to continue?", "Warning - large picture", MessageBoxButtons::YesNo, MessageBoxIcon::Warning, MessageBoxDefaultButton::Button1) == ::DialogResult::Yes)
    {
        ObjectId newPictureId = database->AddPicture(newPicture);
        SetPicture(newPictureId, false);

        PictureEventArgs^ args = gcnew PictureEventArgs();
        args->pictureId = new Duco::ObjectId(newPictureId);
        pictureEventHandler(this, args);
    }
    if (File::Exists(tempFileName))
    {
        File::Delete(tempFileName);
    }
}

System::Void
PictureControl::newBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    unsigned long long picturesDatabaseSize = database->Database().PicturesDatabase().TotalSizeOfAllPictures();
    if (picturesDatabaseSize > database->Config().DatabaseMaximumSize())
    {
        if (MessageBox::Show(Parent, "Warning, your pictures database is getting very large. Are you sure you want to continue? Large databases could run slowly.", "Warning - database size", MessageBoxButtons::YesNo, MessageBoxIcon::Warning, MessageBoxDefaultButton::Button1) == ::DialogResult::No)
        {
            return;
        }
    }

    OpenFileDialog^ newDialog = gcnew OpenFileDialog();
    newDialog->Filter = "Image Files(*.bmp;*.jpeg;*.gif;*.png)|*.bmp;*.jpg;*.gif;*.png|All files (*.*)|*.*";

    if (newDialog->ShowDialog() == ::DialogResult::OK)
    {
        Stream^ originalFilehandle = newDialog->OpenFile();

        std::string fileName;
        System::String^ scaledFilename = "";
        if (Resize(originalFilehandle, scaledFilename, true))
        {
            DucoUtils::ConvertString(scaledFilename, fileName);
        }
        else
        {
            DucoUtils::ConvertString(newDialog->FileName, fileName);
        }

        Duco::Picture newPicture(fileName);
        if (newPicture.PictureSize() < database->Config().MaximumImageSize() || MessageBox::Show(Parent, "Warning, This is a very large picture, large picture databases could run very slowly. Are you sure you want to continue?", "Warning - large picture", MessageBoxButtons::YesNo, MessageBoxIcon::Warning, MessageBoxDefaultButton::Button1) == ::DialogResult::Yes)
        {
            ObjectId newPictureId = database->AddPicture(newPicture);
            SetPicture(newPictureId, false);

            PictureEventArgs^ args = gcnew PictureEventArgs();
            args->pictureId = new Duco::ObjectId(newPictureId);
            pictureEventHandler(this, args);
        }
        if (File::Exists(scaledFilename))
        {
            File::Delete(scaledFilename);
        }
    }
}

System::Drawing::Imaging::ImageCodecInfo^
PictureControl::GetEncoder(System::Drawing::Imaging::ImageFormat^ format)
{
    array<System::Drawing::Imaging::ImageCodecInfo^>^ codecs = System::Drawing::Imaging::ImageCodecInfo::GetImageDecoders();

    for each(System::Drawing::Imaging::ImageCodecInfo^ codec in codecs)
    {
        if (codec->FormatID == format->Guid)
        {
            return codec;
        }
    }
    return nullptr;
}

System::Void
PictureControl::removeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    SetPicture(KNoId, false);
    PictureEventArgs^ args = gcnew PictureEventArgs();
    pictureEventHandler(this, args);
}

Duco::ObjectId
PictureControl::PictureId()
{
    return currentPicture->Id();
}

System::Boolean
PictureControl::Selected()
{
    return selected;
}

System::Void
PictureControl::Unselect()
{
    selected = false;
    this->BorderStyle = System::Windows::Forms::BorderStyle::None;
}

System::Void
PictureControl::EnableSelection()
{
    enableSelection = true;
}

System::Void
PictureControl::DisableSelection()
{
    enableSelection = false;
}

System::Void
PictureControl::pictureBox_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (enableSelection)
    {
        selected = !selected;
        if (selected)
        {
            this->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
            if (parent != nullptr)
            {
                parent->PictureSelected(this);
            }
        }
        else
        {
            this->BorderStyle = System::Windows::Forms::BorderStyle::None;
        }
    }
}
