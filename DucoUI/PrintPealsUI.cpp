#include "DatabaseManager.h"
#include "PrintPealsUI.h"
#include "SystemDefaultIcon.h"
#include "DucoUIUtils.h"
#include "PrintSummaryUtil.h"
#include <Peal.h>
#include <Tower.h>
#include <RingingDatabase.h>
#include "PrintPealUtil.h"
#include <DatabaseSettings.h>
#include <StatisticFilters.h>

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::Windows::Forms;
using namespace System::Drawing::Printing;

PrintPealsUI::PrintPealsUI(DucoUI::DatabaseManager^ theDatabase)
    : database(theDatabase), printSummary(false)
{
    InitializeComponent();
    fromId = new Duco::ObjectId();
    toId = new Duco::ObjectId();
}

PrintPealsUI::!PrintPealsUI()
{
    delete fromId;
    delete toId;
}

PrintPealsUI::~PrintPealsUI()
{
    this->!PrintPealsUI();
    if (components)
    {
        delete components;
    }
}

System::Void
PrintPealsUI::PrintPealsUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    toSelector->Minimum = 1;
    fromSelector->Minimum = 1;

    StatisticFilters filters(database->Database());
    size_t totalNoOfPeals = database->Database().PerformanceInfo(filters).TotalPeals();
    toSelector->Maximum = totalNoOfPeals;
    fromSelector->Maximum = totalNoOfPeals;
}

System::Void
PrintPealsUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void
PrintPealsUI::printBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    DucoUIUtils::Print(this, sender, gcnew System::Drawing::Printing::PrintPageEventHandler( this, &PrintPealsUI::printPeals ), database->PerformanceString(false) + "s", database->Database().Settings().UsePrintPreview(), false);
}

System::Void
PrintPealsUI::printPeals(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args)
{
    PrintDocument^ printDoc = static_cast<PrintDocument^>(sender);
    if (printSummary)
    {
        PrintSummaryUtil printUtil(database, args);
        printUtil.printObject(args, printDoc->PrinterSettings);
        printSummary = false;
        return;
    }
    if (!fromId->ValidId())
    {
        Duco::ObjectId tempFrom (-1);
        Duco::ObjectId tempTo (-1);
        if (!allCheckBox->Checked)
        {
            *fromId = System::Convert::ToInt16(fromSelector->Value);
            *toId = Convert::ToInt16(toSelector->Value);
        }
        else if (database->FirstPeal(tempFrom, false) && database->LastPeal(tempTo))
        {
            *fromId = tempFrom;
            *toId = tempTo;
        }
        else
        {
            fromId->ClearId();
            toId->ClearId();
        }
    }

    if (*fromId <= *toId && fromId->ValidId())
    {
        Duco::Peal currentPeal;
        if (database->FindPeal(*fromId, currentPeal, false))
        {
            if (!currentPeal.Withdrawn() || includeWithdrawn->Checked)
            {
                Duco::Tower* currentTower = new Tower(); // TODO Memory leak
                if (database->FindTower(currentPeal.TowerId(), *currentTower, false))
                {
                    PrintPealUtil printUtil(database, args);
                    printUtil.SetObjects(&currentPeal, currentTower);
                    printUtil.printObject(args, printDoc->PrinterSettings);
                }
            }
        }
        ++(*fromId);
        if (*fromId <= *toId)
        {
            progressBar->Value = int(double(fromId->Id())  / double(toId->Id()) * 100);
            args->HasMorePages = true;
        }
        else
        {
            *fromId = -1;
            *toId = -1;
            progressBar->Value = 0;
            if (allCheckBox->Checked)
            {
                args->HasMorePages = true;
                printSummary = true;
            }
        }
    }
}

System::Void
PrintPealsUI::allCheckBox_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    toSelector->Enabled = !allCheckBox->Checked;
    fromSelector->Enabled = !allCheckBox->Checked;
}
