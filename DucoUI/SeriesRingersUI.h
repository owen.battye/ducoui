#pragma once
namespace DucoUI
{
	public ref class SeriesRingersUI : public System::Windows::Forms::Form
	{
	public:
		SeriesRingersUI(DucoUI::DatabaseManager^ theDatabase, const Duco::ObjectId& newSeriesId);

	protected:
		~SeriesRingersUI();
        !SeriesRingersUI();
        System::Void SeriesRingersUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void seriesSelector_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void printBtn_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void dataGridView_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);
        System::Void dataGridView1_CellPainting(System::Object^ sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^ e);
        System::Void printSeries(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args);

        System::Void StartGenerator();
        System::Void ResetProgrammaticColumnsSortGlyphs();
        System::Void ResetNonProgrammaticColumnsSortGlyphs();

    private:
        System::Windows::Forms::Button^         closeBtn;
        System::Windows::Forms::Button^         printBtn;
        System::Windows::Forms::DataGridView^   dataGridView1;
        System::Windows::Forms::ComboBox^       seriesSelector;
		System::ComponentModel::Container^      components;
        System::Collections::Generic::List<System::Int16>^ allSeriesNames;
        DucoUI::DatabaseManager^                database;
        Duco::ObjectId*                         currentSeriesId;

#pragma region Windows Form Designer generated code
		void InitializeComponent()
		{
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
            this->closeBtn = (gcnew System::Windows::Forms::Button());
            this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
            this->seriesSelector = (gcnew System::Windows::Forms::ComboBox());
            this->printBtn = (gcnew System::Windows::Forms::Button());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            tableLayoutPanel1->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
            this->SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 2;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->Controls->Add(this->closeBtn, 1, 2);
            tableLayoutPanel1->Controls->Add(this->dataGridView1, 0, 1);
            tableLayoutPanel1->Controls->Add(this->seriesSelector, 0, 0);
            tableLayoutPanel1->Controls->Add(this->printBtn, 0, 2);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 3;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 24)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 31)));
            tableLayoutPanel1->Size = System::Drawing::Size(717, 325);
            tableLayoutPanel1->TabIndex = 1;
            // 
            // closeBtn
            // 
            this->closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
            this->closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            this->closeBtn->Location = System::Drawing::Point(639, 299);
            this->closeBtn->Name = L"closeBtn";
            this->closeBtn->Size = System::Drawing::Size(75, 23);
            this->closeBtn->TabIndex = 0;
            this->closeBtn->Text = L"Close";
            this->closeBtn->UseVisualStyleBackColor = true;
            this->closeBtn->Click += gcnew System::EventHandler(this, &SeriesRingersUI::closeBtn_Click);
            // 
            // dataGridView1
            // 
            this->dataGridView1->AllowUserToAddRows = false;
            this->dataGridView1->AllowUserToDeleteRows = false;
            this->dataGridView1->ClipboardCopyMode = System::Windows::Forms::DataGridViewClipboardCopyMode::EnableAlwaysIncludeHeaderText;
            tableLayoutPanel1->SetColumnSpan(this->dataGridView1, 2);
            this->dataGridView1->Dock = System::Windows::Forms::DockStyle::Fill;
            this->dataGridView1->Location = System::Drawing::Point(3, 27);
            this->dataGridView1->Name = L"dataGridView1";
            this->dataGridView1->ReadOnly = true;
            this->dataGridView1->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
            this->dataGridView1->Size = System::Drawing::Size(711, 264);
            this->dataGridView1->TabIndex = 1;
            this->dataGridView1->CellPainting += gcnew System::Windows::Forms::DataGridViewCellPaintingEventHandler(this, &SeriesRingersUI::dataGridView1_CellPainting);
            this->dataGridView1->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &SeriesRingersUI::dataGridView_ColumnHeaderMouseClick);
            // 
            // seriesSelector
            // 
            this->seriesSelector->FormattingEnabled = true;
            this->seriesSelector->Location = System::Drawing::Point(3, 3);
            this->seriesSelector->Name = L"seriesSelector";
            this->seriesSelector->Size = System::Drawing::Size(319, 21);
            this->seriesSelector->TabIndex = 2;
            this->seriesSelector->SelectedIndexChanged += gcnew System::EventHandler(this, &SeriesRingersUI::seriesSelector_SelectedIndexChanged);
            // 
            // printBtn
            // 
            this->printBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
            this->printBtn->Location = System::Drawing::Point(558, 299);
            this->printBtn->Name = L"printBtn";
            this->printBtn->Size = System::Drawing::Size(75, 23);
            this->printBtn->TabIndex = 3;
            this->printBtn->Text = L"Print";
            this->printBtn->UseVisualStyleBackColor = true;
            this->printBtn->Click += gcnew System::EventHandler(this, &SeriesRingersUI::printBtn_Click);
            // 
            // SeriesRingersUI
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->CancelButton = this->closeBtn;
            this->ClientSize = System::Drawing::Size(717, 325);
            this->Controls->Add(tableLayoutPanel1);
            this->MinimumSize = System::Drawing::Size(434, 218);
            this->Name = L"SeriesRingersUI";
            this->Text = L"Series ringers";
            this->Load += gcnew System::EventHandler(this, &SeriesRingersUI::SeriesRingersUI_Load);
            tableLayoutPanel1->ResumeLayout(false);
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
            this->ResumeLayout(false);

        }
#pragma endregion
};
}
