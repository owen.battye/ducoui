#pragma once

#include <SearchFieldRingerArgument.h>

namespace Duco
{
    class DatabaseSearch;
}

namespace DucoUI
{
    ref class DatabaseManager;
    public ref class RingerSearchControl : public System::Windows::Forms::UserControl
    {
    public:
        RingerSearchControl(Duco::TSearchFieldRingerArgument* argument, DucoUI::DatabaseManager^ theDatabase);

        void AddSearchControl(Duco::DatabaseSearch& search);
        bool ShouldBeRemoved();

    protected:
        ~RingerSearchControl();
        !RingerSearchControl();

    private:
        System::Windows::Forms::CheckBox^  remove;
        System::Windows::Forms::Label^  ringerString;
        System::Windows::Forms::TextBox^  ringerName;
        System::Windows::Forms::Label^  bell;
        System::ComponentModel::Container ^components;
        Duco::TSearchFieldRingerArgument* argument;

#pragma region Windows Form Designer generated code
        void InitializeComponent()
        {
            System::Windows::Forms::FlowLayoutPanel^  flowLayoutPanel1;
            this->remove = (gcnew System::Windows::Forms::CheckBox());
            this->ringerString = (gcnew System::Windows::Forms::Label());
            this->ringerName = (gcnew System::Windows::Forms::TextBox());
            this->bell = (gcnew System::Windows::Forms::Label());
            flowLayoutPanel1 = (gcnew System::Windows::Forms::FlowLayoutPanel());
            flowLayoutPanel1->SuspendLayout();
            this->SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            flowLayoutPanel1->Controls->Add(this->remove);
            flowLayoutPanel1->Controls->Add(this->ringerString);
            flowLayoutPanel1->Controls->Add(this->ringerName);
            flowLayoutPanel1->Controls->Add(this->bell);
            flowLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            flowLayoutPanel1->Location = System::Drawing::Point(0, 0);
            flowLayoutPanel1->Margin = System::Windows::Forms::Padding(0);
            flowLayoutPanel1->Name = L"flowLayoutPanel1";
            flowLayoutPanel1->Size = System::Drawing::Size(764, 24);
            flowLayoutPanel1->TabIndex = 1;
            // 
            // remove
            // 
            this->remove->AutoSize = true;
            this->remove->Location = System::Drawing::Point(3, 5);
            this->remove->Margin = System::Windows::Forms::Padding(3, 5, 3, 5);
            this->remove->Name = L"remove";
            this->remove->Size = System::Drawing::Size(15, 14);
            this->remove->TabIndex = 0;
            this->remove->UseVisualStyleBackColor = true;
            // 
            // ringerString
            // 
            this->ringerString->AutoSize = true;
            this->ringerString->Location = System::Drawing::Point(24, 5);
            this->ringerString->Margin = System::Windows::Forms::Padding(3, 5, 3, 5);
            this->ringerString->MaximumSize = System::Drawing::Size(300, 13);
            this->ringerString->MinimumSize = System::Drawing::Size(300, 13);
            this->ringerString->Name = L"ringerString";
            this->ringerString->Size = System::Drawing::Size(300, 13);
            this->ringerString->TabIndex = 1;
            this->ringerString->Text = L"label1";
            // 
            // ringerName
            // 
            this->ringerName->Location = System::Drawing::Point(330, 1);
            this->ringerName->Margin = System::Windows::Forms::Padding(3, 1, 3, 1);
            this->ringerName->MaximumSize = System::Drawing::Size(300, 20);
            this->ringerName->MinimumSize = System::Drawing::Size(300, 20);
            this->ringerName->Name = L"ringerName";
            this->ringerName->Size = System::Drawing::Size(300, 20);
            this->ringerName->TabIndex = 2;
            // 
            // bell
            // 
            this->bell->AutoSize = true;
            this->bell->Location = System::Drawing::Point(636, 5);
            this->bell->Margin = System::Windows::Forms::Padding(3, 5, 3, 5);
            this->bell->Name = L"bell";
            this->bell->Size = System::Drawing::Size(35, 13);
            this->bell->TabIndex = 3;
            this->bell->Text = L"label1";
            // 
            // RingerSearchControl
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            this->Controls->Add(flowLayoutPanel1);
            this->Margin = System::Windows::Forms::Padding(3, 2, 3, 1);
            this->Name = L"RingerSearchControl";
            this->Size = System::Drawing::Size(764, 24);
            flowLayoutPanel1->ResumeLayout(false);
            flowLayoutPanel1->PerformLayout();
            this->ResumeLayout(false);
        }
#pragma endregion
    };
}
