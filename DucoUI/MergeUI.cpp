#include "ProgressWrapper.h"
#include "RingingDatabaseObserver.h"
#include <MergeDatabase.h>
#include "DatabaseManager.h"

#include "MergeUI.h"

#include "DucoUtils.h"
#include "SystemDefaultIcon.h"
#include <Peal.h>
#include <PealDatabase.h>
#include <RingingDatabase.h>
#include <DatabaseSettings.h>
#include "SoundUtils.h"
#include <DucoEngineUtils.h>

using namespace Duco;
using namespace DucoUI;
using namespace System::IO;
using namespace System;
using namespace System::Windows::Forms;

MergeUI::MergeUI(DucoUI::DatabaseManager^ theDatabase)
: database(theDatabase), mergeStarted(false)
{
    progressWrapper = new ProgressCallbackWrapper(this);
    pealIdsToImport = new std::set<Duco::ObjectId>();
    externalDatabase = new Duco::MergeDatabase(*progressWrapper, database->Database());
    InitializeComponent();
}

MergeUI::!MergeUI()
{
    delete progressWrapper;
    delete externalDatabase;
    delete pealIdsToImport;
}

MergeUI::~MergeUI()
{
    this->!MergeUI();
    if (components)
    {
        delete components;
    }
}

void
MergeUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    if (eventId != EImportComplete)
    {
        readBtn->Enabled = true;
        mergeBtn->Enabled = false;
        pealIdsToImport->clear();
        dataGridView1->Rows->Clear();
    }
}

void
MergeUI::Initialised()
{
    progressBar->Value = 0;
}

void
MergeUI::Step(int progressPercent)
{
    progressBar->Value = progressPercent;
}

void
MergeUI::Complete()
{
    progressBar->Value = 0;
    readBtn->Enabled = !mergeStarted;
    mergeBtn->Enabled = !mergeStarted;

    defaultRingerLbl->Text = "Found " + DucoUtils::ConvertString(externalDatabase->DefaultRingerExternalName()) + " in other database";
    towerLbl->Text = DucoUtils::ConvertString(externalDatabase->TowerExternalName());
    if (mergeStarted)
    {
        pealIdsToImport->clear();
        dataGridView1->Rows->Clear();
        database->NotifyImportComplete();
    }
    else
    {
        PopulateView();
    }
    if (mergeStarted)
    {
        database->CallObservers(EAdded, TObjectType::EPeal, Duco::ObjectId(), Duco::ObjectId());
    }
    mergeStarted = false;
    progressBar->Value = 0;
}

System::Void
MergeUI::PopulateView()
{
    std::set<Duco::ObjectId>::const_iterator it = externalDatabase->PealIds().begin();
    while (it != externalDatabase->PealIds().end())
    {
        const Peal* const thePeal = externalDatabase->FindPeal(*it);
        if (thePeal != NULL)
        {
            bool asStrapper (false);
            bool defaultRingerFoundInThisPeal (!defaultRinger->Checked || thePeal->ContainsRinger(externalDatabase->DefaultRingerExternalId(), asStrapper));
            if (defaultRingerFoundInThisPeal && database->UniqueObject(*thePeal, TObjectType::EPeal) )
            {
                pealIdsToImport->insert(*it);

                DataGridViewRow^ newRow = gcnew DataGridViewRow();
                newRow->HeaderCell->Value = DucoUtils::ConvertString(*it);
                DataGridViewCellCollection^ theCells = newRow->Cells;

                //ImportCell;
                DataGridViewCheckBoxCell^ importCell = gcnew DataGridViewCheckBoxCell();
                importCell->TrueValue = 1;
                importCell->Value = importCell->TrueValue;
                theCells->Add(importCell);
                //associationCell;
                DataGridViewTextBoxCell^ associationCell = gcnew DataGridViewTextBoxCell();
                associationCell->Value = DucoUtils::ConvertString(externalDatabase->AssociationName(thePeal->AssociationId()));
                theCells->Add(associationCell);
                //dateColumn;
                DataGridViewTextBoxCell^ dateCell = gcnew DataGridViewTextBoxCell();
                dateCell->Value = DucoUtils::ConvertString(thePeal->Date().Str());
                theCells->Add(dateCell);
                //changesColumn;
                DataGridViewTextBoxCell^ changesCell = gcnew DataGridViewTextBoxCell();
                changesCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(thePeal->NoOfChanges(), true));
                theCells->Add(changesCell);
                //methodColumn;
                DataGridViewTextBoxCell^ methodCell = gcnew DataGridViewTextBoxCell();
                methodCell->Value = DucoUtils::ConvertString(externalDatabase->MethodName(thePeal->MethodId()));
                theCells->Add(methodCell);
                //towerColumn;
                DataGridViewTextBoxCell^ towerCell = gcnew DataGridViewTextBoxCell();
                towerCell->Value = DucoUtils::ConvertString(externalDatabase->TowerName(thePeal->TowerId()));
                theCells->Add(towerCell);

                // Add Row
                dataGridView1->Rows->Add(newRow);
            }
        }
        ++it;
    }
}

System::Void
MergeUI::MergeUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();

    if (!OpenFileSelectionDlg())
        asyncCancel = true;

    if (!database->Settings().DefaultRingerSet())
    {
        defaultRinger->Enabled = false;
    }
    else
        defaultRinger->Checked = true;
}

System::Void
MergeUI::MergeUI_Shown(System::Object^  sender, System::EventArgs^  e)
{
    if (asyncCancel)
        Close();
}

System::Void
MergeUI::fileNameEditor_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (OpenFileSelectionDlg())
    {
        mergeBtn->Enabled = false;
        readBtn->Enabled = true;
    }
}

System::Void
MergeUI::readBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    dataGridView1->Rows->Clear();
    pealIdsToImport->clear();
    towerLbl->Text = "";
    defaultRingerLbl->Text = "";
    warningLbl->Text = "";
    if (File::Exists(fileNameEditor->Text))
    {
        std::string fileName;
        DucoUtils::ConvertString(fileNameEditor->Text, fileName);
        std::wstring towerbaseIdStr;
        DucoUtils::ConvertString(towerbaseId->Text, towerbaseIdStr);

        Duco::ObjectId defaultRingerId;
        if (defaultRinger->Checked)
        {
            defaultRingerId = database->Settings().DefaultRinger();
        }
        externalDatabase->ReadDatabase(fileName, defaultRingerId, towerbaseIdStr);
    }
    mergeStarted = false;
    readBtn->Enabled = false;
    mergeBtn->Enabled = true;
    if (!externalDatabase->DefaultRingerExternalId().ValidId() && defaultRinger->Checked)
    {
        SoundUtils::PlayErrorSound();
        warningLbl->Text = "No default ringer found in the external database";
    }
    if (!externalDatabase->TowerExternalId().ValidId() && towerbaseId->Text->Length > 0)
    {
        if (warningLbl->Text->Length > 0)
            warningLbl->Text += "; ";
        else
            SoundUtils::PlayErrorSound();

        warningLbl->Text += "Could not find tower in the external database";
    }
    if (pealIdsToImport->size() == 0)
    {
        warningLbl->Text += "Could not find any new peals to import";
    }
    else if (warningLbl->Text->Length > 0)
    {
        warningLbl->Text += "; All peals will be merged.";
    }
}

System::Void
MergeUI::mergeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (!database->NothingInEditMode())
    {
        SoundUtils::PlayErrorSound();
        return;
    }

    std::set<Duco::ObjectId> pealIds;
    System::Collections::IEnumerator^ myEnum = safe_cast<System::Collections::IEnumerable^>(dataGridView1->Rows)->GetEnumerator();
    std::set<Duco::ObjectId>::const_iterator it2 = pealIdsToImport->begin();
    while (myEnum->MoveNext())
    {
        DataGridViewRow^ currentRow = safe_cast<DataGridViewRow^>(myEnum->Current);
        DataGridViewCheckBoxCell^ checkedCell = static_cast<DataGridViewCheckBoxCell^>(currentRow->Cells[0]);
        if (checkedCell->Value == checkedCell->TrueValue)
        {
            pealIds.insert(*it2);
        }
        ++it2;
    }

    defaultRinger->Enabled = false;
    mergeBtn->Enabled = false;
    readBtn->Enabled = false;
    mergeStarted = true;
    externalDatabase->Merge(pealIds);
}

System::Void
MergeUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

bool
MergeUI::OpenFileSelectionDlg()
{
    OpenFileDialog^ fileDialog = gcnew OpenFileDialog ();
    fileDialog->Filter = "Duco databases (*.duc)|*.duc" ;
    fileDialog->RestoreDirectory = true;

    switch (fileDialog->ShowDialog())
    {
    case System::Windows::Forms::DialogResult::OK:
        {
            try
            {
                String^ fileName = fileDialog->FileName;
                fileDialog->OpenFile()->Close();
                fileNameEditor->Text = fileName;
                defaultRingerLbl->Text = "";
                readBtn->Enabled = true;
            }
            catch (Exception^ ex)
            {
                SoundUtils::PlayErrorSound();
                MessageBox::Show(this, "Error: Could not open file: " + ex->Message);
                return false;
            }
        }
    default:
        dataGridView1->Rows->Clear();
        pealIdsToImport->clear();
        return true;

    case System::Windows::Forms::DialogResult::Cancel:
        return false;
    }
}

System::Void
MergeUI::defaultRinger_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    readBtn->Enabled = true;
    mergeBtn->Enabled = false;
    dataGridView1->Rows->Clear();
    pealIdsToImport->clear();
}

System::Void
MergeUI::dataGridView1_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    try
    {
        //unsigned int rowIndex = e->RowIndex;
        //DataGridViewRow^ currentRow = dataGridView1->Rows[rowIndex];
        //unsigned int pealId = Convert::ToInt16(currentRow->HeaderCell->Value);
        //PealUI::ShowPeal(externalDatabase, settings, this->MdiParent, pealId);
    }
    catch (Exception^)
    {
        SoundUtils::PlayErrorSound();
    }

}

void 
MergeUI::InitializeComponent()
{
    this->components = (gcnew System::ComponentModel::Container());
    System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
    System::Windows::Forms::Label^ towerBaseLbl;
    System::Windows::Forms::StatusStrip^ statusStrip1;
    System::Windows::Forms::Button^ closeBtn;
    this->warningLbl = (gcnew System::Windows::Forms::Label());
    this->towerbaseId = (gcnew System::Windows::Forms::TextBox());
    this->towerLbl = (gcnew System::Windows::Forms::Label());
    this->defaultRingerLbl = (gcnew System::Windows::Forms::Label());
    this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
    this->importColumn = (gcnew System::Windows::Forms::DataGridViewCheckBoxColumn());
    this->associationColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->dateColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->changesColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->methodColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->towerColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->progressBar = (gcnew System::Windows::Forms::ToolStripProgressBar());
    this->fileNameEditor = (gcnew System::Windows::Forms::TextBox());
    this->defaultRinger = (gcnew System::Windows::Forms::CheckBox());
    this->mergeBtn = (gcnew System::Windows::Forms::Button());
    this->readBtn = (gcnew System::Windows::Forms::Button());
    this->toolTip1 = (gcnew System::Windows::Forms::ToolTip(this->components));
    tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
    towerBaseLbl = (gcnew System::Windows::Forms::Label());
    statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
    closeBtn = (gcnew System::Windows::Forms::Button());
    tableLayoutPanel1->SuspendLayout();
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
    statusStrip1->SuspendLayout();
    this->SuspendLayout();
    // 
    // tableLayoutPanel1
    // 
    tableLayoutPanel1->ColumnCount = 6;
    tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
    tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
    tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
    tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
    tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
    tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
    tableLayoutPanel1->Controls->Add(this->towerLbl, 2, 2);
    tableLayoutPanel1->Controls->Add(this->warningLbl, 0, 3);
    tableLayoutPanel1->Controls->Add(this->defaultRingerLbl, 2, 1);
    tableLayoutPanel1->Controls->Add(towerBaseLbl, 0, 2);
    tableLayoutPanel1->Controls->Add(this->dataGridView1, 0, 4);
    tableLayoutPanel1->Controls->Add(statusStrip1, 0, 5);
    tableLayoutPanel1->Controls->Add(this->fileNameEditor, 0, 0);
    tableLayoutPanel1->Controls->Add(this->defaultRinger, 0, 1);
    tableLayoutPanel1->Controls->Add(closeBtn, 5, 3);
    tableLayoutPanel1->Controls->Add(this->mergeBtn, 4, 3);
    tableLayoutPanel1->Controls->Add(this->readBtn, 3, 3);
    tableLayoutPanel1->Controls->Add(this->towerbaseId, 1, 2);
    tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
    tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
    tableLayoutPanel1->Name = L"tableLayoutPanel1";
    tableLayoutPanel1->RowCount = 6;
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
    tableLayoutPanel1->Size = System::Drawing::Size(892, 578);
    tableLayoutPanel1->TabIndex = 0;
    // 
    // warningLbl
    // 
    this->warningLbl->AutoSize = true;
    tableLayoutPanel1->SetColumnSpan(this->warningLbl, 3);
    this->warningLbl->Dock = System::Windows::Forms::DockStyle::Fill;
    this->warningLbl->Location = System::Drawing::Point(3, 78);
    this->warningLbl->Margin = System::Windows::Forms::Padding(3);
    this->warningLbl->Name = L"warningLbl";
    this->warningLbl->Size = System::Drawing::Size(643, 23);
    this->warningLbl->TabIndex = 9;
    // 
    // towerbaseId
    // 
    this->towerbaseId->Location = System::Drawing::Point(84, 52);
    this->towerbaseId->Name = L"towerbaseId";
    this->towerbaseId->Size = System::Drawing::Size(100, 20);
    this->towerbaseId->TabIndex = 3;
    this->toolTip1->SetToolTip(this->towerbaseId, L"Only import performances, where the tower matches the towerbase id");
    // 
    // towerLbl
    // 
    this->towerLbl->AutoSize = true;
    tableLayoutPanel1->SetColumnSpan(this->towerLbl, 4);
    this->towerLbl->Dock = System::Windows::Forms::DockStyle::Fill;
    this->towerLbl->Location = System::Drawing::Point(209, 52);
    this->towerLbl->Margin = System::Windows::Forms::Padding(3);
    this->towerLbl->Name = L"towerLbl";
    this->towerLbl->Size = System::Drawing::Size(680, 20);
    this->towerLbl->TabIndex = 8;
    this->towerLbl->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
    // 
    // defaultRingerLbl
    // 
    this->defaultRingerLbl->AutoSize = true;
    tableLayoutPanel1->SetColumnSpan(this->defaultRingerLbl, 4);
    this->defaultRingerLbl->Dock = System::Windows::Forms::DockStyle::Fill;
    this->defaultRingerLbl->Location = System::Drawing::Point(209, 29);
    this->defaultRingerLbl->Margin = System::Windows::Forms::Padding(3);
    this->defaultRingerLbl->Name = L"defaultRingerLbl";
    this->defaultRingerLbl->Size = System::Drawing::Size(680, 17);
    this->defaultRingerLbl->TabIndex = 5;
    this->defaultRingerLbl->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
    // 
    // towerBaseLbl
    // 
    towerBaseLbl->AutoSize = true;
    towerBaseLbl->Dock = System::Windows::Forms::DockStyle::Fill;
    towerBaseLbl->Location = System::Drawing::Point(3, 52);
    towerBaseLbl->Margin = System::Windows::Forms::Padding(3);
    towerBaseLbl->Name = L"towerBaseLbl";
    towerBaseLbl->Size = System::Drawing::Size(75, 20);
    towerBaseLbl->TabIndex = 6;
    towerBaseLbl->Text = L"Towerbase Id:";
    towerBaseLbl->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
    // 
    // dataGridView1
    // 
    this->dataGridView1->AllowUserToAddRows = false;
    this->dataGridView1->AllowUserToDeleteRows = false;
    this->dataGridView1->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::DisplayedCells;
    this->dataGridView1->AutoSizeRowsMode = System::Windows::Forms::DataGridViewAutoSizeRowsMode::DisplayedCells;
    this->dataGridView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(6) {
        this->importColumn,
            this->associationColumn, this->dateColumn, this->changesColumn, this->methodColumn, this->towerColumn
    });
    tableLayoutPanel1->SetColumnSpan(this->dataGridView1, 6);
    this->dataGridView1->Dock = System::Windows::Forms::DockStyle::Fill;
    this->dataGridView1->Location = System::Drawing::Point(3, 107);
    this->dataGridView1->Name = L"dataGridView1";
    this->dataGridView1->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToDisplayedHeaders;
    this->dataGridView1->Size = System::Drawing::Size(886, 448);
    this->dataGridView1->TabIndex = 1;
    this->dataGridView1->CellContentDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &MergeUI::dataGridView1_CellContentDoubleClick);
    // 
    // importColumn
    // 
    this->importColumn->Frozen = true;
    this->importColumn->HeaderText = L"Import";
    this->importColumn->Name = L"importColumn";
    this->importColumn->Width = 42;
    // 
    // associationColumn
    // 
    this->associationColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
    this->associationColumn->Frozen = true;
    this->associationColumn->HeaderText = L"Association";
    this->associationColumn->Name = L"associationColumn";
    this->associationColumn->ReadOnly = true;
    this->associationColumn->Width = 86;
    // 
    // dateColumn
    // 
    this->dateColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
    this->dateColumn->HeaderText = L"Date";
    this->dateColumn->Name = L"dateColumn";
    this->dateColumn->ReadOnly = true;
    this->dateColumn->Width = 55;
    // 
    // changesColumn
    // 
    this->changesColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::ColumnHeader;
    this->changesColumn->HeaderText = L"Changes";
    this->changesColumn->Name = L"changesColumn";
    this->changesColumn->ReadOnly = true;
    this->changesColumn->Width = 74;
    // 
    // methodColumn
    // 
    this->methodColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
    this->methodColumn->HeaderText = L"Method";
    this->methodColumn->Name = L"methodColumn";
    this->methodColumn->ReadOnly = true;
    this->methodColumn->Width = 68;
    // 
    // towerColumn
    // 
    this->towerColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
    this->towerColumn->HeaderText = L"Tower";
    this->towerColumn->Name = L"towerColumn";
    this->towerColumn->ReadOnly = true;
    this->towerColumn->Width = 62;
    // 
    // statusStrip1
    // 
    tableLayoutPanel1->SetColumnSpan(statusStrip1, 6);
    statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->progressBar });
    statusStrip1->Location = System::Drawing::Point(0, 558);
    statusStrip1->Name = L"statusStrip1";
    statusStrip1->Size = System::Drawing::Size(892, 20);
    statusStrip1->TabIndex = 2;
    statusStrip1->Text = L"statusStrip1";
    // 
    // progressBar
    // 
    this->progressBar->Name = L"progressBar";
    this->progressBar->Size = System::Drawing::Size(100, 14);
    // 
    // fileNameEditor
    // 
    tableLayoutPanel1->SetColumnSpan(this->fileNameEditor, 6);
    this->fileNameEditor->Dock = System::Windows::Forms::DockStyle::Fill;
    this->fileNameEditor->Location = System::Drawing::Point(3, 3);
    this->fileNameEditor->Name = L"fileNameEditor";
    this->fileNameEditor->ReadOnly = true;
    this->fileNameEditor->Size = System::Drawing::Size(886, 20);
    this->fileNameEditor->TabIndex = 1;
    this->fileNameEditor->Click += gcnew System::EventHandler(this, &MergeUI::fileNameEditor_Click);
    // 
    // defaultRinger
    // 
    this->defaultRinger->AutoSize = true;
    tableLayoutPanel1->SetColumnSpan(this->defaultRinger, 2);
    this->defaultRinger->Location = System::Drawing::Point(3, 29);
    this->defaultRinger->Name = L"defaultRinger";
    this->defaultRinger->Size = System::Drawing::Size(200, 17);
    this->defaultRinger->TabIndex = 2;
    this->defaultRinger->Text = L"Only performances with default ringer";
    this->toolTip1->SetToolTip(this->defaultRinger, L"Try to find a ringer in the other database, which matches the default ringer in t"
        L"his database");
    this->defaultRinger->UseVisualStyleBackColor = true;
    this->defaultRinger->CheckedChanged += gcnew System::EventHandler(this, &MergeUI::defaultRinger_CheckedChanged);
    // 
    // closeBtn
    // 
    closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
    closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
    closeBtn->Location = System::Drawing::Point(814, 78);
    closeBtn->Name = L"closeBtn";
    closeBtn->Size = System::Drawing::Size(75, 23);
    closeBtn->TabIndex = 6;
    closeBtn->Text = L"Close";
    closeBtn->UseVisualStyleBackColor = true;
    closeBtn->Click += gcnew System::EventHandler(this, &MergeUI::closeBtn_Click);
    // 
    // mergeBtn
    // 
    this->mergeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
    this->mergeBtn->Enabled = false;
    this->mergeBtn->Location = System::Drawing::Point(733, 78);
    this->mergeBtn->Name = L"mergeBtn";
    this->mergeBtn->Size = System::Drawing::Size(75, 23);
    this->mergeBtn->TabIndex = 5;
    this->mergeBtn->Text = L"Merge";
    this->mergeBtn->UseVisualStyleBackColor = true;
    this->mergeBtn->Click += gcnew System::EventHandler(this, &MergeUI::mergeBtn_Click);
    // 
    // readBtn
    // 
    this->readBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
    this->readBtn->Location = System::Drawing::Point(652, 78);
    this->readBtn->Name = L"readBtn";
    this->readBtn->Size = System::Drawing::Size(75, 23);
    this->readBtn->TabIndex = 4;
    this->readBtn->Text = L"Read";
    this->readBtn->UseVisualStyleBackColor = true;
    this->readBtn->Click += gcnew System::EventHandler(this, &MergeUI::readBtn_Click);
    // 
    // MergeUI
    // 
    this->AcceptButton = this->mergeBtn;
    this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
    this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
    this->CancelButton = closeBtn;
    this->ClientSize = System::Drawing::Size(892, 578);
    this->Controls->Add(tableLayoutPanel1);
    this->MinimumSize = System::Drawing::Size(594, 296);
    this->Name = L"MergeUI";
    this->Text = L"Merge database";
    this->Load += gcnew System::EventHandler(this, &MergeUI::MergeUI_Load);
    this->Shown += gcnew System::EventHandler(this, &MergeUI::MergeUI_Shown);
    tableLayoutPanel1->ResumeLayout(false);
    tableLayoutPanel1->PerformLayout();
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
    statusStrip1->ResumeLayout(false);
    statusStrip1->PerformLayout();
    this->ResumeLayout(false);

}
