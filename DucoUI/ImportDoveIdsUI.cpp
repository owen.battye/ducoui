#include "ProgressWrapper.h"
#include <DoveDatabase.h>
#include "DatabaseManager.h"

#include "ImportDoveIdsUI.h"
#include "DucoUtils.h"
#include "SoundUtils.h"
#include <DoveDatabase.h>
#include <DoveObject.h>
#include <Tower.h>
#include <TowerDatabase.h>
#include <RingingDatabase.h>
#include "WindowsSettings.h"
#include "SystemDefaultIcon.h"
#include "RingingDatabaseObserver.h"
#include "DucoWindowState.h"
#include "OpenObjectCallback.h"
#include "PictureListControl.h"
#include "PictureControl.h"
#include "TowerUI.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Windows::Forms;

ImportDoveIdsUI::ImportDoveIdsUI(DucoUI::DatabaseManager^ theDatabase)
    : database(theDatabase), doveDatabase(NULL)
{
    callback = new ProgressCallbackWrapper(this);
    InitializeComponent();
    towerAlreadyUsedCellStyle = (gcnew System::Windows::Forms::DataGridViewCellStyle(dataGridView->DefaultCellStyle));
    towerAlreadyUsedCellStyle->ForeColor = System::Drawing::SystemColors::GrayText;

}

ImportDoveIdsUI::!ImportDoveIdsUI()
{
    delete callback;
    delete doveDatabase;
}

ImportDoveIdsUI::~ImportDoveIdsUI()
{
    this->!ImportDoveIdsUI();
    if (components)
    {
        delete components;
    }
}

System::Void
ImportDoveIdsUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (backgroundWorker1->IsBusy)
    {
        backgroundWorker1->CancelAsync();
    }
    else
    {
        Close();
    }
}

System::Void
ImportDoveIdsUI::readFileBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    dataGridView->Rows->Clear();
    StartGeneration(false);
}

System::Void
ImportDoveIdsUI::importbtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (backgroundWorker1->IsBusy || dataGridView->Rows->Count <= 0)
    {
        SoundUtils::PlayErrorSound();
    }
    else
    {
        System::Boolean checkedTowerFound = false;
        System::Collections::IEnumerator^ it = static_cast<System::Collections::IEnumerable^>(dataGridView->Rows)->GetEnumerator();
        while (it->MoveNext() && !checkedTowerFound)
        {
            DataGridViewCheckBoxCell^ checkBoxCell = static_cast<DataGridViewCheckBoxCell^>(static_cast<DataGridViewRow^>(it->Current)->Cells[0]);
            System::Object^ value = checkBoxCell->Value;
            if (Convert::ToBoolean(checkBoxCell->Value))
            {
                checkedTowerFound = true;
            }
        }

        if (checkedTowerFound)
        {
            statusLbl->Text = "";
            importbtn->Enabled = false;
            selectAllBtn->Enabled = false;
            readFileBtn->Enabled = false;
            openBtn->Enabled = false;
            StartGeneration(true);
        }
        else
        {
            statusLbl->Text = "No towers selected";
        }
    }
}

System::Void
ImportDoveIdsUI::StartGeneration(System::Boolean importTowers)
{
    statusLbl->Text = "";
    numberOfTowersFound = 0;
    numberOfTowersProcessable = 0;
    importingTowers = importTowers;

    if (System::IO::File::Exists(database->DoveDataFile()))
    {
        database->SetDoveDataFile(openFileDialog1->FileName);
        DoveImportStartUiArgs^ args = gcnew DoveImportStartUiArgs();
        args->doveFile = database->DoveDataFile();
        args->openDoveFile = !importTowers;
        args->importDoveIdAndPosition = importTowers;

        openBtn->Enabled = false;
        selectAllBtn->Enabled = false;
        readFileBtn->Enabled = false;
        importbtn->Enabled = false;
        backgroundWorker1->RunWorkerAsync(args);
    }
}

System::Void
ImportDoveIdsUI::openBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (backgroundWorker1->IsBusy)
    {
        SoundUtils::PlayErrorSound();
    }
    else
    {
        if (openFileDialog1->ShowDialog() == ::System::Windows::Forms::DialogResult::OK)
        {
            dataGridView->Rows->Clear();
            if (doveDatabase != NULL)
            {
                doveDatabase->Clear();
            }
            database->SetDoveDataFile(openFileDialog1->FileName);

            StartGeneration(false);
        }
    }
}

System::Void
ImportDoveIdsUI::backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
    bool cancelled = false;
    BackgroundWorker^ worker = dynamic_cast<BackgroundWorker^>(sender);
    DoveImportStartUiArgs^ startArgs = static_cast<DoveImportStartUiArgs^>(e->Argument);
    if (startArgs->openDoveFile)
    {
        std::string filename;
        DucoUtils::ConvertString(startArgs->doveFile, filename);
        if (doveDatabase != NULL)
        {
            delete doveDatabase;
        }
        doveDatabase = new DoveDatabase(filename, callback, database->WindowsSettings()->InstallationDir(), true);
        doveDatabase->Import();

        std::set<Duco::ObjectId> towersWithoutDoveId;
        if (database->Database().TowersDatabase().TowersWithoutDoveId(towersWithoutDoveId))
        {
            float total = (float)towersWithoutDoveId.size();
            float count = 0;
            std::set<Duco::ObjectId>::const_iterator it = towersWithoutDoveId.begin();
            while (it != towersWithoutDoveId.end() && !cancelled)
            {
                ++count;
                Duco::Tower theTower;
                if (database->FindTower(*it, theTower, false))
                {
                    DataGridViewRow^ newRow = gcnew DataGridViewRow();
                    DataGridViewCellCollection^ theCells = newRow->Cells;
                    //checkbox Column;
                    DataGridViewCheckBoxCell^ checkboxCell = gcnew DataGridViewCheckBoxCell();
                    checkboxCell->ReadOnly = false;
                    theCells->Add(checkboxCell);
                    //id Column;
                    DataGridViewTextBoxCell^ ducoIdCell = gcnew DataGridViewTextBoxCell();
                    ducoIdCell->Value = DucoUtils::ConvertString(theTower.Id().Str());
                    theCells->Add(ducoIdCell);
                    //tower name Column;
                    DataGridViewTextBoxCell^ ducoTowerCell = gcnew DataGridViewTextBoxCell();
                    ducoTowerCell->Value = DucoUtils::ConvertString(theTower.FullName());
                    theCells->Add(ducoTowerCell);

                    Duco::DoveObject doveTower;
                    Duco::ObjectId existingTowerId;
                    if (doveDatabase->SuggestTower(database->Database().TowersDatabase(), theTower, doveTower, existingTowerId))
                    {
                        //dove id Column;
                        DataGridViewTextBoxCell^ doveIdCell = gcnew DataGridViewTextBoxCell();
                        doveIdCell->Value = DucoUtils::ConvertString(doveTower.DoveId());
                        theCells->Add(doveIdCell);
                        //dove tower name Column;
                        DataGridViewTextBoxCell^ doveNameCell = gcnew DataGridViewTextBoxCell();
                        doveNameCell->Value = DucoUtils::ConvertString(doveTower.FullName());
                        theCells->Add(doveNameCell);
                        if (existingTowerId.ValidId())
                        {
                            // Existing tower Column;
                            DataGridViewTextBoxCell^ existingIdCell = gcnew DataGridViewTextBoxCell();
                            existingIdCell->Value = DucoUtils::ConvertString(existingTowerId.Str());
                            theCells->Add(existingIdCell);
                            checkboxCell->ReadOnly = true;
                            newRow->DefaultCellStyle = towerAlreadyUsedCellStyle;
                        }
                    }
                    else
                    {
                        checkboxCell->ReadOnly = true;
                    }

                    backgroundWorker1->ReportProgress((int)(100 * (count / total)), newRow);
                }
                if (worker->CancellationPending)
                {
                    e->Cancel = true;
                    cancelled = true;
                    break;
                }
                ++it;
            }
        }
    }
    else if (startArgs->importDoveIdAndPosition)
    {
        std::map<std::wstring, Duco::ObjectId> objectsToImport;
        System::Collections::IEnumerator^ it = static_cast<System::Collections::IEnumerable^>(dataGridView->Rows)->GetEnumerator();
        while (it->MoveNext())
        {
            DataGridViewCheckBoxCell^ checkBoxCell = static_cast<DataGridViewCheckBoxCell^>(static_cast<DataGridViewRow^>(it->Current)->Cells[0]);
            System::Object^ value = checkBoxCell->Value;

            if (Convert::ToBoolean(checkBoxCell->Value))
            {
                Duco::ObjectId towerId = Convert::ToInt32(static_cast<DataGridViewRow^>(it->Current)->Cells[1]->Value);
                System::String^ doveId = Convert::ToString(static_cast<DataGridViewRow^>(it->Current)->Cells[3]->Value);
                std::wstring doveIdStr;
                DucoUtils::ConvertString(doveId, doveIdStr);
                std::pair<std::wstring, Duco::ObjectId> newObject(doveIdStr, towerId);
                objectsToImport.insert(newObject);
            }
        }

        doveDatabase->ImportTowerIdAndPosition(database->Database().TowersDatabase(), objectsToImport);
    }
}

System::Void
ImportDoveIdsUI::backgroundWorker1_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    progressBar->Value = e->ProgressPercentage;
    if (e->ProgressPercentage < 100)
    {
        closeBtn->Text = "Cancel";
    }
    else
    {
        closeBtn->Text = "Close";
    }
    if (e->UserState != nullptr)
    {
        DataGridViewRow^ newResultRow = static_cast<DataGridViewRow^>(e->UserState);
        if (newResultRow != nullptr)
        {
            ++numberOfTowersFound;
            dataGridView->Rows->Add(newResultRow);
            DataGridViewCheckBoxCell^ checkBoxCell = static_cast<DataGridViewCheckBoxCell^>(static_cast<DataGridViewRow^>(newResultRow)->Cells[0]);
            if (!checkBoxCell->ReadOnly)
            {
                ++numberOfTowersProcessable;
            }
        }
        statusLbl->Text = "Found " + Convert::ToString(numberOfTowersFound) + " towers, " + Convert::ToString(numberOfTowersProcessable) + " can be imported";
    }
}

System::Void
ImportDoveIdsUI::backgroundWorker1_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    progressBar->Value = 0;
    if (e == nullptr || e->Cancelled)
    {
        dataGridView->Rows->Clear();
        if (doveDatabase != NULL)
        {
            doveDatabase->Clear();
        }
    }
    else if (importingTowers)
    {
        dataGridView->Rows->Clear();
        importingTowers = false;
        database->CallObservers(TEventType::EUpdated, TObjectType::ETower, KNoId, KNoId);
    }
    selectAllBtn->Enabled = true;
    readFileBtn->Enabled = true;
    openBtn->Enabled = true;
    importbtn->Enabled = true;
}

void
ImportDoveIdsUI::Initialised()
{
    backgroundWorker1->ReportProgress(0);
}

void
ImportDoveIdsUI::Step(int progressPercent)
{
    backgroundWorker1->ReportProgress(progressPercent);
}

void
ImportDoveIdsUI::Complete()
{

}

System::Void
ImportDoveIdsUI::dataGridView_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    try
    {
        unsigned int rowIndex = e->RowIndex;
        DataGridViewRow^ currentRow = dataGridView->Rows[rowIndex];
        if (e->ColumnIndex >= 1 && e->ColumnIndex <= 2)
        {
            if (currentRow->Cells[1]->Value != nullptr)
            {
                unsigned int towerId = Convert::ToInt16(currentRow->Cells[1]->Value);
                if (towerId > 0)
                {
                    TowerUI::ShowTower(database, this->MdiParent, towerId, -1);
                }
            }
        }
        else if (e->ColumnIndex == 5)
        {
            if (currentRow->Cells[1]->Value != nullptr)
            {
                unsigned int towerId = Convert::ToInt16(currentRow->Cells[5]->Value);
                if (towerId > 0)
                {
                    TowerUI::ShowTower(database, this->MdiParent, towerId, -1);
                }
            }
        }
    }
    catch (Exception^)
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
ImportDoveIdsUI::ImportDoveIdsUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e)
{
    if (backgroundWorker1->IsBusy)
    {
        backgroundWorker1->CancelAsync();
        e->Cancel = true;
        SoundUtils::PlayErrorSound();
    }
}

System::Void
ImportDoveIdsUI::ImportDoveIdsUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    doveFilenameLbl->Text = "";
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    try
    {
        this->openFileDialog1->InitialDirectory = System::IO::Path::GetDirectoryName(database->DoveDataFile());
        this->openFileDialog1->FileName = System::IO::Path::GetFileName(database->DoveDataFile());
    }
    catch (System::Exception^)
    {
    }
    if (System::IO::File::Exists(database->DoveDataFile()))
    {
        openFileDialog1->InitialDirectory = System::IO::Path::GetDirectoryName(database->DoveDataFile());
        openFileDialog1->FileName = database->DoveDataFile();
        doveFilenameLbl->Text = database->DoveDataFile();
        StartGeneration(false);
    }
    else
    {
        doveFilenameLbl->Text = "Choose dove data file";
    }
}

System::Void
ImportDoveIdsUI::selectAllBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    statusLbl->Text = "";
    System::Collections::IEnumerator^ it = static_cast<System::Collections::IEnumerable^>(dataGridView->Rows)->GetEnumerator();
    while (it->MoveNext())
    {
        DataGridViewCheckBoxCell^ checkBoxCell = static_cast<DataGridViewCheckBoxCell^>(static_cast<DataGridViewRow^>(it->Current)->Cells[0]);
        if (!checkBoxCell->ReadOnly)
        {
            checkBoxCell->Value = true;
        }
    }
}
