#pragma once

namespace DucoUI
{
    public ref class CheckLatestVersion :  public System::ComponentModel::Component
    {
    public:
        CheckLatestVersion(System::Windows::Forms::Form^ newOwner, DucoUI::DatabaseManager^ theDatabase, System::Boolean userForced);
        CheckLatestVersion(System::ComponentModel::IContainer ^container, System::Windows::Forms::Form^ newOwner, DucoUI::DatabaseManager^ theDatabase);

        System::Boolean ShowingUpgradeDialog();

    protected:
        ~CheckLatestVersion();
        System::Void downloadedVersion(System::ComponentModel::AsyncCompletedEventArgs^  e);
        System::Void downloadedInstaller(System::ComponentModel::AsyncCompletedEventArgs^  e);
        System::Void client_DownloadFileCompleted(System::Object^  sender, System::ComponentModel::AsyncCompletedEventArgs^  e);

    private:
        System::ComponentModel::Container ^components;
        System::Net::WebClient^ client;
        System::Windows::Forms::Form^ owner;
        DucoUI::DatabaseManager^ database;
        System::String^ downloadedFilePath;
        System::String^ versionTemporaryfile;
        System::Boolean showingUpgradeDialog;
        System::Boolean downloadingVersion;
        System::Boolean downloadingInstaller;
        System::Boolean userForced;

#pragma region Windows Form Designer generated code
        void InitializeComponent()
        {
            System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(CheckLatestVersion::typeid));
            this->client = (gcnew System::Net::WebClient());
            // 
            // client
            // 
            this->client->AllowReadStreamBuffering = false;
            this->client->AllowWriteStreamBuffering = false;
            this->client->BaseAddress = L"";
            this->client->CachePolicy = nullptr;
            this->client->Credentials = nullptr;
            this->client->Encoding = (cli::safe_cast<System::Text::Encoding^>(resources->GetObject(L"client.Encoding")));
            this->client->Headers = (cli::safe_cast<System::Net::WebHeaderCollection^>(resources->GetObject(L"client.Headers")));
            this->client->QueryString = (cli::safe_cast<System::Collections::Specialized::NameValueCollection^>(resources->GetObject(L"client.QueryString")));
            this->client->UseDefaultCredentials = false;
            this->client->DownloadFileCompleted += gcnew System::ComponentModel::AsyncCompletedEventHandler(this, &CheckLatestVersion::client_DownloadFileCompleted);
        }
#pragma endregion
    };
}
