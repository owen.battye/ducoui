#include "ImportSingleMethod.h"
#include "DatabaseManager.h"
#include "DucoUtils.h"
#include "DatabaseGenUtils.h"
#include "RingingDatabase.h"
#include "MethodDatabase.h"
#include <MethodNotationDatabaseSingleImporter.h>
#include <Method.h>
#include <ExternalMethod.h>
#include "SystemDefaultIcon.h"
#include <DucoEngineUtils.h>

using namespace System;
using namespace System::Windows::Forms;
using namespace System::Collections;
using namespace System::ComponentModel;
using namespace DucoUI;
using namespace Duco;

namespace DucoUI
{
    ref class ImportSingleMethodUIArgs
    {
    public:
        unsigned int stage;
        String^ methodDatabaseFilename;
        String^ filterText;
    };
}

ImportSingleMethod::ImportSingleMethod(DucoUI::DatabaseManager^ theDatabase, Duco::ObjectId& methodId, unsigned int order, System::String^ currentMethodName, System::Boolean importSingle)
:   database(theDatabase), methodCollectionDatabase(NULL), restart(false), selectedMethodId(methodId), importSingleMethod(importSingle)
{
    InitializeComponent();
    progressWrapper = new ProgressCallbackWrapper(this);
    databaseFilename = database->XmlMethodCollectionFile();
    if (currentMethodName->Length > 0)
    {
        if (currentMethodName->IndexOf(' ') > 0)
            filter->Text = currentMethodName->Substring(0, currentMethodName->IndexOf(" "));
        else
            filter->Text = currentMethodName;
    }
    DatabaseGenUtils::GenerateStageOptions(stageSelector, database, true);
    std::wstring orderName;
    if (order != -1 && database->Database().MethodsDatabase().FindOrderName(order, orderName))
    {
        stageSelector->Text = DucoUtils::ConvertString(orderName);
    }
}

ImportSingleMethod::!ImportSingleMethod()
{
    delete progressWrapper;
}

ImportSingleMethod::~ImportSingleMethod()
{
    this->!ImportSingleMethod();
    if (components)
    {
        delete components;
    }
    delete methodCollectionDatabase;
}

System::Void
ImportSingleMethod::openFileBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (openFileDialog1->ShowDialog(Parent) == ::DialogResult::OK)
    {
        databaseFilename = openFileDialog1->FileName;
        database->SetXmlMethodCollectionFile(databaseFilename);
        Restart();
    }
}

System::Void
ImportSingleMethod::importBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (!backgroundWorker->IsBusy)
    {
        unsigned int noOfImportedMethods (0);
        if (methodDisplay->SelectedRows->Count >= 1)
        {
            IEnumerator^ it = methodDisplay->SelectedRows->GetEnumerator();
            while (it->MoveNext())
            {
                if (ImportMethod((System::Windows::Forms::DataGridViewRow^)it->Current))
                {
                    ++noOfImportedMethods;
                }
            }
        }
        else if (methodDisplay->SelectedCells->Count >= 1)
        {
            std::set<int> processedRows;
            IEnumerator^ it = methodDisplay->SelectedCells->GetEnumerator();
            while (it->MoveNext())
            {
                int rowIndex = static_cast<DataGridViewTextBoxCell^>(it->Current)->RowIndex;
                if (rowIndex != -1 && processedRows.insert(rowIndex).second)
                {
                    DataGridViewRow^ theRow = methodDisplay->Rows[rowIndex];
                    if (ImportMethod(theRow))
                    {
                        if (importSingleMethod)
                            Close();
                        else
                            ++noOfImportedMethods;
                        
                    }
                }
            }
        }
        if (noOfImportedMethods == 1)
            progressLbl->Text = "1 method imported";            
        else
            progressLbl->Text = Convert::ToString(noOfImportedMethods) + " methods imported";
    }
}

bool
ImportSingleMethod::ImportMethod(DataGridViewRow^ theRow)
{
    if (theRow != nullptr)
    {
        Duco::ObjectId methodId = Convert::ToInt16(theRow->Cells[0]->Value);
        const ExternalMethod* const methodToAdd = methodCollectionDatabase->FindMethod(methodId);
        if (methodToAdd != NULL)
        {
            Duco::Method existingMethod;
            bool importMethod (true);
            if (database->FindMethod(methodToAdd->Stage(), methodToAdd->Notation(), existingMethod))
            {
                if (MessageBox::Show(MdiParent, "A method with similar notation already exists: " + DucoUtils::ConvertString(existingMethod.FullName(database->Database())) + " already exists.\nDo you still want to import " + DucoUtils::ConvertString(methodToAdd->Title()) + "?", "Warning potential duplicate method", MessageBoxButtons::YesNo, MessageBoxIcon::Question, MessageBoxDefaultButton::Button1) == ::DialogResult::No)
                {
                    importMethod = false;
                }
            }

            if (importMethod)
            {
                Method newMethod (-1, methodToAdd->Stage(), methodToAdd->Name(), methodToAdd->Type(), methodToAdd->Notation());
                methodId = database->AddMethod(newMethod);
                if (methodId.ValidId())
                {
                    selectedMethodId = methodId;
                    methodDisplay->Rows->Clear();
                    return true;
                }
            }
        }
    }
    return false;
}

System::Void
ImportSingleMethod::stageSelector_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    Restart();
}

System::Void
ImportSingleMethod::ImportSingleMethod_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    importBtn->Enabled = false;
    try
    {
        this->openFileDialog1->InitialDirectory = System::IO::Path::GetDirectoryName(database->XmlMethodCollectionFile());
        this->openFileDialog1->FileName = System::IO::Path::GetFileName(database->XmlMethodCollectionFile());
    }
    catch (System::Exception^)
    {
    }
}

System::Void
ImportSingleMethod::methodDisplay_SelectionChanged(System::Object^  sender, System::EventArgs^  e)
{
    importBtn->Enabled = methodDisplay->SelectedCells->Count >= 1 || methodDisplay->SelectedRows->Count >= 1;
}

System::Void
ImportSingleMethod::ImportSingleMethod_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e)
{
    if (backgroundWorker->IsBusy)
    {
        StopProcessing();
        e->Cancel = true;
    }
}

System::Void
ImportSingleMethod::filter_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    Restart();
}

System::Void
ImportSingleMethod::AddMethod(const Duco::ExternalMethod& methodToAdd, double count, double noOfResults)
{
    DataGridViewRow^ newRow = gcnew DataGridViewRow();
    //Id Column;
    DataGridViewTextBoxCell^ idCell = gcnew DataGridViewTextBoxCell();
    idCell ->Value = DucoUtils::ConvertString(methodToAdd.Id());
    newRow->Cells->Add(idCell);
    //Name Column;
    DataGridViewTextBoxCell^ nameCell = gcnew DataGridViewTextBoxCell();
    nameCell->Value = DucoUtils::ConvertString(methodToAdd.Title());
    newRow->Cells->Add(nameCell);
    //Notation Column;
    DataGridViewTextBoxCell^ notationCell = gcnew DataGridViewTextBoxCell();
    notationCell->Value = DucoUtils::ConvertString(methodToAdd.Notation());
    newRow->Cells->Add(notationCell);

    double percent (count / noOfResults);
    percent *= 100;
    backgroundWorker->ReportProgress(int(percent), newRow);
}

System::Void
ImportSingleMethod::Restart()
{
    if (backgroundWorker->IsBusy)
    {
        StopProcessing();
        restart = true;
    }
    else if (databaseFilename->Length > 0)
    {
        bool dualOrder (false);
        unsigned int stage (-1);
        database->FindOrderNumber(stageSelector->Text, stage, dualOrder);
        if (filter->Text->Length > 0 || stage != -1)
        {
            methodDisplay->Rows->Clear();
            methodDisplay->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::EnableResizing;
            ImportSingleMethodUIArgs^ args = gcnew ImportSingleMethodUIArgs;
            args->stage = stage;
            args->methodDatabaseFilename = databaseFilename;
            args->filterText = filter->Text;

            this->Cursor = Cursors::WaitCursor;
            backgroundWorker->RunWorkerAsync(args);
        }
    }
}

System::Void
ImportSingleMethod::backgroundWorker_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
    BackgroundWorker^ worker = dynamic_cast<BackgroundWorker^>(sender);
    progressLbl->Text = "";

    ImportSingleMethodUIArgs^ args = (ImportSingleMethodUIArgs^)e->Argument;
    std::string databaseFileNameStr;
    DucoUtils::ConvertString(args->methodDatabaseFilename, databaseFileNameStr);

    delete methodCollectionDatabase;
    methodCollectionDatabase = new MethodNotationDatabaseSingleImporter(database->Database().MethodsDatabase(), databaseFileNameStr, progressWrapper);
    methodCollectionDatabase->ReadMethods();
    methodDisplay->Rows->Clear();

    if (methodCollectionDatabase != NULL && !worker->CancellationPending)
    {
        std::wstring filterText;
        DucoUtils::ConvertString(args->filterText, filterText);

        double count (0);
        if (args->stage != -1)
        {
            std::pair<std::multimap<unsigned int,Duco::ExternalMethod*>::const_iterator,std::multimap<unsigned int,Duco::ExternalMethod*>::const_iterator> matchingMethods = methodCollectionDatabase->FindMethods(args->stage);
            while (matchingMethods.first != matchingMethods.second && !worker->CancellationPending)
            {
                if (filterText.length() == 0 || DucoEngineUtils::FindSubstring(filterText, matchingMethods.first->second->Name()))
                {
                    AddMethod(*matchingMethods.first->second, count, methodCollectionDatabase->MethodCount());
                }
                backgroundWorker->ReportProgress(int((count / methodCollectionDatabase->MethodCount())*(double)100));
                ++count;
                ++matchingMethods.first;
            }
        }
        else
        {
            std::multimap<unsigned int,Duco::ExternalMethod*>::const_iterator allMethods = methodCollectionDatabase->Begin();
            while (allMethods != methodCollectionDatabase->End() && !worker->CancellationPending)
            {
                if (filterText.length() == 0 || DucoEngineUtils::FindSubstring(filterText, allMethods->second->Name()))
                {
                    AddMethod(*allMethods->second, count, methodCollectionDatabase->MethodCount());
                }
                backgroundWorker->ReportProgress(int((count / methodCollectionDatabase->MethodCount())*(double)100));
                ++count;
                ++allMethods;
            }
        }
    }
}

System::Void
ImportSingleMethod::backgroundWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    if (e->ProgressPercentage <= 100)
    {
        progressBar->Value = e->ProgressPercentage;
    }
    else
    {
        progressBar->Value = 100;
    }
    if (e->UserState != nullptr)
    {
        methodDisplay->Rows->Add((DataGridViewRow^)e->UserState);
    }
}

System::Void
ImportSingleMethod::backgroundWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    progressBar->Value = 0;
    if (restart)
    {
        restart = false;
        Restart();
    }
    else
    {
        methodDisplay->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
        this->Cursor = Cursors::Arrow;
    }
}

void
ImportSingleMethod::Initialised()
{
    backgroundWorker->ReportProgress(0);
}

void
ImportSingleMethod::Step(int progressPercent)
{
    backgroundWorker->ReportProgress(progressPercent);
}

void
ImportSingleMethod::Complete()
{
    backgroundWorker->ReportProgress(0);
}

void
ImportSingleMethod::StopProcessing()
{
    if (methodCollectionDatabase != NULL)
    {
        methodCollectionDatabase->StopProcessing();
    }
    backgroundWorker->CancelAsync();
}
