#include "ProgressWrapper.h"
#include "GenericTablePrintUtil.h"
#include <DatabaseSearch.h>
#include "DatabaseManager.h"

#include "TowerSearchUI.h"

#include "DatabaseGenUtils.h"
#include "DucoUtils.h"
#include <DatabaseSearch.h>
#include "SearchUtils.h"
#include "SoundUtils.h"
#include <SearchGroup.h>
#include "SystemDefaultIcon.h"
#include <Tower.h>
#include "DucoUIUtils.h"
#include <RingingDatabase.h>
#include <PealDatabase.h>
#include "SearchAndReplaceUI.h"
#include <SearchFieldNumberArgument.h>
#include <SearchFieldStringArgument.h>
#include <SearchFieldBoolArgument.h>
#include <SearchFieldIdArgument.h>
#include <SearchValidObject.h>
#include <DucoConfiguration.h>
#include <SearchFieldLinkedTowerIdArgument.h>
#include "RingingDatabaseObserver.h"
#include "DucoWindowState.h"
#include "OpenObjectCallback.h"
#include "PictureListControl.h"
#include "PictureControl.h"
#include "TowerUI.h"
#include "SingleObjectStatsUI.h"
#include <DatabaseSettings.h>
#include <StatisticFilters.h>


using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::Windows::Forms;

TowerSearchUI::TowerSearchUI(DatabaseManager^ newDatabase, bool setSearchInvalid)
:   database(newDatabase), startSearchInvalid(setSearchInvalid)
{
    progressWrapper = new ProgressCallbackWrapper(this);
    foundTowerIds = gcnew System::Collections::Generic::List<System::UInt64>;
    allTowerIds = gcnew System::Collections::Generic::List<System::Int16>();
    InitializeComponent();
    removedTowerStyle = (gcnew System::Windows::Forms::DataGridViewCellStyle(towersData->DefaultCellStyle));
    removedTowerStyle->ForeColor = System::Drawing::SystemColors::GrayText;
}

void
TowerSearchUI::ShowTowers(System::Windows::Forms::Form^ parent, System::Collections::Generic::List<System::UInt64>^ newTowerIds)
{
    if (!backgroundWorker->IsBusy)
    {
        startSearchInvalid = false;
        foundTowerIds = newTowerIds;
        this->MdiParent = parent;
        this->Show();
        backgroundWorker->RunWorkerAsync();
    }
}

TowerSearchUI::~TowerSearchUI()
{
    this->!TowerSearchUI();
    if (components)
    {
        delete components;
    }
}

TowerSearchUI::!TowerSearchUI()
{
    delete progressWrapper;
}

System::Void
TowerSearchUI::TowerSearchUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    DatabaseGenUtils::GetAllPlaceOptions(countyEditor, cityEditor, nameEditor, database);
    DatabaseGenUtils::GenerateTowerOptions(towerSelector, allTowerIds, database, -1, false, false, true);
    DatabaseGenUtils::GenerateTowerOptions(linkedTowerSelector, allTowerIds, database, -1, false, false, true);
    if (startSearchInvalid)
    {
        invalid->Checked = true;
        searchBtn_Click(sender, e);
    }
}

System::Void
TowerSearchUI::ClosingEvent( Object^ /*sender*/, System::ComponentModel::CancelEventArgs^ e )
{
    if (backgroundWorker->IsBusy)
    {
        backgroundWorker->CancelAsync();
        e->Cancel = true;
    }
}

System::Void 
TowerSearchUI::searchBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (backgroundWorker->IsBusy)
        return;

    searchBtn->Enabled = false;
    foundTowerIds->Clear();
    towersData->Rows->Clear();
    results->Text = "";
    DatabaseSearch* search = new DatabaseSearch(database->Database());
    if (CreateSearchParameters(*search))
    {
        SearchUIArgs^ args = gcnew SearchUIArgs();
        args->search = search;
        backgroundWorker->RunWorkerAsync(args);
    }
    else
    {
        SoundUtils::PlayErrorSound();
        delete search;
        searchBtn->Enabled = true;
    }
}

bool
TowerSearchUI::CreateSearchParameters(Duco::DatabaseSearch& search)
{
    try
    {
        unsigned int noOfBells = 0;
        if (DucoUtils::ConvertTextToNumber(bellsEditor->Text, noOfBells))
        {
            TSearchType searchtype (EEqualTo);
            if (bellsMoreThan->Checked)
                searchtype = EMoreThan;
            else if (bellsLessThan->Checked)
                searchtype = ELessThan;
            TSearchFieldNumberArgument* newNumber = new TSearchFieldNumberArgument(Duco::ENoOfBells, noOfBells, searchtype);
            search.AddArgument(newNumber);
        }
        if (nameOrCity->Checked)
        {
            if (nameEditor->Text->Length > 0)
            {
                std::wstring tempStr;
                DucoUtils::ConvertString(nameEditor->Text, tempStr);

                Duco::TSearchGroup* group = new Duco::TSearchGroup(Duco::EOrGroup);
                search.AddArgument(group);

                Duco::TSearchFieldStringArgument* newStr = new Duco::TSearchFieldStringArgument(ETowerName, tempStr, true);
                group->AddArgument(newStr);
                Duco::TSearchFieldStringArgument* altNewStr = new Duco::TSearchFieldStringArgument(ECity, tempStr, true);
                group->AddArgument(altNewStr);
                if (nameIncludesAka->Checked)
                {
                    Duco::TSearchFieldStringArgument* altNewStr = new Duco::TSearchFieldStringArgument(ETowerAka, tempStr, true);
                    group->AddArgument(altNewStr);
                }
            }
        }
        else
        {
            if (nameIncludesAka->Checked)
                SearchUtils::AddStringSearchArgument(search, nameEditor->Text, ETowerName, ETowerAka, false);
            else
                SearchUtils::AddStringSearchArgument(search, nameEditor->Text, ETowerName, false);
            if (cityIncludesAka->Checked)
                SearchUtils::AddStringSearchArgument(search, cityEditor->Text, ECity, ETowerAka, false);
            else
                SearchUtils::AddStringSearchArgument(search, cityEditor->Text, ECity, false);
        }
        if (countyIncludesAka->Checked)
            SearchUtils::AddStringSearchArgument(search, countyEditor->Text, ECountyField, ETowerAka, false);
        else
            SearchUtils::AddStringSearchArgument(search, countyEditor->Text, ECountyField, false);
        SearchUtils::AddStringSearchArgument(search, ringNameEditor->Text, ERingName, false);
        SearchUtils::AddStringSearchArgument(search, tenorWeight->Text, ETenorWeight, false);
        SearchUtils::AddStringSearchArgument(search, tenorKey->Text, ETenorKey, false);
        SearchUtils::AddStringSearchArgument(search, notes->Text, ENotes, false);
        if (towerBells->Checked)
        {
            TSearchFieldBoolArgument* newArg = new TSearchFieldBoolArgument(EHandbellTower, false);
            search.AddArgument(newArg);
        }
        else if (handbells->Checked)
        {
            TSearchFieldBoolArgument* newArg = new TSearchFieldBoolArgument(EHandbellTower, true);
            search.AddArgument(newArg);
        }

        if (blankDoveRef->Checked)
        {
            TSearchFieldBoolArgument* newStr = new TSearchFieldBoolArgument(EDoveReference);
            search.AddArgument(newStr);
        }
        else
        {
            SearchUtils::AddStringSearchArgument(search, doveRef->Text, EDoveReference, false);
        }
        if (blankTowerbaseReference->Checked)
        {
            TSearchFieldBoolArgument* newStr = new TSearchFieldBoolArgument(ETowerbaseId);
            search.AddArgument(newStr);
        }
        else
        {
            SearchUtils::AddStringSearchArgument(search, towerbaseReference->Text, ETowerbaseId, false);
        }
        if (removed->Checked)
        {
            TSearchFieldBoolArgument* newRemovedField = new TSearchFieldBoolArgument(Duco::ERemovedTower, removed->Checked);
            search.AddArgument(newRemovedField);
        }
        if (antiClockwise->Checked)
        {
            TSearchFieldBoolArgument* newAntiClockwiseField = new TSearchFieldBoolArgument(Duco::EAntiClockwise, antiClockwise->Checked);
            search.AddArgument(newAntiClockwiseField);
        }
        if (multipleRings->Checked)
        {
            TSearchFieldBoolArgument* multiRingsField = new TSearchFieldBoolArgument(Duco::EMultipleRings, multipleRings->Checked);
            search.AddArgument(multiRingsField);
        }
        if (specialBells->Checked)
        {
            TSearchFieldBoolArgument* extraBellsField = new TSearchFieldBoolArgument(Duco::EExtraBells, specialBells->Checked);
            search.AddArgument(extraBellsField);
        }
        if (towerSelector->SelectedIndex != -1)
        {
            TSearchFieldBoolArgument* distanceFromField = new TSearchFieldBoolArgument(Duco::ETowerContainsValidLocation, true);
            search.AddArgument(distanceFromField);
        }
        if (invalidPosition->Checked)
        {
            TSearchFieldBoolArgument* newPositionState = new TSearchFieldBoolArgument(ETowerContainsValidLocation, false);
            search.AddArgument(newPositionState);
        }
        if (linkedTowerSelector->SelectedIndex != -1)
        {
            Duco::ObjectId linkedId = DatabaseGenUtils::FindId(allTowerIds, linkedTowerSelector->SelectedIndex);
            if (linkedId.ValidId())
            {
                if (immediateLinkedTowersOnly->Checked)
                {
                    TSearchFieldIdArgument* linkedTowerId = new TSearchFieldIdArgument(Duco::ELinkedTowerId, linkedId);
                    search.AddArgument(linkedTowerId);
                }
                else
                {
                    TSearchFieldLinkedTowerIdArgument* linkedTowerId = new TSearchFieldLinkedTowerIdArgument(linkedId);
                    search.AddArgument(linkedTowerId);
                }
            }
        }
        if (linkedTowersOnly->Checked)
        {
            TSearchFieldBoolArgument* linkedTowerField = new TSearchFieldBoolArgument(Duco::EWithLinkedTower, linkedTowersOnly->Checked);
            search.AddArgument(linkedTowerField);
        }
        if (hasPicture->Checked)
        {
            TSearchFieldBoolArgument* newPictureState = new TSearchFieldBoolArgument(EContainsPicture);
            search.AddArgument(newPictureState);
        }
        else if (doesNotHavePicture->Checked)
        {
            TSearchFieldBoolArgument* newPictureState = new TSearchFieldBoolArgument(EContainsPicture, false);
            search.AddArgument(newPictureState);
        }
        if (invalid->Checked)
        {
            TSearchValidObject* newInvalidField = new TSearchValidObject(database->Config().IncludeWarningsInValidation());
            search.AddValidationArgument(newInvalidField);
        }
    }
    catch (Exception^)
    {
        return false;
    }
    return true;
}

System::Void 
TowerSearchUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void 
TowerSearchUI::clearBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (backgroundWorker->IsBusy)
        return;

    towersData->RowCount = 0;
    towersData->Rows->Clear();
    ringNameEditor->Text = "";
    bellsEditor->Text = "";
    countyEditor->Text = "";
    cityEditor->Text = "";
    nameEditor->Text = "";
    tenorWeight->Text = "";
    tenorKey->Text = "";
    notes->Text = "";
    doveRef->Text = "";
    towerbaseReference->Text = "";
    towerSelector->Text = "";
    towerSelector->SelectedIndex = -1;
    blankDoveRef->Checked = false;
    blankTowerbaseReference->Checked = false;
    bellsEqualTo->Checked = true;
    results->Text = "";
    removed->Checked = false;
    invalid->Checked = false;
    multipleRings->Checked = false;
    nameIncludesAka->Checked = false;
    cityIncludesAka->Checked = false;
    countyIncludesAka->Checked = false;
    linkedTowerSelector->Text = "";
    linkedTowerSelector->SelectedIndex = -1;
    linkedTowersOnly->Checked = false;
    immediateLinkedTowersOnly->Checked = true;
    withAndWithoutPictures->Checked = true;
    allVenueTypes->Checked = true;
    antiClockwise->Checked = false;
    invalidPosition->Checked = false;
}

System::Void
TowerSearchUI::backgroundWorker_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
    if (e->Argument != nullptr)
    {
        SearchUIArgs^ args = static_cast<SearchUIArgs^>(e->Argument);
        System::ComponentModel::BackgroundWorker^ bgworker = static_cast<System::ComponentModel::BackgroundWorker^>(sender);
        std::set<ObjectId> ids;
        args->search->Search(*progressWrapper, ids, TObjectType::ETower);

        if (towerSelector->SelectedIndex != -1)
        {
            Duco::ObjectId distanceTowerId = (DatabaseGenUtils::FindId(allTowerIds, towerSelector->SelectedIndex));
            if (distanceTowerId.ValidId())
            {
                DucoUtils::Convert(args->search->Sort(ids, TObjectType::ETower, distanceTowerId), foundTowerIds);
            }
        }
        else
        {
            DucoUtils::Convert(ids, foundTowerIds);
        }
        delete args->search;
    }
}

System::Void
TowerSearchUI::backgroundWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    progressBar->Value = e->ProgressPercentage;
}

System::Void
TowerSearchUI::backgroundWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    searchBtn->Enabled = true;
    towersData->RowCount = foundTowerIds->Count;
    progressBar->Value = 0;
    results->Text = Convert::ToString(foundTowerIds->Count);
    if (foundTowerIds->Count == 0)
    {
        SoundUtils::PlayErrorSound();
    }
    else
    {
        SoundUtils::PlayFinishedSound();
    }
}

void
TowerSearchUI::Initialised()
{
    if (backgroundWorker->IsBusy)
        backgroundWorker->ReportProgress(0);
    else
        progressBar->Value = 0;
}

void
TowerSearchUI::Step(int progressPercent)
{
    if (backgroundWorker->IsBusy)
        backgroundWorker->ReportProgress(progressPercent);
    else
        progressBar->Value = progressPercent;
}

void
TowerSearchUI::Complete()
{
    if (!backgroundWorker->IsBusy)
        progressBar->Value = 0;
}

System::Void
TowerSearchUI::TowerSearchUI_Scroll(System::Object^  sender, System::Windows::Forms::ScrollEventArgs^  e)
{
    towersData_Scroll(sender, gcnew ScrollEventArgs(ScrollEventType::EndScroll, 0));
}

System::Void
TowerSearchUI::towersData_Scroll(System::Object^  sender, System::Windows::Forms::ScrollEventArgs^  e)
{
    towersData->AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode::None;

    for each (DataGridViewColumn^ column in towersData->Columns)
    {
        int newColumnWidth = column->GetPreferredWidth(DataGridViewAutoSizeColumnMode::DisplayedCells, true);
        int currentColumnWidth = column->Width;
        if (newColumnWidth > currentColumnWidth)
        {
            column->AutoSizeMode = DataGridViewAutoSizeColumnMode::None;
            column->Width = newColumnWidth;
            _ASSERT(column->Width == newColumnWidth);
        }
    }
}

System::Void
TowerSearchUI::towersData_CellValueNeeded(System::Object^  sender, System::Windows::Forms::DataGridViewCellValueEventArgs^  e)
{
    if (e->RowIndex >= foundTowerIds->Count)
        return;

    Duco::ObjectId towerIdToFind((*foundTowerIds)[e->RowIndex]);
    Duco::Tower foundTower;
    if (database->FindTower(towerIdToFind, foundTower, true))
    {
        PerformanceDate firstPealDate;
        PerformanceDate lastPealDate;
        Duco::ObjectId firstPealId;
        Duco::ObjectId lastPealId;
        // Check if hidden cells need displaying
        if (!distanceColumn->Visible && DatabaseGenUtils::FindId(allTowerIds, towerSelector->SelectedIndex).ValidId())
        {
            distanceColumn->Visible = true;
        }
        if (!errorColumn->Visible && foundTower.ErrorString(database->Database().Settings(), database->Config().IncludeWarningsInValidation()).length() > 0)
        {
            errorColumn->Visible = true;
        }
        switch (e->ColumnIndex)
        {
        default:
            break;

        case 0: // Name column;
        {
            DataGridViewRow^ currentRow = towersData->Rows[e->RowIndex];
            currentRow->HeaderCell->Value = Convert::ToString(towerIdToFind.Id());
            e->Value = DucoUtils::ConvertString(foundTower.Name());
        }
        break;
        case 1: //City Column;
            e->Value = DucoUtils::ConvertString(foundTower.City());
            break;

        case 2: //County Column;
            e->Value = DucoUtils::ConvertString(foundTower.County());
            break;
        case 3: //Handbell Column;
            if (foundTower.Handbell())
            {
                e->Value = "H";
            }
            else
            {
                e->Value = "T";
            }
            break;

        case 4: //Bells Column;
            e->Value = Convert::ToString(foundTower.Bells());
            break;
        case 5: //Tenor Column;
            e->Value = DucoUtils::ConvertString(foundTower.HeaviestTenor());
            break;
        case 6: //Tenor key Column;
            e->Value = DucoUtils::ConvertString(foundTower.TowerKey());
            break;
        case 7: //Dove Reference Column;
            e->Value = DucoUtils::ConvertString(foundTower.DoveRef());
            break;
        case 8: //First peal
        {
            Duco::StatisticFilters filters(database->Database());
            filters.SetTower(true, foundTower.Id());
            filters.SetIncludeLinkedTowers(false);

            PealLengthInfo info = database->Database().PealsDatabase().AllMatches(filters);
            if (info.TotalPeals() > 0)
            {
                e->Value = DucoUtils::ConvertString(info.DateOfFirstPealString());
            }
        }
        break;
        case 9: //Last peal
        {
            Duco::StatisticFilters filters(database->Database());
            filters.SetTower(true, foundTower.Id());
            filters.SetIncludeLinkedTowers(false);

            PealLengthInfo info = database->Database().PealsDatabase().AllMatches(filters);
            if (info.TotalPeals() > 1)
            {
                e->Value = DucoUtils::ConvertString(info.DateOfLastPealString());
            }
        }
        break;
        case 10: // Distance cell
        {
            Duco::ObjectId distanceTowerId = (DatabaseGenUtils::FindId(allTowerIds, towerSelector->SelectedIndex));
            if (foundTower.Id() != distanceTowerId && distanceTowerId.ValidId())
            {
                Duco::Tower distanceTower;
                if (database->FindTower(distanceTowerId, distanceTower, false))
                {
                    double distanceFrom(foundTower.DistanceFrom(distanceTower));
                    e->Value = distanceFrom.ToString("F3") + " km";
                }
            }
        }
        break;
        case 11: //Error Column;
        {
            e->Value = DucoUtils::ConvertString(foundTower.ErrorString(database->Database().Settings(), database->Config().IncludeWarningsInValidation()));
        }
        break;
        }
        if (foundTower.Removed())
        {
            DataGridViewRow^ currentRow = towersData->Rows[e->RowIndex];
            currentRow->DefaultCellStyle = removedTowerStyle;
        }
    }
}

void
TowerSearchUI::BellsEditor_KeyDown( Object^ sender, System::Windows::Forms::KeyEventArgs^ e )
{
    // Initialise the flag to false.
    nonNumberEntered = !DucoUIUtils::NumericKey(e, Control::ModifierKeys);
}

void
TowerSearchUI::BellsEditor_KeyPressed(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e)
{
    // Check for the flag being set in the KeyDown event.
    if ( nonNumberEntered == true )
    {
    // Stop the character from being entered into the control since it is non-numerical.
        e->Handled = true;
    }
}

System::Void 
TowerSearchUI::towersData_CellDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    try
    {
        unsigned int rowIndex = e->RowIndex;
        DataGridViewRow^ currentRow = towersData->Rows[rowIndex];
        unsigned int towerId = Convert::ToInt16(currentRow->HeaderCell->Value);
        TowerUI::ShowTower(database, this->MdiParent, towerId, -1);
    }
    catch (Exception^)
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
TowerSearchUI::blankDoveRef_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    doveRef->Enabled = !blankDoveRef->Checked;
    if (blankDoveRef->Checked)
        doveRef->Text = "";
}

System::Void
TowerSearchUI::blankPealBaseOrFelsteadReference_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
{
    towerbaseReference->Enabled = !blankTowerbaseReference->Checked;
    if (blankTowerbaseReference->Checked)
        towerbaseReference->Text = "";
}


System::Void
TowerSearchUI::updateBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (!backgroundWorker->IsBusy)
    {
        std::set<ObjectId> ids;
        DucoUtils::Convert(foundTowerIds, ids);
        SearchAndReplaceUI^ uiForm = gcnew SearchAndReplaceUI(database, progressWrapper, ids, TObjectType::ETower);
        uiForm->ShowDialog();
    }
}

System::Void
TowerSearchUI::printBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (!backgroundWorker->IsBusy)
    {
        DucoUIUtils::Print(this, sender, gcnew System::Drawing::Printing::PrintPageEventHandler( this, &TowerSearchUI::printTowers ), "towers summary", database->Database().Settings().UsePrintPreview(), false);
    }
}

System::Void
TowerSearchUI::printTowers(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args)
{
    if (!backgroundWorker->IsBusy)
    {
        if (printUtil == nullptr)
        {
            printUtil = gcnew GenericTablePrintUtil(database, args, "Results of tower search");
            printUtil->SetObjects(towersData, false);
        }
    
        System::Drawing::Printing::PrintDocument^ printDoc = static_cast<System::Drawing::Printing::PrintDocument^>(sender);
        printUtil->printObject(args, printDoc->PrinterSettings);
        if (!args->HasMorePages)
        {
            printUtil = nullptr;
        }
    }
}

System::Void
TowerSearchUI::copyTowersCmd_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (towersData->GetCellCount(DataGridViewElementStates::Selected) > 0)
    {
        // Add the selection to the clipboard.
        Clipboard::SetDataObject(towersData->GetClipboardContent());
    }
}

System::Void
TowerSearchUI::statisticBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    SingleObjectStatsUI^ statsUI = gcnew SingleObjectStatsUI(database, foundTowerIds, TObjectType::ETower);
    statsUI->MdiParent = this->MdiParent;
    statsUI->Show();
}

System::Void
TowerSearchUI::nameOrCity_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
{
    cityEditor->Enabled = !nameOrCity->Checked;
    cityIncludesAka->Enabled = !nameOrCity->Checked;
}
