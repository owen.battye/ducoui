#pragma once
#include <RenumberProgressCallback.h>
#include <vcclr.h>

namespace DucoUI
{
interface class RenumberProgressCallbackUI
{
public:
    virtual void RenumberInitialised(Duco::RenumberProgressCallback::TRenumberStage newStage) = 0;
    virtual void RenumberStep(size_t objectId, size_t total) = 0;
    virtual void RenumberComplete() = 0;
};

class RenumberProgressWrapper : public Duco::RenumberProgressCallback
{
public:
    RenumberProgressWrapper(DucoUI::RenumberProgressCallbackUI^ theCallback);

    virtual void RenumberInitialised(RenumberProgressCallback::TRenumberStage newStage);
    virtual void RenumberStep(size_t objectId, size_t total);
    virtual void RenumberComplete();

protected:
    gcroot<RenumberProgressCallbackUI^> uiCallback;
};

} // end namespace DucoUI

