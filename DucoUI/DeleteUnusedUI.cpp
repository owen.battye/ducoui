#include "DatabaseManager.h"
#include "DeleteUnusedUI.h"
#include "DucoUtils.h"
#include "DatabaseManager.h"
#include <DatabaseSettings.h>
#include <RingingDatabase.h>
#include <Method.h>
#include <MethodSeries.h>
#include <Tower.h>
#include <Ring.h>
#include <Ringer.h>
#include <Association.h>
#include <Composition.h>
#include "SystemDefaultIcon.h"

using namespace std;
using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::Windows::Forms;

DeleteUnusedUI::DeleteUnusedUI(DatabaseManager^ theDatabase, bool& newChangesMade)
:   database(theDatabase), changesMade(newChangesMade)
{
    changesMade = false;
    InitializeComponent();
    unusedMethods = new set<Duco::ObjectId>();
    unusedRingers = new set<Duco::ObjectId>();
    unusedTowers = new set<Duco::ObjectId>();
    unusedMethodSeries = new set<Duco::ObjectId>();
    unusedCompositions = new set<Duco::ObjectId>();
    unusedAssociations = new set<Duco::ObjectId>();
    unusedRings = new multimap<Duco::ObjectId, Duco::ObjectId>();
}

DeleteUnusedUI::~DeleteUnusedUI()
{
    this->!DeleteUnusedUI();
    if (components)
    {
        delete components;
    }
}

DeleteUnusedUI::!DeleteUnusedUI()
{
    delete unusedMethods;
    delete unusedRingers;
    delete unusedTowers;
    delete unusedRings;
    delete unusedMethodSeries;
    delete unusedCompositions;
}

System::Void
DeleteUnusedUI::findBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Find();
}

System::Void
DeleteUnusedUI::Find()
{
    ringerList->Items->Clear();
    towerList->Items->Clear();
    methodList->Items->Clear();
    ringsList->Items->Clear();
    seriesList->Items->Clear();
    compositionsList->Items->Clear();
    associationsList->Items->Clear();
    deleteBtn->Enabled = false;

    unusedMethods->clear();
    unusedRingers->clear();
    unusedTowers->clear();
    unusedRings->clear();
    unusedMethodSeries->clear();
    unusedCompositions->clear();
    unusedAssociations->clear();
    database->Database().FindUnused(*unusedMethods, *unusedRingers, *unusedTowers, *unusedRings, *unusedMethodSeries, *unusedCompositions, *unusedAssociations);

    ringersEditor->Text = Convert::ToString(unusedRingers->size());
    towersEditor->Text = Convert::ToString(unusedTowers->size());
    methodsEditor->Text = Convert::ToString(unusedMethods->size());
    ringsEditor->Text = Convert::ToString(unusedRings->size());
    seriesEditor->Text = Convert::ToString(unusedMethodSeries->size());
    compositionsEditor->Text = Convert::ToString(unusedCompositions->size());
    associationsEditor->Text = Convert::ToString(unusedAssociations->size());

    set<Duco::ObjectId>::const_iterator itM = unusedMethods->begin();
    while (itM != unusedMethods->end())
    {
        Duco::Method method;
        if (database->FindMethod(*itM, method, false))
        {
            methodList->Items->Add(DucoUtils::ConvertString(method.FullName(database->Database())));
        }
        ++itM;
    }
    set<Duco::ObjectId>::const_iterator itR = unusedRingers->begin();
    while (itR != unusedRingers->end())
    {
        Duco::Ringer theRinger;
        if (database->FindRinger(*itR, theRinger, false))
        {
            ringerList->Items->Add(DucoUtils::ConvertString(theRinger.FullName(database->Settings().LastNameFirst())));
        }
        ++itR;
    }
    set<Duco::ObjectId>::const_iterator itT = unusedTowers->begin();
    while (itT != unusedTowers->end())
    {
        Duco::Tower tower;
        if (database->FindTower(*itT, tower, false))
        {
            towerList->Items->Add(DucoUtils::ConvertString(tower.FullName()));
        }
        ++itT;
    }
    multimap<Duco::ObjectId, Duco::ObjectId>::const_iterator itRings = unusedRings->begin();
    while (itRings != unusedRings->end())
    {
        Duco::Tower tower;
        if (database->FindTower(itRings->first, tower, false))
        {
            const Ring* const ring = tower.FindRing(itRings->second);
            if (ring != NULL)
            {
                ringsList->Items->Add(DucoUtils::ConvertString(ring->Name() + L", " + tower.FullName()));
            }
        }
        ++itRings;
    }
    set<Duco::ObjectId>::const_iterator itC = unusedCompositions->begin();
    while (itC != unusedCompositions->end())
    {
        Duco::Composition theComposition(-1, database->Database().MethodsDatabase());
        if (database->FindComposition(*itC, theComposition, false))
        {
            String^ compositionName = DucoUtils::ConvertString(theComposition.Name());
            Ringer composer;
            if (database->FindRinger(theComposition.ComposerId(), composer, false) && compositionName->Length > 0)
            {
                compositionName += " by ";
                compositionName += DucoUtils::ConvertString(composer.FullName(database->Settings().LastNameFirst()));
            }
            compositionsList->Items->Add(compositionName);
        }
        ++itC;
    }
    set<Duco::ObjectId>::const_iterator itS = unusedMethodSeries->begin();
    while (itS != unusedMethodSeries->end())
    {
        Duco::MethodSeries methodSeries;
        if (database->FindMethodSeries(*itS, methodSeries, false))
        {
            seriesList->Items->Add(DucoUtils::ConvertString(methodSeries.Name()));
        }
        ++itS;
    }
    set<Duco::ObjectId>::const_iterator itAss = unusedAssociations->begin();
    while (itAss != unusedAssociations->end())
    {
        Duco::Association theAssociation;
        if (database->FindAssociation(*itAss, theAssociation, false))
        {
            String^ compositionName = DucoUtils::ConvertString(theAssociation.Name());
            associationsList->Items->Add(compositionName);
        }
        ++itAss;
    }
}

System::Void 
DeleteUnusedUI::EnableDeleteButton(bool checked)
{
    if (checked || towerList->CheckedItems->Count > 0 || 
        methodList->CheckedItems->Count > 0 || 
        ringerList->CheckedItems->Count > 0 ||
        ringsList->CheckedItems->Count > 0 ||
        seriesList->CheckedItems->Count > 0 ||
        compositionsList->CheckedItems->Count > 0 ||
        associationsList->CheckedItems->Count > 0)
    {
        deleteBtn->Enabled = true;
    }
    else
    {
        deleteBtn->Enabled = false;
    }
}

System::Void
DeleteUnusedUI::list_ItemCheck(System::Object^  sender, System::Windows::Forms::ItemCheckEventArgs^  e)
{
    EnableDeleteButton(e->NewValue==CheckState::Checked);
}

System::Void 
DeleteUnusedUI::Delete_Click(System::Object^  sender, System::EventArgs^  e)
{
    renumberInProgress = true;
    deleteBtn->Enabled = false;
    findBtn->Enabled = false;
    closeBtn->Enabled = false;
    selectAllBtn->Enabled = false;
    int count = 0;
    {
        CheckedListBox::CheckedIndexCollection^ selectedMethodsList = methodList->CheckedIndices;
        set<Duco::ObjectId>::const_iterator itM = unusedMethods->begin();
        while ( itM != unusedMethods->end() )
        {
            if (selectedMethodsList->Contains(count))
            {
                if (database->StartEdit(TObjectType::EMethod, *itM))
                {
                    if (database->DeleteMethod(*itM, true))
                        changesMade = true;
                }
            }
            ++count;
            ++itM;
        }    
    }
    {
        count = 0;
        CheckedListBox::CheckedIndexCollection^ selectedRingersList = ringerList->CheckedIndices;
        set<Duco::ObjectId>::const_iterator itR = unusedRingers->begin();
        while ( itR != unusedRingers->end() )
        {
            if (selectedRingersList->Contains(count))
            {
                if (database->StartEdit(TObjectType::ERinger, *itR))
                {
                    if (database->DeleteRinger(*itR, true))
                        changesMade = true;
                }
            }
            ++count;
            ++itR;
        }    
    }
    {
        count = 0;
        CheckedListBox::CheckedIndexCollection^ selectedTowersList = towerList->CheckedIndices;
        set<Duco::ObjectId>::const_iterator itT = unusedTowers->begin();
        while ( itT != unusedTowers->end() )
        {
            if (selectedTowersList->Contains(count))
            {
                if (database->StartEdit(TObjectType::ETower, *itT))
                {
                    if (database->DeleteTower(*itT, true))
                        changesMade = true;
                }
            }
            ++count;
            ++itT;
        }    
    }
    {
        count = 0;
        CheckedListBox::CheckedIndexCollection^ selectedRingsList = ringsList->CheckedIndices;
        multimap<Duco::ObjectId, Duco::ObjectId>::const_iterator itRings = unusedRings->begin();
        while ( itRings != unusedRings->end() )
        {
            if (selectedRingsList->Contains(count))
            {
                if (database->StartEdit(TObjectType::ETower, itRings->first))
                {
                    if (database->DeleteRing(itRings->first, itRings->second, true))
                    {
                        changesMade = true;
                    }
                    else
                    {
                        database->CancelEdit(TObjectType::ETower, itRings->first);
                    }
                }
            }
            ++count;
            ++itRings;
        }    
    }
    {
        count = 0;
        CheckedListBox::CheckedIndexCollection^ selectedCompositionsList = compositionsList->CheckedIndices;
        set<Duco::ObjectId>::const_iterator itCompositions = unusedCompositions->begin();
        while ( itCompositions != unusedCompositions->end() )
        {
            if (selectedCompositionsList->Contains(count))
            {
                if (database->StartEdit(TObjectType::EComposition, *itCompositions))
                {
                    if (database->DeleteComposition(*itCompositions, true))
                    {
                        changesMade = true;
                    }
                    else
                    {
                        database->CancelEdit(TObjectType::EComposition, *itCompositions);
                    }
                }
            }
            ++count;
            ++itCompositions;
        }    
    }
    {
        count = 0;
        CheckedListBox::CheckedIndexCollection^ selectedAssociationsList = associationsList->CheckedIndices;
        set<Duco::ObjectId>::const_iterator itAssociation = unusedAssociations->begin();
        while (itAssociation != unusedAssociations->end())
        {
            if (selectedAssociationsList->Contains(count))
            {
                if (database->StartEdit(TObjectType::EAssociationOrSociety, *itAssociation))
                {
                    if (database->DeleteAssociation(*itAssociation, true))
                    {
                        changesMade = true;
                    }
                    else
                    {
                        database->CancelEdit(TObjectType::EAssociationOrSociety, *itAssociation);
                    }
                }
            }
            ++count;
            ++itAssociation;
        }
    }
    {
        count = 0;
        CheckedListBox::CheckedIndexCollection^ selectedSeriesList = seriesList->CheckedIndices;
        set<Duco::ObjectId>::const_iterator itSeries = unusedMethodSeries->begin();
        while ( itSeries != unusedMethodSeries->end() )
        {
            if (selectedSeriesList->Contains(count))
            {
                if (database->StartEdit(TObjectType::EMethodSeries, *itSeries))
                {
                    if (database->DeleteMethodSeries(*itSeries, true))
                    {
                        changesMade = true;
                    }
                    else
                    {
                        database->CancelEdit(TObjectType::EMethodSeries, *itSeries);
                    }
                }
            }
            ++count;
            ++itSeries;
        }    
    }
    database->CallObservers(DucoUI::TEventType::EDeleted, Duco::TObjectType::EMethod, KNoId, KNoId);
    database->CallObservers(DucoUI::TEventType::EDeleted, Duco::TObjectType::ERinger, KNoId, KNoId);
    database->CallObservers(DucoUI::TEventType::EDeleted, Duco::TObjectType::ETower, KNoId, KNoId);
    database->CallObservers(DucoUI::TEventType::EDeleted, Duco::TObjectType::EComposition, KNoId, KNoId);
    database->CallObservers(DucoUI::TEventType::EDeleted, Duco::TObjectType::EMethodSeries, KNoId, KNoId);
    database->CallObservers(DucoUI::TEventType::EDeleted, Duco::TObjectType::EAssociationOrSociety, KNoId, KNoId);
    deleteBtn->Enabled = true;
    findBtn->Enabled = true;
    closeBtn->Enabled = true;
    selectAllBtn->Enabled = true;
    renumberInProgress = false;
    SetLabels();
    Find();
}

System::Void
DeleteUnusedUI::selectAllBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    for (int i = 0; i < ringerList->Items->Count; ++i)
        ringerList->SetItemChecked(i, true);
    for (int i = 0; i < towerList->Items->Count; ++i)
        towerList->SetItemChecked(i, true);
    for (int i = 0; i < methodList->Items->Count; ++i)
        methodList->SetItemChecked(i, true);
    for (int i = 0; i < ringsList->Items->Count; ++i)
        ringsList->SetItemChecked(i, true);
    for (int i = 0; i < seriesList->Items->Count; ++i)
        seriesList->SetItemChecked(i, true);
    for (int i = 0; i < compositionsList->Items->Count; ++i)
        compositionsList->SetItemChecked(i, true);
    for (int i = 0; i < associationsList->Items->Count; ++i)
        associationsList->SetItemChecked(i, true);
}

System::Void
DeleteUnusedUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void 
DeleteUnusedUI::DeleteUnusedUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    SetLabels();
    Find();
}

System::Void 
DeleteUnusedUI::SetLabels()
{
    totalRingersLbl->Text = "of " + Convert::ToString(database->NumberOfObjects(TObjectType::ERinger) + " ringers");
    totalTowersLbl->Text = " of " + Convert::ToString(database->NumberOfObjects(TObjectType::ETower) + " towers");
    totalMethodsLbl->Text = "of " + Convert::ToString(database->NumberOfObjects(TObjectType::EMethod) + " methods");
    totalRingsLbl->Text = "in " + Convert::ToString(database->NumberOfObjects(TObjectType::ETower) + " towers");
    totalCompositionsLbl->Text = "of " + Convert::ToString(database->NumberOfObjects(TObjectType::EComposition) + " compositions");
    totalSeriesLbl->Text = "of " + Convert::ToString(database->NumberOfObjects(TObjectType::EMethodSeries) + " method series");
    totalAssociationsLbl->Text = "of " + Convert::ToString(database->NumberOfObjects(TObjectType::EAssociationOrSociety) + " associations");
}

System::Void 
DeleteUnusedUI::DeleteUnusedUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e)
{
    if (renumberInProgress)
    {
        MessageBox::Show(this, "Cannot close whilst deleting", "Error - Please wait", MessageBoxButtons::OK, MessageBoxIcon::Information);
        e->Cancel = true;
    }
}

void 
DeleteUnusedUI::InitializeComponent()
{
    System::Windows::Forms::GroupBox^  associationsGrp;
    System::Windows::Forms::TableLayoutPanel^  associationsLayoutPanel;
    System::Windows::Forms::Panel^  associationsPanel;
    System::Windows::Forms::GroupBox^  ringersGroup;
    System::Windows::Forms::GroupBox^  towersGroup;
    System::Windows::Forms::GroupBox^  methodsGroup;
    System::Windows::Forms::GroupBox^  ringsGroup;
    System::Windows::Forms::GroupBox^  seriesGroup;
    System::Windows::Forms::GroupBox^  compositionsGroup;
    this->associationsList = (gcnew System::Windows::Forms::CheckedListBox());
    this->associationsEditor = (gcnew System::Windows::Forms::TextBox());
    this->totalAssociationsLbl = (gcnew System::Windows::Forms::Label());
    this->findBtn = (gcnew System::Windows::Forms::Button());
    this->closeBtn = (gcnew System::Windows::Forms::Button());
    this->selectAllBtn = (gcnew System::Windows::Forms::Button());
    this->ringersLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
    this->ringerList = (gcnew System::Windows::Forms::CheckedListBox());
    this->ringersPanel = (gcnew System::Windows::Forms::Panel());
    this->ringersEditor = (gcnew System::Windows::Forms::TextBox());
    this->totalRingersLbl = (gcnew System::Windows::Forms::Label());
    this->towersLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
    this->towerList = (gcnew System::Windows::Forms::CheckedListBox());
    this->towersPanel = (gcnew System::Windows::Forms::Panel());
    this->towersEditor = (gcnew System::Windows::Forms::TextBox());
    this->totalTowersLbl = (gcnew System::Windows::Forms::Label());
    this->methodsLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
    this->methodList = (gcnew System::Windows::Forms::CheckedListBox());
    this->methodsPanel = (gcnew System::Windows::Forms::Panel());
    this->methodsEditor = (gcnew System::Windows::Forms::TextBox());
    this->totalMethodsLbl = (gcnew System::Windows::Forms::Label());
    this->ringsLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
    this->ringsPanel = (gcnew System::Windows::Forms::Panel());
    this->totalRingsLbl = (gcnew System::Windows::Forms::Label());
    this->ringsEditor = (gcnew System::Windows::Forms::TextBox());
    this->ringsList = (gcnew System::Windows::Forms::CheckedListBox());
    this->seriesLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
    this->seriesList = (gcnew System::Windows::Forms::CheckedListBox());
    this->seriesPanel = (gcnew System::Windows::Forms::Panel());
    this->seriesEditor = (gcnew System::Windows::Forms::TextBox());
    this->totalSeriesLbl = (gcnew System::Windows::Forms::Label());
    this->compositionsLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
    this->compositionsList = (gcnew System::Windows::Forms::CheckedListBox());
    this->compositionsPanel = (gcnew System::Windows::Forms::Panel());
    this->compositionsEditor = (gcnew System::Windows::Forms::TextBox());
    this->totalCompositionsLbl = (gcnew System::Windows::Forms::Label());
    this->masterLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
    this->btnPanel = (gcnew System::Windows::Forms::Panel());
    this->deleteBtn = (gcnew System::Windows::Forms::Button());
    associationsGrp = (gcnew System::Windows::Forms::GroupBox());
    associationsLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
    associationsPanel = (gcnew System::Windows::Forms::Panel());
    ringersGroup = (gcnew System::Windows::Forms::GroupBox());
    towersGroup = (gcnew System::Windows::Forms::GroupBox());
    methodsGroup = (gcnew System::Windows::Forms::GroupBox());
    ringsGroup = (gcnew System::Windows::Forms::GroupBox());
    seriesGroup = (gcnew System::Windows::Forms::GroupBox());
    compositionsGroup = (gcnew System::Windows::Forms::GroupBox());
    associationsGrp->SuspendLayout();
    associationsLayoutPanel->SuspendLayout();
    associationsPanel->SuspendLayout();
    ringersGroup->SuspendLayout();
    this->ringersLayoutPanel->SuspendLayout();
    this->ringersPanel->SuspendLayout();
    towersGroup->SuspendLayout();
    this->towersLayoutPanel->SuspendLayout();
    this->towersPanel->SuspendLayout();
    methodsGroup->SuspendLayout();
    this->methodsLayoutPanel->SuspendLayout();
    this->methodsPanel->SuspendLayout();
    ringsGroup->SuspendLayout();
    this->ringsLayoutPanel->SuspendLayout();
    this->ringsPanel->SuspendLayout();
    seriesGroup->SuspendLayout();
    this->seriesLayoutPanel->SuspendLayout();
    this->seriesPanel->SuspendLayout();
    compositionsGroup->SuspendLayout();
    this->compositionsLayoutPanel->SuspendLayout();
    this->compositionsPanel->SuspendLayout();
    this->masterLayoutPanel->SuspendLayout();
    this->btnPanel->SuspendLayout();
    this->SuspendLayout();
    // 
    // associationsGrp
    // 
    this->masterLayoutPanel->SetColumnSpan(associationsGrp, 2);
    associationsGrp->Controls->Add(associationsLayoutPanel);
    associationsGrp->Dock = System::Windows::Forms::DockStyle::Fill;
    associationsGrp->Location = System::Drawing::Point(3, 390);
    associationsGrp->Name = L"associationsGrp";
    associationsGrp->Size = System::Drawing::Size(602, 123);
    associationsGrp->TabIndex = 27;
    associationsGrp->TabStop = false;
    associationsGrp->Text = L"Associations";
    // 
    // associationsLayoutPanel
    // 
    associationsLayoutPanel->ColumnCount = 1;
    associationsLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
        100)));
    associationsLayoutPanel->Controls->Add(this->associationsList, 0, 1);
    associationsLayoutPanel->Controls->Add(associationsPanel, 0, 0);
    associationsLayoutPanel->Dock = System::Windows::Forms::DockStyle::Fill;
    associationsLayoutPanel->Location = System::Drawing::Point(3, 16);
    associationsLayoutPanel->Name = L"associationsLayoutPanel";
    associationsLayoutPanel->RowCount = 2;
    associationsLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 32)));
    associationsLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
    associationsLayoutPanel->Size = System::Drawing::Size(596, 104);
    associationsLayoutPanel->TabIndex = 26;
    // 
    // associationsList
    // 
    this->associationsList->Dock = System::Windows::Forms::DockStyle::Fill;
    this->associationsList->FormattingEnabled = true;
    this->associationsList->IntegralHeight = false;
    this->associationsList->Location = System::Drawing::Point(3, 35);
    this->associationsList->Name = L"associationsList";
    this->associationsList->Size = System::Drawing::Size(590, 66);
    this->associationsList->TabIndex = 30;
    this->associationsList->ItemCheck += gcnew System::Windows::Forms::ItemCheckEventHandler(this, &DeleteUnusedUI::list_ItemCheck);
    // 
    // associationsPanel
    // 
    associationsPanel->Controls->Add(this->associationsEditor);
    associationsPanel->Controls->Add(this->totalAssociationsLbl);
    associationsPanel->Dock = System::Windows::Forms::DockStyle::Fill;
    associationsPanel->Location = System::Drawing::Point(3, 3);
    associationsPanel->Name = L"associationsPanel";
    associationsPanel->Size = System::Drawing::Size(590, 26);
    associationsPanel->TabIndex = 27;
    // 
    // associationsEditor
    // 
    this->associationsEditor->Location = System::Drawing::Point(5, 3);
    this->associationsEditor->Name = L"associationsEditor";
    this->associationsEditor->ReadOnly = true;
    this->associationsEditor->Size = System::Drawing::Size(53, 20);
    this->associationsEditor->TabIndex = 28;
    // 
    // totalAssociationsLbl
    // 
    this->totalAssociationsLbl->AutoSize = true;
    this->totalAssociationsLbl->Location = System::Drawing::Point(62, 6);
    this->totalAssociationsLbl->Name = L"totalAssociationsLbl";
    this->totalAssociationsLbl->Size = System::Drawing::Size(0, 13);
    this->totalAssociationsLbl->TabIndex = 29;
    // 
    // findBtn
    // 
    this->findBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
    this->findBtn->Location = System::Drawing::Point(285, 3);
    this->findBtn->Name = L"findBtn";
    this->findBtn->Size = System::Drawing::Size(75, 23);
    this->findBtn->TabIndex = 38;
    this->findBtn->Text = L"Find";
    this->findBtn->UseVisualStyleBackColor = true;
    this->findBtn->Click += gcnew System::EventHandler(this, &DeleteUnusedUI::findBtn_Click);
    // 
    // closeBtn
    // 
    this->closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
    this->closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
    this->closeBtn->Location = System::Drawing::Point(525, 3);
    this->closeBtn->Name = L"closeBtn";
    this->closeBtn->Size = System::Drawing::Size(75, 23);
    this->closeBtn->TabIndex = 40;
    this->closeBtn->Text = L"Close";
    this->closeBtn->UseVisualStyleBackColor = true;
    this->closeBtn->Click += gcnew System::EventHandler(this, &DeleteUnusedUI::closeBtn_Click);
    // 
    // selectAllBtn
    // 
    this->selectAllBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
    this->selectAllBtn->Location = System::Drawing::Point(366, 3);
    this->selectAllBtn->Name = L"selectAllBtn";
    this->selectAllBtn->Size = System::Drawing::Size(75, 23);
    this->selectAllBtn->TabIndex = 41;
    this->selectAllBtn->Text = L"Select all";
    this->selectAllBtn->UseVisualStyleBackColor = true;
    this->selectAllBtn->Click += gcnew System::EventHandler(this, &DeleteUnusedUI::selectAllBtn_Click);
    // 
    // ringersGroup
    // 
    ringersGroup->Controls->Add(this->ringersLayoutPanel);
    ringersGroup->Dock = System::Windows::Forms::DockStyle::Fill;
    ringersGroup->Location = System::Drawing::Point(307, 261);
    ringersGroup->Name = L"ringersGroup";
    ringersGroup->Size = System::Drawing::Size(298, 123);
    ringersGroup->TabIndex = 31;
    ringersGroup->TabStop = false;
    ringersGroup->Text = L"Ringers";
    // 
    // ringersLayoutPanel
    // 
    this->ringersLayoutPanel->ColumnCount = 1;
    this->ringersLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
        100)));
    this->ringersLayoutPanel->Controls->Add(this->ringerList, 0, 1);
    this->ringersLayoutPanel->Controls->Add(this->ringersPanel, 0, 0);
    this->ringersLayoutPanel->Dock = System::Windows::Forms::DockStyle::Fill;
    this->ringersLayoutPanel->Location = System::Drawing::Point(3, 16);
    this->ringersLayoutPanel->Name = L"ringersLayoutPanel";
    this->ringersLayoutPanel->RowCount = 2;
    this->ringersLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute,
        32)));
    this->ringersLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
    this->ringersLayoutPanel->Size = System::Drawing::Size(292, 104);
    this->ringersLayoutPanel->TabIndex = 32;
    // 
    // ringerList
    // 
    this->ringerList->Dock = System::Windows::Forms::DockStyle::Fill;
    this->ringerList->FormattingEnabled = true;
    this->ringerList->IntegralHeight = false;
    this->ringerList->Location = System::Drawing::Point(3, 35);
    this->ringerList->Name = L"ringerList";
    this->ringerList->Size = System::Drawing::Size(286, 66);
    this->ringerList->TabIndex = 36;
    this->ringerList->ItemCheck += gcnew System::Windows::Forms::ItemCheckEventHandler(this, &DeleteUnusedUI::list_ItemCheck);
    // 
    // ringersPanel
    // 
    this->ringersPanel->Controls->Add(this->ringersEditor);
    this->ringersPanel->Controls->Add(this->totalRingersLbl);
    this->ringersPanel->Dock = System::Windows::Forms::DockStyle::Fill;
    this->ringersPanel->Location = System::Drawing::Point(3, 3);
    this->ringersPanel->Name = L"ringersPanel";
    this->ringersPanel->Size = System::Drawing::Size(286, 26);
    this->ringersPanel->TabIndex = 33;
    // 
    // ringersEditor
    // 
    this->ringersEditor->Location = System::Drawing::Point(5, 3);
    this->ringersEditor->Name = L"ringersEditor";
    this->ringersEditor->ReadOnly = true;
    this->ringersEditor->Size = System::Drawing::Size(53, 20);
    this->ringersEditor->TabIndex = 35;
    // 
    // totalRingersLbl
    // 
    this->totalRingersLbl->AutoSize = true;
    this->totalRingersLbl->Location = System::Drawing::Point(62, 6);
    this->totalRingersLbl->Name = L"totalRingersLbl";
    this->totalRingersLbl->Size = System::Drawing::Size(0, 13);
    this->totalRingersLbl->TabIndex = 34;
    // 
    // towersGroup
    // 
    towersGroup->Controls->Add(this->towersLayoutPanel);
    towersGroup->Dock = System::Windows::Forms::DockStyle::Fill;
    towersGroup->Location = System::Drawing::Point(3, 3);
    towersGroup->Name = L"towersGroup";
    towersGroup->Size = System::Drawing::Size(298, 123);
    towersGroup->TabIndex = 1;
    towersGroup->TabStop = false;
    towersGroup->Text = L"Towers";
    // 
    // towersLayoutPanel
    // 
    this->towersLayoutPanel->ColumnCount = 1;
    this->towersLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
        100)));
    this->towersLayoutPanel->Controls->Add(this->towerList, 0, 1);
    this->towersLayoutPanel->Controls->Add(this->towersPanel, 0, 0);
    this->towersLayoutPanel->Dock = System::Windows::Forms::DockStyle::Fill;
    this->towersLayoutPanel->Location = System::Drawing::Point(3, 16);
    this->towersLayoutPanel->Name = L"towersLayoutPanel";
    this->towersLayoutPanel->RowCount = 2;
    this->towersLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 32)));
    this->towersLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
    this->towersLayoutPanel->Size = System::Drawing::Size(292, 104);
    this->towersLayoutPanel->TabIndex = 2;
    // 
    // towerList
    // 
    this->towerList->Dock = System::Windows::Forms::DockStyle::Fill;
    this->towerList->FormattingEnabled = true;
    this->towerList->IntegralHeight = false;
    this->towerList->Location = System::Drawing::Point(3, 35);
    this->towerList->Name = L"towerList";
    this->towerList->Size = System::Drawing::Size(286, 66);
    this->towerList->TabIndex = 6;
    this->towerList->ItemCheck += gcnew System::Windows::Forms::ItemCheckEventHandler(this, &DeleteUnusedUI::list_ItemCheck);
    // 
    // towersPanel
    // 
    this->towersPanel->Controls->Add(this->towersEditor);
    this->towersPanel->Controls->Add(this->totalTowersLbl);
    this->towersPanel->Dock = System::Windows::Forms::DockStyle::Fill;
    this->towersPanel->Location = System::Drawing::Point(3, 3);
    this->towersPanel->Name = L"towersPanel";
    this->towersPanel->Size = System::Drawing::Size(286, 26);
    this->towersPanel->TabIndex = 3;
    // 
    // towersEditor
    // 
    this->towersEditor->Location = System::Drawing::Point(3, 3);
    this->towersEditor->Name = L"towersEditor";
    this->towersEditor->ReadOnly = true;
    this->towersEditor->Size = System::Drawing::Size(53, 20);
    this->towersEditor->TabIndex = 4;
    // 
    // totalTowersLbl
    // 
    this->totalTowersLbl->AutoSize = true;
    this->totalTowersLbl->Location = System::Drawing::Point(62, 6);
    this->totalTowersLbl->Name = L"totalTowersLbl";
    this->totalTowersLbl->Size = System::Drawing::Size(0, 13);
    this->totalTowersLbl->TabIndex = 5;
    // 
    // methodsGroup
    // 
    methodsGroup->Controls->Add(this->methodsLayoutPanel);
    methodsGroup->Dock = System::Windows::Forms::DockStyle::Fill;
    methodsGroup->Location = System::Drawing::Point(3, 132);
    methodsGroup->Name = L"methodsGroup";
    methodsGroup->Size = System::Drawing::Size(298, 123);
    methodsGroup->TabIndex = 13;
    methodsGroup->TabStop = false;
    methodsGroup->Text = L"Methods";
    // 
    // methodsLayoutPanel
    // 
    this->methodsLayoutPanel->ColumnCount = 1;
    this->methodsLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
        100)));
    this->methodsLayoutPanel->Controls->Add(this->methodList, 0, 1);
    this->methodsLayoutPanel->Controls->Add(this->methodsPanel, 0, 0);
    this->methodsLayoutPanel->Dock = System::Windows::Forms::DockStyle::Fill;
    this->methodsLayoutPanel->Location = System::Drawing::Point(3, 16);
    this->methodsLayoutPanel->Name = L"methodsLayoutPanel";
    this->methodsLayoutPanel->RowCount = 2;
    this->methodsLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute,
        32)));
    this->methodsLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
    this->methodsLayoutPanel->Size = System::Drawing::Size(292, 104);
    this->methodsLayoutPanel->TabIndex = 14;
    // 
    // methodList
    // 
    this->methodList->Dock = System::Windows::Forms::DockStyle::Fill;
    this->methodList->FormattingEnabled = true;
    this->methodList->IntegralHeight = false;
    this->methodList->Location = System::Drawing::Point(3, 35);
    this->methodList->Name = L"methodList";
    this->methodList->Size = System::Drawing::Size(286, 66);
    this->methodList->TabIndex = 16;
    this->methodList->ItemCheck += gcnew System::Windows::Forms::ItemCheckEventHandler(this, &DeleteUnusedUI::list_ItemCheck);
    // 
    // methodsPanel
    // 
    this->methodsPanel->Controls->Add(this->methodsEditor);
    this->methodsPanel->Controls->Add(this->totalMethodsLbl);
    this->methodsPanel->Dock = System::Windows::Forms::DockStyle::Fill;
    this->methodsPanel->Location = System::Drawing::Point(3, 3);
    this->methodsPanel->Name = L"methodsPanel";
    this->methodsPanel->Size = System::Drawing::Size(286, 26);
    this->methodsPanel->TabIndex = 15;
    // 
    // methodsEditor
    // 
    this->methodsEditor->Location = System::Drawing::Point(3, 3);
    this->methodsEditor->Name = L"methodsEditor";
    this->methodsEditor->ReadOnly = true;
    this->methodsEditor->Size = System::Drawing::Size(53, 20);
    this->methodsEditor->TabIndex = 17;
    // 
    // totalMethodsLbl
    // 
    this->totalMethodsLbl->AutoSize = true;
    this->totalMethodsLbl->Location = System::Drawing::Point(62, 6);
    this->totalMethodsLbl->Name = L"totalMethodsLbl";
    this->totalMethodsLbl->Size = System::Drawing::Size(0, 13);
    this->totalMethodsLbl->TabIndex = 18;
    // 
    // ringsGroup
    // 
    ringsGroup->Controls->Add(this->ringsLayoutPanel);
    ringsGroup->Dock = System::Windows::Forms::DockStyle::Fill;
    ringsGroup->Location = System::Drawing::Point(307, 3);
    ringsGroup->Name = L"ringsGroup";
    ringsGroup->Size = System::Drawing::Size(298, 123);
    ringsGroup->TabIndex = 7;
    ringsGroup->TabStop = false;
    ringsGroup->Text = L"Rings";
    // 
    // ringsLayoutPanel
    // 
    this->ringsLayoutPanel->ColumnCount = 1;
    this->ringsLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
        100)));
    this->ringsLayoutPanel->Controls->Add(this->ringsPanel, 0, 0);
    this->ringsLayoutPanel->Controls->Add(this->ringsList, 0, 1);
    this->ringsLayoutPanel->Dock = System::Windows::Forms::DockStyle::Fill;
    this->ringsLayoutPanel->Location = System::Drawing::Point(3, 16);
    this->ringsLayoutPanel->Name = L"ringsLayoutPanel";
    this->ringsLayoutPanel->RowCount = 2;
    this->ringsLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 32)));
    this->ringsLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
    this->ringsLayoutPanel->Size = System::Drawing::Size(292, 104);
    this->ringsLayoutPanel->TabIndex = 8;
    // 
    // ringsPanel
    // 
    this->ringsPanel->Controls->Add(this->totalRingsLbl);
    this->ringsPanel->Controls->Add(this->ringsEditor);
    this->ringsPanel->Dock = System::Windows::Forms::DockStyle::Fill;
    this->ringsPanel->Location = System::Drawing::Point(3, 3);
    this->ringsPanel->Name = L"ringsPanel";
    this->ringsPanel->Size = System::Drawing::Size(286, 26);
    this->ringsPanel->TabIndex = 9;
    // 
    // totalRingsLbl
    // 
    this->totalRingsLbl->AutoSize = true;
    this->totalRingsLbl->Location = System::Drawing::Point(62, 7);
    this->totalRingsLbl->Name = L"totalRingsLbl";
    this->totalRingsLbl->Size = System::Drawing::Size(0, 13);
    this->totalRingsLbl->TabIndex = 11;
    // 
    // ringsEditor
    // 
    this->ringsEditor->Location = System::Drawing::Point(3, 3);
    this->ringsEditor->Name = L"ringsEditor";
    this->ringsEditor->ReadOnly = true;
    this->ringsEditor->Size = System::Drawing::Size(53, 20);
    this->ringsEditor->TabIndex = 10;
    // 
    // ringsList
    // 
    this->ringsList->Dock = System::Windows::Forms::DockStyle::Fill;
    this->ringsList->FormattingEnabled = true;
    this->ringsList->IntegralHeight = false;
    this->ringsList->Location = System::Drawing::Point(3, 35);
    this->ringsList->Name = L"ringsList";
    this->ringsList->Size = System::Drawing::Size(286, 66);
    this->ringsList->TabIndex = 12;
    this->ringsList->ItemCheck += gcnew System::Windows::Forms::ItemCheckEventHandler(this, &DeleteUnusedUI::list_ItemCheck);
    // 
    // seriesGroup
    // 
    seriesGroup->Controls->Add(this->seriesLayoutPanel);
    seriesGroup->Dock = System::Windows::Forms::DockStyle::Fill;
    seriesGroup->Location = System::Drawing::Point(307, 132);
    seriesGroup->Name = L"seriesGroup";
    seriesGroup->Size = System::Drawing::Size(298, 123);
    seriesGroup->TabIndex = 19;
    seriesGroup->TabStop = false;
    seriesGroup->Text = L"Method series";
    // 
    // seriesLayoutPanel
    // 
    this->seriesLayoutPanel->ColumnCount = 1;
    this->seriesLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
        100)));
    this->seriesLayoutPanel->Controls->Add(this->seriesList, 0, 1);
    this->seriesLayoutPanel->Controls->Add(this->seriesPanel, 0, 0);
    this->seriesLayoutPanel->Dock = System::Windows::Forms::DockStyle::Fill;
    this->seriesLayoutPanel->Location = System::Drawing::Point(3, 16);
    this->seriesLayoutPanel->Name = L"seriesLayoutPanel";
    this->seriesLayoutPanel->RowCount = 2;
    this->seriesLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 32)));
    this->seriesLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
    this->seriesLayoutPanel->Size = System::Drawing::Size(292, 104);
    this->seriesLayoutPanel->TabIndex = 20;
    // 
    // seriesList
    // 
    this->seriesList->Dock = System::Windows::Forms::DockStyle::Fill;
    this->seriesList->FormattingEnabled = true;
    this->seriesList->IntegralHeight = false;
    this->seriesList->Location = System::Drawing::Point(3, 35);
    this->seriesList->Name = L"seriesList";
    this->seriesList->Size = System::Drawing::Size(286, 66);
    this->seriesList->TabIndex = 24;
    this->seriesList->ItemCheck += gcnew System::Windows::Forms::ItemCheckEventHandler(this, &DeleteUnusedUI::list_ItemCheck);
    // 
    // seriesPanel
    // 
    this->seriesPanel->Controls->Add(this->seriesEditor);
    this->seriesPanel->Controls->Add(this->totalSeriesLbl);
    this->seriesPanel->Dock = System::Windows::Forms::DockStyle::Fill;
    this->seriesPanel->Location = System::Drawing::Point(3, 3);
    this->seriesPanel->Name = L"seriesPanel";
    this->seriesPanel->Size = System::Drawing::Size(286, 26);
    this->seriesPanel->TabIndex = 21;
    // 
    // seriesEditor
    // 
    this->seriesEditor->Location = System::Drawing::Point(5, 3);
    this->seriesEditor->Name = L"seriesEditor";
    this->seriesEditor->ReadOnly = true;
    this->seriesEditor->Size = System::Drawing::Size(53, 20);
    this->seriesEditor->TabIndex = 23;
    // 
    // totalSeriesLbl
    // 
    this->totalSeriesLbl->AutoSize = true;
    this->totalSeriesLbl->Location = System::Drawing::Point(62, 6);
    this->totalSeriesLbl->Name = L"totalSeriesLbl";
    this->totalSeriesLbl->Size = System::Drawing::Size(0, 13);
    this->totalSeriesLbl->TabIndex = 22;
    // 
    // compositionsGroup
    // 
    compositionsGroup->Controls->Add(this->compositionsLayoutPanel);
    compositionsGroup->Dock = System::Windows::Forms::DockStyle::Fill;
    compositionsGroup->Location = System::Drawing::Point(3, 261);
    compositionsGroup->Name = L"compositionsGroup";
    compositionsGroup->Size = System::Drawing::Size(298, 123);
    compositionsGroup->TabIndex = 25;
    compositionsGroup->TabStop = false;
    compositionsGroup->Text = L"Compositions";
    // 
    // compositionsLayoutPanel
    // 
    this->compositionsLayoutPanel->ColumnCount = 1;
    this->compositionsLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
        100)));
    this->compositionsLayoutPanel->Controls->Add(this->compositionsList, 0, 1);
    this->compositionsLayoutPanel->Controls->Add(this->compositionsPanel, 0, 0);
    this->compositionsLayoutPanel->Dock = System::Windows::Forms::DockStyle::Fill;
    this->compositionsLayoutPanel->Location = System::Drawing::Point(3, 16);
    this->compositionsLayoutPanel->Name = L"compositionsLayoutPanel";
    this->compositionsLayoutPanel->RowCount = 2;
    this->compositionsLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute,
        32)));
    this->compositionsLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent,
        100)));
    this->compositionsLayoutPanel->Size = System::Drawing::Size(292, 104);
    this->compositionsLayoutPanel->TabIndex = 26;
    // 
    // compositionsList
    // 
    this->compositionsList->Dock = System::Windows::Forms::DockStyle::Fill;
    this->compositionsList->FormattingEnabled = true;
    this->compositionsList->IntegralHeight = false;
    this->compositionsList->Location = System::Drawing::Point(3, 35);
    this->compositionsList->Name = L"compositionsList";
    this->compositionsList->Size = System::Drawing::Size(286, 66);
    this->compositionsList->TabIndex = 30;
    this->compositionsList->ItemCheck += gcnew System::Windows::Forms::ItemCheckEventHandler(this, &DeleteUnusedUI::list_ItemCheck);
    // 
    // compositionsPanel
    // 
    this->compositionsPanel->Controls->Add(this->compositionsEditor);
    this->compositionsPanel->Controls->Add(this->totalCompositionsLbl);
    this->compositionsPanel->Dock = System::Windows::Forms::DockStyle::Fill;
    this->compositionsPanel->Location = System::Drawing::Point(3, 3);
    this->compositionsPanel->Name = L"compositionsPanel";
    this->compositionsPanel->Size = System::Drawing::Size(286, 26);
    this->compositionsPanel->TabIndex = 27;
    // 
    // compositionsEditor
    // 
    this->compositionsEditor->Location = System::Drawing::Point(5, 3);
    this->compositionsEditor->Name = L"compositionsEditor";
    this->compositionsEditor->ReadOnly = true;
    this->compositionsEditor->Size = System::Drawing::Size(53, 20);
    this->compositionsEditor->TabIndex = 28;
    // 
    // totalCompositionsLbl
    // 
    this->totalCompositionsLbl->AutoSize = true;
    this->totalCompositionsLbl->Location = System::Drawing::Point(62, 6);
    this->totalCompositionsLbl->Name = L"totalCompositionsLbl";
    this->totalCompositionsLbl->Size = System::Drawing::Size(0, 13);
    this->totalCompositionsLbl->TabIndex = 29;
    // 
    // masterLayoutPanel
    // 
    this->masterLayoutPanel->ColumnCount = 2;
    this->masterLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
        50)));
    this->masterLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
        50)));
    this->masterLayoutPanel->Controls->Add(associationsGrp, 0, 3);
    this->masterLayoutPanel->Controls->Add(towersGroup, 0, 0);
    this->masterLayoutPanel->Controls->Add(ringsGroup, 1, 0);
    this->masterLayoutPanel->Controls->Add(methodsGroup, 0, 1);
    this->masterLayoutPanel->Controls->Add(seriesGroup, 1, 1);
    this->masterLayoutPanel->Controls->Add(compositionsGroup, 0, 2);
    this->masterLayoutPanel->Controls->Add(ringersGroup, 1, 2);
    this->masterLayoutPanel->Controls->Add(this->btnPanel, 0, 4);
    this->masterLayoutPanel->Dock = System::Windows::Forms::DockStyle::Fill;
    this->masterLayoutPanel->Location = System::Drawing::Point(0, 0);
    this->masterLayoutPanel->Name = L"masterLayoutPanel";
    this->masterLayoutPanel->RowCount = 5;
    this->masterLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 25)));
    this->masterLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 25)));
    this->masterLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 25)));
    this->masterLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 25)));
    this->masterLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 35)));
    this->masterLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
    this->masterLayoutPanel->Size = System::Drawing::Size(608, 553);
    this->masterLayoutPanel->TabIndex = 0;
    // 
    // btnPanel
    // 
    this->btnPanel->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
    this->masterLayoutPanel->SetColumnSpan(this->btnPanel, 2);
    this->btnPanel->Controls->Add(this->selectAllBtn);
    this->btnPanel->Controls->Add(this->closeBtn);
    this->btnPanel->Controls->Add(this->deleteBtn);
    this->btnPanel->Controls->Add(this->findBtn);
    this->btnPanel->Dock = System::Windows::Forms::DockStyle::Fill;
    this->btnPanel->Location = System::Drawing::Point(3, 519);
    this->btnPanel->Name = L"btnPanel";
    this->btnPanel->Size = System::Drawing::Size(602, 31);
    this->btnPanel->TabIndex = 37;
    // 
    // deleteBtn
    // 
    this->deleteBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
    this->deleteBtn->Enabled = false;
    this->deleteBtn->Location = System::Drawing::Point(441, 3);
    this->deleteBtn->Name = L"deleteBtn";
    this->deleteBtn->Size = System::Drawing::Size(75, 23);
    this->deleteBtn->TabIndex = 39;
    this->deleteBtn->Text = L"Delete";
    this->deleteBtn->UseVisualStyleBackColor = true;
    this->deleteBtn->Click += gcnew System::EventHandler(this, &DeleteUnusedUI::Delete_Click);
    // 
    // DeleteUnusedUI
    // 
    this->AcceptButton = this->closeBtn;
    this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
    this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
    this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
    this->CancelButton = this->closeBtn;
    this->ClientSize = System::Drawing::Size(608, 553);
    this->Controls->Add(this->masterLayoutPanel);
    this->MinimumSize = System::Drawing::Size(528, 350);
    this->Name = L"DeleteUnusedUI";
    this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Show;
    this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
    this->Text = L"Delete Unused";
    this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &DeleteUnusedUI::DeleteUnusedUI_FormClosing);
    this->Load += gcnew System::EventHandler(this, &DeleteUnusedUI::DeleteUnusedUI_Load);
    associationsGrp->ResumeLayout(false);
    associationsLayoutPanel->ResumeLayout(false);
    associationsPanel->ResumeLayout(false);
    associationsPanel->PerformLayout();
    ringersGroup->ResumeLayout(false);
    this->ringersLayoutPanel->ResumeLayout(false);
    this->ringersPanel->ResumeLayout(false);
    this->ringersPanel->PerformLayout();
    towersGroup->ResumeLayout(false);
    this->towersLayoutPanel->ResumeLayout(false);
    this->towersPanel->ResumeLayout(false);
    this->towersPanel->PerformLayout();
    methodsGroup->ResumeLayout(false);
    this->methodsLayoutPanel->ResumeLayout(false);
    this->methodsPanel->ResumeLayout(false);
    this->methodsPanel->PerformLayout();
    ringsGroup->ResumeLayout(false);
    this->ringsLayoutPanel->ResumeLayout(false);
    this->ringsPanel->ResumeLayout(false);
    this->ringsPanel->PerformLayout();
    seriesGroup->ResumeLayout(false);
    this->seriesLayoutPanel->ResumeLayout(false);
    this->seriesPanel->ResumeLayout(false);
    this->seriesPanel->PerformLayout();
    compositionsGroup->ResumeLayout(false);
    this->compositionsLayoutPanel->ResumeLayout(false);
    this->compositionsPanel->ResumeLayout(false);
    this->compositionsPanel->PerformLayout();
    this->masterLayoutPanel->ResumeLayout(false);
    this->btnPanel->ResumeLayout(false);
    this->ResumeLayout(false);
}
