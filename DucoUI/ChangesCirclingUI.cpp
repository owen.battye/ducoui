#include "RingingDatabaseObserver.h"
#include <StatisticFilters.h>
#include "DatabaseManager.h"
#include "RingerDisplayCache.h"

#include "ChangesCirclingUI.h"

#include <RingingDatabase.h>
#include "SystemDefaultIcon.h"
#include <PealDatabase.h>
#include "DucoUtils.h"
#include <DucoEngineUtils.h>
#include "DucoTableSorter.h"
#include "FiltersUI.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Windows::Forms;

ChangesCirclingUI::ChangesCirclingUI(DatabaseManager^ theDatabase)
: restart(false), database (theDatabase)
{
    filters = new Duco::StatisticFilters(database->Database());
	InitializeComponent();
}

ChangesCirclingUI::!ChangesCirclingUI()
{
    delete filters;
}

ChangesCirclingUI::~ChangesCirclingUI()
{
    this->!ChangesCirclingUI();
    if (components)
    {
        delete components;
    }
}

System::Void
ChangesCirclingUI::ChangesCirclingUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    StartGenerator();
}

System::Void
ChangesCirclingUI::ChangesCirclingUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e)
{
    if (backgroundWorker->IsBusy)
    {
        backgroundWorker->CancelAsync();
        e->Cancel = true;
    }
}

void 
ChangesCirclingUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    switch (type)
    {
    case TObjectType::EPeal:
            StartGenerator();
            break;

        default:
            break;
    }
}

System::Void
ChangesCirclingUI::StartGenerator()
{
    if (backgroundWorker->IsBusy)
        restart = true;
    else
    {
        restart = false;
        dataGridView->Rows->Clear();
        backgroundWorker->RunWorkerAsync();
    }
}

System::Void
ChangesCirclingUI::backgroundWorker_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
    std::map<unsigned int, Duco::PealLengthInfo> noOfChanges;
    database->Database().PealsDatabase().GetNoOfChangesRange(*filters, noOfChanges);

    float total = float(noOfChanges.size());
    float count = 0;
    std::map<unsigned int, Duco::PealLengthInfo>::const_iterator it = noOfChanges.begin();
    while (it != noOfChanges.end())
    {
        DataGridViewRow^ newRow = gcnew DataGridViewRow();
        newRow->HeaderCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->first));
        DataGridViewTextBoxCell^ valueCell = gcnew DataGridViewTextBoxCell();
        valueCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->second.TotalPeals(), true));
        newRow->Cells->Add(valueCell);
        DataGridViewTextBoxCell^ changesCell = gcnew DataGridViewTextBoxCell();
        changesCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->second.TotalChanges(), true));
        newRow->Cells->Add(changesCell);
        DataGridViewTextBoxCell^ timeCell = gcnew DataGridViewTextBoxCell();
        timeCell->Value = DucoUtils::PrintPealTime(it->second.TotalMinutes(), false);
        newRow->Cells->Add(timeCell);

        ++count;
        backgroundWorker->ReportProgress(int((count / total) * float(100)), newRow);
        ++it;
    }
}

System::Void
ChangesCirclingUI::backgroundWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    dataGridView->Rows->Add((DataGridViewRow^)e->UserState);
    progressBar->Value = e->ProgressPercentage;
}

System::Void
ChangesCirclingUI::backgroundWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    progressBar->Value = 0;
    if (restart)
    {
        StartGenerator();
    }
}

System::Void
ChangesCirclingUI::dataGridView_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
    DataGridViewColumn^ newColumn = dataGridView->Columns[e->ColumnIndex];
    SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;
    if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
        newSortOrder = SortOrder::Ascending;
    else
        newSortOrder = SortOrder::Descending;

    newColumn->HeaderCell->SortGlyphDirection = newSortOrder;

    DucoTableSorter^ sorter = gcnew DucoTableSorter(newSortOrder != SortOrder::Ascending, ECount);
    sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, 0);
    dataGridView->Sort(sorter);
}

System::Void
ChangesCirclingUI::filtersBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    FiltersUI^ filtersDialog = gcnew FiltersUI(*filters, database);
    System::Windows::Forms::DialogResult returnVal = filtersDialog->ShowDialog(this);
    if (returnVal == System::Windows::Forms::DialogResult::OK)
    {
        StartGenerator();
    }
}

#pragma region Windows Form Designer generated code
void ChangesCirclingUI::InitializeComponent(void)
{
    System::Windows::Forms::StatusStrip^ statusStrip1;
    System::Windows::Forms::ToolStrip^ toolStrip1;
    System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(ChangesCirclingUI::typeid));
    System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle1 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
    System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle2 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
    this->progressBar = (gcnew System::Windows::Forms::ToolStripProgressBar());
    this->filtersBtn = (gcnew System::Windows::Forms::ToolStripButton());
    this->dataGridView = (gcnew System::Windows::Forms::DataGridView());
    this->backgroundWorker = (gcnew System::ComponentModel::BackgroundWorker());
    this->PealCount = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->changesColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->timeColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
    toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
    statusStrip1->SuspendLayout();
    toolStrip1->SuspendLayout();
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->BeginInit();
    this->SuspendLayout();
    // 
    // statusStrip1
    // 
    statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->progressBar });
    statusStrip1->Location = System::Drawing::Point(0, 429);
    statusStrip1->Name = L"statusStrip1";
    statusStrip1->Size = System::Drawing::Size(385, 22);
    statusStrip1->TabIndex = 0;
    statusStrip1->Text = L"statusStrip1";
    // 
    // progressBar
    // 
    this->progressBar->Name = L"progressBar";
    this->progressBar->Size = System::Drawing::Size(100, 16);
    // 
    // toolStrip1
    // 
    toolStrip1->GripStyle = System::Windows::Forms::ToolStripGripStyle::Hidden;
    toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->filtersBtn });
    toolStrip1->Location = System::Drawing::Point(0, 0);
    toolStrip1->Name = L"toolStrip1";
    toolStrip1->Size = System::Drawing::Size(385, 25);
    toolStrip1->TabIndex = 2;
    toolStrip1->Text = L"toolStrip1";
    // 
    // filtersBtn
    // 
    this->filtersBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
    this->filtersBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"filtersBtn.Image")));
    this->filtersBtn->ImageTransparentColor = System::Drawing::Color::Magenta;
    this->filtersBtn->Name = L"filtersBtn";
    this->filtersBtn->Size = System::Drawing::Size(42, 22);
    this->filtersBtn->Text = L"&Filters";
    this->filtersBtn->Click += gcnew System::EventHandler(this, &ChangesCirclingUI::filtersBtn_Click);
    // 
    // dataGridView
    // 
    this->dataGridView->AllowUserToAddRows = false;
    this->dataGridView->AllowUserToDeleteRows = false;
    this->dataGridView->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(3) {
        this->PealCount,
            this->changesColumn, this->timeColumn
    });
    this->dataGridView->Dock = System::Windows::Forms::DockStyle::Fill;
    this->dataGridView->Location = System::Drawing::Point(0, 25);
    this->dataGridView->Name = L"dataGridView";
    this->dataGridView->ReadOnly = true;
    this->dataGridView->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToDisplayedHeaders;
    this->dataGridView->Size = System::Drawing::Size(385, 404);
    this->dataGridView->TabIndex = 1;
    this->dataGridView->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &ChangesCirclingUI::dataGridView_ColumnHeaderMouseClick);
    // 
    // backgroundWorker
    // 
    this->backgroundWorker->WorkerReportsProgress = true;
    this->backgroundWorker->WorkerSupportsCancellation = true;
    this->backgroundWorker->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &ChangesCirclingUI::backgroundWorker_DoWork);
    this->backgroundWorker->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &ChangesCirclingUI::backgroundWorker_ProgressChanged);
    this->backgroundWorker->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &ChangesCirclingUI::backgroundWorker_RunWorkerCompleted);
    // 
    // PealCount
    // 
    this->PealCount->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
    this->PealCount->HeaderText = L"Performances";
    this->PealCount->Name = L"PealCount";
    this->PealCount->ReadOnly = true;
    this->PealCount->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
    // 
    // changesColumn
    // 
    this->changesColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
    dataGridViewCellStyle1->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
    this->changesColumn->DefaultCellStyle = dataGridViewCellStyle1;
    this->changesColumn->HeaderText = L"Changes";
    this->changesColumn->Name = L"changesColumn";
    this->changesColumn->ReadOnly = true;
    this->changesColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
    this->changesColumn->Width = 74;
    // 
    // timeColumn
    // 
    this->timeColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
    dataGridViewCellStyle2->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
    this->timeColumn->DefaultCellStyle = dataGridViewCellStyle2;
    this->timeColumn->HeaderText = L"Time";
    this->timeColumn->Name = L"timeColumn";
    this->timeColumn->ReadOnly = true;
    this->timeColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
    this->timeColumn->Width = 55;
    // 
    // ChangesCirclingUI
    // 
    this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
    this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
    this->ClientSize = System::Drawing::Size(385, 451);
    this->Controls->Add(this->dataGridView);
    this->Controls->Add(toolStrip1);
    this->Controls->Add(statusStrip1);
    this->Name = L"ChangesCirclingUI";
    this->Text = L"Changes";
    this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &ChangesCirclingUI::ChangesCirclingUI_FormClosing);
    this->Load += gcnew System::EventHandler(this, &ChangesCirclingUI::ChangesCirclingUI_Load);
    statusStrip1->ResumeLayout(false);
    statusStrip1->PerformLayout();
    toolStrip1->ResumeLayout(false);
    toolStrip1->PerformLayout();
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->EndInit();
    this->ResumeLayout(false);
    this->PerformLayout();

}
#pragma endregion
