#pragma once
namespace DucoUI
{

    public ref class SelectMethodUI : public System::Windows::Forms::Form
    {
    public:
        SelectMethodUI(DucoUI::DatabaseManager^ theDatabase, Duco::ObjectId& newSelectedMethodId, const Duco::MethodSeries& methSeries);

    protected:
        ~SelectMethodUI();
        System::Void SelectMethodUI_Load(System::Object^  sender, System::EventArgs^  e);
        void InitializeComponent();

        System::Void cancelBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void acceptBtn_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void methodSelector_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);

    private:
        DucoUI::DatabaseManager^                            database;
        Duco::ObjectId&                                     selectedMethodId;
        const Duco::MethodSeries&                           currentMethodSeries;
        System::Collections::Generic::List<System::Int16>^  allMethodNames;
        System::ComponentModel::Container^                  components;
        System::Windows::Forms::Button^                     cancelBtn;
        System::Windows::Forms::Button^                     acceptBtn;
        System::Windows::Forms::ComboBox^                   methodSelector;
    };
}
