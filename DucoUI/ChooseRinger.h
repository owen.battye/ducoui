#pragma once

namespace DucoUI {

	public ref class ChooseRinger : public System::Windows::Forms::Form
	{
	public:
        ChooseRinger(DatabaseManager^ theDatabase, System::String^ missingRinger);

        Duco::ObjectId SelectedId();

	protected:
        ~ChooseRinger();
        System::Void ChooseRinger_Load(System::Object^ sender, System::EventArgs^ e);

        void CreateRingerControls(System::String^ ringerName);
        System::Void okButton_Click(System::Object^ sender, System::EventArgs^ e);

    private:
        System::Windows::Forms::Label^ ringerLbl;
        System::Windows::Forms::FlowLayoutPanel^ ringers;
        DatabaseManager^ database;
        System::Collections::Generic::List<System::Int64>^ ringerIds;
        System::String^ missingRingerName;

        System::ComponentModel::IContainer^ components;
        System::Windows::Forms::ToolTip^ toolTip1;
    private: System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;

#pragma region Windows Form Designer generated code
		void InitializeComponent()
		{
            this->components = (gcnew System::ComponentModel::Container());
            System::Windows::Forms::Button^ cancelBtn;
            System::Windows::Forms::Button^ okButton;
            this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            this->ringerLbl = (gcnew System::Windows::Forms::Label());
            this->ringers = (gcnew System::Windows::Forms::FlowLayoutPanel());
            this->toolTip1 = (gcnew System::Windows::Forms::ToolTip(this->components));
            cancelBtn = (gcnew System::Windows::Forms::Button());
            okButton = (gcnew System::Windows::Forms::Button());
            this->tableLayoutPanel1->SuspendLayout();
            this->SuspendLayout();
            // 
            // cancelBtn
            // 
            cancelBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            cancelBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            cancelBtn->Location = System::Drawing::Point(376, 217);
            cancelBtn->Name = L"cancelBtn";
            cancelBtn->Size = System::Drawing::Size(75, 23);
            cancelBtn->TabIndex = 1;
            cancelBtn->Text = L"Cancel";
            cancelBtn->UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this->tableLayoutPanel1->ColumnCount = 3;
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
                100)));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
                20)));
            this->tableLayoutPanel1->Controls->Add(okButton, 1, 2);
            this->tableLayoutPanel1->Controls->Add(cancelBtn, 2, 2);
            this->tableLayoutPanel1->Controls->Add(this->ringerLbl, 0, 0);
            this->tableLayoutPanel1->Controls->Add(this->ringers, 0, 1);
            this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            this->tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
            this->tableLayoutPanel1->RowCount = 2;
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            this->tableLayoutPanel1->Size = System::Drawing::Size(454, 243);
            this->tableLayoutPanel1->TabIndex = 0;
            // 
            // okButton
            // 
            okButton->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            okButton->Location = System::Drawing::Point(295, 217);
            okButton->Name = L"okButton";
            okButton->Size = System::Drawing::Size(75, 23);
            okButton->TabIndex = 3;
            okButton->Text = L"Choose";
            this->toolTip1->SetToolTip(okButton, L"Choose the selected ringer");
            okButton->UseVisualStyleBackColor = true;
            okButton->Click += gcnew System::EventHandler(this, &ChooseRinger::okButton_Click);
            // 
            // ringerLbl
            // 
            this->ringerLbl->AutoSize = true;
            this->tableLayoutPanel1->SetColumnSpan(this->ringerLbl, 3);
            this->ringerLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            this->ringerLbl->Location = System::Drawing::Point(3, 3);
            this->ringerLbl->Margin = System::Windows::Forms::Padding(3);
            this->ringerLbl->Name = L"ringerLbl";
            this->ringerLbl->Size = System::Drawing::Size(448, 13);
            this->ringerLbl->TabIndex = 3;
            this->ringerLbl->Text = L"Could not find an exact match for ringer.";
            // 
            // ringers
            // 
            this->ringers->AutoScroll = true;
            this->ringers->AutoSize = true;
            this->ringers->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            this->tableLayoutPanel1->SetColumnSpan(this->ringers, 3);
            this->ringers->Dock = System::Windows::Forms::DockStyle::Fill;
            this->ringers->FlowDirection = System::Windows::Forms::FlowDirection::TopDown;
            this->ringers->Location = System::Drawing::Point(3, 22);
            this->ringers->MinimumSize = System::Drawing::Size(100, 45);
            this->ringers->Name = L"ringers";
            this->ringers->Size = System::Drawing::Size(448, 189);
            this->ringers->TabIndex = 4;
            this->ringers->WrapContents = false;
            // 
            // ChooseRinger
            // 
            this->AcceptButton = okButton;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->CancelButton = cancelBtn;
            this->ClientSize = System::Drawing::Size(454, 243);
            this->Controls->Add(this->tableLayoutPanel1);
            this->Name = L"ChooseRinger";
            this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
            this->Text = L"Choose ringer";
            this->Load += gcnew System::EventHandler(this, &ChooseRinger::ChooseRinger_Load);
            this->tableLayoutPanel1->ResumeLayout(false);
            this->tableLayoutPanel1->PerformLayout();
            this->ResumeLayout(false);

        }
#pragma endregion
    };
}
