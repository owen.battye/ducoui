#include "DatabaseManager.h"
#include <DatabaseSettings.h>

#include "WebsiteLinksUI.h"

#include "SystemDefaultIcon.h"
#include "DucoUtils.h"

using namespace Duco;
using namespace DucoUI;
using namespace System::Windows::Forms;

WebsiteLinksUI::WebsiteLinksUI(DucoUI::DatabaseManager^ theDatabase)
:   database(theDatabase), populatingData(false), settingsChanged(false)
{
    InitializeComponent();
}

WebsiteLinksUI::~WebsiteLinksUI()
{
    if (components)
    {
        delete components;
    }
}

System::Void
WebsiteLinksUI::WebsiteLinksUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    this->Closing += gcnew System::ComponentModel::CancelEventHandler(this, &WebsiteLinksUI::ClosingEvent);
    Populate(database->Settings());
}

System::Void
WebsiteLinksUI::ClosingEvent( Object^ /*sender*/, System::ComponentModel::CancelEventArgs^ e )
{
    if (settingsChanged)
    {
        if ( MessageBox::Show(this, "Do you want to save changes?", "Unsaved settings changes", MessageBoxButtons::YesNo, MessageBoxIcon::Warning ) == ::DialogResult::Yes )
        {
            SaveSettings();
        }
    }
}

System::Void
WebsiteLinksUI::TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (!populatingData)
    {
        settingsChanged = true;
    }
    saveBtn->Enabled = settingsChanged;
}

System::Void
WebsiteLinksUI::cancelBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    settingsChanged = false;
    Close();
}

System::Void
WebsiteLinksUI::saveBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    bool closeAfterSave = settingsChanged;
    SaveSettings();
    if (closeAfterSave)
        Close();
}

System::Void
WebsiteLinksUI::Populate(const Duco::DatabaseSettings& populateWith)
{
    populatingData = true;
    bellBoardEditor->Text = DucoUtils::ConvertString(populateWith.BellBoardURL());
    bellBoardSuffixEditor->Text = DucoUtils::ConvertString(populateWith.BellBoardViewSuffixURL());
    felsteadUrlEditor->Text = DucoUtils::ConvertString(populateWith.FelsteadURL());
    pealBaseAssociationUrl->Text = DucoUtils::ConvertString(populateWith.PealbaseAssociationURL());
    pealBaseTowersUrl->Text = DucoUtils::ConvertString(populateWith.PealbaseTowerURL());
    compLibUrl->Text = DucoUtils::ConvertString(populateWith.CompLibURL());
    populatingData = false;
    settingsChanged = false;
}

System::Void 
WebsiteLinksUI::SaveSettings()
{
    if (settingsChanged)
    {
        DatabaseSettings& settings = database->Settings();
        std::wstring tempUrl;
        DucoUtils::ConvertString(DucoUtils::ParseUrl(bellBoardEditor->Text), tempUrl);
        settings.SetBellBoardURL(tempUrl);
        DucoUtils::ConvertString(DucoUtils::ParseUrl(bellBoardSuffixEditor->Text), tempUrl);
        settings.SetBellBoardViewSuffixURL(tempUrl);
        DucoUtils::ConvertString(DucoUtils::ParseUrl(felsteadUrlEditor->Text), tempUrl);
        settings.SetFelsteadURL(tempUrl);

        DucoUtils::ConvertString(DucoUtils::ParseUrl(pealBaseAssociationUrl->Text), tempUrl);
        settings.SetPealbaseAssociationURL(tempUrl);
        DucoUtils::ConvertString(DucoUtils::ParseUrl(pealBaseTowersUrl->Text), tempUrl);
        settings.SetPealbaseTowerURL(tempUrl);
        DucoUtils::ConvertString(DucoUtils::ParseUrl(compLibUrl->Text), tempUrl);
        settings.SetCompLibURL(tempUrl);

        settingsChanged = false;
    }
}

System::Void
WebsiteLinksUI::resetBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    DatabaseSettings newSettings = database->Settings();
    newSettings.ResetWebsiteLinks();
    Populate(newSettings);
    settingsChanged = true;
    saveBtn->Enabled = true;
}
