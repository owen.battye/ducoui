#include <Association.h>
#include "RingingDatabaseObserver.h"
#include "DatabaseManager.h"
#include "DucoWindowState.h"

#include "AssociationsUI.h"

#include <AssociationDatabase.h>
#include <RingingDatabase.h>
#include <ObjectType.h>
#include <PealLengthInfo.h>
#include <DatabaseSettings.h>
#include "DucoUtils.h"
#include "SoundUtils.h"
#include "SingleObjectStatsUI.h"
#include "DatabaseGenUtils.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;

System::Void
AssociationsUI::ShowAssociation(DucoUI::DatabaseManager^ theDatabase, System::Windows::Forms::Form^ parent, const Duco::ObjectId& associationId)
{
    if (associationId.ValidId())
    {
        AssociationsUI^ associationUI = gcnew AssociationsUI(theDatabase, DucoWindowState::EViewMode);
        if (theDatabase->FindAssociation(associationId, *associationUI->association, true))
        {
            associationUI->MdiParent = parent;
            associationUI->Show();
        }
    }
}

AssociationsUI::AssociationsUI(DatabaseManager^ theDatabase, DucoUI::DucoWindowState newState)
    : database(theDatabase), windowState(newState)
{
    InitializeComponent();
    association = new Duco::Association();
    allAssociationIds = gcnew System::Collections::Generic::List<System::Int16>();
}

AssociationsUI::~AssociationsUI()
{
    delete association;
    if (components)
    {
        delete components;
    }
}

System::Void
AssociationsUI::AssociationsUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    switch (windowState)
    {
    case DucoWindowState::EViewMode:
    default:
        if (association->Id().ValidId())
        {
            PopulateView();
        }
        else
        {
            startBtn_Click(sender, e);
        }
        break;
    case DucoWindowState::ENewMode:
        {
        ClearView();
        SetEditMode();
        UpdateIdLabel();
        dataChanged = true;
        }
        break;
    }
    database->AddObserver(this);
}

System::Void
AssociationsUI::ClosingEvent(System::Object^ object, System::ComponentModel::CancelEventArgs^ e)
{
    if (dataChanged)
    {
        if (DucoUtils::ConfirmLooseChanges("association", this))
        {
            e->Cancel = true;
        }
    }
    if (!e->Cancel)
    {
        database->CancelEdit(TObjectType::EAssociationOrSociety, association->Id());
        database->RemoveObserver(this);
    }
}

System::Void
AssociationsUI::UpdateIdLabel()
{
    if (association->Id().ValidId())
        numberOfObjectsLbl->Text = String::Format("{0:D}/{1:D}", association->Id().Id(), database->NumberOfObjects(TObjectType::EAssociationOrSociety));
    else
        numberOfObjectsLbl->Text = "New Association";
}

System::Void
AssociationsUI::PopulateView()
{
    nameEditor->Text = DucoUtils::ConvertString(association->Name());
    notesEditor->Text = DucoUtils::ConvertString(association->Notes());
    pealbaseEditor->Text = DucoUtils::ConvertString(association->PealbaseLink());
    linkedAssociations->Items->Clear();
    DatabaseGenUtils::GenerateAssociationOptions(linkedAssociations, allAssociationIds, database, *association, windowState != DucoWindowState::EEditMode && windowState != DucoWindowState::ENewMode);
    UpdateIdLabel();
    dataChanged = false;
}

System::Void
AssociationsUI::PopulateViewWithId(const Duco::ObjectId& assocationId)
{
    if (database->FindAssociation(assocationId, *association, false))
    {
        PopulateView();
    }
    else
    {
        ClearView();
        SoundUtils::PlayErrorSound();
    }
}

void
AssociationsUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    switch (type)
    {
    default:
        break;

    case TObjectType::EAssociationOrSociety:
        //DatabaseGenUtils::GenerateMethodOptions(allMethodIds, methodSelector, database, -1, -1, true, false, false);
        break;

    }
}

System::Void
AssociationsUI::editBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->StartEdit(TObjectType::EAssociationOrSociety, association->Id()))
    {
        SetEditMode();
        PopulateView();
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
AssociationsUI::cancelBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    database->CancelEdit(TObjectType::EAssociationOrSociety, association->Id());
    if (association->Id().ValidId())
    {
        SetViewMode();
        PopulateViewWithId(association->Id());
    }
    else
    {
        SetViewMode();
        startBtn_Click(sender, e);
    }
}

System::Void
AssociationsUI::saveBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (dataChanged)
    {
        if (association->Valid(database->Database(), false, true))
        {
            if (windowState == DucoWindowState::EEditMode)
            {
                if (database->UpdateAssociation(*association, true))
                {
                    SetViewMode();
                    database->CancelEdit(TObjectType::EAssociationOrSociety, association->Id());
                    PopulateViewWithId(association->Id());
                }
            }
            else if (windowState == DucoWindowState::ENewMode)
            {
                SetViewMode();
                PopulateViewWithId(database->AddAssociation(*association));
            }
        }
        else
        {
            //MessageBox::Show(this, DucoUtils::ConvertString(association->ErrorString(database->Database().Settings())), "Association is missing data");
            SoundUtils::PlayErrorSound();
        }
    }
}

System::Void
AssociationsUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    switch (windowState)
    {
    case DucoWindowState::EViewMode:
        Close();
        break;
    }
}

System::Void
AssociationsUI::startBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId objectId;
    if (database->FirstAssociation(objectId, false))
    {
        PopulateViewWithId(objectId);
    }
    else
    {
        ClearView();
        SoundUtils::PlayErrorSound();
    }
}

System::Void
AssociationsUI::back10Btn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId objectId(association->Id());
    if (database->ForwardMultipleAssociation(objectId, false))
    {
        PopulateViewWithId(objectId);
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
AssociationsUI::backBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId objectId(association->Id());
    if (database->NextAssociation(objectId, false, false))
    {
        PopulateViewWithId(objectId);
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }

}

System::Void
AssociationsUI::forwardBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId objectId(association->Id());
    if (database->NextAssociation(objectId, true, true))
    {
        PopulateViewWithId(objectId);
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
AssociationsUI::forward10Btn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId objectId(association->Id());
    if (database->ForwardMultipleAssociation(objectId, true))
    {
        PopulateViewWithId(objectId);
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
AssociationsUI::endBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId objectId;
    if (database->LastAssociation(objectId))
    {
        PopulateViewWithId(objectId);
    }
    else
    {
        ClearView();
        SoundUtils::PlayErrorSound();
    }
}

System::Void
AssociationsUI::SetViewMode()
{
    statisticsToolStripMenuItem->Enabled = true;
    linkedAssociations->Enabled = false;
    editBtn->Visible = true;
    closeBtn->Visible = true;
    cancelBtn->Visible = false;
    saveBtn->Visible = false;
    nameEditor->ReadOnly = true;
    notesEditor->ReadOnly = true;
    pealbaseEditor->ReadOnly = true;
    openBtn->Visible = true;
    startBtn->Visible = true;
    back10Btn->Visible = true;
    backBtn->Visible = true;
    forwardBtn->Visible = true;
    forward10Btn->Visible = true;
    endBtn->Visible = true;
    windowState = DucoUI::DucoWindowState::EViewMode;
}

System::Void
AssociationsUI::SetEditMode()
{
    statisticsToolStripMenuItem->Enabled = false;
    linkedAssociations->Enabled = true;
    editBtn->Visible = false;
    closeBtn->Visible = false;
    cancelBtn->Visible = true;
    saveBtn->Visible = true;
    nameEditor->ReadOnly = false;
    notesEditor->ReadOnly = false;
    openBtn->Visible = false;
    pealbaseEditor->ReadOnly = false;
    startBtn->Visible = false;
    back10Btn->Visible = false;
    backBtn->Visible = false;
    forwardBtn->Visible = false;
    forward10Btn->Visible = false;
    endBtn->Visible = false;
    if (windowState == DucoWindowState::EViewMode)
        windowState = DucoUI::DucoWindowState::EEditMode;
}

System::Void
AssociationsUI::ClearView()
{
    nameEditor->Text = "";
    notesEditor->Text = "";
    pealbaseEditor->Text = "";
    //DatabaseGenUtils::GenerateAssociationOptions(linkedAssociations, allAssociationIds, database, *association, windowState != DucoWindowState::EEditMode && windowState != DucoWindowState::ENewMode);
}

System::Void
AssociationsUI::nameEditor_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        dataChanged = true;
        std::wstring newName;
        DucoUtils::ConvertString(nameEditor->Text, newName);
        association->SetName(newName);
    }
}

System::Void
AssociationsUI::notesEditor_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        dataChanged = true;
        std::wstring newNotes;
        DucoUtils::ConvertString(notesEditor->Text, newNotes);
        association->SetNotes(newNotes);
    }
}

System::Void
AssociationsUI::pealbaseEditor_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        dataChanged = true;
        std::wstring newPealBaseCode;
        DucoUtils::ConvertString(pealbaseEditor->Text, newPealBaseCode);
        association->SetPealbaseLink(newPealBaseCode);
    }
}

System::Void
AssociationsUI::openBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (association->PealbaseLink().length() > 0)
    {
        String^ website = DucoUtils::ConvertString(database->Database().Settings().PealbaseAssociationURL());
        website += DucoUtils::ConvertString(association->PealbaseLink());
        System::Diagnostics::Process::Start(website);
    }
}

System::Void
AssociationsUI::statisticsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    SingleObjectStatsUI^ statsUI = gcnew SingleObjectStatsUI(database, association->Id(), TObjectType::EAssociationOrSociety);
    statsUI->MdiParent = this->MdiParent;
    statsUI->Show();
}

System::Void
AssociationsUI::linkedAssociations_ItemCheck(System::Object^ sender, System::Windows::Forms::ItemCheckEventArgs^ e)
{
    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        dataChanged = true;
        if (e->NewValue == System::Windows::Forms::CheckState::Checked)
        {
            System::Int16 associationId = allAssociationIds[e->Index];
            association->AddAssociationLink(associationId);
        }
        else
        {
            System::Int16 associationId = allAssociationIds[e->Index];
            association->RemoveAssociationLink(associationId);
        }
    }
}

