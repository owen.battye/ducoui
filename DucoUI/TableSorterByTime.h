#pragma once

namespace DucoUI
{
	public ref class TableSorterByTime :  public System::Collections::IComparer
	{
	public:
		TableSorterByTime(bool newAscending, int newColumnId);
        virtual int Compare(System::Object^, System::Object^);

	protected:
		~TableSorterByTime();
        int Compare(System::Windows::Forms::DataGridViewRow^ firstRow, System::Windows::Forms::DataGridViewRow^ secondRow);

        bool    ascending;
        int     columnId;
	};
}
