#pragma once
#include "RingingDatabaseObserver.h"

namespace Duco
{
    class StatisticFilters;
}

namespace DucoUI
{
    ref class DatabaseManager;

    public ref class YearCirclingUI : public System::Windows::Forms::Form, public DucoUI::RingingDatabaseObserver
    {
    public:
        YearCirclingUI(DucoUI::DatabaseManager^ theDatabase);

        // from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

    protected:
        ~YearCirclingUI();

        System::Void SetMonthNames();
        System::Void CreateMonthRow(const wchar_t* name);
        System::Void GenerateStats(System::Object^ sender, System::ComponentModel::DoWorkEventArgs^ e);
        System::Void StatsCompleted(System::Object^ sender, System::ComponentModel::RunWorkerCompletedEventArgs^ e);
        System::Void statsGenerator_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void conducted_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void printBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void filtersBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void printData(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args);
        System::Void ringerSelector_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);

        System::Void AddDayToStats(System::ComponentModel::BackgroundWorker^ worker, unsigned int month, unsigned int day, unsigned int pealCount, bool firstPealWasThisYear);
        System::Void SetNumberOfTimesYearCircled();
        System::Void YearCirclingUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void CreateColumns();
        System::Void InitColumn(System::Windows::Forms::DataGridViewTextBoxColumn^ theColumn, System::String^ name, System::String^ text, System::Windows::Forms::DataGridViewCellStyle^ style);
        System::Void SetBlanks();
        System::Void SetBlank(unsigned int month, unsigned int maxDays);
        System::Void ClearAll();
        System::Void ClearRow(unsigned int month, unsigned int maxDays);
        System::Void StartGenerator();

    protected:
        DucoUI::DatabaseManager^                  database;
        Duco::StatisticFilters*                         filters;
        System::ComponentModel::Container^              components;
        int                                             noOfTimesYearCircled;
        int                                             noOfDaysRemaining;
        System::Boolean                                 restartGenerator;
        System::Boolean                                 loading;

        System::ComponentModel::BackgroundWorker^       statsGenerator;
        System::Windows::Forms::DataGridView^           yearCirclingData;
        System::Windows::Forms::ToolStripProgressBar^   progressBar;
        System::Windows::Forms::ToolStripStatusLabel^  yearCircledEditor;

    protected:
    private: System::Windows::Forms::ToolStripButton^  printBtn;
             System::Windows::Forms::ToolStripStatusLabel^  remainingDaysEditor;


        System::Void InitializeComponent()
        {
            System::Windows::Forms::StatusStrip^  statusStrip1;
            System::Windows::Forms::ToolStrip^  toolStrip1;
            System::Windows::Forms::ToolStripButton^  filtersBtn;
            this->progressBar = (gcnew System::Windows::Forms::ToolStripProgressBar());
            this->yearCircledEditor = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->remainingDaysEditor = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->printBtn = (gcnew System::Windows::Forms::ToolStripButton());
            this->yearCirclingData = (gcnew System::Windows::Forms::DataGridView());
            this->statsGenerator = (gcnew System::ComponentModel::BackgroundWorker());
            statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
            filtersBtn = (gcnew System::Windows::Forms::ToolStripButton());
            statusStrip1->SuspendLayout();
            toolStrip1->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->yearCirclingData))->BeginInit();
            this->SuspendLayout();
            // 
            // statusStrip1
            // 
            statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
                this->progressBar, this->yearCircledEditor,
                    this->remainingDaysEditor
            });
            statusStrip1->Location = System::Drawing::Point(0, 323);
            statusStrip1->Name = L"statusStrip1";
            statusStrip1->Size = System::Drawing::Size(1209, 22);
            statusStrip1->TabIndex = 3;
            statusStrip1->Text = L"statusStrip1";
            // 
            // progressBar
            // 
            this->progressBar->Name = L"progressBar";
            this->progressBar->Size = System::Drawing::Size(100, 16);
            // 
            // yearCircledEditor
            // 
            this->yearCircledEditor->Name = L"yearCircledEditor";
            this->yearCircledEditor->Size = System::Drawing::Size(88, 17);
            this->yearCircledEditor->Text = L"Year not circled";
            // 
            // remainingDaysEditor
            // 
            this->remainingDaysEditor->Name = L"remainingDaysEditor";
            this->remainingDaysEditor->Size = System::Drawing::Size(1004, 17);
            this->remainingDaysEditor->Spring = true;
            this->remainingDaysEditor->Text = L"365 days left";
            this->remainingDaysEditor->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // toolStrip1
            // 
            toolStrip1->GripStyle = System::Windows::Forms::ToolStripGripStyle::Hidden;
            toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) { filtersBtn, this->printBtn });
            toolStrip1->Location = System::Drawing::Point(0, 0);
            toolStrip1->Name = L"toolStrip1";
            toolStrip1->Size = System::Drawing::Size(1209, 25);
            toolStrip1->TabIndex = 5;
            toolStrip1->Text = L"toolStrip1";
            // 
            // filtersBtn
            // 
            filtersBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
            filtersBtn->ImageTransparentColor = System::Drawing::Color::Magenta;
            filtersBtn->Name = L"filtersBtn";
            filtersBtn->Size = System::Drawing::Size(42, 22);
            filtersBtn->Text = L"&Filters";
            filtersBtn->ToolTipText = L"Set performance filters";
            filtersBtn->Click += gcnew System::EventHandler(this, &YearCirclingUI::filtersBtn_Click);
            // 
            // printBtn
            // 
            this->printBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
            this->printBtn->ImageTransparentColor = System::Drawing::Color::Magenta;
            this->printBtn->Name = L"printBtn";
            this->printBtn->Size = System::Drawing::Size(36, 22);
            this->printBtn->Text = L"&Print";
            this->printBtn->Click += gcnew System::EventHandler(this, &YearCirclingUI::printBtn_Click);
            // 
            // yearCirclingData
            // 
            this->yearCirclingData->AllowUserToAddRows = false;
            this->yearCirclingData->AllowUserToDeleteRows = false;
            this->yearCirclingData->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::DisplayedCells;
            this->yearCirclingData->AutoSizeRowsMode = System::Windows::Forms::DataGridViewAutoSizeRowsMode::DisplayedCells;
            this->yearCirclingData->ClipboardCopyMode = System::Windows::Forms::DataGridViewClipboardCopyMode::EnableAlwaysIncludeHeaderText;
            this->yearCirclingData->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->yearCirclingData->Dock = System::Windows::Forms::DockStyle::Fill;
            this->yearCirclingData->Location = System::Drawing::Point(0, 25);
            this->yearCirclingData->Name = L"yearCirclingData";
            this->yearCirclingData->ReadOnly = true;
            this->yearCirclingData->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
            this->yearCirclingData->Size = System::Drawing::Size(1209, 298);
            this->yearCirclingData->TabIndex = 2;
            // 
            // statsGenerator
            // 
            this->statsGenerator->WorkerReportsProgress = true;
            this->statsGenerator->WorkerSupportsCancellation = true;
            // 
            // YearCirclingUI
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->AutoScroll = true;
            this->ClientSize = System::Drawing::Size(1209, 345);
            this->Controls->Add(this->yearCirclingData);
            this->Controls->Add(statusStrip1);
            this->Controls->Add(toolStrip1);
            this->MinimumSize = System::Drawing::Size(575, 350);
            this->Name = L"YearCirclingUI";
            this->Text = L"Year circling";
            this->Load += gcnew System::EventHandler(this, &YearCirclingUI::YearCirclingUI_Load);
            statusStrip1->ResumeLayout(false);
            statusStrip1->PerformLayout();
            toolStrip1->ResumeLayout(false);
            toolStrip1->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->yearCirclingData))->EndInit();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
    };
}
