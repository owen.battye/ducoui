#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

namespace DucoUI {

    /// <summary>
    /// Summary for UploadPealUI
    /// </summary>
    public ref class UploadPealUI : public System::Windows::Forms::Form
    {
    public:
        UploadPealUI(DucoUI::DatabaseManager^ theDatabase, const Duco::Peal& thePeal);

    protected:
        !UploadPealUI();
        ~UploadPealUI();

        System::Void UploadPealUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void UploadPealUI_Activated(System::Object^  sender, System::EventArgs^  e);
        System::Void populatePerformanceToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void InitializeCEFSharp();
        System::Void GoToWebSite();
        System::Void GoToSubmit();
        System::Void RequestView();
        System::Void SavePealId();
        System::Void SetRWParameters(System::String^ address);
        System::Void SetUploadParameters();
        static System::Void BrowserLoadingStateChanged(System::Object ^, CefSharp::LoadingStateChangedEventArgs ^  e);
        System::ComponentModel::IContainer^     components;
        DucoUI::DatabaseManager^                database;
        Duco::Peal*                             currentPeal;
        System::Boolean                         submitted;
        System::Boolean                         rwpagesubmitted;
        CefSharp::WinForms::ChromiumWebBrowser^ chromeBrowser;
        System::Windows::Forms::ToolStripMenuItem^  populatePerformanceToolStripMenuItem;
        System::Windows::Forms::ToolStripStatusLabel^  statusLabel;
        System::Boolean                         firstActivation;

        void InitializeComponent()
        {
            System::Windows::Forms::MenuStrip^ menuStrip1;
            System::Windows::Forms::StatusStrip^ statusStrip1;
            this->populatePerformanceToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->statusLabel = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
            statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            menuStrip1->SuspendLayout();
            statusStrip1->SuspendLayout();
            this->SuspendLayout();
            // 
            // menuStrip1
            // 
            menuStrip1->AllowMerge = false;
            menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {
                this->populatePerformanceToolStripMenuItem
            });
            menuStrip1->Location = System::Drawing::Point(0, 0);
            menuStrip1->Name = L"menuStrip1";
            menuStrip1->ShowItemToolTips = true;
            menuStrip1->Size = System::Drawing::Size(1223, 24);
            menuStrip1->TabIndex = 0;
            menuStrip1->Text = L"menuStrip1";
            // 
            // populatePerformanceToolStripMenuItem
            // 
            this->populatePerformanceToolStripMenuItem->Name = L"populatePerformanceToolStripMenuItem";
            this->populatePerformanceToolStripMenuItem->Size = System::Drawing::Size(155, 20);
            this->populatePerformanceToolStripMenuItem->Text = L"Re-populate performance";
            this->populatePerformanceToolStripMenuItem->Click += gcnew System::EventHandler(this, &UploadPealUI::populatePerformanceToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->statusLabel });
            statusStrip1->Location = System::Drawing::Point(0, 789);
            statusStrip1->Name = L"statusStrip1";
            statusStrip1->Size = System::Drawing::Size(1223, 22);
            statusStrip1->TabIndex = 1;
            statusStrip1->Text = L"statusStrip1";
            // 
            // statusLabel
            // 
            this->statusLabel->Name = L"statusLabel";
            this->statusLabel->Size = System::Drawing::Size(1208, 17);
            this->statusLabel->Spring = true;
            // 
            // UploadPealUI
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(1223, 811);
            this->Controls->Add(statusStrip1);
            this->Controls->Add(menuStrip1);
            this->MainMenuStrip = menuStrip1;
            this->Name = L"UploadPealUI";
            this->StartPosition = System::Windows::Forms::FormStartPosition::Manual;
            this->Text = L"Upload a performance to Bellboard";
            this->Activated += gcnew System::EventHandler(this, &UploadPealUI::UploadPealUI_Activated);
            this->Load += gcnew System::EventHandler(this, &UploadPealUI::UploadPealUI_Load);
            menuStrip1->ResumeLayout(false);
            menuStrip1->PerformLayout();
            statusStrip1->ResumeLayout(false);
            statusStrip1->PerformLayout();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion
};
}
