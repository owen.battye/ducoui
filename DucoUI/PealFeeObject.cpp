#include <Peal.h>
#include "DatabaseManager.h"
#include "RingerEventArgs.h"

#include "PealFeeObject.h"

#include "DucoCommon.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Drawing;
using namespace Duco;
using namespace DucoUI;

PealFeeObject::PealFeeObject(DatabaseManager^ theDatabase, unsigned int theBellNumber, Duco::Peal* thePeal)
:   PealFeeObjectBase(theDatabase, theBellNumber, false, thePeal)
{
    InitializeComponent();
}

PealFeeObject::~PealFeeObject()
{
}

bool
PealFeeObject::SetPeal()
{
    bool ringerFound = PealFeeObjectBase::SetPeal();

    if (currentPeal != NULL)
    {
        if (currentPeal->StrapperOnBell(bellNumber))
        {
            AddStrapper();
            ringerFound &= strapperCtrl->SetPeal();
        }
    }

    return ringerFound;
}

void
PealFeeObject::InitializeComponent()
{
    this->SuspendLayout();
    // 
    // PealFeeObject
    // 
    this->Name = L"PealFeeObject";
    this->Size = System::Drawing::Size(KRingerWidth, KRingerLineHeight);
    this->AutoSize = true;

    this->ResumeLayout(false);
}

void
PealFeeObject::AddStrapper()
{
    this->SuspendLayout();
    if (strapperCtrl == nullptr)
    {
        strapperCtrl = gcnew PealFeeObjectBase(database, bellNumber, true, currentPeal);
        strapperCtrl->dataChangedEventHandler += gcnew System::EventHandler(this, &PealFeeObjectBase::PealFeeObjectChanged);
        strapperCtrl->Location = System::Drawing::Point(0, KRingerLineHeight);
        Controls->AddRange(gcnew cli::array< System::Windows::Forms::Control^  >(1){strapperCtrl});
    }
    else
    {
        strapperCtrl->Visible = true;
    }
    this->Size = System::Drawing::Size(KRingerWidth, KRingerAndConductorLineHeight);
    this->ResumeLayout(false);
}
