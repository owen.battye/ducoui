#include "BellboardParserWrapper.h"
#include "DucoUtils.h"

using namespace DucoUI;

BellboardParserWrapper::BellboardParserWrapper(DucoUI::BellboardParserUIWrapper^ theUiCallback)
: uiCallback(theUiCallback)
{

}

void
BellboardParserWrapper::Cancelled(const std::wstring& bellboardId, const char* message)
{
    uiCallback->Cancelled(DucoUtils::ConvertString(bellboardId), DucoUtils::ConvertString(message));
}

void
BellboardParserWrapper::Completed(const std::wstring& bellboardId, bool errors)
{
    uiCallback->Completed(DucoUtils::ConvertString(bellboardId), errors);
}


// BellboardRingerParserWrapper

BellboardRingerParserWrapper::BellboardRingerParserWrapper(DucoUI::BellboardPerformanceParserConfirmRingerCallbackUIWrapper^ theUiCallback)
    : uiCallback(theUiCallback)
{

}

Duco::ObjectId
BellboardRingerParserWrapper::ChooseRinger(const std::wstring& missingRingerName, const std::set<Duco::RingerPealDates>& nearMatches)
{
    return uiCallback->ChooseRinger(DucoUtils::ConvertString(missingRingerName), nearMatches);
}
