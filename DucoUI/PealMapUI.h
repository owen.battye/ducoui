#pragma once

namespace DucoUI
{
    public ref class PealMapUI : public System::Windows::Forms::Form
    {
    public:
        PealMapUI(DucoUI::DatabaseManager^ theManager);

    protected:
        ~PealMapUI();

        System::Void PealMapUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void InitializeCEFSharp();
        System::Void setting_CheckedChanged(System::Object^  sender, System::EventArgs^  e);

        System::Void GoToMap();

        System::Void startDateBackButton_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void startDateForwardsButton_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void endDateBackButton_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void endDateForwardsButton_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void urlBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void resetDatesBtn_Click(System::Object^ sender, System::EventArgs^ e);

        System::Void CheckDateRange();

    private:
        CefSharp::WinForms::ChromiumWebBrowser^ chromeBrowser;
        DucoUI::DatabaseManager^                database;
        System::ComponentModel::IContainer^     components;
        System::DateTime^                       startDate;
        System::DateTime^                       endDate;
        System::Windows::Forms::CheckBox^       zeroPerformances;
        System::Windows::Forms::CheckBox^       onePerformance;
        System::Windows::Forms::CheckBox^       moreThanOnePerformance;
        System::Windows::Forms::ToolTip^        toolTip1;

    private: System::Windows::Forms::TableLayoutPanel^  layoutPanel;
    private: System::Windows::Forms::ComboBox^  countySelector;
    private: System::Windows::Forms::Button^ urlBtn;
    private: System::Windows::Forms::CheckBox^ europeOnly;
    private: System::Windows::Forms::Label^ endDateLabel;

    private: System::Windows::Forms::Label^ startDateLabel;
    private: System::Windows::Forms::CheckBox^ combineLinked;
    private: System::Windows::Forms::CheckBox^ useFelstead;


    private: System::Windows::Forms::ToolStripStatusLabel^  statusLabel;

#pragma region Windows Form Designer generated code
        void InitializeComponent(void)
        {
            this->components = (gcnew System::ComponentModel::Container());
            System::Windows::Forms::StatusStrip^ statusStrip1;
            System::Windows::Forms::GroupBox^ settingsBox;
            System::Windows::Forms::GroupBox^ countyGroup;
            System::Windows::Forms::GroupBox^ startGroupBox;
            System::Windows::Forms::Button^ startDateUpBtn;
            System::Windows::Forms::Button^ startDateDownBtn;
            System::Windows::Forms::GroupBox^ endGroupBox;
            System::Windows::Forms::Button^ endDateUpBtn;
            System::Windows::Forms::Button^ endDateDownBtn;
            System::Windows::Forms::Button^ resetDatesBtn;
            System::Windows::Forms::GroupBox^ performanceSettingsGroups;
            this->statusLabel = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->combineLinked = (gcnew System::Windows::Forms::CheckBox());
            this->europeOnly = (gcnew System::Windows::Forms::CheckBox());
            this->countySelector = (gcnew System::Windows::Forms::ComboBox());
            this->startDateLabel = (gcnew System::Windows::Forms::Label());
            this->endDateLabel = (gcnew System::Windows::Forms::Label());
            this->useFelstead = (gcnew System::Windows::Forms::CheckBox());
            this->zeroPerformances = (gcnew System::Windows::Forms::CheckBox());
            this->moreThanOnePerformance = (gcnew System::Windows::Forms::CheckBox());
            this->onePerformance = (gcnew System::Windows::Forms::CheckBox());
            this->toolTip1 = (gcnew System::Windows::Forms::ToolTip(this->components));
            this->urlBtn = (gcnew System::Windows::Forms::Button());
            this->layoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
            statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            settingsBox = (gcnew System::Windows::Forms::GroupBox());
            countyGroup = (gcnew System::Windows::Forms::GroupBox());
            startGroupBox = (gcnew System::Windows::Forms::GroupBox());
            startDateUpBtn = (gcnew System::Windows::Forms::Button());
            startDateDownBtn = (gcnew System::Windows::Forms::Button());
            endGroupBox = (gcnew System::Windows::Forms::GroupBox());
            endDateUpBtn = (gcnew System::Windows::Forms::Button());
            endDateDownBtn = (gcnew System::Windows::Forms::Button());
            resetDatesBtn = (gcnew System::Windows::Forms::Button());
            performanceSettingsGroups = (gcnew System::Windows::Forms::GroupBox());
            statusStrip1->SuspendLayout();
            settingsBox->SuspendLayout();
            countyGroup->SuspendLayout();
            startGroupBox->SuspendLayout();
            endGroupBox->SuspendLayout();
            performanceSettingsGroups->SuspendLayout();
            this->layoutPanel->SuspendLayout();
            this->SuspendLayout();
            // 
            // statusStrip1
            // 
            statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->statusLabel });
            statusStrip1->Location = System::Drawing::Point(0, 595);
            statusStrip1->Name = L"statusStrip1";
            statusStrip1->Size = System::Drawing::Size(870, 22);
            statusStrip1->TabIndex = 1;
            statusStrip1->Text = L"statusStrip1";
            // 
            // statusLabel
            // 
            this->statusLabel->Name = L"statusLabel";
            this->statusLabel->Size = System::Drawing::Size(855, 17);
            this->statusLabel->Spring = true;
            this->statusLabel->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
            // 
            // settingsBox
            // 
            settingsBox->AutoSize = true;
            settingsBox->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            settingsBox->Controls->Add(this->combineLinked);
            settingsBox->Controls->Add(this->europeOnly);
            settingsBox->Dock = System::Windows::Forms::DockStyle::Fill;
            settingsBox->Location = System::Drawing::Point(3, 3);
            settingsBox->Name = L"settingsBox";
            settingsBox->Size = System::Drawing::Size(200, 71);
            settingsBox->TabIndex = 6;
            settingsBox->TabStop = false;
            settingsBox->Text = L"Settings";
            // 
            // combineLinked
            // 
            this->combineLinked->AutoSize = true;
            this->combineLinked->Checked = true;
            this->combineLinked->CheckState = System::Windows::Forms::CheckState::Checked;
            this->combineLinked->Location = System::Drawing::Point(6, 38);
            this->combineLinked->Margin = System::Windows::Forms::Padding(3, 3, 3, 0);
            this->combineLinked->Name = L"combineLinked";
            this->combineLinked->Size = System::Drawing::Size(98, 17);
            this->combineLinked->TabIndex = 5;
            this->combineLinked->Text = L"Combine linked";
            this->toolTip1->SetToolTip(this->combineLinked, L"Combine linked towers, so long as they dont have explicit different locations");
            this->combineLinked->UseVisualStyleBackColor = true;
            this->combineLinked->CheckedChanged += gcnew System::EventHandler(this, &PealMapUI::setting_CheckedChanged);
            // 
            // europeOnly
            // 
            this->europeOnly->AutoSize = true;
            this->europeOnly->Location = System::Drawing::Point(6, 18);
            this->europeOnly->Margin = System::Windows::Forms::Padding(3, 3, 3, 0);
            this->europeOnly->Name = L"europeOnly";
            this->europeOnly->Size = System::Drawing::Size(82, 17);
            this->europeOnly->TabIndex = 4;
            this->europeOnly->Text = L"Europe only";
            this->europeOnly->UseVisualStyleBackColor = true;
            this->europeOnly->CheckedChanged += gcnew System::EventHandler(this, &PealMapUI::setting_CheckedChanged);
            // 
            // countyGroup
            // 
            countyGroup->AutoSize = true;
            countyGroup->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            this->layoutPanel->SetColumnSpan(countyGroup, 2);
            countyGroup->Controls->Add(this->countySelector);
            countyGroup->Dock = System::Windows::Forms::DockStyle::Fill;
            countyGroup->Location = System::Drawing::Point(325, 3);
            countyGroup->Name = L"countyGroup";
            countyGroup->Size = System::Drawing::Size(542, 71);
            countyGroup->TabIndex = 7;
            countyGroup->TabStop = false;
            countyGroup->Text = L"County";
            // 
            // countySelector
            // 
            this->countySelector->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
            this->countySelector->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->countySelector->Dock = System::Windows::Forms::DockStyle::Fill;
            this->countySelector->FormattingEnabled = true;
            this->countySelector->Location = System::Drawing::Point(3, 16);
            this->countySelector->Margin = System::Windows::Forms::Padding(20, 10, 20, 0);
            this->countySelector->Name = L"countySelector";
            this->countySelector->Size = System::Drawing::Size(536, 21);
            this->countySelector->TabIndex = 0;
            this->toolTip1->SetToolTip(this->countySelector, L"Select a county to limit results to");
            this->countySelector->SelectedIndexChanged += gcnew System::EventHandler(this, &PealMapUI::setting_CheckedChanged);
            this->countySelector->TextUpdate += gcnew System::EventHandler(this, &PealMapUI::setting_CheckedChanged);
            // 
            // startGroupBox
            // 
            startGroupBox->AutoSize = true;
            startGroupBox->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            startGroupBox->Controls->Add(this->startDateLabel);
            startGroupBox->Controls->Add(startDateUpBtn);
            startGroupBox->Controls->Add(startDateDownBtn);
            startGroupBox->Dock = System::Windows::Forms::DockStyle::Fill;
            startGroupBox->Location = System::Drawing::Point(209, 3);
            startGroupBox->Name = L"startGroupBox";
            startGroupBox->Size = System::Drawing::Size(110, 71);
            startGroupBox->TabIndex = 6;
            startGroupBox->TabStop = false;
            startGroupBox->Text = L"Start date";
            // 
            // startDateLabel
            // 
            this->startDateLabel->AutoSize = true;
            this->startDateLabel->Location = System::Drawing::Point(71, 19);
            this->startDateLabel->Name = L"startDateLabel";
            this->startDateLabel->Size = System::Drawing::Size(33, 13);
            this->startDateLabel->TabIndex = 5;
            this->startDateLabel->Text = L"Label";
            // 
            // startDateUpBtn
            // 
            startDateUpBtn->AutoSize = true;
            startDateUpBtn->Location = System::Drawing::Point(35, 15);
            startDateUpBtn->Margin = System::Windows::Forms::Padding(10, 10, 10, 0);
            startDateUpBtn->Name = L"startDateUpBtn";
            startDateUpBtn->Size = System::Drawing::Size(23, 23);
            startDateUpBtn->TabIndex = 4;
            startDateUpBtn->Text = L">";
            this->toolTip1->SetToolTip(startDateUpBtn, L"Forward one year");
            startDateUpBtn->UseVisualStyleBackColor = true;
            startDateUpBtn->Click += gcnew System::EventHandler(this, &PealMapUI::startDateForwardsButton_Click);
            // 
            // startDateDownBtn
            // 
            startDateDownBtn->AutoSize = true;
            startDateDownBtn->Location = System::Drawing::Point(6, 15);
            startDateDownBtn->Margin = System::Windows::Forms::Padding(10, 10, 10, 0);
            startDateDownBtn->Name = L"startDateDownBtn";
            startDateDownBtn->Size = System::Drawing::Size(23, 23);
            startDateDownBtn->TabIndex = 3;
            startDateDownBtn->Text = L"<";
            this->toolTip1->SetToolTip(startDateDownBtn, L"Back one year");
            startDateDownBtn->UseVisualStyleBackColor = true;
            startDateDownBtn->Click += gcnew System::EventHandler(this, &PealMapUI::startDateBackButton_Click);
            // 
            // endGroupBox
            // 
            endGroupBox->AutoSize = true;
            endGroupBox->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            endGroupBox->Controls->Add(this->endDateLabel);
            endGroupBox->Controls->Add(endDateUpBtn);
            endGroupBox->Controls->Add(endDateDownBtn);
            endGroupBox->Dock = System::Windows::Forms::DockStyle::Fill;
            endGroupBox->Location = System::Drawing::Point(209, 80);
            endGroupBox->Name = L"endGroupBox";
            endGroupBox->Size = System::Drawing::Size(110, 100);
            endGroupBox->TabIndex = 6;
            endGroupBox->TabStop = false;
            endGroupBox->Text = L"End date";
            // 
            // endDateLabel
            // 
            this->endDateLabel->AutoSize = true;
            this->endDateLabel->Location = System::Drawing::Point(71, 25);
            this->endDateLabel->Name = L"endDateLabel";
            this->endDateLabel->Size = System::Drawing::Size(0, 13);
            this->endDateLabel->TabIndex = 5;
            // 
            // endDateUpBtn
            // 
            endDateUpBtn->AutoSize = true;
            endDateUpBtn->Location = System::Drawing::Point(35, 19);
            endDateUpBtn->Margin = System::Windows::Forms::Padding(10, 10, 10, 0);
            endDateUpBtn->Name = L"endDateUpBtn";
            endDateUpBtn->Size = System::Drawing::Size(23, 23);
            endDateUpBtn->TabIndex = 4;
            endDateUpBtn->Text = L">";
            this->toolTip1->SetToolTip(endDateUpBtn, L"Forward one year");
            endDateUpBtn->UseVisualStyleBackColor = true;
            endDateUpBtn->Click += gcnew System::EventHandler(this, &PealMapUI::endDateForwardsButton_Click);
            // 
            // endDateDownBtn
            // 
            endDateDownBtn->AutoSize = true;
            endDateDownBtn->Location = System::Drawing::Point(6, 19);
            endDateDownBtn->Margin = System::Windows::Forms::Padding(10, 10, 10, 0);
            endDateDownBtn->Name = L"endDateDownBtn";
            endDateDownBtn->Size = System::Drawing::Size(23, 23);
            endDateDownBtn->TabIndex = 3;
            endDateDownBtn->Text = L"<";
            this->toolTip1->SetToolTip(endDateDownBtn, L"Back one year");
            endDateDownBtn->UseVisualStyleBackColor = true;
            endDateDownBtn->Click += gcnew System::EventHandler(this, &PealMapUI::endDateBackButton_Click);
            // 
            // resetDatesBtn
            // 
            resetDatesBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
            resetDatesBtn->Location = System::Drawing::Point(325, 157);
            resetDatesBtn->Name = L"resetDatesBtn";
            resetDatesBtn->Size = System::Drawing::Size(75, 23);
            resetDatesBtn->TabIndex = 9;
            resetDatesBtn->Text = L"Reset dates";
            resetDatesBtn->UseVisualStyleBackColor = true;
            resetDatesBtn->Click += gcnew System::EventHandler(this, &PealMapUI::resetDatesBtn_Click);
            // 
            // performanceSettingsGroups
            // 
            performanceSettingsGroups->Controls->Add(this->useFelstead);
            performanceSettingsGroups->Controls->Add(this->zeroPerformances);
            performanceSettingsGroups->Controls->Add(this->moreThanOnePerformance);
            performanceSettingsGroups->Controls->Add(this->onePerformance);
            performanceSettingsGroups->Location = System::Drawing::Point(3, 80);
            performanceSettingsGroups->Name = L"performanceSettingsGroups";
            performanceSettingsGroups->Size = System::Drawing::Size(200, 100);
            performanceSettingsGroups->TabIndex = 10;
            performanceSettingsGroups->TabStop = false;
            performanceSettingsGroups->Text = L"Number of performances";
            // 
            // useFelstead
            // 
            this->useFelstead->AutoSize = true;
            this->useFelstead->Location = System::Drawing::Point(0, 80);
            this->useFelstead->Margin = System::Windows::Forms::Padding(3, 3, 3, 0);
            this->useFelstead->Name = L"useFelstead";
            this->useFelstead->Size = System::Drawing::Size(124, 17);
            this->useFelstead->TabIndex = 4;
            this->useFelstead->Text = L"Felstead peal counts";
            this->toolTip1->SetToolTip(this->useFelstead, L"Use the number of peals on felstead to plot numbers");
            this->useFelstead->UseVisualStyleBackColor = true;
            this->useFelstead->CheckedChanged += gcnew System::EventHandler(this, &PealMapUI::setting_CheckedChanged);
            // 
            // zeroPerformances
            // 
            this->zeroPerformances->AutoSize = true;
            this->zeroPerformances->Location = System::Drawing::Point(0, 19);
            this->zeroPerformances->Name = L"zeroPerformances";
            this->zeroPerformances->Size = System::Drawing::Size(117, 17);
            this->zeroPerformances->TabIndex = 2;
            this->zeroPerformances->Text = L"Locations with zero";
            this->zeroPerformances->UseVisualStyleBackColor = true;
            this->zeroPerformances->CheckedChanged += gcnew System::EventHandler(this, &PealMapUI::setting_CheckedChanged);
            // 
            // moreThanOnePerformance
            // 
            this->moreThanOnePerformance->AutoSize = true;
            this->moreThanOnePerformance->Checked = true;
            this->moreThanOnePerformance->CheckState = System::Windows::Forms::CheckState::Checked;
            this->moreThanOnePerformance->Location = System::Drawing::Point(0, 61);
            this->moreThanOnePerformance->Margin = System::Windows::Forms::Padding(3, 3, 3, 0);
            this->moreThanOnePerformance->Name = L"moreThanOnePerformance";
            this->moreThanOnePerformance->Size = System::Drawing::Size(165, 17);
            this->moreThanOnePerformance->TabIndex = 1;
            this->moreThanOnePerformance->Text = L"Locations with more than one";
            this->toolTip1->SetToolTip(this->moreThanOnePerformance, L"Only show towers where more than one performance has been rung.");
            this->moreThanOnePerformance->UseVisualStyleBackColor = true;
            this->moreThanOnePerformance->CheckedChanged += gcnew System::EventHandler(this, &PealMapUI::setting_CheckedChanged);
            // 
            // onePerformance
            // 
            this->onePerformance->AutoSize = true;
            this->onePerformance->Checked = true;
            this->onePerformance->CheckState = System::Windows::Forms::CheckState::Checked;
            this->onePerformance->Location = System::Drawing::Point(0, 40);
            this->onePerformance->Name = L"onePerformance";
            this->onePerformance->Size = System::Drawing::Size(151, 17);
            this->onePerformance->TabIndex = 3;
            this->onePerformance->Text = L"Locations with exactly one";
            this->toolTip1->SetToolTip(this->onePerformance, L"Only show towers where one performance has been rung.");
            this->onePerformance->UseVisualStyleBackColor = true;
            this->onePerformance->CheckedChanged += gcnew System::EventHandler(this, &PealMapUI::setting_CheckedChanged);
            // 
            // urlBtn
            // 
            this->urlBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
            this->urlBtn->Location = System::Drawing::Point(792, 157);
            this->urlBtn->Name = L"urlBtn";
            this->urlBtn->Size = System::Drawing::Size(75, 23);
            this->urlBtn->TabIndex = 8;
            this->urlBtn->Text = L"Open";
            this->toolTip1->SetToolTip(this->urlBtn, L"Open in browser");
            this->urlBtn->UseVisualStyleBackColor = true;
            this->urlBtn->Click += gcnew System::EventHandler(this, &PealMapUI::urlBtn_Click);
            // 
            // layoutPanel
            // 
            this->layoutPanel->ColumnCount = 4;
            this->layoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->layoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->layoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            this->layoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->layoutPanel->Controls->Add(settingsBox, 0, 0);
            this->layoutPanel->Controls->Add(countyGroup, 2, 0);
            this->layoutPanel->Controls->Add(endGroupBox, 1, 1);
            this->layoutPanel->Controls->Add(startGroupBox, 1, 0);
            this->layoutPanel->Controls->Add(this->urlBtn, 3, 1);
            this->layoutPanel->Controls->Add(resetDatesBtn, 2, 1);
            this->layoutPanel->Controls->Add(performanceSettingsGroups, 0, 1);
            this->layoutPanel->Dock = System::Windows::Forms::DockStyle::Fill;
            this->layoutPanel->Location = System::Drawing::Point(0, 0);
            this->layoutPanel->Name = L"layoutPanel";
            this->layoutPanel->RowCount = 3;
            this->layoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            this->layoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            this->layoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            this->layoutPanel->Size = System::Drawing::Size(870, 595);
            this->layoutPanel->TabIndex = 7;
            // 
            // PealMapUI
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(870, 617);
            this->Controls->Add(this->layoutPanel);
            this->Controls->Add(statusStrip1);
            this->Name = L"PealMapUI";
            this->Text = L"Performance Map";
            this->Load += gcnew System::EventHandler(this, &PealMapUI::PealMapUI_Load);
            statusStrip1->ResumeLayout(false);
            statusStrip1->PerformLayout();
            settingsBox->ResumeLayout(false);
            settingsBox->PerformLayout();
            countyGroup->ResumeLayout(false);
            startGroupBox->ResumeLayout(false);
            startGroupBox->PerformLayout();
            endGroupBox->ResumeLayout(false);
            endGroupBox->PerformLayout();
            performanceSettingsGroups->ResumeLayout(false);
            performanceSettingsGroups->PerformLayout();
            this->layoutPanel->ResumeLayout(false);
            this->layoutPanel->PerformLayout();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion
    };
}
