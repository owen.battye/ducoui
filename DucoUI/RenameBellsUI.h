#pragma once

namespace DucoUI
{

    public ref class RenameBellsUI : public System::Windows::Forms::Form
    {
    public:
        RenameBellsUI(DucoUI::DatabaseManager^ theDatabase, Duco::Tower& theCurrentTower, bool& newChangesMade);

    protected:
        ~RenameBellsUI();

        System::Void saveBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void RenameBellsUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void RenameBellsUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e);

        System::Void extraTrebleBtn_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void sharpBtn_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void flatBtn_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void normalBtn_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void bellList_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
        void InitializeComponent();
        System::Void SetBellNameType(unsigned int bellNumber, Duco::TRenameBellType);

    private:
        Duco::Tower&                    currentTower;
        bool                            dataChanged;
        bool&                           changesMade;
        bool                            switchingBell;
        std::map<unsigned int, Duco::TRenameBellType>* renamedBells;

        DucoUI::DatabaseManager^              database;
        System::ComponentModel::Container^    components;
    
        System::Windows::Forms::ComboBox^     bellList;
        System::Windows::Forms::RadioButton^  extraTrebleBtn;
        System::Windows::Forms::RadioButton^  normalBtn;
        System::Windows::Forms::RadioButton^  sharpBtn;
        System::Windows::Forms::RadioButton^  flatBtn;
    };
}
