#include "TableSorterByHeaderCell.h"

using namespace DucoUI;
using namespace System::Windows::Forms;
using namespace System;
using namespace std;

TableSorterByHeaderCell::TableSorterByHeaderCell(bool newAscending)
:   ascending(newAscending)
{
}

TableSorterByHeaderCell::~TableSorterByHeaderCell()
{
}

int
TableSorterByHeaderCell::Compare(System::Object^ firstObject, System::Object^ secondObject)
{
    DataGridViewRow^ row1 = static_cast<DataGridViewRow^>(firstObject);
    DataGridViewRow^ row2 = static_cast<DataGridViewRow^>(secondObject);
    String^ row1Value = Convert::ToString(row1->HeaderCell->Value);
    String^ row2Value = Convert::ToString(row2->HeaderCell->Value);

    if (row1Value->Length != row2Value->Length)
        return row2Value->Length - row1Value->Length;

    return String::Compare(row1Value, row2Value);
}
