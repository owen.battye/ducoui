#pragma once

#include "TableSorterByFloat.h"

namespace DucoUI
{
    public ref class TableSorterByDistance :  public DucoUI::TableSorterByFloat
	{
	public:
		TableSorterByDistance(bool newAscending, int newColumnId);

	protected:
		~TableSorterByDistance();
        virtual float ConvertValueToFloat(System::Windows::Forms::DataGridViewRow^ row, int cellNumber) override;
	};
}
