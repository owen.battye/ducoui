#include "PrintYearCirclingUtil.h"
#include <Ringer.h>
#include "DucoUtils.h"
#include <DucoEngineUtils.h>
#include "DatabaseManager.h"

using namespace System;
using namespace System::Drawing;
using namespace System::Windows::Forms;
using namespace DucoUI;
using namespace Duco;

PrintYearCirclingUtil::PrintYearCirclingUtil(DucoUI::DatabaseManager^ theDatabase, System::Drawing::Printing::PrintPageEventArgs^ args)
:	PrintUtilBase(theDatabase, 10, args, false)
{
    ringerId = new Duco::ObjectId();
}

PrintYearCirclingUtil::!PrintYearCirclingUtil()
{
    delete ringerId;
}

PrintYearCirclingUtil::~PrintYearCirclingUtil()
{
    this->!PrintYearCirclingUtil();
}

void
PrintYearCirclingUtil::SetObjects(System::Windows::Forms::DataGridView^ theDataGridView, const Duco::ObjectId& newRingerId, unsigned int noOfDaysRemaining, unsigned int noOfTimesYearCircled, bool conductedOnly)
{
    dataGridView = theDataGridView;
    *ringerId = newRingerId;
    numberOfDaysRemaining = noOfDaysRemaining;
    numberOfTimesYearCircled = noOfTimesYearCircled;
    conductedPealsOnly = conductedOnly;
}

System::Void
PrintYearCirclingUtil::printObject(System::Drawing::Printing::PrintPageEventArgs^ printArgs, System::Drawing::Printing::PrinterSettings^ printSettings)
{
    StringFormat^ stringFormat = gcnew StringFormat;

    Duco::Ringer theRinger;
    String^ titlePrintBuffer = "";
    if (database->FindRinger(*ringerId, theRinger, false))
    {
        titlePrintBuffer = "Year circling for " + DucoUtils::ConvertString(theRinger.FullName(false));
    }
    else
    {
        titlePrintBuffer = "Summary for database: ";
        titlePrintBuffer += DucoUtils::FormatFilename(database->Filename());
    }

    if (titlePrintBuffer->Length > 0)
    {
        if (conductedPealsOnly)
            titlePrintBuffer += ", Conducted peals.";
        printArgs->Graphics->DrawString(titlePrintBuffer, boldFont, Brushes::Black, leftMargin, yPos, stringFormat);
        noOfLines -= 2;
        yPos += lineHeight + lineHeight;
    }

    String^ printBuffer = "Month\t  1\t  2\t  3\t  4\t  5\t  6\t  7\t  8\t  9\t10\t11\t12\t13\t14\t15\t16\t17\t18\t19\t20\t21\t22\t23\t24\t25\t26\t27\t28\t29\t30\t31";
    array<Single>^ tabStops = {25.0f};
    for (int cellNumber = 1; cellNumber < dataGridView->Rows->Count; ++cellNumber)
    {
        array<Single>^ extraTabArray = {25.0f};
        System::Array::Copy( extraTabArray, 0, tabStops, 0, 1);
    }
    stringFormat->SetTabStops(100.0f, tabStops);

    addDucoFooter(normalFont, stringFormat, printArgs);

    printArgs->Graphics->DrawString(printBuffer, boldFont, Brushes::Black, leftMargin, yPos, stringFormat);
    int printingResultNumber = 0;
    while (noOfLines > 0 && printingResultNumber < dataGridView->Rows->Count)
    {
        yPos += lineHeight;
        DataGridViewRow^ currentRow = dataGridView->Rows[printingResultNumber];
        printBuffer = currentRow->HeaderCell->Value->ToString() + "\t";
        for (int cellNumber = 0; cellNumber < currentRow->Cells->Count; ++cellNumber)
        {
            DataGridViewCell^ currentCell = currentRow->Cells[cellNumber];
            if (currentCell != nullptr && currentCell->Value != nullptr)
            {
                String^ callValue = currentCell->Value->ToString();
                if (callValue->Length < 2)
                {
                    printBuffer += "  ";
                }
                printBuffer += callValue + "\t";
            }
            else
            {
                cellNumber = currentRow->Cells->Count;
            }
            
        }
        printArgs->Graphics->DrawString(printBuffer, normalFont, Brushes::Black, leftMargin, yPos, stringFormat);

        noOfLines--;
        printingResultNumber++;
    }

    if (noOfLines >= 3)
    {
        yPos += lineHeight + lineHeight;
        printBuffer = Convert::ToString(numberOfDaysRemaining) + " day";
        if (numberOfDaysRemaining > 1)
            printBuffer += "s";
        printBuffer += " remaining to circle the year";
        if (conductedPealsOnly)
        {
            printBuffer += " as conductor";
        }
        if (numberOfTimesYearCircled > 0)
        {
            printBuffer += " again";
        }
        printBuffer += ".";
        printArgs->Graphics->DrawString(printBuffer, normalFont, Brushes::Black, leftMargin, yPos, stringFormat);
        yPos += lineHeight;
        if (numberOfTimesYearCircled > 0)
        {
            printBuffer = "Year circled " + Convert::ToString(numberOfTimesYearCircled) + " time";
            if (numberOfTimesYearCircled > 1)
                printBuffer += "s";
            printBuffer += ".";
            printArgs->Graphics->DrawString(printBuffer, normalFont, Brushes::Black, leftMargin, yPos, stringFormat);
        }
    }
}

