#pragma once

namespace DucoUI
{
	public ref class BellBoardConfirmIdUpdate : public System::Windows::Forms::Form
	{
	public:
        BellBoardConfirmIdUpdate(DucoUI::DatabaseManager^ theDatabase);

        System::Void PopulateView(const Duco::DownloadedPeal& downloadedPeal, const Duco::Peal& existingPeal);

	protected:
        ~BellBoardConfirmIdUpdate();
        System::Void AddLabel(System::String^ text, System::Int32 column, System::Int32 row);
        System::Void AddClickableLabel(System::String^ text, System::Int32 column, System::Int32 row);
        System::Void linkLabel_LinkClicked(System::Object^ sender, System::Windows::Forms::LinkLabelLinkClickedEventArgs^ e);
        System::Void cancelButton_Click(System::Object^ sender, System::EventArgs^ e);

    private:
        DucoUI::DatabaseManager^ database;
        System::Windows::Forms::Button^ acceptButton;
        System::Windows::Forms::Button^ declineButton;

        System::Windows::Forms::Button^ cancelButton;
    private: System::Windows::Forms::ToolTip^ toolTip1;
    private: System::Windows::Forms::TableLayoutPanel^ layoutPanel;
    private: System::ComponentModel::IContainer^ components;


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
            this->components = (gcnew System::ComponentModel::Container());
            System::Windows::Forms::Label^ label1;
            System::Windows::Forms::Label^ label2;
            System::Windows::Forms::TableLayoutPanel^ pageLayoutPanel;
            System::Windows::Forms::GroupBox^ groupBox1;
            System::Windows::Forms::Label^ label4;
            System::Windows::Forms::Label^ label3;
            System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(BellBoardConfirmIdUpdate::typeid));
            this->acceptButton = (gcnew System::Windows::Forms::Button());
            this->declineButton = (gcnew System::Windows::Forms::Button());
            this->cancelButton = (gcnew System::Windows::Forms::Button());
            this->layoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
            this->toolTip1 = (gcnew System::Windows::Forms::ToolTip(this->components));
            label1 = (gcnew System::Windows::Forms::Label());
            label2 = (gcnew System::Windows::Forms::Label());
            pageLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
            groupBox1 = (gcnew System::Windows::Forms::GroupBox());
            label4 = (gcnew System::Windows::Forms::Label());
            label3 = (gcnew System::Windows::Forms::Label());
            pageLayoutPanel->SuspendLayout();
            groupBox1->SuspendLayout();
            this->layoutPanel->SuspendLayout();
            this->SuspendLayout();
            // 
            // label1
            // 
            label1->AutoSize = true;
            pageLayoutPanel->SetColumnSpan(label1, 4);
            label1->Dock = System::Windows::Forms::DockStyle::Fill;
            label1->Location = System::Drawing::Point(3, 3);
            label1->Margin = System::Windows::Forms::Padding(3);
            label1->Name = L"label1";
            label1->Size = System::Drawing::Size(1090, 13);
            label1->TabIndex = 2;
            label1->Text = L"This peal might already exist in the database. ";
            // 
            // label2
            // 
            label2->AutoSize = true;
            pageLayoutPanel->SetColumnSpan(label2, 4);
            label2->Dock = System::Windows::Forms::DockStyle::Fill;
            label2->Location = System::Drawing::Point(3, 22);
            label2->Margin = System::Windows::Forms::Padding(3, 3, 3, 10);
            label2->Name = L"label2";
            label2->Size = System::Drawing::Size(1090, 13);
            label2->TabIndex = 3;
            label2->Text = L"Confirm the two peals below are the same performance to update or add the bell bo"
                L"ard id to the existing peal.";
            // 
            // pageLayoutPanel
            // 
            pageLayoutPanel->AutoSize = true;
            pageLayoutPanel->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            pageLayoutPanel->ColumnCount = 4;
            pageLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            pageLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            pageLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            pageLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            pageLayoutPanel->Controls->Add(label2, 0, 1);
            pageLayoutPanel->Controls->Add(this->acceptButton, 2, 3);
            pageLayoutPanel->Controls->Add(this->declineButton, 3, 3);
            pageLayoutPanel->Controls->Add(label1, 0, 0);
            pageLayoutPanel->Controls->Add(this->cancelButton, 0, 3);
            pageLayoutPanel->Controls->Add(groupBox1, 0, 2);
            pageLayoutPanel->Dock = System::Windows::Forms::DockStyle::Fill;
            pageLayoutPanel->Location = System::Drawing::Point(0, 0);
            pageLayoutPanel->Name = L"pageLayoutPanel";
            pageLayoutPanel->RowCount = 4;
            pageLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            pageLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            pageLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            pageLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            pageLayoutPanel->Size = System::Drawing::Size(1096, 146);
            pageLayoutPanel->TabIndex = 0;
            // 
            // acceptButton
            // 
            this->acceptButton->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->acceptButton->DialogResult = System::Windows::Forms::DialogResult::Yes;
            this->acceptButton->Location = System::Drawing::Point(920, 114);
            this->acceptButton->Margin = System::Windows::Forms::Padding(3, 3, 20, 3);
            this->acceptButton->Name = L"acceptButton";
            this->acceptButton->Size = System::Drawing::Size(75, 23);
            this->acceptButton->TabIndex = 0;
            this->acceptButton->Text = L"Accept";
            this->toolTip1->SetToolTip(this->acceptButton, L"Update bellboard id on this peal and continue");
            this->acceptButton->UseVisualStyleBackColor = true;
            // 
            // declineButton
            // 
            this->declineButton->DialogResult = System::Windows::Forms::DialogResult::No;
            this->declineButton->Location = System::Drawing::Point(1018, 114);
            this->declineButton->Name = L"declineButton";
            this->declineButton->Size = System::Drawing::Size(75, 23);
            this->declineButton->TabIndex = 1;
            this->declineButton->Text = L"Decline";
            this->toolTip1->SetToolTip(this->declineButton, L"Decline this peal update, but continue downloads");
            this->declineButton->UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this->cancelButton->Location = System::Drawing::Point(3, 114);
            this->cancelButton->Name = L"cancelButton";
            this->cancelButton->Size = System::Drawing::Size(75, 23);
            this->cancelButton->TabIndex = 6;
            this->cancelButton->Text = L"Stop";
            this->toolTip1->SetToolTip(this->cancelButton, L"Cancel this download, and stop all others");
            this->cancelButton->UseVisualStyleBackColor = true;
            this->cancelButton->Click += gcnew System::EventHandler(this, &BellBoardConfirmIdUpdate::cancelButton_Click);
            // 
            // groupBox1
            // 
            groupBox1->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            pageLayoutPanel->SetColumnSpan(groupBox1, 4);
            groupBox1->Controls->Add(this->layoutPanel);
            groupBox1->Dock = System::Windows::Forms::DockStyle::Fill;
            groupBox1->Location = System::Drawing::Point(3, 48);
            groupBox1->Name = L"groupBox1";
            groupBox1->Size = System::Drawing::Size(1090, 60);
            groupBox1->TabIndex = 7;
            groupBox1->TabStop = false;
            groupBox1->Text = L"Performance details";
            // 
            // layoutPanel
            // 
            this->layoutPanel->ColumnCount = 10;
            this->layoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->layoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->layoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->layoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->layoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->layoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->layoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            this->layoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->layoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->layoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->layoutPanel->Controls->Add(label4, 0, 1);
            this->layoutPanel->Controls->Add(label3, 0, 0);
            this->layoutPanel->Dock = System::Windows::Forms::DockStyle::Fill;
            this->layoutPanel->GrowStyle = System::Windows::Forms::TableLayoutPanelGrowStyle::FixedSize;
            this->layoutPanel->Location = System::Drawing::Point(3, 16);
            this->layoutPanel->Name = L"layoutPanel";
            this->layoutPanel->RowCount = 2;
            this->layoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            this->layoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            this->layoutPanel->Size = System::Drawing::Size(1084, 41);
            this->layoutPanel->TabIndex = 0;
            // 
            // label4
            // 
            label4->AutoSize = true;
            label4->Dock = System::Windows::Forms::DockStyle::Fill;
            label4->Location = System::Drawing::Point(3, 22);
            label4->Margin = System::Windows::Forms::Padding(3);
            label4->Name = L"label4";
            label4->Size = System::Drawing::Size(77, 16);
            label4->TabIndex = 6;
            label4->Text = L"Existing peal:";
            label4->TextAlign = System::Drawing::ContentAlignment::TopRight;
            // 
            // label3
            // 
            label3->AutoSize = true;
            label3->Dock = System::Windows::Forms::DockStyle::Fill;
            label3->Location = System::Drawing::Point(3, 3);
            label3->Margin = System::Windows::Forms::Padding(3);
            label3->Name = L"label3";
            label3->Size = System::Drawing::Size(77, 13);
            label3->TabIndex = 5;
            label3->Text = L"Bellboard peal:";
            label3->TextAlign = System::Drawing::ContentAlignment::TopRight;
            // 
            // BellBoardConfirmIdUpdate
            // 
            this->AcceptButton = this->acceptButton;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            this->CancelButton = this->declineButton;
            this->ClientSize = System::Drawing::Size(1096, 146);
            this->Controls->Add(pageLayoutPanel);
            this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
            this->Name = L"BellBoardConfirmIdUpdate";
            this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
            this->Text = L"Confirm bellboard id update";
            pageLayoutPanel->ResumeLayout(false);
            pageLayoutPanel->PerformLayout();
            groupBox1->ResumeLayout(false);
            this->layoutPanel->ResumeLayout(false);
            this->layoutPanel->PerformLayout();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion
    };
}
