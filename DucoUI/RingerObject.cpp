#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"

#include "RingerObject.h"

#include "RingerList.h"
#include "DucoCommon.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Drawing;
using namespace System::Windows::Forms;

using namespace Duco;
using namespace DucoUI;

RingerObject::RingerObject(DucoUI::RingerList^ theparent, unsigned int theBellNumber)
:   RingerObjectBase(theparent, theBellNumber, false)
{
    InitializeComponent();
    strapper->CheckedChanged += gcnew System::EventHandler(this, &RingerObject::strapper_CheckedChanged);
}

RingerObject::~RingerObject()
{
}

System::Void
RingerObject::SetState(DucoWindowState newState) 
{
    RingerObjectBase::SetState(newState);
    if (strapperCtrl != nullptr)
        strapperCtrl->SetState(state);

    switch (state)
    {
    case DucoWindowState::EEditMode:
    case DucoWindowState::ENewMode:
        {
        strapper->Enabled = true;
        strapper->Visible = parent->ShowStrapperCheckBox(bellNumber);
        }
        break;

    default:
        {
        strapper->Enabled = false;
        strapper->Visible = parent->ShowStrapperCheckBox(bellNumber);
        }
        break;
    }
}

System::Boolean
RingerObject::SetRinger(Duco::Peal& newPeal, int index, bool ringerIsConductor, int strapperIndex, bool strapperIsConductor, unsigned int noOfBellsInPeal)
{
    bool ringerFound = RingerObjectBase::SetRinger(newPeal, index, ringerIsConductor, -1, false, noOfBellsInPeal);

    if (strapperIndex != -1)
    {
        AddStrapper();
        ringerFound &= strapperCtrl->SetRinger(newPeal, strapperIndex, strapperIsConductor, -1, false, noOfBellsInPeal);
    }
    else if (strapperCtrl != nullptr)
    {
        RemoveStrapper(true);
    }

    return ringerFound;
}

System::Void
RingerObject::SetHandbell(bool handbell)
{
    if (handbell && strapper->Checked)
        strapper->Checked = false;
}


System::Void 
RingerObject::strapper_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode)
    {
        if (strapper->Checked)
        {
            AddStrapper();
            strapperCtrl->SetState(state);
            strapperCtrl->SetStrapper();
            parent->StrapperStatusChanged(bellNumber, true);
        }
        else
        {
            RemoveStrapper(false);
            parent->StrapperStatusChanged(bellNumber, false);
        }
    }
}

System::Void
RingerObject::CheckConductor(bool isConductor, bool silentAndNonConducted, bool strapperIsConductor)
{
    RingerObjectBase::CheckConductor(isConductor, silentAndNonConducted, strapperIsConductor);
    if (strapperCtrl != nullptr)
        strapperCtrl->CheckConductor(strapperIsConductor, silentAndNonConducted, false);
}

System::Void
RingerObject::BeginUpdate(System::Boolean saveOldValues)
{
    RingerObjectBase::BeginUpdate(saveOldValues);
    if (strapperCtrl != nullptr)
        strapperCtrl->BeginUpdate(saveOldValues);
}

System::Void
RingerObject::EndUpdate(DucoUI::RingerDisplayCache^ cache)
{
    RingerObjectBase::EndUpdate(cache);
    if (strapperCtrl != nullptr)
        strapperCtrl->EndUpdate(cache);
}


void
RingerObject::InitializeComponent()
{
    this->strapper = (gcnew System::Windows::Forms::CheckBox());
    this->SuspendLayout();
    // 
    // strapper
    // 
    this->strapper->AutoSize = true;
    this->strapper->Location = System::Drawing::Point(KStrapperXLocation, KYLabelPosition);
    this->strapper->Name = L"strapper";
    this->strapper->Size = System::Drawing::Size(KCheckBoxWidth, KCheckBoxHeight);
    this->strapper->TabIndex = 0;
    this->strapper->Text = "";
    this->strapper->UseVisualStyleBackColor = true;
    // 
    // RingerObject
    // 
    this->Name = L"RingerObject";
    this->Size = System::Drawing::Size(KRingerWidth, KRingerLineHeight);
    this->AutoSize = true;

    Controls->AddRange(gcnew cli::array< System::Windows::Forms::Control^  >(1){strapper});
    this->ResumeLayout(false);
}

void
RingerObject::RemoveStrapper(bool deleteControl)
{
    this->strapper->Checked = false;
    this->SuspendLayout();
    if (deleteControl)
    {
        Controls->Remove(strapperCtrl);
        delete strapperCtrl;
        strapperCtrl = nullptr;
    }
    else
    {
        strapperCtrl->Visible = false;
    }
    this->Size = System::Drawing::Size(KRingerWidth, KRingerLineHeight);
    this->ResumeLayout(false);
}

void
RingerObject::AddStrapper()
{
    this->SuspendLayout();
    if (strapperCtrl == nullptr)
    {
        strapperCtrl = gcnew RingerObjectBase(parent, bellNumber, true);
        BindingSource^ binding = gcnew BindingSource();
        binding->DataSource = ((BindingSource^)ringerEditor->DataSource)->DataSource;
        strapperCtrl->Editor()->DataSource = binding;
        strapperCtrl->Location = System::Drawing::Point(0, KRingerLineHeight);
        Controls->AddRange(gcnew cli::array< System::Windows::Forms::Control^  >(1){strapperCtrl});
    }
    else
    {
        strapperCtrl->Visible = true;
    }
    strapperCtrl->SetState(state);
    this->Size = System::Drawing::Size(KRingerWidth, KRingerAndConductorLineHeight);
    this->ResumeLayout(false);
    this->strapper->Checked = true;
    strapperCtrl->SetState(state);
}

System::Void
RingerObject::SetNameAndConductor(System::String^ ringerName, bool conductor, bool strapper, int index)
{
    if (strapper)
    {
        if (strapperCtrl == nullptr)
            AddStrapper();
        strapperCtrl->SetNameAndConductor(ringerName, conductor, strapper, index);
    }
    else
    {
        RingerObjectBase::SetNameAndConductor(ringerName, conductor, strapper, index);
    }
}

System::Boolean
RingerObject::SetIndex(System::Boolean isStrapper, int index)
{
    if (isStrapper && strapper && strapperCtrl != nullptr)
    {
        strapperCtrl->SetIndex(false, index);
    }
    else if (!isStrapper)
    {
        return RingerObjectBase::SetIndex(false, index);
    }
    return true;
}
