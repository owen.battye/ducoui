#pragma once

#include "MethodLineDrawer.h"

namespace DucoUI
{
    public ref class LongMethodLineDrawer : public MethodLineDrawer
    {
    public:
        LongMethodLineDrawer(System::Drawing::Graphics^ lineDrwr, Duco::Method& theMethod, DucoUI::DatabaseManager^ theDatabase, System::ComponentModel::BackgroundWorker^ bgWorker);
        ~LongMethodLineDrawer();

        void SetSecondHalfOnSecondLine(bool wrapHalfWay);

    protected: // new functions
        virtual System::Void DrawLead(System::Drawing::Pen^% penToUse) override;

    protected: // from base class MethodLineDrawer
        virtual void DrawGridLines() override;
        virtual bool DrawLine() override;
        virtual System::Void MoveTrebleLineAndDraw(System::Drawing::Pen^% penToUse) override;

    private:
        float   longLineResetPosition;
        bool    secondHalfOnSecondLine;
    };
}
