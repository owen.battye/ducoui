#include "MethodLineDrawer.h"
#include <Lead.h>
#include <Music.h>
#include <Method.h>
#include <Change.h>
#include <PlaceNotation.h>
#include "DatabaseManager.h"
#include "WindowsSettings.h"
#include <DatabaseSettings.h>
#include <RingingDatabase.h>
#include "DucoUtils.h"
#include <DucoEngineUtils.h>
#include <algorithm>

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::Drawing;
using namespace System::IO;
using namespace System::Windows::Forms;

const unsigned int KDefaultCallChanges = 6;

MethodLineDrawer::MethodLineDrawer(System::Drawing::Graphics^ lineDrwr, Duco::Method& theMethod, DucoUI::DatabaseManager^ theDatabase, System::ComponentModel::BackgroundWorker^ bgWorker)
:   lineDrawer(lineDrwr), music(NULL), currentMethod(theMethod), database(theDatabase),
    singleLead(NULL), worker(bgWorker), leadNumber(0), xPosWrapOffset(0), maxXPosition(0), maxYPosition(0), printCallings(true)
{
    lineDrawingPen = gcnew Pen(KNormalColour, KLineDrawingPenSize);
    trebleDrawingPen = gcnew Pen(KTrebleColour, KTrebleLineDrawingPenSize);
    leadStartBrush = gcnew SolidBrush(KNormalColour);
    gridTrebleDrawingPen = gcnew Pen(KTrebleColour, KGridLinesPenSize);
    treblePath = gcnew System::Drawing::Drawing2D::GraphicsPath();
}

MethodLineDrawer::~MethodLineDrawer()
{
    this->!MethodLineDrawer();
}

MethodLineDrawer::!MethodLineDrawer()
{
    delete singleLead;
    delete notation;
    delete music;
}

bool
MethodLineDrawer::StartDrawing(unsigned int startingFromBell)
{
    delete music;
    if (ShowMusic())
    {
        music = new Duco::Music(database->WindowsSettings()->InstallationDir(), currentMethod.Order());
    }
    else
    {
        music = NULL;
    }

    bool processedOk (false);
    if (currentMethod.PlaceNotation().length() > 0)
    {
        notation = new PlaceNotation(currentMethod, 0);
        singleLead = notation->CreateLead(-1);
        if (notation->NotationUpdated())
        {
            std::wstring newFullNotation = notation->FullNotation(true);
            if (database->UpdateNotation(currentMethod.Id(), newFullNotation))
            {
                currentMethod.SetPlaceNotation(newFullNotation);
            }
        }
        leadLine = gcnew System::Collections::Generic::List<System::Drawing::PointF>(singleLead->BlowsInLead()+1);
        perLeadXOffsetValue = KPositionOffset + (KPositionGap * float(currentMethod.Order()));
        leadStartFont = gcnew System::Drawing::Font(KGenericMethodFont, KLeadStartFontSize, FontStyle::Regular, GraphicsUnit::Point);
        allBellsFont = gcnew System::Drawing::Font(KGenericMethodFont, KAllBellsFontSize, FontStyle::Regular, GraphicsUnit::Point);
        allBellsBrush = gcnew SolidBrush(KBellsColour);
        callingTitleFont = gcnew System::Drawing::Font(KGenericMethodFont, KPositionGap, FontStyle::Regular, GraphicsUnit::Point);

        if (database->Settings().ShowGridLines() || database->Settings().ShowBobsAndSingles())
        {
            CreateGridPens();
        }
        if (database->Settings().ShowGridLines())
        {
            DrawGridLines();
        }
        drawingBell = startingFromBell;
        if (drawingBell <= 0 || drawingBell > currentMethod.Order())
        {
            drawingBell = currentMethod.PrintFromBell();
            if (drawingBell <= 0 || drawingBell > currentMethod.Order())
                drawingBell = notation->BestStartingLead(database->Database().MethodsDatabase());
        }
        if (DrawLine())
            processedOk = true;

        if (database->Settings().ShowBobsAndSingles() && printCallings)
            DrawCallings();
    }

    if (processedOk)
    {
        unsigned int noOfLeadsDrawn (leadNumber);
        if (database->Settings().ShowFullGrid())
            --noOfLeadsDrawn;
        if (currentMethod.NoOfLeads() == -1 || currentMethod.NoOfLeads() != noOfLeadsDrawn)
        {
            database->SetNumberOfLeads(currentMethod.Id(), noOfLeadsDrawn);
            currentMethod.SetNoOfLeads(noOfLeadsDrawn);
        }
    }
    lineDrawer = nullptr;
    System::GC::Collect();
    return processedOk;
}

bool
MethodLineDrawer::ShowTreble()
{
    return currentMethod.DrawTreble(database->Settings());
}

System::Void
MethodLineDrawer::CreateGridPens()
{
    if (linesDrawingBrush == nullptr)
    {
        linesDrawingBrush = gcnew SolidBrush(KGridColour);
        gridDrawingPen = gcnew Pen(KNormalColour, KGridLinesPenSize);
    }
}

bool
MethodLineDrawer::DrawCallings()
{
    delete music;
    music = NULL;
    float yOffset (0);
    if (database->Settings().ShowFullGrid())
        xPosWrapOffset += perLeadXOffsetValue;
    DrawCalling("Bob", yOffset, EBobLead);
    DrawCalling("Single", yOffset, ESingleLead);
    return true;
}

System::Void
MethodLineDrawer::DrawCalling(System::String^ leadDescription, float& yOffset, TLeadType leadEnd)
{
    if (!singleLead->SetLeadEndType(leadEnd))
        return;

    float newYOffSet (0);
    std::vector<unsigned int> affectedBells;
    if (singleLead->AffectedBells(leadEnd, affectedBells))
    {
        System::Collections::Generic::List<System::Drawing::PointF>^ trebleCallingLine;
        bool generateTreble(true);
        if (ShowTreble())
        {
            trebleCallingLine = gcnew System::Collections::Generic::List<System::Drawing::PointF>(KDefaultCallChanges+KDefaultCallChanges+1);
        }
        else
            generateTreble = false;

        Change leadEndChange(currentMethod.Order());
        singleLead->LeadEnd(leadEndChange);
        Lead* nextLead = notation->CreateLead(leadEndChange, -1);

        SizeF leadStartStrSize = lineDrawer->MeasureString(leadDescription, callingTitleFont);
        lineDrawer->DrawString(leadDescription, callingTitleFont, leadStartBrush, xPosWrapOffset + KPositionOffset, yOffset+(float(leadStartStrSize.Height)/4));
        yOffset += leadStartStrSize.Height;

        bool printChanges (database->Settings().LineType() != DatabaseSettings::ELongLine);
        std::vector<unsigned int>::const_iterator bellsIt = affectedBells.begin();
        while (bellsIt != affectedBells.end())
        {
            size_t changeNumber (0);
            System::Collections::Generic::List<System::Drawing::PointF>^ line = gcnew System::Collections::Generic::List<System::Drawing::PointF>(KDefaultCallChanges * 2 + 1);
            for (size_t changeNumberInFirstLead(singleLead->BlowsInLead()-KDefaultCallChanges+1); changeNumberInFirstLead <= singleLead->BlowsInLead(); ++changeNumberInFirstLead)
            {
                if (printChanges)
                    PrintRow(changeNumber-1, singleLead->Change(changeNumberInFirstLead), yOffset);
                unsigned int position (0);
                if (generateTreble && singleLead->Position(changeNumberInFirstLead, 1, position))
                {
                    AddPointToLine(trebleCallingLine, position, 1, changeNumber, yOffset);
                }
                if (singleLead->Position(changeNumberInFirstLead, *bellsIt, position))
                {
                    AddPointToLine(line, position, *bellsIt, changeNumber, yOffset);
                    ++changeNumber;
                }
            }
            for (unsigned int changeNumberInSecondLead(1); changeNumberInSecondLead < (KDefaultCallChanges-1); ++changeNumberInSecondLead)
            {
                if (printChanges)
                {
                    PrintRow(changeNumber - 1, singleLead->Change(changeNumberInSecondLead), yOffset);
                }
                unsigned int position (0);
                if (generateTreble && singleLead->Position(changeNumberInSecondLead, 1, position))
                {
                    AddPointToLine(trebleCallingLine, position, 1, changeNumber, yOffset);
                }
                if (nextLead->Position(changeNumberInSecondLead, *bellsIt, position))
                {
                    AddPointToLine(line, position, *bellsIt, changeNumber, yOffset);
                    ++changeNumber;
                }
            }
            if (generateTreble)
            {
                lineDrawer->DrawLines(gridTrebleDrawingPen, trebleCallingLine->ToArray());
                generateTreble = false;
            }
            ++bellsIt;
            lineDrawer->DrawLines(gridDrawingPen, line->ToArray());
            newYOffSet = std::max(newYOffSet,(*line)[line->Count-1].Y);
            printChanges = false;
        }
        delete nextLead;
    }

    yOffset = newYOffSet;
}

System::Void
MethodLineDrawer::AddPointToLine(System::Collections::Generic::List<System::Drawing::PointF>^% line, unsigned int position, unsigned int bellNumber, unsigned int changeNumber, float yOffset)
{
    float nextXPos (KPositionOffset + (float(position)*KPositionGap) + xPosWrapOffset);
    float nextYPos ((float(changeNumber)*KBlowGap) + yOffset);
    PointF nextPosition(nextXPos, nextYPos);
    line->Add(nextPosition);
}

System::Void 
MethodLineDrawer::PrintRow(unsigned int changeNo)
{
    Change change(currentMethod.Order());
    PrintRow(changeNo, singleLead->Change(changeNo), 0);
}

System::Void 
MethodLineDrawer::PrintRow(unsigned int changeNo, Change& change, float yOffset)
{
    String^ nextBell = DucoUtils::ConvertString(DucoEngineUtils::ToChar(8));
    SizeF stringSize = lineDrawer->MeasureString(nextBell, allBellsFont);
    float xCharPosition = xPosWrapOffset+KPositionOffset + KPositionGap - float(float(stringSize.Width/1.5F));
    float yPosition = yOffset+KBlowOffset-(stringSize.Height/1.8F)+(float(changeNo) * KBlowGap);

    bool printMusic (false);
    if (music != NULL)
    {
        if (music->CheckChange(change) || music->CheckChangeForRollup(change))
        {
            printMusic = true;
        }
    }

    unsigned int positionNo(0);
    while (positionNo < currentMethod.Order())
    {
        unsigned int bell(change[positionNo]);
        nextBell = DucoUtils::ConvertString(DucoEngineUtils::ToChar(bell));
        if (printMusic && change.MusicalBell(positionNo))
        {
            if (change.Rollup())
            {
                System::Drawing::SolidBrush^ musicalBrush = gcnew SolidBrush(KRollupColour);
                lineDrawer->DrawString(nextBell, allBellsFont, musicalBrush, xCharPosition, yPosition);
            }
            else
            {
                System::Drawing::SolidBrush^ musicalBrush = gcnew SolidBrush(KMusicalColour);
                lineDrawer->DrawString(nextBell, allBellsFont, musicalBrush, xCharPosition, yPosition);
            }
        }
        else if (change.IsLeadEnd() && changeNo > 1)
        {
            System::Drawing::SolidBrush^ leadEndBrush = gcnew SolidBrush(KLeadEndColour);
            lineDrawer->DrawString(nextBell, allBellsFont, leadEndBrush, xCharPosition, yPosition);
        }
        else
            lineDrawer->DrawString(nextBell, allBellsFont, allBellsBrush, xCharPosition, yPosition);
        xCharPosition += KPositionGap;
        ++positionNo;
    }
}

System::Void 
MethodLineDrawer::CreateNextLead()
{
    Change currentLeadEnd(currentMethod.Order());
    if (singleLead->LeadEnd(currentLeadEnd))
    {
        Duco::Lead* nextlead = notation->CreateLead(currentLeadEnd, -1);
        delete singleLead;
        singleLead = nextlead;
    }
}

bool
MethodLineDrawer::ShowMusic()
{
    return false;   
}

void
MethodLineDrawer::CheckHighestPosition(PointF nextPosition)
{
    if (maxXPosition < nextPosition.X)
        maxXPosition = nextPosition.X;
   if (maxYPosition < nextPosition.Y)
        maxYPosition = nextPosition.Y;
}

void
MethodLineDrawer::ShiftLineRight(float additionalXPosWrapOffset)
{
    xPosWrapOffset += additionalXPosWrapOffset;
}

void
MethodLineDrawer::SetPrintCallings(bool printCalls)
{
    printCallings = printCalls;
}