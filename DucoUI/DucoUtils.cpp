#include "DucoUtils.h"

#include <DatabaseSettings.h>
#include <DucoEngineUtils.h>
#include "SoundUtils.h"
#include <msclr\marshal.h>
#include <msclr\marshal_cppstd.h>
using namespace System::Text::RegularExpressions;

using namespace System;
using namespace System::IO;
using namespace System::Windows::Forms;
using namespace Duco;
using namespace DucoUI;
using namespace Runtime::InteropServices;

void 
DucoUtils::ConvertString (String^ s, std::string& os)
{
    os.clear();
    if (s != nullptr && s->Length > 0)
    {
        const char* chars = (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
        os = chars;
        Marshal::FreeHGlobal(IntPtr((void*)chars));
    }
}

void
DucoUtils::ConvertString(String^ s, std::wstring& os)
{
    os.clear();
    if (s != nullptr && s->Length > 0)
    {
        os = msclr::interop::marshal_as<std::wstring>(s);
    }
}

String^ 
DucoUtils::ConvertString (const std::string& os)
{
    String^ newString = gcnew String(os.c_str());
    return newString;
}

String^
DucoUtils::ConvertString(const std::wstring& os)
{
    String^ newString = gcnew String(os.data());
    return newString;
}

String^
DucoUtils::EncodeString(const std::wstring& os)
{
    String^ newString = ConvertString(DucoEngineUtils::EncodeString(os));
    return newString;
}

System::String^
DucoUtils::ConvertString(wchar_t singleChar)
{
    return Convert::ToString(singleChar);
}

System::String^ 
DucoUtils::ConvertString(const Duco::PerformanceDate& date, bool longDate)
{
    if (!date.IsYearValid())
    {
        return "";
    }
    if (longDate)
    {
        return ConvertDate(date)->ToString("dddd, d MMMM yyyy");
    }

    return ConvertDate(date)->ToString("d MMM yyyy");
}

System::String^
DucoUtils::ConvertString(const Duco::ObjectId& id)
{
    return DucoUtils::ConvertString(id.Str());
}

String^
DucoUtils::ParseUrl (System::String^ str)
{
    String^ tempStr = str->Trim();

    wchar_t charsToTrim = {L'\\'};
    tempStr = tempStr->TrimEnd(charsToTrim);

    return tempStr;
}

bool
DucoUtils::CheckUrlForDomain(System::String^ url, const Duco::DatabaseSettings& settings)
{
    String^ websiteDomainString = DucoUtils::ConvertString(settings.BellBoardURL());

    int startIndex = websiteDomainString->IndexOf("http://");
    websiteDomainString = websiteDomainString->Substring(startIndex + 7);

    return url->Contains(websiteDomainString) || websiteDomainString->Contains(url);
}

System::DateTime^
DucoUtils::ConvertDate(const PerformanceDate& date)
{
    try
    {
        System::DateTime^ returnDate = gcnew System::DateTime(date.Year(), date.Month(), date.Day());
        return returnDate;
    }
    catch (Exception^)
    {
    }
    return System::DateTime::Today;
}

System::DateTime^
DucoUtils::FirstDate()
{
    PerformanceDate firstDate;
    firstDate.ResetToEarliest();
    System::DateTime^ returnDate = gcnew System::DateTime(firstDate.Year(), firstDate.Month(), firstDate.Day());
    return returnDate;
}


Duco::PerformanceDate
DucoUtils::ConvertDate(System::DateTime^ date)
{
    PerformanceDate dateTime(date->Year, date->Month, date->Day);
    return dateTime;
}

Duco::PerformanceTime
DucoUtils::ToTime(System::String^ timeString)
{
    std::wstring temp;
    DucoUtils::ConvertString(timeString, temp);

    return PerformanceTime(temp);
}

String^ 
DucoUtils::FormatUIntToPounds(unsigned int pence)
{
    float pounds = float(pence) / 100;
    return String::Format("{0:C}", pounds);
}

String^ 
DucoUtils::FormatDoubleToString(double number, unsigned int digits)
{
    String^ newString = "";
    switch (digits)
    {
    case 0:
        newString = String::Format("{0:F0}", number);
        break;
    default:
    case 2:
        newString = String::Format("{0:F2}", number);
        break;
    case 5:
        newString = String::Format("{0:F5}", number);
        break;
    }

    return newString;
}

String^ 
DucoUtils::FormatFloatToString(float number)
{
    String^ newString = "";
    newString = String::Format("{0:F2}", number);

    return newString;
}

System::String^ 
DucoUtils::PrintPealTime(const Duco::PerformanceTime& time, bool longFormat)
{
    return DucoUtils::ConvertString(time.PrintPealTime(longFormat));
}

System::String^ 
DucoUtils::PrintPealTime(size_t timeInMinutes, bool longFormat)
{
    Duco::PerformanceTime pealTime(timeInMinutes);
    return DucoUtils::ConvertString(pealTime.PrintPealTime(longFormat));
}

unsigned int 
DucoUtils::ConvertString(String^ numberString)
{
    if (numberString->Length == 0)
        return 0;

    String^ stripped = Regex::Replace(numberString, "[^0-9]", "");
    return Convert::ToInt32(stripped);
}


bool
DucoUtils::ConfirmChanges(IWin32Window^ owner)
{
    if (MessageBox::Show(owner, "Are you sure? This operation cannot be undone.", "Warning", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning) == ::DialogResult::OK)
        return true;

    return false;
}

bool
DucoUtils::ConfirmLooseChanges(String^ objectName, IWin32Window^ owner)
{
    SoundUtils::PlayErrorSound();
    if (MessageBox::Show(owner, "Do you want to continue and lose changes?", "Duco - Unsaved changes to " + objectName, MessageBoxButtons::YesNo, MessageBoxIcon::Warning ) == ::DialogResult::No)
    {
        return true;
    }
    return false;
}

bool
DucoUtils::ShowInvalidPealDataDialog(Duco::DatabaseSettings& settings, const Duco::Peal& currentPeal, IWin32Window^ owner)
{
//    if (settings.AllowErrors())
    {
        if (MessageBox::Show(owner, "Data incorrectly entered, ignore?", "Performance is missing data", MessageBoxButtons::OKCancel) == ::DialogResult::OK)
            return false;
    }
/*    else
    {
        MessageBox::Show(owner, "Cannot save this peal with the current errors. \n" + DucoUtils::ConvertString(currentPeal.ErrorString(settings)), "Check all the data is entered correctly");
    }*/
    return true;
}

bool
DucoUtils::ConvertTextToNumber(System::String^  str, unsigned int& returnNumber)
{
    if (str == nullptr || str->Length <= 0)
        return false;

    try
    {
        returnNumber = Convert::ToInt16(str);
        return true;
    }
    catch (Exception^)
    {
        return false;
    }
}

DateTime^
DucoUtils::ConvertDate(time_t newDate)
{
    tm dateStr;
    gmtime_s(&dateStr, &newDate);

    DateTime^ convertedDate = gcnew DateTime(dateStr.tm_year + 1900, dateStr.tm_mon + 1,dateStr.tm_mday);
    return convertedDate;
}


bool 
DucoUtils::CompareString (String^ lhs, const std::wstring& rhs)
{
    System::String^ newRHS = ConvertString (rhs);
    return (lhs->CompareTo(newRHS) == 0);
}

System::Void
DucoUtils::RemoveAllExceptNumbers(System::String^% idNumber)
{
    String^ originalString = idNumber;
    idNumber = "";

    CharEnumerator^ it = originalString->GetEnumerator();
    while (it->MoveNext())
    {
        if (isdigit(it->Current))
        {
            idNumber += (it->Current);
        }
    }
}

String^
DucoUtils::FormatFilename(String^ originalFilename)
{
    if (originalFilename->Length <= 0)
    {
        return originalFilename;
    }
    String^ tempFileName = Path::GetFullPath(originalFilename);
    int lastSlash = tempFileName->LastIndexOf('\\');
    if (lastSlash == -1)
        return tempFileName;

    String^ newString = tempFileName->Substring(lastSlash+1);

    return newString;
}

short
DucoUtils::ToInt(System::Windows::Forms::DataGridViewCell^ currentCell)
{
    short value = 0;
    try
    {
        if (currentCell->Value != nullptr)
        {
            Object^ obj = currentCell->Value;
            if (Type::GetTypeCode(obj->GetType()) == TypeCode::String)
            {
                String^ objStr = static_cast<String^>(obj);
                if (objStr->Length > 0)
                {
                    value = Convert::ToInt16(currentCell->Value);
                }
            }
            else
                value = Convert::ToInt16(currentCell->Value);
        }
    }
    catch (Exception^ /*Ex*/)
    {
        value = 0;
    }
    return value;
}

System::String^
DucoUtils::ConvertString(System::Windows::Forms::DataGridViewCell^ currentCell)
{
    try
    {
        if (currentCell->Value != nullptr)
        {
            Object^ obj = currentCell->Value;
            if (Type::GetTypeCode(obj->GetType()) == TypeCode::String)
            {
                String^ objStr = static_cast<String^>(obj);
                if (objStr->Length > 0)
                {
                    return objStr;
                }
            }
            else
                return Convert::ToString(obj);
        }
    }
    catch (Exception^ /*Ex*/)
    {
    }
    return "";
}

System::String^
DucoUtils::ConvertString(System::Collections::Generic::List<System::UInt64>^ idList)
{
    String^ items = "";
    System::Collections::Generic::List<System::UInt64>::Enumerator it = idList->GetEnumerator();
    while (it.MoveNext())
    {
        if (items->Length > 0)
        {
            items += ",";
        }
        items += it.Current.ToString();
    }

    return items;
}

System::String^
DucoUtils::PadWithChar(System::String^ str, System::Char paddingChar, Int16 noOfChars)
{
    Int16 decimalPointPosition = str->IndexOf('.');

    if (decimalPointPosition != -1 && decimalPointPosition < noOfChars)
    {
        String^ newChars = gcnew String(paddingChar, noOfChars - decimalPointPosition);
        return str->Insert(0, newChars);
    }
    
    return str;
}


System::Collections::Generic::List<System::UInt64>^
DucoUtils::ConvertCommaSeperatedIdStringToList(System::String^ idString)
{
    array<String^>^ ids = idString->Split(',');
    System::Collections::Generic::List<System::UInt64>^ performanceIds = gcnew System::Collections::Generic::List<System::UInt64>();

    for each(String^ idStr in ids)
    {
        UInt64 id = Convert::ToUInt64(idStr);
        performanceIds->Add(id);
    }

    return performanceIds;
}

System::Void
DucoUtils::Convert(const std::list<Duco::ObjectId>& inList, System::Collections::Generic::List<System::UInt64>^% outList)
{
    outList->Clear();
    for (const auto& objectId : inList)
    {
        outList->Add(objectId.Id());
    }
}

System::Void
DucoUtils::Convert(const std::set<Duco::ObjectId>& inList, System::Collections::Generic::List<System::UInt64>^% outList)
{
    outList->Clear();
    for (const auto& objectId : inList)
    {
        outList->Add(objectId.Id());
    }
}

System::Void
DucoUtils::Convert(System::Collections::Generic::List<System::UInt64>^% inList, std::set<Duco::ObjectId>& outList)
{
    outList.clear();
    System::Collections::Generic::List<System::UInt64>::Enumerator objectId = inList->GetEnumerator();
    while (objectId.MoveNext())
    {
        Duco::ObjectId nextId(objectId.Current);
        outList.insert(nextId);
    }
}

