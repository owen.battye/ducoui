#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"

#include "RingerDisplayCache.h"

#include "DatabaseSettings.h"
#include "RingingDatabase.h"
#include "PealDatabase.h"
#include <Ringer.h>
#include "DucoUtils.h"
#include <StatisticFilters.h>
#include "DucoUILog.h"
#include <chrono>

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::Collections::Generic;
using namespace System::Windows::Forms;
using namespace std::chrono;

Int16 MaxRingerCacheSize = 400;
Int16 MinRingerCacheSize = (Int16)(MaxRingerCacheSize * 0.25);
Int16 MaxYearsToCache = 10;
Int16 DefaultYearsToCache = 5;

RingerDisplayCache::RingerDisplayCache(DucoUI::DatabaseManager^ _database, const Duco::PerformanceDate& perfDate, System::Boolean _addBlank, System::Boolean _currentRingers)
    : database(_database), addBlank(_addBlank), currentRingers(_currentRingers), ringerSubsetDisplayEnabled(false), yearsToCache(DefaultYearsToCache)
{
    nameRenderingDate = new Duco::PerformanceDate(perfDate);
    bindingList = gcnew System::ComponentModel::BindingList<DucoUI::RingerItem^>();
}

DucoUI::RingerDisplayCache^
RingerDisplayCache::CreateDisplayCacheWithConductors(DucoUI::DatabaseManager^ database, System::Boolean addBlank)
{
    Duco::PerformanceDate perfDate;
    RingerDisplayCache^ cache = gcnew RingerDisplayCache(database, perfDate, addBlank, false);
    cache->GenerateConductorOptions();
    return cache;
}

DucoUI::RingerDisplayCache^
RingerDisplayCache::CreateDisplayCacheWithFeePayers(DucoUI::DatabaseManager^ database, System::Boolean addBlank)
{
    Duco::PerformanceDate perfDate;
    RingerDisplayCache^ cache = gcnew RingerDisplayCache(database, perfDate, addBlank, false);
    cache->GenerateFeePayersOptions(KNoId);
    return cache;
}

DucoUI::RingerDisplayCache^
RingerDisplayCache::CreateDisplayCacheWithRingers(DucoUI::DatabaseManager^ database, System::Boolean currentRingers, System::Boolean addBlank)
{
    Duco::PerformanceDate _nameRenderingDate;
    RingerDisplayCache^ cache = gcnew RingerDisplayCache(database, _nameRenderingDate, addBlank, currentRingers);
    cache->GenerateRingerOptions(_nameRenderingDate);
    return cache;
}

DucoUI::RingerDisplayCache^
RingerDisplayCache::CreateDisplayCacheWithRingers(DucoUI::DatabaseManager^ database, const Duco::PerformanceDate& _nameRenderingDate, System::Boolean currentRingers, System::Boolean addBlank)
{
    RingerDisplayCache^ cache = gcnew RingerDisplayCache(database, _nameRenderingDate, addBlank, currentRingers);
    cache->GenerateRingerOptions(_nameRenderingDate);
    return cache;
}

RingerDisplayCache::!RingerDisplayCache()
{
    delete nameRenderingDate;
}

RingerDisplayCache::~RingerDisplayCache()
{
    this->!RingerDisplayCache();
}

System::Boolean
RingerDisplayCache::NeedRegeneration(const Duco::PerformanceDate& _newDate)
{
    if (ringerSubsetDisplayEnabled && bindingList->Count != 0)
    {
        if (bindingList->Count < 25 && yearsToCache < MaxYearsToCache)
        {
            return true;
        }
        if (*nameRenderingDate == _newDate)
            return false;

        unsigned int noOfDays = _newDate.NumberOfDaysUntil(*nameRenderingDate);
        if (noOfDays < 91)
            return false;

        if (noOfDays >= 365)
        {
            DUCOUILOGDEBUG("Reset Number of years to cache");
            yearsToCache = DefaultYearsToCache;
        }
    }
    return true;
}


System::Void
RingerDisplayCache::GenerateRingerOptions(const Duco::PerformanceDate& _nameRenderingDate)
{
    if (!NeedRegeneration(_nameRenderingDate))
    {
        return;
    }

    time_point<high_resolution_clock> start = high_resolution_clock::now();
    bindingList->Clear();
    *nameRenderingDate = _nameRenderingDate;

    if (addBlank)
    {
        bindingList->Add(gcnew DucoUI::RingerItem("", KNoId));
    }

    if (currentRingers && database->NumberOfObjects(TObjectType::ERinger) > MaxRingerCacheSize)
    {
        ringerSubsetDisplayEnabled = true;
        Duco::PerformanceDate filterStartDate(_nameRenderingDate);
        filterStartDate.RemoveMonths(1 + (12 * yearsToCache));
        Duco::StatisticFilters filters(database->Database());
        filters.SetStartDate(true, filterStartDate);

        DUCOUILOGDEBUG("Ringer cache start date: " + DucoUtils::ConvertString(filters.StartDate().Str()));

        Duco::PerformanceDate filterEndDate (*nameRenderingDate);
        filterEndDate = filterEndDate.AddDays(365 * yearsToCache);
        filters.SetEndDate(true, filterEndDate);
        DUCOUILOGDEBUG("Ringer cache end date: " + DucoUtils::ConvertString(filters.EndDate().Str()));

        std::set<Duco::ObjectId> ringerIds;

        time_point<high_resolution_clock> beforeGenerateRingerIds = high_resolution_clock::now();
        database->Database().PealsDatabase().RingersAroundDates(filters, ringerIds);
        DUCOUILOGDEBUGWITHTIME(beforeGenerateRingerIds, "Time to generate ringer ids");
        Duco::ObjectId currentRingerId;
        bool ringerFound = database->FirstRinger(currentRingerId, true);
        while (ringerFound)
        {
            if (ringerIds.contains(currentRingerId))
            {
                AddRingerOption(currentRingerId.Id());
            }
            ringerFound = database->NextRinger(currentRingerId, true, true);
        }
        DUCOUILOGDEBUGWITHTIME(beforeGenerateRingerIds, "Time to populate ringer controls from time");
    }
    else
    {
        time_point<high_resolution_clock> beforePopulateControls = high_resolution_clock::now();
        Duco::ObjectId currentRingerId;
        bool ringerFound = database->FirstRinger(currentRingerId, true);
        while (ringerFound)
        {
            AddRingerOption(currentRingerId.Id());
            ringerFound = database->NextRinger(currentRingerId, true, true);
        }
        DUCOUILOGDEBUGWITHTIME(beforePopulateControls, "Time to populate controls for all ringers");
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    if (bindingList->Count > MaxRingerCacheSize && yearsToCache > 2)
    {
        yearsToCache -= 1;
        DUCOUILOGDEBUG("Decreased number of cache years to : " + yearsToCache.ToString());
    }
    else if (bindingList->Count < 50 && yearsToCache < MaxYearsToCache)
    {
        yearsToCache += 5;
        DUCOUILOGDEBUG("Increased number of cache years to : " + yearsToCache.ToString());
    }
    else if (bindingList->Count < MinRingerCacheSize && yearsToCache < MaxYearsToCache)
    {
        yearsToCache += 1;
        DUCOUILOGDEBUG("Increased number of cache years to : " + yearsToCache.ToString());
    }
    DUCOUILOGDEBUG("Number of items in ringer cache: " + bindingList->Count.ToString() + ", in " + duration.count() + "us");
}

System::Void
RingerDisplayCache::GenerateConductorOptions()
{
    bindingList->Clear();

    if (addBlank)
    {
        bindingList->Add(gcnew DucoUI::RingerItem("", KNoId));
    }

    std::set<Duco::ObjectId> conductorIds;
    database->Database().PealsDatabase().GetAllConductorIds(conductorIds);

    std::set<Duco::ObjectId>::const_iterator it = conductorIds.begin();
    while (it != conductorIds.end())
    {
        AddRingerOption(*it);
        ++it;
    }
}

System::Void
RingerDisplayCache::GenerateFeePayersOptions(const Duco::ObjectId& associationId)
{
    bindingList->Clear();

    if (addBlank)
    {
        bindingList->Add(gcnew DucoUI::RingerItem("", KNoId));
    }

    std::set<Duco::ObjectId> feePayerIds;
    database->Database().PealsDatabase().GetAllFeePayers(associationId, feePayerIds);

    std::set<Duco::ObjectId>::const_iterator it = feePayerIds.begin();
    while (it != feePayerIds.end())
    {
        AddRingerOption(*it);
        ++it;
    }
}

System::Void
RingerDisplayCache::PopulateControl(System::Windows::Forms::ComboBox^ control, System::Boolean setDefaultRinger)
{
    time_point<high_resolution_clock> start = high_resolution_clock::now();
    control->BeginUpdate();
    PopulateControl((System::Windows::Forms::ListControl^)control, setDefaultRinger);
    control->EndUpdate();
    DUCOUILOGDEBUGWITHTIME(start, "RingerDisplayCache::PopulateControl 1 took");
}

System::Void
RingerDisplayCache::PopulateControl(System::Windows::Forms::ListBox^ control, System::Boolean setDefaultRinger)
{
    time_point<high_resolution_clock> start = high_resolution_clock::now();
    control->BeginUpdate();
    PopulateControl((System::Windows::Forms::ListControl^)control, setDefaultRinger);
    control->EndUpdate();
    DUCOUILOGDEBUGWITHTIME(start, "RingerDisplayCache::PopulateControl 2 took");
}


System::Void
RingerDisplayCache::PopulateControl(System::Windows::Forms::ListControl^ toControl, System::Boolean setDefaultRinger)
{
    time_point<high_resolution_clock> start = high_resolution_clock::now();
    toControl->Text = "";
    toControl->SelectedIndex = -1;

    BindingSource^ binding = gcnew BindingSource();
    binding->DataSource = bindingList;
    toControl->DataSource = binding;
    if (setDefaultRinger)
    {
        ObjectId defaultRingerId = database->Settings().DefaultRinger();
        System::Int32 defaultRingerIndex = FindIndexOrAdd(defaultRingerId);
        if (defaultRingerIndex != -1)
        {
            toControl->SelectedIndex = defaultRingerIndex;
        }
    }
    DUCOUILOGDEBUGWITHTIME(start, "RingerDisplayCache::PopulateControl 3 took");
}

System::Void
RingerDisplayCache::RepopulateControl(System::Windows::Forms::ComboBox^ toControl, Duco::ObjectId& ringerId)
{
    BindingSource^ binding = gcnew BindingSource();
    binding->DataSource = bindingList;
    toControl->DataSource = binding;
    System::Int32 ringerIndex = FindIndexOrAdd(ringerId);
    toControl->SelectedIndex = ringerIndex;
}

std::set<Duco::ObjectId>
RingerDisplayCache::FindIds(System::Windows::Forms::ListBox::SelectedObjectCollection^ indexes)
{
    std::set<Duco::ObjectId> ids;
    System::Collections::IEnumerator^ it = indexes->GetEnumerator();
    while (it->MoveNext())
    {
        RingerItem^ ringer = (RingerItem^)it->Current;
        ids.insert(ringer->Id());
    }

    return ids;
}

System::Int32
RingerDisplayCache::FindIndexOrAdd(const Duco::ObjectId& id)
{
    for (int i = 0; i < bindingList->Count; ++i)
    {
        if (bindingList[i]->Id() == id.Id())
        {
            return i;
        }
    }
    // Ringer not found, must have been filtered for performance, so add it
    if (AddRingerOption(id) != UInt32::MaxValue)
    {
        return bindingList->Count - 1;
    }
    return -1;
}

System::Int32
RingerDisplayCache::AddRingerOption(const Duco::ObjectId& ringerId)
{
    if (ringerId.ValidId())
    {
        time_point<high_resolution_clock> start = high_resolution_clock::now();
        System::String^ ringerName = database->RingerName(ringerId, *nameRenderingDate);
        DucoUI::RingerItem^ newItem = gcnew DucoUI::RingerItem(ringerName, ringerId.Id());
        bindingList->Add(newItem);
        DUCOUILOGDEBUGWITHTIME(start, "Adding ringer took");
        return bindingList->IndexOf(newItem);
    }
    return Int32::MaxValue;
}

System::Boolean
RingerDisplayCache::UpdateRingerOption(const Duco::ObjectId& ringerId)
{
    System::Int32 index = FindIndexOrAdd(ringerId);
    if (index != -1)
    {
        System::String^ ringerName = database->RingerName(ringerId, *nameRenderingDate);
        DucoUI::RingerItem^ replacementItem = gcnew DucoUI::RingerItem(ringerName, ringerId.Id());
        bindingList[index]->SetDisplayName(ringerName);
        return true;
    }

    return false;
}

System::Boolean
RingerDisplayCache::RemoveRingerOption(const Duco::ObjectId& ringerId)
{
    System::Int32 index = FindIndexOrAdd(ringerId);
    if (index != -1)
    {
        bindingList->RemoveAt(index);
        return true;
    }
    return false;
}

System::Boolean
RingerDisplayCache::RingerSubsetDisplayEnabled()
{
    return ringerSubsetDisplayEnabled;
}

RingerItem::RingerItem(System::String^ _displayName, System::UInt32 _id)
    : displayName(_displayName), id(_id)
{

}

System::String^
RingerItem::ToString()
{
    return DisplayName();
}

System::String^
RingerItem::DisplayName()
{
    return displayName;
}

System::Void
RingerItem::SetDisplayName(System::String^ newName)
{
    displayName = newName;
}

System::UInt32
RingerItem::Id()
{
    return id;
}
