#include "PrintMethodSeriesUtil.h"

#include "DucoUtils.h"
#include "DucoCommon.h"
#include <MethodSeries.h>
#include <Method.h>
#include "DatabaseManager.h"
#include "LongMethodLineDrawer.h"

using namespace System;
using namespace System::Collections::Generic;
using namespace System::Drawing;
using namespace Duco;
using namespace DucoUI;
using namespace System::Drawing;
using namespace System::Drawing::Imaging;

PrintMethodSeriesUtil::PrintMethodSeriesUtil(DatabaseManager^ theDatabase, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
:   PrintUtilBase(theDatabase, 12, printArgs, false)
{

}

void
PrintMethodSeriesUtil::SetObjects(const Duco::MethodSeries* theMethodSeries)
{
    currentSeries = theMethodSeries;
}

System::Void
PrintMethodSeriesUtil::printObject(System::Drawing::Printing::PrintPageEventArgs^ args, System::Drawing::Printing::PrinterSettings^ printSettings)
{
    // Print title of method.
    String^ printBuffer = DucoUtils::ConvertString(currentSeries->Name());
    args->Graphics->DrawString(printBuffer, boldFont, Brushes::Black, leftMargin, yPos, gcnew StringFormat );

    Bitmap^ allMethodsImage = gcnew Bitmap(args->MarginBounds.Width, args->MarginBounds.Height-int(3*lineHeight));

    Graphics^ allMethodsGraphics = Graphics::FromImage(allMethodsImage);
    Font^ methodNameFont = gcnew Font(KGenericMethodFont, 5, FontStyle::Regular, GraphicsUnit::Point);
    float methodNameLineHeight = methodNameFont->GetHeight(allMethodsGraphics);

    float additionalXPosWrapOffset = 0.0F;
    float additionalNameXPosWrapOffset = 0.0F;

    std::set<Duco::ObjectId>::const_iterator methodId = currentSeries->Methods().begin();
    while (methodId != currentSeries->Methods().end())
    {
        Method nextMethod;
        if (database->FindMethod(*methodId, nextMethod, false))
        {
            LongMethodLineDrawer^ lineDrawer = gcnew LongMethodLineDrawer(allMethodsGraphics, nextMethod, database, nullptr);
            lineDrawer->SetSecondHalfOnSecondLine(false);
            lineDrawer->SetPrintCallings(false);
            lineDrawer->ShiftLineRight(additionalXPosWrapOffset);
            lineDrawer->StartDrawing(nextMethod.PrintFromBell());

            float methodWidth = (KPositionGap * float(nextMethod.Order())) + KPositionOffset;
            //Print method name
            String^ methodName = DucoUtils::ConvertString(nextMethod.ShortName(database->Database()));
            RectangleF methodNameBox(float(args->MarginBounds.Left)+additionalXPosWrapOffset+additionalNameXPosWrapOffset, float(args->MarginBounds.Top)+(2*lineHeight), methodWidth+(KPositionGap/2), methodNameLineHeight);
            args->Graphics->DrawString(methodName, methodNameFont, Brushes::Gray, methodNameBox);
            //Shift next method right
            additionalXPosWrapOffset += methodWidth;
            additionalNameXPosWrapOffset += KPositionGap/2;
        }
        ++methodId;
    }

    args->Graphics->DrawImage(allMethodsImage, float(args->MarginBounds.Left), float(args->MarginBounds.Top)+(2*lineHeight)+methodNameLineHeight);
    args->Graphics->Flush(System::Drawing::Drawing2D::FlushIntention::Flush);

    // Add Duco footer.
    addDucoFooter(smallFont, gcnew StringFormat, args);
}

