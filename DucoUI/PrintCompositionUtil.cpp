#include "PrintCompositionUtil.h"
#include <LeadEndCollection.h>
#include <Composition.h>
#include "DucoUtils.h"
#include "DatabaseManager.h"
#include "WindowsSettings.h"
#include <LeadEnd.h>

using namespace System;
using namespace System::Drawing;
using namespace Duco;
using namespace DucoUI;

const float KColumnGap = 10;
const float KLineSpace = 3;
const float KLeadEndSpacing = 30;

PrintCompositionUtil::PrintCompositionUtil(DatabaseManager^ theDatabase, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
:	PrintUtilBase(theDatabase, 12, printArgs, false), leadEndWidth(0)
{
    courseEndPen = gcnew System::Drawing::Pen(Color::Gray);
    partEndPen = gcnew System::Drawing::Pen(Color::Black);
}

PrintCompositionUtil::!PrintCompositionUtil()
{
    delete leadEnds;
}

PrintCompositionUtil::~PrintCompositionUtil()
{
    this->!PrintCompositionUtil();
}

void
PrintCompositionUtil::SetObjects(Duco::Composition* theComposition, System::Windows::Forms::DataGridView^ theDataGridView)
{
    currentComposition = theComposition;
    dataGridView = theDataGridView;
    leadEnds = new Duco::LeadEndCollection();
}

System::Void
PrintCompositionUtil::printObject(System::Drawing::Printing::PrintPageEventArgs^ args, System::Drawing::Printing::PrinterSettings^ printSettings)
{
    if (leadEnds->NoOfLeads() <= 1)
    {
        PrintFirstPage(args);
    }
    PrintLeadEnds(args);
}

System::Void
PrintCompositionUtil::PrintFirstPage(System::Drawing::Printing::PrintPageEventArgs^ args)
{
    if (!currentComposition->Proven())
    {
        ProgressCallbackWrapper wrapper (this);
        currentComposition->Prove(wrapper, database->WindowsSettings()->InstallationDir());
    }
    (*leadEnds) = currentComposition->LeadEnds();

    std::wstring title;
    std::wstring composer;
    currentComposition->FullName(database->Database(), title, composer);
    PrintLongString(DucoUtils::ConvertString(title), boldFont, args);
    PrintLongString(DucoUtils::ConvertString(composer), normalFont, args);
    addDucoFooter(smallFont, gcnew StringFormat, args);

    // Calculate column widths
    std::list<float> columnWidths;
    for (int columnNo(0); columnNo < dataGridView->Columns->Count; ++columnNo)
    {
        float stringWidth (0);
        for (int rowNo(0); rowNo < dataGridView->Rows->Count; ++rowNo)
        {
            if (dataGridView[columnNo, rowNo]->Value != nullptr)
            {
                String^ columnStr = dataGridView[columnNo, rowNo]->Value->ToString();
                SizeF stringSize = args->Graphics->MeasureString(columnStr, normalFont, args->PageBounds.Width);
                stringWidth = Math::Max(stringWidth, stringSize.Width);
            }
        }
        columnWidths.push_back(stringWidth);
    }

    // Print column headers
    std::list<float>::const_iterator it = columnWidths.begin();
    float xPosition (leftMargin);
    float xLineEndPosition (leftMargin);
    for (int columnNo(0); columnNo < dataGridView->Columns->Count; ++columnNo)
    {
        if (it != columnWidths.end())
        {
            if (*it > 0 && dataGridView->Columns[columnNo]->HeaderCell->Value != nullptr)
            {
                String^ compositionData = dataGridView->Columns[columnNo]->HeaderCell->Value->ToString();
                args->Graphics->DrawString(compositionData, normalFont, Brushes::Black, xPosition, yPos, gcnew StringFormat() );

                xPosition += *it;
                xPosition += KColumnGap;
                xLineEndPosition = Math::Max(xLineEndPosition, xPosition);
            }
            ++it;
        }
    }
    --noOfLines;
    args->Graphics->DrawLine(partEndPen, leftMargin, yPos-KLineSpace+lineHeight, xLineEndPosition, yPos-KLineSpace+lineHeight);

    // Print composition data
    for (int rowNo(0); rowNo < dataGridView->Rows->Count; ++rowNo)
    {
        yPos += lineHeight;
        it = columnWidths.begin();
        xPosition = leftMargin;
        for (int columnNo(0); columnNo < dataGridView->Columns->Count; ++columnNo)
        {
            if (it != columnWidths.end())
            {
                if (*it > 0 && dataGridView[columnNo, rowNo]->Value != nullptr)
                {
                    String^ compositionData = dataGridView[columnNo, rowNo]->Value->ToString();
                    args->Graphics->DrawString(compositionData, normalFont, Brushes::Black, xPosition, yPos, gcnew StringFormat() );

                    xPosition += *it;
                    xPosition += KColumnGap;
                }
                ++it;
            }
        }
        --noOfLines;
    }

    // Print repeats
    if (currentComposition->Repeats() > 0)
    {
        String^ repeatsString = "";
        switch (currentComposition->Repeats())
        {
        case 1:
            repeatsString += "Once";
            break;
        case 2:
            repeatsString += "Twice";
            break;
        default:
            repeatsString = Convert::ToString(currentComposition->Repeats());
            repeatsString += " times";
            break;
        }
        repeatsString += " repeated";
        args->Graphics->DrawString(repeatsString, smallFont, Brushes::Black, leftMargin, yPos, gcnew StringFormat() );
        yPos += lineHeight;
    }
    leadEnds->Start();
}

System::Void
PrintCompositionUtil::PrintLeadEnds(System::Drawing::Printing::PrintPageEventArgs^ args)
{
    unsigned int originalNoOfLines (noOfLines);
    float        originalYPos (yPos);
    while (leadEnds->MoreLeadEnds())
    {
        yPos += lineHeight;

        const Duco::LeadEnd& nextLeadEnd = leadEnds->LeadEnd();

        String^ printBuffer = DucoUtils::ConvertString(nextLeadEnd.Str());
        SizeF stringSize = args->Graphics->MeasureString(printBuffer, normalFont, args->PageBounds.Width);
        leadEndWidth = Math::Max(leadEndWidth, stringSize.Width);
        if (nextLeadEnd.LeadType() == EBobLead)
        {
            printBuffer += " -";
        }
        else if (nextLeadEnd.LeadType() == ESingleLead)
        {
            printBuffer += " s";
        }

        if ( (leftMargin + stringSize.Width + KLeadEndSpacing) <= args->PageBounds.Right)
        {
            args->Graphics->DrawString(printBuffer, normalFont, Brushes::Black, leftMargin, yPos, gcnew StringFormat );
            if (nextLeadEnd.PartEnd())
            {
                args->Graphics->DrawLine(partEndPen, leftMargin, yPos+stringSize.Height-KLineSpace, leftMargin+stringSize.Width, yPos+stringSize.Height-KLineSpace);
            }
            else if (nextLeadEnd.CourseEnd())
            {
                args->Graphics->DrawLine(courseEndPen, leftMargin, yPos+stringSize.Height-KLineSpace, leftMargin+stringSize.Width, yPos+stringSize.Height-KLineSpace);
            }

            ++printingResultNumber;
            --noOfLines;

            if (noOfLines == 0)
            {
                noOfLines = originalNoOfLines;
                yPos = originalYPos;
                leftMargin += leadEndWidth;
                leftMargin += KLeadEndSpacing;
            }
        }
        else
        {
            ResetPosition(args);
            leftMargin = (float)args->MarginBounds.Left;
            args->HasMorePages = true;
            break;
        }
    }
}

void
PrintCompositionUtil::Initialised()
{

}

void
PrintCompositionUtil::Step(int progressPercent)
{

}

void
PrintCompositionUtil::Complete()
{

}
