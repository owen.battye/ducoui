#pragma once

namespace DucoUI {
	public ref class AssociationReportOptions : public System::Windows::Forms::Form
	{
	public:
        AssociationReportOptions(DucoUI::DatabaseManager^ newDatabase);

	protected:
        !AssociationReportOptions();
        ~AssociationReportOptions();

		System::Void SetDefaults();

		System::Void closeBtn_Click(System::Object^ sender, System::EventArgs^ e);
		System::Void printBtn_Click(System::Object^ sender, System::EventArgs^ e);
		System::Void printPreviewBtn_Click(System::Object^ sender, System::EventArgs^ e);
		System::Void pageSetupBtn_Click(System::Object^ sender, System::EventArgs^ e);
		System::Void fieldsUpdated(System::Object^ sender, System::EventArgs^ e);
		System::Void numberingStart_Validating(System::Object^ sender, System::ComponentModel::CancelEventArgs^ e);
		System::Void numberingStart_Validated(Object^ sender, System::EventArgs^ e);

		System::Void printAssociationReport(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args);
        System::Void clearReport(System::Object^ sender, System::Drawing::Printing::PrintEventArgs^ args);
		System::Void AssociationReportOptions_FormClosing(System::Object^ sender, System::Windows::Forms::FormClosingEventArgs^ e);

		System::Windows::Forms::ToolStripStatusLabel^ statusLabel;
		System::Windows::Forms::TextBox^ numberingStart;
		System::Windows::Forms::ListBox^ reportYear;
		System::Windows::Forms::TextBox^ extraPealIds;

	private:
		System::ComponentModel::IContainer^ components;
	protected:
		DucoUI::PrintAssociationReportUtil^				printAssociationReportUtil;
		DucoUI::DatabaseManager^						database;
		System::Windows::Forms::ErrorProvider^			errorProvider1;
		System::Windows::Forms::NumericUpDown^			fontSize;
		std::set<Duco::ObjectId>*						additionalPealIds;
		System::Windows::Forms::NumericUpDown^			columnCount;
        System::Drawing::Printing::PrintDocument^       document;
        System::Windows::Forms::NumericUpDown^          columnSpacing;
        System::Windows::Forms::ComboBox^               fontSelector;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
            this->components = (gcnew System::ComponentModel::Container());
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
            System::Windows::Forms::Button^ closeBtn;
            System::Windows::Forms::Button^ previewBtn;
            System::Windows::Forms::Button^ printBtn;
            System::Windows::Forms::GroupBox^ groupBox1;
            System::Windows::Forms::Label^ fontLbl;
            System::Windows::Forms::Label^ columSpacingLabel;
            System::Windows::Forms::Label^ fontSizeLabel;
            System::Windows::Forms::Label^ NoOfColumnsLabel;
            System::Windows::Forms::GroupBox^ groupBox2;
            System::Windows::Forms::Label^ yearLabel;
            System::Windows::Forms::Label^ numberingLabel;
            System::Windows::Forms::Label^ extraIdsLabel;
            System::Windows::Forms::ToolTip^ toolTip1;
            System::Windows::Forms::StatusStrip^ statusStrip1;
            System::Windows::Forms::Button^ pageSetupButton;
            System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(AssociationReportOptions::typeid));
            this->fontSelector = (gcnew System::Windows::Forms::ComboBox());
            this->columnSpacing = (gcnew System::Windows::Forms::NumericUpDown());
            this->fontSize = (gcnew System::Windows::Forms::NumericUpDown());
            this->columnCount = (gcnew System::Windows::Forms::NumericUpDown());
            this->reportYear = (gcnew System::Windows::Forms::ListBox());
            this->numberingStart = (gcnew System::Windows::Forms::TextBox());
            this->extraPealIds = (gcnew System::Windows::Forms::TextBox());
            this->statusLabel = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->errorProvider1 = (gcnew System::Windows::Forms::ErrorProvider(this->components));
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            closeBtn = (gcnew System::Windows::Forms::Button());
            previewBtn = (gcnew System::Windows::Forms::Button());
            printBtn = (gcnew System::Windows::Forms::Button());
            groupBox1 = (gcnew System::Windows::Forms::GroupBox());
            fontLbl = (gcnew System::Windows::Forms::Label());
            columSpacingLabel = (gcnew System::Windows::Forms::Label());
            fontSizeLabel = (gcnew System::Windows::Forms::Label());
            NoOfColumnsLabel = (gcnew System::Windows::Forms::Label());
            groupBox2 = (gcnew System::Windows::Forms::GroupBox());
            yearLabel = (gcnew System::Windows::Forms::Label());
            numberingLabel = (gcnew System::Windows::Forms::Label());
            extraIdsLabel = (gcnew System::Windows::Forms::Label());
            toolTip1 = (gcnew System::Windows::Forms::ToolTip(this->components));
            statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            pageSetupButton = (gcnew System::Windows::Forms::Button());
            tableLayoutPanel1->SuspendLayout();
            groupBox1->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->columnSpacing))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->fontSize))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->columnCount))->BeginInit();
            groupBox2->SuspendLayout();
            statusStrip1->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->errorProvider1))->BeginInit();
            this->SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 4;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->Controls->Add(closeBtn, 3, 3);
            tableLayoutPanel1->Controls->Add(previewBtn, 0, 3);
            tableLayoutPanel1->Controls->Add(printBtn, 1, 3);
            tableLayoutPanel1->Controls->Add(groupBox1, 0, 1);
            tableLayoutPanel1->Controls->Add(groupBox2, 0, 0);
            tableLayoutPanel1->Controls->Add(pageSetupButton, 2, 3);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->Padding = System::Windows::Forms::Padding(0, 0, 0, 21);
            tableLayoutPanel1->RowCount = 4;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->Size = System::Drawing::Size(519, 247);
            tableLayoutPanel1->TabIndex = 0;
            // 
            // closeBtn
            // 
            closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            closeBtn->CausesValidation = false;
            closeBtn->Location = System::Drawing::Point(441, 200);
            closeBtn->Name = L"closeBtn";
            closeBtn->Size = System::Drawing::Size(75, 23);
            closeBtn->TabIndex = 6;
            closeBtn->Text = L"Close";
            closeBtn->UseVisualStyleBackColor = true;
            closeBtn->Click += gcnew System::EventHandler(this, &AssociationReportOptions::closeBtn_Click);
            // 
            // previewBtn
            // 
            previewBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            previewBtn->Location = System::Drawing::Point(3, 200);
            previewBtn->Name = L"previewBtn";
            previewBtn->Size = System::Drawing::Size(75, 23);
            previewBtn->TabIndex = 3;
            previewBtn->Text = L"Preview";
            previewBtn->UseVisualStyleBackColor = true;
            previewBtn->Click += gcnew System::EventHandler(this, &AssociationReportOptions::printPreviewBtn_Click);
            // 
            // printBtn
            // 
            printBtn->Location = System::Drawing::Point(84, 200);
            printBtn->Name = L"printBtn";
            printBtn->Size = System::Drawing::Size(75, 23);
            printBtn->TabIndex = 4;
            printBtn->Text = L"Print";
            printBtn->UseVisualStyleBackColor = true;
            printBtn->Click += gcnew System::EventHandler(this, &AssociationReportOptions::printBtn_Click);
            // 
            // groupBox1
            // 
            groupBox1->AutoSize = true;
            groupBox1->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            tableLayoutPanel1->SetColumnSpan(groupBox1, 4);
            groupBox1->Controls->Add(fontLbl);
            groupBox1->Controls->Add(this->fontSelector);
            groupBox1->Controls->Add(this->columnSpacing);
            groupBox1->Controls->Add(columSpacingLabel);
            groupBox1->Controls->Add(fontSizeLabel);
            groupBox1->Controls->Add(this->fontSize);
            groupBox1->Controls->Add(NoOfColumnsLabel);
            groupBox1->Controls->Add(this->columnCount);
            groupBox1->Dock = System::Windows::Forms::DockStyle::Fill;
            groupBox1->Location = System::Drawing::Point(3, 109);
            groupBox1->Name = L"groupBox1";
            groupBox1->Size = System::Drawing::Size(513, 85);
            groupBox1->TabIndex = 2;
            groupBox1->TabStop = false;
            groupBox1->Text = L"Settings";
            // 
            // fontLbl
            // 
            fontLbl->AutoSize = true;
            fontLbl->Location = System::Drawing::Point(30, 48);
            fontLbl->Name = L"fontLbl";
            fontLbl->Size = System::Drawing::Size(31, 13);
            fontLbl->TabIndex = 14;
            fontLbl->Text = L"Font:";
            fontLbl->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // fontSelector
            // 
            this->fontSelector->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Append;
            this->fontSelector->FormattingEnabled = true;
            this->fontSelector->Location = System::Drawing::Point(67, 45);
            this->fontSelector->Name = L"fontSelector";
            this->fontSelector->Size = System::Drawing::Size(190, 21);
            this->fontSelector->TabIndex = 3;
            // 
            // columnSpacing
            // 
            this->columnSpacing->Location = System::Drawing::Point(355, 45);
            this->columnSpacing->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
            this->columnSpacing->Name = L"columnSpacing";
            this->columnSpacing->Size = System::Drawing::Size(55, 20);
            this->columnSpacing->TabIndex = 4;
            this->columnSpacing->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 6, 0, 0, 0 });
            // 
            // columSpacingLabel
            // 
            columSpacingLabel->AutoSize = true;
            columSpacingLabel->Location = System::Drawing::Point(264, 47);
            columSpacingLabel->Name = L"columSpacingLabel";
            columSpacingLabel->Size = System::Drawing::Size(85, 13);
            columSpacingLabel->TabIndex = 11;
            columSpacingLabel->Text = L"Column spacing:";
            // 
            // fontSizeLabel
            // 
            fontSizeLabel->AutoSize = true;
            fontSizeLabel->Location = System::Drawing::Point(9, 21);
            fontSizeLabel->Name = L"fontSizeLabel";
            fontSizeLabel->Size = System::Drawing::Size(52, 13);
            fontSizeLabel->TabIndex = 8;
            fontSizeLabel->Text = L"Font size:";
            fontSizeLabel->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // fontSize
            // 
            this->fontSize->DecimalPlaces = 1;
            this->fontSize->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 5, 0, 0, 65536 });
            this->fontSize->Location = System::Drawing::Point(67, 19);
            this->fontSize->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 16, 0, 0, 0 });
            this->fontSize->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
            this->fontSize->Name = L"fontSize";
            this->fontSize->Size = System::Drawing::Size(45, 20);
            this->fontSize->TabIndex = 1;
            toolTip1->SetToolTip(this->fontSize, L"Report font size");
            this->fontSize->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 110, 0, 0, 65536 });
            // 
            // NoOfColumnsLabel
            // 
            NoOfColumnsLabel->AutoSize = true;
            NoOfColumnsLabel->Location = System::Drawing::Point(268, 21);
            NoOfColumnsLabel->Name = L"NoOfColumnsLabel";
            NoOfColumnsLabel->Size = System::Drawing::Size(81, 13);
            NoOfColumnsLabel->TabIndex = 9;
            NoOfColumnsLabel->Text = L"No. of columns:";
            NoOfColumnsLabel->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // columnCount
            // 
            this->columnCount->Location = System::Drawing::Point(355, 19);
            this->columnCount->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 4, 0, 0, 0 });
            this->columnCount->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
            this->columnCount->Name = L"columnCount";
            this->columnCount->Size = System::Drawing::Size(52, 20);
            this->columnCount->TabIndex = 2;
            this->columnCount->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 3, 0, 0, 0 });
            // 
            // groupBox2
            // 
            tableLayoutPanel1->SetColumnSpan(groupBox2, 4);
            groupBox2->Controls->Add(yearLabel);
            groupBox2->Controls->Add(this->reportYear);
            groupBox2->Controls->Add(this->numberingStart);
            groupBox2->Controls->Add(numberingLabel);
            groupBox2->Controls->Add(extraIdsLabel);
            groupBox2->Controls->Add(this->extraPealIds);
            groupBox2->Dock = System::Windows::Forms::DockStyle::Fill;
            groupBox2->Location = System::Drawing::Point(3, 3);
            groupBox2->Name = L"groupBox2";
            groupBox2->Size = System::Drawing::Size(513, 100);
            groupBox2->TabIndex = 1;
            groupBox2->TabStop = false;
            groupBox2->Text = L"Data selection";
            // 
            // yearLabel
            // 
            yearLabel->AutoSize = true;
            yearLabel->Location = System::Drawing::Point(9, 21);
            yearLabel->Name = L"yearLabel";
            yearLabel->Size = System::Drawing::Size(65, 13);
            yearLabel->TabIndex = 3;
            yearLabel->Text = L"Report year:";
            yearLabel->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // reportYear
            // 
            this->reportYear->FormatString = L"N0";
            this->reportYear->FormattingEnabled = true;
            this->reportYear->Location = System::Drawing::Point(80, 16);
            this->reportYear->Name = L"reportYear";
            this->reportYear->Size = System::Drawing::Size(75, 69);
            this->reportYear->TabIndex = 1;
            toolTip1->SetToolTip(this->reportYear, L"Enter the year for the report - the entire year will be printed");
            this->reportYear->SelectedIndexChanged += gcnew System::EventHandler(this, &AssociationReportOptions::fieldsUpdated);
            // 
            // numberingStart
            // 
            this->numberingStart->Location = System::Drawing::Point(263, 18);
            this->numberingStart->Margin = System::Windows::Forms::Padding(3, 9, 20, 3);
            this->numberingStart->Name = L"numberingStart";
            this->numberingStart->Size = System::Drawing::Size(58, 20);
            this->numberingStart->TabIndex = 2;
            toolTip1->SetToolTip(this->numberingStart, L"For peals in the year chosen, start the peal numbering at this number, if empty u"
                L"ses the peal number in the database.");
            this->numberingStart->Validating += gcnew System::ComponentModel::CancelEventHandler(this, &AssociationReportOptions::numberingStart_Validating);
            this->numberingStart->Validated += gcnew System::EventHandler(this, &AssociationReportOptions::numberingStart_Validated);
            // 
            // numberingLabel
            // 
            numberingLabel->AutoSize = true;
            numberingLabel->Location = System::Drawing::Point(161, 21);
            numberingLabel->Name = L"numberingLabel";
            numberingLabel->Size = System::Drawing::Size(96, 13);
            numberingLabel->TabIndex = 6;
            numberingLabel->Text = L"Start numbering at:";
            numberingLabel->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // extraIdsLabel
            // 
            extraIdsLabel->AutoSize = true;
            extraIdsLabel->Location = System::Drawing::Point(184, 65);
            extraIdsLabel->Name = L"extraIdsLabel";
            extraIdsLabel->Padding = System::Windows::Forms::Padding(0, 4, 0, 0);
            extraIdsLabel->Size = System::Drawing::Size(73, 17);
            extraIdsLabel->TabIndex = 4;
            extraIdsLabel->Text = L"Extra peal ids:";
            extraIdsLabel->TextAlign = System::Drawing::ContentAlignment::TopRight;
            toolTip1->SetToolTip(extraIdsLabel, L"List the extra peals that need to be printed in addition to the year above. Seper"
                L"ate each peal id with a comma.");
            // 
            // extraPealIds
            // 
            this->extraPealIds->Location = System::Drawing::Point(263, 65);
            this->extraPealIds->Name = L"extraPealIds";
            this->extraPealIds->Size = System::Drawing::Size(242, 20);
            this->extraPealIds->TabIndex = 3;
            toolTip1->SetToolTip(this->extraPealIds, L"Extra peals to be printed in the report. They\'ll be printed without IDs and they\'"
                L"ll be printed in date order. List them with a comma between each id.");
            this->extraPealIds->TextChanged += gcnew System::EventHandler(this, &AssociationReportOptions::fieldsUpdated);
            // 
            // statusStrip1
            // 
            statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->statusLabel });
            statusStrip1->Location = System::Drawing::Point(0, 225);
            statusStrip1->Name = L"statusStrip1";
            statusStrip1->Size = System::Drawing::Size(519, 22);
            statusStrip1->TabIndex = 1;
            statusStrip1->Text = L"statusStrip1";
            // 
            // statusLabel
            // 
            this->statusLabel->Name = L"statusLabel";
            this->statusLabel->Size = System::Drawing::Size(0, 17);
            // 
            // errorProvider1
            // 
            this->errorProvider1->ContainerControl = this;
            // 
            // pageSetupButton
            // 
            pageSetupButton->Location = System::Drawing::Point(360, 200);
            pageSetupButton->Name = L"pageSetupButton";
            pageSetupButton->Size = System::Drawing::Size(75, 23);
            pageSetupButton->TabIndex = 5;
            pageSetupButton->Text = L"Page Setup";
            pageSetupButton->UseVisualStyleBackColor = true;
            pageSetupButton->Click += gcnew System::EventHandler(this, &AssociationReportOptions::pageSetupBtn_Click);
            // 
            // AssociationReportOptions
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(519, 247);
            this->Controls->Add(statusStrip1);
            this->Controls->Add(tableLayoutPanel1);
            this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
            this->Name = L"AssociationReportOptions";
            this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
            this->Text = L"Association report options";
            this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &AssociationReportOptions::AssociationReportOptions_FormClosing);
            tableLayoutPanel1->ResumeLayout(false);
            tableLayoutPanel1->PerformLayout();
            groupBox1->ResumeLayout(false);
            groupBox1->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->columnSpacing))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->fontSize))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->columnCount))->EndInit();
            groupBox2->ResumeLayout(false);
            groupBox2->PerformLayout();
            statusStrip1->ResumeLayout(false);
            statusStrip1->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->errorProvider1))->EndInit();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion
	};
}
