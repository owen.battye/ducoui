#include "PrintUtilBase.h"
#include "DucoCommon.h"
#include "DucoUtils.h"

using namespace System;
using namespace System::Drawing;
using namespace System::Drawing::Printing;
using namespace System::Windows::Forms;
using namespace DucoUI;

PrintUtilBase::PrintUtilBase(DatabaseManager^ theDatabase, float fontSize, System::Drawing::Printing::PrintPageEventArgs^ newArgs, bool newRotateHeaderCells)
    : database(theDatabase), maxHeaderRowHeight(0), printingResultNumber(0), rotateHeaderCells(newRotateHeaderCells), printThisPage(true)
{
    SetFont(KDefaultPrintFont, fontSize, newArgs);
    leftMargin = (float)newArgs->MarginBounds.Left;
    ResetPosition(newArgs);
}

System::Void
PrintUtilBase::SetFont(System::String^ fontName, float fontSize, System::Drawing::Printing::PrintPageEventArgs^ newArgs)
{
    smallFont = gcnew System::Drawing::Font(fontName, fontSize - 2);
    normalFont = gcnew System::Drawing::Font(fontName, fontSize);
    largeFont = gcnew Font(normalFont->FontFamily, fontSize + 2, FontStyle::Bold);
    boldFont = gcnew System::Drawing::Font(fontName, Math::Ceiling(fontSize), FontStyle::Bold);
    lineHeight = boldFont->GetHeight(newArgs->Graphics);
    smallLineHeight = smallFont->GetHeight(newArgs->Graphics);
}

System::Void
PrintUtilBase::ResetPosition(System::Drawing::Printing::PrintPageEventArgs^ newArgs)
{
    noOfLines = unsigned int(newArgs->MarginBounds.Height / lineHeight);
    yPos = (float)newArgs->MarginBounds.Top;
}

System::Void
PrintUtilBase::PrintStringCentrally(System::String^ printBuffer, System::Drawing::Font^ font, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
{
    StringFormat^ stringToPrintFormat = gcnew StringFormat();
    SizeF stringSize = printArgs->Graphics->MeasureString(printBuffer, font, printArgs->PageBounds.Width);
    float xOffSet = (printArgs->PageBounds.Width - stringSize.Width) / float(2);
    if (printThisPage)
    {
        printArgs->Graphics->DrawString(printBuffer, font, Brushes::Black, xOffSet, yPos, stringToPrintFormat);
    }
}

System::Void
PrintUtilBase::PrintLongString(const std::wstring& stringToPrint, System::Drawing::Font^ font, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
{
    String^ printBuffer = DucoUtils::ConvertString(stringToPrint);
    PrintLongString(printBuffer, font, printArgs);
}

System::Void
PrintUtilBase::PrintLongString(System::String^ stringToPrint, System::Drawing::Font^ font, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
{
    PrintLongString(stringToPrint, font, float(printArgs->MarginBounds.Left), printArgs);
}

System::Void
PrintUtilBase::PrintLongString(System::String^ stringToPrint, System::Drawing::Font^ font, float leftPosition, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
{
    float maxStringHeight (300);
    RectangleF drawInside (leftPosition, yPos, float(printArgs->MarginBounds.Width), maxStringHeight);
    if (printThisPage)
    {
        printArgs->Graphics->DrawString(stringToPrint, font, Brushes::Black, drawInside, gcnew StringFormat);
    }
    int linesFilled (0);
    int charactersFitted (0);
    SizeF footnoteStrSize = printArgs->Graphics->MeasureString(stringToPrint, font, drawInside.Size, gcnew StringFormat, charactersFitted, linesFilled);
    yPos += footnoteStrSize.Height;
    noOfLines -= linesFilled;
}

System::Void
PrintUtilBase::PrintLongString(System::String^ stringToPrint, System::Drawing::StringFormat^ stringFormat, System::Drawing::Font^ font, float leftPosition, float maxWidth, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
{
    float maxStringHeight(1000);
    RectangleF drawInside(leftPosition, yPos, maxWidth, maxStringHeight);
    if (printThisPage)
    {
        printArgs->Graphics->DrawString(stringToPrint, font, Brushes::Black, drawInside, stringFormat);
    }
    int linesFilled (0);
    int charactersFitted (0);
    SizeF longStrSize = printArgs->Graphics->MeasureString(stringToPrint, font, drawInside.Size, stringFormat, charactersFitted, linesFilled);
    yPos += longStrSize.Height;
    noOfLines -= linesFilled;
}

unsigned int
PrintUtilBase::MeasureLongStringHeight(System::String^ stringToPrint, System::Drawing::StringFormat^ stringFormat, System::Drawing::Font^ font, float maxWidth, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
{
    float maxStringHeight(1000);
    RectangleF drawInside(0, 0, maxWidth, maxStringHeight);

    int linesFilled(0);
    int charactersFitted(0);
    printArgs->Graphics->MeasureString(stringToPrint, font, drawInside.Size, stringFormat, charactersFitted, linesFilled);
    // Ignoring size returned from the MeasureString function - using lines
    return linesFilled;
}

System::String^
PrintUtilBase::TrimString(DataGridViewCell^ nextCell, StringFormat^ stringFormat, System::Drawing::Font^ font, Single maxLength, System::Drawing::Printing::PrintPageEventArgs^ printArgs, System::String^ charToAppend, System::Boolean% trimmed)
{
    Object^ objectToAdd = nextCell->Value;
    if (objectToAdd != nullptr)
    {
        String^ stringToAdd = objectToAdd->ToString()->Trim();
        return TrimString(stringToAdd, stringFormat, font, maxLength, printArgs, charToAppend, trimmed);
    }
    return gcnew String(charToAppend);
}
String^
PrintUtilBase::TrimString(String^ stringToPrint, StringFormat^ stringFormat, System::Drawing::Font^ font, Single maxLength, System::Drawing::Printing::PrintPageEventArgs^ printArgs, System::String^ charToAppend, System::Boolean% trimmed)
{
    trimmed = false;
    if (maxLength != -1)
    {
        // decrease the max length to allow a sensible space before the next string
        maxLength -= TextRenderer::MeasureText("a", font).Width + 5;
    }

    String^ stringToAdd = stringToPrint->Trim();
    int stringChars = stringToAdd->Length;

    System::Drawing::SizeF textSize = printArgs->Graphics->MeasureString(stringToAdd, font, printArgs->PageBounds.Width, stringFormat);
    while (textSize.Width > maxLength && maxLength != -1)
    {
        --stringChars;
        textSize = printArgs->Graphics->MeasureString(stringToAdd->Substring(0, stringChars), font, printArgs->PageBounds.Width, stringFormat);
        if (!trimmed)
            trimmed = true;
    }
    return gcnew String(stringToAdd->Substring(0, stringChars) + charToAppend);
}

System::Void
PrintUtilBase::PrintStringMaxLength(System::String^ stringToPrint, System::Drawing::Font^ font, float& xPosition, System::Single maxLength, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
{
    if (maxLength <= 0)
        return;

    System::String^ actualStringToPrint = stringToPrint;

    SizeF textSize = printArgs->Graphics->MeasureString(actualStringToPrint, font, printArgs->PageBounds.Width);
    while (textSize.Width > maxLength && actualStringToPrint->Length > 0)
    {
        actualStringToPrint = actualStringToPrint->Substring(0, actualStringToPrint->Length-1);
        textSize = printArgs->Graphics->MeasureString(actualStringToPrint, font, printArgs->PageBounds.Width);
    }

    StringFormat^ stringFormat = gcnew StringFormat;
    if (printThisPage)
    {
        printArgs->Graphics->DrawString(actualStringToPrint, font, System::Drawing::Brushes::Black, xPosition, yPos, stringFormat);
    }
    xPosition += maxLength;
}

System::Void
PrintUtilBase::PrintStringRightJustified(System::String^ stringToPrint, System::Drawing::Font^ font, float& xPosition, System::Single maxLength, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
{
    if (maxLength <= 0)
        return;

    System::String^ actualStringToPrint = stringToPrint;

    SizeF textSize = printArgs->Graphics->MeasureString(actualStringToPrint, font, printArgs->PageBounds.Width);
    while (textSize.Width > maxLength && actualStringToPrint->Length > 0)
    {
        actualStringToPrint = actualStringToPrint->Substring(0, actualStringToPrint->Length-1);
        textSize = printArgs->Graphics->MeasureString(actualStringToPrint, font, printArgs->PageBounds.Width);
    }
    xPosition += (maxLength - textSize.Width);

    StringFormat^ stringFormat = gcnew StringFormat;
    if (printThisPage)
    {
        printArgs->Graphics->DrawString(actualStringToPrint, font, System::Drawing::Brushes::Black, xPosition, yPos, stringFormat);
    }
    xPosition += textSize.Width;
}

System::Void
PrintUtilBase::PrintLine(System::Int32 left, System::UInt32 right, System::UInt32 yPos, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
{
    if (printThisPage)
    {
        Pen^ grayPen = gcnew Pen(Brushes::LightGray);
        Point start(left, yPos);
        Point end(right, yPos);

        printArgs->Graphics->DrawLine(grayPen, start, end);
    }
}

System::Void
PrintUtilBase::addDucoFooter(System::Drawing::Font^ printFont, System::Drawing::StringFormat^ ringerStringFormat, System::Drawing::Printing::PrintPageEventArgs^ printArgs, System::Int16 pageNo)
{
    addDucoFooter(printFont, ringerStringFormat, printArgs);
    float localLineHeight = printFont->GetHeight(printArgs->Graphics);
    String^ printBuffer = "Page " + pageNo;
    System::Drawing::SizeF footerStrSize = printArgs->Graphics->MeasureString(printBuffer, printFont);

    float bottomMarginHeight = float(printArgs->PageBounds.Height) - float(printArgs->MarginBounds.Height + printArgs->MarginBounds.Top);
    float footerXPos = (float(printArgs->MarginBounds.Width) - footerStrSize.Width);
    footerXPos += (float)printArgs->MarginBounds.Left;

    float footerYPos;
    if (bottomMarginHeight >= (localLineHeight * 1.5))
    {
        footerYPos = float (printArgs->MarginBounds.Bottom + (localLineHeight * 0.5) ) ;
        if (bottomMarginHeight >  (localLineHeight * 0.5))
        {
            footerYPos += (bottomMarginHeight - (localLineHeight * 0.5f)) / 2.0f;
        }
    }
    else
    {
        footerYPos = float(printArgs->MarginBounds.Height + printArgs->MarginBounds.Top) - localLineHeight;
    }
    if (printThisPage)
    {
        printArgs->Graphics->DrawString(printBuffer, printFont, System::Drawing::Brushes::LightGray, footerXPos, footerYPos, ringerStringFormat);
    }
}

System::Void 
PrintUtilBase::addDucoFooter(System::Drawing::Font^ printFont, System::Drawing::StringFormat^ ringerStringFormat, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
{
    String^ printBuffer = KPrintFooter;
    System::Drawing::SizeF footerStrSize = printArgs->Graphics->MeasureString(printBuffer, printFont);

    float bottomMarginHeight = float(printArgs->PageBounds.Height) - float(printArgs->MarginBounds.Height + printArgs->MarginBounds.Top);
    float footerXPos = (float(printArgs->MarginBounds.Width) - footerStrSize.Width) / 2;
    footerXPos += (float)printArgs->MarginBounds.Left;

    noOfLines -= KFooterSizeLines; // to allow for the duco print footer.
    if (printThisPage)
    {
        printArgs->Graphics->DrawString(printBuffer, printFont, System::Drawing::Brushes::LightGray, footerXPos, float(printArgs->MarginBounds.Bottom), ringerStringFormat);
    }
}

System::Void
PrintUtilBase::GenerateColumnWidths(std::vector<float>& columnWidths, System::Drawing::Printing::PrintPageEventArgs^ printArgs, System::Windows::Forms::DataGridView^ dataGridView)
{
    columnWidths.clear();

    for (int i (0); i < dataGridView->Columns->Count; ++i)
    {
        float nextColumnWidth(0);
        DataGridViewColumn^ currentColumn = dataGridView->Columns[i];
        SizeF stringSize = printArgs->Graphics->MeasureString(dataGridView->Columns[i]->HeaderCell->Value->ToString(), boldFont, printArgs->PageBounds.Width);
        for (int j (0); j < dataGridView->Rows->Count; ++j)
        {
            SizeF stringSize = printArgs->Graphics->MeasureString(DucoUtils::ConvertString(dataGridView[i,j]), normalFont, printArgs->PageBounds.Width);
            nextColumnWidth = Math::Max(nextColumnWidth, stringSize.Width);
        }
        columnWidths.push_back(nextColumnWidth);
    }

    // Now check column titles.
    int columnNo (0);
    std::vector<float>::iterator it = columnWidths.begin();
    while (it != columnWidths.end())
    {
        if (*it > 0)
        {
            float columnHeaderWidth = ColumnHeaderSize(dataGridView->Columns[columnNo], printArgs, columnNo != 0 && rotateHeaderCells);
            if (*it < columnHeaderWidth)
            {
                (*it) = columnHeaderWidth;
            }
        }

        ++columnNo;
        ++it;
    }
}

float
PrintUtilBase::ColumnHeaderSize(DataGridViewColumn^ theColumn, System::Drawing::Printing::PrintPageEventArgs^ printArgs, bool rotate)
{
    SizeF stringSize = printArgs->Graphics->MeasureString(theColumn->HeaderCell->Value->ToString(), largeFont);
    if (rotate)
    {
        maxHeaderRowHeight = Math::Max(maxHeaderRowHeight, stringSize.Width);
        return stringSize.Height;
    }

    return stringSize.Width;
}
