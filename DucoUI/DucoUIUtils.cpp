#include "DucoUIUtils.h"
#include <RingingDatabase.h>

using namespace DucoUI;
using namespace System::Windows::Forms;
using namespace System::Drawing::Printing;

bool
DucoUIUtils::NumericKey(System::Windows::Forms::KeyEventArgs^% keys, System::Windows::Forms::Keys currentModifiers)
{
    bool numberEntered = false;
    switch (keys->KeyCode)
    {
    case Keys::D0:
    case Keys::D1:
    case Keys::D2:
    case Keys::D3:
    case Keys::D4:
    case Keys::D5:
    case Keys::D6:
    case Keys::D7:
    case Keys::D8:
    case Keys::D9:
    case Keys::NumPad0:
    case Keys::NumPad1:
    case Keys::NumPad2:
    case Keys::NumPad3:
    case Keys::NumPad4:
    case Keys::NumPad5:
    case Keys::NumPad6:
    case Keys::NumPad7:
    case Keys::NumPad8:
    case Keys::NumPad9:
    case Keys::Back:
    case Keys::Left:
    case Keys::Right:
    case Keys::Up:
    case Keys::Down:
    case Keys::Delete:
        if (currentModifiers == Keys::None)
            numberEntered = true;
        break;
    default:
        break;
    }

    return numberEntered;
}

bool
DucoUIUtils::NotationKey(System::Windows::Forms::KeyEventArgs^% keys, System::Windows::Forms::Keys currentModifiers, bool fullNotation, unsigned int noOfBells)
{
    bool validCharEntered (false);
    switch (keys->KeyCode)
    {
    case Keys::NumPad0:
    case Keys::D0:
        if (noOfBells >= 10)
            validCharEntered = true;
        break;
    case Keys::NumPad1:
    case Keys::D1:
        if (noOfBells >= 1)
            validCharEntered = true;
        break;
    case Keys::NumPad2:
    case Keys::D2:
        if (noOfBells >= 2)
            validCharEntered = true;
        break;
    case Keys::NumPad3:
    case Keys::D3:
        if (noOfBells >= 3)
            validCharEntered = true;
        break;
    case Keys::NumPad4:
    case Keys::D4:
        if (noOfBells >= 4)
            validCharEntered = true;
        break;
    case Keys::NumPad5:
    case Keys::D5:
        if (noOfBells >= 5)
            validCharEntered = true;
        break;
    case Keys::NumPad6:
    case Keys::D6:
        if (noOfBells >= 6)
            validCharEntered = true;
        break;
    case Keys::NumPad7:
        if (noOfBells >= 7)
            validCharEntered = true;
        break;
    case Keys::D7:
        if (noOfBells >= 7 || currentModifiers == Keys::Shift)
            validCharEntered = true;
        break;
    case Keys::NumPad8:
    case Keys::D8:
        if (noOfBells >= 8)
            validCharEntered = true;
        break;
    case Keys::NumPad9:
    case Keys::D9:
        if (noOfBells >= 9)
            validCharEntered = true;
        break;
    case Keys::E:
        if (noOfBells >= 11)
            validCharEntered = true;
        break;
    case Keys::T:
        if (noOfBells >= 12)
            validCharEntered = true;
        break;
    case Keys::A:
        if (noOfBells >= 13)
            validCharEntered = true;
        break;
    case Keys::B:
        if (noOfBells >= 14)
            validCharEntered = true;
        break;
    case Keys::C:
        if (noOfBells >= 15 || currentModifiers == Keys::Control) // && copy
            validCharEntered = true;
        break;
    case Keys::D:
        if (noOfBells >= 16)
            validCharEntered = true;
        break;
    case Keys::Back:
    case Keys::Left:
    case Keys::Right:
    case Keys::Up:
    case Keys::Down:
    case Keys::Delete:
        if (currentModifiers == Keys::None)
            validCharEntered = true;
        break;
    case Keys::V:
        if (currentModifiers == Keys::Control) // && paste
            validCharEntered = true;
        break;

    default:
        validCharEntered = false;
        break;
    }

    if (!validCharEntered && fullNotation)
    {
        switch (keys->KeyCode)
        {
        case Keys::X:
        case Keys::OemPeriod:
        case Keys::OemMinus:
        case Keys::Subtract:
        case Keys::Decimal:
            validCharEntered = true;
            break;
        }
    }

    return validCharEntered;
}

System::Void
DucoUIUtils::Print(System::Windows::Forms::Form^ owner, System::Object^ sender, System::Drawing::Printing::PrintPageEventHandler^ printHandler, System::String^ objectName, System::Boolean usePrintPreview, bool landscape)
{
    try
    {
        if (usePrintPreview)
        {
            PrintPreviewDialog^ printPreviewDialog = gcnew PrintPreviewDialog();
            printPreviewDialog->Icon = owner->Icon;
            printPreviewDialog->Document = gcnew PrintDocument;
            printPreviewDialog->Document->DefaultPageSettings->Landscape = landscape;
            printPreviewDialog->Document->PrintPage += printHandler;
            printPreviewDialog->ShowDialog();
        }
        else
        {
            PrintDialog^ printDialog = gcnew PrintDialog();
            printDialog->UseEXDialog = false;
            printDialog->AllowSomePages = true;
            printDialog->Document = gcnew PrintDocument;
            printDialog->Document->DefaultPageSettings->Landscape = landscape;
            printDialog->Document->PrintPage += printHandler;
            if (printDialog->ShowDialog() == ::DialogResult::OK )
            {
                printDialog->Document->Print();
            }
        }
    }
    catch (System::Exception^ ex)
    {
        MessageBox::Show(owner, ex->Message, "Error printing " + objectName, MessageBoxButtons::OK, MessageBoxIcon::Error);
    }
}
