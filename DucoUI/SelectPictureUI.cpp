#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "OpenObjectCallback.h"

#include "PictureListControl.h"
#include "SelectPictureUI.h"
#include "SystemDefaultIcon.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Windows::Forms;
using namespace DucoUI;

SelectPictureUI::SelectPictureUI(DucoUI::DatabaseManager^ theDatabase)
    : database(theDatabase)
{
    InitializeComponent();
    pictureList = gcnew DucoUI::PictureListControl(database, nullptr);
    this->Controls->Add(this->pictureList);
    this->pictureList->Dock = System::Windows::Forms::DockStyle::Fill;
    this->pictureList->Location = System::Drawing::Point(0, 0);
    this->pictureList->Margin = System::Windows::Forms::Padding(0);
    this->pictureList->Name = L"pictureList";
    this->pictureList->Size = System::Drawing::Size(363, 237);
    this->pictureList->TabIndex = 0;
    pictureList->SetSingleSelectionMode();
    pictureList->closeHandler += gcnew System::EventHandler(this, &SelectPictureUI::pictureListCancelled);
    pictureList->pictureSelectedHandler += gcnew System::EventHandler(this, &SelectPictureUI::pictureSelected);
}

SelectPictureUI::~SelectPictureUI()
{
    if (components)
    {
        delete components;
    }
}

System::Void
SelectPictureUI::SelectPictureUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    this->Icon = SystemDefaultIcon::DefaultSystemIcon();
    pictureList->RefreshList(true, false, true);
}

System::Void
SelectPictureUI::pictureListCancelled(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void
SelectPictureUI::pictureSelected(System::Object^  sender, System::EventArgs^  e)
{
    pictureEventHandler(this, e);
    Close();
}

System::Void
SelectPictureUI::ShowPicture(Duco::ObjectId& pictureId)
{
    pictureList->ShowPicture(pictureId);
}
