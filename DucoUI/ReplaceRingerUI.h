#pragma once

namespace DucoUI
{
    public ref class ReplaceRingerUI : public System::Windows::Forms::Form, public DucoUI::RingingDatabaseObserver
    {
    public:
        ReplaceRingerUI(DucoUI::DatabaseManager^ theDatabase);
        ReplaceRingerUI(DucoUI::DatabaseManager^ theDatabase, const Duco::ObjectId& id, const Duco::ObjectId& id2);
        !ReplaceRingerUI();

        // from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

    protected:
        ~ReplaceRingerUI();
        System::Void ReplaceRingerUI_Load(System::Object^  sender, System::EventArgs^  e);

        System::Void OldRinger_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void NewRinger_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);

        System::Void ReplaceBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void CloseBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void swapBtn_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void EnableButtons();

    private:
        DucoUI::DatabaseManager^                     database;
        DucoUI::RingerDisplayCache^                  ringerCache;

        System::ComponentModel::Container^  components;
        System::Windows::Forms::ComboBox^   oldRinger;
        System::Windows::Forms::ComboBox^   newRinger;
        System::Windows::Forms::CheckBox^   deleteRinger;
        System::Windows::Forms::Label^      oldRingerPerfCountLbl;
        System::Windows::Forms::Label^      newRingerPerfCountLbl;
        System::Windows::Forms::ToolStripStatusLabel^  statusLabel;
        System::Windows::Forms::Label^      newRingerLastLbl;
        System::Windows::Forms::Label^      oldRingerLastLbl;
        System::Windows::Forms::Label^      newRingerFirstLbl;
        System::Windows::Forms::Label^      oldRingerFirstLbl;
        Duco::ObjectId* idOne;
        Duco::ObjectId* idTwo;
    private: System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;

        System::Windows::Forms::Button^ replaceBtn;

        void InitializeComponent()
        {
            System::Windows::Forms::Button^ closeBtn;
            System::Windows::Forms::Label^ label1;
            System::Windows::Forms::Label^ label2;
            System::Windows::Forms::StatusStrip^ statusStrip1;
            System::Windows::Forms::Label^ label6;
            System::Windows::Forms::Button^ swapBtn;
            System::Windows::Forms::Label^ label3;
            System::Windows::Forms::Label^ label5;
            this->statusLabel = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            this->newRingerLastLbl = (gcnew System::Windows::Forms::Label());
            this->oldRingerLastLbl = (gcnew System::Windows::Forms::Label());
            this->newRingerFirstLbl = (gcnew System::Windows::Forms::Label());
            this->oldRingerFirstLbl = (gcnew System::Windows::Forms::Label());
            this->replaceBtn = (gcnew System::Windows::Forms::Button());
            this->oldRinger = (gcnew System::Windows::Forms::ComboBox());
            this->newRingerPerfCountLbl = (gcnew System::Windows::Forms::Label());
            this->oldRingerPerfCountLbl = (gcnew System::Windows::Forms::Label());
            this->deleteRinger = (gcnew System::Windows::Forms::CheckBox());
            this->newRinger = (gcnew System::Windows::Forms::ComboBox());
            closeBtn = (gcnew System::Windows::Forms::Button());
            label1 = (gcnew System::Windows::Forms::Label());
            label2 = (gcnew System::Windows::Forms::Label());
            statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            label6 = (gcnew System::Windows::Forms::Label());
            swapBtn = (gcnew System::Windows::Forms::Button());
            label3 = (gcnew System::Windows::Forms::Label());
            label5 = (gcnew System::Windows::Forms::Label());
            statusStrip1->SuspendLayout();
            this->tableLayoutPanel1->SuspendLayout();
            this->SuspendLayout();
            // 
            // closeBtn
            // 
            closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->tableLayoutPanel1->SetColumnSpan(closeBtn, 2);
            closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            closeBtn->Location = System::Drawing::Point(522, 99);
            closeBtn->Name = L"closeBtn";
            closeBtn->Size = System::Drawing::Size(75, 23);
            closeBtn->TabIndex = 10;
            closeBtn->Text = L"Close";
            closeBtn->UseVisualStyleBackColor = true;
            closeBtn->Click += gcnew System::EventHandler(this, &ReplaceRingerUI::CloseBtn_Click);
            // 
            // label1
            // 
            label1->AutoSize = true;
            label1->Dock = System::Windows::Forms::DockStyle::Fill;
            label1->Location = System::Drawing::Point(3, 13);
            label1->Name = L"label1";
            label1->Size = System::Drawing::Size(98, 27);
            label1->TabIndex = 1;
            label1->Text = L"Remove ringer";
            label1->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // label2
            // 
            label2->AutoSize = true;
            label2->Dock = System::Windows::Forms::DockStyle::Fill;
            label2->Location = System::Drawing::Point(3, 40);
            label2->Name = L"label2";
            label2->Size = System::Drawing::Size(98, 27);
            label2->TabIndex = 4;
            label2->Text = L"Replace with ringer";
            label2->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // statusStrip1
            // 
            statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->statusLabel });
            statusStrip1->Location = System::Drawing::Point(0, 126);
            statusStrip1->Name = L"statusStrip1";
            statusStrip1->Size = System::Drawing::Size(600, 22);
            statusStrip1->TabIndex = 11;
            statusStrip1->Text = L"statusStrip1";
            // 
            // statusLabel
            // 
            this->statusLabel->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
            this->statusLabel->Name = L"statusLabel";
            this->statusLabel->Size = System::Drawing::Size(585, 17);
            this->statusLabel->Spring = true;
            this->statusLabel->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // label6
            // 
            label6->AutoSize = true;
            label6->Location = System::Drawing::Point(554, 0);
            label6->Name = L"label6";
            label6->Size = System::Drawing::Size(27, 13);
            label6->TabIndex = 19;
            label6->Text = L"Last";
            // 
            // swapBtn
            // 
            swapBtn->Location = System::Drawing::Point(3, 99);
            swapBtn->Name = L"swapBtn";
            swapBtn->Size = System::Drawing::Size(75, 23);
            swapBtn->TabIndex = 11;
            swapBtn->Text = L"Swap";
            swapBtn->UseVisualStyleBackColor = true;
            swapBtn->Click += gcnew System::EventHandler(this, &ReplaceRingerUI::swapBtn_Click);
            // 
            // label3
            // 
            label3->AutoSize = true;
            label3->Location = System::Drawing::Point(441, 0);
            label3->Name = L"label3";
            label3->Size = System::Drawing::Size(72, 13);
            label3->TabIndex = 12;
            label3->Text = L"Performances";
            // 
            // label5
            // 
            label5->AutoSize = true;
            label5->Location = System::Drawing::Point(522, 0);
            label5->Name = L"label5";
            label5->Size = System::Drawing::Size(26, 13);
            label5->TabIndex = 14;
            label5->Text = L"First";
            // 
            // tableLayoutPanel1
            // 
            this->tableLayoutPanel1->ColumnCount = 5;
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
                100)));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->Controls->Add(label6, 3, 0);
            this->tableLayoutPanel1->Controls->Add(this->newRingerLastLbl, 4, 2);
            this->tableLayoutPanel1->Controls->Add(this->oldRingerLastLbl, 4, 1);
            this->tableLayoutPanel1->Controls->Add(this->newRingerFirstLbl, 3, 2);
            this->tableLayoutPanel1->Controls->Add(this->oldRingerFirstLbl, 3, 1);
            this->tableLayoutPanel1->Controls->Add(closeBtn, 3, 4);
            this->tableLayoutPanel1->Controls->Add(this->replaceBtn, 2, 4);
            this->tableLayoutPanel1->Controls->Add(this->oldRinger, 1, 1);
            this->tableLayoutPanel1->Controls->Add(this->newRingerPerfCountLbl, 2, 2);
            this->tableLayoutPanel1->Controls->Add(label1, 0, 1);
            this->tableLayoutPanel1->Controls->Add(this->oldRingerPerfCountLbl, 2, 1);
            this->tableLayoutPanel1->Controls->Add(label2, 0, 2);
            this->tableLayoutPanel1->Controls->Add(this->deleteRinger, 1, 3);
            this->tableLayoutPanel1->Controls->Add(this->newRinger, 1, 2);
            this->tableLayoutPanel1->Controls->Add(swapBtn, 0, 4);
            this->tableLayoutPanel1->Controls->Add(label3, 2, 0);
            this->tableLayoutPanel1->Controls->Add(label5, 3, 0);
            this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            this->tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
            this->tableLayoutPanel1->Padding = System::Windows::Forms::Padding(0, 0, 0, 23);
            this->tableLayoutPanel1->RowCount = 5;
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            this->tableLayoutPanel1->Size = System::Drawing::Size(600, 148);
            this->tableLayoutPanel1->TabIndex = 12;
            // 
            // newRingerLastLbl
            // 
            this->newRingerLastLbl->AutoSize = true;
            this->newRingerLastLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            this->newRingerLastLbl->Location = System::Drawing::Point(554, 40);
            this->newRingerLastLbl->Name = L"newRingerLastLbl";
            this->newRingerLastLbl->Size = System::Drawing::Size(43, 27);
            this->newRingerLastLbl->TabIndex = 18;
            this->newRingerLastLbl->Text = L"0";
            this->newRingerLastLbl->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
            // 
            // oldRingerLastLbl
            // 
            this->oldRingerLastLbl->AutoSize = true;
            this->oldRingerLastLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            this->oldRingerLastLbl->Location = System::Drawing::Point(554, 13);
            this->oldRingerLastLbl->Name = L"oldRingerLastLbl";
            this->oldRingerLastLbl->Size = System::Drawing::Size(43, 27);
            this->oldRingerLastLbl->TabIndex = 17;
            this->oldRingerLastLbl->Text = L"0";
            this->oldRingerLastLbl->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
            // 
            // newRingerFirstLbl
            // 
            this->newRingerFirstLbl->AutoSize = true;
            this->newRingerFirstLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            this->newRingerFirstLbl->Location = System::Drawing::Point(522, 40);
            this->newRingerFirstLbl->Name = L"newRingerFirstLbl";
            this->newRingerFirstLbl->Size = System::Drawing::Size(26, 27);
            this->newRingerFirstLbl->TabIndex = 16;
            this->newRingerFirstLbl->Text = L"0";
            this->newRingerFirstLbl->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
            // 
            // oldRingerFirstLbl
            // 
            this->oldRingerFirstLbl->AutoSize = true;
            this->oldRingerFirstLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            this->oldRingerFirstLbl->Location = System::Drawing::Point(522, 13);
            this->oldRingerFirstLbl->Name = L"oldRingerFirstLbl";
            this->oldRingerFirstLbl->Size = System::Drawing::Size(26, 27);
            this->oldRingerFirstLbl->TabIndex = 15;
            this->oldRingerFirstLbl->Text = L"0";
            this->oldRingerFirstLbl->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
            // 
            // replaceBtn
            // 
            this->replaceBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->replaceBtn->Enabled = false;
            this->replaceBtn->Location = System::Drawing::Point(441, 99);
            this->replaceBtn->Name = L"replaceBtn";
            this->replaceBtn->Size = System::Drawing::Size(75, 23);
            this->replaceBtn->TabIndex = 9;
            this->replaceBtn->Text = L"Replace";
            this->replaceBtn->UseVisualStyleBackColor = true;
            this->replaceBtn->Click += gcnew System::EventHandler(this, &ReplaceRingerUI::ReplaceBtn_Click);
            // 
            // oldRinger
            // 
            this->oldRinger->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
            this->oldRinger->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->oldRinger->Dock = System::Windows::Forms::DockStyle::Fill;
            this->oldRinger->FormattingEnabled = true;
            this->oldRinger->Location = System::Drawing::Point(107, 16);
            this->oldRinger->Name = L"oldRinger";
            this->oldRinger->Size = System::Drawing::Size(328, 21);
            this->oldRinger->TabIndex = 2;
            this->oldRinger->SelectedIndexChanged += gcnew System::EventHandler(this, &ReplaceRingerUI::OldRinger_SelectedIndexChanged);
            // 
            // newRingerPerfCountLbl
            // 
            this->newRingerPerfCountLbl->AutoSize = true;
            this->newRingerPerfCountLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            this->newRingerPerfCountLbl->Location = System::Drawing::Point(441, 40);
            this->newRingerPerfCountLbl->Name = L"newRingerPerfCountLbl";
            this->newRingerPerfCountLbl->Size = System::Drawing::Size(75, 27);
            this->newRingerPerfCountLbl->TabIndex = 6;
            this->newRingerPerfCountLbl->Text = L"0";
            this->newRingerPerfCountLbl->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
            // 
            // oldRingerPerfCountLbl
            // 
            this->oldRingerPerfCountLbl->AutoSize = true;
            this->oldRingerPerfCountLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            this->oldRingerPerfCountLbl->Location = System::Drawing::Point(441, 13);
            this->oldRingerPerfCountLbl->Name = L"oldRingerPerfCountLbl";
            this->oldRingerPerfCountLbl->Size = System::Drawing::Size(75, 27);
            this->oldRingerPerfCountLbl->TabIndex = 3;
            this->oldRingerPerfCountLbl->Text = L"0";
            this->oldRingerPerfCountLbl->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
            // 
            // deleteRinger
            // 
            this->deleteRinger->AutoSize = true;
            tableLayoutPanel1->SetColumnSpan(this->deleteRinger, 2);
            this->deleteRinger->Location = System::Drawing::Point(107, 70);
            this->deleteRinger->Name = L"deleteRinger";
            this->deleteRinger->Size = System::Drawing::Size(130, 17);
            this->deleteRinger->TabIndex = 7;
            this->deleteRinger->Text = L"Delete replaced ringer";
            this->deleteRinger->UseVisualStyleBackColor = true;
            // 
            // newRinger
            // 
            this->newRinger->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
            this->newRinger->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->newRinger->Dock = System::Windows::Forms::DockStyle::Fill;
            this->newRinger->FormattingEnabled = true;
            this->newRinger->Location = System::Drawing::Point(107, 43);
            this->newRinger->Name = L"newRinger";
            this->newRinger->Size = System::Drawing::Size(328, 21);
            this->newRinger->TabIndex = 5;
            this->newRinger->SelectedIndexChanged += gcnew System::EventHandler(this, &ReplaceRingerUI::NewRinger_SelectedIndexChanged);
            // 
            // ReplaceRingerUI
            // 
            this->AcceptButton = this->replaceBtn;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->CancelButton = closeBtn;
            this->ClientSize = System::Drawing::Size(600, 148);
            this->Controls->Add(statusStrip1);
            this->Controls->Add(this->tableLayoutPanel1);
            this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
            this->MinimumSize = System::Drawing::Size(465, 159);
            this->Name = L"ReplaceRingerUI";
            this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
            this->Text = L"Replace ringer";
            this->Load += gcnew System::EventHandler(this, &ReplaceRingerUI::ReplaceRingerUI_Load);
            statusStrip1->ResumeLayout(false);
            statusStrip1->PerformLayout();
            this->tableLayoutPanel1->ResumeLayout(false);
            this->tableLayoutPanel1->PerformLayout();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
    };

}
