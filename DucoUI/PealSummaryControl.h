#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Windows::Forms;

namespace DucoUI {

    /// <summary>
    /// Summary for PealSummaryControl
    /// </summary>
    public ref class PealSummaryControl : public System::Windows::Forms::UserControl
    {
    public:
        PealSummaryControl(DucoUI::DatabaseManager^ theDatabase)
            : database(theDatabase)
        {
            InitializeComponent();
        }

        System::Void SetPealDetails(System::UInt32 pealId, System::Boolean firstOrOnlyPeal);
    protected:
        System::Void expandBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void openBtn_Click(System::Object^  sender, System::EventArgs^  e);

    ~PealSummaryControl()
    {
        if (components)
        {
            delete components;
        }
    }
        System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
        System::Windows::Forms::Button^            expandBtn;
        System::Windows::Forms::Label^             pealSummary;
        System::Windows::Forms::RichTextBox^       pealDetails;
        DucoUI::DatabaseManager^                   database;
        System::UInt32                             pealId;
	private:
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
            System::Windows::Forms::Button^  openBtn;
            this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            this->expandBtn = (gcnew System::Windows::Forms::Button());
            this->pealSummary = (gcnew System::Windows::Forms::Label());
            this->pealDetails = (gcnew System::Windows::Forms::RichTextBox());
            openBtn = (gcnew System::Windows::Forms::Button());
            this->tableLayoutPanel1->SuspendLayout();
            this->SuspendLayout();
            // 
            // openBtn
            // 
            openBtn->AutoSize = true;
            openBtn->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            openBtn->Location = System::Drawing::Point(368, 3);
            openBtn->Name = L"openBtn";
            openBtn->Size = System::Drawing::Size(29, 23);
            openBtn->TabIndex = 1;
            openBtn->Text = L">>";
            openBtn->UseVisualStyleBackColor = true;
            openBtn->Click += gcnew System::EventHandler(this, &PealSummaryControl::openBtn_Click);
            // 
            // tableLayoutPanel1
            // 
            this->tableLayoutPanel1->ColumnCount = 3;
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
                100)));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->Controls->Add(this->expandBtn, 1, 0);
            this->tableLayoutPanel1->Controls->Add(openBtn, 2, 0);
            this->tableLayoutPanel1->Controls->Add(this->pealSummary, 0, 0);
            this->tableLayoutPanel1->Controls->Add(this->pealDetails, 0, 1);
            this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            this->tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
            this->tableLayoutPanel1->RowCount = 2;
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            this->tableLayoutPanel1->Size = System::Drawing::Size(400, 30);
            this->tableLayoutPanel1->TabIndex = 0;
            // 
            // expandBtn
            // 
            this->expandBtn->AutoSize = true;
            this->expandBtn->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            this->expandBtn->Font = (gcnew System::Drawing::Font(L"Wingdings", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
                static_cast<System::Byte>(2)));
            this->expandBtn->Location = System::Drawing::Point(337, 3);
            this->expandBtn->MinimumSize = System::Drawing::Size(25, 23);
            this->expandBtn->Name = L"expandBtn";
            this->expandBtn->Size = System::Drawing::Size(25, 23);
            this->expandBtn->TabIndex = 2;
            this->expandBtn->Text = L"�";
            this->expandBtn->UseVisualStyleBackColor = true;
            this->expandBtn->Click += gcnew System::EventHandler(this, &PealSummaryControl::expandBtn_Click);
            // 
            // pealSummary
            // 
            this->pealSummary->AutoSize = true;
            this->pealSummary->Dock = System::Windows::Forms::DockStyle::Fill;
            this->pealSummary->Location = System::Drawing::Point(3, 7);
            this->pealSummary->Margin = System::Windows::Forms::Padding(3, 7, 3, 7);
            this->pealSummary->Name = L"pealSummary";
            this->pealSummary->Size = System::Drawing::Size(328, 15);
            this->pealSummary->TabIndex = 0;
            this->pealSummary->Text = L"1 Peal summary";
            // 
            // pealDetails
            // 
            this->tableLayoutPanel1->SetColumnSpan(this->pealDetails, 3);
            this->pealDetails->Dock = System::Windows::Forms::DockStyle::Fill;
            this->pealDetails->Location = System::Drawing::Point(3, 32);
            this->pealDetails->Name = L"pealDetails";
            this->pealDetails->ReadOnly = true;
            this->pealDetails->Size = System::Drawing::Size(394, 1);
            this->pealDetails->TabIndex = 3;
            this->pealDetails->Text = L"";
            // 
            // PealSummaryControl
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            this->CausesValidation = false;
            this->Controls->Add(this->tableLayoutPanel1);
            this->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
            this->MinimumSize = System::Drawing::Size(550, 30);
            this->Name = L"PealSummaryControl";
            this->Size = System::Drawing::Size(400, 30);
            this->tableLayoutPanel1->ResumeLayout(false);
            this->tableLayoutPanel1->PerformLayout();
            this->ResumeLayout(false);

        }
#pragma endregion
    };
}
