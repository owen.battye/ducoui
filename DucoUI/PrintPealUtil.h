#pragma once
#include "PrintUtilBase.h"
namespace Duco
{
    class Peal;
    class Tower;
}

namespace DucoUI
{
public ref class PrintPealUtil : public PrintUtilBase
{
public:
    PrintPealUtil(DucoUI::DatabaseManager^ theDatabase, System::Drawing::Printing::PrintPageEventArgs^ args);
    void SetObjects(const Duco::Peal* thePeal, const Duco::Tower* theTower);
    virtual System::Void printObject(System::Drawing::Printing::PrintPageEventArgs^ args, System::Drawing::Printing::PrinterSettings^ printSettings) override;

protected:
    System::Void printPealPealBoardFormat(const Duco::Peal& currentPeal, const Duco::Tower& currentTower, System::Drawing::Printing::PrintPageEventArgs^ printArgs);
    System::Void printPealRWFormat(const Duco::Peal& currentPeal, const Duco::Tower& currentTower, System::Drawing::Printing::PrintPageEventArgs^ printArgs);

    const Duco::Peal*  currentPeal;
    const Duco::Tower* currentTower;
};

}