#pragma once

#include "ProgressWrapper.h"
#include <ObjectId.h>
#include <set>
namespace Duco
{
    class DatabaseSearch;
}

namespace DucoUI
{
    ref class DatabaseManager;

    public ref class MethodSeriesSearch : public System::Windows::Forms::Form,
                                          public DucoUI::ProgressCallbackUI
    {
    public:
        MethodSeriesSearch(DucoUI::DatabaseManager^ theDatabase, bool setStartSearchForInvalid);

        // from ProgressCallbackUI
        virtual void Initialised();
        virtual void Step(int progressPercent);
        virtual void Complete();

    protected:
        !MethodSeriesSearch();
        ~MethodSeriesSearch();

        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void searchBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void clearBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void MethodSeriesSearch_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void ClosingEvent( Object^ /*sender*/, System::ComponentModel::CancelEventArgs^ e );
        System::Void dataGridView1_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);
        System::Void dataGridView1_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);

        System::Void backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void backgroundWorker1_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void backgroundWorker1_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);
        System::Void PopulateResults(System::ComponentModel::BackgroundWorker^ bgworker);
        bool CreateSearchParameters(Duco::DatabaseSearch& search);

    private:
        DucoUI::DatabaseManager^ database;
        bool startSearchForInvalid;
        System::Collections::Generic::List<System::Int16>^ allMethodIds;
        std::set<Duco::ObjectId>* foundSeriesIds;
        DucoUI::ProgressCallbackWrapper* progressWrapper;
    private: System::Windows::Forms::TextBox^  resultsEditor;
    private: System::Windows::Forms::ProgressBar^  progressBar1;
    private: System::Windows::Forms::DataGridView^  dataGridView1;
    private: System::Windows::Forms::Button^  clearBtn;
    private: System::Windows::Forms::Button^  searchBtn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  idColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  nameColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  bellsColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  methodCountColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  methodsStringColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  errorColumn;

    private: System::Windows::Forms::CheckBox^  completed;
    private: System::Windows::Forms::CheckBox^  containsError;
    private: System::Windows::Forms::ComboBox^  methodSelector;
    private: System::Windows::Forms::NumericUpDown^  noOfMethodsEditor;
    private: System::Windows::Forms::NumericUpDown^  bellsEditor;
    private: System::ComponentModel::BackgroundWorker^  backgroundWorker;
    System::Windows::Forms::TextBox^                    nameEditor;

    private:
        System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
        void InitializeComponent()
        {
            System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
            System::Windows::Forms::Panel^  btnPanel;
            System::Windows::Forms::Button^  closeBtn;
            System::Windows::Forms::Label^  resultsLbl;
            System::Windows::Forms::Panel^  panel1;
            System::Windows::Forms::GroupBox^  otherGroup;
            System::Windows::Forms::GroupBox^  methodsGroup;
            System::Windows::Forms::GroupBox^  nameGroup;
            System::Windows::Forms::GroupBox^  noOfMethodsGroup;
            System::Windows::Forms::GroupBox^  bellsGroup;
            System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(MethodSeriesSearch::typeid));
            this->clearBtn = (gcnew System::Windows::Forms::Button());
            this->searchBtn = (gcnew System::Windows::Forms::Button());
            this->resultsEditor = (gcnew System::Windows::Forms::TextBox());
            this->progressBar1 = (gcnew System::Windows::Forms::ProgressBar());
            this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
            this->idColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->nameColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->bellsColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->methodCountColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->methodsStringColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->errorColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->completed = (gcnew System::Windows::Forms::CheckBox());
            this->containsError = (gcnew System::Windows::Forms::CheckBox());
            this->methodSelector = (gcnew System::Windows::Forms::ComboBox());
            this->noOfMethodsEditor = (gcnew System::Windows::Forms::NumericUpDown());
            this->bellsEditor = (gcnew System::Windows::Forms::NumericUpDown());
            this->nameEditor = (gcnew System::Windows::Forms::TextBox());
            this->backgroundWorker = (gcnew System::ComponentModel::BackgroundWorker());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            btnPanel = (gcnew System::Windows::Forms::Panel());
            closeBtn = (gcnew System::Windows::Forms::Button());
            resultsLbl = (gcnew System::Windows::Forms::Label());
            panel1 = (gcnew System::Windows::Forms::Panel());
            otherGroup = (gcnew System::Windows::Forms::GroupBox());
            methodsGroup = (gcnew System::Windows::Forms::GroupBox());
            nameGroup = (gcnew System::Windows::Forms::GroupBox());            
            noOfMethodsGroup = (gcnew System::Windows::Forms::GroupBox());
            bellsGroup = (gcnew System::Windows::Forms::GroupBox());
            tableLayoutPanel1->SuspendLayout();
            btnPanel->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView1))->BeginInit();
            panel1->SuspendLayout();
            otherGroup->SuspendLayout();
            methodsGroup->SuspendLayout();
            nameGroup->SuspendLayout();
            noOfMethodsGroup->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->noOfMethodsEditor))->BeginInit();
            bellsGroup->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->bellsEditor))->BeginInit();
            this->SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 1;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->Controls->Add(btnPanel, 0, 2);
            tableLayoutPanel1->Controls->Add(this->dataGridView1, 0, 1);
            tableLayoutPanel1->Controls->Add(panel1, 0, 0);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 3;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 113)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 29)));
            tableLayoutPanel1->Size = System::Drawing::Size(677, 347);
            tableLayoutPanel1->TabIndex = 0;
            // 
            // btnPanel
            // 
            btnPanel->Controls->Add(this->clearBtn);
            btnPanel->Controls->Add(this->searchBtn);
            btnPanel->Controls->Add(closeBtn);
            btnPanel->Controls->Add(this->resultsEditor);
            btnPanel->Controls->Add(resultsLbl);
            btnPanel->Controls->Add(this->progressBar1);
            btnPanel->Dock = System::Windows::Forms::DockStyle::Fill;
            btnPanel->Location = System::Drawing::Point(0, 318);
            btnPanel->Margin = System::Windows::Forms::Padding(0);
            btnPanel->Name = L"btnPanel";
            btnPanel->Size = System::Drawing::Size(677, 29);
            btnPanel->TabIndex = 0;
            // 
            // clearBtn
            // 
            this->clearBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->clearBtn->Location = System::Drawing::Point(407, 4);
            this->clearBtn->Name = L"clearBtn";
            this->clearBtn->Size = System::Drawing::Size(75, 23);
            this->clearBtn->TabIndex = 5;
            this->clearBtn->Text = L"Clear";
            this->clearBtn->UseVisualStyleBackColor = true;
            this->clearBtn->Click += gcnew System::EventHandler(this, &MethodSeriesSearch::clearBtn_Click);
            // 
            // searchBtn
            // 
            this->searchBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->searchBtn->Location = System::Drawing::Point(488, 4);
            this->searchBtn->Name = L"searchBtn";
            this->searchBtn->Size = System::Drawing::Size(75, 23);
            this->searchBtn->TabIndex = 4;
            this->searchBtn->Text = L"Search";
            this->searchBtn->UseVisualStyleBackColor = true;
            this->searchBtn->Click += gcnew System::EventHandler(this, &MethodSeriesSearch::searchBtn_Click);
            // 
            // closeBtn
            // 
            closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            closeBtn->Location = System::Drawing::Point(569, 4);
            closeBtn->Name = L"closeBtn";
            closeBtn->Size = System::Drawing::Size(75, 23);
            closeBtn->TabIndex = 3;
            closeBtn->Text = L"Close";
            closeBtn->UseVisualStyleBackColor = true;
            closeBtn->Click += gcnew System::EventHandler(this, &MethodSeriesSearch::closeBtn_Click);
            // 
            // resultsEditor
            // 
            this->resultsEditor->Location = System::Drawing::Point(157, 3);
            this->resultsEditor->Name = L"resultsEditor";
            this->resultsEditor->ReadOnly = true;
            this->resultsEditor->Size = System::Drawing::Size(53, 20);
            this->resultsEditor->TabIndex = 2;
            // 
            // resultsLbl
            // 
            resultsLbl->AutoSize = true;
            resultsLbl->Location = System::Drawing::Point(109, 6);
            resultsLbl->Name = L"resultsLbl";
            resultsLbl->Size = System::Drawing::Size(42, 13);
            resultsLbl->TabIndex = 1;
            resultsLbl->Text = L"Results";
            // 
            // progressBar1
            // 
            this->progressBar1->Location = System::Drawing::Point(3, 4);
            this->progressBar1->Name = L"progressBar1";
            this->progressBar1->Size = System::Drawing::Size(100, 18);
            this->progressBar1->TabIndex = 0;
            // 
            // dataGridView1
            // 
            this->dataGridView1->AllowUserToAddRows = false;
            this->dataGridView1->AllowUserToDeleteRows = false;
            this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->dataGridView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(6) {this->idColumn, 
                this->nameColumn, this->bellsColumn, this->methodCountColumn, this->methodsStringColumn, this->errorColumn});
            this->dataGridView1->Dock = System::Windows::Forms::DockStyle::Fill;
            this->dataGridView1->Location = System::Drawing::Point(3, 116);
            this->dataGridView1->Name = L"dataGridView1";
            this->dataGridView1->ReadOnly = true;
            this->dataGridView1->Size = System::Drawing::Size(671, 199);
            this->dataGridView1->TabIndex = 1;
            this->dataGridView1->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &MethodSeriesSearch::dataGridView1_ColumnHeaderMouseClick);
            this->dataGridView1->CellContentDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &MethodSeriesSearch::dataGridView1_CellContentDoubleClick);
            // 
            // idColumn
            // 
            this->idColumn->HeaderText = L"Column1";
            this->idColumn->Name = L"idColumn";
            this->idColumn->ReadOnly = true;
            this->idColumn->Visible = false;
            // 
            // nameColumn
            // 
            this->nameColumn->HeaderText = L"Series name";
            this->nameColumn->Name = L"nameColumn";
            this->nameColumn->ReadOnly = true;
            this->nameColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            // 
            // bellsColumn
            // 
            this->bellsColumn->HeaderText = L"Bells";
            this->bellsColumn->Name = L"bellsColumn";
            this->bellsColumn->ReadOnly = true;
            this->bellsColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->bellsColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            // 
            // methodCountColumn
            // 
            this->methodCountColumn->HeaderText = L"Methods";
            this->methodCountColumn->Name = L"methodCountColumn";
            this->methodCountColumn->ReadOnly = true;
            this->methodCountColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->methodCountColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            // 
            // methodsStringColumn
            // 
            this->methodsStringColumn->HeaderText = L"Methods";
            this->methodsStringColumn->Name = L"methodsStringColumn";
            this->methodsStringColumn->ReadOnly = true;
            this->methodsStringColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            // 
            // errorColumn
            // 
            this->errorColumn->HeaderText = L"Error";
            this->errorColumn->Name = L"methodsStringColumn";
            this->errorColumn->ReadOnly = true;
            this->errorColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            // 
            // panel1
            // 
            panel1->Controls->Add(otherGroup);
            panel1->Controls->Add(methodsGroup);
            panel1->Controls->Add(nameGroup);
            panel1->Controls->Add(noOfMethodsGroup);
            panel1->Controls->Add(bellsGroup);
            panel1->Dock = System::Windows::Forms::DockStyle::Fill;
            panel1->Location = System::Drawing::Point(0, 0);
            panel1->Margin = System::Windows::Forms::Padding(0);
            panel1->Name = L"panel1";
            panel1->Size = System::Drawing::Size(677, 113);
            panel1->TabIndex = 2;
            // 
            // otherGroup
            // 
            otherGroup->Controls->Add(this->completed);
            otherGroup->Controls->Add(this->containsError);
            otherGroup->Location = System::Drawing::Point(382, 3);
            otherGroup->Name = L"otherGroup";
            otherGroup->Size = System::Drawing::Size(278, 51);
            otherGroup->TabIndex = 3;
            otherGroup->TabStop = false;
            otherGroup->Text = L"Other";
            // 
            // completed
            // 
            this->completed->AutoSize = true;
            this->completed->Location = System::Drawing::Point(120, 19);
            this->completed->Name = L"completed";
            this->completed->Size = System::Drawing::Size(76, 17);
            this->completed->TabIndex = 1;
            this->completed->Text = L"Completed";
            this->completed->UseVisualStyleBackColor = true;
            // 
            // containsError
            // 
            this->containsError->AutoSize = true;
            this->containsError->Location = System::Drawing::Point(6, 19);
            this->containsError->Name = L"containsError";
            this->containsError->Size = System::Drawing::Size(91, 17);
            this->containsError->TabIndex = 0;
            this->containsError->Text = L"Contains error";
            this->containsError->UseVisualStyleBackColor = true;
            // 
            // methodsGroup
            // 
            methodsGroup->Controls->Add(this->methodSelector);
            methodsGroup->Location = System::Drawing::Point(101, 60);
            methodsGroup->Name = L"methodsGroup";
            methodsGroup->Size = System::Drawing::Size(278, 51);
            methodsGroup->TabIndex = 2;
            methodsGroup->TabStop = false;
            methodsGroup->Text = L"Methods";
            // 
            // nameGroup
            // 
            nameGroup->Controls->Add(this->nameEditor);
            nameGroup->Location = System::Drawing::Point(101, 3);
            nameGroup->Name = L"nameGroup";
            nameGroup->Size = System::Drawing::Size(278, 51);
            nameGroup->TabIndex = 2;
            nameGroup->TabStop = false;
            nameGroup->Text = L"Name";
            // 
            // nameEditor
            // 
            this->nameEditor->Location = System::Drawing::Point(6, 19);
            this->nameEditor->Name = L"nameEditor";
            this->nameEditor->Size = System::Drawing::Size(266, 20);
            this->nameEditor->TabIndex = 1;
            // 
            // methodSelector
            // 
            this->methodSelector->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
            this->methodSelector->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->methodSelector->FormattingEnabled = true;
            this->methodSelector->Location = System::Drawing::Point(6, 19);
            this->methodSelector->Name = L"methodSelector";
            this->methodSelector->Size = System::Drawing::Size(266, 21);
            this->methodSelector->TabIndex = 0;
            // 
            // noOfMethodsGroup
            // 
            noOfMethodsGroup->Controls->Add(this->noOfMethodsEditor);
            noOfMethodsGroup->Location = System::Drawing::Point(3, 60);
            noOfMethodsGroup->Name = L"noOfMethodsGroup";
            noOfMethodsGroup->Size = System::Drawing::Size(92, 51);
            noOfMethodsGroup->TabIndex = 1;
            noOfMethodsGroup->TabStop = false;
            noOfMethodsGroup->Text = L"No of methods";
            // 
            // noOfMethodsEditor
            // 
            this->noOfMethodsEditor->Location = System::Drawing::Point(9, 19);
            this->noOfMethodsEditor->Name = L"noOfMethodsEditor";
            this->noOfMethodsEditor->Size = System::Drawing::Size(53, 20);
            this->noOfMethodsEditor->TabIndex = 0;
            // 
            // bellsGroup
            // 
            bellsGroup->Controls->Add(this->bellsEditor);
            bellsGroup->Location = System::Drawing::Point(3, 3);
            bellsGroup->Name = L"bellsGroup";
            bellsGroup->Size = System::Drawing::Size(92, 51);
            bellsGroup->TabIndex = 0;
            bellsGroup->TabStop = false;
            bellsGroup->Text = L"No of bells";
            // 
            // bellsEditor
            // 
            this->bellsEditor->Location = System::Drawing::Point(9, 19);
            this->bellsEditor->Name = L"bellsEditor";
            this->bellsEditor->Size = System::Drawing::Size(53, 20);
            this->bellsEditor->TabIndex = 0;
            // 
            // backgroundWorker
            // 
            this->backgroundWorker->WorkerReportsProgress = true;
            this->backgroundWorker->WorkerSupportsCancellation = true;
            this->backgroundWorker->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &MethodSeriesSearch::backgroundWorker1_DoWork);
            this->backgroundWorker->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &MethodSeriesSearch::backgroundWorker1_ProgressChanged);
            this->backgroundWorker->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &MethodSeriesSearch::backgroundWorker1_RunWorkerCompleted);
            // 
            // MethodSeriesSearch
            // 
            this->AcceptButton = this->searchBtn;
            this->CancelButton = closeBtn;
            this->ClientSize = System::Drawing::Size(677, 347);
            this->Controls->Add(tableLayoutPanel1);
            this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
            this->MinimumSize = System::Drawing::Size(582, 280);
            this->Name = L"MethodSeriesSearch";
            this->Text = L"Search method series";
            this->Closing += gcnew System::ComponentModel::CancelEventHandler(this, &MethodSeriesSearch::ClosingEvent);
            this->Load += gcnew System::EventHandler(this, &MethodSeriesSearch::MethodSeriesSearch_Load);
            tableLayoutPanel1->ResumeLayout(false);
            btnPanel->ResumeLayout(false);
            btnPanel->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView1))->EndInit();
            panel1->ResumeLayout(false);
            otherGroup->ResumeLayout(false);
            otherGroup->PerformLayout();
            methodsGroup->ResumeLayout(false);
            nameGroup->ResumeLayout(false);
            noOfMethodsGroup->ResumeLayout(false);
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->noOfMethodsEditor))->EndInit();
            bellsGroup->ResumeLayout(false);
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->bellsEditor))->EndInit();
            this->ResumeLayout(false);

        }
#pragma endregion
};
}
