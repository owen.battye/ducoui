#pragma once

namespace DucoUI
{
	public ref class TableSorterByString :  public System::Collections::IComparer
	{
	public:
		TableSorterByString(bool newAscending, int newColumnId);
        virtual int Compare(System::Object^, System::Object^);

	protected:
		~TableSorterByString();
        System::String^ ConvertValueToString(System::Windows::Forms::DataGridViewRow^ row, int cellNumber);

    private:
        bool ascending;
        int columnId;
	};
}
