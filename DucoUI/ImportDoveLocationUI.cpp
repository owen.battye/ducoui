#include "ProgressWrapper.h"
#include <DoveDatabase.h>
#include "DatabaseManager.h"

#include <DucoConfiguration.h>
#include "ImportDoveLocationUI.h"
#include "DucoUtils.h"
#include "SoundUtils.h"
#include <DoveDatabase.h>
#include <DoveObject.h>
#include <Tower.h>
#include <TowerDatabase.h>
#include <RingingDatabase.h>
#include "WindowsSettings.h"
#include "SystemDefaultIcon.h"
#include "RingingDatabaseObserver.h"
#include "DucoWindowState.h"
#include "OpenObjectCallback.h"
#include "PictureListControl.h"
#include "PictureControl.h"
#include "TowerUI.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Windows::Forms;

ImportDoveLocationUI::ImportDoveLocationUI(DucoUI::DatabaseManager^ theDatabase)
    : database(theDatabase), doveDatabase(NULL)
{
    callback = new ProgressCallbackWrapper(this);
    InitializeComponent();
    towerAlreadyUsedCellStyle = (gcnew System::Windows::Forms::DataGridViewCellStyle(dataGridView->DefaultCellStyle));
    towerAlreadyUsedCellStyle->ForeColor = System::Drawing::SystemColors::GrayText;
}

ImportDoveLocationUI::!ImportDoveLocationUI()
{
    delete callback;
    delete doveDatabase;
}

ImportDoveLocationUI::~ImportDoveLocationUI()
{
    this->!ImportDoveLocationUI();
    if (components)
    {
        delete components;
    }
}

System::Void
ImportDoveLocationUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (backgroundWorker1->IsBusy)
    {
        backgroundWorker1->CancelAsync();
    }
    else
    {
        Close();
    }
}

System::Void
ImportDoveLocationUI::readFileBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    dataGridView->Rows->Clear();
    StartGeneration(false);
}

System::Void
ImportDoveLocationUI::importbtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (backgroundWorker1->IsBusy || dataGridView->Rows->Count <= 0)
    {
        SoundUtils::PlayErrorSound();
    }
    else
    {
        System::Boolean checkedTowerFound = false;
        System::Collections::IEnumerator^ it = static_cast<System::Collections::IEnumerable^>(dataGridView->Rows)->GetEnumerator();
        while (it->MoveNext() && !checkedTowerFound)
        {
            DataGridViewCheckBoxCell^ checkBoxCell = static_cast<DataGridViewCheckBoxCell^>(static_cast<DataGridViewRow^>(it->Current)->Cells[0]);
            System::Object^ value = checkBoxCell->Value;
            if (Convert::ToBoolean(checkBoxCell->Value))
            {
                checkedTowerFound = true;
            }
        }

        if (checkedTowerFound)
        {
            statusLbl->Text = "";
            importbtn->Enabled = false;
            selectAllBtn->Enabled = false;
            readFileBtn->Enabled = false;
            openBtn->Enabled = false;
            StartGeneration(true);
        }
        else
        {
            statusLbl->Text = "No towers selected";
            SoundUtils::PlayErrorSound();
        }
    }
}

System::Void
ImportDoveLocationUI::StartGeneration(System::Boolean importTowers)
{
    statusLbl->Text = "";
    numberOfTowersFound = 0;
    numberOfTowersProcessable = 0;
    importingTowers = importTowers;

    if (System::IO::File::Exists(database->DoveDataFile()))
    {
        database->SetDoveDataFile(openFileDialog1->FileName);
        DoveUpdateUiArgs^ args = gcnew DoveUpdateUiArgs();
        args->doveFile = database->DoveDataFile();
        args->openDoveFile = !importTowers;
        args->importAllTowersWithDoveId = allTowers->Checked;

        openBtn->Enabled = false;
        selectAllBtn->Enabled = false;
        readFileBtn->Enabled = false;
        importbtn->Enabled = false;
        backgroundWorker1->RunWorkerAsync(args);
    }
}

System::Void
ImportDoveLocationUI::openBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (backgroundWorker1->IsBusy)
    {
        SoundUtils::PlayErrorSound();
    }
    else
    {
        if (openFileDialog1->ShowDialog() == ::System::Windows::Forms::DialogResult::OK)
        {
            dataGridView->Rows->Clear();
            if (doveDatabase != NULL)
            {
                doveDatabase->Clear();
            }
            database->SetDoveDataFile(openFileDialog1->FileName);

            StartGeneration(false);
        }
    }
}

System::Void
ImportDoveLocationUI::backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
    bool cancelled = false;
    BackgroundWorker^ worker = dynamic_cast<BackgroundWorker^>(sender);
    DoveUpdateUiArgs^ startArgs = static_cast<DoveUpdateUiArgs^>(e->Argument);
    if (startArgs->openDoveFile)
    {
        std::string filename;
        DucoUtils::ConvertString(startArgs->doveFile, filename);
        if (doveDatabase != NULL)
        {
            delete doveDatabase;
        }
        doveDatabase = new DoveDatabase(filename, callback, database->WindowsSettings()->InstallationDir(), true);
        doveDatabase->Import();

        std::set<Duco::ObjectId> towersWithDoveId;
        if (database->Database().TowersDatabase().TowersWithDoveId(towersWithDoveId, !startArgs->importAllTowersWithDoveId))
        {
            float total = (float)towersWithDoveId.size();
            float count = 0;
            std::set<Duco::ObjectId>::const_iterator it = towersWithDoveId.begin();
            while (it != towersWithDoveId.end() && !cancelled)
            {
                ++count;
                Duco::Tower theTower;
                if (database->FindTower(*it, theTower, false))
                {
                    DataGridViewRow^ newRow = gcnew DataGridViewRow();
                    DataGridViewCellCollection^ theCells = newRow->Cells;
                    //checkbox Column;
                    DataGridViewCheckBoxCell^ checkboxCell = gcnew DataGridViewCheckBoxCell();
                    checkboxCell->ReadOnly = false;
                    theCells->Add(checkboxCell);
                    //id Column;
                    DataGridViewTextBoxCell^ ducoIdCell = gcnew DataGridViewTextBoxCell();
                    ducoIdCell->Value = DucoUtils::ConvertString(theTower.Id().Str());
                    theCells->Add(ducoIdCell);
                    //tower name Column;
                    DataGridViewTextBoxCell^ ducoTowerCell = gcnew DataGridViewTextBoxCell();
                    ducoTowerCell->Value = DucoUtils::ConvertString(theTower.FullName());
                    theCells->Add(ducoTowerCell);

                    //dove id Column;
                    DataGridViewTextBoxCell^ doveIdCell = gcnew DataGridViewTextBoxCell();
                    doveIdCell->Value = DucoUtils::ConvertString(theTower.DoveRef());
                    theCells->Add(doveIdCell);
                    //Location Column;
                    DataGridViewTextBoxCell^ doveNameCell = gcnew DataGridViewTextBoxCell();
                    if (theTower.Latitude().length() > 0 || theTower.Longitude().length() > 0)
                    {
                        doveNameCell->Value = DucoUtils::ConvertString(theTower.Latitude()) + ", " + DucoUtils::ConvertString(theTower.Longitude());
                    }
                    else
                    {
                        doveNameCell->Value = "Not set";
                    }
                    theCells->Add(doveNameCell);
                    //towerbaseId Column;
                    DataGridViewTextBoxCell^ pealbaseIdCell = gcnew DataGridViewTextBoxCell();
                    pealbaseIdCell->Value = DucoUtils::ConvertString(theTower.TowerbaseId());
                    theCells->Add(pealbaseIdCell);

                    backgroundWorker1->ReportProgress((int)(100 * (count / total)), newRow);
                }
                if (worker->CancellationPending)
                {
                    e->Cancel = true;
                    cancelled = true;
                    break;
                }
                ++it;
            }
        }
    }
    else
    {
        std::set<Duco::ObjectId> objectsToImport;
        System::Collections::IEnumerator^ it = static_cast<System::Collections::IEnumerable^>(dataGridView->Rows)->GetEnumerator();
        while (it->MoveNext())
        {
            DataGridViewCheckBoxCell^ checkBoxCell = static_cast<DataGridViewCheckBoxCell^>(static_cast<DataGridViewRow^>(it->Current)->Cells[0]);
            System::Object^ value = checkBoxCell->Value;

            if (Convert::ToBoolean(checkBoxCell->Value))
            {
                Duco::ObjectId towerId = Convert::ToInt32(static_cast<DataGridViewRow^>(it->Current)->Cells[1]->Value);
                objectsToImport.insert(towerId);
            }
        }

        doveDatabase->ImportPositionAndIds(database->Database().TowersDatabase(), objectsToImport);
    }
}

System::Void
ImportDoveLocationUI::backgroundWorker1_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    progressBar->Value = e->ProgressPercentage;
    if (e->ProgressPercentage < 100)
    {
        closeBtn->Text = "Cancel";
    }
    else
    {
        closeBtn->Text = "Close";
    }
    if (e->UserState != nullptr)
    {
        DataGridViewRow^ newResultRow = static_cast<DataGridViewRow^>(e->UserState);
        if (newResultRow != nullptr)
        {
            ++numberOfTowersFound;
            dataGridView->Rows->Add(newResultRow);
            DataGridViewCheckBoxCell^ checkBoxCell = static_cast<DataGridViewCheckBoxCell^>(static_cast<DataGridViewRow^>(newResultRow)->Cells[0]);
            if (!checkBoxCell->ReadOnly)
            {
                ++numberOfTowersProcessable;
            }
        }
        statusLbl->Text = "Found " + Convert::ToString(numberOfTowersFound) + " towers with dove id and no location";
    }
}

System::Void
ImportDoveLocationUI::backgroundWorker1_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    progressBar->Value = 0;
    if (e == nullptr || e->Cancelled)
    {
        dataGridView->Rows->Clear();
        if (doveDatabase != NULL)
        {
            doveDatabase->Clear();
        }
    }
    else if (importingTowers)
    {
        dataGridView->Rows->Clear();
        importingTowers = false;
        database->CallObservers(TEventType::EUpdated, TObjectType::ETower, KNoId, KNoId);
    }
    selectAllBtn->Enabled = true;
    readFileBtn->Enabled = true;
    openBtn->Enabled = true;
    importbtn->Enabled = true;
    statusLbl->Text = "Found " + Convert::ToString(numberOfTowersFound) + " towers with dove id but no location or Pealbase id";
}

void
ImportDoveLocationUI::Initialised()
{
    backgroundWorker1->ReportProgress(0);
}

void
ImportDoveLocationUI::Step(int progressPercent)
{
    backgroundWorker1->ReportProgress(progressPercent);
}

void
ImportDoveLocationUI::Complete()
{

}

System::Void
ImportDoveLocationUI::dataGridView_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    try
    {
        unsigned int rowIndex = e->RowIndex;
        DataGridViewRow^ currentRow = dataGridView->Rows[rowIndex];
        if (e->ColumnIndex >= 1 && e->ColumnIndex <= 2)
        {
            if (currentRow->Cells[1]->Value != nullptr)
            {
                unsigned int towerId = Convert::ToInt16(currentRow->Cells[1]->Value);
                if (towerId > 0)
                {
                    TowerUI::ShowTower(database, this->MdiParent, towerId, -1);
                }
            }
        }
        else if (e->ColumnIndex == 5)
        {
            if (currentRow->Cells[1]->Value != nullptr)
            {
                unsigned int towerId = Convert::ToInt16(currentRow->Cells[5]->Value);
                if (towerId > 0)
                {
                    TowerUI::ShowTower(database, this->MdiParent, towerId, -1);
                }
            }
        }
    }
    catch (Exception^)
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
ImportDoveLocationUI::ImportDoveLocationUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e)
{
    if (backgroundWorker1->IsBusy)
    {
        backgroundWorker1->CancelAsync();
        e->Cancel = true;
        SoundUtils::PlayErrorSound();
    }
}

System::Void
ImportDoveLocationUI::ImportDoveLocationUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    doveFilenameLbl->Text = "";
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    
    if (database->Config().DisableFeaturesForPerformance() &&  database->NumberOfObjects(TObjectType::ETower) > 500)
    {
        allTowers->Checked = false;
        allTowers->Visible = false;
    }
    try
    {
        this->openFileDialog1->InitialDirectory = System::IO::Path::GetDirectoryName(database->DoveDataFile());
        this->openFileDialog1->FileName = System::IO::Path::GetFileName(database->DoveDataFile());
    }
    catch (System::Exception^)
    {
    }
    if (System::IO::File::Exists(database->DoveDataFile()))
    {
        openFileDialog1->InitialDirectory = System::IO::Path::GetDirectoryName(database->DoveDataFile());
        openFileDialog1->FileName = database->DoveDataFile();
        doveFilenameLbl->Text = database->DoveDataFile();
        StartGeneration(false);
    }
    else
    {
        doveFilenameLbl->Text = "Choose dove data file";
    }
}

System::Void
ImportDoveLocationUI::selectAllBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    statusLbl->Text = "";
    System::Collections::IEnumerator^ it = static_cast<System::Collections::IEnumerable^>(dataGridView->Rows)->GetEnumerator();
    while (it->MoveNext())
    {
        DataGridViewCheckBoxCell^ checkBoxCell = static_cast<DataGridViewCheckBoxCell^>(static_cast<DataGridViewRow^>(it->Current)->Cells[0]);
        if (!checkBoxCell->ReadOnly)
        {
            checkBoxCell->Value = true;
        }
    }
}
