#pragma once

namespace Duco
{
    class DucoVersionNumber;
    class DucoConfiguration;
}

namespace DucoUI
{
    /// <summary>
    /// Summary for DownloadNewVersionUI
    /// </summary>
    public ref class DownloadNewVersionUI : public System::Windows::Forms::Form
    {
    public:
        static bool Show(System::Windows::Forms::IWin32Window^ owner, System::Net::WebClient^ client, Duco::DucoVersionNumber& newVersionNumber, System::String^ downloadFilename, Duco::DucoConfiguration& config);

    private:
        DownloadNewVersionUI(System::Net::WebClient^ client, Duco::DucoVersionNumber& newVersionNumber, System::String^ downloadFilename, Duco::DucoConfiguration& theConfig);

    protected:
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        ~DownloadNewVersionUI();

    private: System::Void yesButton_Click(System::Object^  sender, System::EventArgs^  e);
    private: System::Void noButton_Click(System::Object^  sender, System::EventArgs^  e);
    private: System::Void ignoreButton_Click(System::Object^  sender, System::EventArgs^  e);
    private: System::Void DownloadNewVersionUI_Load(System::Object^  sender, System::EventArgs^  e);
    
    private: System::Net::WebClient^ downloadClient;
    private: System::Windows::Forms::Button^  yesButton;
    private: System::Windows::Forms::Button^  noButton;

    private: System::Windows::Forms::Label^  newDownloadLabel;


    private: System::ComponentModel::IContainer^  components;
    private: Duco::DucoConfiguration& config;
    private: Duco::DucoVersionNumber& newVersionNumber;

    private: System::String^ downloadFilepath;
        /// <summary>
        /// Required designer variable.
        /// </summary>


#pragma region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        void InitializeComponent(void)
        {
            this->components = (gcnew System::ComponentModel::Container());
            System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
            System::Windows::Forms::Button^  ignoreButton;
            System::Windows::Forms::ToolTip^  toolTip1;
            this->yesButton = (gcnew System::Windows::Forms::Button());
            this->noButton = (gcnew System::Windows::Forms::Button());
            this->newDownloadLabel = (gcnew System::Windows::Forms::Label());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            ignoreButton = (gcnew System::Windows::Forms::Button());
            toolTip1 = (gcnew System::Windows::Forms::ToolTip(this->components));
            tableLayoutPanel1->SuspendLayout();
            this->SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 3;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 50)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 50)));
            tableLayoutPanel1->Controls->Add(this->newDownloadLabel, 0, 0);
            tableLayoutPanel1->Controls->Add(this->yesButton, 0, 1);
            tableLayoutPanel1->Controls->Add(this->noButton, 1, 1);
            tableLayoutPanel1->Controls->Add(ignoreButton, 2, 1);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 2;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
            tableLayoutPanel1->Size = System::Drawing::Size(388, 96);
            tableLayoutPanel1->TabIndex = 0;
            // 
            // yesButton
            // 
            this->yesButton->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->yesButton->Location = System::Drawing::Point(75, 70);
            this->yesButton->Name = L"yesButton";
            this->yesButton->Size = System::Drawing::Size(75, 23);
            this->yesButton->TabIndex = 0;
            this->yesButton->Text = L"Yes";
            toolTip1->SetToolTip(this->yesButton, L"Start downloading the new version of Duco now.");
            this->yesButton->UseVisualStyleBackColor = true;
            this->yesButton->Click += gcnew System::EventHandler(this, &DownloadNewVersionUI::yesButton_Click);
            // 
            // noButton
            // 
            this->noButton->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            this->noButton->Location = System::Drawing::Point(156, 70);
            this->noButton->Name = L"noButton";
            this->noButton->Size = System::Drawing::Size(75, 23);
            this->noButton->TabIndex = 1;
            this->noButton->Text = L"No";
            toolTip1->SetToolTip(this->noButton, L"Don\'t download this version now");
            this->noButton->UseVisualStyleBackColor = true;
            this->noButton->Click += gcnew System::EventHandler(this, &DownloadNewVersionUI::noButton_Click);
            // 
            // ignoreButton
            // 
            ignoreButton->Location = System::Drawing::Point(237, 70);
            ignoreButton->Name = L"ignoreButton";
            ignoreButton->Size = System::Drawing::Size(75, 23);
            ignoreButton->TabIndex = 2;
            ignoreButton->Text = L"Ignore";
            toolTip1->SetToolTip(ignoreButton, L"Ignore this and all future updates.");
            ignoreButton->UseVisualStyleBackColor = true;
            ignoreButton->Click += gcnew System::EventHandler(this, &DownloadNewVersionUI::ignoreButton_Click);
            // 
            // newDownloadLabel
            // 
            tableLayoutPanel1->SetColumnSpan(this->newDownloadLabel, 3);
            this->newDownloadLabel->Dock = System::Windows::Forms::DockStyle::Fill;
            this->newDownloadLabel->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
            this->newDownloadLabel->Location = System::Drawing::Point(3, 3);
            this->newDownloadLabel->Margin = System::Windows::Forms::Padding(3);
            this->newDownloadLabel->Name = L"newDownloadLabel";
            this->newDownloadLabel->Size = System::Drawing::Size(382, 61);
            this->newDownloadLabel->TabIndex = 1;
            this->newDownloadLabel->Text = L"A new version of Duco is available, do you wish to download it now\?";
            this->newDownloadLabel->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // DownloadNewVersionUI
            // 
            this->AcceptButton = this->yesButton;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->CancelButton = this->noButton;
            this->ClientSize = System::Drawing::Size(388, 96);
            this->ControlBox = false;
            this->Controls->Add(tableLayoutPanel1);
            this->MaximizeBox = false;
            this->MinimizeBox = false;
            this->Name = L"DownloadNewVersionUI";
            this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
            this->Text = L"Download new version of Duco\?";
            this->Load += gcnew System::EventHandler(this, &DownloadNewVersionUI::DownloadNewVersionUI_Load);
            tableLayoutPanel1->ResumeLayout(false);
            this->ResumeLayout(false);

        }
#pragma endregion
};
}
