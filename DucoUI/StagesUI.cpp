#include "StagesUI.h"
#include "DatabaseManager.h"
#include "SystemDefaultIcon.h"
#include "SoundUtils.h"
#include "NewStageUI.h"
#include "DucoUtils.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::Windows::Forms;

StagesUI::StagesUI(DatabaseManager^ theDatabase)
    :   database(theDatabase), loadingComplete(false)
{
    InitializeComponent();
}

StagesUI::~StagesUI()
{
    this->!StagesUI();
    if (components)
    {
        delete components;
    }
}

StagesUI::!StagesUI()
{
    database->RemoveObserver(this);
}

System::Void
StagesUI::StagesUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    GenerateOrders();
    loadingComplete = true;
    database->AddObserver(this);
}

System::Void
StagesUI::GenerateOrders()
{
    ordersDisplay->Rows->Clear();

    unsigned int j = 0;
    for (unsigned int i = 0; i < database->NumberOfObjects(TObjectType::EOrderName);)
    {
        System::String^ orderName = "";
        if (database->FindOrderName(j, orderName))
        {
            DataGridViewRow^ newRow = gcnew DataGridViewRow();
            newRow->HeaderCell->Value = Convert::ToString(j);
            DataGridViewCellCollection^ theCells = newRow->Cells;

            //Type Column;
            DataGridViewTextBoxCell^ typeCell = gcnew DataGridViewTextBoxCell();
            typeCell->Value = orderName;
            theCells->Add(typeCell);

            // Add Row
            ordersDisplay->Rows->Add(newRow);

            ++i;
        }
        ++j;
    }
}

System::Void
StagesUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void
StagesUI::addBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
    }
    else
    {
        NewStageUI^ stageUI = gcnew NewStageUI(database);
        stageUI->ShowDialog();
        GenerateOrders();
    }
}

System::Void
StagesUI::ordersDisplay_CellValueChanged(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    if (loadingComplete)
    {
        try
        {
            DataGridViewRow^ currentRow = ordersDisplay->Rows[e->RowIndex];
            DataGridViewCell^ currentCell = currentRow->Cells[e->ColumnIndex];
            unsigned int orderNumber = Convert::ToInt16(currentRow->HeaderCell->Value);
            database->UpdateOrderName(orderNumber, Convert::ToString(currentCell->Value), true);
        }
        catch (Exception^)
        {
            SoundUtils::PlayErrorSound();
        }
    }
}

System::Void
StagesUI::ordersDisplay_SelectionChanged(System::Object^  sender, System::EventArgs^  e)
{
    deleteBtn->Enabled = ordersDisplay->SelectedCells->Count == 1;
}

System::Void
StagesUI::deleteBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->InternaliseExternaliseInProgress() || ordersDisplay->SelectedCells->Count != 1)
    {
        SoundUtils::PlayErrorSound();
        return;
    }

    DataGridViewRow^ selectedRow = ordersDisplay->Rows[ordersDisplay->SelectedCells[0]->RowIndex];

    unsigned int orderNumberToDelete = Convert::ToInt16(selectedRow->HeaderCell->Value);
    std::set<unsigned int> methodOrders;
    database->GetAllMethodOrders(methodOrders);

    if (methodOrders.find(orderNumberToDelete) == methodOrders.end())
    {
        if (database->RemoveOrderName(orderNumberToDelete))
        {
            SoundUtils::PlayFinishedSound();
        }
        else
        {
            SoundUtils::PlayErrorSound();
        }
    }
    else
    {
        MessageBox::Show("Cannot delete " + selectedRow->Cells[0]->Value + " as it is used in the database", "Cannot delete " + selectedRow->Cells[0]->Value, MessageBoxButtons::OK, MessageBoxIcon::Warning);
        SoundUtils::PlayErrorSound();
    }
}

void
StagesUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    switch (type)
    {
    case TObjectType::EOrderName:
        loadingComplete = false;
        GenerateOrders();
        loadingComplete = true;
        break;
    default:
        break;
    }
}
