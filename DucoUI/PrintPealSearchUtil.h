#pragma once
#include "PrintUtilBase.h"

namespace DucoUI
{
public ref class PrintPealSearchUtil : public PrintUtilBase
{
public:
    PrintPealSearchUtil(DucoUI::DatabaseManager^ theDatabase, System::Drawing::Printing::PrintPageEventArgs^ printArgs);
    void SetObjects(System::Windows::Forms::DataGridView^ theDataGridView);
    virtual System::Void printObject(System::Drawing::Printing::PrintPageEventArgs^ args, System::Drawing::Printing::PrinterSettings^ printSettings) override;

protected:
    System::Windows::Forms::DataGridView^               dataGridView;
    System::Collections::Generic::List<System::Single>^ tabStops;
};
}