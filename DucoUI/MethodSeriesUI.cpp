#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"
#include "DucoWindowState.h"
#include <MethodSeries.h>

#include "MethodSeriesUI.h"

#include "SoundUtils.h"
#include "DucoUtils.h"
#include "SystemDefaultIcon.h"
#include "DatabaseGenUtils.h"
#include "DucoTableSorter.h"
#include "ProgressWrapper.h"
#include <DatabaseSearch.h>
#include "SearchUtils.h"
#include "PrintPealSearchUtil.h"
#include "PealSearchUI.h"
#include "MethodUI.h"
#include "InputIndexQueryUI.h"
#include "PrintMethodSeriesUtil.h"
#include "SeriesRingersUI.h"
#include "DucoUIUtils.h"
#include "SelectMethodUI.h"
#include <Method.h>
#include <DucoConfiguration.h>
#include <RingingDatabase.h>
#include <DatabaseSettings.h>
#include <PealLengthInfo.h>
#include <DucoEngineUtils.h>

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Drawing::Printing;

void
MethodSeriesUI::ShowSeries(DatabaseManager^ theDatabase, System::Windows::Forms::Form^ parent, const Duco::ObjectId& methodSeriesId)
{
    if (methodSeriesId.ValidId())
    {
        MethodSeriesUI^ seriesUI = gcnew MethodSeriesUI(theDatabase, methodSeriesId);
        seriesUI->MdiParent = parent;
        seriesUI->Show();
    }
}

void
MethodSeriesUI::CreateSeries(DucoUI::DatabaseManager^ theDatabase, System::Windows::Forms::Form^ parent, const std::set<Duco::ObjectId>& methodIds, Duco::ObjectId& methodSeriesId)
{
    MethodSeriesUI^ seriesUI = gcnew MethodSeriesUI(theDatabase, methodIds);
    seriesUI->ShowDialog();
    methodSeriesId = seriesUI->currentMethodSeries->Id();
    delete seriesUI;
}

MethodSeriesUI::MethodSeriesUI(DucoUI::DatabaseManager^ theDatabase)
: database(theDatabase), windowState(DucoWindowState::EViewMode), populatingFromPealUIDialog(false)
{
    previousSeriesId = new Duco::ObjectId();
    currentMethodSeries = new MethodSeries();
    allMethodSeriesIds = gcnew System::Collections::Generic::List<System::Int16>();
	InitializeComponent();
}

MethodSeriesUI::MethodSeriesUI(DucoUI::DatabaseManager^ theDatabase, const Duco::ObjectId& methodSeriesId)
: database(theDatabase), windowState(DucoWindowState::EViewMode), populatingFromPealUIDialog(false)
{
    previousSeriesId = new Duco::ObjectId();
    currentMethodSeries = new MethodSeries();
    database->FindMethodSeries(methodSeriesId, *currentMethodSeries, false);
    allMethodSeriesIds = gcnew System::Collections::Generic::List<System::Int16>();
    InitializeComponent();
}

MethodSeriesUI::MethodSeriesUI(DucoUI::DatabaseManager^ theDatabase, const std::set<Duco::ObjectId>& methodIds)
: database(theDatabase), windowState(DucoWindowState::ENewMode), populatingFromPealUIDialog(true)
{
    previousSeriesId = new Duco::ObjectId();
    currentMethodSeries = new MethodSeries();
    currentMethodSeries->AddMethods(methodIds, database->Database());
    allMethodSeriesIds = gcnew System::Collections::Generic::List<System::Int16>();
    InitializeComponent();
}

MethodSeriesUI::!MethodSeriesUI()
{
    delete previousSeriesId;
    delete currentMethodSeries;
}

MethodSeriesUI::~MethodSeriesUI()
{
    this->!MethodSeriesUI();
	if (components)
	{
		delete components;
	}
}

System::Void
MethodSeriesUI::ClosingEvent(System::Object^ object, System::ComponentModel::CancelEventArgs^ e)
{
    if (dataChanged)
    {
        if (DucoUtils::ConfirmLooseChanges("method series", this))
        {
            e->Cancel = true;
        }
    }
    if (!e->Cancel)
    {
        database->CancelEdit(TObjectType::EMethodSeries, currentMethodSeries->Id());
        database->RemoveObserver(this);
    }
}

void
MethodSeriesUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    switch (type)
    {
    default:
        break;

    case TObjectType::EMethodSeries:
        switch (eventId)
        {
        case EDeleted:
        case EAdded:
            UpdateIdLabel();
            break;
        case EUpdated:
            if (windowState != DucoWindowState::EEditMode && id == currentMethodSeries->Id())
            {
                PopulateView(currentMethodSeries->Id());
            }
            break;

        default:
            break;
        }
        break;

    case TObjectType::EMethod:
        {
        switch (eventId)
            {   
            default:
                if (windowState != DucoWindowState::EEditMode && windowState != DucoWindowState::ENewMode)
                {
                    PopulateView(currentMethodSeries->Id());
                }
                break;
            case EAdded:
                break;
            }
        }
    }
}

System::Void
MethodSeriesUI::MethodSeriesUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    this->Icon = SystemDefaultIcon::DefaultSystemIcon();

    DatabaseGenUtils::GenerateMethodSeriesOptions(seriesName, allMethodSeriesIds, database, -1, -1, false);
    if (windowState == DucoWindowState::ENewMode)
    {
        SetNewState();
        ClearView();
        PopulateView(*currentMethodSeries);
    }
    else
    {
        SetViewState();

        if (currentMethodSeries->Id().ValidId())
        {
            PopulateView(currentMethodSeries->Id());
        }
        else
        {
            Duco::ObjectId nextSeriesId;
            if (database->FirstMethodSeries(nextSeriesId, false))
            {
                PopulateView(nextSeriesId);
            }
            else
            {
                ClearView();
                SoundUtils::PlayErrorSound();
            }
        }
    }
    database->AddObserver(this);
}

System::Void
MethodSeriesUI::SetViewState()
{
    windowState = DucoWindowState::EViewMode;

    seriesName->Enabled = false;
    noOfMethods->Enabled = false;
    singleOrder->Enabled = false;

    createNewSeriesButton->Visible = true;
    statsBtn->Visible = true;
    printBtn->Visible = true;
    startBtn->Visible = true;
    back10Btn->Visible = true;
    previousBtn->Visible = true;
    nextBtn->Visible = true;
    forward10Btn->Visible = true;
    endBtn->Visible = true;
    editBtn->Visible = true;
    saveBtn->Visible = false;
    closeBtn->Text = "Close";
    addMethodBtn->Enabled = false;
    removeMethodBtn->Enabled = false;
    dataGridView->ReadOnly = true;

    deleteColumn->Visible = false;

    bool enableBtns = database->NumberOfObjects(TObjectType::EMethodSeries) > 0;
    startBtn->Enabled = enableBtns;
    back10Btn->Enabled = enableBtns;
    previousBtn->Enabled = enableBtns;
    nextBtn->Enabled = enableBtns;
    forward10Btn->Enabled = enableBtns;
    endBtn->Enabled = enableBtns;
    editBtn->Enabled = enableBtns;
}

System::Void
MethodSeriesUI::SetEditState()
{
    windowState = DucoWindowState::EEditMode;

    seriesName->Enabled = true;
    noOfMethods->Enabled = true;
    singleOrder->Enabled = true;

    createNewSeriesButton->Visible = false;
    statsBtn->Visible = false;
    printBtn->Visible = false;
    startBtn->Visible = false;
    back10Btn->Visible = false;
    previousBtn->Visible = false;
    nextBtn->Visible = false;
    forward10Btn->Visible = false;
    endBtn->Visible = false;
    editBtn->Visible = false;
    saveBtn->Visible = true;
    addMethodBtn->Enabled = true;
    removeMethodBtn->Enabled = true;
    dataGridView->ReadOnly = false;

    deleteColumn->Visible = true;

    closeBtn->Text = "Cancel";
}

System::Void
MethodSeriesUI::SetNewState()
{
    windowState = DucoWindowState::ENewMode;

    seriesName->Enabled = true;
    noOfMethods->Enabled = true;
    singleOrder->Enabled = true;

    createNewSeriesButton->Visible = false;
    startBtn->Visible = false;
    back10Btn->Visible = false;
    previousBtn->Visible = false;
    nextBtn->Visible = false;
    forward10Btn->Visible = false;
    endBtn->Visible = false;
    statsBtn->Visible = false;
    printBtn->Visible = false;
    editBtn->Visible = false;
    saveBtn->Visible = true;
    addMethodBtn->Enabled = true;
    removeMethodBtn->Enabled = true;
    dataGridView->ReadOnly = false;

    deleteColumn->Visible = true;

    closeBtn->Text = "Cancel";
}

System::Void
MethodSeriesUI::PopulateView(const Duco::ObjectId& nextSeriesId)
{
    ResetAllSortGlyphs();
    MethodSeries nextMethodSeries;
    if (!database->FindMethodSeries(nextSeriesId, nextMethodSeries, false))
    {
        ClearView();
    }
    else
    {
        PopulateView(nextMethodSeries);
    }
}

System::Void
MethodSeriesUI::PopulateView(const Duco::MethodSeries& series)
{
    ResetAllSortGlyphs();
    (*currentMethodSeries) = series;
    seriesName->Text = DucoUtils::ConvertString(currentMethodSeries->Name());
    noOfMethods->Value = currentMethodSeries->NoOfMethods();
    PopulateMethods(*currentMethodSeries);

    PerformanceDate completedDate;
    PealLengthInfo info;
    size_t remaining = 0;
    if (currentMethodSeries->SeriesCompletedDate(database->Database(), completedDate, remaining, info))
    {
        completedDateLbl->Text = "Completed";
        completedDateDisplay->Text = DucoUtils::ConvertString(completedDate.Str());
    }
    else
    {
        completedDateLbl->Text = "Remaining";
        completedDateDisplay->Text = Convert::ToString(remaining);
    }
    singleOrder->Checked = currentMethodSeries->SingleOrder();
    ShowNumberOfActualMethods();
    UpdateIdLabel();
    dataChanged = false;
}

System::Void
MethodSeriesUI::ShowNumberOfActualMethods()
{
    noOfActualMethodsLbl->Text = Convert::ToString(currentMethodSeries->NoOfExistingMethods()) + " methods defined";
}

System::Void
MethodSeriesUI::PopulateMethods(const MethodSeries& methodSeries)
{
    dataGridView->Rows->Clear();
    std::set<Duco::ObjectId> methods = methodSeries.Methods();
    std::set<Duco::ObjectId>::const_iterator it = methods.begin();
    while (it != methods.end())
    {
        AddMethod(*it);
        ++it;
    }

    // Add date whole series rung, if at all
    PerformanceDate firstPealDate;
    
    System::Collections::Generic::List<System::UInt64>^ pealIds = database->PealsInSeries(methodSeries.Id(), firstPealDate);
    if (pealIds->Count > 0)
    {
        System::Windows::Forms::DataGridViewCellStyle^ completedStyle = (gcnew System::Windows::Forms::DataGridViewCellStyle(dataGridView->DefaultCellStyle));
        completedStyle->ForeColor = System::Drawing::SystemColors::GrayText;

        DataGridViewRow^ newRow = gcnew DataGridViewRow();
        DataGridViewCellCollection^ theCells = newRow->Cells;

        //Id Column;
        DataGridViewTextBoxCell^ idCell = gcnew DataGridViewTextBoxCell();
        theCells->Add(idCell);

        //Delete Column;
        DataGridViewCheckBoxCell^ deleteCell = gcnew DataGridViewCheckBoxCell();
        theCells->Add(deleteCell);

        //Method Column;
        DataGridViewTextBoxCell^ methodCell = gcnew DataGridViewTextBoxCell();
        methodCell->Style = completedStyle;
        methodCell->Value = "Date series rung";
        theCells->Add(methodCell);

        //Date Rung Column;
        DataGridViewTextBoxCell^ dateRungCell = gcnew DataGridViewTextBoxCell();
        dateRungCell->Style = completedStyle;
        dateRungCell->Value = DucoUtils::ConvertString(firstPealDate.Str());
        theCells->Add(dateRungCell);

        //Peal count Column;
        DataGridViewTextBoxCell^ pealCountCell = gcnew DataGridViewTextBoxCell();
        pealCountCell->Style = completedStyle;
        pealCountCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(pealIds->Count, true));
        theCells->Add(pealCountCell);

        // Add Row
        dataGridView->Rows->Add(newRow);
    }
}

System::Void
MethodSeriesUI::ClearView()
{
    seriesName->Text = "";
    noOfMethods->Value = 0;
    dataGridView->Rows->Clear();
    completedDateDisplay->Text = "";
    noOfActualMethodsLbl->Text = "";
    singleOrder->Checked = true;
    seriesIdLabel->Text = "";
    dataChanged = false;
}

System::Void
MethodSeriesUI::CloseBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    switch (windowState)
    {
    case DucoUI::DucoWindowState::EViewMode:
        Close();
        break;

    case DucoUI::DucoWindowState::EEditMode:
        database->CancelEdit(TObjectType::EMethodSeries, currentMethodSeries->Id());
        PopulateView(currentMethodSeries->Id());
        SetViewState();
        break;

    case DucoUI::DucoWindowState::ENewMode:
        PopulateView(*previousSeriesId);
        SetViewState();
        break;

    default:
        break;
    }
}

System::Void
MethodSeriesUI::CreateNewSeriesButton_Click(System::Object^  sender, System::EventArgs^  e)
{
    *previousSeriesId = currentMethodSeries->Id();
    delete currentMethodSeries;
    currentMethodSeries = new MethodSeries();
    ClearView();
    SetNewState();
}

System::Void
MethodSeriesUI::PreviousBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (windowState == DucoWindowState::EViewMode)
    {
        Duco::ObjectId nextMethodSeriesId = currentMethodSeries->Id();
        if (database->NextMethodSeries(nextMethodSeriesId, false, false))
        {
            PopulateView(nextMethodSeriesId);
        }
        else
        {
            SoundUtils::PlayErrorSound();
        }
    }
}

System::Void
MethodSeriesUI::Back10Btn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (windowState == DucoWindowState::EViewMode)
    {
        Duco::ObjectId nextMethodSeriesId = currentMethodSeries->Id();
        if (database->ForwardMultipleMethodSeries(nextMethodSeriesId, false))
        {
            PopulateView(nextMethodSeriesId);
        }
        else
        {
            SoundUtils::PlayErrorSound();
        }
    }
}

System::Void
MethodSeriesUI::Forward10Btn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (windowState == DucoWindowState::EViewMode)
    {
        Duco::ObjectId nextMethodSeriesId = currentMethodSeries->Id();
        if (database->ForwardMultipleMethodSeries(nextMethodSeriesId, true))
        {
            PopulateView(nextMethodSeriesId);
        }
        else
        {
            SoundUtils::PlayErrorSound();
        }
    }
}

System::Void
MethodSeriesUI::StatsBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    SeriesRingersUI^ statsUI = gcnew SeriesRingersUI(database, currentMethodSeries->Id());
    statsUI->MdiParent = this->MdiParent;
    statsUI->Show();
}

System::Void
MethodSeriesUI::PrintBtn_Click(System::Object^ sender, System::EventArgs^  e)
{
    bool landscape = true;
    if (currentMethodSeries->NoOfBells() >= 7)
    {
        landscape = false;
    }
    DucoUIUtils::Print(this, sender, gcnew System::Drawing::Printing::PrintPageEventHandler( this, &MethodSeriesUI::PrintMethodSeries ), "Method series", database->Database().Settings().UsePrintPreview(), landscape);
}

System::Void 
MethodSeriesUI::PrintMethodSeries(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args)
{
    PrintDocument^ printDoc = static_cast<PrintDocument^>(sender);

    PrintMethodSeriesUtil printUtil(database, args);
    printUtil.SetObjects(currentMethodSeries);
    printUtil.printObject(args, printDoc->PrinterSettings);
}


System::Void
MethodSeriesUI::NextBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (windowState == DucoWindowState::EViewMode)
    {
        Duco::ObjectId nextMethodSeriesId = currentMethodSeries->Id();
        if (database->NextMethodSeries(nextMethodSeriesId, true, false))
        {
            PopulateView(nextMethodSeriesId);
        }
        else
        {
            SoundUtils::PlayErrorSound();
        }
    }
}

System::Void
MethodSeriesUI::StartBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (windowState == DucoWindowState::EViewMode)
    {
        Duco::ObjectId nextMethodSeriesId;
        if (database->FirstMethodSeries(nextMethodSeriesId, false))
        {
            PopulateView(nextMethodSeriesId);
        }
        else
        {
            SoundUtils::PlayErrorSound();
        }
    }
}

System::Void
MethodSeriesUI::EndBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (windowState == DucoWindowState::EViewMode)
    {
        Duco::ObjectId nextMethodSeriesId;
        if (database->LastMethodSeries(nextMethodSeriesId))
        {
            PopulateView(nextMethodSeriesId);
        }
        else
        {
            SoundUtils::PlayErrorSound();
        }
    }
}


System::Void
MethodSeriesUI::EditBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->StartEdit(TObjectType::EMethodSeries, currentMethodSeries->Id()))
        SetEditState();
    else
        SoundUtils::PlayErrorSound();
}

System::Void
MethodSeriesUI::SaveBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (dataChanged)
    {
        if (!currentMethodSeries->Valid(database->Database(), false, true))
        {
            MessageBox::Show(this, "Please fix the following error(s) before saving: " + DucoUtils::ConvertString(currentMethodSeries->ErrorString(database->Settings(), database->Config().IncludeWarningsInValidation())) + ".", "Please check all the data is entered correctly", MessageBoxButtons::OKCancel);
            return;
        }
    }

    switch (windowState)
    {
    case DucoWindowState::ENewMode:
        {
            PopulateView(database->AddMethodSeries(*currentMethodSeries));
            if (populatingFromPealUIDialog)
            {
                Close();
            }
        }
        break;

    default:
        {
        database->UpdateMethodSeries(*currentMethodSeries, true);
        PopulateView(currentMethodSeries->Id());
        }
        break;
    }
    if (!populatingFromPealUIDialog)
        SetViewState();
}

System::Void
MethodSeriesUI::RemoveMethodFromSeriesBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    DataGridViewRowCollection^ rows = dataGridView->Rows;
    
    bool methodRemoved = false;
    for (int i = 0; i < rows->Count; ++i)
    {
        DataGridViewRow^ theRow = rows[i];
        DataGridViewCheckBoxCell^ removeCell = static_cast<DataGridViewCheckBoxCell^>(theRow->Cells[1]);
        if (Convert::ToBoolean(removeCell->Value))
        {
            DataGridViewTextBoxCell^ idCell = static_cast<DataGridViewTextBoxCell^>(theRow->Cells[0]);
            unsigned int methodId = -1;
            DucoUtils::ConvertTextToNumber(Convert::ToString(idCell->Value), methodId);
            if (methodId != -1)
            {
                currentMethodSeries->RemoveMethod(methodId);
                methodRemoved = true;
            }
        }
    }
    if (methodRemoved)
    {
        PopulateMethods(*currentMethodSeries);
        ShowNumberOfActualMethods();
    }
}

System::Void
MethodSeriesUI::AddMethodToSeriesBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId methId = -1;
    SelectMethodUI^ methodUI = gcnew SelectMethodUI(database, methId, *currentMethodSeries);
    methodUI->ShowDialog();
    if (methId.ValidId())
    {
        if (currentMethodSeries->AddMethod(methId, database->Database()))
        {
            AddMethod(methId);
            noOfMethods->Value = currentMethodSeries->NoOfMethods();
            ShowNumberOfActualMethods();
        }
    }
}

System::Void
MethodSeriesUI::AddMethod(const Duco::ObjectId& methodId)
{
    DataGridViewRow^ newRow = gcnew DataGridViewRow();
    DataGridViewCellCollection^ theCells = newRow->Cells;

    Duco::Method theMethod;
    database->FindMethod(methodId, theMethod, false);
    //Id Column;
    DataGridViewTextBoxCell^ idCell = gcnew DataGridViewTextBoxCell();
    idCell->Value = DucoUtils::ConvertString(methodId);
    theCells->Add(idCell);

    //Delete Column;
    DataGridViewCheckBoxCell^ deleteCell = gcnew DataGridViewCheckBoxCell();
    theCells->Add(deleteCell);

    //Method Column;
    DataGridViewTextBoxCell^ methodCell = gcnew DataGridViewTextBoxCell();
    if (theMethod.Id().ValidId())
        methodCell->Value = DucoUtils::ConvertString(theMethod.FullName(database->Database()));
    theCells->Add(methodCell);

    //Date Rung Column;
    DataGridViewTextBoxCell^ dateRungCell = gcnew DataGridViewTextBoxCell();
    PerformanceDate dateMethodRung;
    System::Collections::Generic::List<System::UInt64>^ noOfPeals = database->PealsInMethod(methodId, dateMethodRung);
    if (noOfPeals->Count > 0)
    {
        dateRungCell->Value = DucoUtils::ConvertString(dateMethodRung.Str());
    }
    theCells->Add(dateRungCell);
    //Number Column;
    DataGridViewTextBoxCell^ numberCell = gcnew DataGridViewTextBoxCell();
    numberCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(noOfPeals->Count, true));
    theCells->Add(numberCell);

    // Add Row
    dataGridView->Rows->Add(newRow);
}

System::Void
MethodSeriesUI::SeriesName_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (IsEditState())
    {
        std::wstring newName;
        DucoUtils::ConvertString(seriesName->Text, newName);
        currentMethodSeries->SetName(newName);
        dataChanged = true;
    }
}

System::Void
MethodSeriesUI::NoOfMethods_ValueChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (IsEditState())
    {
        if (noOfMethods->Value >= currentMethodSeries->NoOfExistingMethods())
        {
            currentMethodSeries->SetNoOfMethods(Convert::ToInt16(noOfMethods->Value));
        }
        else
            noOfMethods->Value = currentMethodSeries->NoOfMethods();

        dataChanged = true;
    }
}

System::Void
MethodSeriesUI::SingleOrder_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (IsEditState())
    {
        if (currentMethodSeries->SetSingleOrder(singleOrder->Checked, database->Database()))
        {
            dataChanged = true;
        }
        else
        {
            singleOrder->Checked = !singleOrder->Checked;
        }
    }
}

System::Void
MethodSeriesUI::DataGridView_CellDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    try
    {
        unsigned int rowIndex = e->RowIndex;
        DataGridViewRow^ currentRow = dataGridView->Rows[rowIndex];
        if (currentRow->Cells[0]->Value != nullptr)
        {
            Duco::ObjectId methodId = Convert::ToInt16(currentRow->Cells[0]->Value);
            if (e->ColumnIndex == 4)
            {
                Duco::PerformanceDate firstPealDate;
                System::Collections::Generic::List<System::UInt64>^ pealIds = database->PealsInMethod(methodId, firstPealDate);
                if (pealIds->Count >= 1)
                {
                    PealSearchUI^ pealSearchUI = gcnew PealSearchUI(database, false);
                    pealSearchUI->Show(this->MdiParent, pealIds);
                }
                else
                {
                    SoundUtils::PlayErrorSound();
                }
            }
            else
            {
                MethodUI::ShowMethod(database, this->MdiParent, methodId);
            }
        }
        else
        {
            Duco::PerformanceDate firstPealDate2;
            System::Collections::Generic::List<System::UInt64>^ pealIds2 = database->PealsInSeries(currentMethodSeries->Id(), firstPealDate2);
            {
                PealSearchUI^ pealSearchUI = gcnew PealSearchUI(database, false);
                pealSearchUI->Show(this->MdiParent, pealIds2);
            }
        }
    }
    catch (Exception^)
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
MethodSeriesUI::DataGridView_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
    DataGridViewColumn^ newColumn = dataGridView->Columns[e->ColumnIndex];
    SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;
    ResetProgrammicSortGlyphs();
    if (newColumn->SortMode != DataGridViewColumnSortMode::Programmatic)
    {
        return;
    }
    ResetAllSortGlyphs();

    if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
        newSortOrder = SortOrder::Ascending;
    else
        newSortOrder = SortOrder::Descending;

    newColumn->HeaderCell->SortGlyphDirection = newSortOrder;
    DucoTableSorter^ sorter = gcnew DucoTableSorter(newSortOrder == SortOrder::Ascending, EString);

    switch (e->ColumnIndex)
    {
    case 3:
        {
        sorter->AddSortObject(EDate, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
        }
        break;
    case 4:
        {
        sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
        }
        break;
    default:
        break;
    }
    dataGridView->Sort(sorter);
}

System::Void
MethodSeriesUI::ResetAllSortGlyphs()
{
    deleteColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
    methodName->HeaderCell->SortGlyphDirection = SortOrder::None;
    rungDateColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
    numberRungColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
}

System::Void
MethodSeriesUI::ResetProgrammicSortGlyphs()
{
    rungDateColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
    numberRungColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
}

System::Void
MethodSeriesUI::SeriesIdLabel_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (windowState == DucoWindowState::EViewMode)
    {
        System::Int16 id = -1;
        InputIndexQueryUI^ findIdDialog = gcnew InputIndexQueryUI("Method series id:", id);
        System::Windows::Forms::DialogResult returnVal = findIdDialog->ShowDialog();
        if (returnVal == ::DialogResult::OK)
        {
            PopulateView(id);
        }
    }
}

System::Void
MethodSeriesUI::UpdateIdLabel()
{
    if (currentMethodSeries->Id() != -1)
        seriesIdLabel->Text = String::Format("{0:D}/{1:D}", currentMethodSeries->Id().Id(), database->NumberOfObjects(TObjectType::EMethodSeries));
    else
        seriesIdLabel->Text = "New Method series";
}

System::Boolean
MethodSeriesUI::IsEditState()
{
    return (windowState == DucoWindowState::ENewMode || windowState == DucoWindowState::EEditMode);
}
