#pragma once

#include "PrintUtilBase.h"

namespace Duco
{
    class MethodSeries;
}

namespace DucoUI
{
public ref class PrintMethodSeriesUtil : public PrintUtilBase
{
public:
    PrintMethodSeriesUtil(DucoUI::DatabaseManager^ theDatabase, System::Drawing::Printing::PrintPageEventArgs^ printArgs);
    void SetObjects(const Duco::MethodSeries* theMethodSeries);
    virtual System::Void printObject(System::Drawing::Printing::PrintPageEventArgs^ args, System::Drawing::Printing::PrinterSettings^ printSettings) override;

protected:
    const Duco::MethodSeries*   currentSeries;
};

}