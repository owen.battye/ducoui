#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"

#include "PealTableUI.h"

#include <DownloadedPeal.h>
#include "DucoUILog.h"
#include "DucoUtils.h"
#include "DucoWindowState.h"
#include <Method.h>
#include "PrintBandSummaryUtil.h"
#include <Peal.h>
#include <PealDatabase.h>
#include "OpenObjectCallback.h"
#include "PictureListControl.h"
#include "PictureControl.h"
#include "RingerList.h"
#include "PealUI.h"
#include <RingingDatabase.h>
#include "SystemDefaultIcon.h"
#include <Tower.h>
#include <Ring.h>
#include <DucoEngineUtils.h>

using namespace DucoUI;
using namespace Duco;
using namespace System;
using namespace System::Windows::Forms;

void
PealTableUI::ShowAllPeals(DucoUI::DatabaseManager^ theDatabase, System::Windows::Forms::Form^ parent)
{
    PealTableUI^ ui = gcnew PealTableUI(theDatabase);
    ui->MdiParent = parent;
    ui->Show();
}

PealTableUI::PealTableUI(DucoUI::DatabaseManager^ theDatabase)
    : database(theDatabase), associationColumnUsed(false), timeColumnUsed (false)
{
    pealIds = gcnew System::Collections::Generic::List<UInt64>();
    InitializeComponent();
}

PealTableUI::!PealTableUI()
{
    database->RemoveObserver(this);
}

PealTableUI::~PealTableUI()
{
    this->!PealTableUI();
    if (components)
    {
        delete components;
    }
}

void
PealTableUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    if (eventId == DucoUI::TEventType::EDeleted && type == Duco::TObjectType::EPeal)
    {
        pealIds->Remove(id.Id());
        dataGridView1->RowCount -= 1;
    }
    else if (eventId == DucoUI::TEventType::EAdded && type == Duco::TObjectType::EPeal)
    {
        if (id.ValidId())
        {
            pealIds->Insert(0, id.Id());
            dataGridView1->RowCount += 1;
        }
        else
        {
            LoadData();
        }
    }
    dataGridView1->Invalidate();
}

System::Void
PealTableUI::PealTableUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    DUCOUILOGDEBUG("PealTableUI_Load");
    this->Icon = SystemDefaultIcon::DefaultSystemIcon();
    LoadData();
    database->AddObserver(this);
}

System::Void
PealTableUI::LoadData()
{
    pealIds->Clear();
    std::set<Duco::ObjectId> allIds;
    database->Database().PealsDatabase().GetAllObjectIds(allIds);
    std::set<Duco::ObjectId>::const_reverse_iterator it = allIds.rbegin();
    while (it != allIds.rend())
    {
        pealIds->Add(it->Id());
        ++it;
    }
    dataGridView1->RowCount = (int)database->NumberOfObjects(Duco::TObjectType::EPeal);
}

System::Void
PealTableUI::SetCellStyle(Duco::Peal& foundPeal, int rowIndex, int columnIndex)
{
    if (foundPeal.Withdrawn())
    {
        DataGridViewRow^ currentRow = dataGridView1->Rows[rowIndex];
        DataGridViewCell^ cell = currentRow->Cells[columnIndex];
        cell->Style->ForeColor = System::Drawing::SystemColors::GrayText;
    }
}

System::Void
PealTableUI::dataGridView1_CellValueNeeded(System::Object^  sender, System::Windows::Forms::DataGridViewCellValueEventArgs^  e)
{
    Duco::ObjectId pealIdToFind(pealIds[e->RowIndex]);
    DUCOUILOGDEBUG("PealTableUI::dataGridView1_CellValueNeeded Row: " + Convert::ToString(pealIdToFind.Id()) + ", Column: " + e->ColumnIndex.ToString());

    Duco::Peal foundPeal;
    if (database->FindPeal(pealIdToFind, foundPeal, true))
    {
        if (foundPeal.Withdrawn())
        {
            SetCellStyle(foundPeal, e->RowIndex, e->ColumnIndex);
        }
        switch (e->ColumnIndex)
        {
        case 0: // Date;
        {
            e->Value = DucoUI::DucoUtils::ConvertString(foundPeal.Date().DateWithLongerMonth());
            dataGridView1->Rows[e->RowIndex]->HeaderCell->Value = Convert::ToString(pealIdToFind.Id());
        }
        break;

        case 1: // Association;
        {
            System::String^ associationStr = DucoUI::DucoUtils::ConvertString(foundPeal.AssociationName(database->Database()));
            e->Value = associationStr;
            if (!String::IsNullOrEmpty(associationStr))
            {
                associationColumnUsed = true;
            }
            if (!associationColumnUsed)
            {
                AssociationColumn->Visible = false;
            }
        }
        break;
        case 2: // Changes;
        {
            e->Value = DucoUI::DucoUtils::ConvertString(DucoEngineUtils::ToString(foundPeal.NoOfChanges(), true));
        }
        break;
        case 3: // Method;
        {
            Duco::Method foundMethod;
            if (database->FindMethod(foundPeal.MethodId(), foundMethod, true))
            {
                e->Value = DucoUI::DucoUtils::ConvertString(foundMethod.FullName(database->Database()));
            }
        }
        break;
        case 4: // Tower or Hand
        {
            if (foundPeal.Handbell())
            {
                e->Value = "H";
            }
            else
            {
                e->Value = "T";
            }
        }
        break;
        case 5: // Tower;
        {
            Duco::Tower foundTower;
            if (database->FindTower(foundPeal.TowerId(), foundTower, true))
            {
                e->Value = DucoUI::DucoUtils::ConvertString(foundTower.FullName());
                const Duco::Ring* const foundRing = foundTower.FindRing(foundPeal.RingId());
                if (foundRing != NULL)
                {
                    try
                    {
                        String^ tenorDescription = DucoUI::DucoUtils::ConvertString(foundRing->TenorDescription());
                        if (tenorDescription->Length > 0)
                        {
                            e->Value += " (" + tenorDescription + ")";
                        }
                    }
                    catch (Exception^ ex)
                    {
                        Console::WriteLine(ex);
                    }
                }
            }
        }
        break;
        case 6: // Peal time;
        {
            e->Value = DucoUI::DucoUtils::ConvertString(foundPeal.Time().PrintPealTime(false));
            if (foundPeal.Time().Valid())
            {
                timeColumnUsed = true;
            }
            if (!timeColumnUsed)
            {
                PealTimeColumn->Visible = false;
            }
        }
        break;
        case 7: // Conductor;
        {
            e->Value = DucoUI::DucoUtils::ConvertString(foundPeal.ConductorName(database->Database()));
        }
        break;
        default:
            e->Value = "Field missing";
            break;
        }
    }
    else
    {
        e->Value = "Peal missing";
    }
}

System::Void
PealTableUI::dataGridView1_CellDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    Duco::ObjectId pealIdToFind(pealIds[e->RowIndex]);
    PealUI::ShowPeal(database, MdiParent, pealIdToFind);
}

System::Void
PealTableUI::dataGridView1_Scroll(System::Object^ sender, System::Windows::Forms::ScrollEventArgs^ e)
{
    if (e->ScrollOrientation == ScrollOrientation::VerticalScroll)
    {
        dataGridView1->AutoResizeColumns(DataGridViewAutoSizeColumnsMode::DisplayedCellsExceptHeader);
    }
}
