#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"

#include "UnpaidPealFeesUI.h"
#include "SystemDefaultIcon.h"
#include "DatabaseGenUtils.h"
#include "DucoWindowState.h"
#include "OpenObjectCallback.h"
#include <Picture.h>
#include "PictureListControl.h"
#include <Peal.h>
#include "PictureControl.h"
#include "RingerList.h"
#include <Tower.h>
#include "PrintBandSummaryUtil.h"
#include <DownloadedPeal.h>
#include "PealUI.h"
#include <UnpaidPealFeeData.h>
#include "DucoUtils.h"
#include "DucoTableSorter.h"
#include <Ringer.h>
#include "DucoEngineUtils.h"
#include <DatabaseSettings.h>

using namespace Duco;
using namespace DucoUI;
using namespace System::Collections;
using namespace System::Windows::Forms;

UnpaidPealFeesUI::UnpaidPealFeesUI(DucoUI::DatabaseManager^ theDatabase)
: database(theDatabase), generating(false)
{
    ringerCache = DucoUI::RingerDisplayCache::CreateDisplayCacheWithFeePayers(database, true);
    allAssociationIds = gcnew System::Collections::Generic::List<System::Int16>();
    InitializeComponent();
}

UnpaidPealFeesUI::~UnpaidPealFeesUI()
{
    if (components)
    {
        delete components;
    }
}

System::Void
UnpaidPealFeesUI::UnpaidPealFeesUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    DatabaseGenUtils::GenerateAssociationOptions(association, allAssociationIds, database, KNoId, true);
    if (association->Items->Count == 1)
    {
        association->SelectedIndex = 0;
    }
    ringerCache->PopulateControl(feePayers, false);

    System::DateTime^ startDate = gcnew System::DateTime();
    System::DateTime^ endDate = gcnew System::DateTime();
    database->GetDateRange(startDate, endDate);
    this->yearSelector->Minimum = startDate->Year;
    this->yearSelector->Maximum = endDate->Year;
    System::Decimal currentValue = System::DateTime::Now.Year;
    if (endDate->Year < currentValue)
        currentValue = endDate->Year;
    this->yearSelector->Value = currentValue;
}

System::Void
UnpaidPealFeesUI::DataGridView1_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    try
    {
        System::Windows::Forms::DataGridView^ gridView = static_cast<System::Windows::Forms::DataGridView^>(sender);
        unsigned int rowIndex = e->RowIndex;
        DataGridViewRow^ currentRow = gridView->Rows[rowIndex];
        unsigned int pealId = System::Convert::ToInt16(currentRow->Cells[0]->Value);
        PealUI::ShowPeal(database, this->MdiParent, pealId);
    }
    catch (System::Exception^)
    {

    }
}

System::Void
UnpaidPealFeesUI::CloseBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void
UnpaidPealFeesUI::CopyBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    System::String^ data = "";

    IEnumerator^ it = static_cast<IEnumerable^>(dataGridView1->Rows)->GetEnumerator();
    while (it->MoveNext())
    {
        DataGridViewRow^ nextRow = static_cast<DataGridViewRow^>(it->Current);
        data += nextRow->Cells[1]->Value;
        data += "\t";
        data += nextRow->Cells[2]->Value;
        data += "\t";
        data += nextRow->Cells[5]->Value;
        data += KNewlineChar;

    }

    Clipboard::SetDataObject(data);
}

System::Void
UnpaidPealFeesUI::ItemChanged(System::Object^  sender, System::EventArgs^  e)
{
    Regenerate();
}

System::Void
UnpaidPealFeesUI::Association_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId oldFeePayerId;
    if (feePayers->SelectedIndex != -1)
    {
        oldFeePayerId = ((RingerItem^)(feePayers)->SelectedItem)->Id();
    }
    Duco::ObjectId oldAssociationId = DatabaseGenUtils::FindId(allAssociationIds, association->SelectedIndex);

    ringerCache->GenerateFeePayersOptions(oldAssociationId);

    if (oldFeePayerId.ValidId())
    {
        feePayers->SelectedIndex = ringerCache->FindIndexOrAdd(oldFeePayerId);
    }
    Regenerate();
}

System::Void
UnpaidPealFeesUI::Regenerate()
{
    if (generating)
        return;

    generating = true;
    dataGridView1->Rows->Clear();
    dataGridView1->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::EnableResizing;

    if (association->SelectedIndex != -1)
    {
        Duco::ObjectId feePayerId;
        if (feePayers->SelectedIndex > 0)
        {
            feePayerId = ((RingerItem^)(feePayers)->SelectedItem)->Id();
        }
        Duco::ObjectId associationId = DatabaseGenUtils::FindId(allAssociationIds, association->SelectedIndex);
		unsigned int totalUnpaidPealFees (0);

        std::set<Duco::UnpaidPealFeeData> pealsData;
        unsigned int year = -1;
        if (this->enableYear->Checked)
        {
            year = System::Convert::ToInt16(this->yearSelector->Value);
        }

        if (database->FindPealsWithUnpaidFees(feePayerId, associationId, year, pealsData))
        {
            std::set<Duco::UnpaidPealFeeData>::const_iterator nextPealData = pealsData.begin();
            while (nextPealData != pealsData.end())
            {
                DataGridViewRow^ newRow = gcnew DataGridViewRow();
                newRow->HeaderCell->Value = DucoUtils::ConvertString(nextPealData->PealId().Str());

                DataGridViewTextBoxCell^ idCell = gcnew DataGridViewTextBoxCell();
                idCell->Value = DucoUtils::ConvertString(nextPealData->PealId().Str());
                newRow->Cells->Add(idCell);

                DataGridViewTextBoxCell^ titleCell = gcnew DataGridViewTextBoxCell();
                newRow->Cells->Add(titleCell);

                DataGridViewTextBoxCell^ dateCell = gcnew DataGridViewTextBoxCell();
                newRow->Cells->Add(dateCell);

                DataGridViewTextBoxCell^ feePayerCell = gcnew DataGridViewTextBoxCell();
                newRow->Cells->Add(feePayerCell);

                DataGridViewTextBoxCell^ amountPaidCell = gcnew DataGridViewTextBoxCell();
                newRow->Cells->Add(amountPaidCell);

                DataGridViewTextBoxCell^ amountOwingCell = gcnew DataGridViewTextBoxCell();
                newRow->Cells->Add(amountOwingCell);

                Duco::Peal thePeal;
                if (database->FindPeal(nextPealData->PealId(), thePeal, false))
                {
                    std::wstring pealTitle;
                    thePeal.Title(pealTitle, database->Database());
                    titleCell->Value = DucoUtils::ConvertString(pealTitle);
                    dateCell->Value = DucoUtils::ConvertString(thePeal.Date().Str());

                    Duco::Ringer theRinger;
                    if (database->FindRinger(nextPealData->FeePayerId(), theRinger, false))
                    {
                        feePayerCell->Value = DucoUtils::ConvertString(theRinger.FullName(database->Settings().LastNameFirst()));
                    }

                    if (thePeal.TotalPealFeePaid() > 0)
                    {
                        amountPaidCell->Value = DucoUtils::FormatUIntToPounds(thePeal.TotalPealFeePaid());
                    }
                    amountOwingCell->Value = DucoUtils::FormatUIntToPounds(nextPealData->AmountUnPaidInPence());
					totalUnpaidPealFees += nextPealData->AmountUnPaidInPence();
                }
                
                dataGridView1->Rows->Add(newRow);
                ++nextPealData;
            }
        
			if (feePayerId.ValidId())
			{
				DataGridViewRow^ newTotalRow = gcnew DataGridViewRow();
				DataGridViewTextBoxCell^ idCell = gcnew DataGridViewTextBoxCell();
				newTotalRow->Cells->Add(idCell);

				DataGridViewTextBoxCell^ titleCell = gcnew DataGridViewTextBoxCell();
				titleCell->Value = "Total unpaid";
				newTotalRow->Cells->Add(titleCell);

				DataGridViewTextBoxCell^ dateCell = gcnew DataGridViewTextBoxCell();
				newTotalRow->Cells->Add(dateCell);

				DataGridViewTextBoxCell^ feePayerCell = gcnew DataGridViewTextBoxCell();
				newTotalRow->Cells->Add(feePayerCell);

				DataGridViewTextBoxCell^ amountPaidCell = gcnew DataGridViewTextBoxCell();
				newTotalRow->Cells->Add(amountPaidCell);

				DataGridViewTextBoxCell^ amountOwingCell = gcnew DataGridViewTextBoxCell();
				newTotalRow->Cells->Add(amountOwingCell);
                if (totalUnpaidPealFees > 0)
                {
                    amountOwingCell->Value = DucoUtils::FormatUIntToPounds(totalUnpaidPealFees);
                }
				dataGridView1->Rows->Add(newTotalRow);
			}
		}
    }
    dataGridView1->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
    generating = false;
}

System::Void
UnpaidPealFeesUI::dataGridView1_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
    DataGridViewColumn^ newColumn = dataGridView1->Columns[e->ColumnIndex];
    if (newColumn->SortMode == DataGridViewColumnSortMode::Programmatic)
    {
        pealNameColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
        FeePayerColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
        paidColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
        owingColumn->HeaderCell->SortGlyphDirection = SortOrder::None;

        SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;
        if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
            newSortOrder = SortOrder::Ascending;
        else
            newSortOrder = SortOrder::Descending;

        DucoTableSorter^ sorter = gcnew DucoTableSorter(newSortOrder == SortOrder::Ascending, ECount);
        sorter->AddSortObject(EDate, newSortOrder == SortOrder::Ascending, 0);
        dataGridView1->Sort(sorter);
        newColumn->HeaderCell->SortGlyphDirection = newSortOrder;
    }
    else
    {
        dateColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
    }
}
