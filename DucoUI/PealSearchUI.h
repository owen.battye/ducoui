#pragma once

namespace DucoUI
{
    public ref class PealSearchUI : public System::Windows::Forms::Form,
                                    public DucoUI::RingingDatabaseObserver, 
                                    public DucoUI::ImportExportProgressCallbackUI, 
                                    public DucoUI::ProgressCallbackUI
    {
    public:
        PealSearchUI(DucoUI::DatabaseManager^ theDatabase, bool setStartInvalidSearch);
        void Show(System::Windows::Forms::Form^ parent, System::Collections::Generic::List<System::UInt64>^ newPealIds);
        void SearchPealsInTower(System::Windows::Forms::Form^ parent, System::UInt64 towerId);

        // from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

        // ImportExportProgressCallback
        virtual void InitialisingImportExport(System::Boolean internalising, unsigned int versionNumber, size_t totalNumberOfObjects);
        virtual void ObjectProcessed(System::Boolean internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase);
        virtual void ImportExportComplete(System::Boolean internalising);
        virtual void ImportExportFailed(System::Boolean internalising, int errorCode);

        // from ProgressCallbackUI
        virtual void Initialised();
        virtual void Step(int progressPercent);
        virtual void Complete();

    protected:
        !PealSearchUI();
        ~PealSearchUI();
        System::Void ringerSelector_ItemCheck(System::Object^  sender, System::Windows::Forms::ItemCheckEventArgs^  e);
        System::Void ringerEditor_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void AssociationEditor_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);

        System::Void SetAsStrapperAvailability(int noOfRingersSelected, int ringerTextLength);
        System::Void dataGridView_CellValueNeeded(System::Object^  sender, System::Windows::Forms::DataGridViewCellValueEventArgs^  e);

        System::Void PealSearchUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void ClosingEvent( Object^ /*sender*/, System::ComponentModel::CancelEventArgs^ e );
        System::Void searchBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void updateBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void clearbtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void printBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void statsBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void saveAsBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void AddAdvancedRinger_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void RemoveAdvancedRingers_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void copyPealDataCmd_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void printPeals(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args);
        System::Void dataGridView_CellDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);
        System::Void towerSelector_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void towerSelector_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void ringSelector_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void Editor_KeyDown( Object^ sender, System::Windows::Forms::KeyEventArgs^ e );
        System::Void Editor_KeyPressed(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e);
        System::Void dateBefore_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void dateOn_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void dateAfter_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void dateOff_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void dateBetween_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void blankAssociation_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void emptyFootnotes_CheckedChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void nonEmptyFootnotes_CheckedChanged(System::Object ^ sender, System::EventArgs ^ e);
        System::Void linkedTowers_CheckedChanged(System::Object^ sender, System::EventArgs^ e);

        System::Void conducted_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void silentConductor_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void jointConductor_CheckedChanged(System::Object^  sender, System::EventArgs^  e);

        System::Void emptyRWRef_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void nonEmptyRWRef_CheckedChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void fullValidRWReference_CheckedChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void invalidRWReference_CheckedChanged(System::Object ^ sender, System::EventArgs ^ e);
        System::Void blankBellBoardRef_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void nonBlankBellBoardRef_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void nonBlankNotes_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void showAssociationColumn_CheckedChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void showComposerColumn_CheckedChanged(System::Object ^ sender, System::EventArgs ^ e);
        System::Void showConductorColumn_CheckedChanged(System::Object ^ sender, System::EventArgs ^ e);
        System::Void showRingColumn_CheckedChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void showBellRungColumn_CheckedChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void showReferencesColumns_CheckedChanged(System::Object^ sender, System::EventArgs^ e);

        // Search tools
        bool CreateSearchParameters(Duco::DatabaseSearch& search);
        System::Void GenerateRingerArguments(Duco::DatabaseSearch& search, System::Windows::Forms::CheckedListBox^ control, System::Windows::Forms::TextBox^ editorControl, Duco::TSearchFieldId idField, Duco::TSearchFieldId nameField, bool excluding);
        System::Void GenerateConductorArguments(Duco::DatabaseSearch& search, System::Windows::Forms::ComboBox^ control, Duco::TSearchFieldId idField, Duco::TSearchFieldId nameField);

        System::Void updateThread_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void updateThread_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void updateThread_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);
        System::Void searchThread_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void searchThread_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void searchThread_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);

    private:
        System::Boolean                                     startInvalidSearch;
        System::UInt64                                      startTowerId;
        System::UInt32                                      withdrawnCount;

        System::Boolean                                     errorFound;
        DucoUI::ImportExportProgressWrapper*                progressWrapper;
        DucoUI::ProgressCallbackWrapper*                    updateWrapper;
        DucoUI::DatabaseManager^                            database;
        System::ComponentModel::IContainer^                 components;
        DucoUI::ProgressCallbackWrapper*                    populateWrapper;
        System::ComponentModel::BackgroundWorker^           searchThread;

        System::Collections::Generic::List<System::UInt64>^ foundPealIds;
        DucoUI::RingerDisplayCache^                         ringerCache;
        DucoUI::RingerDisplayCache^                         conductorCache;
        System::Collections::Generic::List<System::Int16>^  allMethodNames;
        System::Collections::Generic::List<System::Int16>^  allMethodSeriesNames;
        System::Collections::Generic::List<System::Int16>^  allCompositionNames;
        System::Collections::Generic::List<System::Int16>^  allAssociationNames;
        System::Collections::Generic::List<System::Int16>^  allTowerNames;
        System::Collections::Generic::List<System::Int16>^  allRingNames;
        System::Boolean                                     nonNumberEntered;
        System::Boolean                                     populating;
        DucoUI::PrintPealSearchUtil^                        printUtil;

    private: System::Windows::Forms::Button^  searchBtn;
    private: System::Windows::Forms::Button^  updateBtn;
    private: System::Windows::Forms::Button^  clearbtn;
    private: System::Windows::Forms::Button^  printBtn;
    private: System::Windows::Forms::Button^  statsBtn;
    private: System::Windows::Forms::Button^  saveAsBtn;
    private: System::Windows::Forms::TextBox^  changesEditor;
    private: System::Windows::Forms::RadioButton^  changesMoreThan;
    private: System::Windows::Forms::RadioButton^  changesEqualTo;
    private: System::Windows::Forms::RadioButton^  changesLessThan;

    private: System::Windows::Forms::CheckedListBox^  ringerSelector;
    private: System::Windows::Forms::TextBox^  ringerEditor;
    private: System::Windows::Forms::ComboBox^  conductorSelector;
    private: System::Windows::Forms::RadioButton^  timeMoreThan;
    private: System::Windows::Forms::RadioButton^  timeEqualTo;
    private: System::Windows::Forms::RadioButton^  timeLessThan;
    private: System::Windows::Forms::TextBox^  pealTimeEditor;
    private: System::Windows::Forms::ProgressBar^  progressBar;

    private: System::Windows::Forms::CheckBox^  asStrapper;
    private: System::Windows::Forms::RadioButton^  dateOff;
    private: System::Windows::Forms::RadioButton^  dateAfter;
    private: System::Windows::Forms::RadioButton^  dateOn;
    private: System::Windows::Forms::RadioButton^  dateBefore;
    private: System::Windows::Forms::RadioButton^  dateBetween;
    private: System::Windows::Forms::DateTimePicker^  pealDate1;
    private: System::Windows::Forms::DateTimePicker^  pealDate2;
    private: System::Windows::Forms::TextBox^  composerEditor;
    private: System::Windows::Forms::RadioButton^  conducted;
    private: System::Windows::Forms::RadioButton^  silentConductor;
    private: System::Windows::Forms::RadioButton^  jointConductor;
    private: System::Windows::Forms::ComboBox^  associationEditor;
    private: System::Windows::Forms::CheckBox^  excludingRinger;
private: System::Windows::Forms::TextBox^ footnotesEditor;

    private: System::Windows::Forms::CheckBox^  dualOrder;

    private: System::Windows::Forms::CheckBox^  invalid;
    private: System::Windows::Forms::ComboBox^  methodSelector;
    private: System::Windows::Forms::ComboBox^  methodSeriesSelector;
    private: System::Windows::Forms::ComboBox^  ringSelector;
    private: System::Windows::Forms::ComboBox^  towerSelector;
    private: System::Windows::Forms::ComboBox^  compositionSelector;
    private: System::Windows::Forms::TextBox^  bellsEditor;
    private: System::Windows::Forms::RadioButton^  bellsMoreThan;
    private: System::Windows::Forms::RadioButton^  bellsEqualTo;
    private: System::Windows::Forms::RadioButton^  bellsLessThan;
    private: System::Windows::Forms::DataGridView^  dataGridView;
             System::Windows::Forms::DataGridViewCellStyle^ withdrawnPealStyle;
    private: System::Windows::Forms::CheckBox^  blankAssociation;
    private: System::Windows::Forms::CheckBox^  splicedMethods;
    private: System::Windows::Forms::TextBox^  bellBoardRef;
    private: System::Windows::Forms::CheckBox^  blankBellBoardRef;
    private: System::Windows::Forms::TextBox^  rwRef;
    private: System::Windows::Forms::CheckBox^  emptyRWRef;
    private: System::Windows::Forms::CheckBox^  nonBlankNotes;
    private: System::Windows::Forms::TextBox^  notes;
    private: System::Windows::Forms::Panel^  buttonPanel;
    private: System::Windows::Forms::Label^  resultLbl;
    private: System::Windows::Forms::TabPage^  detailsPage;
    private: System::Windows::Forms::GroupBox^  footnoteGroup;
    private: System::Windows::Forms::GroupBox^  associationGroup;
    private: System::Windows::Forms::GroupBox^  pealDateGroup;
    private: System::Windows::Forms::GroupBox^  changesGroup;
    private: System::Windows::Forms::GroupBox^  timeGroup;
    private: System::Windows::Forms::TabPage^  ringersPage;
    private: System::Windows::Forms::GroupBox^  composerGroup;
    private: System::Windows::Forms::GroupBox^  ringerGroup;
    private: System::Windows::Forms::GroupBox^  conductedGroup;
    private: System::Windows::Forms::TabPage^  towerTab;
    private: System::Windows::Forms::GroupBox^  towerGroup;
    private: System::Windows::Forms::GroupBox^  bellsGroup;
    private: System::Windows::Forms::TabPage^  methodTab;
    private: System::Windows::Forms::GroupBox^  methodGroup;
    private: System::Windows::Forms::GroupBox^  methodSeriesGroup;
    private: System::Windows::Forms::Label^  noOfSeriesLbl;
    private: System::Windows::Forms::GroupBox^  compositionGroup;
    private: System::Windows::Forms::TabPage^  referencesPage;
    private: System::Windows::Forms::GroupBox^  notesGroup;
    private: System::Windows::Forms::GroupBox^  rwGroup;
    private: System::Windows::Forms::GroupBox^  bellBoardGroup;
    private: System::Windows::Forms::CheckBox^  inconsistantPealFees;
    private: System::Windows::Forms::CheckBox^  unpaidPealFees;
    private: System::Windows::Forms::ToolTip^  toolTip1;
    private: System::Windows::Forms::TabPage^  advancedRingersTab;
    private: System::Windows::Forms::Button^  addAdvancedRinger;
    private: System::Windows::Forms::ComboBox^  advRingerName;
    private: System::Windows::Forms::ComboBox^  advBellNo;
    private: System::Windows::Forms::FlowLayoutPanel^  ringerAdvSearchParams;
    private: System::Windows::Forms::Button^  removeAdvancedRingers;
    private: System::Windows::Forms::CheckBox^  nonBlankBellBoardRef;
    private: System::Windows::Forms::CheckBox^  doubleHanded;
    private: System::Windows::Forms::CheckBox^  containsMethodSeries;
    private: System::Windows::Forms::CheckBox^  duplicate;
    private: System::Windows::Forms::TextBox^ tenorKeyEditor;
    private: System::Windows::Forms::TextBox^ tenorWeightEditor;
    private: System::Windows::Forms::CheckBox^ splicedMethod;
    private: System::Windows::Forms::CheckBox^ nonEmptyRWRef;
    private: System::Windows::Forms::RadioButton^ withAndWithoutPictures;
    private: System::Windows::Forms::RadioButton^ doesNotHavePicture;
    private: System::Windows::Forms::RadioButton^ hasPicture;
    private: System::Windows::Forms::CheckBox^ realBellNumber;
    private: System::Windows::Forms::GroupBox^ towerTypeGroup;
    private: System::Windows::Forms::RadioButton^ handbellSelector;
    private: System::Windows::Forms::RadioButton^ towerBellSelector;
    private: System::Windows::Forms::RadioButton^ allPealsSelector;
    private: System::Windows::Forms::Label^ resultCount;
    private: System::Windows::Forms::RadioButton^ bothValidAndWithdrawnBtn;
    private: System::Windows::Forms::RadioButton^ validOnlyBtn;

    private: System::Windows::Forms::RadioButton^ withdrawnOnlyBtn;

    private: System::Windows::Forms::CheckBox^ nonEmptyFootnotes;
    private: System::Windows::Forms::CheckBox^ emptyFootnotes;
    private: System::Windows::Forms::CheckBox^ notable;
    private: System::Windows::Forms::CheckBox^ antiClockwise;
    private: System::Windows::Forms::CheckBox^ linkedTowers;
    private: System::Windows::Forms::RadioButton^ bothStrapperOptions;

    private: System::Windows::Forms::RadioButton^ withoutStrapper;

    private: System::Windows::Forms::RadioButton^ withStrapper;













    private: System::Windows::Forms::CheckBox^ showBellRungColumn;
    private: System::Windows::Forms::CheckBox^ showComposerColumn;
    private: System::Windows::Forms::CheckBox^ showAssociationColumn;
    private: System::Windows::Forms::CheckBox^ showRingColumn;
    private: System::Windows::Forms::CheckBox^ showConductorColumn;
private: System::Windows::Forms::CheckBox^ fullValidRWReference;
private: System::Windows::Forms::CheckBox^ invalidRWReference;













private: System::Windows::Forms::CheckBox^ showReferencesColumns;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ rowCountColumn;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ pealIdColumn;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ dateColumn;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ associationColumn;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ changesColumn;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ methodColumn;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ towerColumn;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ ringColumn;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ timeColumn;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ conductorColumn;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ composerColumn;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ ringingWorldColumn;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ bellboardColumn;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ bellRungColumn;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ errorColumn;
private: System::Windows::Forms::CheckBox^ simulatedSound;































    private: System::Windows::Forms::NumericUpDown^ noOfMethodsInSeries;

    void InitializeComponent(void)
    {
        this->components = (gcnew System::ComponentModel::Container());
        System::Windows::Forms::Button^ closeBtn;
        System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
        System::Windows::Forms::SplitContainer^ splitContainer1;
        System::Windows::Forms::TabControl^ tabbedControl;
        System::Windows::Forms::GroupBox^ picturesGroup;
        System::Windows::Forms::GroupBox^ tenorGroup;
        System::Windows::Forms::Label^ tenorKeyLbl;
        System::Windows::Forms::Label^ tenorWeightLbl;
        System::Windows::Forms::Label^ ringLbl;
        System::Windows::Forms::TableLayoutPanel^ ringersPageLayoutPanel;
        System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel2;
        System::Windows::Forms::GroupBox^ settingsGroup;
        System::Windows::Forms::GroupBox^ strapperGroup;
        System::Windows::Forms::GroupBox^ withdrawnBox;
        System::Windows::Forms::GroupBox^ errorsGroupBox;
        System::Windows::Forms::GroupBox^ pealFeesGroup;
        System::Windows::Forms::TableLayoutPanel^ advancedRingerGrp;
        System::Windows::Forms::GroupBox^ newRinger;
        System::Windows::Forms::TabPage^ displayTab;
        System::Windows::Forms::GroupBox^ fieldsGroup;
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle1 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle2 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle3 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle4 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::ContextMenuStrip^ pealDataMenu;
        System::Windows::Forms::ToolStripMenuItem^ copyPealDataCmd;
        this->detailsPage = (gcnew System::Windows::Forms::TabPage());
        this->withAndWithoutPictures = (gcnew System::Windows::Forms::RadioButton());
        this->doesNotHavePicture = (gcnew System::Windows::Forms::RadioButton());
        this->hasPicture = (gcnew System::Windows::Forms::RadioButton());
        this->footnoteGroup = (gcnew System::Windows::Forms::GroupBox());
        this->nonEmptyFootnotes = (gcnew System::Windows::Forms::CheckBox());
        this->emptyFootnotes = (gcnew System::Windows::Forms::CheckBox());
        this->footnotesEditor = (gcnew System::Windows::Forms::TextBox());
        this->associationGroup = (gcnew System::Windows::Forms::GroupBox());
        this->blankAssociation = (gcnew System::Windows::Forms::CheckBox());
        this->associationEditor = (gcnew System::Windows::Forms::ComboBox());
        this->pealDateGroup = (gcnew System::Windows::Forms::GroupBox());
        this->dateOff = (gcnew System::Windows::Forms::RadioButton());
        this->pealDate2 = (gcnew System::Windows::Forms::DateTimePicker());
        this->pealDate1 = (gcnew System::Windows::Forms::DateTimePicker());
        this->dateBetween = (gcnew System::Windows::Forms::RadioButton());
        this->dateAfter = (gcnew System::Windows::Forms::RadioButton());
        this->dateOn = (gcnew System::Windows::Forms::RadioButton());
        this->dateBefore = (gcnew System::Windows::Forms::RadioButton());
        this->changesGroup = (gcnew System::Windows::Forms::GroupBox());
        this->changesEditor = (gcnew System::Windows::Forms::TextBox());
        this->changesMoreThan = (gcnew System::Windows::Forms::RadioButton());
        this->changesEqualTo = (gcnew System::Windows::Forms::RadioButton());
        this->changesLessThan = (gcnew System::Windows::Forms::RadioButton());
        this->timeGroup = (gcnew System::Windows::Forms::GroupBox());
        this->pealTimeEditor = (gcnew System::Windows::Forms::TextBox());
        this->timeMoreThan = (gcnew System::Windows::Forms::RadioButton());
        this->timeEqualTo = (gcnew System::Windows::Forms::RadioButton());
        this->timeLessThan = (gcnew System::Windows::Forms::RadioButton());
        this->towerTab = (gcnew System::Windows::Forms::TabPage());
        this->towerTypeGroup = (gcnew System::Windows::Forms::GroupBox());
        this->handbellSelector = (gcnew System::Windows::Forms::RadioButton());
        this->towerBellSelector = (gcnew System::Windows::Forms::RadioButton());
        this->allPealsSelector = (gcnew System::Windows::Forms::RadioButton());
        this->tenorKeyEditor = (gcnew System::Windows::Forms::TextBox());
        this->tenorWeightEditor = (gcnew System::Windows::Forms::TextBox());
        this->towerGroup = (gcnew System::Windows::Forms::GroupBox());
        this->linkedTowers = (gcnew System::Windows::Forms::CheckBox());
        this->antiClockwise = (gcnew System::Windows::Forms::CheckBox());
        this->ringSelector = (gcnew System::Windows::Forms::ComboBox());
        this->towerSelector = (gcnew System::Windows::Forms::ComboBox());
        this->bellsGroup = (gcnew System::Windows::Forms::GroupBox());
        this->bellsEditor = (gcnew System::Windows::Forms::TextBox());
        this->bellsMoreThan = (gcnew System::Windows::Forms::RadioButton());
        this->bellsEqualTo = (gcnew System::Windows::Forms::RadioButton());
        this->bellsLessThan = (gcnew System::Windows::Forms::RadioButton());
        this->methodTab = (gcnew System::Windows::Forms::TabPage());
        this->methodGroup = (gcnew System::Windows::Forms::GroupBox());
        this->splicedMethod = (gcnew System::Windows::Forms::CheckBox());
        this->splicedMethods = (gcnew System::Windows::Forms::CheckBox());
        this->dualOrder = (gcnew System::Windows::Forms::CheckBox());
        this->methodSelector = (gcnew System::Windows::Forms::ComboBox());
        this->methodSeriesGroup = (gcnew System::Windows::Forms::GroupBox());
        this->containsMethodSeries = (gcnew System::Windows::Forms::CheckBox());
        this->methodSeriesSelector = (gcnew System::Windows::Forms::ComboBox());
        this->noOfMethodsInSeries = (gcnew System::Windows::Forms::NumericUpDown());
        this->noOfSeriesLbl = (gcnew System::Windows::Forms::Label());
        this->compositionGroup = (gcnew System::Windows::Forms::GroupBox());
        this->compositionSelector = (gcnew System::Windows::Forms::ComboBox());
        this->ringersPage = (gcnew System::Windows::Forms::TabPage());
        this->conductedGroup = (gcnew System::Windows::Forms::GroupBox());
        this->jointConductor = (gcnew System::Windows::Forms::RadioButton());
        this->silentConductor = (gcnew System::Windows::Forms::RadioButton());
        this->conducted = (gcnew System::Windows::Forms::RadioButton());
        this->conductorSelector = (gcnew System::Windows::Forms::ComboBox());
        this->ringerGroup = (gcnew System::Windows::Forms::GroupBox());
        this->asStrapper = (gcnew System::Windows::Forms::CheckBox());
        this->ringerSelector = (gcnew System::Windows::Forms::CheckedListBox());
        this->ringerEditor = (gcnew System::Windows::Forms::TextBox());
        this->excludingRinger = (gcnew System::Windows::Forms::CheckBox());
        this->composerGroup = (gcnew System::Windows::Forms::GroupBox());
        this->composerEditor = (gcnew System::Windows::Forms::TextBox());
        this->realBellNumber = (gcnew System::Windows::Forms::CheckBox());
        this->doubleHanded = (gcnew System::Windows::Forms::CheckBox());
        this->bothStrapperOptions = (gcnew System::Windows::Forms::RadioButton());
        this->withoutStrapper = (gcnew System::Windows::Forms::RadioButton());
        this->withStrapper = (gcnew System::Windows::Forms::RadioButton());
        this->referencesPage = (gcnew System::Windows::Forms::TabPage());
        this->bothValidAndWithdrawnBtn = (gcnew System::Windows::Forms::RadioButton());
        this->validOnlyBtn = (gcnew System::Windows::Forms::RadioButton());
        this->withdrawnOnlyBtn = (gcnew System::Windows::Forms::RadioButton());
        this->duplicate = (gcnew System::Windows::Forms::CheckBox());
        this->invalid = (gcnew System::Windows::Forms::CheckBox());
        this->inconsistantPealFees = (gcnew System::Windows::Forms::CheckBox());
        this->unpaidPealFees = (gcnew System::Windows::Forms::CheckBox());
        this->notesGroup = (gcnew System::Windows::Forms::GroupBox());
        this->notable = (gcnew System::Windows::Forms::CheckBox());
        this->nonBlankNotes = (gcnew System::Windows::Forms::CheckBox());
        this->notes = (gcnew System::Windows::Forms::TextBox());
        this->rwGroup = (gcnew System::Windows::Forms::GroupBox());
        this->invalidRWReference = (gcnew System::Windows::Forms::CheckBox());
        this->fullValidRWReference = (gcnew System::Windows::Forms::CheckBox());
        this->nonEmptyRWRef = (gcnew System::Windows::Forms::CheckBox());
        this->rwRef = (gcnew System::Windows::Forms::TextBox());
        this->emptyRWRef = (gcnew System::Windows::Forms::CheckBox());
        this->bellBoardGroup = (gcnew System::Windows::Forms::GroupBox());
        this->nonBlankBellBoardRef = (gcnew System::Windows::Forms::CheckBox());
        this->bellBoardRef = (gcnew System::Windows::Forms::TextBox());
        this->blankBellBoardRef = (gcnew System::Windows::Forms::CheckBox());
        this->advancedRingersTab = (gcnew System::Windows::Forms::TabPage());
        this->removeAdvancedRingers = (gcnew System::Windows::Forms::Button());
        this->advBellNo = (gcnew System::Windows::Forms::ComboBox());
        this->advRingerName = (gcnew System::Windows::Forms::ComboBox());
        this->addAdvancedRinger = (gcnew System::Windows::Forms::Button());
        this->ringerAdvSearchParams = (gcnew System::Windows::Forms::FlowLayoutPanel());
        this->showReferencesColumns = (gcnew System::Windows::Forms::CheckBox());
        this->showRingColumn = (gcnew System::Windows::Forms::CheckBox());
        this->showConductorColumn = (gcnew System::Windows::Forms::CheckBox());
        this->showBellRungColumn = (gcnew System::Windows::Forms::CheckBox());
        this->showComposerColumn = (gcnew System::Windows::Forms::CheckBox());
        this->showAssociationColumn = (gcnew System::Windows::Forms::CheckBox());
        this->dataGridView = (gcnew System::Windows::Forms::DataGridView());
        this->rowCountColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->pealIdColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dateColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->associationColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->changesColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->methodColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->towerColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->ringColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->timeColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->conductorColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->composerColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->ringingWorldColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->bellboardColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->bellRungColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->errorColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->buttonPanel = (gcnew System::Windows::Forms::Panel());
        this->resultCount = (gcnew System::Windows::Forms::Label());
        this->printBtn = (gcnew System::Windows::Forms::Button());
        this->statsBtn = (gcnew System::Windows::Forms::Button());
        this->clearbtn = (gcnew System::Windows::Forms::Button());
        this->progressBar = (gcnew System::Windows::Forms::ProgressBar());
        this->searchBtn = (gcnew System::Windows::Forms::Button());
        this->resultLbl = (gcnew System::Windows::Forms::Label());
        this->updateBtn = (gcnew System::Windows::Forms::Button());
        this->saveAsBtn = (gcnew System::Windows::Forms::Button());
        this->searchThread = (gcnew System::ComponentModel::BackgroundWorker());
        this->toolTip1 = (gcnew System::Windows::Forms::ToolTip(this->components));
        this->simulatedSound = (gcnew System::Windows::Forms::CheckBox());
        closeBtn = (gcnew System::Windows::Forms::Button());
        tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
        splitContainer1 = (gcnew System::Windows::Forms::SplitContainer());
        tabbedControl = (gcnew System::Windows::Forms::TabControl());
        picturesGroup = (gcnew System::Windows::Forms::GroupBox());
        tenorGroup = (gcnew System::Windows::Forms::GroupBox());
        tenorKeyLbl = (gcnew System::Windows::Forms::Label());
        tenorWeightLbl = (gcnew System::Windows::Forms::Label());
        ringLbl = (gcnew System::Windows::Forms::Label());
        ringersPageLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
        tableLayoutPanel2 = (gcnew System::Windows::Forms::TableLayoutPanel());
        settingsGroup = (gcnew System::Windows::Forms::GroupBox());
        strapperGroup = (gcnew System::Windows::Forms::GroupBox());
        withdrawnBox = (gcnew System::Windows::Forms::GroupBox());
        errorsGroupBox = (gcnew System::Windows::Forms::GroupBox());
        pealFeesGroup = (gcnew System::Windows::Forms::GroupBox());
        advancedRingerGrp = (gcnew System::Windows::Forms::TableLayoutPanel());
        newRinger = (gcnew System::Windows::Forms::GroupBox());
        displayTab = (gcnew System::Windows::Forms::TabPage());
        fieldsGroup = (gcnew System::Windows::Forms::GroupBox());
        pealDataMenu = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
        copyPealDataCmd = (gcnew System::Windows::Forms::ToolStripMenuItem());
        tableLayoutPanel1->SuspendLayout();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(splitContainer1))->BeginInit();
        splitContainer1->Panel1->SuspendLayout();
        splitContainer1->Panel2->SuspendLayout();
        splitContainer1->SuspendLayout();
        tabbedControl->SuspendLayout();
        this->detailsPage->SuspendLayout();
        picturesGroup->SuspendLayout();
        this->footnoteGroup->SuspendLayout();
        this->associationGroup->SuspendLayout();
        this->pealDateGroup->SuspendLayout();
        this->changesGroup->SuspendLayout();
        this->timeGroup->SuspendLayout();
        this->towerTab->SuspendLayout();
        this->towerTypeGroup->SuspendLayout();
        tenorGroup->SuspendLayout();
        this->towerGroup->SuspendLayout();
        this->bellsGroup->SuspendLayout();
        this->methodTab->SuspendLayout();
        this->methodGroup->SuspendLayout();
        this->methodSeriesGroup->SuspendLayout();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->noOfMethodsInSeries))->BeginInit();
        this->compositionGroup->SuspendLayout();
        this->ringersPage->SuspendLayout();
        ringersPageLayoutPanel->SuspendLayout();
        this->conductedGroup->SuspendLayout();
        this->ringerGroup->SuspendLayout();
        tableLayoutPanel2->SuspendLayout();
        this->composerGroup->SuspendLayout();
        settingsGroup->SuspendLayout();
        strapperGroup->SuspendLayout();
        this->referencesPage->SuspendLayout();
        withdrawnBox->SuspendLayout();
        errorsGroupBox->SuspendLayout();
        pealFeesGroup->SuspendLayout();
        this->notesGroup->SuspendLayout();
        this->rwGroup->SuspendLayout();
        this->bellBoardGroup->SuspendLayout();
        this->advancedRingersTab->SuspendLayout();
        advancedRingerGrp->SuspendLayout();
        newRinger->SuspendLayout();
        displayTab->SuspendLayout();
        fieldsGroup->SuspendLayout();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->BeginInit();
        pealDataMenu->SuspendLayout();
        this->buttonPanel->SuspendLayout();
        this->SuspendLayout();
        // 
        // closeBtn
        // 
        closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
        closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
        closeBtn->Location = System::Drawing::Point(1063, 1);
        closeBtn->Name = L"closeBtn";
        closeBtn->Size = System::Drawing::Size(74, 23);
        closeBtn->TabIndex = 90;
        closeBtn->Text = L"Close";
        closeBtn->UseVisualStyleBackColor = true;
        closeBtn->Click += gcnew System::EventHandler(this, &PealSearchUI::closeBtn_Click);
        // 
        // tableLayoutPanel1
        // 
        tableLayoutPanel1->ColumnCount = 1;
        tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
        tableLayoutPanel1->Controls->Add(splitContainer1, 0, 0);
        tableLayoutPanel1->Controls->Add(this->buttonPanel, 0, 1);
        tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
        tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
        tableLayoutPanel1->Name = L"tableLayoutPanel1";
        tableLayoutPanel1->RowCount = 2;
        tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
        tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 30)));
        tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
        tableLayoutPanel1->Size = System::Drawing::Size(1143, 568);
        tableLayoutPanel1->TabIndex = 0;
        // 
        // splitContainer1
        // 
        splitContainer1->Dock = System::Windows::Forms::DockStyle::Fill;
        splitContainer1->Location = System::Drawing::Point(3, 3);
        splitContainer1->Name = L"splitContainer1";
        splitContainer1->Orientation = System::Windows::Forms::Orientation::Horizontal;
        // 
        // splitContainer1.Panel1
        // 
        splitContainer1->Panel1->Controls->Add(tabbedControl);
        splitContainer1->Panel1MinSize = 200;
        // 
        // splitContainer1.Panel2
        // 
        splitContainer1->Panel2->Controls->Add(this->dataGridView);
        splitContainer1->Panel2MinSize = 50;
        splitContainer1->Size = System::Drawing::Size(1137, 532);
        splitContainer1->SplitterDistance = 208;
        splitContainer1->TabIndex = 1;
        // 
        // tabbedControl
        // 
        tabbedControl->Controls->Add(this->detailsPage);
        tabbedControl->Controls->Add(this->towerTab);
        tabbedControl->Controls->Add(this->methodTab);
        tabbedControl->Controls->Add(this->ringersPage);
        tabbedControl->Controls->Add(this->referencesPage);
        tabbedControl->Controls->Add(this->advancedRingersTab);
        tabbedControl->Controls->Add(displayTab);
        tabbedControl->Dock = System::Windows::Forms::DockStyle::Fill;
        tabbedControl->Location = System::Drawing::Point(0, 0);
        tabbedControl->Margin = System::Windows::Forms::Padding(1);
        tabbedControl->Name = L"tabbedControl";
        tabbedControl->SelectedIndex = 0;
        tabbedControl->Size = System::Drawing::Size(1137, 208);
        tabbedControl->TabIndex = 1;
        // 
        // detailsPage
        // 
        this->detailsPage->Controls->Add(picturesGroup);
        this->detailsPage->Controls->Add(this->footnoteGroup);
        this->detailsPage->Controls->Add(this->associationGroup);
        this->detailsPage->Controls->Add(this->pealDateGroup);
        this->detailsPage->Controls->Add(this->changesGroup);
        this->detailsPage->Controls->Add(this->timeGroup);
        this->detailsPage->Location = System::Drawing::Point(4, 22);
        this->detailsPage->Name = L"detailsPage";
        this->detailsPage->Padding = System::Windows::Forms::Padding(3);
        this->detailsPage->Size = System::Drawing::Size(1129, 182);
        this->detailsPage->TabIndex = 2;
        this->detailsPage->Text = L"Performance details";
        this->detailsPage->UseVisualStyleBackColor = true;
        // 
        // picturesGroup
        // 
        picturesGroup->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
        picturesGroup->Controls->Add(this->withAndWithoutPictures);
        picturesGroup->Controls->Add(this->doesNotHavePicture);
        picturesGroup->Controls->Add(this->hasPicture);
        picturesGroup->Location = System::Drawing::Point(416, 6);
        picturesGroup->Name = L"picturesGroup";
        picturesGroup->Size = System::Drawing::Size(114, 120);
        picturesGroup->TabIndex = 25;
        picturesGroup->TabStop = false;
        picturesGroup->Text = L"Pictures";
        // 
        // withAndWithoutPictures
        // 
        this->withAndWithoutPictures->AutoSize = true;
        this->withAndWithoutPictures->Checked = true;
        this->withAndWithoutPictures->Location = System::Drawing::Point(7, 66);
        this->withAndWithoutPictures->Name = L"withAndWithoutPictures";
        this->withAndWithoutPictures->Size = System::Drawing::Size(47, 17);
        this->withAndWithoutPictures->TabIndex = 2;
        this->withAndWithoutPictures->TabStop = true;
        this->withAndWithoutPictures->Text = L"Both";
        this->withAndWithoutPictures->UseVisualStyleBackColor = true;
        // 
        // doesNotHavePicture
        // 
        this->doesNotHavePicture->AutoSize = true;
        this->doesNotHavePicture->Location = System::Drawing::Point(7, 43);
        this->doesNotHavePicture->Name = L"doesNotHavePicture";
        this->doesNotHavePicture->Size = System::Drawing::Size(94, 17);
        this->doesNotHavePicture->TabIndex = 1;
        this->doesNotHavePicture->TabStop = true;
        this->doesNotHavePicture->Text = L"Has no picture";
        this->doesNotHavePicture->UseVisualStyleBackColor = true;
        // 
        // hasPicture
        // 
        this->hasPicture->AutoSize = true;
        this->hasPicture->Location = System::Drawing::Point(7, 20);
        this->hasPicture->Name = L"hasPicture";
        this->hasPicture->Size = System::Drawing::Size(101, 17);
        this->hasPicture->TabIndex = 0;
        this->hasPicture->TabStop = true;
        this->hasPicture->Text = L"Contains picture";
        this->hasPicture->UseVisualStyleBackColor = true;
        // 
        // footnoteGroup
        // 
        this->footnoteGroup->Controls->Add(this->nonEmptyFootnotes);
        this->footnoteGroup->Controls->Add(this->emptyFootnotes);
        this->footnoteGroup->Controls->Add(this->footnotesEditor);
        this->footnoteGroup->Location = System::Drawing::Point(536, 79);
        this->footnoteGroup->Name = L"footnoteGroup";
        this->footnoteGroup->Size = System::Drawing::Size(441, 74);
        this->footnoteGroup->TabIndex = 24;
        this->footnoteGroup->TabStop = false;
        this->footnoteGroup->Text = L"Footnote";
        // 
        // nonEmptyFootnotes
        // 
        this->nonEmptyFootnotes->AutoSize = true;
        this->nonEmptyFootnotes->Location = System::Drawing::Point(68, 46);
        this->nonEmptyFootnotes->Name = L"nonEmptyFootnotes";
        this->nonEmptyFootnotes->Size = System::Drawing::Size(77, 17);
        this->nonEmptyFootnotes->TabIndex = 27;
        this->nonEmptyFootnotes->Text = L"Non empty";
        this->nonEmptyFootnotes->UseVisualStyleBackColor = true;
        this->nonEmptyFootnotes->CheckedChanged += gcnew System::EventHandler(this, &PealSearchUI::nonEmptyFootnotes_CheckedChanged);
        // 
        // emptyFootnotes
        // 
        this->emptyFootnotes->AutoSize = true;
        this->emptyFootnotes->Location = System::Drawing::Point(7, 46);
        this->emptyFootnotes->Name = L"emptyFootnotes";
        this->emptyFootnotes->Size = System::Drawing::Size(55, 17);
        this->emptyFootnotes->TabIndex = 26;
        this->emptyFootnotes->Text = L"Empty";
        this->emptyFootnotes->UseVisualStyleBackColor = true;
        this->emptyFootnotes->CheckedChanged += gcnew System::EventHandler(this, &PealSearchUI::emptyFootnotes_CheckedChanged);
        // 
        // footnotesEditor
        // 
        this->footnotesEditor->Location = System::Drawing::Point(6, 19);
        this->footnotesEditor->Name = L"footnotesEditor";
        this->footnotesEditor->Size = System::Drawing::Size(429, 20);
        this->footnotesEditor->TabIndex = 25;
        // 
        // associationGroup
        // 
        this->associationGroup->Controls->Add(this->blankAssociation);
        this->associationGroup->Controls->Add(this->associationEditor);
        this->associationGroup->Location = System::Drawing::Point(536, 6);
        this->associationGroup->Name = L"associationGroup";
        this->associationGroup->Size = System::Drawing::Size(441, 67);
        this->associationGroup->TabIndex = 21;
        this->associationGroup->TabStop = false;
        this->associationGroup->Text = L"Association";
        // 
        // blankAssociation
        // 
        this->blankAssociation->AutoSize = true;
        this->blankAssociation->Location = System::Drawing::Point(6, 44);
        this->blankAssociation->Name = L"blankAssociation";
        this->blankAssociation->Size = System::Drawing::Size(160, 17);
        this->blankAssociation->TabIndex = 23;
        this->blankAssociation->Text = L"Search for blank association";
        this->blankAssociation->UseVisualStyleBackColor = true;
        this->blankAssociation->CheckedChanged += gcnew System::EventHandler(this, &PealSearchUI::blankAssociation_CheckedChanged);
        // 
        // associationEditor
        // 
        this->associationEditor->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
        this->associationEditor->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
        this->associationEditor->FormattingEnabled = true;
        this->associationEditor->Location = System::Drawing::Point(6, 20);
        this->associationEditor->Name = L"associationEditor";
        this->associationEditor->Size = System::Drawing::Size(429, 21);
        this->associationEditor->TabIndex = 22;
        this->associationEditor->SelectedIndexChanged += gcnew System::EventHandler(this, &PealSearchUI::AssociationEditor_SelectedIndexChanged);
        // 
        // pealDateGroup
        // 
        this->pealDateGroup->Controls->Add(this->dateOff);
        this->pealDateGroup->Controls->Add(this->pealDate2);
        this->pealDateGroup->Controls->Add(this->pealDate1);
        this->pealDateGroup->Controls->Add(this->dateBetween);
        this->pealDateGroup->Controls->Add(this->dateAfter);
        this->pealDateGroup->Controls->Add(this->dateOn);
        this->pealDateGroup->Controls->Add(this->dateBefore);
        this->pealDateGroup->Location = System::Drawing::Point(201, 6);
        this->pealDateGroup->Name = L"pealDateGroup";
        this->pealDateGroup->Size = System::Drawing::Size(209, 119);
        this->pealDateGroup->TabIndex = 13;
        this->pealDateGroup->TabStop = false;
        this->pealDateGroup->Text = L"Date";
        // 
        // dateOff
        // 
        this->dateOff->AutoSize = true;
        this->dateOff->Checked = true;
        this->dateOff->Location = System::Drawing::Point(7, 91);
        this->dateOff->Name = L"dateOff";
        this->dateOff->Size = System::Drawing::Size(79, 17);
        this->dateOff->TabIndex = 16;
        this->dateOff->TabStop = true;
        this->dateOff->Text = L"Ignore date";
        this->dateOff->UseVisualStyleBackColor = true;
        this->dateOff->CheckedChanged += gcnew System::EventHandler(this, &PealSearchUI::dateOff_CheckedChanged);
        // 
        // pealDate2
        // 
        this->pealDate2->Location = System::Drawing::Point(7, 47);
        this->pealDate2->Name = L"pealDate2";
        this->pealDate2->Size = System::Drawing::Size(126, 20);
        this->pealDate2->TabIndex = 15;
        this->pealDate2->Visible = false;
        // 
        // pealDate1
        // 
        this->pealDate1->Enabled = false;
        this->pealDate1->Location = System::Drawing::Point(7, 20);
        this->pealDate1->Name = L"pealDate1";
        this->pealDate1->Size = System::Drawing::Size(126, 20);
        this->pealDate1->TabIndex = 14;
        // 
        // dateBetween
        // 
        this->dateBetween->AutoSize = true;
        this->dateBetween->Location = System::Drawing::Point(139, 91);
        this->dateBetween->Name = L"dateBetween";
        this->dateBetween->Size = System::Drawing::Size(67, 17);
        this->dateBetween->TabIndex = 20;
        this->dateBetween->Text = L"Between";
        this->dateBetween->UseVisualStyleBackColor = true;
        this->dateBetween->CheckedChanged += gcnew System::EventHandler(this, &PealSearchUI::dateBetween_CheckedChanged);
        // 
        // dateAfter
        // 
        this->dateAfter->AutoSize = true;
        this->dateAfter->Location = System::Drawing::Point(139, 68);
        this->dateAfter->Name = L"dateAfter";
        this->dateAfter->Size = System::Drawing::Size(47, 17);
        this->dateAfter->TabIndex = 19;
        this->dateAfter->Text = L"After";
        this->dateAfter->UseVisualStyleBackColor = true;
        this->dateAfter->CheckedChanged += gcnew System::EventHandler(this, &PealSearchUI::dateAfter_CheckedChanged);
        // 
        // dateOn
        // 
        this->dateOn->AutoSize = true;
        this->dateOn->Location = System::Drawing::Point(139, 45);
        this->dateOn->Name = L"dateOn";
        this->dateOn->Size = System::Drawing::Size(39, 17);
        this->dateOn->TabIndex = 18;
        this->dateOn->Text = L"On";
        this->dateOn->UseVisualStyleBackColor = true;
        this->dateOn->CheckedChanged += gcnew System::EventHandler(this, &PealSearchUI::dateOn_CheckedChanged);
        // 
        // dateBefore
        // 
        this->dateBefore->AutoSize = true;
        this->dateBefore->Location = System::Drawing::Point(139, 22);
        this->dateBefore->Name = L"dateBefore";
        this->dateBefore->Size = System::Drawing::Size(56, 17);
        this->dateBefore->TabIndex = 17;
        this->dateBefore->Text = L"Before";
        this->dateBefore->UseVisualStyleBackColor = true;
        this->dateBefore->CheckedChanged += gcnew System::EventHandler(this, &PealSearchUI::dateBefore_CheckedChanged);
        // 
        // changesGroup
        // 
        this->changesGroup->Controls->Add(this->changesEditor);
        this->changesGroup->Controls->Add(this->changesMoreThan);
        this->changesGroup->Controls->Add(this->changesEqualTo);
        this->changesGroup->Controls->Add(this->changesLessThan);
        this->changesGroup->Location = System::Drawing::Point(6, 6);
        this->changesGroup->Name = L"changesGroup";
        this->changesGroup->Size = System::Drawing::Size(90, 119);
        this->changesGroup->TabIndex = 3;
        this->changesGroup->TabStop = false;
        this->changesGroup->Text = L"Changes";
        // 
        // changesEditor
        // 
        this->changesEditor->Location = System::Drawing::Point(6, 19);
        this->changesEditor->Name = L"changesEditor";
        this->changesEditor->Size = System::Drawing::Size(73, 20);
        this->changesEditor->TabIndex = 4;
        this->changesEditor->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &PealSearchUI::Editor_KeyDown);
        this->changesEditor->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &PealSearchUI::Editor_KeyPressed);
        // 
        // changesMoreThan
        // 
        this->changesMoreThan->AutoSize = true;
        this->changesMoreThan->Location = System::Drawing::Point(6, 91);
        this->changesMoreThan->Name = L"changesMoreThan";
        this->changesMoreThan->Size = System::Drawing::Size(73, 17);
        this->changesMoreThan->TabIndex = 5;
        this->changesMoreThan->Text = L"More than";
        this->changesMoreThan->UseVisualStyleBackColor = true;
        // 
        // changesEqualTo
        // 
        this->changesEqualTo->AutoSize = true;
        this->changesEqualTo->Checked = true;
        this->changesEqualTo->Location = System::Drawing::Point(6, 68);
        this->changesEqualTo->Name = L"changesEqualTo";
        this->changesEqualTo->Size = System::Drawing::Size(64, 17);
        this->changesEqualTo->TabIndex = 6;
        this->changesEqualTo->TabStop = true;
        this->changesEqualTo->Text = L"Equal to";
        this->changesEqualTo->UseVisualStyleBackColor = true;
        // 
        // changesLessThan
        // 
        this->changesLessThan->AutoSize = true;
        this->changesLessThan->Location = System::Drawing::Point(6, 45);
        this->changesLessThan->Name = L"changesLessThan";
        this->changesLessThan->Size = System::Drawing::Size(71, 17);
        this->changesLessThan->TabIndex = 7;
        this->changesLessThan->Text = L"Less than";
        this->changesLessThan->UseVisualStyleBackColor = true;
        // 
        // timeGroup
        // 
        this->timeGroup->Controls->Add(this->pealTimeEditor);
        this->timeGroup->Controls->Add(this->timeMoreThan);
        this->timeGroup->Controls->Add(this->timeEqualTo);
        this->timeGroup->Controls->Add(this->timeLessThan);
        this->timeGroup->Location = System::Drawing::Point(104, 6);
        this->timeGroup->Name = L"timeGroup";
        this->timeGroup->Size = System::Drawing::Size(90, 119);
        this->timeGroup->TabIndex = 8;
        this->timeGroup->TabStop = false;
        this->timeGroup->Text = L"Time";
        // 
        // pealTimeEditor
        // 
        this->pealTimeEditor->Location = System::Drawing::Point(6, 19);
        this->pealTimeEditor->Name = L"pealTimeEditor";
        this->pealTimeEditor->Size = System::Drawing::Size(71, 20);
        this->pealTimeEditor->TabIndex = 9;
        this->pealTimeEditor->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &PealSearchUI::Editor_KeyDown);
        this->pealTimeEditor->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &PealSearchUI::Editor_KeyPressed);
        // 
        // timeMoreThan
        // 
        this->timeMoreThan->AutoSize = true;
        this->timeMoreThan->Location = System::Drawing::Point(6, 91);
        this->timeMoreThan->Name = L"timeMoreThan";
        this->timeMoreThan->Size = System::Drawing::Size(73, 17);
        this->timeMoreThan->TabIndex = 10;
        this->timeMoreThan->Text = L"More than";
        this->timeMoreThan->UseVisualStyleBackColor = true;
        // 
        // timeEqualTo
        // 
        this->timeEqualTo->AutoSize = true;
        this->timeEqualTo->Checked = true;
        this->timeEqualTo->Location = System::Drawing::Point(6, 68);
        this->timeEqualTo->Name = L"timeEqualTo";
        this->timeEqualTo->Size = System::Drawing::Size(64, 17);
        this->timeEqualTo->TabIndex = 11;
        this->timeEqualTo->TabStop = true;
        this->timeEqualTo->Text = L"Equal to";
        this->timeEqualTo->UseVisualStyleBackColor = true;
        // 
        // timeLessThan
        // 
        this->timeLessThan->AutoSize = true;
        this->timeLessThan->Location = System::Drawing::Point(6, 45);
        this->timeLessThan->Name = L"timeLessThan";
        this->timeLessThan->Size = System::Drawing::Size(71, 17);
        this->timeLessThan->TabIndex = 12;
        this->timeLessThan->Text = L"Less than";
        this->timeLessThan->UseVisualStyleBackColor = true;
        // 
        // towerTab
        // 
        this->towerTab->Controls->Add(this->towerTypeGroup);
        this->towerTab->Controls->Add(tenorGroup);
        this->towerTab->Controls->Add(this->towerGroup);
        this->towerTab->Controls->Add(this->bellsGroup);
        this->towerTab->Location = System::Drawing::Point(4, 22);
        this->towerTab->Name = L"towerTab";
        this->towerTab->Padding = System::Windows::Forms::Padding(3);
        this->towerTab->Size = System::Drawing::Size(1129, 182);
        this->towerTab->TabIndex = 40;
        this->towerTab->Text = L"Tower";
        this->towerTab->UseVisualStyleBackColor = true;
        // 
        // towerTypeGroup
        // 
        this->towerTypeGroup->Controls->Add(this->handbellSelector);
        this->towerTypeGroup->Controls->Add(this->towerBellSelector);
        this->towerTypeGroup->Controls->Add(this->allPealsSelector);
        this->towerTypeGroup->Location = System::Drawing::Point(5, 6);
        this->towerTypeGroup->Name = L"towerTypeGroup";
        this->towerTypeGroup->Size = System::Drawing::Size(99, 119);
        this->towerTypeGroup->TabIndex = 85;
        this->towerTypeGroup->TabStop = false;
        this->towerTypeGroup->Text = L"Venue type";
        // 
        // handbellSelector
        // 
        this->handbellSelector->AutoSize = true;
        this->handbellSelector->Location = System::Drawing::Point(6, 42);
        this->handbellSelector->Name = L"handbellSelector";
        this->handbellSelector->Size = System::Drawing::Size(75, 17);
        this->handbellSelector->TabIndex = 52;
        this->handbellSelector->Text = L"Hand bells";
        this->handbellSelector->UseVisualStyleBackColor = true;
        // 
        // towerBellSelector
        // 
        this->towerBellSelector->AutoSize = true;
        this->towerBellSelector->Location = System::Drawing::Point(6, 19);
        this->towerBellSelector->Name = L"towerBellSelector";
        this->towerBellSelector->Size = System::Drawing::Size(79, 17);
        this->towerBellSelector->TabIndex = 51;
        this->towerBellSelector->Text = L"Tower bells";
        this->towerBellSelector->UseVisualStyleBackColor = true;
        // 
        // allPealsSelector
        // 
        this->allPealsSelector->AutoSize = true;
        this->allPealsSelector->Checked = true;
        this->allPealsSelector->Location = System::Drawing::Point(6, 65);
        this->allPealsSelector->Name = L"allPealsSelector";
        this->allPealsSelector->Size = System::Drawing::Size(47, 17);
        this->allPealsSelector->TabIndex = 50;
        this->allPealsSelector->TabStop = true;
        this->allPealsSelector->Text = L"Both";
        this->allPealsSelector->UseVisualStyleBackColor = true;
        // 
        // tenorGroup
        // 
        tenorGroup->Controls->Add(this->tenorKeyEditor);
        tenorGroup->Controls->Add(this->tenorWeightEditor);
        tenorGroup->Controls->Add(tenorKeyLbl);
        tenorGroup->Controls->Add(tenorWeightLbl);
        tenorGroup->Location = System::Drawing::Point(204, 6);
        tenorGroup->Name = L"tenorGroup";
        tenorGroup->Size = System::Drawing::Size(175, 85);
        tenorGroup->TabIndex = 50;
        tenorGroup->TabStop = false;
        tenorGroup->Text = L"Tenor";
        // 
        // tenorKeyEditor
        // 
        this->tenorKeyEditor->Location = System::Drawing::Point(58, 50);
        this->tenorKeyEditor->Name = L"tenorKeyEditor";
        this->tenorKeyEditor->Size = System::Drawing::Size(100, 20);
        this->tenorKeyEditor->TabIndex = 3;
        // 
        // tenorWeightEditor
        // 
        this->tenorWeightEditor->Location = System::Drawing::Point(58, 20);
        this->tenorWeightEditor->Name = L"tenorWeightEditor";
        this->tenorWeightEditor->Size = System::Drawing::Size(100, 20);
        this->tenorWeightEditor->TabIndex = 2;
        // 
        // tenorKeyLbl
        // 
        tenorKeyLbl->AutoSize = true;
        tenorKeyLbl->Location = System::Drawing::Point(24, 53);
        tenorKeyLbl->Name = L"tenorKeyLbl";
        tenorKeyLbl->Size = System::Drawing::Size(28, 13);
        tenorKeyLbl->TabIndex = 1;
        tenorKeyLbl->Text = L"Key:";
        // 
        // tenorWeightLbl
        // 
        tenorWeightLbl->AutoSize = true;
        tenorWeightLbl->Location = System::Drawing::Point(8, 23);
        tenorWeightLbl->Name = L"tenorWeightLbl";
        tenorWeightLbl->Size = System::Drawing::Size(44, 13);
        tenorWeightLbl->TabIndex = 0;
        tenorWeightLbl->Text = L"Weight:";
        // 
        // towerGroup
        // 
        this->towerGroup->Controls->Add(this->simulatedSound);
        this->towerGroup->Controls->Add(this->linkedTowers);
        this->towerGroup->Controls->Add(this->antiClockwise);
        this->towerGroup->Controls->Add(ringLbl);
        this->towerGroup->Controls->Add(this->ringSelector);
        this->towerGroup->Controls->Add(this->towerSelector);
        this->towerGroup->Location = System::Drawing::Point(385, 6);
        this->towerGroup->Name = L"towerGroup";
        this->towerGroup->Size = System::Drawing::Size(452, 125);
        this->towerGroup->TabIndex = 46;
        this->towerGroup->TabStop = false;
        this->towerGroup->Text = L"Tower";
        // 
        // linkedTowers
        // 
        this->linkedTowers->AutoSize = true;
        this->linkedTowers->Location = System::Drawing::Point(7, 49);
        this->linkedTowers->Name = L"linkedTowers";
        this->linkedTowers->Size = System::Drawing::Size(126, 17);
        this->linkedTowers->TabIndex = 87;
        this->linkedTowers->Text = L"Include linked towers";
        this->linkedTowers->UseVisualStyleBackColor = true;
        this->linkedTowers->CheckedChanged += gcnew System::EventHandler(this, &PealSearchUI::linkedTowers_CheckedChanged);
        // 
        // antiClockwise
        // 
        this->antiClockwise->AutoSize = true;
        this->antiClockwise->Location = System::Drawing::Point(7, 72);
        this->antiClockwise->Name = L"antiClockwise";
        this->antiClockwise->Size = System::Drawing::Size(94, 17);
        this->antiClockwise->TabIndex = 86;
        this->antiClockwise->Text = L"Anti clockwise";
        this->antiClockwise->UseVisualStyleBackColor = true;
        // 
        // ringLbl
        // 
        ringLbl->AutoSize = true;
        ringLbl->Location = System::Drawing::Point(242, 50);
        ringLbl->Name = L"ringLbl";
        ringLbl->Size = System::Drawing::Size(32, 13);
        ringLbl->TabIndex = 0;
        ringLbl->Text = L"Ring:";
        // 
        // ringSelector
        // 
        this->ringSelector->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
        this->ringSelector->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
        this->ringSelector->Enabled = false;
        this->ringSelector->FormattingEnabled = true;
        this->ringSelector->Location = System::Drawing::Point(280, 47);
        this->ringSelector->Name = L"ringSelector";
        this->ringSelector->Size = System::Drawing::Size(158, 21);
        this->ringSelector->TabIndex = 48;
        this->ringSelector->SelectedIndexChanged += gcnew System::EventHandler(this, &PealSearchUI::ringSelector_SelectedIndexChanged);
        // 
        // towerSelector
        // 
        this->towerSelector->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
        this->towerSelector->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
        this->towerSelector->FormattingEnabled = true;
        this->towerSelector->Location = System::Drawing::Point(7, 20);
        this->towerSelector->Name = L"towerSelector";
        this->towerSelector->Size = System::Drawing::Size(431, 21);
        this->towerSelector->TabIndex = 47;
        this->towerSelector->SelectedIndexChanged += gcnew System::EventHandler(this, &PealSearchUI::towerSelector_SelectedIndexChanged);
        // 
        // bellsGroup
        // 
        this->bellsGroup->Controls->Add(this->bellsEditor);
        this->bellsGroup->Controls->Add(this->bellsMoreThan);
        this->bellsGroup->Controls->Add(this->bellsEqualTo);
        this->bellsGroup->Controls->Add(this->bellsLessThan);
        this->bellsGroup->Location = System::Drawing::Point(110, 6);
        this->bellsGroup->Name = L"bellsGroup";
        this->bellsGroup->Size = System::Drawing::Size(88, 119);
        this->bellsGroup->TabIndex = 41;
        this->bellsGroup->TabStop = false;
        this->bellsGroup->Text = L"Bells";
        // 
        // bellsEditor
        // 
        this->bellsEditor->Location = System::Drawing::Point(6, 19);
        this->bellsEditor->Name = L"bellsEditor";
        this->bellsEditor->Size = System::Drawing::Size(44, 20);
        this->bellsEditor->TabIndex = 42;
        this->bellsEditor->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &PealSearchUI::Editor_KeyDown);
        this->bellsEditor->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &PealSearchUI::Editor_KeyPressed);
        // 
        // bellsMoreThan
        // 
        this->bellsMoreThan->AutoSize = true;
        this->bellsMoreThan->Location = System::Drawing::Point(6, 91);
        this->bellsMoreThan->Name = L"bellsMoreThan";
        this->bellsMoreThan->Size = System::Drawing::Size(73, 17);
        this->bellsMoreThan->TabIndex = 43;
        this->bellsMoreThan->Text = L"More than";
        this->bellsMoreThan->UseVisualStyleBackColor = true;
        // 
        // bellsEqualTo
        // 
        this->bellsEqualTo->AutoSize = true;
        this->bellsEqualTo->Checked = true;
        this->bellsEqualTo->Location = System::Drawing::Point(6, 68);
        this->bellsEqualTo->Name = L"bellsEqualTo";
        this->bellsEqualTo->Size = System::Drawing::Size(64, 17);
        this->bellsEqualTo->TabIndex = 44;
        this->bellsEqualTo->TabStop = true;
        this->bellsEqualTo->Text = L"Equal to";
        this->bellsEqualTo->UseVisualStyleBackColor = true;
        // 
        // bellsLessThan
        // 
        this->bellsLessThan->AutoSize = true;
        this->bellsLessThan->Location = System::Drawing::Point(6, 45);
        this->bellsLessThan->Name = L"bellsLessThan";
        this->bellsLessThan->Size = System::Drawing::Size(71, 17);
        this->bellsLessThan->TabIndex = 45;
        this->bellsLessThan->Text = L"Less than";
        this->bellsLessThan->UseVisualStyleBackColor = true;
        // 
        // methodTab
        // 
        this->methodTab->Controls->Add(this->methodGroup);
        this->methodTab->Controls->Add(this->methodSeriesGroup);
        this->methodTab->Controls->Add(this->compositionGroup);
        this->methodTab->Location = System::Drawing::Point(4, 22);
        this->methodTab->Name = L"methodTab";
        this->methodTab->Padding = System::Windows::Forms::Padding(3);
        this->methodTab->Size = System::Drawing::Size(1129, 182);
        this->methodTab->TabIndex = 53;
        this->methodTab->Text = L"Method";
        this->methodTab->UseVisualStyleBackColor = true;
        // 
        // methodGroup
        // 
        this->methodGroup->Controls->Add(this->splicedMethod);
        this->methodGroup->Controls->Add(this->splicedMethods);
        this->methodGroup->Controls->Add(this->dualOrder);
        this->methodGroup->Controls->Add(this->methodSelector);
        this->methodGroup->Location = System::Drawing::Point(4, 6);
        this->methodGroup->Name = L"methodGroup";
        this->methodGroup->Size = System::Drawing::Size(235, 129);
        this->methodGroup->TabIndex = 54;
        this->methodGroup->TabStop = false;
        this->methodGroup->Text = L"Method";
        // 
        // splicedMethod
        // 
        this->splicedMethod->AutoSize = true;
        this->splicedMethod->Location = System::Drawing::Point(7, 98);
        this->splicedMethod->Name = L"splicedMethod";
        this->splicedMethod->Size = System::Drawing::Size(118, 17);
        this->splicedMethod->TabIndex = 58;
        this->splicedMethod->Text = L"Any spliced method";
        this->splicedMethod->UseVisualStyleBackColor = true;
        // 
        // splicedMethods
        // 
        this->splicedMethods->AutoSize = true;
        this->splicedMethods->Location = System::Drawing::Point(7, 75);
        this->splicedMethods->Name = L"splicedMethods";
        this->splicedMethods->Size = System::Drawing::Size(198, 17);
        this->splicedMethods->TabIndex = 57;
        this->splicedMethods->Text = L"Also search spliced methods / series";
        this->splicedMethods->UseVisualStyleBackColor = true;
        // 
        // dualOrder
        // 
        this->dualOrder->AutoSize = true;
        this->dualOrder->Location = System::Drawing::Point(7, 51);
        this->dualOrder->Name = L"dualOrder";
        this->dualOrder->Size = System::Drawing::Size(223, 17);
        this->dualOrder->TabIndex = 56;
        this->dualOrder->Text = L"Spliced Order (e.g. Cinques and Maximus)";
        this->dualOrder->UseVisualStyleBackColor = true;
        // 
        // methodSelector
        // 
        this->methodSelector->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
        this->methodSelector->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
        this->methodSelector->FormattingEnabled = true;
        this->methodSelector->Location = System::Drawing::Point(7, 20);
        this->methodSelector->Name = L"methodSelector";
        this->methodSelector->Size = System::Drawing::Size(214, 21);
        this->methodSelector->TabIndex = 55;
        // 
        // methodSeriesGroup
        // 
        this->methodSeriesGroup->Controls->Add(this->containsMethodSeries);
        this->methodSeriesGroup->Controls->Add(this->methodSeriesSelector);
        this->methodSeriesGroup->Controls->Add(this->noOfMethodsInSeries);
        this->methodSeriesGroup->Controls->Add(this->noOfSeriesLbl);
        this->methodSeriesGroup->Location = System::Drawing::Point(243, 6);
        this->methodSeriesGroup->Name = L"methodSeriesGroup";
        this->methodSeriesGroup->Size = System::Drawing::Size(335, 75);
        this->methodSeriesGroup->TabIndex = 58;
        this->methodSeriesGroup->TabStop = false;
        this->methodSeriesGroup->Text = L"Method Series";
        // 
        // containsMethodSeries
        // 
        this->containsMethodSeries->AutoSize = true;
        this->containsMethodSeries->Location = System::Drawing::Point(166, 49);
        this->containsMethodSeries->Name = L"containsMethodSeries";
        this->containsMethodSeries->Size = System::Drawing::Size(135, 17);
        this->containsMethodSeries->TabIndex = 62;
        this->containsMethodSeries->Text = L"Contains method series";
        this->containsMethodSeries->UseVisualStyleBackColor = true;
        // 
        // methodSeriesSelector
        // 
        this->methodSeriesSelector->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
        this->methodSeriesSelector->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
        this->methodSeriesSelector->FormattingEnabled = true;
        this->methodSeriesSelector->Location = System::Drawing::Point(7, 20);
        this->methodSeriesSelector->Name = L"methodSeriesSelector";
        this->methodSeriesSelector->Size = System::Drawing::Size(314, 21);
        this->methodSeriesSelector->TabIndex = 59;
        // 
        // noOfMethodsInSeries
        // 
        this->noOfMethodsInSeries->Location = System::Drawing::Point(110, 47);
        this->noOfMethodsInSeries->Name = L"noOfMethodsInSeries";
        this->noOfMethodsInSeries->Size = System::Drawing::Size(50, 20);
        this->noOfMethodsInSeries->TabIndex = 60;
        // 
        // noOfSeriesLbl
        // 
        this->noOfSeriesLbl->AutoSize = true;
        this->noOfSeriesLbl->Location = System::Drawing::Point(5, 49);
        this->noOfSeriesLbl->Name = L"noOfSeriesLbl";
        this->noOfSeriesLbl->Size = System::Drawing::Size(102, 13);
        this->noOfSeriesLbl->TabIndex = 61;
        this->noOfSeriesLbl->Text = L"Number of methods:";
        // 
        // compositionGroup
        // 
        this->compositionGroup->Controls->Add(this->compositionSelector);
        this->compositionGroup->Location = System::Drawing::Point(243, 85);
        this->compositionGroup->Name = L"compositionGroup";
        this->compositionGroup->Size = System::Drawing::Size(435, 50);
        this->compositionGroup->TabIndex = 60;
        this->compositionGroup->TabStop = false;
        this->compositionGroup->Text = L"Composition";
        // 
        // compositionSelector
        // 
        this->compositionSelector->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
        this->compositionSelector->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
        this->compositionSelector->FormattingEnabled = true;
        this->compositionSelector->Location = System::Drawing::Point(7, 20);
        this->compositionSelector->Name = L"compositionSelector";
        this->compositionSelector->Size = System::Drawing::Size(414, 21);
        this->compositionSelector->TabIndex = 61;
        // 
        // ringersPage
        // 
        this->ringersPage->Controls->Add(ringersPageLayoutPanel);
        this->ringersPage->Location = System::Drawing::Point(4, 22);
        this->ringersPage->Name = L"ringersPage";
        this->ringersPage->Padding = System::Windows::Forms::Padding(3);
        this->ringersPage->Size = System::Drawing::Size(1129, 182);
        this->ringersPage->TabIndex = 26;
        this->ringersPage->Text = L"Ringers";
        this->ringersPage->UseVisualStyleBackColor = true;
        // 
        // ringersPageLayoutPanel
        // 
        ringersPageLayoutPanel->ColumnCount = 3;
        ringersPageLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
            100)));
        ringersPageLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
        ringersPageLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
        ringersPageLayoutPanel->Controls->Add(this->conductedGroup, 1, 0);
        ringersPageLayoutPanel->Controls->Add(this->ringerGroup, 0, 0);
        ringersPageLayoutPanel->Controls->Add(this->composerGroup, 2, 0);
        ringersPageLayoutPanel->Controls->Add(settingsGroup, 2, 1);
        ringersPageLayoutPanel->Controls->Add(strapperGroup, 1, 2);
        ringersPageLayoutPanel->Dock = System::Windows::Forms::DockStyle::Fill;
        ringersPageLayoutPanel->Location = System::Drawing::Point(3, 3);
        ringersPageLayoutPanel->Name = L"ringersPageLayoutPanel";
        ringersPageLayoutPanel->RowCount = 3;
        ringersPageLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 55)));
        ringersPageLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 75)));
        ringersPageLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
        ringersPageLayoutPanel->Size = System::Drawing::Size(1123, 176);
        ringersPageLayoutPanel->TabIndex = 3;
        // 
        // conductedGroup
        // 
        this->conductedGroup->Controls->Add(this->jointConductor);
        this->conductedGroup->Controls->Add(this->silentConductor);
        this->conductedGroup->Controls->Add(this->conducted);
        this->conductedGroup->Controls->Add(this->conductorSelector);
        this->conductedGroup->Dock = System::Windows::Forms::DockStyle::Fill;
        this->conductedGroup->Location = System::Drawing::Point(679, 3);
        this->conductedGroup->Name = L"conductedGroup";
        ringersPageLayoutPanel->SetRowSpan(this->conductedGroup, 2);
        this->conductedGroup->Size = System::Drawing::Size(203, 124);
        this->conductedGroup->TabIndex = 1;
        this->conductedGroup->TabStop = false;
        this->conductedGroup->Text = L"Conductor";
        // 
        // jointConductor
        // 
        this->jointConductor->AutoSize = true;
        this->jointConductor->Location = System::Drawing::Point(7, 95);
        this->jointConductor->Name = L"jointConductor";
        this->jointConductor->Size = System::Drawing::Size(108, 17);
        this->jointConductor->TabIndex = 3;
        this->jointConductor->Text = L"Jointly conducted";
        this->jointConductor->UseVisualStyleBackColor = true;
        this->jointConductor->CheckedChanged += gcnew System::EventHandler(this, &PealSearchUI::jointConductor_CheckedChanged);
        // 
        // silentConductor
        // 
        this->silentConductor->AutoSize = true;
        this->silentConductor->Location = System::Drawing::Point(7, 72);
        this->silentConductor->Name = L"silentConductor";
        this->silentConductor->Size = System::Drawing::Size(147, 17);
        this->silentConductor->TabIndex = 2;
        this->silentConductor->Text = L"Silent and non conducted";
        this->silentConductor->UseVisualStyleBackColor = true;
        this->silentConductor->CheckedChanged += gcnew System::EventHandler(this, &PealSearchUI::silentConductor_CheckedChanged);
        // 
        // conducted
        // 
        this->conducted->AutoSize = true;
        this->conducted->Checked = true;
        this->conducted->Location = System::Drawing::Point(7, 48);
        this->conducted->Name = L"conducted";
        this->conducted->Size = System::Drawing::Size(77, 17);
        this->conducted->TabIndex = 1;
        this->conducted->TabStop = true;
        this->conducted->Text = L"Conducted";
        this->conducted->UseVisualStyleBackColor = true;
        this->conducted->CheckedChanged += gcnew System::EventHandler(this, &PealSearchUI::conducted_CheckedChanged);
        // 
        // conductorSelector
        // 
        this->conductorSelector->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
        this->conductorSelector->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
        this->conductorSelector->FormattingEnabled = true;
        this->conductorSelector->Location = System::Drawing::Point(6, 20);
        this->conductorSelector->Name = L"conductorSelector";
        this->conductorSelector->Size = System::Drawing::Size(187, 21);
        this->conductorSelector->TabIndex = 0;
        // 
        // ringerGroup
        // 
        this->ringerGroup->Controls->Add(tableLayoutPanel2);
        this->ringerGroup->Dock = System::Windows::Forms::DockStyle::Fill;
        this->ringerGroup->Location = System::Drawing::Point(3, 3);
        this->ringerGroup->Name = L"ringerGroup";
        ringersPageLayoutPanel->SetRowSpan(this->ringerGroup, 3);
        this->ringerGroup->Size = System::Drawing::Size(670, 170);
        this->ringerGroup->TabIndex = 0;
        this->ringerGroup->TabStop = false;
        this->ringerGroup->Text = L"Ringers";
        // 
        // tableLayoutPanel2
        // 
        tableLayoutPanel2->ColumnCount = 2;
        tableLayoutPanel2->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
        tableLayoutPanel2->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
        tableLayoutPanel2->Controls->Add(this->asStrapper, 1, 0);
        tableLayoutPanel2->Controls->Add(this->ringerSelector, 0, 0);
        tableLayoutPanel2->Controls->Add(this->ringerEditor, 0, 2);
        tableLayoutPanel2->Controls->Add(this->excludingRinger, 1, 1);
        tableLayoutPanel2->Dock = System::Windows::Forms::DockStyle::Fill;
        tableLayoutPanel2->Location = System::Drawing::Point(3, 16);
        tableLayoutPanel2->Name = L"tableLayoutPanel2";
        tableLayoutPanel2->RowCount = 3;
        tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
        tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
        tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
        tableLayoutPanel2->Size = System::Drawing::Size(664, 151);
        tableLayoutPanel2->TabIndex = 6;
        // 
        // asStrapper
        // 
        this->asStrapper->AutoSize = true;
        this->asStrapper->Enabled = false;
        this->asStrapper->Location = System::Drawing::Point(580, 3);
        this->asStrapper->Name = L"asStrapper";
        this->asStrapper->Size = System::Drawing::Size(81, 17);
        this->asStrapper->TabIndex = 2;
        this->asStrapper->Text = L"As Strapper";
        this->asStrapper->UseVisualStyleBackColor = true;
        // 
        // ringerSelector
        // 
        this->ringerSelector->Dock = System::Windows::Forms::DockStyle::Fill;
        this->ringerSelector->FormattingEnabled = true;
        this->ringerSelector->Location = System::Drawing::Point(3, 3);
        this->ringerSelector->MultiColumn = true;
        this->ringerSelector->Name = L"ringerSelector";
        tableLayoutPanel2->SetRowSpan(this->ringerSelector, 2);
        this->ringerSelector->Size = System::Drawing::Size(571, 119);
        this->ringerSelector->TabIndex = 0;
        this->ringerSelector->ItemCheck += gcnew System::Windows::Forms::ItemCheckEventHandler(this, &PealSearchUI::ringerSelector_ItemCheck);
        // 
        // ringerEditor
        // 
        this->ringerEditor->Dock = System::Windows::Forms::DockStyle::Fill;
        this->ringerEditor->Location = System::Drawing::Point(3, 128);
        this->ringerEditor->Name = L"ringerEditor";
        this->ringerEditor->Size = System::Drawing::Size(571, 20);
        this->ringerEditor->TabIndex = 1;
        this->ringerEditor->TextChanged += gcnew System::EventHandler(this, &PealSearchUI::ringerEditor_TextChanged);
        // 
        // excludingRinger
        // 
        this->excludingRinger->AutoSize = true;
        this->excludingRinger->Location = System::Drawing::Point(580, 26);
        this->excludingRinger->Name = L"excludingRinger";
        this->excludingRinger->Size = System::Drawing::Size(72, 17);
        this->excludingRinger->TabIndex = 3;
        this->excludingRinger->Text = L"Excluding";
        this->excludingRinger->UseVisualStyleBackColor = true;
        // 
        // composerGroup
        // 
        this->composerGroup->Controls->Add(this->composerEditor);
        this->composerGroup->Location = System::Drawing::Point(888, 3);
        this->composerGroup->Name = L"composerGroup";
        this->composerGroup->Size = System::Drawing::Size(232, 49);
        this->composerGroup->TabIndex = 2;
        this->composerGroup->TabStop = false;
        this->composerGroup->Text = L"Composer";
        // 
        // composerEditor
        // 
        this->composerEditor->Location = System::Drawing::Point(6, 19);
        this->composerEditor->Name = L"composerEditor";
        this->composerEditor->Size = System::Drawing::Size(215, 20);
        this->composerEditor->TabIndex = 0;
        // 
        // settingsGroup
        // 
        settingsGroup->Controls->Add(this->realBellNumber);
        settingsGroup->Controls->Add(this->doubleHanded);
        settingsGroup->Dock = System::Windows::Forms::DockStyle::Fill;
        settingsGroup->Location = System::Drawing::Point(888, 58);
        settingsGroup->Name = L"settingsGroup";
        settingsGroup->Size = System::Drawing::Size(232, 69);
        settingsGroup->TabIndex = 82;
        settingsGroup->TabStop = false;
        settingsGroup->Text = L"Other";
        // 
        // realBellNumber
        // 
        this->realBellNumber->AutoSize = true;
        this->realBellNumber->Location = System::Drawing::Point(6, 19);
        this->realBellNumber->Name = L"realBellNumber";
        this->realBellNumber->Size = System::Drawing::Size(137, 17);
        this->realBellNumber->TabIndex = 81;
        this->realBellNumber->Text = L"Display real bell number";
        this->toolTip1->SetToolTip(this->realBellNumber, L"Show bell number in tower, rather than bell number in ring");
        this->realBellNumber->UseVisualStyleBackColor = true;
        // 
        // doubleHanded
        // 
        this->doubleHanded->AutoSize = true;
        this->doubleHanded->Location = System::Drawing::Point(6, 39);
        this->doubleHanded->Name = L"doubleHanded";
        this->doubleHanded->Padding = System::Windows::Forms::Padding(0, 5, 0, 0);
        this->doubleHanded->Size = System::Drawing::Size(99, 22);
        this->doubleHanded->TabIndex = 4;
        this->doubleHanded->Text = L"Double handed";
        this->doubleHanded->UseVisualStyleBackColor = true;
        // 
        // strapperGroup
        // 
        ringersPageLayoutPanel->SetColumnSpan(strapperGroup, 2);
        strapperGroup->Controls->Add(this->bothStrapperOptions);
        strapperGroup->Controls->Add(this->withoutStrapper);
        strapperGroup->Controls->Add(this->withStrapper);
        strapperGroup->Dock = System::Windows::Forms::DockStyle::Fill;
        strapperGroup->Location = System::Drawing::Point(679, 133);
        strapperGroup->Name = L"strapperGroup";
        strapperGroup->Size = System::Drawing::Size(441, 40);
        strapperGroup->TabIndex = 83;
        strapperGroup->TabStop = false;
        strapperGroup->Text = L"Strapper";
        // 
        // bothStrapperOptions
        // 
        this->bothStrapperOptions->AutoSize = true;
        this->bothStrapperOptions->Location = System::Drawing::Point(303, 15);
        this->bothStrapperOptions->Name = L"bothStrapperOptions";
        this->bothStrapperOptions->Size = System::Drawing::Size(47, 17);
        this->bothStrapperOptions->TabIndex = 2;
        this->bothStrapperOptions->TabStop = true;
        this->bothStrapperOptions->Text = L"Both";
        this->bothStrapperOptions->UseVisualStyleBackColor = true;
        // 
        // withoutStrapper
        // 
        this->withoutStrapper->AutoSize = true;
        this->withoutStrapper->Location = System::Drawing::Point(148, 15);
        this->withoutStrapper->Name = L"withoutStrapper";
        this->withoutStrapper->Size = System::Drawing::Size(105, 17);
        this->withoutStrapper->TabIndex = 1;
        this->withoutStrapper->TabStop = true;
        this->withoutStrapper->Text = L"Without Strapper";
        this->withoutStrapper->UseVisualStyleBackColor = true;
        // 
        // withStrapper
        // 
        this->withStrapper->AutoSize = true;
        this->withStrapper->Location = System::Drawing::Point(7, 15);
        this->withStrapper->Name = L"withStrapper";
        this->withStrapper->Size = System::Drawing::Size(88, 17);
        this->withStrapper->TabIndex = 0;
        this->withStrapper->TabStop = true;
        this->withStrapper->Text = L"With strapper";
        this->withStrapper->UseVisualStyleBackColor = true;
        // 
        // referencesPage
        // 
        this->referencesPage->Controls->Add(withdrawnBox);
        this->referencesPage->Controls->Add(errorsGroupBox);
        this->referencesPage->Controls->Add(pealFeesGroup);
        this->referencesPage->Controls->Add(this->notesGroup);
        this->referencesPage->Controls->Add(this->rwGroup);
        this->referencesPage->Controls->Add(this->bellBoardGroup);
        this->referencesPage->Location = System::Drawing::Point(4, 22);
        this->referencesPage->Name = L"referencesPage";
        this->referencesPage->Padding = System::Windows::Forms::Padding(3);
        this->referencesPage->Size = System::Drawing::Size(1129, 182);
        this->referencesPage->TabIndex = 62;
        this->referencesPage->Text = L"Other";
        this->referencesPage->UseVisualStyleBackColor = true;
        // 
        // withdrawnBox
        // 
        withdrawnBox->Controls->Add(this->bothValidAndWithdrawnBtn);
        withdrawnBox->Controls->Add(this->validOnlyBtn);
        withdrawnBox->Controls->Add(this->withdrawnOnlyBtn);
        withdrawnBox->Location = System::Drawing::Point(521, 81);
        withdrawnBox->Name = L"withdrawnBox";
        withdrawnBox->Size = System::Drawing::Size(147, 72);
        withdrawnBox->TabIndex = 84;
        withdrawnBox->TabStop = false;
        withdrawnBox->Text = L"Withdrawn";
        // 
        // bothValidAndWithdrawnBtn
        // 
        this->bothValidAndWithdrawnBtn->AutoSize = true;
        this->bothValidAndWithdrawnBtn->Checked = true;
        this->bothValidAndWithdrawnBtn->Location = System::Drawing::Point(88, 19);
        this->bothValidAndWithdrawnBtn->Name = L"bothValidAndWithdrawnBtn";
        this->bothValidAndWithdrawnBtn->Size = System::Drawing::Size(47, 17);
        this->bothValidAndWithdrawnBtn->TabIndex = 2;
        this->bothValidAndWithdrawnBtn->TabStop = true;
        this->bothValidAndWithdrawnBtn->Text = L"Both";
        this->bothValidAndWithdrawnBtn->UseVisualStyleBackColor = true;
        // 
        // validOnlyBtn
        // 
        this->validOnlyBtn->AutoSize = true;
        this->validOnlyBtn->Location = System::Drawing::Point(6, 40);
        this->validOnlyBtn->Name = L"validOnlyBtn";
        this->validOnlyBtn->Size = System::Drawing::Size(48, 17);
        this->validOnlyBtn->TabIndex = 1;
        this->validOnlyBtn->Text = L"Valid";
        this->validOnlyBtn->UseVisualStyleBackColor = true;
        // 
        // withdrawnOnlyBtn
        // 
        this->withdrawnOnlyBtn->AutoSize = true;
        this->withdrawnOnlyBtn->Location = System::Drawing::Point(6, 19);
        this->withdrawnOnlyBtn->Name = L"withdrawnOnlyBtn";
        this->withdrawnOnlyBtn->Size = System::Drawing::Size(76, 17);
        this->withdrawnOnlyBtn->TabIndex = 0;
        this->withdrawnOnlyBtn->Text = L"Withdrawn";
        this->withdrawnOnlyBtn->UseVisualStyleBackColor = true;
        // 
        // errorsGroupBox
        // 
        errorsGroupBox->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
        errorsGroupBox->Controls->Add(this->duplicate);
        errorsGroupBox->Controls->Add(this->invalid);
        errorsGroupBox->Location = System::Drawing::Point(383, 81);
        errorsGroupBox->Name = L"errorsGroupBox";
        errorsGroupBox->Size = System::Drawing::Size(132, 72);
        errorsGroupBox->TabIndex = 83;
        errorsGroupBox->TabStop = false;
        errorsGroupBox->Text = L"Errors";
        // 
        // duplicate
        // 
        this->duplicate->AutoSize = true;
        this->duplicate->Location = System::Drawing::Point(7, 18);
        this->duplicate->Name = L"duplicate";
        this->duplicate->Size = System::Drawing::Size(111, 17);
        this->duplicate->TabIndex = 82;
        this->duplicate->Text = L"Possible duplicate";
        this->toolTip1->SetToolTip(this->duplicate, L"Display performances with are a possible duplicate");
        this->duplicate->UseVisualStyleBackColor = true;
        // 
        // invalid
        // 
        this->invalid->AutoSize = true;
        this->invalid->Location = System::Drawing::Point(7, 41);
        this->invalid->Name = L"invalid";
        this->invalid->Size = System::Drawing::Size(91, 17);
        this->invalid->TabIndex = 79;
        this->invalid->Text = L"Contains error";
        this->invalid->UseVisualStyleBackColor = true;
        // 
        // pealFeesGroup
        // 
        pealFeesGroup->Controls->Add(this->inconsistantPealFees);
        pealFeesGroup->Controls->Add(this->unpaidPealFees);
        pealFeesGroup->Location = System::Drawing::Point(674, 81);
        pealFeesGroup->Name = L"pealFeesGroup";
        pealFeesGroup->Size = System::Drawing::Size(143, 72);
        pealFeesGroup->TabIndex = 81;
        pealFeesGroup->TabStop = false;
        pealFeesGroup->Text = L"Peal fees";
        // 
        // inconsistantPealFees
        // 
        this->inconsistantPealFees->AutoSize = true;
        this->inconsistantPealFees->Enabled = false;
        this->inconsistantPealFees->Location = System::Drawing::Point(6, 42);
        this->inconsistantPealFees->Name = L"inconsistantPealFees";
        this->inconsistantPealFees->Size = System::Drawing::Size(129, 17);
        this->inconsistantPealFees->TabIndex = 1;
        this->inconsistantPealFees->Text = L"Inconsistant peal fees";
        this->toolTip1->SetToolTip(this->inconsistantPealFees, L"Show peals with fees that are lower than a median for that society in that year. "
            L"This option isn\'t enabled until an association is selected.");
        this->inconsistantPealFees->UseVisualStyleBackColor = true;
        // 
        // unpaidPealFees
        // 
        this->unpaidPealFees->AutoSize = true;
        this->unpaidPealFees->Location = System::Drawing::Point(6, 19);
        this->unpaidPealFees->Name = L"unpaidPealFees";
        this->unpaidPealFees->Size = System::Drawing::Size(106, 17);
        this->unpaidPealFees->TabIndex = 0;
        this->unpaidPealFees->Text = L"Unpaid peal fees";
        this->toolTip1->SetToolTip(this->unpaidPealFees, L"Show peals with no peal fees at all.");
        this->unpaidPealFees->UseVisualStyleBackColor = true;
        // 
        // notesGroup
        // 
        this->notesGroup->Controls->Add(this->notable);
        this->notesGroup->Controls->Add(this->nonBlankNotes);
        this->notesGroup->Controls->Add(this->notes);
        this->notesGroup->Location = System::Drawing::Point(383, 6);
        this->notesGroup->Name = L"notesGroup";
        this->notesGroup->Size = System::Drawing::Size(434, 68);
        this->notesGroup->TabIndex = 75;
        this->notesGroup->TabStop = false;
        this->notesGroup->Text = L"Notes";
        // 
        // notable
        // 
        this->notable->AutoSize = true;
        this->notable->Location = System::Drawing::Point(365, 45);
        this->notable->Name = L"notable";
        this->notable->Size = System::Drawing::Size(63, 17);
        this->notable->TabIndex = 78;
        this->notable->Text = L"Notable";
        this->notable->UseVisualStyleBackColor = true;
        // 
        // nonBlankNotes
        // 
        this->nonBlankNotes->AutoSize = true;
        this->nonBlankNotes->Location = System::Drawing::Point(7, 45);
        this->nonBlankNotes->Name = L"nonBlankNotes";
        this->nonBlankNotes->Size = System::Drawing::Size(104, 17);
        this->nonBlankNotes->TabIndex = 77;
        this->nonBlankNotes->Text = L"Non-blank notes";
        this->nonBlankNotes->UseVisualStyleBackColor = true;
        this->nonBlankNotes->CheckedChanged += gcnew System::EventHandler(this, &PealSearchUI::nonBlankNotes_CheckedChanged);
        // 
        // notes
        // 
        this->notes->Location = System::Drawing::Point(6, 19);
        this->notes->Name = L"notes";
        this->notes->Size = System::Drawing::Size(422, 20);
        this->notes->TabIndex = 76;
        // 
        // rwGroup
        // 
        this->rwGroup->Controls->Add(this->invalidRWReference);
        this->rwGroup->Controls->Add(this->fullValidRWReference);
        this->rwGroup->Controls->Add(this->nonEmptyRWRef);
        this->rwGroup->Controls->Add(this->rwRef);
        this->rwGroup->Controls->Add(this->emptyRWRef);
        this->rwGroup->Location = System::Drawing::Point(5, 6);
        this->rwGroup->Name = L"rwGroup";
        this->rwGroup->Size = System::Drawing::Size(183, 147);
        this->rwGroup->TabIndex = 63;
        this->rwGroup->TabStop = false;
        this->rwGroup->Text = L"Ringing world";
        // 
        // invalidRWReference
        // 
        this->invalidRWReference->AutoSize = true;
        this->invalidRWReference->Location = System::Drawing::Point(6, 114);
        this->invalidRWReference->Name = L"invalidRWReference";
        this->invalidRWReference->Size = System::Drawing::Size(156, 17);
        this->invalidRWReference->TabIndex = 68;
        this->invalidRWReference->Text = L"Search for invalid reference";
        this->invalidRWReference->UseVisualStyleBackColor = true;
        this->invalidRWReference->CheckedChanged += gcnew System::EventHandler(this, &PealSearchUI::invalidRWReference_CheckedChanged);
        // 
        // fullValidRWReference
        // 
        this->fullValidRWReference->AutoSize = true;
        this->fullValidRWReference->Location = System::Drawing::Point(6, 91);
        this->fullValidRWReference->Name = L"fullValidRWReference";
        this->fullValidRWReference->Size = System::Drawing::Size(164, 17);
        this->fullValidRWReference->TabIndex = 67;
        this->fullValidRWReference->Text = L"Search for full valid reference";
        this->fullValidRWReference->UseVisualStyleBackColor = true;
        this->fullValidRWReference->CheckedChanged += gcnew System::EventHandler(this, &PealSearchUI::fullValidRWReference_CheckedChanged);
        // 
        // nonEmptyRWRef
        // 
        this->nonEmptyRWRef->AutoSize = true;
        this->nonEmptyRWRef->Location = System::Drawing::Point(6, 68);
        this->nonEmptyRWRef->Name = L"nonEmptyRWRef";
        this->nonEmptyRWRef->Size = System::Drawing::Size(173, 17);
        this->nonEmptyRWRef->TabIndex = 66;
        this->nonEmptyRWRef->Text = L"Search for non blank reference";
        this->nonEmptyRWRef->UseVisualStyleBackColor = true;
        this->nonEmptyRWRef->CheckedChanged += gcnew System::EventHandler(this, &PealSearchUI::nonEmptyRWRef_CheckedChanged);
        // 
        // rwRef
        // 
        this->rwRef->Location = System::Drawing::Point(6, 19);
        this->rwRef->Name = L"rwRef";
        this->rwRef->Size = System::Drawing::Size(170, 20);
        this->rwRef->TabIndex = 64;
        this->rwRef->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &PealSearchUI::Editor_KeyDown);
        this->rwRef->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &PealSearchUI::Editor_KeyPressed);
        // 
        // emptyRWRef
        // 
        this->emptyRWRef->AutoSize = true;
        this->emptyRWRef->Location = System::Drawing::Point(6, 45);
        this->emptyRWRef->Name = L"emptyRWRef";
        this->emptyRWRef->Size = System::Drawing::Size(152, 17);
        this->emptyRWRef->TabIndex = 65;
        this->emptyRWRef->Text = L"Search for blank reference";
        this->emptyRWRef->UseVisualStyleBackColor = true;
        this->emptyRWRef->CheckedChanged += gcnew System::EventHandler(this, &PealSearchUI::emptyRWRef_CheckedChanged);
        // 
        // bellBoardGroup
        // 
        this->bellBoardGroup->Controls->Add(this->nonBlankBellBoardRef);
        this->bellBoardGroup->Controls->Add(this->bellBoardRef);
        this->bellBoardGroup->Controls->Add(this->blankBellBoardRef);
        this->bellBoardGroup->Location = System::Drawing::Point(194, 6);
        this->bellBoardGroup->Name = L"bellBoardGroup";
        this->bellBoardGroup->Size = System::Drawing::Size(183, 91);
        this->bellBoardGroup->TabIndex = 72;
        this->bellBoardGroup->TabStop = false;
        this->bellBoardGroup->Text = L"Bell Board";
        // 
        // nonBlankBellBoardRef
        // 
        this->nonBlankBellBoardRef->AutoSize = true;
        this->nonBlankBellBoardRef->Location = System::Drawing::Point(6, 68);
        this->nonBlankBellBoardRef->Name = L"nonBlankBellBoardRef";
        this->nonBlankBellBoardRef->Size = System::Drawing::Size(173, 17);
        this->nonBlankBellBoardRef->TabIndex = 75;
        this->nonBlankBellBoardRef->Text = L"Search for non blank reference";
        this->nonBlankBellBoardRef->UseVisualStyleBackColor = true;
        this->nonBlankBellBoardRef->CheckedChanged += gcnew System::EventHandler(this, &PealSearchUI::nonBlankBellBoardRef_CheckedChanged);
        // 
        // bellBoardRef
        // 
        this->bellBoardRef->Location = System::Drawing::Point(6, 19);
        this->bellBoardRef->Name = L"bellBoardRef";
        this->bellBoardRef->Size = System::Drawing::Size(171, 20);
        this->bellBoardRef->TabIndex = 73;
        this->bellBoardRef->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &PealSearchUI::Editor_KeyDown);
        this->bellBoardRef->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &PealSearchUI::Editor_KeyPressed);
        // 
        // blankBellBoardRef
        // 
        this->blankBellBoardRef->AutoSize = true;
        this->blankBellBoardRef->Location = System::Drawing::Point(6, 45);
        this->blankBellBoardRef->Name = L"blankBellBoardRef";
        this->blankBellBoardRef->Size = System::Drawing::Size(152, 17);
        this->blankBellBoardRef->TabIndex = 74;
        this->blankBellBoardRef->Text = L"Search for blank reference";
        this->blankBellBoardRef->UseVisualStyleBackColor = true;
        this->blankBellBoardRef->CheckedChanged += gcnew System::EventHandler(this, &PealSearchUI::blankBellBoardRef_CheckedChanged);
        // 
        // advancedRingersTab
        // 
        this->advancedRingersTab->Controls->Add(advancedRingerGrp);
        this->advancedRingersTab->Location = System::Drawing::Point(4, 22);
        this->advancedRingersTab->Name = L"advancedRingersTab";
        this->advancedRingersTab->Padding = System::Windows::Forms::Padding(3);
        this->advancedRingersTab->Size = System::Drawing::Size(1129, 182);
        this->advancedRingersTab->TabIndex = 63;
        this->advancedRingersTab->Text = L"Advanced ringers";
        this->advancedRingersTab->UseVisualStyleBackColor = true;
        // 
        // advancedRingerGrp
        // 
        advancedRingerGrp->ColumnCount = 1;
        advancedRingerGrp->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
        advancedRingerGrp->Controls->Add(newRinger, 0, 0);
        advancedRingerGrp->Controls->Add(this->ringerAdvSearchParams, 0, 1);
        advancedRingerGrp->Dock = System::Windows::Forms::DockStyle::Fill;
        advancedRingerGrp->Location = System::Drawing::Point(3, 3);
        advancedRingerGrp->Name = L"advancedRingerGrp";
        advancedRingerGrp->RowCount = 2;
        advancedRingerGrp->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 52)));
        advancedRingerGrp->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
        advancedRingerGrp->Size = System::Drawing::Size(1123, 176);
        advancedRingerGrp->TabIndex = 2;
        // 
        // newRinger
        // 
        newRinger->Controls->Add(this->removeAdvancedRingers);
        newRinger->Controls->Add(this->advBellNo);
        newRinger->Controls->Add(this->advRingerName);
        newRinger->Controls->Add(this->addAdvancedRinger);
        newRinger->Dock = System::Windows::Forms::DockStyle::Fill;
        newRinger->Location = System::Drawing::Point(3, 3);
        newRinger->Name = L"newRinger";
        newRinger->Size = System::Drawing::Size(1117, 46);
        newRinger->TabIndex = 0;
        newRinger->TabStop = false;
        newRinger->Text = L"Add ringer";
        // 
        // removeAdvancedRingers
        // 
        this->removeAdvancedRingers->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
        this->removeAdvancedRingers->Location = System::Drawing::Point(1036, 17);
        this->removeAdvancedRingers->Name = L"removeAdvancedRingers";
        this->removeAdvancedRingers->Size = System::Drawing::Size(75, 23);
        this->removeAdvancedRingers->TabIndex = 4;
        this->removeAdvancedRingers->Text = L"Remove checked";
        this->toolTip1->SetToolTip(this->removeAdvancedRingers, L"Remove checked ringers, or ignore on search");
        this->removeAdvancedRingers->UseVisualStyleBackColor = true;
        this->removeAdvancedRingers->Click += gcnew System::EventHandler(this, &PealSearchUI::RemoveAdvancedRingers_Click);
        // 
        // advBellNo
        // 
        this->advBellNo->AutoCompleteCustomSource->AddRange(gcnew cli::array< System::String^  >(13) {
            L"Any bell", L"Treble", L"2",
                L"3", L"4", L"5", L"6", L"N-5", L"N-4", L"N-3", L"N-2", L"N-1", L"Tenor"
        });
        this->advBellNo->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
        this->advBellNo->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
        this->advBellNo->FormattingEnabled = true;
        this->advBellNo->Location = System::Drawing::Point(423, 20);
        this->advBellNo->Name = L"advBellNo";
        this->advBellNo->Size = System::Drawing::Size(121, 21);
        this->advBellNo->TabIndex = 2;
        // 
        // advRingerName
        // 
        this->advRingerName->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
        this->advRingerName->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
        this->advRingerName->FormattingEnabled = true;
        this->advRingerName->Location = System::Drawing::Point(7, 20);
        this->advRingerName->Name = L"advRingerName";
        this->advRingerName->Size = System::Drawing::Size(410, 21);
        this->advRingerName->TabIndex = 1;
        this->toolTip1->SetToolTip(this->advRingerName, L"Choose ringer or enter text");
        // 
        // addAdvancedRinger
        // 
        this->addAdvancedRinger->Location = System::Drawing::Point(550, 18);
        this->addAdvancedRinger->Name = L"addAdvancedRinger";
        this->addAdvancedRinger->Size = System::Drawing::Size(75, 23);
        this->addAdvancedRinger->TabIndex = 3;
        this->addAdvancedRinger->Text = L"Add";
        this->addAdvancedRinger->UseVisualStyleBackColor = true;
        this->addAdvancedRinger->Click += gcnew System::EventHandler(this, &PealSearchUI::AddAdvancedRinger_Click);
        // 
        // ringerAdvSearchParams
        // 
        this->ringerAdvSearchParams->AutoScroll = true;
        this->ringerAdvSearchParams->Dock = System::Windows::Forms::DockStyle::Fill;
        this->ringerAdvSearchParams->FlowDirection = System::Windows::Forms::FlowDirection::TopDown;
        this->ringerAdvSearchParams->Location = System::Drawing::Point(3, 55);
        this->ringerAdvSearchParams->MaximumSize = System::Drawing::Size(900, 0);
        this->ringerAdvSearchParams->Name = L"ringerAdvSearchParams";
        this->ringerAdvSearchParams->Size = System::Drawing::Size(900, 118);
        this->ringerAdvSearchParams->TabIndex = 1;
        this->ringerAdvSearchParams->WrapContents = false;
        // 
        // displayTab
        // 
        displayTab->Controls->Add(fieldsGroup);
        displayTab->Location = System::Drawing::Point(4, 22);
        displayTab->Name = L"displayTab";
        displayTab->Padding = System::Windows::Forms::Padding(3);
        displayTab->Size = System::Drawing::Size(1129, 182);
        displayTab->TabIndex = 64;
        displayTab->Text = L"Display";
        displayTab->UseVisualStyleBackColor = true;
        // 
        // fieldsGroup
        // 
        fieldsGroup->AutoSize = true;
        fieldsGroup->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
        fieldsGroup->Controls->Add(this->showReferencesColumns);
        fieldsGroup->Controls->Add(this->showRingColumn);
        fieldsGroup->Controls->Add(this->showConductorColumn);
        fieldsGroup->Controls->Add(this->showBellRungColumn);
        fieldsGroup->Controls->Add(this->showComposerColumn);
        fieldsGroup->Controls->Add(this->showAssociationColumn);
        fieldsGroup->Location = System::Drawing::Point(6, 6);
        fieldsGroup->Name = L"fieldsGroup";
        fieldsGroup->Padding = System::Windows::Forms::Padding(3, 3, 3, 0);
        fieldsGroup->Size = System::Drawing::Size(119, 168);
        fieldsGroup->TabIndex = 0;
        fieldsGroup->TabStop = false;
        fieldsGroup->Text = L"Field display";
        // 
        // showReferencesColumns
        // 
        this->showReferencesColumns->AutoSize = true;
        this->showReferencesColumns->Checked = true;
        this->showReferencesColumns->CheckState = System::Windows::Forms::CheckState::Checked;
        this->showReferencesColumns->Location = System::Drawing::Point(6, 135);
        this->showReferencesColumns->Name = L"showReferencesColumns";
        this->showReferencesColumns->Size = System::Drawing::Size(107, 17);
        this->showReferencesColumns->TabIndex = 5;
        this->showReferencesColumns->Text = L"Web References";
        this->showReferencesColumns->UseVisualStyleBackColor = true;
        this->showReferencesColumns->CheckedChanged += gcnew System::EventHandler(this, &PealSearchUI::showReferencesColumns_CheckedChanged);
        // 
        // showRingColumn
        // 
        this->showRingColumn->AutoSize = true;
        this->showRingColumn->Checked = true;
        this->showRingColumn->CheckState = System::Windows::Forms::CheckState::Checked;
        this->showRingColumn->Location = System::Drawing::Point(6, 43);
        this->showRingColumn->Name = L"showRingColumn";
        this->showRingColumn->Size = System::Drawing::Size(48, 17);
        this->showRingColumn->TabIndex = 4;
        this->showRingColumn->Text = L"Ring";
        this->showRingColumn->UseVisualStyleBackColor = true;
        this->showRingColumn->CheckStateChanged += gcnew System::EventHandler(this, &PealSearchUI::showRingColumn_CheckedChanged);
        // 
        // showConductorColumn
        // 
        this->showConductorColumn->AutoSize = true;
        this->showConductorColumn->Checked = true;
        this->showConductorColumn->CheckState = System::Windows::Forms::CheckState::Checked;
        this->showConductorColumn->Location = System::Drawing::Point(6, 66);
        this->showConductorColumn->Name = L"showConductorColumn";
        this->showConductorColumn->Size = System::Drawing::Size(75, 17);
        this->showConductorColumn->TabIndex = 3;
        this->showConductorColumn->Text = L"Conductor";
        this->showConductorColumn->UseVisualStyleBackColor = true;
        this->showConductorColumn->CheckStateChanged += gcnew System::EventHandler(this, &PealSearchUI::showConductorColumn_CheckedChanged);
        // 
        // showBellRungColumn
        // 
        this->showBellRungColumn->AutoSize = true;
        this->showBellRungColumn->Checked = true;
        this->showBellRungColumn->CheckState = System::Windows::Forms::CheckState::Checked;
        this->showBellRungColumn->Location = System::Drawing::Point(6, 112);
        this->showBellRungColumn->Name = L"showBellRungColumn";
        this->showBellRungColumn->Size = System::Drawing::Size(67, 17);
        this->showBellRungColumn->TabIndex = 2;
        this->showBellRungColumn->Text = L"Bell rung";
        this->showBellRungColumn->UseVisualStyleBackColor = true;
        this->showBellRungColumn->CheckedChanged += gcnew System::EventHandler(this, &PealSearchUI::showBellRungColumn_CheckedChanged);
        // 
        // showComposerColumn
        // 
        this->showComposerColumn->AutoSize = true;
        this->showComposerColumn->Checked = true;
        this->showComposerColumn->CheckState = System::Windows::Forms::CheckState::Checked;
        this->showComposerColumn->Location = System::Drawing::Point(6, 89);
        this->showComposerColumn->Name = L"showComposerColumn";
        this->showComposerColumn->Size = System::Drawing::Size(73, 17);
        this->showComposerColumn->TabIndex = 1;
        this->showComposerColumn->Text = L"Composer";
        this->showComposerColumn->UseVisualStyleBackColor = true;
        this->showComposerColumn->CheckedChanged += gcnew System::EventHandler(this, &PealSearchUI::showComposerColumn_CheckedChanged);
        // 
        // showAssociationColumn
        // 
        this->showAssociationColumn->AutoSize = true;
        this->showAssociationColumn->Checked = true;
        this->showAssociationColumn->CheckState = System::Windows::Forms::CheckState::Checked;
        this->showAssociationColumn->Location = System::Drawing::Point(6, 20);
        this->showAssociationColumn->Name = L"showAssociationColumn";
        this->showAssociationColumn->Size = System::Drawing::Size(80, 17);
        this->showAssociationColumn->TabIndex = 0;
        this->showAssociationColumn->Text = L"Association";
        this->showAssociationColumn->UseVisualStyleBackColor = true;
        this->showAssociationColumn->CheckedChanged += gcnew System::EventHandler(this, &PealSearchUI::showAssociationColumn_CheckedChanged);
        // 
        // dataGridView
        // 
        this->dataGridView->AllowUserToAddRows = false;
        this->dataGridView->AllowUserToDeleteRows = false;
        this->dataGridView->AllowUserToResizeColumns = false;
        this->dataGridView->AllowUserToResizeRows = false;
        this->dataGridView->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::DisplayedCells;
        this->dataGridView->AutoSizeRowsMode = System::Windows::Forms::DataGridViewAutoSizeRowsMode::DisplayedCells;
        this->dataGridView->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
        this->dataGridView->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(15) {
            this->rowCountColumn,
                this->pealIdColumn, this->dateColumn, this->associationColumn, this->changesColumn, this->methodColumn, this->towerColumn, this->ringColumn,
                this->timeColumn, this->conductorColumn, this->composerColumn, this->ringingWorldColumn, this->bellboardColumn, this->bellRungColumn,
                this->errorColumn
        });
        this->dataGridView->ContextMenuStrip = pealDataMenu;
        this->dataGridView->Dock = System::Windows::Forms::DockStyle::Fill;
        this->dataGridView->Location = System::Drawing::Point(0, 0);
        this->dataGridView->Name = L"dataGridView";
        this->dataGridView->ReadOnly = true;
        this->dataGridView->RowHeadersVisible = false;
        this->dataGridView->RowHeadersWidth = 35;
        this->dataGridView->Size = System::Drawing::Size(1137, 320);
        this->dataGridView->TabIndex = 81;
        this->dataGridView->VirtualMode = true;
        this->dataGridView->CellDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &PealSearchUI::dataGridView_CellDoubleClick);
        this->dataGridView->CellValueNeeded += gcnew System::Windows::Forms::DataGridViewCellValueEventHandler(this, &PealSearchUI::dataGridView_CellValueNeeded);
        // 
        // rowCountColumn
        // 
        dataGridViewCellStyle1->BackColor = System::Drawing::SystemColors::MenuBar;
        this->rowCountColumn->DefaultCellStyle = dataGridViewCellStyle1;
        this->rowCountColumn->HeaderText = L"#";
        this->rowCountColumn->MinimumWidth = 25;
        this->rowCountColumn->Name = L"rowCountColumn";
        this->rowCountColumn->ReadOnly = true;
        this->rowCountColumn->Width = 39;
        // 
        // pealIdColumn
        // 
        this->pealIdColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
        dataGridViewCellStyle2->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
        this->pealIdColumn->DefaultCellStyle = dataGridViewCellStyle2;
        this->pealIdColumn->HeaderText = L"Id";
        this->pealIdColumn->MinimumWidth = 50;
        this->pealIdColumn->Name = L"pealIdColumn";
        this->pealIdColumn->ReadOnly = true;
        this->pealIdColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->pealIdColumn->Width = 50;
        // 
        // dateColumn
        // 
        this->dateColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
        dataGridViewCellStyle3->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
        this->dateColumn->DefaultCellStyle = dataGridViewCellStyle3;
        this->dateColumn->HeaderText = L"Date";
        this->dateColumn->MinimumWidth = 25;
        this->dateColumn->Name = L"dateColumn";
        this->dateColumn->ReadOnly = true;
        this->dateColumn->Resizable = System::Windows::Forms::DataGridViewTriState::False;
        this->dateColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->dateColumn->Width = 75;
        // 
        // associationColumn
        // 
        this->associationColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
        this->associationColumn->HeaderText = L"Association";
        this->associationColumn->Name = L"associationColumn";
        this->associationColumn->ReadOnly = true;
        this->associationColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->associationColumn->Width = 67;
        // 
        // changesColumn
        // 
        this->changesColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
        dataGridViewCellStyle4->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
        this->changesColumn->DefaultCellStyle = dataGridViewCellStyle4;
        this->changesColumn->HeaderText = L"Changes";
        this->changesColumn->MinimumWidth = 25;
        this->changesColumn->Name = L"changesColumn";
        this->changesColumn->ReadOnly = true;
        this->changesColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->changesColumn->Width = 55;
        // 
        // methodColumn
        // 
        this->methodColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCellsExceptHeader;
        this->methodColumn->HeaderText = L"Method";
        this->methodColumn->MinimumWidth = 25;
        this->methodColumn->Name = L"methodColumn";
        this->methodColumn->ReadOnly = true;
        this->methodColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->methodColumn->Width = 25;
        // 
        // towerColumn
        // 
        this->towerColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCellsExceptHeader;
        this->towerColumn->HeaderText = L"Tower";
        this->towerColumn->MinimumWidth = 25;
        this->towerColumn->Name = L"towerColumn";
        this->towerColumn->ReadOnly = true;
        this->towerColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->towerColumn->Width = 25;
        // 
        // ringColumn
        // 
        this->ringColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
        this->ringColumn->HeaderText = L"Ring";
        this->ringColumn->MinimumWidth = 25;
        this->ringColumn->Name = L"ringColumn";
        this->ringColumn->ReadOnly = true;
        this->ringColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->ringColumn->Width = 35;
        // 
        // timeColumn
        // 
        this->timeColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
        this->timeColumn->HeaderText = L"Time";
        this->timeColumn->MinimumWidth = 25;
        this->timeColumn->Name = L"timeColumn";
        this->timeColumn->ReadOnly = true;
        this->timeColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->timeColumn->Width = 36;
        // 
        // conductorColumn
        // 
        this->conductorColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCellsExceptHeader;
        this->conductorColumn->HeaderText = L"Conductor";
        this->conductorColumn->MinimumWidth = 25;
        this->conductorColumn->Name = L"conductorColumn";
        this->conductorColumn->ReadOnly = true;
        this->conductorColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->conductorColumn->Width = 25;
        // 
        // composerColumn
        // 
        this->composerColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCellsExceptHeader;
        this->composerColumn->HeaderText = L"Composer";
        this->composerColumn->MinimumWidth = 45;
        this->composerColumn->Name = L"composerColumn";
        this->composerColumn->ReadOnly = true;
        this->composerColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->composerColumn->Width = 45;
        // 
        // ringingWorldColumn
        // 
        this->ringingWorldColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
        this->ringingWorldColumn->HeaderText = L"RW";
        this->ringingWorldColumn->Name = L"ringingWorldColumn";
        this->ringingWorldColumn->ReadOnly = true;
        this->ringingWorldColumn->Width = 60;
        // 
        // bellboardColumn
        // 
        this->bellboardColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
        this->bellboardColumn->HeaderText = L"Bellboard";
        this->bellboardColumn->Name = L"bellboardColumn";
        this->bellboardColumn->ReadOnly = true;
        this->bellboardColumn->Width = 60;
        // 
        // bellRungColumn
        // 
        this->bellRungColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
        this->bellRungColumn->HeaderText = L"Bell rung";
        this->bellRungColumn->MinimumWidth = 25;
        this->bellRungColumn->Name = L"bellRungColumn";
        this->bellRungColumn->ReadOnly = true;
        this->bellRungColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->bellRungColumn->Width = 54;
        // 
        // errorColumn
        // 
        this->errorColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
        this->errorColumn->HeaderText = L"Error";
        this->errorColumn->Name = L"errorColumn";
        this->errorColumn->ReadOnly = true;
        this->errorColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
        this->errorColumn->Visible = false;
        // 
        // pealDataMenu
        // 
        pealDataMenu->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { copyPealDataCmd });
        pealDataMenu->Name = L"pealDataMenu";
        pealDataMenu->Size = System::Drawing::Size(103, 26);
        // 
        // copyPealDataCmd
        // 
        copyPealDataCmd->Name = L"copyPealDataCmd";
        copyPealDataCmd->Size = System::Drawing::Size(102, 22);
        copyPealDataCmd->Text = L"Copy";
        copyPealDataCmd->Click += gcnew System::EventHandler(this, &PealSearchUI::copyPealDataCmd_Click);
        // 
        // buttonPanel
        // 
        this->buttonPanel->Controls->Add(this->resultCount);
        this->buttonPanel->Controls->Add(this->printBtn);
        this->buttonPanel->Controls->Add(this->statsBtn);
        this->buttonPanel->Controls->Add(this->clearbtn);
        this->buttonPanel->Controls->Add(this->progressBar);
        this->buttonPanel->Controls->Add(closeBtn);
        this->buttonPanel->Controls->Add(this->searchBtn);
        this->buttonPanel->Controls->Add(this->resultLbl);
        this->buttonPanel->Controls->Add(this->updateBtn);
        this->buttonPanel->Controls->Add(this->saveAsBtn);
        this->buttonPanel->Dock = System::Windows::Forms::DockStyle::Fill;
        this->buttonPanel->Location = System::Drawing::Point(3, 541);
        this->buttonPanel->Name = L"buttonPanel";
        this->buttonPanel->Size = System::Drawing::Size(1137, 24);
        this->buttonPanel->TabIndex = 82;
        // 
        // resultCount
        // 
        this->resultCount->AutoSize = true;
        this->resultCount->Location = System::Drawing::Point(148, 5);
        this->resultCount->Name = L"resultCount";
        this->resultCount->Size = System::Drawing::Size(13, 13);
        this->resultCount->TabIndex = 92;
        this->resultCount->Text = L"0";
        // 
        // printBtn
        // 
        this->printBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
        this->printBtn->Location = System::Drawing::Point(834, 1);
        this->printBtn->Name = L"printBtn";
        this->printBtn->Size = System::Drawing::Size(75, 23);
        this->printBtn->TabIndex = 87;
        this->printBtn->Text = L"Print";
        this->printBtn->UseVisualStyleBackColor = true;
        this->printBtn->Click += gcnew System::EventHandler(this, &PealSearchUI::printBtn_Click);
        // 
        // statsBtn
        // 
        this->statsBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
        this->statsBtn->Location = System::Drawing::Point(759, 1);
        this->statsBtn->Name = L"statsBtn";
        this->statsBtn->Size = System::Drawing::Size(75, 23);
        this->statsBtn->TabIndex = 86;
        this->statsBtn->Text = L"Statistics";
        this->statsBtn->UseVisualStyleBackColor = true;
        this->statsBtn->Click += gcnew System::EventHandler(this, &PealSearchUI::statsBtn_Click);
        // 
        // clearbtn
        // 
        this->clearbtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
        this->clearbtn->Location = System::Drawing::Point(678, 1);
        this->clearbtn->Name = L"clearbtn";
        this->clearbtn->Size = System::Drawing::Size(75, 23);
        this->clearbtn->TabIndex = 85;
        this->clearbtn->Text = L"Clear";
        this->clearbtn->UseVisualStyleBackColor = true;
        this->clearbtn->Click += gcnew System::EventHandler(this, &PealSearchUI::clearbtn_Click);
        // 
        // progressBar
        // 
        this->progressBar->Location = System::Drawing::Point(3, 2);
        this->progressBar->Name = L"progressBar";
        this->progressBar->Size = System::Drawing::Size(88, 18);
        this->progressBar->Step = 1;
        this->progressBar->TabIndex = 82;
        // 
        // searchBtn
        // 
        this->searchBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
        this->searchBtn->Location = System::Drawing::Point(989, 1);
        this->searchBtn->Name = L"searchBtn";
        this->searchBtn->Size = System::Drawing::Size(74, 23);
        this->searchBtn->TabIndex = 89;
        this->searchBtn->Text = L"Search";
        this->searchBtn->UseVisualStyleBackColor = true;
        this->searchBtn->Click += gcnew System::EventHandler(this, &PealSearchUI::searchBtn_Click);
        // 
        // resultLbl
        // 
        this->resultLbl->AutoSize = true;
        this->resultLbl->Location = System::Drawing::Point(97, 5);
        this->resultLbl->Name = L"resultLbl";
        this->resultLbl->Size = System::Drawing::Size(45, 13);
        this->resultLbl->TabIndex = 91;
        this->resultLbl->Text = L"Results:";
        // 
        // updateBtn
        // 
        this->updateBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
        this->updateBtn->Location = System::Drawing::Point(598, 1);
        this->updateBtn->Name = L"updateBtn";
        this->updateBtn->Size = System::Drawing::Size(74, 23);
        this->updateBtn->TabIndex = 84;
        this->updateBtn->Text = L"Update";
        this->toolTip1->SetToolTip(this->updateBtn, L"Make changes to the peals found.");
        this->updateBtn->UseVisualStyleBackColor = true;
        this->updateBtn->Click += gcnew System::EventHandler(this, &PealSearchUI::updateBtn_Click);
        // 
        // saveAsBtn
        // 
        this->saveAsBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
        this->saveAsBtn->Location = System::Drawing::Point(909, 1);
        this->saveAsBtn->Name = L"saveAsBtn";
        this->saveAsBtn->Size = System::Drawing::Size(74, 23);
        this->saveAsBtn->TabIndex = 88;
        this->saveAsBtn->Text = L"Save as";
        this->toolTip1->SetToolTip(this->saveAsBtn, L"Save peals to a different file.");
        this->saveAsBtn->UseVisualStyleBackColor = true;
        this->saveAsBtn->Click += gcnew System::EventHandler(this, &PealSearchUI::saveAsBtn_Click);
        // 
        // searchThread
        // 
        this->searchThread->WorkerReportsProgress = true;
        this->searchThread->WorkerSupportsCancellation = true;
        this->searchThread->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &PealSearchUI::searchThread_DoWork);
        this->searchThread->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &PealSearchUI::searchThread_ProgressChanged);
        this->searchThread->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &PealSearchUI::searchThread_RunWorkerCompleted);
        // 
        // simulatedSound
        // 
        this->simulatedSound->AutoSize = true;
        this->simulatedSound->Location = System::Drawing::Point(7, 95);
        this->simulatedSound->Name = L"simulatedSound";
        this->simulatedSound->Size = System::Drawing::Size(104, 17);
        this->simulatedSound->TabIndex = 88;
        this->simulatedSound->Text = L"Simulated sound";
        this->simulatedSound->UseVisualStyleBackColor = true;
        // 
        // PealSearchUI
        // 
        this->AcceptButton = this->searchBtn;
        this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
        this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
        this->CancelButton = closeBtn;
        this->ClientSize = System::Drawing::Size(1143, 568);
        this->Controls->Add(tableLayoutPanel1);
        this->MinimumSize = System::Drawing::Size(850, 470);
        this->Name = L"PealSearchUI";
        this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Show;
        this->Text = L"Search performances";
        this->Closing += gcnew System::ComponentModel::CancelEventHandler(this, &PealSearchUI::ClosingEvent);
        this->Load += gcnew System::EventHandler(this, &PealSearchUI::PealSearchUI_Load);
        tableLayoutPanel1->ResumeLayout(false);
        splitContainer1->Panel1->ResumeLayout(false);
        splitContainer1->Panel2->ResumeLayout(false);
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(splitContainer1))->EndInit();
        splitContainer1->ResumeLayout(false);
        tabbedControl->ResumeLayout(false);
        this->detailsPage->ResumeLayout(false);
        picturesGroup->ResumeLayout(false);
        picturesGroup->PerformLayout();
        this->footnoteGroup->ResumeLayout(false);
        this->footnoteGroup->PerformLayout();
        this->associationGroup->ResumeLayout(false);
        this->associationGroup->PerformLayout();
        this->pealDateGroup->ResumeLayout(false);
        this->pealDateGroup->PerformLayout();
        this->changesGroup->ResumeLayout(false);
        this->changesGroup->PerformLayout();
        this->timeGroup->ResumeLayout(false);
        this->timeGroup->PerformLayout();
        this->towerTab->ResumeLayout(false);
        this->towerTypeGroup->ResumeLayout(false);
        this->towerTypeGroup->PerformLayout();
        tenorGroup->ResumeLayout(false);
        tenorGroup->PerformLayout();
        this->towerGroup->ResumeLayout(false);
        this->towerGroup->PerformLayout();
        this->bellsGroup->ResumeLayout(false);
        this->bellsGroup->PerformLayout();
        this->methodTab->ResumeLayout(false);
        this->methodGroup->ResumeLayout(false);
        this->methodGroup->PerformLayout();
        this->methodSeriesGroup->ResumeLayout(false);
        this->methodSeriesGroup->PerformLayout();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->noOfMethodsInSeries))->EndInit();
        this->compositionGroup->ResumeLayout(false);
        this->ringersPage->ResumeLayout(false);
        ringersPageLayoutPanel->ResumeLayout(false);
        this->conductedGroup->ResumeLayout(false);
        this->conductedGroup->PerformLayout();
        this->ringerGroup->ResumeLayout(false);
        tableLayoutPanel2->ResumeLayout(false);
        tableLayoutPanel2->PerformLayout();
        this->composerGroup->ResumeLayout(false);
        this->composerGroup->PerformLayout();
        settingsGroup->ResumeLayout(false);
        settingsGroup->PerformLayout();
        strapperGroup->ResumeLayout(false);
        strapperGroup->PerformLayout();
        this->referencesPage->ResumeLayout(false);
        withdrawnBox->ResumeLayout(false);
        withdrawnBox->PerformLayout();
        errorsGroupBox->ResumeLayout(false);
        errorsGroupBox->PerformLayout();
        pealFeesGroup->ResumeLayout(false);
        pealFeesGroup->PerformLayout();
        this->notesGroup->ResumeLayout(false);
        this->notesGroup->PerformLayout();
        this->rwGroup->ResumeLayout(false);
        this->rwGroup->PerformLayout();
        this->bellBoardGroup->ResumeLayout(false);
        this->bellBoardGroup->PerformLayout();
        this->advancedRingersTab->ResumeLayout(false);
        advancedRingerGrp->ResumeLayout(false);
        newRinger->ResumeLayout(false);
        displayTab->ResumeLayout(false);
        displayTab->PerformLayout();
        fieldsGroup->ResumeLayout(false);
        fieldsGroup->PerformLayout();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->EndInit();
        pealDataMenu->ResumeLayout(false);
        this->buttonPanel->ResumeLayout(false);
        this->buttonPanel->PerformLayout();
        this->ResumeLayout(false);

    }
};
}
