#pragma once

namespace DucoUI
{
	public ref class TableSorterByCount :  public System::Collections::IComparer
	{
	public:
		TableSorterByCount(bool newAscending, int newColumnId);
        virtual int Compare(System::Object^, System::Object^);

	protected:
		~TableSorterByCount();
        int ConvertValueToInt(System::Windows::Forms::DataGridViewRow^ row, int cellNumber);

    private:
        bool ascending;
        int columnId;
	};
}
