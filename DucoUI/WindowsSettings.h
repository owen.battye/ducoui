#pragma once
#include <string>

namespace DucoUI
{
public ref class WindowsSettings
    {
    public:
        WindowsSettings(System::Windows::Forms::IWin32Window^ newOwner);
        virtual ~WindowsSettings();
        !WindowsSettings();

        System::Void SaveSettings();

        System::Void GetHistory(System::Windows::Forms::ToolStripMenuItem^  fileHistoryMenu,
                                  System::Windows::Forms::ToolStripMenuItem^  fileHistory1,
                                  System::Windows::Forms::ToolStripMenuItem^  fileHistory2,
                                  System::Windows::Forms::ToolStripMenuItem^  fileHistory3,
                                  System::Windows::Forms::ToolStripMenuItem^  fileHistory4,
                                  System::Windows::Forms::ToolStripMenuItem^  fileHistory5);

        System::Void AddToHistory(System::String^ newFileName,
                              System::Windows::Forms::ToolStripMenuItem^  fileHistoryMenu,
                              System::Windows::Forms::ToolStripMenuItem^  fileHistory1,
                              System::Windows::Forms::ToolStripMenuItem^  fileHistory2,
                              System::Windows::Forms::ToolStripMenuItem^  fileHistory3,
                              System::Windows::Forms::ToolStripMenuItem^  fileHistory4,
                              System::Windows::Forms::ToolStripMenuItem^  fileHistory5);
        System::String^ HistoryItem(unsigned int index);
        System::Boolean RemoveHistoryItem(unsigned int index);
        System::Boolean AutoRenumberDisabled();

        const std::string& InstallationDir();
        static std::string ConfigDir();

    protected:
        System::Boolean LoadSettings();
        System::Boolean SetValue(Microsoft::Win32::RegistryKey^ software, System::String^ settingName, System::String^ settingValue);

        System::Boolean SetHistoryMenuCommand(System::Windows::Forms::ToolStripMenuItem^ fileHistoryItem, System::String^ historyFileName);
        System::Boolean MoveHistoryItem(System::String^ newFileName, System::String^% currentHistoryItem, System::String^% nextHistoryItem);
        System::Void ReadHistorySetting(Microsoft::Win32::RegistryKey^ regKey, System::String^ settingName, System::String^% settingValue);
        System::Boolean ReadSetting(Microsoft::Win32::RegistryKey^ regKey, System::String^ settingName, System::String^% settingValue);
        System::Void ReadSetting(Microsoft::Win32::RegistryKey^ regKey, System::String^ settingName, System::String^% settingValue, System::String^ defaultValue);

    private:
        System::Windows::Forms::IWin32Window^ owner;

        System::String^     historyFile1;
        System::String^     historyFile2;
        System::String^     historyFile3;
        System::String^     historyFile4;
        System::String^     historyFile5;
        std::string*        installationDir;
        System::Boolean     autoRenumberDisabled;
    };

}
