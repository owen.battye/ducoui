#include "DatabaseManager.h"

#include "SelectMethodUI.h"

#include "SystemDefaultIcon.h"
#include "DatabaseGenUtils.h"
#include <ObjectId.h>

using namespace DucoUI;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Windows::Forms;

SelectMethodUI::SelectMethodUI(DucoUI::DatabaseManager^ theDatabase, Duco::ObjectId& newSelectedMethodId, const Duco::MethodSeries& methSeries)
: database(theDatabase), selectedMethodId(newSelectedMethodId), currentMethodSeries(methSeries)
{
    allMethodNames = gcnew System::Collections::Generic::List<System::Int16>();
    InitializeComponent();
}

System::Void
SelectMethodUI::SelectMethodUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    DatabaseGenUtils::GenerateMethodOptions(allMethodNames, methodSelector, database, -1, currentMethodSeries, true);
}

SelectMethodUI::~SelectMethodUI()
{
    if (components)
    {
        delete components;
    }
}

System::Void
SelectMethodUI::cancelBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    selectedMethodId = -1;
    Close();
}

System::Void
SelectMethodUI::acceptBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void
SelectMethodUI::methodSelector_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    selectedMethodId = DatabaseGenUtils::FindId(allMethodNames, methodSelector->SelectedIndex);
}

#pragma region Windows Form Designer generated code
void SelectMethodUI::InitializeComponent(void)
{
    System::Windows::Forms::Label^  methodLbl;
    System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
    this->cancelBtn = (gcnew System::Windows::Forms::Button());
    this->acceptBtn = (gcnew System::Windows::Forms::Button());
    this->methodSelector = (gcnew System::Windows::Forms::ComboBox());
    methodLbl = (gcnew System::Windows::Forms::Label());
    tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
    tableLayoutPanel1->SuspendLayout();
    this->SuspendLayout();
    // 
    // methodLbl
    // 
    methodLbl->AutoSize = true;
    methodLbl->Location = System::Drawing::Point(3, 7);
    methodLbl->Margin = System::Windows::Forms::Padding(3, 7, 3, 0);
    methodLbl->Name = L"methodLbl";
    methodLbl->Size = System::Drawing::Size(46, 13);
    methodLbl->TabIndex = 0;
    methodLbl->Text = L"Method:";
    // 
    // cancelBtn
    // 
    this->cancelBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
    this->cancelBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
    this->cancelBtn->Location = System::Drawing::Point(313, 30);
    this->cancelBtn->Name = L"cancelBtn";
    this->cancelBtn->Size = System::Drawing::Size(75, 23);
    this->cancelBtn->TabIndex = 3;
    this->cancelBtn->Text = L"Cancel";
    this->cancelBtn->UseVisualStyleBackColor = true;
    this->cancelBtn->Click += gcnew System::EventHandler(this, &SelectMethodUI::cancelBtn_Click);
    // 
    // acceptBtn
    // 
    this->acceptBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
    this->acceptBtn->Location = System::Drawing::Point(232, 30);
    this->acceptBtn->Name = L"acceptBtn";
    this->acceptBtn->Size = System::Drawing::Size(75, 23);
    this->acceptBtn->TabIndex = 2;
    this->acceptBtn->Text = L"Accept";
    this->acceptBtn->UseVisualStyleBackColor = true;
    this->acceptBtn->Click += gcnew System::EventHandler(this, &SelectMethodUI::acceptBtn_Click);
    // 
    // methodSelector
    // 
    this->methodSelector->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
    this->methodSelector->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
    tableLayoutPanel1->SetColumnSpan(this->methodSelector, 2);
    this->methodSelector->Dock = System::Windows::Forms::DockStyle::Fill;
    this->methodSelector->FormattingEnabled = true;
    this->methodSelector->Location = System::Drawing::Point(55, 3);
    this->methodSelector->Name = L"methodSelector";
    this->methodSelector->Size = System::Drawing::Size(333, 21);
    this->methodSelector->TabIndex = 1;
    this->methodSelector->SelectedIndexChanged += gcnew System::EventHandler(this, &SelectMethodUI::methodSelector_SelectedIndexChanged);
    // 
    // tableLayoutPanel1
    // 
    tableLayoutPanel1->ColumnCount = 3;
    tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
    tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
    tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
    tableLayoutPanel1->Controls->Add(this->cancelBtn, 2, 1);
    tableLayoutPanel1->Controls->Add(methodLbl, 0, 0);
    tableLayoutPanel1->Controls->Add(this->acceptBtn, 1, 1);
    tableLayoutPanel1->Controls->Add(this->methodSelector, 1, 0);
    tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
    tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
    tableLayoutPanel1->Name = L"tableLayoutPanel1";
    tableLayoutPanel1->RowCount = 2;
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
    tableLayoutPanel1->Size = System::Drawing::Size(391, 56);
    tableLayoutPanel1->TabIndex = 4;
    // 
    // SelectMethodUI
    // 
    this->AcceptButton = this->acceptBtn;
    this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
    this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
    this->AutoSize = true;
    this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
    this->CancelButton = this->cancelBtn;
    this->ClientSize = System::Drawing::Size(391, 56);
    this->Controls->Add(tableLayoutPanel1);
    this->MaximumSize = System::Drawing::Size(407, 95);
    this->MinimumSize = System::Drawing::Size(407, 95);
    this->Name = L"SelectMethodUI";
    this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
    this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
    this->Text = L"Select method";
    this->Load += gcnew System::EventHandler(this, &SelectMethodUI::SelectMethodUI_Load);
    tableLayoutPanel1->ResumeLayout(false);
    tableLayoutPanel1->PerformLayout();
    this->ResumeLayout(false);

}
#pragma endregion
