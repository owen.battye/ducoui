#pragma once

namespace DucoUI
{
    enum TSortType
    {
        EHeaderCell,
        ETime,
        ECount,
        EDate,
        EFloat,
        EDistance,
        EString
    };

	public ref class DucoTableSorter :  public System::Collections::IComparer
	{
	public:
		DucoTableSorter(bool headerCellAscending, TSortType newHeaderCellSortType);

        System::Collections::IComparer^ AddSortObject(TSortType objectType, bool ascending, int cellNumber);
        virtual int Compare(System::Object^ lhr, System::Object^ rhs);

	protected:
		~DucoTableSorter();
        System::Collections::IComparer^ CreateSortObject(TSortType sortType, bool ascending, int cellNumber);

    private:
        System::Collections::ArrayList^ sorters;
        System::Collections::IComparer^ headerCellSort;
	};
}
