#pragma once

namespace DucoUI
{

	public ref class BellboardUpdate : public System::Windows::Forms::Form, public DucoUI::ProgressCallbackUI
	{
	public:
        BellboardUpdate(DucoUI::DatabaseManager^ theDatabase);

        // from ProgressCallbackUI
        virtual void Initialised();
        virtual void Step(int progressPercent);
        virtual void Complete();

    protected:
        System::Void BellboardUpdate_Load(System::Object^ sender, System::EventArgs^ e);
        ~BellboardUpdate();
        !BellboardUpdate();

        System::Void closeBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void startBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void stopBtn_Click(System::Object^ sender, System::EventArgs^ e);


        System::Void backgroundWorker_DoWork(System::Object^ sender, System::ComponentModel::DoWorkEventArgs^ e);
        System::Void backgroundWorker_ProgressChanged(System::Object ^ sender, System::ComponentModel::ProgressChangedEventArgs ^ e);
        System::Void backgroundWorker_RunWorkerCompleted(System::Object ^ sender, System::ComponentModel::RunWorkerCompletedEventArgs ^ e);
    private: System::ComponentModel::IContainer^ components;
    protected:

	private:

        System::Windows::Forms::Button^     startBtn;
        System::Windows::Forms::Button^     stopBtn;
        System::Windows::Forms::Button^     closeBtn;
        System::Windows::Forms::ProgressBar^ progressBar;
        bool*                               cancellationFlag;
        DucoUI::DatabaseManager^            database;
        System::ComponentModel::BackgroundWorker^ backgroundWorker;
    private: System::Windows::Forms::Label^ resultLbl;
    private: System::Windows::Forms::CheckBox^ rwPagesOnly;
           DucoUI::ProgressCallbackWrapper* progressWrapper;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
            this->components = (gcnew System::ComponentModel::Container());
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
            System::Windows::Forms::ToolTip^ toolTip1;
            this->startBtn = (gcnew System::Windows::Forms::Button());
            this->progressBar = (gcnew System::Windows::Forms::ProgressBar());
            this->stopBtn = (gcnew System::Windows::Forms::Button());
            this->closeBtn = (gcnew System::Windows::Forms::Button());
            this->resultLbl = (gcnew System::Windows::Forms::Label());
            this->rwPagesOnly = (gcnew System::Windows::Forms::CheckBox());
            this->backgroundWorker = (gcnew System::ComponentModel::BackgroundWorker());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            toolTip1 = (gcnew System::Windows::Forms::ToolTip(this->components));
            tableLayoutPanel1->SuspendLayout();
            this->SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 4;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->Controls->Add(this->startBtn, 0, 2);
            tableLayoutPanel1->Controls->Add(this->progressBar, 0, 1);
            tableLayoutPanel1->Controls->Add(this->stopBtn, 1, 2);
            tableLayoutPanel1->Controls->Add(this->closeBtn, 3, 2);
            tableLayoutPanel1->Controls->Add(this->resultLbl, 2, 2);
            tableLayoutPanel1->Controls->Add(this->rwPagesOnly, 0, 0);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 3;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 30)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->Size = System::Drawing::Size(376, 85);
            tableLayoutPanel1->TabIndex = 0;
            // 
            // startBtn
            // 
            this->startBtn->Location = System::Drawing::Point(3, 56);
            this->startBtn->Name = L"startBtn";
            this->startBtn->Size = System::Drawing::Size(75, 23);
            this->startBtn->TabIndex = 1;
            this->startBtn->Text = L"Start";
            this->startBtn->UseVisualStyleBackColor = true;
            this->startBtn->Click += gcnew System::EventHandler(this, &BellboardUpdate::startBtn_Click);
            // 
            // progressBar
            // 
            tableLayoutPanel1->SetColumnSpan(this->progressBar, 4);
            this->progressBar->Dock = System::Windows::Forms::DockStyle::Fill;
            this->progressBar->Location = System::Drawing::Point(25, 33);
            this->progressBar->Margin = System::Windows::Forms::Padding(25, 10, 25, 10);
            this->progressBar->Name = L"progressBar";
            this->progressBar->Size = System::Drawing::Size(326, 10);
            this->progressBar->TabIndex = 2;
            // 
            // stopBtn
            // 
            this->stopBtn->Location = System::Drawing::Point(84, 56);
            this->stopBtn->Name = L"stopBtn";
            this->stopBtn->Size = System::Drawing::Size(75, 23);
            this->stopBtn->TabIndex = 3;
            this->stopBtn->Text = L"Stop";
            this->stopBtn->UseVisualStyleBackColor = true;
            this->stopBtn->Visible = false;
            this->stopBtn->Click += gcnew System::EventHandler(this, &BellboardUpdate::stopBtn_Click);
            // 
            // closeBtn
            // 
            this->closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->closeBtn->Location = System::Drawing::Point(298, 56);
            this->closeBtn->Name = L"closeBtn";
            this->closeBtn->Size = System::Drawing::Size(75, 23);
            this->closeBtn->TabIndex = 0;
            this->closeBtn->Text = L"Close";
            this->closeBtn->UseVisualStyleBackColor = true;
            this->closeBtn->Click += gcnew System::EventHandler(this, &BellboardUpdate::closeBtn_Click);
            // 
            // resultLbl
            // 
            this->resultLbl->AutoSize = true;
            this->resultLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            this->resultLbl->Location = System::Drawing::Point(165, 53);
            this->resultLbl->Name = L"resultLbl";
            this->resultLbl->Size = System::Drawing::Size(127, 32);
            this->resultLbl->TabIndex = 5;
            this->resultLbl->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
            // 
            // rwPagesOnly
            // 
            this->rwPagesOnly->AutoSize = true;
            tableLayoutPanel1->SetColumnSpan(this->rwPagesOnly, 4);
            this->rwPagesOnly->Location = System::Drawing::Point(3, 3);
            this->rwPagesOnly->Name = L"rwPagesOnly";
            this->rwPagesOnly->Size = System::Drawing::Size(165, 17);
            this->rwPagesOnly->TabIndex = 6;
            this->rwPagesOnly->Text = L"Peals missing RW pages only";
            toolTip1->SetToolTip(this->rwPagesOnly, L"If you have a big database, just updating RW pages rather than every performance "
                L"will be a lot faster because every performance doesnt have to be checked against"
                L" bell board.");
            this->rwPagesOnly->UseVisualStyleBackColor = true;
            // 
            // backgroundWorker
            // 
            this->backgroundWorker->WorkerReportsProgress = true;
            this->backgroundWorker->WorkerSupportsCancellation = true;
            this->backgroundWorker->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &BellboardUpdate::backgroundWorker_DoWork);
            this->backgroundWorker->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &BellboardUpdate::backgroundWorker_ProgressChanged);
            this->backgroundWorker->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &BellboardUpdate::backgroundWorker_RunWorkerCompleted);
            // 
            // BellboardUpdate
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(376, 85);
            this->Controls->Add(tableLayoutPanel1);
            this->Name = L"BellboardUpdate";
            this->Text = L"Update bellboard ids and Ringing World page nos.";
            this->Load += gcnew System::EventHandler(this, &BellboardUpdate::BellboardUpdate_Load);
            tableLayoutPanel1->ResumeLayout(false);
            tableLayoutPanel1->PerformLayout();
            this->ResumeLayout(false);

        }
#pragma endregion
};
}
