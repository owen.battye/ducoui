#pragma once
namespace DucoUI
{

    public ref class PealStatsUI : public System::Windows::Forms::Form
    {
    public:
        PealStatsUI(DucoUI::DatabaseManager^ theDatabase, const Duco::ObjectId& newPealId);

    protected:
        ~PealStatsUI();
        !PealStatsUI();

        System::Void PealStatsUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);

    private:
        System::ComponentModel::Container^  components;
        DucoUI::DatabaseManager^      database;
        Duco::Peal*                         currentPeal;
    
        System::Windows::Forms::TextBox^    changesPerMinute;
        System::Windows::Forms::TrackBar^   allPealsComparision;
        System::Windows::Forms::TrackBar^   towerComparision;
        System::Windows::Forms::Label^      allPealsSlowestLbl;
        System::Windows::Forms::Label^      allPealsFastestLbl;
        System::Windows::Forms::Label^      thisTowerSlowestLbl;
        System::Windows::Forms::Label^      thisTowerFastestLbl;
        System::Windows::Forms::TextBox^    totalPeals;


#pragma region Windows Form Designer generated code
    protected:
        void InitializeComponent()
        {
            System::Windows::Forms::Label^ label1;
            System::Windows::Forms::Label^ label2;
            System::Windows::Forms::Label^ label3;
            System::Windows::Forms::Button^ closeBtn;
            System::Windows::Forms::Label^ label5;
            System::Windows::Forms::GroupBox^ groupBox1;
            this->thisTowerFastestLbl = (gcnew System::Windows::Forms::Label());
            this->allPealsComparision = (gcnew System::Windows::Forms::TrackBar());
            this->thisTowerSlowestLbl = (gcnew System::Windows::Forms::Label());
            this->towerComparision = (gcnew System::Windows::Forms::TrackBar());
            this->allPealsFastestLbl = (gcnew System::Windows::Forms::Label());
            this->allPealsSlowestLbl = (gcnew System::Windows::Forms::Label());
            this->changesPerMinute = (gcnew System::Windows::Forms::TextBox());
            this->totalPeals = (gcnew System::Windows::Forms::TextBox());
            label1 = (gcnew System::Windows::Forms::Label());
            label2 = (gcnew System::Windows::Forms::Label());
            label3 = (gcnew System::Windows::Forms::Label());
            closeBtn = (gcnew System::Windows::Forms::Button());
            label5 = (gcnew System::Windows::Forms::Label());
            groupBox1 = (gcnew System::Windows::Forms::GroupBox());
            groupBox1->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->allPealsComparision))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->towerComparision))->BeginInit();
            this->SuspendLayout();
            // 
            // label1
            // 
            label1->AutoSize = true;
            label1->Location = System::Drawing::Point(9, 12);
            label1->Name = L"label1";
            label1->Size = System::Drawing::Size(101, 13);
            label1->TabIndex = 0;
            label1->Text = L"Changes per minute";
            // 
            // label2
            // 
            label2->AutoSize = true;
            label2->Location = System::Drawing::Point(8, 45);
            label2->Name = L"label2";
            label2->Size = System::Drawing::Size(108, 13);
            label2->TabIndex = 4;
            label2->Text = L"Compared to all peals";
            // 
            // label3
            // 
            label3->AutoSize = true;
            label3->Location = System::Drawing::Point(8, 86);
            label3->Name = L"label3";
            label3->Size = System::Drawing::Size(154, 13);
            label3->TabIndex = 9;
            label3->Text = L"Compared to peals in this tower";
            // 
            // closeBtn
            // 
            closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
            closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            closeBtn->Location = System::Drawing::Point(366, 177);
            closeBtn->Margin = System::Windows::Forms::Padding(2);
            closeBtn->Name = L"closeBtn";
            closeBtn->Size = System::Drawing::Size(75, 23);
            closeBtn->TabIndex = 13;
            closeBtn->Text = L"Close";
            closeBtn->UseVisualStyleBackColor = true;
            closeBtn->Click += gcnew System::EventHandler(this, &PealStatsUI::closeBtn_Click);
            // 
            // label5
            // 
            label5->AutoSize = true;
            label5->Location = System::Drawing::Point(244, 11);
            label5->Name = L"label5";
            label5->Size = System::Drawing::Size(143, 13);
            label5->TabIndex = 3;
            label5->Text = L"Number of peals in this tower";
            // 
            // groupBox1
            // 
            groupBox1->Controls->Add(label2);
            groupBox1->Controls->Add(this->thisTowerFastestLbl);
            groupBox1->Controls->Add(this->allPealsComparision);
            groupBox1->Controls->Add(this->thisTowerSlowestLbl);
            groupBox1->Controls->Add(this->towerComparision);
            groupBox1->Controls->Add(this->allPealsFastestLbl);
            groupBox1->Controls->Add(label3);
            groupBox1->Controls->Add(this->allPealsSlowestLbl);
            groupBox1->Location = System::Drawing::Point(8, 34);
            groupBox1->Name = L"groupBox1";
            groupBox1->Size = System::Drawing::Size(433, 136);
            groupBox1->TabIndex = 5;
            groupBox1->TabStop = false;
            groupBox1->Text = L"Peal speed comparison";
            // 
            // thisTowerFastestLbl
            // 
            this->thisTowerFastestLbl->AutoSize = true;
            this->thisTowerFastestLbl->Location = System::Drawing::Point(377, 118);
            this->thisTowerFastestLbl->Name = L"thisTowerFastestLbl";
            this->thisTowerFastestLbl->Size = System::Drawing::Size(34, 13);
            this->thisTowerFastestLbl->TabIndex = 12;
            this->thisTowerFastestLbl->Text = L"12.34";
            this->thisTowerFastestLbl->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
            // 
            // allPealsComparision
            // 
            this->allPealsComparision->CausesValidation = false;
            this->allPealsComparision->Enabled = false;
            this->allPealsComparision->Location = System::Drawing::Point(168, 45);
            this->allPealsComparision->Name = L"allPealsComparision";
            this->allPealsComparision->Size = System::Drawing::Size(244, 45);
            this->allPealsComparision->TabIndex = 6;
            this->allPealsComparision->TickFrequency = 0;
            this->allPealsComparision->TickStyle = System::Windows::Forms::TickStyle::TopLeft;
            // 
            // thisTowerSlowestLbl
            // 
            this->thisTowerSlowestLbl->AutoSize = true;
            this->thisTowerSlowestLbl->Location = System::Drawing::Point(165, 118);
            this->thisTowerSlowestLbl->Name = L"thisTowerSlowestLbl";
            this->thisTowerSlowestLbl->Size = System::Drawing::Size(34, 13);
            this->thisTowerSlowestLbl->TabIndex = 10;
            this->thisTowerSlowestLbl->Text = L"12.34";
            this->thisTowerSlowestLbl->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
            // 
            // towerComparision
            // 
            this->towerComparision->Enabled = false;
            this->towerComparision->Location = System::Drawing::Point(168, 86);
            this->towerComparision->Name = L"towerComparision";
            this->towerComparision->Size = System::Drawing::Size(244, 45);
            this->towerComparision->TabIndex = 10;
            this->towerComparision->TickFrequency = 0;
            // 
            // allPealsFastestLbl
            // 
            this->allPealsFastestLbl->AutoSize = true;
            this->allPealsFastestLbl->Location = System::Drawing::Point(378, 29);
            this->allPealsFastestLbl->Name = L"allPealsFastestLbl";
            this->allPealsFastestLbl->Size = System::Drawing::Size(34, 13);
            this->allPealsFastestLbl->TabIndex = 7;
            this->allPealsFastestLbl->Text = L"12.34";
            this->allPealsFastestLbl->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
            // 
            // allPealsSlowestLbl
            // 
            this->allPealsSlowestLbl->AutoSize = true;
            this->allPealsSlowestLbl->Location = System::Drawing::Point(165, 29);
            this->allPealsSlowestLbl->Name = L"allPealsSlowestLbl";
            this->allPealsSlowestLbl->Size = System::Drawing::Size(34, 13);
            this->allPealsSlowestLbl->TabIndex = 5;
            this->allPealsSlowestLbl->Text = L"12.34";
            this->allPealsSlowestLbl->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
            // 
            // changesPerMinute
            // 
            this->changesPerMinute->CausesValidation = false;
            this->changesPerMinute->Location = System::Drawing::Point(116, 8);
            this->changesPerMinute->Name = L"changesPerMinute";
            this->changesPerMinute->ReadOnly = true;
            this->changesPerMinute->Size = System::Drawing::Size(47, 20);
            this->changesPerMinute->TabIndex = 1;
            this->changesPerMinute->TabStop = false;
            this->changesPerMinute->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
            // 
            // totalPeals
            // 
            this->totalPeals->Location = System::Drawing::Point(393, 9);
            this->totalPeals->Name = L"totalPeals";
            this->totalPeals->ReadOnly = true;
            this->totalPeals->Size = System::Drawing::Size(47, 20);
            this->totalPeals->TabIndex = 4;
            this->totalPeals->TabStop = false;
            // 
            // PealStatsUI
            // 
            this->AcceptButton = closeBtn;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->AutoValidate = System::Windows::Forms::AutoValidate::Disable;
            this->CancelButton = closeBtn;
            this->ClientSize = System::Drawing::Size(452, 211);
            this->Controls->Add(groupBox1);
            this->Controls->Add(this->totalPeals);
            this->Controls->Add(label5);
            this->Controls->Add(closeBtn);
            this->Controls->Add(this->changesPerMinute);
            this->Controls->Add(label1);
            this->MaximumSize = System::Drawing::Size(468, 265);
            this->MinimumSize = System::Drawing::Size(400, 200);
            this->Name = L"PealStatsUI";
            this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
            this->Text = L"Peal speed";
            this->Load += gcnew System::EventHandler(this, &PealStatsUI::PealStatsUI_Load);
            groupBox1->ResumeLayout(false);
            groupBox1->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->allPealsComparision))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->towerComparision))->EndInit();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion

    };
}
