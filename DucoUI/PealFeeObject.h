#pragma once

#include "PealFeeObjectBase.h"

namespace Duco
{
    class Peal;
}

namespace DucoUI
{
    ref class DatabaseManager;

	public ref class PealFeeObject :  public DucoUI::PealFeeObjectBase
	{
	public:
        PealFeeObject(DucoUI::DatabaseManager^ theDatabase, unsigned int theBellNumber, Duco::Peal* const thePeal);
		virtual ~PealFeeObject();

        // from PealFeeObjectBase
        virtual bool SetPeal() override;

        // New functions
        void AddStrapper();

    private:
        void InitializeComponent();
        DucoUI::PealFeeObjectBase^           strapperCtrl;
	};

}
