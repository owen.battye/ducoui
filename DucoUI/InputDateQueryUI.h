#pragma once
namespace DucoUI
{
    public ref class InputDateQueryUI : public System::Windows::Forms::Form
    {
    public:
        InputDateQueryUI(DucoUI::DatabaseManager^ theDatabase, Duco::ObjectId& newId, System::DateTime^ currentDate);

    protected:
        ~InputDateQueryUI();
        void InitializeComponent();

        System::Void cancelBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void okBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void InputDateQueryUI_Load(System::Object^  sender, System::EventArgs^  e);

    private:
        System::Windows::Forms::DateTimePicker^  dateTimePicker1;
        System::ComponentModel::Container ^components;

        DucoUI::DatabaseManager^ database;
        Duco::ObjectId& id;
    };
}
