#pragma once

namespace DucoUI
{
	public ref class TableSorterByFloat :  public System::Collections::IComparer
	{
	public:
		TableSorterByFloat(bool newAscending, int newColumnId);

        virtual int Compare(System::Object^, System::Object^);
        void SetAccuracy(int newAccuracy);
        void SetSimpleString(bool newSimpleString);

	protected:
		~TableSorterByFloat();
        virtual float ConvertValueToFloat(System::Windows::Forms::DataGridViewRow^ row, int cellNumber);

        bool ascending;
        int columnId;
        int accuracy;
        bool simpleString;
	};
}
