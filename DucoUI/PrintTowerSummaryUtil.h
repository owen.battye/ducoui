#pragma once

#include "PrintUtilBase.h"

namespace Duco
{
    class ObjectId;
    class PealLengthInfo;
}

namespace DucoUI
{
    public ref class PrintTowerSummaryUtil : public PrintUtilBase
    {
    public:
        PrintTowerSummaryUtil(DucoUI::DatabaseManager^ theDatabase, System::Drawing::Printing::PrintPageEventArgs^ printArgs, const Duco::ObjectId& newTowerId);
        ~PrintTowerSummaryUtil();
        virtual System::Void printObject(System::Drawing::Printing::PrintPageEventArgs^ args, System::Drawing::Printing::PrinterSettings^ printSettings) override;

    protected:
        System::Void PrintPealTitle(System::String^ title, const Duco::ObjectId& pealId, bool includeSpeed, System::Drawing::Printing::PrintPageEventArgs^ printArgs);
        System::Void PrintRinger(const Duco::ObjectId& ringerId, const Duco::PealLengthInfo& noOfPeals, System::Drawing::Printing::PrintPageEventArgs^ printArgs);
        System::String^ PrintTower();
        size_t NumberOfBellsInTower();
        System::String^ TowerHighestOrder();
        System::String^ TowerHighestNumberOfBellsName();
        System::Void PrintMethod(const Duco::ObjectId& methodId, const Duco::PealLengthInfo& noOfPeals, System::Drawing::Printing::PrintPageEventArgs^ printArgs);
        System::Void PrintPeal(const Duco::ObjectId& pealId, System::String^ title, System::Drawing::Printing::PrintPageEventArgs^ printArgs);

        System::Drawing::Font^      smallBoldFont;
        Duco::ObjectId*             towerId;
    };
}