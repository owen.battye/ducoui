#pragma once

namespace DucoUI
{
    public ref class SingleObjectStatsUI : public System::Windows::Forms::Form, public DucoUI::RingingDatabaseObserver
    {
    public:
        SingleObjectStatsUI(DucoUI::DatabaseManager^ theDatabase, Duco::ObjectId theObjectId, Duco::TObjectType theObjectType);
        SingleObjectStatsUI(DucoUI::DatabaseManager^ theDatabase, System::Collections::Generic::List<System::UInt64>^ pealIds, Duco::TObjectType theObjectType);
        !SingleObjectStatsUI();

        //from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

        System::Void SingleObjectStatsUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void SingleObjectStatsUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e);

    protected:
        ~SingleObjectStatsUI();
        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void ringersCircledBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void methodsBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void summaryBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void printSummary(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args);

        System::Void longestTime_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
        System::Void longestChanges_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
        System::Void longestCPM_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
        System::Void shortestTime_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
        System::Void shortestChanges_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
        System::Void shortestCPM_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
        System::Void firstPeal_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
        System::Void lastPeal_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
        System::Void totalNumberOfPeals_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
        System::Void linkTowers_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void bandMetricsToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e);

        void InitializeComponent();
        System::Void GenerateStats();

    private:
        Duco::PealLengthInfo*               pealInfo;
        System::ComponentModel::Container^  components;
        DucoUI::DatabaseManager^            database;

        System::Windows::Forms::TextBox^    averageCPM;
        System::Windows::Forms::TextBox^    shortestCPM;
        System::Windows::Forms::TextBox^    longestCPM;
        System::Windows::Forms::TextBox^    totalChanges;
        System::Windows::Forms::TextBox^    averageChanges;
        System::Windows::Forms::TextBox^    shortestChanges;
        System::Windows::Forms::TextBox^    longestChanges;
        System::Windows::Forms::TextBox^    totalTime;
        System::Windows::Forms::TextBox^    averageTime;
        System::Windows::Forms::TextBox^    shortestTime;
        System::Windows::Forms::TextBox^    longestTime;
        System::Windows::Forms::TextBox^    firstPeal;
        System::Windows::Forms::TextBox^    lastPeal;
        System::Windows::Forms::TextBox^    totalNumberOfPeals;

        System::Windows::Forms::CheckBox^   linkTowers;
        System::Windows::Forms::CheckBox^   includeWithdrawn;

        Duco::TObjectType                                   objectType;
        Duco::ObjectId*                                     objectId;
        System::Collections::Generic::List<System::UInt64>^ objectIds;
        System::Collections::Generic::List<System::UInt64>^ pealIds;

    private: System::Windows::Forms::ToolStripMenuItem^ summaryBtn;
    private: System::Windows::Forms::ToolStripMenuItem^ methodsBtn;
    private: System::Windows::Forms::ToolStripMenuItem^ ringersCircledBtn;
    private: System::Windows::Forms::MenuStrip^ menuStrip1;
    };
}
