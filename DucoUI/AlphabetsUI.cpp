#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"
#include <DatabaseSearch.h>
#include "SearchUtils.h"
#include "PrintPealSearchUtil.h"
#include "ProgressWrapper.h"

#include "AlphabetsUI.h"

#include <AlphabetData.h>
#include <AlphabetLetter.h>
#include <AlphabetObject.h>
#include <StatisticFilters.h>
#include "DucoUtils.h"
#include <DucoEngineUtils.h>
#include <RingingDatabase.h>
#include <Method.h>
#include "DucoWindowState.h"
#include "OpenObjectCallback.h"
#include "PictureListControl.h"
#include "PictureControl.h"
#include "RingerList.h"
#include "PrintBandSummaryUtil.h"
#include <DownloadedPeal.h>
#include "PealUI.h"
#include "PealSearchUI.h"
#include "SystemDefaultIcon.h"
#include "FiltersUI.h"
#include "SoundUtils.h"
#include <PealDatabase.h>
#include "DucoTableSorter.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace Duco;
using namespace DucoUI;

namespace DucoUI
{
    ref class AlphabetsUIArgs
    {
    public:
        bool            uniqueMethods;
        bool            insideOnly;
        Duco::AlphabetData* alphabets;
    };
}

AlphabetsUI::AlphabetsUI(DucoUI::DatabaseManager^ theDatabase)
:   database(theDatabase), autoRestart(false)
{
    filters = new Duco::StatisticFilters(database->Database());
    filters->SetDefaultRinger();
    InitializeComponent();
}

AlphabetsUI::!AlphabetsUI()
{
    delete filters;
}

AlphabetsUI::~AlphabetsUI()
{
    this->!AlphabetsUI();
    if (components)
    {
        delete components;
    }
}
System::Void
AlphabetsUI::AlphabetsUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    StartGeneration();
}

System::Void
AlphabetsUI::filterBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    FiltersUI^ filtersDialog = gcnew FiltersUI(*filters, database);
    System::Windows::Forms::DialogResult returnVal = filtersDialog->ShowDialog(this);
    if (returnVal == System::Windows::Forms::DialogResult::OK)
    {
        StartGeneration();
    }
}

System::Void
AlphabetsUI::settingsUpdated(System::Object^  sender, System::EventArgs^  e)
{
    StartGeneration();
}

System::Void
AlphabetsUI::AlphabetsUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e)
{
    if (backgroundGenerator->IsBusy)
    {
        backgroundGenerator->CancelAsync();
        e->Cancel = true;
        SoundUtils::PlayErrorSound();
        autoRestart = false;
    }
}

System::Void
AlphabetsUI::StartGeneration()
{
    if (backgroundGenerator->IsBusy)
    {
        backgroundGenerator->CancelAsync();
        autoRestart = true;
        return;
    }

    uniqueMethods->Enabled = false;
    insideBellOnly->Enabled = false;
    dataView->Columns->Clear();
    dataView->Rows->Clear();

    AlphabetsUIArgs^ args = gcnew AlphabetsUIArgs();
    args->uniqueMethods = uniqueMethods->Checked;
    args->alphabets = NULL;
    args->insideOnly = insideBellOnly->Checked;
    backgroundGenerator->RunWorkerAsync(args);
}

System::Void
AlphabetsUI::backgroundGenerator_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    uniqueMethods->Enabled = true;
    insideBellOnly->Enabled = true;

    if (autoRestart)
    {
        autoRestart = false;
        StartGeneration();
    }
    else if (!e->Cancelled)
    {
        AlphabetsUIArgs^ args = static_cast<AlphabetsUIArgs^>(e->Result);
        System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle1 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        for (unsigned int alphabetNo (0); alphabetNo <= (args->alphabets->NumberOfCompletedAlphabets()+1); ++alphabetNo)
        {
            DataGridViewTextBoxColumn^ newColumn = gcnew DataGridViewTextBoxColumn();
            newColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            dataGridViewCellStyle1->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
            newColumn->DefaultCellStyle = dataGridViewCellStyle1;
            newColumn->HeaderText = DucoUtils::ConvertString(DucoEngineUtils::ToString(alphabetNo+1));
            newColumn->Name = newColumn->HeaderText;
            newColumn->ReadOnly = true;
            newColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            newColumn->Width = 55;
            dataView->Columns->Add(newColumn);
        }
        
        for (wchar_t i = 'a'; i <= 'z'; ++i)
        {
            const Duco::AlphabetLetter& nextLetter = (args->alphabets->operator[](i));

            // Add tower name, and peal totals here.
            DataGridViewRow^ newRow = gcnew DataGridViewRow();
            for (unsigned int alphabetNo (0); alphabetNo <= (args->alphabets->NumberOfCompletedAlphabets()+1); ++alphabetNo)
            {
                std::string methodChar;
                methodChar += toupper(i);
                newRow->HeaderCell->Value = DucoUtils::ConvertString(methodChar);
                DataGridViewCellCollection^ theCells = newRow->Cells;

                DataGridViewTextBoxCell^ methodCell = gcnew DataGridViewTextBoxCell();

                if (alphabetNo < nextLetter.Count())
                {
                    const Duco::AlphabetObject& letterNextPeal = nextLetter[alphabetNo];
                    methodCell->Tag = DucoUtils::ConvertString(letterNextPeal.PealId());
                    methodCell->ToolTipText = DucoUtils::ConvertString(letterNextPeal.Date().Str());
                    Method theMethod;
                    methodCell->Value = DucoUtils::ConvertString(letterNextPeal.MethodName(database->Database().MethodsDatabase()));
                }
                theCells->Add(methodCell);
            }
            dataView->Rows->Add(newRow);
        }

        // Completed dates row.
        System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle2 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        dataGridViewCellStyle2->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;

        DataGridViewRow^ completionDatesRow = gcnew DataGridViewRow();
        completionDatesRow->HeaderCell->Value = "Completed";
        DataGridViewCellCollection^ completionDatesRowCells = completionDatesRow->Cells;
        for (unsigned int alphabetNo(0); alphabetNo <= (args->alphabets->NumberOfCompletedAlphabets()+1); ++alphabetNo)
        {
            DataGridViewTextBoxCell^ dateCompletedCell = gcnew DataGridViewTextBoxCell();
            dateCompletedCell->Style = dataGridViewCellStyle2;

            Duco::PerformanceDate circledDate;
            std::set<ObjectId> pealIds;
            if (args->alphabets->Completed(alphabetNo, circledDate, pealIds))
            {
                dateCompletedCell->Value = DucoUtils::ConvertString(circledDate.Str());
            }
            String^ pealIdStr = "";
            std::set<ObjectId>::const_iterator it = pealIds.begin();
            while (it != pealIds.end())
            {
                if (pealIdStr->Length != 0)
                {
                    pealIdStr += ",";
                }
                pealIdStr += DucoUtils::ConvertString(it->Str());
                ++it;
            }
            dateCompletedCell->Tag = pealIdStr;
            completionDatesRowCells->Add(dateCompletedCell);
        }
        dataView->Rows->Add(completionDatesRow);
        delete args->alphabets;
    }
}

System::Void
AlphabetsUI::backgroundGenerator_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
    AlphabetsUIArgs^ args = static_cast<AlphabetsUIArgs^>(e->Argument);
    args->alphabets = new AlphabetData();
    database->Database().PealsDatabase().GetAlphabeticCompletions(*filters, args->uniqueMethods, args->insideOnly, *args->alphabets);

    e->Result = args;
}

System::Void
AlphabetsUI::dataView_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
    DataGridViewColumn^ newColumn = dataView->Columns[e->ColumnIndex];

    SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;
    if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
        newSortOrder = SortOrder::Ascending;
    else
        newSortOrder = SortOrder::Descending;

    for (int i(0); i < dataView->Columns->Count; ++i)
    {
        dataView->Columns[i]->HeaderCell->SortGlyphDirection = SortOrder::None;
    }

    newColumn->HeaderCell->SortGlyphDirection = newSortOrder;

    DucoTableSorter^ sorter = gcnew DucoTableSorter(false, EString);
    sorter->AddSortObject(EString, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
    sorter->AddSortObject(EHeaderCell, true, -1);
    dataView->Sort(sorter);
}

System::Void
AlphabetsUI::dataView_CellContentDoubleClick(System::Object^ sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    try
    {
        DataGridViewCell^ currentCell = dataView->CurrentCell;
        if (currentCell->Tag != nullptr)
        {
            if (e->RowIndex == 26)
            {
                String^ pealIdsStr = (String^)(currentCell->Tag);
                System::Collections::Generic::List<System::UInt64>^ objectIds = DucoUtils::ConvertCommaSeperatedIdStringToList(pealIdsStr);
                if (objectIds->Count > 0)
                {
                    PealSearchUI^ searchUI = gcnew PealSearchUI(database, false);
                    searchUI->Show(MdiParent, objectIds);
                }
            }
            else
            {
                unsigned int pealId = Convert::ToInt16(currentCell->Tag);
                PealUI::ShowPeal(database, this->MdiParent, pealId);
            }
        }
    }
    catch (Exception^)
    {
        SoundUtils::PlayErrorSound();
    }
}
