#include "MethodSeriesSearch.h"

#include "DucoUtils.h"
#include "DatabaseManager.h"
#include <DatabaseSearch.h>
#include "SystemDefaultIcon.h"
#include "DatabaseGenUtils.h"
#include "SoundUtils.h"
#include <RingingDatabase.h>
#include <DucoEngineUtils.h>
#include <DucoConfiguration.h>
#include "RingingDatabaseObserver.h"
#include "DucoWindowState.h"
#include "MethodSeriesUI.h"
#include <MethodSeries.h>
#include <Method.h>
#include "DucoUIUtils.h"

#include "SearchUtils.h"
#include <SearchFieldNumberArgument.h>
#include <SearchFieldBoolArgument.h>
#include <SearchFieldIdArgument.h>
#include <SearchValidObject.h>
#include "DucoTableSorter.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::Windows::Forms;

MethodSeriesSearch::MethodSeriesSearch(DucoUI::DatabaseManager^ theDatabase, bool setStartSearchForInvalid)
: database(theDatabase), startSearchForInvalid(setStartSearchForInvalid)
{
    InitializeComponent();
    foundSeriesIds = new std::set<Duco::ObjectId>;
    progressWrapper = new ProgressCallbackWrapper(this);
    allMethodIds = gcnew System::Collections::Generic::List<System::Int16>();
}

MethodSeriesSearch::!MethodSeriesSearch()
{
    delete foundSeriesIds;
    delete progressWrapper;
}

MethodSeriesSearch::~MethodSeriesSearch()
{
    this->!MethodSeriesSearch();
    if (components)
    {
        delete components;
    }
}

System::Void
MethodSeriesSearch::MethodSeriesSearch_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    DatabaseGenUtils::GenerateMethodOptions(allMethodIds, methodSelector, database, -1, -1, false, false, true);
    if (startSearchForInvalid)
    {
        containsError->Checked = true;
        searchBtn_Click(sender, e);
    }
}

System::Void
MethodSeriesSearch::ClosingEvent( Object^ /*sender*/, System::ComponentModel::CancelEventArgs^ e )
{
    if (backgroundWorker->IsBusy)
    {
        backgroundWorker->CancelAsync();
        e->Cancel = true;
    }
}

System::Void
MethodSeriesSearch::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

bool
MethodSeriesSearch::CreateSearchParameters(Duco::DatabaseSearch& search)
{
    try
    {
        unsigned int noOfBells (bellsEditor->Value);
        if (noOfBells > 0)
        {
            TSearchFieldNumberArgument* newNumber = new TSearchFieldNumberArgument(Duco::ENoOfBells, noOfBells, EEqualTo);
            search.AddArgument(newNumber);
        }
        unsigned int noOfMethods (noOfMethodsEditor->Value);
        if (noOfMethods > 0)
        {
            TSearchFieldNumberArgument* newNumber = new TSearchFieldNumberArgument(Duco::EMethodSeriesCount, noOfMethods, EEqualTo);
            search.AddArgument(newNumber);
        }
        SearchUtils::AddStringSearchArgument(search, nameEditor->Text, EMethodSeriesStr, false);
        if (methodSelector->SelectedIndex == -1)
        {
            SearchUtils::AddStringSearchArgument(search, methodSelector->Text, EFullMethodName, false);
        }
        else
        {
            Duco::ObjectId methodId = DatabaseGenUtils::FindId(allMethodIds, methodSelector->SelectedIndex);
            TSearchFieldIdArgument* newId = new TSearchFieldIdArgument(Duco::EMethodId, methodId);
            search.AddArgument(newId);
        }
        if (completed->Checked)
        {
            TSearchFieldBoolArgument* newArg = new TSearchFieldBoolArgument(Duco::EMethodSeriesCompleted, true);
            search.AddArgument(newArg);
        }
        if (containsError->Checked)
        {
            TSearchValidObject* newArg = new TSearchValidObject(database->Config().IncludeWarningsInValidation());
            search.AddValidationArgument(newArg);
        }
    }
    catch (Exception^)
    {
        return false;
    }
    return true;
}

System::Void
MethodSeriesSearch::searchBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (!backgroundWorker->IsBusy)
    {
        searchBtn->Enabled = false;
        foundSeriesIds->clear();
        nameColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
        bellsColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
        methodCountColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
        methodsStringColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
        errorColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
        dataGridView1->Rows->Clear();
        resultsEditor->Text = "";
        dataGridView1->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::EnableResizing;
        dataGridView1->ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode::EnableResizing;

        DatabaseSearch* search = new DatabaseSearch(database->Database());
        if (CreateSearchParameters(*search))
        {
            SearchUIArgs^ args = gcnew SearchUIArgs();
            args->search = search;
            backgroundWorker->RunWorkerAsync(args);
        }
        else
        {
            SoundUtils::PlayErrorSound();
            delete search;
            searchBtn->Enabled = true;
        }
    }
}

System::Void
MethodSeriesSearch::clearBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (!backgroundWorker->IsBusy)
    {
        resultsEditor->Text = "";
        progressBar1->Value = 0;
        dataGridView1->Rows->Clear();
        searchBtn->Enabled = true;
        completed->Checked = false;
        containsError->Checked = false;
        nameEditor->Text = "";
        methodSelector->Text = "";
        methodSelector->SelectedIndex = -1;
        noOfMethodsEditor->Value = 0;
        bellsEditor->Value = 0;
    }
}

System::Void
MethodSeriesSearch::dataGridView1_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    try
    {
        if (!backgroundWorker->IsBusy)
        {
            unsigned int rowIndex = e->RowIndex;
            DataGridViewRow^ currentRow = dataGridView1->Rows[rowIndex];
            unsigned int methodId = Convert::ToInt16(currentRow->HeaderCell->Value);
            MethodSeriesUI::ShowSeries(database, this->MdiParent, methodId);
        }
    }
    catch (Exception^)
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
MethodSeriesSearch::backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
    SearchUIArgs^ args = static_cast<SearchUIArgs^>(e->Argument);
    System::ComponentModel::BackgroundWorker^ bgworker = static_cast<System::ComponentModel::BackgroundWorker^>(sender);
    args->search->Search(*progressWrapper, *foundSeriesIds, TObjectType::EMethodSeries);
    PopulateResults(bgworker);
    delete args->search;
}

System::Void
MethodSeriesSearch::backgroundWorker1_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    progressBar1->Value = e->ProgressPercentage;
    if (e->UserState != nullptr)
    {
        DataGridViewRow^ newRow = static_cast<DataGridViewRow^>(e->UserState);
        dataGridView1->Rows->Add(newRow);
    }
}

System::Void
MethodSeriesSearch::backgroundWorker1_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    dataGridView1->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
    dataGridView1->ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode::AutoSize;
    searchBtn->Enabled = true;
    progressBar1->Value = 0;
    resultsEditor->Text = Convert::ToString(foundSeriesIds->size());
    if (foundSeriesIds->empty())
    {
        SoundUtils::PlayErrorSound();
    }
}

void
MethodSeriesSearch::Initialised()
{
    backgroundWorker->ReportProgress(0);
}

void
MethodSeriesSearch::Step(int progressPercent)
{
    backgroundWorker->ReportProgress(progressPercent/2);
}

void
MethodSeriesSearch::Complete()
{
}

System::Void
MethodSeriesSearch::PopulateResults(System::ComponentModel::BackgroundWorker^ bgworker)
{
    float total = float(foundSeriesIds->size());
    float count (0);
    std::set<Duco::ObjectId>::const_iterator it = foundSeriesIds->begin();
    while (it != foundSeriesIds->end() && !bgworker->CancellationPending)
    {
        Duco::MethodSeries theSeries;
        if (database->FindMethodSeries(*it, theSeries, false))
        {
            DataGridViewRow^ newRow = gcnew DataGridViewRow();
            newRow->HeaderCell->Value = DucoUtils::ConvertString(*it);
            DataGridViewCellCollection^ theCells = newRow->Cells;
            //id Column;
            DataGridViewTextBoxCell^ idCell = gcnew DataGridViewTextBoxCell();
            idCell->Value = DucoUtils::ConvertString(*it);
            theCells->Add(idCell);
            //Name Column;
            DataGridViewTextBoxCell^ nameCell = gcnew DataGridViewTextBoxCell();
            nameCell->Value = DucoUtils::ConvertString(theSeries.Name());
            theCells->Add(nameCell);
            //Bells Column;
            DataGridViewTextBoxCell^ typeCell = gcnew DataGridViewTextBoxCell();
            if (theSeries.NoOfBellsSet())
            {
                typeCell->Value = Convert::ToString(theSeries.NoOfBells());
            }
            theCells->Add(typeCell);
            //No Of Methods Column;
            DataGridViewTextBoxCell^ bellsCell = gcnew DataGridViewTextBoxCell();
            bellsCell->Value = Convert::ToString(theSeries.NoOfMethods());
            theCells->Add(bellsCell);
            // Methods column
            DataGridViewTextBoxCell^ placeNotationCell = gcnew DataGridViewTextBoxCell();
            placeNotationCell->Value = DucoUtils::ConvertString(theSeries.MethodNames(database->Database().MethodsDatabase()));
            theCells->Add(placeNotationCell);
            // Error column
            DataGridViewTextBoxCell^ errorStringCell = gcnew DataGridViewTextBoxCell();
            errorStringCell->Value = DucoUtils::ConvertString(theSeries.ErrorString(database->Database().Settings(), database->Config().IncludeWarningsInValidation()));
            theCells->Add(errorStringCell);

            ++count;
            backgroundWorker->ReportProgress(int(((count / total) * 50)+50), newRow);
        }
        ++it;
    }
}

System::Void
MethodSeriesSearch::dataGridView1_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
    DataGridViewColumn^ newColumn = dataGridView1->Columns[e->ColumnIndex];
    if (newColumn->SortMode != DataGridViewColumnSortMode::Programmatic)
    {
        dataGridView1->Columns[2]->HeaderCell->SortGlyphDirection = SortOrder::None;
        dataGridView1->Columns[3]->HeaderCell->SortGlyphDirection = SortOrder::None;
        return;
    }

    DataGridViewColumn^ oldColumn = dataGridView1->SortedColumn;

    SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;
    if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
        newSortOrder = SortOrder::Ascending;
    else
        newSortOrder = SortOrder::Descending;

    if (oldColumn != nullptr && oldColumn != newColumn)
        oldColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
    else
    {
        dataGridView1->Columns[2]->HeaderCell->SortGlyphDirection = SortOrder::None;
        dataGridView1->Columns[3]->HeaderCell->SortGlyphDirection = SortOrder::None;
    }

    newColumn->HeaderCell->SortGlyphDirection = newSortOrder;

    DucoTableSorter^ sorter = gcnew DucoTableSorter(newSortOrder == SortOrder::Ascending, EString);
    sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
    dataGridView1->Sort(sorter);
}
