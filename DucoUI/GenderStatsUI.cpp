#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"
#include <StatisticFilters.h>

#include "GenderStatsUI.h"

#include "FiltersUI.h"
#include "SystemDefaultIcon.h"
#include "RingingDatabase.h"
#include "RingerDatabase.h"
#include "PealDatabase.h"

using namespace DucoUI;
using namespace System::Windows::Forms::DataVisualization::Charting;

GenderStatsUI::GenderStatsUI(DucoUI::DatabaseManager^ theDatabase)
:   database(theDatabase)
{
    filters = new Duco::StatisticFilters(database->Database());
    InitializeComponent();
}

GenderStatsUI::!GenderStatsUI()
{
    delete filters;
}

GenderStatsUI::~GenderStatsUI()
{
    this->!GenderStatsUI();
    if (components)
    {
        delete components;
    }
}

System::Void
GenderStatsUI::GenderStatsUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    Generate();
}

System::Void
GenderStatsUI::Generate()
{
    float percentMale;
    float percentFemale;
    float percentNotSet;
    float percentNonHuman;

    database->Database().RingersDatabase().RingerGenderStats(percentMale, percentFemale, percentNotSet, percentNonHuman);

    DataPoint^ newPointTotalMale = gcnew DataPoint(1, percentMale);
    totalRingers->Series[0]->Points->Add(newPointTotalMale);
    DataPoint^ newPointTotalFemale = gcnew DataPoint(1, percentFemale);
    totalRingers->Series[1]->Points->Add(newPointTotalFemale);
    DataPoint^ newPointTotalNotSet = gcnew DataPoint(1, percentNotSet);
    totalRingers->Series[2]->Points->Add(newPointTotalNotSet);
    DataPoint^ newPointTotalNonHuman = gcnew DataPoint(1, percentNonHuman);
    totalRingers->Series[3]->Points->Add(newPointTotalNonHuman);

    float percentMaleConductors;
    float percentFemaleConductors;
    float percentNotSetConductors;
    float percentNonHumanConductors;
    database->Database().PealsDatabase().RingerGenderStats(*filters, percentMale, percentFemale, percentNotSet, percentNonHuman, percentMaleConductors, percentFemaleConductors, percentNotSetConductors, percentNonHumanConductors);
    DataPoint^ newPointMalePeals = gcnew DataPoint(1, percentMale);
    pealRingers->Series[0]->Points->Add(newPointMalePeals);
    DataPoint^ newPointFemalePeals = gcnew DataPoint(1, percentFemale);
    pealRingers->Series[1]->Points->Add(newPointFemalePeals);
    DataPoint^ newPointNotSetPeals = gcnew DataPoint(1, percentNotSet);
    pealRingers->Series[2]->Points->Add(newPointNotSetPeals);
    DataPoint^ newPointNonHumanPeals = gcnew DataPoint(1, percentNonHuman);
    pealRingers->Series[3]->Points->Add(newPointNonHumanPeals);

    DataPoint^ newPointMaleConductors = gcnew DataPoint(1, percentMaleConductors);
    pealConductors->Series[0]->Points->Add(newPointMaleConductors);
    DataPoint^ newPointFemaleConductors = gcnew DataPoint(1, percentFemaleConductors);
    pealConductors->Series[1]->Points->Add(newPointFemaleConductors);
    DataPoint^ newPointNotSetConductors = gcnew DataPoint(1, percentNotSetConductors);
    pealConductors->Series[2]->Points->Add(newPointNotSetConductors);
    DataPoint^ newPointNonHumanConductors = gcnew DataPoint(1, percentNonHumanConductors);
    pealConductors->Series[3]->Points->Add(newPointNonHumanConductors);
}

System::Void
GenderStatsUI::filtersBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    FiltersUI^ filtersDialog = gcnew FiltersUI(*filters, database);
    System::Windows::Forms::DialogResult returnVal = filtersDialog->ShowDialog(this);
    if (returnVal == System::Windows::Forms::DialogResult::OK)
    {
        Generate();
    }
}
