#pragma once

namespace DucoUI
{

public interface class WinRkImportProgressCallback
{
public:
    enum class TImportStage
    {
        ENotStarted = 0,
        EImportingRingers,
        EImportingRingerAkas,
        EImportingCounties,
        EImportingCities,
        EImportingTowers,
        EImportingTenors,
        EImportingRings,
        EImportingOrderNames,
        EImportingMethodNames,
        EImportingMethodTypes,
        EImportingMethods,
        EImportingMultiMethods,
        EImportingSocieties,
        EImportingComposers,
        EImportingPeals,

        ENumberOfStages
    };
    virtual void Initialised(WinRkImportProgressCallback::TImportStage newStage) = 0;
    virtual void Step(int percent) = 0;
    virtual void Complete(bool cancelled) = 0;
};

} // end namespace Duco