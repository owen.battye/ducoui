#pragma once

namespace DucoUI
{
    ref class SoundUtils
    {
    public:
        static System::Void PlayErrorSound();
        static System::Void PlayFinishedSound();
    };
}