#include "DatabaseGenUtils.h"

#include "ReplaceTowerUI.h"

#include "SoundUtils.h"
#include "DatabaseManager.h"
#include <RingingDatabase.h>
#include <TowerDatabase.h>
#include <PealDatabase.h>
#include <StatisticFilters.h>
#include "SystemDefaultIcon.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;

ReplaceTowerUI::ReplaceTowerUI(DatabaseManager^ theDatabase)
:   database(theDatabase)
{
    InitializeComponent();
    allTowerIds = gcnew System::Collections::Generic::List<System::Int16>();
}

ReplaceTowerUI::~ReplaceTowerUI()
{
    if (components)
    {
        delete components;
    }
}

System::Void
ReplaceTowerUI::ReplaceBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId oldTowerId = DatabaseGenUtils::FindId(allTowerIds, oldTower->SelectedIndex);
    Duco::ObjectId newTowerId = DatabaseGenUtils::FindId(allTowerIds, newTower->SelectedIndex);
    if (database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
    }
    else
    {
        if (!database->Database().TowersDatabase().RingsIdentical(oldTowerId, newTowerId))
        {
            MessageBox::Show(this, "Cannot replace tower with selection, Rings in these towers aren't identical.", "Error", MessageBoxButtons::OK, MessageBoxIcon::Warning);
            SoundUtils::PlayErrorSound();
        }
        else
        {
            String^ oldTowerName = database->FullName(TObjectType::ETower, oldTowerId);
            if (MessageBox::Show(this, "Are you sure you want to remove \"" + oldTowerName + "\" from all peals?", "Warning", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning) == ::DialogResult::OK)
            {
                database->ReplaceTower(oldTowerId, newTowerId);

                oldTower->SelectedIndex = -1;
                oldTower->Text = "";
                newTower->SelectedIndex = -1;
                newTower->Text = "";

                Duco::StatisticFilters filters(database->Database());
                filters.SetTower(true, newTowerId);
                size_t newNoOfPeals = database->Database().PealsDatabase().PerformanceInfo(filters).TotalPeals();
                String^ newTowerName = database->FullName(TObjectType::ETower, newTowerId);
                statusLabel->Text = "\"" + newTowerName + "\" is now in " + Convert::ToString(newNoOfPeals) + " performances", "New tower peal count";

                SoundUtils::PlayFinishedSound();
            }
        }
    }
}

System::Void
ReplaceTowerUI::swapBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    int oldIndex = oldTower->SelectedIndex;
    oldTower->SelectedIndex = newTower->SelectedIndex;
    newTower->SelectedIndex = oldIndex;
}

System::Void
ReplaceTowerUI::CloseBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void
ReplaceTowerUI::ReplaceTowerUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    DatabaseGenUtils::GenerateTowerOptions(oldTower, allTowerIds, database, KNoId, false, false, true);
    DatabaseGenUtils::GenerateTowerOptions(newTower, allTowerIds, database, KNoId, false, false, true);
}

System::Void 
ReplaceTowerUI::OldTower_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId removeTowerId = DatabaseGenUtils::FindId(allTowerIds, oldTower->SelectedIndex);
    Duco::ObjectId newTowerId = DatabaseGenUtils::FindId(allTowerIds, newTower->SelectedIndex);
    if (removeTowerId.ValidId())
    {
        Duco::StatisticFilters filters(database->Database());
        filters.SetTower(true, removeTowerId);
        size_t pealCount = database->Database().PealsDatabase().PerformanceInfo(filters).TotalPeals();
        oldTowerLbl->Text = "Exists in " + Convert::ToString(pealCount) + " " + database->PerformanceString(true);
    }
    else
    {
        oldTowerLbl->Text = "";
    }
    EnableButtons(removeTowerId, newTowerId);
}

System::Void 
ReplaceTowerUI::NewTower_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId removeTowerId = DatabaseGenUtils::FindId(allTowerIds, oldTower->SelectedIndex);
    Duco::ObjectId newTowerId = DatabaseGenUtils::FindId(allTowerIds, newTower->SelectedIndex);
    if (newTowerId.ValidId())
    {
        Duco::StatisticFilters filters(database->Database());
        filters.SetTower(true, newTowerId);
        size_t pealCount = database->Database().PealsDatabase().PerformanceInfo(filters).TotalPeals();
        newTowerLbl->Text = "Exists in " + Convert::ToString(pealCount) + " " + database->PerformanceString(true);
    }
    else
    {
        newTowerLbl->Text = "";
    }
    EnableButtons(removeTowerId, newTowerId);
}

System::Void
ReplaceTowerUI::EnableButtons(const Duco::ObjectId& removeTowerId, const Duco::ObjectId& newTowerId)
{
    replaceBtn->Enabled = newTowerId.ValidId() && newTowerId.ValidId() && newTowerId != removeTowerId;
    if (replaceBtn->Enabled)
    {
        statusLabel->Text = "Replace tower id " + Convert::ToString(removeTowerId.Id()) + " with tower id " + Convert::ToString(newTowerId.Id());
    }
    else if (newTowerId.ValidId() && removeTowerId.ValidId() && newTowerId == removeTowerId)
    {
        statusLabel->Text = "Same tower selected";
    }
}
