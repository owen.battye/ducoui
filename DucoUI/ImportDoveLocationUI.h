#pragma once
namespace DucoUI
{
    public ref class ImportDoveLocationUI : public System::Windows::Forms::Form, public DucoUI::ProgressCallbackUI
    {
    public:
        ImportDoveLocationUI(DucoUI::DatabaseManager^ theDatabase);

        // from ProgressCallbackUI
        virtual void Initialised();
        virtual void Step(int progressPercent);
        virtual void Complete();

    protected:
        ~ImportDoveLocationUI();
        !ImportDoveLocationUI();
        System::Void readFileBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void openBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void importbtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void dataGridView_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);
        System::Void selectAllBtn_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void backgroundWorker1_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void backgroundWorker1_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);
        System::Void ImportDoveLocationUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void ImportDoveLocationUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e);
        System::Void StartGeneration(System::Boolean importTowers);

    private:
        System::Windows::Forms::Button^                 closeBtn;
        System::Windows::Forms::Button^                 openBtn;
        System::Windows::Forms::Button^                 importbtn;
        System::Windows::Forms::ToolStripProgressBar^   progressBar;


        System::Windows::Forms::OpenFileDialog^         openFileDialog1;
        System::ComponentModel::BackgroundWorker^       backgroundWorker1;

        DucoUI::ProgressCallbackWrapper*                callback;
        Duco::DoveDatabase*                             doveDatabase;
        DucoUI::DatabaseManager^                        database;
        int                                             numberOfTowersFound;
        int                                             numberOfTowersProcessable;
        System::Boolean                                 importingTowers;

    private: System::Windows::Forms::ToolTip^                   toolTip1;
    private: System::Windows::Forms::DataGridView^              dataGridView;
    private: System::Windows::Forms::ToolStripStatusLabel^      statusLbl;
    private: System::Windows::Forms::Button^                    selectAllBtn;
    private: System::ComponentModel::IContainer^  components;
    private: System::Windows::Forms::ToolStripStatusLabel^  doveFilenameLbl;
    private: System::Windows::Forms::Button^  readFileBtn;
    private: System::Windows::Forms::CheckBox^  allTowers;
    private: System::Windows::Forms::DataGridViewCheckBoxColumn^ includeColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ towerId;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ towerNameColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ doveColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ doveLocationColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ pealbaseColumn;

    private: System::Windows::Forms::DataGridViewCellStyle^ towerAlreadyUsedCellStyle;

    ref class DoveUpdateUiArgs
    {
    public:
        System::String^ doveFile;
        System::Boolean openDoveFile;
        System::Boolean importAllTowersWithDoveId;
    };

#pragma region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        void InitializeComponent(void)
        {
            this->components = (gcnew System::ComponentModel::Container());
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
            System::Windows::Forms::StatusStrip^ statusStrip1;
            this->readFileBtn = (gcnew System::Windows::Forms::Button());
            this->dataGridView = (gcnew System::Windows::Forms::DataGridView());
            this->includeColumn = (gcnew System::Windows::Forms::DataGridViewCheckBoxColumn());
            this->towerId = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->towerNameColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->doveColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->doveLocationColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->pealbaseColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->importbtn = (gcnew System::Windows::Forms::Button());
            this->openBtn = (gcnew System::Windows::Forms::Button());
            this->closeBtn = (gcnew System::Windows::Forms::Button());
            this->selectAllBtn = (gcnew System::Windows::Forms::Button());
            this->allTowers = (gcnew System::Windows::Forms::CheckBox());
            this->progressBar = (gcnew System::Windows::Forms::ToolStripProgressBar());
            this->statusLbl = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->doveFilenameLbl = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
            this->backgroundWorker1 = (gcnew System::ComponentModel::BackgroundWorker());
            this->toolTip1 = (gcnew System::Windows::Forms::ToolTip(this->components));
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            tableLayoutPanel1->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->BeginInit();
            statusStrip1->SuspendLayout();
            this->SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 6;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute, 20)));
            tableLayoutPanel1->Controls->Add(this->readFileBtn, 2, 1);
            tableLayoutPanel1->Controls->Add(this->dataGridView, 0, 0);
            tableLayoutPanel1->Controls->Add(this->importbtn, 4, 1);
            tableLayoutPanel1->Controls->Add(this->openBtn, 1, 1);
            tableLayoutPanel1->Controls->Add(this->closeBtn, 5, 1);
            tableLayoutPanel1->Controls->Add(this->selectAllBtn, 3, 1);
            tableLayoutPanel1->Controls->Add(this->allTowers, 0, 1);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 2;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 51)));
            tableLayoutPanel1->Size = System::Drawing::Size(926, 395);
            tableLayoutPanel1->TabIndex = 0;
            // 
            // readFileBtn
            // 
            this->readFileBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->readFileBtn->Enabled = false;
            this->readFileBtn->Location = System::Drawing::Point(605, 347);
            this->readFileBtn->Name = L"readFileBtn";
            this->readFileBtn->Size = System::Drawing::Size(75, 23);
            this->readFileBtn->TabIndex = 6;
            this->readFileBtn->Text = L"Read file";
            this->toolTip1->SetToolTip(this->readFileBtn, L"Read the dove data file and search towers");
            this->readFileBtn->UseVisualStyleBackColor = true;
            this->readFileBtn->Click += gcnew System::EventHandler(this, &ImportDoveLocationUI::readFileBtn_Click);
            // 
            // dataGridView
            // 
            this->dataGridView->AllowUserToAddRows = false;
            this->dataGridView->AllowUserToDeleteRows = false;
            this->dataGridView->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::AllCells;
            this->dataGridView->AutoSizeRowsMode = System::Windows::Forms::DataGridViewAutoSizeRowsMode::AllCells;
            this->dataGridView->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->dataGridView->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(6) {
                this->includeColumn,
                    this->towerId, this->towerNameColumn, this->doveColumn, this->doveLocationColumn, this->pealbaseColumn
            });
            tableLayoutPanel1->SetColumnSpan(this->dataGridView, 6);
            this->dataGridView->Dock = System::Windows::Forms::DockStyle::Fill;
            this->dataGridView->Location = System::Drawing::Point(3, 3);
            this->dataGridView->Name = L"dataGridView";
            this->dataGridView->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
            this->dataGridView->Size = System::Drawing::Size(920, 338);
            this->dataGridView->TabIndex = 4;
            this->dataGridView->CellContentDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &ImportDoveLocationUI::dataGridView_CellContentDoubleClick);
            // 
            // includeColumn
            // 
            this->includeColumn->FalseValue = L"";
            this->includeColumn->HeaderText = L"";
            this->includeColumn->Name = L"includeColumn";
            this->includeColumn->TrueValue = L"";
            this->includeColumn->Width = 5;
            // 
            // towerId
            // 
            this->towerId->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            this->towerId->HeaderText = L"Id";
            this->towerId->Name = L"towerId";
            this->towerId->ReadOnly = true;
            this->towerId->Width = 41;
            // 
            // towerNameColumn
            // 
            this->towerNameColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
            this->towerNameColumn->HeaderText = L"Tower";
            this->towerNameColumn->Name = L"towerNameColumn";
            this->towerNameColumn->ReadOnly = true;
            // 
            // doveColumn
            // 
            this->doveColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            this->doveColumn->HeaderText = L"Dove reference";
            this->doveColumn->Name = L"doveColumn";
            this->doveColumn->ReadOnly = true;
            this->doveColumn->Width = 97;
            // 
            // doveLocationColumn
            // 
            this->doveLocationColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            this->doveLocationColumn->HeaderText = L"Dove location";
            this->doveLocationColumn->Name = L"doveLocationColumn";
            this->doveLocationColumn->ReadOnly = true;
            this->doveLocationColumn->Width = 90;
            // 
            // pealbaseColumn
            // 
            this->pealbaseColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            this->pealbaseColumn->HeaderText = L"Pealbase";
            this->pealbaseColumn->Name = L"pealbaseColumn";
            this->pealbaseColumn->ReadOnly = true;
            this->pealbaseColumn->Width = 76;
            // 
            // importbtn
            // 
            this->importbtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->importbtn->Enabled = false;
            this->importbtn->Location = System::Drawing::Point(767, 347);
            this->importbtn->Name = L"importbtn";
            this->importbtn->Size = System::Drawing::Size(75, 23);
            this->importbtn->TabIndex = 3;
            this->importbtn->Text = L"Import";
            this->toolTip1->SetToolTip(this->importbtn, L"Import locations for the selected towers which already have Dove Ids");
            this->importbtn->UseVisualStyleBackColor = true;
            this->importbtn->Click += gcnew System::EventHandler(this, &ImportDoveLocationUI::importbtn_Click);
            // 
            // openBtn
            // 
            this->openBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->openBtn->Location = System::Drawing::Point(524, 347);
            this->openBtn->Name = L"openBtn";
            this->openBtn->Size = System::Drawing::Size(75, 23);
            this->openBtn->TabIndex = 1;
            this->openBtn->Text = L"Choose file";
            this->toolTip1->SetToolTip(this->openBtn, L"Download the latest Dove datafile from \"http://dove.cccbr.org.uk/home.php\" and th"
                L"en tell Duco where to find it");
            this->openBtn->UseVisualStyleBackColor = true;
            this->openBtn->Click += gcnew System::EventHandler(this, &ImportDoveLocationUI::openBtn_Click);
            // 
            // closeBtn
            // 
            this->closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            this->closeBtn->Location = System::Drawing::Point(848, 347);
            this->closeBtn->Name = L"closeBtn";
            this->closeBtn->Size = System::Drawing::Size(75, 23);
            this->closeBtn->TabIndex = 0;
            this->closeBtn->Text = L"Close";
            this->closeBtn->UseVisualStyleBackColor = true;
            this->closeBtn->Click += gcnew System::EventHandler(this, &ImportDoveLocationUI::closeBtn_Click);
            // 
            // selectAllBtn
            // 
            this->selectAllBtn->Location = System::Drawing::Point(686, 347);
            this->selectAllBtn->Name = L"selectAllBtn";
            this->selectAllBtn->Size = System::Drawing::Size(75, 23);
            this->selectAllBtn->TabIndex = 5;
            this->selectAllBtn->Text = L"Select all";
            this->selectAllBtn->UseVisualStyleBackColor = true;
            this->selectAllBtn->Click += gcnew System::EventHandler(this, &ImportDoveLocationUI::selectAllBtn_Click);
            // 
            // allTowers
            // 
            this->allTowers->AutoSize = true;
            this->allTowers->Location = System::Drawing::Point(3, 347);
            this->allTowers->Name = L"allTowers";
            this->allTowers->Padding = System::Windows::Forms::Padding(0, 3, 0, 0);
            this->allTowers->Size = System::Drawing::Size(133, 20);
            this->allTowers->TabIndex = 7;
            this->allTowers->Text = L"All towers with Dove id";
            this->toolTip1->SetToolTip(this->allTowers, L"Update all towers with a dove id, not just those without a location or towerbase "
                L"id missing location.");
            this->allTowers->UseVisualStyleBackColor = true;
            // 
            // statusStrip1
            // 
            statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
                this->progressBar, this->statusLbl,
                    this->doveFilenameLbl
            });
            statusStrip1->Location = System::Drawing::Point(0, 373);
            statusStrip1->Name = L"statusStrip1";
            statusStrip1->Size = System::Drawing::Size(926, 22);
            statusStrip1->TabIndex = 1;
            statusStrip1->Text = L"statusStrip1";
            // 
            // progressBar
            // 
            this->progressBar->Name = L"progressBar";
            this->progressBar->Size = System::Drawing::Size(100, 16);
            // 
            // statusLbl
            // 
            this->statusLbl->Name = L"statusLbl";
            this->statusLbl->Size = System::Drawing::Size(759, 17);
            this->statusLbl->Spring = true;
            this->statusLbl->Text = L"status text";
            // 
            // doveFilenameLbl
            // 
            this->doveFilenameLbl->Name = L"doveFilenameLbl";
            this->doveFilenameLbl->Size = System::Drawing::Size(50, 17);
            this->doveFilenameLbl->Text = L"dove.csv";
            // 
            // openFileDialog1
            // 
            this->openFileDialog1->DefaultExt = L"*.csv";
            this->openFileDialog1->Filter = L"CSV files|*.csv|All files|*.*";
            // 
            // backgroundWorker1
            // 
            this->backgroundWorker1->WorkerReportsProgress = true;
            this->backgroundWorker1->WorkerSupportsCancellation = true;
            this->backgroundWorker1->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &ImportDoveLocationUI::backgroundWorker1_DoWork);
            this->backgroundWorker1->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &ImportDoveLocationUI::backgroundWorker1_ProgressChanged);
            this->backgroundWorker1->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &ImportDoveLocationUI::backgroundWorker1_RunWorkerCompleted);
            // 
            // ImportDoveLocationUI
            // 
            this->AcceptButton = this->importbtn;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->CancelButton = this->closeBtn;
            this->ClientSize = System::Drawing::Size(926, 395);
            this->Controls->Add(statusStrip1);
            this->Controls->Add(tableLayoutPanel1);
            this->MinimumSize = System::Drawing::Size(459, 157);
            this->Name = L"ImportDoveLocationUI";
            this->Text = L"Update tower locations and pealbase id";
            this->toolTip1->SetToolTip(this, L"Replaces the location for all towers");
            this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &ImportDoveLocationUI::ImportDoveLocationUI_FormClosing);
            this->Load += gcnew System::EventHandler(this, &ImportDoveLocationUI::ImportDoveLocationUI_Load);
            tableLayoutPanel1->ResumeLayout(false);
            tableLayoutPanel1->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->EndInit();
            statusStrip1->ResumeLayout(false);
            statusStrip1->PerformLayout();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion
    };
}
