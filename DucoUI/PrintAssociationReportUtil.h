#pragma once
#include "PrintUtilBase.h"
#include <set>

namespace Duco
{
    class ObjectId;
    class Peal;
    class Tower;
}

namespace DucoUI
{
    public ref class PrintAssociationReportUtil : public PrintUtilBase
    {
    public:
        PrintAssociationReportUtil(DucoUI::DatabaseManager^ theDatabase, System::Drawing::Printing::PrintPageEventArgs^ printArgs, float fontSize, System::Int32 noOfColumns, float columnSeperation, System::String^ fontname);
        !PrintAssociationReportUtil();
        ~PrintAssociationReportUtil();
        virtual System::Void printObject(System::Drawing::Printing::PrintPageEventArgs^ args, System::Drawing::Printing::PrinterSettings^ printSettings) override;
        System::Void FindPeals(unsigned int pealYear, const std::set<Duco::ObjectId>& additionalPealId, unsigned int numberingStart);

    protected:
        System::String^ GetTextForDateTimeLine(const Duco::Peal& currentPeal, System::Boolean incrementPealCount);
        System::String^ GetTextForLocationLineOne(const Duco::Tower& currentTower);
        System::String^ GetTextForLocationLineTwo(const Duco::Tower& currentTower);
        System::String^ GetTextForDateLine(const Duco::Peal& currentPeal);
        System::String^ GetTextForTimeLine(const Duco::Peal& currentPeal, const Duco::Tower& currentTower);
        System::String^ GetTextForMethodLine(const Duco::Peal& currentPeal);
        System::String^ GetTextForComposerLine(const Duco::Peal& currentPeal);
        System::String^ GetTextForHeaderLine(const Duco::Peal& currentPeal);
        System::String^ GetTextForRingerLines(const Duco::Peal& currentPeal);
        System::Drawing::StringFormat^ GetStringFormatForRingerLines(System::Boolean handbell, System::Drawing::Printing::PrintPageEventArgs^ printArgs);

        System::Void printPeal(const Duco::Peal& currentPeal, System::Single maxWidth, System::Drawing::Printing::PrintPageEventArgs^ printArgs);
        unsigned int measurePeal(const Duco::Peal& currentPeal, System::Single maxWidth, System::Drawing::Printing::PrintPageEventArgs^ printArgs);

    private:
        std::set<Duco::ObjectId>* pealIds;
        Duco::ObjectId* lastPealId;
        Duco::ObjectId* indexId;
        unsigned int pealYear;
        unsigned int lastPrintedYear;
    
        System::Single               columnSeperation;
        System::Single               maxWidth;
        unsigned int                 noOfColumns;
    };

}