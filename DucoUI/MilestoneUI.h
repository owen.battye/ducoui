#pragma once
namespace DucoUI
{

    public ref class MilestoneUI : public System::Windows::Forms::Form, public DucoUI::RingingDatabaseObserver
    {
    public:
        MilestoneUI(DucoUI::DatabaseManager^ theDatabase);
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

    protected:
        !MilestoneUI();
        ~MilestoneUI();
        System::Void MilestoneUI_Load(System::Object^  sender, System::EventArgs^  e);

        System::Void GenerateMilestones();
        System::Void filtersBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void dataGridView_CellClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);
        System::Void AddMilestoneToTables(const Duco::MilestoneData& data);

    private:
        DucoUI::DatabaseManager^                            database;
        System::ComponentModel::Container^                  components;
        Duco::StatisticFilters*                             filters;
        System::Boolean                                     isLoading;




    private: System::Windows::Forms::DataGridView^  milestonePredictions;






    private: System::Windows::Forms::DataVisualization::Charting::Chart^  trendDisplay;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  pealIdColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  pealNoColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  dateColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn2;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn4;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn5;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn6;





    private: System::Windows::Forms::DataGridView^  completedMilestones;














        void InitializeComponent()
        {
            System::Windows::Forms::ToolStrip^  toolStrip1;
            System::Windows::Forms::ToolStripButton^  filtersBtn;
            System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(MilestoneUI::typeid));
            System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
            System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle1 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle2 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle3 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle4 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataVisualization::Charting::ChartArea^  chartArea1 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
            System::Windows::Forms::DataVisualization::Charting::Legend^  legend1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Legend());
            System::Windows::Forms::DataVisualization::Charting::Series^  series1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
            System::Windows::Forms::DataVisualization::Charting::Series^  series2 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
            System::Windows::Forms::DataVisualization::Charting::Series^  series3 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
            System::Windows::Forms::DataVisualization::Charting::Series^  series4 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
            this->completedMilestones = (gcnew System::Windows::Forms::DataGridView());
            this->pealIdColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->pealNoColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->dateColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->milestonePredictions = (gcnew System::Windows::Forms::DataGridView());
            this->dataGridViewTextBoxColumn2 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->dataGridViewTextBoxColumn4 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->dataGridViewTextBoxColumn5 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->dataGridViewTextBoxColumn6 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->trendDisplay = (gcnew System::Windows::Forms::DataVisualization::Charting::Chart());
            toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
            filtersBtn = (gcnew System::Windows::Forms::ToolStripButton());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            toolStrip1->SuspendLayout();
            tableLayoutPanel1->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->completedMilestones))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->milestonePredictions))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trendDisplay))->BeginInit();
            this->SuspendLayout();
            // 
            // toolStrip1
            // 
            toolStrip1->GripStyle = System::Windows::Forms::ToolStripGripStyle::Hidden;
            toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { filtersBtn });
            toolStrip1->Location = System::Drawing::Point(0, 0);
            toolStrip1->Name = L"toolStrip1";
            toolStrip1->Size = System::Drawing::Size(1148, 25);
            toolStrip1->TabIndex = 4;
            toolStrip1->Text = L"toolStrip1";
            // 
            // filtersBtn
            // 
            filtersBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
            filtersBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"filtersBtn.Image")));
            filtersBtn->ImageTransparentColor = System::Drawing::Color::Magenta;
            filtersBtn->Name = L"filtersBtn";
            filtersBtn->Size = System::Drawing::Size(42, 22);
            filtersBtn->Text = L"&Filters";
            filtersBtn->Click += gcnew System::EventHandler(this, &MilestoneUI::filtersBtn_Click);
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 2;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->Controls->Add(this->completedMilestones, 0, 0);
            tableLayoutPanel1->Controls->Add(this->milestonePredictions, 1, 1);
            tableLayoutPanel1->Controls->Add(this->trendDisplay, 1, 0);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 25);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 2;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->Size = System::Drawing::Size(1148, 503);
            tableLayoutPanel1->TabIndex = 5;
            // 
            // completedMilestones
            // 
            this->completedMilestones->AllowUserToAddRows = false;
            this->completedMilestones->AllowUserToDeleteRows = false;
            this->completedMilestones->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::AllCells;
            this->completedMilestones->AutoSizeRowsMode = System::Windows::Forms::DataGridViewAutoSizeRowsMode::AllCells;
            this->completedMilestones->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->completedMilestones->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(3) {
                this->pealIdColumn,
                    this->pealNoColumn, this->dateColumn
            });
            this->completedMilestones->Dock = System::Windows::Forms::DockStyle::Fill;
            this->completedMilestones->Location = System::Drawing::Point(3, 3);
            this->completedMilestones->Name = L"completedMilestones";
            this->completedMilestones->ReadOnly = true;
            tableLayoutPanel1->SetRowSpan(this->completedMilestones, 2);
            this->completedMilestones->Size = System::Drawing::Size(201, 497);
            this->completedMilestones->TabIndex = 3;
            this->completedMilestones->CellClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &MilestoneUI::dataGridView_CellClick);
            // 
            // pealIdColumn
            // 
            this->pealIdColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
            this->pealIdColumn->HeaderText = L"Peal Id";
            this->pealIdColumn->Name = L"pealIdColumn";
            this->pealIdColumn->ReadOnly = true;
            this->pealIdColumn->Visible = false;
            // 
            // pealNoColumn
            // 
            this->pealNoColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->pealNoColumn->HeaderText = L"Peal no";
            this->pealNoColumn->Name = L"pealNoColumn";
            this->pealNoColumn->ReadOnly = true;
            this->pealNoColumn->Width = 68;
            // 
            // dateColumn
            // 
            this->dateColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            dataGridViewCellStyle1->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->dateColumn->DefaultCellStyle = dataGridViewCellStyle1;
            this->dateColumn->HeaderText = L"Date";
            this->dateColumn->Name = L"dateColumn";
            this->dateColumn->ReadOnly = true;
            this->dateColumn->ToolTipText = L"Date this milestone was reached, click row to show peal.";
            this->dateColumn->Width = 55;
            // 
            // milestonePredictions
            // 
            this->milestonePredictions->AllowUserToAddRows = false;
            this->milestonePredictions->AllowUserToDeleteRows = false;
            this->milestonePredictions->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::AllCells;
            this->milestonePredictions->AutoSizeRowsMode = System::Windows::Forms::DataGridViewAutoSizeRowsMode::AllCells;
            this->milestonePredictions->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->milestonePredictions->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(4) {
                this->dataGridViewTextBoxColumn2,
                    this->dataGridViewTextBoxColumn4, this->dataGridViewTextBoxColumn5, this->dataGridViewTextBoxColumn6
            });
            this->milestonePredictions->Dock = System::Windows::Forms::DockStyle::Fill;
            this->milestonePredictions->Location = System::Drawing::Point(210, 310);
            this->milestonePredictions->Name = L"milestonePredictions";
            this->milestonePredictions->ReadOnly = true;
            this->milestonePredictions->Size = System::Drawing::Size(935, 190);
            this->milestonePredictions->TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this->dataGridViewTextBoxColumn2->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->dataGridViewTextBoxColumn2->HeaderText = L"Peal no";
            this->dataGridViewTextBoxColumn2->Name = L"dataGridViewTextBoxColumn2";
            this->dataGridViewTextBoxColumn2->ReadOnly = true;
            this->dataGridViewTextBoxColumn2->Width = 63;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this->dataGridViewTextBoxColumn4->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCellsExceptHeader;
            dataGridViewCellStyle2->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->dataGridViewTextBoxColumn4->DefaultCellStyle = dataGridViewCellStyle2;
            this->dataGridViewTextBoxColumn4->HeaderText = L"6 month average";
            this->dataGridViewTextBoxColumn4->Name = L"dataGridViewTextBoxColumn4";
            this->dataGridViewTextBoxColumn4->ReadOnly = true;
            this->dataGridViewTextBoxColumn4->ToolTipText = L"Predicted time to milestone using peal count from the last 6 months";
            this->dataGridViewTextBoxColumn4->Width = 5;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this->dataGridViewTextBoxColumn5->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCellsExceptHeader;
            dataGridViewCellStyle3->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->dataGridViewTextBoxColumn5->DefaultCellStyle = dataGridViewCellStyle3;
            this->dataGridViewTextBoxColumn5->HeaderText = L"12 month average";
            this->dataGridViewTextBoxColumn5->Name = L"dataGridViewTextBoxColumn5";
            this->dataGridViewTextBoxColumn5->ReadOnly = true;
            this->dataGridViewTextBoxColumn5->ToolTipText = L"Predicted time to milestone using peal count from the last 12 months";
            this->dataGridViewTextBoxColumn5->Width = 5;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this->dataGridViewTextBoxColumn6->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCellsExceptHeader;
            dataGridViewCellStyle4->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->dataGridViewTextBoxColumn6->DefaultCellStyle = dataGridViewCellStyle4;
            this->dataGridViewTextBoxColumn6->HeaderText = L"All time average";
            this->dataGridViewTextBoxColumn6->Name = L"dataGridViewTextBoxColumn6";
            this->dataGridViewTextBoxColumn6->ReadOnly = true;
            this->dataGridViewTextBoxColumn6->ToolTipText = L"Predicted time to milestone using peal count from all peals";
            this->dataGridViewTextBoxColumn6->Width = 5;
            // 
            // trendDisplay
            // 
            chartArea1->AxisX->IsMarginVisible = false;
            chartArea1->AxisX->MinorTickMark->LineColor = System::Drawing::Color::LightGray;
            chartArea1->AxisX->Title = L"Date";
            chartArea1->AxisY->Title = L"Performances";
            chartArea1->Name = L"ChartArea1";
            this->trendDisplay->ChartAreas->Add(chartArea1);
            this->trendDisplay->Dock = System::Windows::Forms::DockStyle::Fill;
            legend1->Name = L"Legend1";
            this->trendDisplay->Legends->Add(legend1);
            this->trendDisplay->Location = System::Drawing::Point(210, 3);
            this->trendDisplay->Name = L"trendDisplay";
            this->trendDisplay->Palette = System::Windows::Forms::DataVisualization::Charting::ChartColorPalette::None;
            series1->ChartArea = L"ChartArea1";
            series1->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Line;
            series1->Color = System::Drawing::Color::Green;
            series1->Legend = L"Legend1";
            series1->LegendText = L"History";
            series1->MarkerStyle = System::Windows::Forms::DataVisualization::Charting::MarkerStyle::Circle;
            series1->Name = L"realData";
            series1->XValueType = System::Windows::Forms::DataVisualization::Charting::ChartValueType::DateTime;
            series2->BorderDashStyle = System::Windows::Forms::DataVisualization::Charting::ChartDashStyle::Dash;
            series2->ChartArea = L"ChartArea1";
            series2->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Line;
            series2->Color = System::Drawing::Color::Olive;
            series2->LabelForeColor = System::Drawing::Color::Olive;
            series2->Legend = L"Legend1";
            series2->LegendText = L"All time average";
            series2->MarkerColor = System::Drawing::Color::Olive;
            series2->MarkerStyle = System::Windows::Forms::DataVisualization::Charting::MarkerStyle::Circle;
            series2->Name = L"allTimeAverage";
            series2->XValueType = System::Windows::Forms::DataVisualization::Charting::ChartValueType::DateTime;
            series3->BorderDashStyle = System::Windows::Forms::DataVisualization::Charting::ChartDashStyle::Dash;
            series3->ChartArea = L"ChartArea1";
            series3->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Line;
            series3->Color = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(192)), static_cast<System::Int32>(static_cast<System::Byte>(64)),
                static_cast<System::Int32>(static_cast<System::Byte>(0)));
            series3->Legend = L"Legend1";
            series3->LegendText = L"12 month average";
            series3->MarkerColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(192)), static_cast<System::Int32>(static_cast<System::Byte>(64)),
                static_cast<System::Int32>(static_cast<System::Byte>(0)));
            series3->MarkerStyle = System::Windows::Forms::DataVisualization::Charting::MarkerStyle::Circle;
            series3->Name = L"twelveMonthAverage";
            series3->XValueType = System::Windows::Forms::DataVisualization::Charting::ChartValueType::DateTime;
            series4->BorderDashStyle = System::Windows::Forms::DataVisualization::Charting::ChartDashStyle::Dash;
            series4->ChartArea = L"ChartArea1";
            series4->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Line;
            series4->Color = System::Drawing::Color::Maroon;
            series4->Legend = L"Legend1";
            series4->LegendText = L"6 month average";
            series4->MarkerStyle = System::Windows::Forms::DataVisualization::Charting::MarkerStyle::Circle;
            series4->Name = L"sixMonthAverage";
            series4->XValueType = System::Windows::Forms::DataVisualization::Charting::ChartValueType::DateTime;
            this->trendDisplay->Series->Add(series1);
            this->trendDisplay->Series->Add(series2);
            this->trendDisplay->Series->Add(series3);
            this->trendDisplay->Series->Add(series4);
            this->trendDisplay->Size = System::Drawing::Size(935, 301);
            this->trendDisplay->TabIndex = 5;
            this->trendDisplay->Text = L"chart1";
            // 
            // MilestoneUI
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(1148, 528);
            this->Controls->Add(tableLayoutPanel1);
            this->Controls->Add(toolStrip1);
            this->Name = L"MilestoneUI";
            this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Show;
            this->Text = L"Milestones";
            this->Load += gcnew System::EventHandler(this, &MilestoneUI::MilestoneUI_Load);
            toolStrip1->ResumeLayout(false);
            toolStrip1->PerformLayout();
            tableLayoutPanel1->ResumeLayout(false);
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->completedMilestones))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->milestonePredictions))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trendDisplay))->EndInit();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
    };
}
