#include "RingingDatabaseObserver.h"
#include "DatabaseManager.h"

#include "DayOrderUI.h"

#include <RingingDatabase.h>
#include "SoundUtils.h"
#include "SystemDefaultIcon.h"
#include "DucoUtils.h"
#include <PealDatabase.h>
#include <Peal.h>

using namespace Duco;
using namespace DucoUI;
using namespace System;

DayOrderUI::DayOrderUI(DucoUI::DatabaseManager^ theDatabase)
:   database(theDatabase)
{
    lastSelectedPealId = new Duco::ObjectId();
    InitializeComponent();
}

DayOrderUI::~DayOrderUI()
{
    delete lastSelectedPealId;
    database->RemoveObserver(this);
    if (components)
    {
        delete components;
    }
}

void
DayOrderUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    switch (type)
    {
    case TObjectType::EPeal:
        {
        unsigned int previousDateIndex = dateSelector->SelectedIndex;
        unsigned int previousPealIndex = -1;
        if (!lastSelectedPealId->ValidId())
        {
            *lastSelectedPealId = SelectedPealId(SelectedPealIndex());
        }
        dateSelector->Items->Clear();
        GenerateData(previousDateIndex);
        }
        break;

    default:
        break;
    }
}

System::Void
DayOrderUI::upBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
        return;
    }

    int selectedIndex = SelectedPealIndex();
    if (selectedIndex > 0 && selectedIndex <= (pealList->Items->Count-1) )
    {
        *lastSelectedPealId = SelectedPealId(selectedIndex-1);
        unsigned int pealId2 = SelectedPealId(selectedIndex);

        database->SwapPeals(*lastSelectedPealId, pealId2, selectedIndex-1);
    }
}

System::Void
DayOrderUI::downBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
        return;
    }

    int selectedIndex = SelectedPealIndex();
    if (selectedIndex >= 0 && selectedIndex <= (pealList->Items->Count-2) )
    {
        unsigned int pealId1 = SelectedPealId(selectedIndex);
        *lastSelectedPealId = SelectedPealId(selectedIndex+1);
        database->SwapPeals(pealId1, *lastSelectedPealId, selectedIndex);
    }
}

System::Void
DayOrderUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void
DayOrderUI::DayOrderUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    database->AddObserver(this);
    GenerateData(-1);
}

System::Void
DayOrderUI::GenerateData(int dateIndex)
{
    std::list<PerformanceDate> dates;
    database->Database().PealsDatabase().GetDatesWithMultiplePeals(dates);

    std::list<PerformanceDate>::const_iterator it = dates.begin();
    dateSelector->BeginUpdate();
    while (it != dates.end())
    {
        System::DateTime^ date = DucoUtils::ConvertDate(*it++);
        dateSelector->Items->Add(date->ToString("dddd, d MMMM yyyy"));
    }
    dateSelector->EndUpdate();
    if (!dates.empty())
    {
        if (dateIndex < 0 || dateIndex > dateSelector->Items->Count)
            dateSelector->SelectedIndex = int(dates.size()-1);
        else
        {
            dateSelector->SelectedIndex = dateIndex;
        }
        SelectLastPeal();
    }
}

System::Void
DayOrderUI::dateSelector_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    pealList->Items->Clear();

    PerformanceDate date = DucoUtils::ConvertDate(Convert::ToDateTime(dateSelector->Text));
    std::vector<Duco::ObjectId> pealIds;
    database->Database().PealsDatabase().GetPealWithDate(date, pealIds);

    std::vector<Duco::ObjectId>::const_iterator it = pealIds.begin();
    while (it != pealIds.end())
    {
        Duco::Peal thePeal;
        if (database->FindPeal(*it, thePeal, false))
        {
            std::wstring pealTitle;
            thePeal.Title(pealTitle, database->Database());
            pealList->Items->Add(DucoUtils::ConvertString(pealTitle));
        }
        ++it;
    }
    EnableButtons(-1, pealList->Items->Count);
}

System::Void
DayOrderUI::pealList_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    EnableButtons(SelectedPealIndex(), pealList->Items->Count);
}

int
DayOrderUI::SelectedPealIndex()
{
    int selectedIndex = -1;
    if (pealList->SelectedItems->Count > 0)
    {
        selectedIndex = pealList->SelectedItems[0]->Index;
    }
    return selectedIndex;
}

unsigned int
DayOrderUI::SelectedPealId(int index)
{
    unsigned int pealId = -1;
    if (pealList->Items->Count > index && index >= 0)
    {
        String^ tempStr = pealList->Items[index]->Text;
        int seperatorIndex = tempStr->IndexOf(":");
        String^ tempStrPealId = tempStr->Substring(0,seperatorIndex);
        pealId = Convert::ToInt16(tempStrPealId);
    }
    return pealId;
}

System::Void
DayOrderUI::EnableButtons(int position, int count)
{
    upBtn->Enabled = position > 0;
    downBtn->Enabled = position < (count-1) && count > 1 && position >= 0;
}

System::Void
DayOrderUI::SelectLastPeal()
{
    if (lastSelectedPealId->ValidId())
    {
        for each (System::Windows::Forms::ListViewItem^ var in pealList->Items)
        {
            String^ tempStr = var->Text;
            int seperatorIndex = tempStr->IndexOf(":");
            String^ tempStrPealId = tempStr->Substring(0,seperatorIndex);
            ObjectId pealId = Convert::ToInt16(tempStrPealId);
            if (pealId == *lastSelectedPealId)
            {
               pealList->Focus();
               var->Selected = true;
            }
        }
    }
    lastSelectedPealId->ClearId();
}
