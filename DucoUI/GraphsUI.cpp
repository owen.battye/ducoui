#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"
#include <StatisticFilters.h>

#include "GraphsUI.h"

#include "SystemDefaultIcon.h"
#include "FiltersUI.h"
#include <RingingDatabase.h>
#include "DucoUtils.h"
#include <PealDatabase.h>
#include <Ringer.h>
#include <Tower.h>
#include <algorithm>
#include <DatabaseSettings.h>

using namespace std;
using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::Windows::Forms::DataVisualization::Charting;

GraphsUI::GraphsUI(DucoUI::DatabaseManager^ theDatabase)
    : database(theDatabase)
{
    filters = new Duco::StatisticFilters(database->Database());
    InitializeComponent();
}

GraphsUI::!GraphsUI()
{
    delete filters;
}

GraphsUI::~GraphsUI()
{
    this->!GraphsUI();
    if (components)
    {
        delete components;
    }
}

System::Void
GraphsUI::GraphsUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    chart->Series->Clear();
    DrawYears();
}

System::Void
GraphsUI::filtersBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    FiltersUI^ filtersDialog = gcnew FiltersUI(*filters, database);
    System::Windows::Forms::DialogResult returnVal = filtersDialog->ShowDialog(this);
    if (returnVal == System::Windows::Forms::DialogResult::OK)
    {
        if (years->Checked)
            yearGraph_CheckedChanged(sender, e);
        else if (towers->Checked)
            towers_CheckedChanged(sender, e);
        else if (ringers->Checked)
            ringers_CheckedChanged(sender, e);
        else if (stages->Checked)
            stages_CheckedChanged(sender, e);
        else if (uniqueCounts->Checked)
            uniqueCounts_CheckedChanged(sender, e);
    }
}

System::Void
GraphsUI::yearGraph_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (years->Checked)
    {
        towers->Checked = false;
        ringers->Checked = false;
        stages->Checked = false;
        uniqueCounts->Checked = false;
        chart->Series->Clear();
        DrawYears();
        chart->ChartAreas[0]->RecalculateAxesScale();
    }
}

System::Void
GraphsUI::towers_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (towers->Checked)
    {
        years->Checked = false;
        ringers->Checked = false;
        stages->Checked = false;
        uniqueCounts->Checked = false;
        chart->Series->Clear();
        DrawTowers();
        chart->ChartAreas[0]->RecalculateAxesScale();
    }
}

System::Void
GraphsUI::ringers_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (ringers->Checked)
    {
        years->Checked = false;
        towers->Checked = false;
        stages->Checked = false;
        uniqueCounts->Checked = false;
        chart->Series->Clear();
        DrawRingers();
        chart->ChartAreas[0]->RecalculateAxesScale();
    }
}

System::Void
GraphsUI::stages_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (stages->Checked)
    {
        years->Checked = false;
        towers->Checked = false;
        ringers->Checked = false;
        uniqueCounts->Checked = false;
        chart->Series->Clear();
        DrawStages();
        chart->ChartAreas[0]->RecalculateAxesScale();
    }
}

System::Void
GraphsUI::uniqueCounts_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (uniqueCounts->Checked)
    {
        years->Checked = false;
        towers->Checked = false;
        ringers->Checked = false;
        stages->Checked = false;
        chart->Series->Clear();
        DrawUniqueCounts();
        chart->ChartAreas[0]->RecalculateAxesScale();
    }
}

System::Void
GraphsUI::DrawRingers()
{
    std::map<Duco::ObjectId, std::map<unsigned int, PealLengthInfo> > sortedPealCounts;
    database->Database().PealsDatabase().GetTopRingersPealCountPerYear(*filters, true, 15, sortedPealCounts);

    std::map<Duco::ObjectId, std::map<unsigned int, PealLengthInfo> >::const_iterator it = sortedPealCounts.begin();
    while (it != sortedPealCounts.end())
    {
        Duco::Ringer theRinger;
        if (database->FindRinger(it->first, theRinger, false))
        {
            String^ seriesName = DucoUtils::ConvertString(theRinger.FullName(database->Database().Settings().LastNameFirst()));
            UniqueSeriesName(seriesName);

            Series^ newSeries = gcnew Series(seriesName);
            std::map<unsigned int, PealLengthInfo>::const_iterator it2 = it->second.begin();
            while (it2 != it->second.end())
            {
                DataPoint^ newPoint = gcnew DataPoint(it2->first, double(it2->second.TotalPeals()));
                newSeries->Points->Add(newPoint);
                ++it2;
            }
            newSeries->ChartType = SeriesChartType::Spline;
            newSeries->BorderWidth = 2;
            chart->Series->Add(newSeries);
        }
        ++it;
    }
    currentDisplayLabel->Text = "Number of " + database->PerformanceString(true) + " with leading ringers per year";
}

System::Void
GraphsUI::DrawTowers()
{
    std::map<Duco::ObjectId, std::map<unsigned int, Duco::PealLengthInfo> > sortedPealCounts;
    database->Database().PealsDatabase().GetTopTowersPealCountPerYear(*filters, 10, sortedPealCounts);

    std::map<Duco::ObjectId, std::map<unsigned int, Duco::PealLengthInfo> >::const_iterator it = sortedPealCounts.begin();
    while (it != sortedPealCounts.end())
    {
        Duco::Tower theTower;
        if (database->FindTower(it->first, theTower, false))
        {
            String^ seriesName = DucoUtils::ConvertString(theTower.FullName());
            UniqueSeriesName(seriesName);

            Series^ newSeries = gcnew Series(seriesName);
            std::map<unsigned int, Duco::PealLengthInfo>::const_iterator it2 = it->second.begin();
            while (it2 != it->second.end())
            {
                DataPoint^ newPoint = gcnew DataPoint(it2->first, double(it2->second.TotalPeals()));
                newSeries->Points->Add(newPoint);
                ++it2;
            }
            newSeries->ChartType = SeriesChartType::Spline;
            newSeries->BorderWidth = 2;
            chart->Series->Add(newSeries);
        }
        ++it;
    }

    currentDisplayLabel->Text = "Number of performances at leading venues per year";
}

System::Void
GraphsUI::DrawStages()
{
    std::map<unsigned int, std::map<unsigned int, PealLengthInfo> > sortedPealCounts;
    database->Database().PealsDatabase().GetStagesCountByYear(*filters, sortedPealCounts);

    std::map<unsigned int, std::map<unsigned int, PealLengthInfo> >::const_iterator it = sortedPealCounts.begin();
    while (it != sortedPealCounts.end())
    {
        System::String^ seriesName = "";
        if (!database->FindOrderName(it->first, seriesName))
            seriesName = Convert::ToString(it->first);
        UniqueSeriesName(seriesName);

        Series^ newSeries = gcnew Series(seriesName);
        std::map<unsigned int, PealLengthInfo>::const_iterator it2 = it->second.begin();
        while (it2 != it->second.end())
        {
            DataPoint^ newPoint = gcnew DataPoint(it2->first, double(it2->second.TotalPeals()));
            newSeries->Points->Add(newPoint);
            ++it2;
        }
        newSeries->ChartType = SeriesChartType::Spline;
        newSeries->BorderWidth = 2;
        chart->Series->Add(newSeries);
        ++it;
    }

    currentDisplayLabel->Text = "Number of " + database->PerformanceString(false) + "s on each stage per year";
}

System::Void
GraphsUI::DrawYears()
{
    std::map<unsigned int, Duco::PealLengthInfo> sortedPealCounts;
    database->Database().PealsDatabase().GetAllYearsByPealCount(*filters, sortedPealCounts);

    Series^ newSeries = gcnew Series("Year");
    newSeries->ChartType = SeriesChartType::Column;
    std::map<unsigned int, Duco::PealLengthInfo>::const_iterator it = sortedPealCounts.begin();
    while (it != sortedPealCounts.end())
    {
        DataPoint^ newPoint = gcnew DataPoint(it->first, double(it->second.TotalPeals()));
        newSeries->ChartType = SeriesChartType::Spline;
        newSeries->Points->Add(newPoint);
        ++it;
    }

    newSeries->IsVisibleInLegend = false;
    newSeries->BorderWidth = 2;
    currentDisplayLabel->Text = "Performances per year";
    chart->Series->Add(newSeries);
}

System::Void
GraphsUI::DrawUniqueCounts()
{
    std::map<unsigned int, size_t> towerCounts;
    std::map<unsigned int, size_t> ringerCounts;
    std::map<unsigned int, size_t> methodCounts;
    database->Database().PealsDatabase().GetUniqueCounts(*filters, towerCounts, ringerCounts, methodCounts);

    Series^ towerSeries = gcnew Series("Towers");
    towerSeries->ChartType = SeriesChartType::Line;
    std::map<unsigned int, size_t>::const_iterator towerIt = towerCounts.begin();
    while (towerIt != towerCounts.end())
    {
        DataPoint^ newPoint = gcnew DataPoint(towerIt->first, double(towerIt->second));
        towerSeries->Points->Add(newPoint);
        ++towerIt;
    }
    chart->Series->Add(towerSeries);

    Series^ ringerSeries = gcnew Series("Ringers");
    ringerSeries->ChartType = SeriesChartType::Line;
    std::map<unsigned int, size_t>::const_iterator ringerIt = ringerCounts.begin();
    while (ringerIt != ringerCounts.end())
    {
        DataPoint^ newPoint = gcnew DataPoint(ringerIt->first, double(ringerIt->second));
        ringerSeries->Points->Add(newPoint);
        ++ringerIt;
    }
    ringerSeries->ChartType = SeriesChartType::Spline;
    chart->Series->Add(ringerSeries);

    Series^ methodSeries = gcnew Series("Methods");
    methodSeries->ChartType = SeriesChartType::Line;
    std::map<unsigned int, size_t>::const_iterator methodIt = methodCounts.begin();
    while (methodIt != methodCounts.end())
    {
        DataPoint^ newPoint = gcnew DataPoint(methodIt->first, double(methodIt->second));
        methodSeries->Points->Add(newPoint);
        ++methodIt;
    }
    methodSeries->ChartType = SeriesChartType::Spline;
    chart->Series->Add(methodSeries);

    currentDisplayLabel->Text = "Number of unique towers, ringers and methods per year";
}

System::Void
GraphsUI::UniqueSeriesName(System::String^% seriesName)
{
    String^ seriesNameBase = seriesName;
    for (int i(2); !chart->Series->IsUniqueName(seriesName); ++i)
    {
        seriesName = String::Format("{0} {1}", seriesNameBase, i);
    }
}
