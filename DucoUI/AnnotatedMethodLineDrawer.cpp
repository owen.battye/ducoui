#include "AnnotatedMethodLineDrawer.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::Drawing;

AnnotatedMethodLineDrawer::AnnotatedMethodLineDrawer(System::Drawing::Graphics^ lineDrwr, Duco::Method& theMethod, DucoUI::DatabaseManager^ theDatabase, System::ComponentModel::BackgroundWorker^ bgWorker)
: NormalMethodLineDrawer(lineDrwr, theMethod, theDatabase, bgWorker)
{
    CreateGridPens();
}

bool
AnnotatedMethodLineDrawer::ShowMusic()
{
    return true;
}
