#include "OpenObjectCallback.h"
#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"
#include "PictureListControl.h"

#include "PictureManagerUI.h"

#include "DucoWindowState.h"
#include "PictureControl.h"
#include "RingerList.h"
#include <Tower.h>
#include "PrintBandSummaryUtil.h"
#include <DownloadedPeal.h>
#include "PealUI.h"
#include "TowerUI.h"
#include "SystemDefaultIcon.h"
#include "PictureListControl.h"

using namespace System;
using namespace System::Data;
using namespace System::Drawing;
using namespace System::Text;
using namespace System::Windows::Forms;
using namespace DucoUI;
using namespace Duco;

PictureManagerUI::PictureManagerUI(DucoUI::DatabaseManager^ theDatabase)
    : database(theDatabase)
{
    InitializeComponent();
    pictureList = gcnew DucoUI::PictureListControl(database, this);
    this->Controls->Add(this->pictureList);
    this->pictureList->Dock = System::Windows::Forms::DockStyle::Fill;
    this->pictureList->Location = System::Drawing::Point(0, 0);
    this->pictureList->Margin = System::Windows::Forms::Padding(0);
    this->pictureList->Name = L"pictureList";
    this->pictureList->Size = System::Drawing::Size(363, 217);
    this->pictureList->TabIndex = 0;
    this->pictureList->closeHandler += gcnew System::EventHandler(this, &PictureManagerUI::closeBtn_Click);
}

PictureManagerUI::~PictureManagerUI()
{
    if (components)
    {
        delete components;
    }
}

System::Void
PictureManagerUI::PictureManagerUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    this->Icon = SystemDefaultIcon::DefaultSystemIcon();
    pictureList->RefreshList(true, true, true);
}

System::Void
PictureManagerUI::PictureManagerUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e)
{
}

System::Void
PictureManagerUI::PictureManagerUI_Resize(System::Object^  sender, System::EventArgs^  e)
{
    pictureList->RefreshList(false, false, false);
}

System::Void
PictureManagerUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

void
PictureManagerUI::OpenObject(Duco::ObjectId& objectId, Duco::TObjectType objectType)
{
    switch (objectType)
    {
    case TObjectType::EPeal:
        PealUI::ShowPeal(database, this->MdiParent, objectId);
        break;
    case TObjectType::ETower:
        TowerUI::ShowTower(database, this->MdiParent, objectId, KNoId);
        break;
    default:
        break;
    }
}
