#pragma once

namespace DucoUI
{
    public ref class NewStageUI : public System::Windows::Forms::Form
    {
    public:
        NewStageUI(DucoUI::DatabaseManager^ theManager);

    protected:
        ~NewStageUI();

        System::Void addBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void cancelBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void NewStageUI_Load(System::Object^  sender, System::EventArgs^  e);

    private:
        System::ComponentModel::Container ^     components;
        DucoUI::DatabaseManager^                database;
        System::Windows::Forms::Button^         addBtn;
        System::Windows::Forms::NumericUpDown^  noOfBellsEditor;
        System::Windows::Forms::TextBox^        stageNameEditor;

        void InitializeComponent()
        {
            System::Windows::Forms::Button^  cancelBtn;
            System::Windows::Forms::Label^  label1;
            System::Windows::Forms::Label^  label2;
            System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
            this->addBtn = (gcnew System::Windows::Forms::Button());
            this->noOfBellsEditor = (gcnew System::Windows::Forms::NumericUpDown());
            this->stageNameEditor = (gcnew System::Windows::Forms::TextBox());
            cancelBtn = (gcnew System::Windows::Forms::Button());
            label1 = (gcnew System::Windows::Forms::Label());
            label2 = (gcnew System::Windows::Forms::Label());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->noOfBellsEditor))->BeginInit();
            tableLayoutPanel1->SuspendLayout();
            this->SuspendLayout();
            // 
            // cancelBtn
            // 
            cancelBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            cancelBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            cancelBtn->Location = System::Drawing::Point(205, 56);
            cancelBtn->Name = L"cancelBtn";
            cancelBtn->Size = System::Drawing::Size(75, 23);
            cancelBtn->TabIndex = 5;
            cancelBtn->Text = L"Cancel";
            cancelBtn->UseVisualStyleBackColor = true;
            cancelBtn->Click += gcnew System::EventHandler(this, &NewStageUI::cancelBtn_Click);
            // 
            // label1
            // 
            label1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            label1->AutoSize = true;
            label1->Location = System::Drawing::Point(10, 7);
            label1->Margin = System::Windows::Forms::Padding(3, 7, 3, 0);
            label1->Name = L"label1";
            label1->Size = System::Drawing::Size(57, 13);
            label1->TabIndex = 0;
            label1->Text = L"No of bells";
            // 
            // label2
            // 
            label2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            label2->AutoSize = true;
            label2->Location = System::Drawing::Point(3, 33);
            label2->Margin = System::Windows::Forms::Padding(3, 7, 3, 0);
            label2->Name = L"label2";
            label2->Size = System::Drawing::Size(64, 13);
            label2->TabIndex = 2;
            label2->Text = L"Stage name";
            // 
            // addBtn
            // 
            this->addBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->addBtn->Location = System::Drawing::Point(124, 56);
            this->addBtn->Name = L"addBtn";
            this->addBtn->Size = System::Drawing::Size(75, 23);
            this->addBtn->TabIndex = 4;
            this->addBtn->Text = L"Add";
            this->addBtn->UseVisualStyleBackColor = true;
            this->addBtn->Click += gcnew System::EventHandler(this, &NewStageUI::addBtn_Click);
            // 
            // noOfBellsEditor
            // 
            this->noOfBellsEditor->Dock = System::Windows::Forms::DockStyle::Fill;
            this->noOfBellsEditor->Location = System::Drawing::Point(73, 3);
            this->noOfBellsEditor->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 24, 0, 0, 0 });
            this->noOfBellsEditor->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 3, 0, 0, 0 });
            this->noOfBellsEditor->Name = L"noOfBellsEditor";
            this->noOfBellsEditor->Size = System::Drawing::Size(126, 20);
            this->noOfBellsEditor->TabIndex = 1;
            this->noOfBellsEditor->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 4, 0, 0, 0 });
            // 
            // stageNameEditor
            // 
            tableLayoutPanel1->SetColumnSpan(this->stageNameEditor, 2);
            this->stageNameEditor->Dock = System::Windows::Forms::DockStyle::Fill;
            this->stageNameEditor->Location = System::Drawing::Point(73, 29);
            this->stageNameEditor->Name = L"stageNameEditor";
            this->stageNameEditor->Size = System::Drawing::Size(207, 20);
            this->stageNameEditor->TabIndex = 3;
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 3;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel1->Controls->Add(this->addBtn, 1, 2);
            tableLayoutPanel1->Controls->Add(label2, 0, 1);
            tableLayoutPanel1->Controls->Add(cancelBtn, 2, 2);
            tableLayoutPanel1->Controls->Add(label1, 0, 0);
            tableLayoutPanel1->Controls->Add(this->stageNameEditor, 1, 1);
            tableLayoutPanel1->Controls->Add(this->noOfBellsEditor, 1, 0);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 3;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel1->Size = System::Drawing::Size(283, 82);
            tableLayoutPanel1->TabIndex = 6;
            // 
            // NewStageUI
            // 
            this->AcceptButton = this->addBtn;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->CancelButton = cancelBtn;
            this->ClientSize = System::Drawing::Size(283, 82);
            this->Controls->Add(tableLayoutPanel1);
            this->MaximizeBox = false;
            this->MaximumSize = System::Drawing::Size(299, 121);
            this->MinimizeBox = false;
            this->MinimumSize = System::Drawing::Size(299, 121);
            this->Name = L"NewStageUI";
            this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
            this->Text = L"New stage";
            this->Load += gcnew System::EventHandler(this, &NewStageUI::NewStageUI_Load);
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->noOfBellsEditor))->EndInit();
            tableLayoutPanel1->ResumeLayout(false);
            tableLayoutPanel1->PerformLayout();
            this->ResumeLayout(false);

        }
    };
}

