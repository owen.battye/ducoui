#include "WinRKv3Import.h"
#include "DatabaseManager.h"
#include <RingingDatabase.h>
#include <DucoEngineUtils.h>
#include <Tower.h>
#include <Ring.h>
#include <Ringer.h>
#include <Peal.h>
#include <PealDatabase.h>
#include <RingerDatabase.h>
#include <MethodDatabase.h>
#include "DucoUtils.h"
#include <Method.h>
#include <TowerDatabase.h>
#include <Association.h>
#include <AssociationDatabase.h>
#include <StatisticFilters.h>

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace std;
using namespace System::Data::Odbc;

WinRKv3Import::WinRKv3Import(DucoUI::DatabaseManager^ theDatabase, DucoUI::WinRkImportProgressCallback^ newCallback, System::String^ theFileName)
: DatabaseImportUtil(theDatabase, newCallback, theFileName), ringingWorldPublicationId(-1)
{
}

WinRKv3Import::~WinRKv3Import()
{
    if (connection != nullptr)
        connection->Close();
}

void
WinRKv3Import::Import(System::ComponentModel::BackgroundWorker^ worker, System::ComponentModel::DoWorkEventArgs^  e)
{
    String^ connectionString = "Driver={Microsoft Access Driver (*.mdb)};DBQ=" + fileName + ";provider=SQLOLEDB;User Id="";Password=""";
    connection = gcnew OdbcConnection (connectionString);
    connection->Open();
    OdbcCommand^ command = connection->CreateCommand();

    map<Duco::ObjectId, Duco::ObjectId> oldAKAs;
    map<unsigned int, unsigned int> oldAKAsWithOutDates;
    map<unsigned int, wstring> multiMethods;
    map<unsigned int, unsigned int> oldRingIdsToTowerIds;
    map<unsigned int, Duco::ObjectId> oldRingIdsToNewRingIds;
    map<unsigned int, unsigned int> oldOrderNamesToNew;

    FindRingingWorldPublicationId(command);

    if (!worker->CancellationPending)
        ImportRingers(command, worker);
    if (!worker->CancellationPending)
        ImportRingerAkas(command, oldAKAs, oldAKAsWithOutDates, worker);
    if (!worker->CancellationPending)
        ImportTowers(command, worker);
    if (!worker->CancellationPending)
        ImportRings(command, oldRingIdsToTowerIds, oldRingIdsToNewRingIds, worker);
    map<unsigned int, std::wstring> extraOrders;
    if (!worker->CancellationPending)
        ImportOrderNames(command, extraOrders, oldOrderNamesToNew, worker);
    if (!worker->CancellationPending)
        ImportMethods(command, extraOrders, oldOrderNamesToNew, worker);
    if (!worker->CancellationPending)
        ImportSingleStringFromTable(command, L"MultiMethods", multiMethods, DucoUI::WinRkImportProgressCallback::TImportStage::EImportingMultiMethods, worker);
    if (!worker->CancellationPending)
        ImportPeals(command, oldAKAs, multiMethods, oldRingIdsToTowerIds, oldRingIdsToNewRingIds, worker);
    if (!worker->CancellationPending)
        RepairRingerAkasWithoutDates(oldAKAsWithOutDates, worker);
    database->Database().PostWinRkImportChecks();

    oldAKAs.clear();
    multiMethods.clear();
    oldRingIdsToTowerIds.clear();
    oldRingIdsToNewRingIds.clear();
    oldAKAsWithOutDates.clear();

    if (worker->CancellationPending)
        e->Cancel = true;
}

System::String^
WinRKv3Import::FileType()
{
    return "WinRk (version 3)";
}

/*****************************************************************************************
* Import from tables
******************************************************************************************/

void
WinRKv3Import::ImportPeals(OdbcCommand^ command,
                         std::map<Duco::ObjectId, Duco::ObjectId>& oldAKAs, 
                         const map<unsigned int, wstring>& multiMethods,
                         const std::map<unsigned int, unsigned int>& oldRingIdsToTowerIds,
                         const std::map<unsigned int, Duco::ObjectId>& oldRingIdsToNewRingIds,
                         System::ComponentModel::BackgroundWorker^ worker)
{
    map<unsigned int, Duco::ObjectId> societyIds;
    {
        map<unsigned int, wstring> societies;
        ImportSingleStringFromTable(command, "Societies", societies, DucoUI::WinRkImportProgressCallback::TImportStage::EImportingSocieties, worker);

        map<unsigned int, wstring>::const_iterator societiesIt = societies.begin();
        while (societiesIt != societies.end())
        {
            Duco::ObjectId newAssociationId = database->Database().AssociationsDatabase().SuggestAssociationOrCreate(societiesIt->second);
            std::pair<unsigned int, Duco::ObjectId> newSoc(societiesIt->first, newAssociationId);
            societyIds.insert(newSoc);
            ++societiesIt;
        }
    }

    map<unsigned int, wstring> composers;
    ImportSingleStringFromTable(command, "Composers", composers, DucoUI::WinRkImportProgressCallback::TImportStage::EImportingComposers, worker);

    command->CommandText = "SELECT COUNT(*) FROM Peals";
    int noOfRecords = (int)command->ExecuteScalar();
    int count(0);
    ReportProgress(DucoUI::WinRkImportProgressCallback::TImportStage::EImportingPeals, noOfRecords, count);
    command->CommandText = "SELECT * FROM Peals";
    OdbcDataReader^ reader = command->ExecuteReader();
    bool containsPeals (false);
    bool containsQuarters (false);
    unsigned int pealsIgnored = 0;

    while (reader->Read() && !worker->CancellationPending)
    {
        unsigned int pealId = reader->GetInt16(0);
        unsigned int societyId = reader->GetInt16(1);
        unsigned int ringId = reader->GetInt16(2);
        unsigned int methodId = reader->GetInt16(3);
        unsigned int composerId = reader->GetInt16(4);
        unsigned int multimethodId = reader->GetInt16(5);
        wstring footnote;
        ReadString(reader, 6, footnote);
        int pealType = reader->GetInt16(7);
        bool handBells (false);
        switch (pealType)
        {
        case 0:
            containsPeals = true;
            break;
        case 1:
            containsQuarters = true;
            break;
        case 2:
            containsPeals = true;
            handBells = true;
            break;
        case 3:
            containsQuarters = true;
            handBells = true;
            break;
        }
        TConductorType conductedType = static_cast<TConductorType>(reader->GetInt16(8));
        PerformanceDate performanceDate = ReadDateFromV3(reader, 9);
        unsigned int changes = reader->GetInt32(10);
        PerformanceTime performanceTime = ReadTime(reader, 11);
        std::map<unsigned int, Duco::ObjectId> ringerIds;
        int handbellOffset = 0;
        for (int fieldNumber = 12;  fieldNumber <= 31; ++fieldNumber)
        {
            Duco::ObjectId ringerId = reader->GetInt16(fieldNumber);
            if (ringerId.Id() == 0)
            {
                ringerId.ClearId();
            }
            if (ringerId.ValidId())
            {
                ringerId = DatabaseImportUtil::CheckRingerAkaIds(ringerId, oldAKAs);
                pair<unsigned int, Duco::ObjectId> newObject(fieldNumber - 11 - handbellOffset, ringerId);
                ringerIds.insert(newObject);
            }
            if (handBells)
            {
                ++fieldNumber;
                ++handbellOffset;
            }
        }
        Duco::ObjectId strapperId = reader->GetInt16(32);
        if (strapperId != 0 && strapperId != -1)
        {
            strapperId = DatabaseImportUtil::CheckRingerAkaIds(strapperId, oldAKAs);
        }
        std::set<Duco::ObjectId> conductorIds;
        if (conductedType == ESingleConductor)
        {
            conductorIds.insert(reader->GetInt16(33));
        }
        std::list<std::wstring> multiConductorIds;
        if (conductedType == EJointlyConducted)
        {
            std::wstring conductorList;
            ReadString(reader, 34, conductorList);
            DucoEngineUtils::Tokenise<std::wstring>(conductorList, multiConductorIds);
        }
        std::wstring publicationVolume;
        if (ringingWorldPublicationId != -1)
        {
            ReadString(reader, 36, publicationVolume);
            if (publicationVolume.length() > 0)
            {
                unsigned int pageNumber (reader->GetInt32(38));
                if (pageNumber > 0 && pageNumber < USHRT_MAX)
                {
                    publicationVolume.append(L".");
                    if (pageNumber < 1000)
                        publicationVolume.append(L"0");
                    if (pageNumber < 100)
                        publicationVolume.append(L"0");
                    if (pageNumber < 10)
                        publicationVolume.append(L"0");
                    publicationVolume.append(DucoEngineUtils::ToString(pageNumber));
                }
            }
        }
        unsigned int dateOrder = reader->GetInt16(40);

        // Got all fields, create ringer
        map<unsigned int, Duco::ObjectId>::const_iterator socIt = societyIds.find(societyId);
        Duco::ObjectId newSocietyId;
        if (socIt != societyIds.end())
        {
            newSocietyId = socIt->second;
        }
        wstring composer (L"");
        if (composerId > 0)
        {
            composer = composers.find(composerId)->second;
        }
        std::map<unsigned int, Duco::ObjectId>::const_iterator ringIdIt = oldRingIdsToNewRingIds.find(ringId);
        std::map<unsigned int, unsigned int>::const_iterator towerIdIt = oldRingIdsToTowerIds.find(ringId);
        if (ringIdIt != oldRingIdsToNewRingIds.end() && towerIdIt != oldRingIdsToTowerIds.end())
        {
            Duco::ObjectId convertedRingId = ringIdIt->second;
            Duco::ObjectId towerId = towerIdIt->second;
            Peal peal(pealId, newSocietyId, towerId, convertedRingId, methodId, composer, footnote, performanceDate, performanceTime, changes,
                    handBells, ringerIds, conductedType, conductorIds, dateOrder);
            if (conductedType == EJointlyConducted)
            {
                std::list<std::wstring>::const_iterator condIt = multiConductorIds.begin();
                while (condIt != multiConductorIds.end())
                {
                    peal.SetConductorId(*condIt);
                    ++condIt;
                }
            }

            if (multimethodId > 0)
            {
                std::wstring multiMethodString = multiMethods.find(multimethodId)->second;
                peal.AddMultiMethods(0, multiMethodString);
            }
            if (ringingWorldPublicationId != -1 && publicationVolume.length() > 0)
            {
                peal.SetRingingWorldReference(publicationVolume);
            }
            if (strapperId.ValidId() && strapperId != 0)
            {
                unsigned int noOfBellsRung = peal.NoOfBellsRung(database->Database().TowersDatabase(), database->Database().MethodsDatabase()) - 1;
                Duco::ObjectId newRingId = FindRingOrCreate(towerId, convertedRingId, noOfBellsRung);
                peal.SetRingId(newRingId);
                peal.SetRingerId(strapperId, noOfBellsRung, true);
            }
            if (!database->AddPeal(peal).ValidId())
            {
                ++pealsIgnored;
            }
            conductorIds.clear();
        }
        else
        {
            ++pealsIgnored;

        }
        ReportProgress(DucoUI::WinRkImportProgressCallback::TImportStage::EImportingPeals, noOfRecords, ++count);
        if (containsQuarters && !containsPeals)
        {
            database->Settings().SetPealDatabase(false);
        }
    }
    reader->Close();
}

Duco::ObjectId
WinRKv3Import::FindRingOrCreate(const Duco::ObjectId& towerId, const Duco::ObjectId& ringId, unsigned int noOfBellsRung)
{
    Tower foundTower;
    if (database->FindTower(towerId, foundTower, true))
    {
        const Ring* const foundRing = foundTower.FindRing(ringId);
        if (foundRing != NULL)
        {
            ObjectId newRingId;
            if (foundTower.SuggestRing(noOfBellsRung, foundRing->TenorWeight(), L"", newRingId, false) && newRingId.ValidId() && newRingId != ringId)
            {
                return newRingId;
            }
        }
    }

    return ringId;
}

void
WinRKv3Import::ImportRingers(OdbcCommand^ command, System::ComponentModel::BackgroundWorker^ worker)
{
    command->CommandText = "SELECT COUNT(*) FROM Ringers";
    int noOfRecords = (int)command->ExecuteScalar();
    int count(0);
    
    ReportProgress(DucoUI::WinRkImportProgressCallback::TImportStage::EImportingRingers, noOfRecords, count);

    command->CommandText = "SELECT * FROM Ringers";
    OdbcDataReader^ reader = command->ExecuteReader();

    while (reader->Read() && !worker->CancellationPending)
    {
        int ringerId = reader->GetInt16(0);
        std::wstring ringerName;
        ReadString(reader, 2, ringerName);
        size_t seperator = ringerName.find(fieldSeperator);
        unsigned int spaceBetweenFields = 2;
        bool reverseFields = false;
        if (seperator == -1)
        {
            seperator = ringerName.find(altFieldSeperator);
            spaceBetweenFields = 1;
            reverseFields = true;
        }
        if (seperator == -1)
        {
            seperator = 0;
            spaceBetweenFields = 0;
        }

        std::wstring nextRingerSurname;
        std::wstring nextRingerForename;
        if (seperator != -1)
            {
                nextRingerSurname = ringerName.substr(0, seperator);
                if (ringerName.length() > seperator+2)
                {
                    nextRingerForename = ringerName.substr(seperator+spaceBetweenFields);
                }
            }
        else
        {
                nextRingerSurname = ringerName;
        }
        if (reverseFields)
        {
            std::wstring tempName = nextRingerForename;
            nextRingerForename = DucoEngineUtils::Trim(nextRingerSurname);
            nextRingerSurname = DucoEngineUtils::Trim(tempName);
        }
        else
        {
            nextRingerForename = DucoEngineUtils::Trim(nextRingerForename);
            nextRingerSurname = DucoEngineUtils::Trim(nextRingerSurname);
        }

        Ringer ringer(ringerId, nextRingerForename, nextRingerSurname);
        database->AddRinger(ringer);
        ReportProgress(DucoUI::WinRkImportProgressCallback::TImportStage::EImportingRingers, noOfRecords, ++count);
    }
    reader->Close();
}

void
WinRKv3Import::ImportRingerAkas(OdbcCommand^ command,
                            std::map<Duco::ObjectId, Duco::ObjectId>& oldAKAs,
                            std::map<unsigned int, unsigned int>& oldAKAsWithoutDates,
                            System::ComponentModel::BackgroundWorker^ worker)
{
    oldAKAsWithoutDates.clear();
    command->CommandText = "SELECT COUNT(*) FROM RingerAKAs";

    int noOfRecords = (int)command->ExecuteScalar();
    int count(0);
    ReportProgress(DucoUI::WinRkImportProgressCallback::TImportStage::EImportingRingerAkas, noOfRecords, count);

    command->CommandText = "SELECT * FROM RingerAKAs";
    OdbcDataReader^ reader = command->ExecuteReader();

    while (reader->Read() && !worker->CancellationPending)
    {
        int ringerId = reader->GetInt16(1);
        int ringerAKAId = reader->GetInt16(2);
        PerformanceDate dateChanged = ReadDateFromV3(reader,4);
        if (dateChanged.Valid())
        {
            Ringer ringer;
            Ringer ringerAka;
            if (database->FindRinger(ringerId, ringer, false) && database->FindRinger(ringerAKAId, ringerAka, false) && ringer != ringerAka)
            {
                Ringer newRinger (ringer);
                newRinger.AddNameChange(ringerAka, dateChanged);
                pair<unsigned int,unsigned int> newObject(ringerAKAId, ringerId);
                oldAKAs.insert(newObject);
                if (database->Database().RingersDatabase().UpdateObject(newRinger))
                {
                    database->Database().RingersDatabase().DeleteObject(ringerAKAId);
                }
            }
        }
        else
        {
            pair<unsigned int,unsigned int> newObject(ringerAKAId, ringerId);
            oldAKAsWithoutDates.insert(newObject);
        }
        ReportProgress(DucoUI::WinRkImportProgressCallback::TImportStage::EImportingRingerAkas, noOfRecords, ++count);
    }
    reader->Close();
}

void
WinRKv3Import::ImportTowers(OdbcCommand^ command, System::ComponentModel::BackgroundWorker^ worker)
{
    map<unsigned int, wstring> counties;
    ImportSingleStringFromTable(command, "Counties", counties, DucoUI::WinRkImportProgressCallback::TImportStage::EImportingCounties, worker);

    map<unsigned int, wstring> cities;
    ImportSingleStringFromTable(command, "Cities", cities, DucoUI::WinRkImportProgressCallback::TImportStage::EImportingCities, worker);

    command->CommandText = "SELECT COUNT(*) FROM Towers";
    int noOfRecords = (int)command->ExecuteScalar();
    int count(0);
    ReportProgress(DucoUI::WinRkImportProgressCallback::TImportStage::EImportingTowers, noOfRecords, count);

    command->CommandText = "SELECT * FROM Towers";
    OdbcDataReader^ reader = command->ExecuteReader();

    while (reader->Read() && !worker->CancellationPending)
    {
        unsigned int towerId = reader->GetInt16(0);
        wstring dedication;
        ReadString(reader, 1, dedication);
        unsigned int cityId = reader->GetInt16(2);
        unsigned int countyId = reader->GetInt16(3);
        unsigned int noOfBells = reader->GetInt16(4);
        wstring notes;
        ReadString(reader, 9, notes);

        wstring county (L"");
        if (countyId > 0 && countyId != -1)
        {
            const wstring& existingCounty = counties.find(countyId)->second;
            county = existingCounty;
        }
        wstring city = L"";
        if (cityId > 0 && cityId != -1)
        {
            city = cities.find(cityId)->second;
        }

        Tower tower(towerId, dedication, city, county, noOfBells);
        database->AddTower(tower);
        ReportProgress(DucoUI::WinRkImportProgressCallback::TImportStage::EImportingTowers, noOfRecords, ++count);
    }
    reader->Close();
}

void
WinRKv3Import::ImportRings(OdbcCommand^ command,
                         std::map<unsigned int, unsigned int>& oldRingIdsToTowerIds,
                         std::map<unsigned int, Duco::ObjectId>& oldRingIdsToNewRingIds,
                         System::ComponentModel::BackgroundWorker^ worker)
{
    map<unsigned int, wstring> tenors;
    ImportTenors(command, tenors, worker);

    command->CommandText = "SELECT COUNT(*) FROM Rings";
    int noOfRecords = (int)command->ExecuteScalar();
    int count(0);
    ReportProgress(DucoUI::WinRkImportProgressCallback::TImportStage::EImportingRings, noOfRecords, count);
    command->CommandText = "SELECT * FROM Rings";
    OdbcDataReader^ reader = command->ExecuteReader();

    while (reader->Read() && !worker->CancellationPending)
    {
        unsigned int id = reader->GetInt16(0);
        wstring name;
        ReadString(reader, 1, name);
        unsigned int towerId = reader->GetInt16(2);
        unsigned int tenorId = reader->GetInt16(3);
        unsigned int noOfBells (0);
        std::set<unsigned int> bells;
        for (int bellNo = 5; bellNo <= 20; ++bellNo)
        {
            unsigned int bellId = reader->GetInt16(bellNo);
            if (bellId > 0)
            {
                bells.insert(bellId);
                ++noOfBells;
            }
        }

        map<unsigned int, wstring>::const_iterator tenorIt = tenors.find(tenorId);
        wstring tenor = L"";
        if (tenorIt != tenors.end())
        {
            tenor = tenorIt->second;
        }
        Duco::Tower tower;
        if (database->FindTower(towerId, tower, false))
        {
            Tower updatedTower (tower);
            Duco::ObjectId newRingId = tower.NextFreeRingId();
            if (noOfBells != bells.size())
            {
                noOfBells = unsigned int(bells.size());
            }
            Ring ring(newRingId, noOfBells, name, bells, tenor, L"");
            updatedTower.AddRing(ring, true);

            if (database->Database().TowersDatabase().UpdateObject(updatedTower))
            {
                pair<unsigned int, unsigned int> newObject(id, towerId);
                oldRingIdsToTowerIds.insert(newObject);
                pair<unsigned int, Duco::ObjectId> newObject2(id, newRingId);
                oldRingIdsToNewRingIds.insert(newObject2);
            }
        }
        ReportProgress(DucoUI::WinRkImportProgressCallback::TImportStage::EImportingRings, noOfRecords, ++count);
    }
    tenors.clear();

    reader->Close();
}

void
WinRKv3Import::ImportTenors(OdbcCommand^ command, std::map<unsigned int,wstring>& tenors,
                            System::ComponentModel::BackgroundWorker^ worker)
{
    command->CommandText = "SELECT COUNT(*) FROM Tenors";
    int noOfRecords = (int)command->ExecuteScalar();
    int count(0);
    ReportProgress(DucoUI::WinRkImportProgressCallback::TImportStage::EImportingTenors, noOfRecords, count);
    command->CommandText = "SELECT * FROM Tenors";
    OdbcDataReader^ reader = command->ExecuteReader();

    while (reader->Read() && !worker->CancellationPending)
    {
        unsigned int tenorId = reader->GetInt16(0);
        unsigned int weight = reader->GetInt16(1);
        String^ weightStr = Convert::ToString(weight);
        String^ minorWeightStr = reader->GetString(2);
        if (minorWeightStr->Length >= 1)
        {
            if (Char::IsDigit(weightStr, weightStr->Length -1) &&
                Char::IsDigit(minorWeightStr, 0))
            {
                weightStr += "-";
            }

        }
        weightStr += minorWeightStr;
        
        std::wstring weightString;
        DucoUtils::ConvertString(weightStr, weightString);
        pair<int, wstring> newObject(tenorId, weightString);
        tenors.insert(newObject);
        ReportProgress(DucoUI::WinRkImportProgressCallback::TImportStage::EImportingTenors, noOfRecords, ++count);
    }
    reader->Close();
}

void
WinRKv3Import::ImportMethods(OdbcCommand^ command, const std::map<unsigned int, std::wstring>& extraOrders,
                             map<unsigned int, unsigned int>& oldOrderNamesToNew,
                             System::ComponentModel::BackgroundWorker^ worker)
{
    map<unsigned int,wstring> names;
    ImportSingleStringFromTable(command, "MethodNames", names, DucoUI::WinRkImportProgressCallback::TImportStage::EImportingMethodNames, worker);
    map<unsigned int,wstring> types;
    ImportSingleStringFromTable(command, "MethodTypes", types, DucoUI::WinRkImportProgressCallback::TImportStage::EImportingMethodTypes, worker);

    command->CommandText = "SELECT COUNT(*) FROM Methods";
    int noOfRecords = (int)command->ExecuteScalar();
    int count(0);
    ReportProgress(DucoUI::WinRkImportProgressCallback::TImportStage::EImportingMethods, noOfRecords, count);
    command->CommandText = "SELECT * FROM Methods";
    OdbcDataReader^ reader = command->ExecuteReader();

    while (reader->Read() && !worker->CancellationPending)
    {
        unsigned int methodId = reader->GetInt16(0);
        unsigned int nameId = reader->GetInt16(1);
        unsigned int typeId = reader->GetInt16(2);
        unsigned int noOfBellsId = reader->GetInt16(3);
        unsigned int noOfBells (-1);

        std::map<unsigned int, unsigned int>::const_iterator orderIt = oldOrderNamesToNew.find(noOfBellsId);
        if (orderIt == oldOrderNamesToNew.end())
        {
            noOfBells = noOfBellsId;
        }
        else
        {
            noOfBells = orderIt->second;
        }

        wstring notation(L"");
        wstring name (L"");
        wstring type (L"");
        if (nameId > 0)
            name = names.find(nameId)->second;
        if (typeId > 0)
            type = types.find(typeId)->second;

        std::map<unsigned int, std::wstring>::const_iterator it = extraOrders.find(noOfBells);
        bool dualOrder (false);
        if (it != extraOrders.end())
        {
            if (database->Database().MethodsDatabase().SplitOrderName(it->second, noOfBells))
            {
                dualOrder = true;
            }
            else
            {
                wstring fullMethodName (name);
                if (name.length() > 0 && type.length() > 0 && *name.rbegin() != ' ' && *type.begin() != ' ')
                {
                    fullMethodName += L" ";
                }
                fullMethodName += type;
                if (it->second.length() > 0 &&
                        fullMethodName.length() > 0 &&
                        *it->second.begin() != ' ' &&
                        *fullMethodName.rbegin() != ' ' && 
                        *fullMethodName.rbegin() != '(')
                {
                    fullMethodName += L" ";
                }
                fullMethodName += it->second;
                fullMethodName = DucoEngineUtils::Trim(fullMethodName);
                if (database->Database().MethodsDatabase().FindAndRemoveOrderName(noOfBells, fullMethodName, dualOrder))
                {
                    database->Database().MethodsDatabase().FindAndRemoveMethodType(type, fullMethodName);
                    name = fullMethodName;
                }
            }
        }
        Method method(methodId, noOfBells, name, type, notation, dualOrder);
        database->AddMethod(method);
        ReportProgress(DucoUI::WinRkImportProgressCallback::TImportStage::EImportingMethods, noOfRecords, ++count);
    }
    reader->Close();

    types.clear();
    names.clear();
}

void
WinRKv3Import::ImportOrderNames(OdbcCommand^ command, std::map<unsigned int, std::wstring>& extraOrders,
                                std::map<unsigned int, unsigned int>& oldOrderNamesToNew,
                                System::ComponentModel::BackgroundWorker^ worker)
{
    extraOrders.clear();
    command->CommandText = "SELECT COUNT(*) FROM MethodBells";
    int noOfRecords = (int)command->ExecuteScalar();
    int count(0);
    ReportProgress(DucoUI::WinRkImportProgressCallback::TImportStage::EImportingOrderNames, noOfRecords, count);
    command->CommandText = "SELECT * FROM MethodBells";
    OdbcDataReader^ reader = command->ExecuteReader();

    while (reader->Read() && !worker->CancellationPending)
    {
        unsigned int orderNameId = reader->GetInt16(0);
        String^ orderNameStr = reader->GetString(1);
        unsigned int newOrderNumber (0);
        bool dualOrder(false);
        if (database->FindOrderNumber(orderNameStr, newOrderNumber, dualOrder))
        {
            if (dualOrder)
            {
                std::wstring orderName;
                DucoUtils::ConvertString(orderNameStr, orderName);
                pair<unsigned int, std::wstring> newObject (orderNameId, orderName);
                extraOrders.insert(newObject);
            }
            else
            {
                pair<unsigned int, unsigned int> newObject (orderNameId, newOrderNumber);
                oldOrderNamesToNew.insert(newObject);
            }
        }
        else
        {
            std::wstring orderName;
            DucoUtils::ConvertString(orderNameStr, orderName);
            pair<unsigned int, std::wstring> newObject (orderNameId, orderName);
            extraOrders.insert(newObject);
        }
        ReportProgress(DucoUI::WinRkImportProgressCallback::TImportStage::EImportingOrderNames, noOfRecords, ++count);
    }
    reader->Close();
}

void
WinRKv3Import::RepairRingerAkasWithoutDates(const std::map<unsigned int, unsigned int>& oldAKAsWithoutDates,
                                           System::ComponentModel::BackgroundWorker^ worker)
{
    std::map<unsigned int, unsigned int>::const_iterator it = oldAKAsWithoutDates.begin();
    while (it != oldAKAsWithoutDates.end() && !worker->CancellationPending)
    {
        StatisticFilters filters(database->Database());
        filters.SetRinger(true, it->first);
        PealLengthInfo firstRingerInfo = database->Database().PealsDatabase().AllMatches(filters);
        filters.SetRinger(true, it->second);
        PealLengthInfo secondRingerInfo = database->Database().PealsDatabase().AllMatches(filters);


        if (firstRingerInfo.TotalPeals() > 0 && secondRingerInfo.TotalPeals() > 0)
        {
            if (firstRingerInfo.DateOfLastPeal() < firstRingerInfo.DateOfFirstPeal())
            {
                RepairRingerAkasWithoutDates(it->first, it->second, secondRingerInfo.DateOfLastPeal());
            }
            else if (secondRingerInfo.DateOfFirstPeal() < firstRingerInfo.DateOfFirstPeal())
            {
                RepairRingerAkasWithoutDates(it->second, it->first, firstRingerInfo.DateOfFirstPeal());
            }
        }
        ++it;
    }
}

bool
WinRKv3Import::RepairRingerAkasWithoutDates(unsigned int ringerId, unsigned int ringerIdToMergeAndDelete, const PerformanceDate& dateChanged)
{
    Ringer ringerToUpdate; 
    Ringer ringerToRemove;
    if (database->FindRinger(ringerIdToMergeAndDelete, ringerToRemove, false) && database->FindRinger(ringerId, ringerToUpdate, false))
    {
        Ringer updatedRinger (ringerToUpdate);
        updatedRinger.AddNameChange(ringerToRemove, dateChanged);
        if (database->Database().RingersDatabase().UpdateObject(updatedRinger))
        {
            database->DeleteRinger(ringerToRemove.Id(), false);
            return true;
        }
    }
    return false;
}

bool
WinRKv3Import::FindRingingWorldPublicationId(OdbcCommand^ command)
{
    command->CommandText = "SELECT * FROM Publications";
    OdbcDataReader^ reader = command->ExecuteReader();

    while (reader->Read() && ringingWorldPublicationId == -1)
    {
        System::String^ publicationName = ReadString(reader, 1)->ToLower();

        if (publicationName->Contains("ringing") && publicationName->Contains("world"))
        {
            ringingWorldPublicationId = reader->GetInt32(0);
        }
    }
    reader->Close();

    return ringingWorldPublicationId != -1;
}

double
WinRKv3Import::TotalNoOfStages()
{
    return (double)WinRkImportProgressCallback::TImportStage::ENumberOfStages;
}

double
WinRKv3Import::StageNumber(DucoUI::WinRkImportProgressCallback::TImportStage currentStage)
{
    return (double)currentStage-1;
}