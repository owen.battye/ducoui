#pragma once

namespace Duco
{
    class ObjectId;
    class Peal;
}

namespace DucoUI
{
    ref class DatabaseManager;
    ref class PealFeeList;

	/// <summary>
	/// Summary for PealFeesUI
	/// </summary>
	public ref class PealFeesUI : public System::Windows::Forms::Form
	{
	public:
		PealFeesUI(DucoUI::DatabaseManager^ theManager, Duco::ObjectId pealId);

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~PealFeesUI();
		!PealFeesUI();
        System::Void PealFeesUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void PealFeesUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e);

        void CreateRingersList();
        System::Void RingerObjectChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void SaveBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void CloseBtn_Click(System::Object^  sender, System::EventArgs^  e);

    private: System::Windows::Forms::Button^  closebtn;
    private: System::Windows::Forms::Button^  saveBtn;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container^ components;
        DucoUI::DatabaseManager^     database;
        PealFeeList^                       ringerList;
        Duco::Peal*                        currentPeal;

             bool                               dataChanged;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
            System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(PealFeesUI::typeid));
            this->closebtn = (gcnew System::Windows::Forms::Button());
            this->saveBtn = (gcnew System::Windows::Forms::Button());
            this->SuspendLayout();
            // 
            // closebtn
            // 
            this->closebtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
            this->closebtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            this->closebtn->Location = System::Drawing::Point(186, 52);
            this->closebtn->Margin = System::Windows::Forms::Padding(1);
            this->closebtn->Name = L"closebtn";
            this->closebtn->Size = System::Drawing::Size(75, 23);
            this->closebtn->TabIndex = 0;
            this->closebtn->Text = L"Close";
            this->closebtn->UseVisualStyleBackColor = true;
            this->closebtn->Click += gcnew System::EventHandler(this, &PealFeesUI::CloseBtn_Click);
            // 
            // saveBtn
            // 
            this->saveBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
            this->saveBtn->Location = System::Drawing::Point(109, 52);
            this->saveBtn->Margin = System::Windows::Forms::Padding(1);
            this->saveBtn->Name = L"saveBtn";
            this->saveBtn->Size = System::Drawing::Size(75, 23);
            this->saveBtn->TabIndex = 1;
            this->saveBtn->Text = L"Save";
            this->saveBtn->UseVisualStyleBackColor = true;
            this->saveBtn->Click += gcnew System::EventHandler(this, &PealFeesUI::SaveBtn_Click);
            // 
            // PealFeesUI
            // 
            this->AcceptButton = this->saveBtn;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->AutoScroll = true;
            this->AutoSize = true;
            this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            this->CancelButton = this->closebtn;
            this->ClientSize = System::Drawing::Size(271, 85);
            this->Controls->Add(this->saveBtn);
            this->Controls->Add(this->closebtn);
            this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
            this->MaximizeBox = false;
            this->MinimizeBox = false;
            this->Name = L"PealFeesUI";
            this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
            this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
            this->Text = L"Peal fees";
            this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &PealFeesUI::PealFeesUI_FormClosing);
            this->Load += gcnew System::EventHandler(this, &PealFeesUI::PealFeesUI_Load);
            this->ResumeLayout(false);

        }
#pragma endregion
    };
}
