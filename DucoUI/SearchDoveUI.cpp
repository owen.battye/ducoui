#include "DatabaseManager.h"
#include "GenericTablePrintUtil.h"
#include "DoveSearchWrapper.h"
#include <DoveSearch.h>

#include "SearchDoveUI.h"

#include <DucoConfiguration.h>
#include "SoundUtils.h"
#include "DucoTableSorter.h"
#include "DucoUtils.h"
#include "WindowsSettings.h"
#include "SystemDefaultIcon.h"
#include "DatabaseGenUtils.h"
#include <RingingDatabase.h>
#include <DoveObject.h>
#include <PealDatabase.h>
#include "DucoUIUtils.h"
#include <Tower.h>
#include <TowerDatabase.h>
#include <DatabaseSettings.h>

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::Collections;
using namespace System::Windows::Forms;

SearchDoveUI::SearchDoveUI(DucoUI::DatabaseManager^ theDatabase)
:   restart(false), database(theDatabase), populating(true)
{
    distanceTowerId = new Duco::ObjectId();
    callback = new DoveSearchWrapper(this);
    InitializeComponent();
    doveSearch = new Duco::DoveSearch(database->Database(), database->Config().DoveDataFile(), callback, database->WindowsSettings()->InstallationDir());
}

SearchDoveUI::!SearchDoveUI()
{
    delete distanceTowerId;
    delete callback;
    delete doveSearch;
}

SearchDoveUI::~SearchDoveUI()
{
    this->!SearchDoveUI();
    if (components)
    {
        delete components;
    }
}

System::Void
SearchDoveUI::SearchDoveUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    populating = true;
    unringableTowerStyle = (gcnew System::Windows::Forms::DataGridViewCellStyle(doveTowers->DefaultCellStyle));
    unringableTowerStyle->ForeColor = System::Drawing::SystemColors::GrayText;
    unringableTowerStyleNumbers = (gcnew System::Windows::Forms::DataGridViewCellStyle(doveTowers->DefaultCellStyle));
    unringableTowerStyleNumbers->ForeColor = System::Drawing::SystemColors::GrayText;
    unringableTowerStyleNumbers->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;

    allTowerNames = gcnew System::Collections::Generic::List<System::Int16>();
    Duco::ObjectId topTowerId;
    Duco::PealLengthInfo noOfPeals;
    database->Database().PealsDatabase().GetTopTower(topTowerId, noOfPeals, database->Database());
    DatabaseGenUtils::GenerateTowerOptions(distanceFrom, allTowerNames, database, topTowerId, false, true, true);

    size_t activeTowersWithoutDoveId = database->Database().TowersDatabase().NumberOfActiveTowersWithoutDoveId();
    if (activeTowersWithoutDoveId > 0)
        towerWarningsLbl->Text = "Warning: " + Convert::ToString(activeTowersWithoutDoveId)+ " existing towers without DoveId.";
    else
    {
        towerWarningsLbl->Text = "";
    }

    countLbl->Text = "";
    populating = false;
    System::String^ doveFileName = DucoUtils::ConvertString(database->Config().DoveDataFile());
    currentDoveDatabase->Text = doveFileName;
    if (System::IO::File::Exists(doveFileName))
    {
        openFileDialog1->InitialDirectory = System::IO::Path::GetDirectoryName(doveFileName);
        openFileDialog1->FileName = System::IO::Path::GetFileName(doveFileName);
        importBtn->Enabled = true;
        printBtn->Enabled = false;
        StartGeneration();
    }
    else
    {
        importBtn->Enabled = false;
        currentDoveDatabase->Text = "Previous file missing - please choose another";
    }
}

System::Void
SearchDoveUI::SearchDoveUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e)
{
    if (backgroundWorker1->IsBusy)
    {
        SoundUtils::PlayErrorSound();
        backgroundWorker1->CancelAsync();
        e->Cancel = true;
    }
}

System::Void
SearchDoveUI::valueChanged(System::Object^  sender, System::EventArgs^  e)
{
    StartGeneration();
}

System::Void
SearchDoveUI::ignoreTowersRung_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    allBells->Enabled = ignoreTowersRung->Checked;
    StartGeneration();
}

System::Void
SearchDoveUI::allBells_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    StartGeneration();
}

System::Void
SearchDoveUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void
SearchDoveUI::doveTowers_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
    DataGridViewColumn^ newColumn = doveTowers->Columns[e->ColumnIndex];
    SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;
    ResetAllProgrammaticGlyphs();
    if (newColumn->SortMode != DataGridViewColumnSortMode::Programmatic)
    {
        return;
    }

    if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
        newSortOrder = SortOrder::Ascending;
    else
        newSortOrder = SortOrder::Descending;

    newColumn->HeaderCell->SortGlyphDirection = newSortOrder;

    DucoTableSorter^ sorter = gcnew DucoTableSorter(newSortOrder == SortOrder::Ascending, EString);

    switch (e->ColumnIndex)
    {
    case 3: // Bells
        {
        sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
        bellsColumn->HeaderCell->SortGlyphDirection = newSortOrder;
        }
        break;
    case 4: // Tenor
        {
        sorter->AddSortObject(EString, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
        tenorColumn->HeaderCell->SortGlyphDirection = newSortOrder;
        }
        break;
    case 5: // Distance
        {
        sorter->AddSortObject(EDistance, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
        distanceColumn->HeaderCell->SortGlyphDirection = newSortOrder;
        }
        break;
    default:
        break;
    }
    sorter->AddSortObject(EString, true, 1);
    doveTowers->Sort(sorter);
}

System::Void
SearchDoveUI::ResetAllSortGlyphs()
{
    ResetAllProgrammaticGlyphs();
    importColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
    towerName->HeaderCell->SortGlyphDirection = SortOrder::None;
    doveIdColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
}

System::Void
SearchDoveUI::ResetAllProgrammaticGlyphs()
{
    bellsColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
    tenorColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
    distanceColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
}


System::Void
SearchDoveUI::openBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (backgroundWorker1->IsBusy)
    {
        SoundUtils::PlayErrorSound();
    }
    else
    {
        if (openFileDialog1->ShowDialog() == ::System::Windows::Forms::DialogResult::OK)
        {
            currentDoveDatabase->Text = openFileDialog1->FileName;
            std::string fileName;
            DucoUtils::ConvertString(openFileDialog1->FileName, fileName);
            database->Config().SetDoveDataFile(fileName);
            delete doveSearch;
            doveSearch = new Duco::DoveSearch(database->Database(), fileName, callback, database->WindowsSettings()->InstallationDir());
            importBtn->Enabled = true;
            StartGeneration();
        }
    }
}

System::Void
SearchDoveUI::importBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (backgroundWorker1->IsBusy)
    {
        SoundUtils::PlayErrorSound();
        return;
    }
    std::vector<std::wstring> selectedTowers;

    //Count number of towers chosen
    DataGridViewRowCollection^ allTowers = doveTowers->Rows;
    for (Int16 i(0); i < allTowers->Count; ++i)
    {
        DataGridViewRow^ row = allTowers[i];
        DataGridViewCheckBoxCell^ selected = static_cast<DataGridViewCheckBoxCell^>(row->Cells[0]);
        if (String::Compare(selected->Value->ToString(), "TRUE", StringComparison::CurrentCultureIgnoreCase) == 0)
        {
            String^ doveId = static_cast<String^>(row->Cells[6]->Value);
            std::wstring doveIdStr;
            DucoUtils::ConvertString(doveId, doveIdStr);
            selectedTowers.push_back(doveIdStr);
        }
    }

    UInt64 numberOfTowers = selectedTowers.size();
    if (numberOfTowers > 10 && MessageBox::Show(this, "Are you sure you want to import " + numberOfTowers.ToString() + " towers?",
        "Warning - Confirm Dove data import", MessageBoxButtons::YesNo, MessageBoxIcon::Warning) == ::DialogResult::No)
    {
        return;
    }

    // Start importing
    std::vector<std::wstring>::const_iterator it = selectedTowers.begin();
    while (it != selectedTowers.end())
    {
        std::set<Duco::DoveObject> doveTowers;
        if (doveSearch->FindTower(*it, doveTowers))
        {
            for (Duco::DoveObject doveTower : doveTowers)
            {
                database->AddTower(doveTower);
            }
        }
        ++it;
    }
    StartGeneration();
}

System::Void
SearchDoveUI::uncheckBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    DataGridViewRowCollection^ allTowers = doveTowers->Rows;
    for (Int16 i (0); i < allTowers->Count; ++i)
    {
        DataGridViewCheckBoxCell^ selected = static_cast<DataGridViewCheckBoxCell^>(allTowers[i]->Cells[0]);
        if (String::Compare(selected->Value->ToString(), "TRUE", StringComparison::CurrentCultureIgnoreCase) == 0)
        {
            selected->Value = "FALSE";
        }
    }
}

namespace DucoUI
{
    ref class SearchDoveUIArgs
    {
    public:
        System::String^ searchString;
        unsigned int    stage;
        bool            atLeastStage;
        bool            ignoreTowersAlreadyRung;
        bool            onlyIgnoreTowersWhereAllBellsRung;
        bool            ukOnly;
        bool            cathedrals;
        bool            ignoreUnringable;
        bool            antiClockwise;
    };
}

System::Void
SearchDoveUI::StartGeneration()
{
    if (populating)
    {
        return;
    }

    if (backgroundWorker1->IsBusy)
    {
        restart = true;
        backgroundWorker1->CancelAsync();
    }
    else
    {
        ResetAllSortGlyphs();
        countLbl->Text = "";
        doveTowers->Rows->Clear();
        importBtn->Enabled = false;
        openBtn->Enabled = false;
        closeBtn->Enabled = false;
        printBtn->Enabled = false;
        doveTowers->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::EnableResizing;
        doveTowers->ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode::EnableResizing;

        SearchDoveUIArgs^ args = gcnew SearchDoveUIArgs();
        args->searchString = searchString->Text;
        args->stage = Convert::ToInt16(noOfBells->Value);
        args->atLeastStage = atLeast->Checked;
        args->ignoreTowersAlreadyRung = ignoreTowersRung->Checked;
        args->onlyIgnoreTowersWhereAllBellsRung = allBells->Checked;
        args->ukOnly = ukOrIrelandOnly->Checked;
        args->cathedrals = cathedralsOnly->Checked;
        args->ignoreUnringable = unringable->Checked;
        args->antiClockwise = antiClockwise->Checked;
        (*distanceTowerId) = DatabaseGenUtils::FindId(allTowerNames, distanceFrom->SelectedIndex);
        restart = false;
        if (backgroundWorker1->IsBusy)
        {
            restart = true;
            backgroundWorker1->CancelAsync();
        }
        else
        {
            backgroundWorker1->RunWorkerAsync(args);
        }
    }
}

System::Void
SearchDoveUI::backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
    SearchDoveUIArgs^ args = static_cast<SearchDoveUIArgs^>(e->Argument);
    std::wstring searchString;
    DucoUtils::ConvertString(args->searchString, searchString);

    doveSearch->Search(searchString, args->stage, args->atLeastStage,
                       args->ignoreTowersAlreadyRung, args->onlyIgnoreTowersWhereAllBellsRung,
                       args->ukOnly, args->cathedrals, args->ignoreUnringable, args->antiClockwise);
}

System::Void
SearchDoveUI::backgroundWorker1_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    if (e->UserState != nullptr)
    {
        DataGridViewRow^ newRow = static_cast<DataGridViewRow^>(e->UserState);
        doveTowers->Rows->Add(newRow);
    }

    if (e->ProgressPercentage > 0)
        progress->Value = e->ProgressPercentage;
}

System::Void
SearchDoveUI::backgroundWorker1_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    progress->Value = 0;
    if (restart)
    {
        StartGeneration();
    }
    else
    {
        printBtn->Enabled = true;
        importBtn->Enabled = true;
        openBtn->Enabled = true;
        closeBtn->Enabled = true;
        doveTowers->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
        doveTowers->ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode::AutoSize;
        DucoTableSorter^ sorter = gcnew DucoTableSorter(true, EString);
        if (!distanceTowerId->ValidId())
        {
            towerName->HeaderCell->SortGlyphDirection = SortOrder::Ascending;
            sorter->AddSortObject(EString, true, 1);
        }
        else
        {
            distanceColumn->HeaderCell->SortGlyphDirection = SortOrder::Ascending;
            sorter->AddSortObject(EDistance, true, 5);
        }
        doveTowers->Sort(sorter);
        countLbl->Text = Convert::ToString(doveTowers->Rows->Count) + " towers.";
    }
}

void
SearchDoveUI::Initialised()
{
    backgroundWorker1->ReportProgress(0);
}

void
SearchDoveUI::Step(int progressPercent)
{
    backgroundWorker1->ReportProgress(progressPercent);
}

void
SearchDoveUI::Complete()
{

}

void
SearchDoveUI::TowerFound(const Duco::DoveObject& newTower)
{
    Duco::ObjectId existingTowerId = database->Database().TowersDatabase().FindTowerByDoveId(newTower.DoveId(), false);

    DataGridViewRow^ newRow = gcnew DataGridViewRow();
    DataGridViewCellCollection^ theCells = newRow->Cells;

    //importColumn;
    DataGridViewCheckBoxCell^ importCell = gcnew DataGridViewCheckBoxCell();
    theCells->Add(importCell);
    if (existingTowerId.ValidId())
    {
        importCell->Value = "FALSE";
        importCell->ReadOnly = true;
        newRow->HeaderCell->Value = DucoUtils::ConvertString(existingTowerId.Str());
    }
    else
    {
        importCell->Value = "TRUE";
        importCell->ReadOnly = false;
    }

    //nameColumn;
    DataGridViewTextBoxCell^ nameCell = gcnew DataGridViewTextBoxCell();
    nameCell->Value = DucoUtils::ConvertString(newTower.FullName());
    theCells->Add(nameCell);

    //countryColumn;
    DataGridViewTextBoxCell^ countryColumn = gcnew DataGridViewTextBoxCell();
    countryColumn->Value = DucoUtils::ConvertString(newTower.Country());
    theCells->Add(countryColumn);

    //bellsColumn;
    DataGridViewTextBoxCell^ noOfBellsCell = gcnew DataGridViewTextBoxCell();
    noOfBellsCell->Value = Convert::ToString(newTower.Bells());
    theCells->Add(noOfBellsCell);

    //tenorColumn;
    DataGridViewTextBoxCell^ tenorCell = gcnew DataGridViewTextBoxCell();
    tenorCell->Value = DucoUtils::ConvertString(newTower.Tenor());
    theCells->Add(tenorCell);

    //distanceColumn;
    DataGridViewTextBoxCell^ distanceCell = gcnew DataGridViewTextBoxCell();
    if (distanceTowerId->ValidId())
    {
        Duco::Tower distanceTower;
        if (database->FindTower(*distanceTowerId, distanceTower, false))
        {
            double distanceFrom (distanceTower.DistanceFrom(newTower));
            distanceCell->Value = distanceFrom.ToString("F3") + " km";
        }
    }
    theCells->Add(distanceCell);

    //doveIdColumn;
    DataGridViewTextBoxCell^ doveIdCell = gcnew DataGridViewTextBoxCell();
    doveIdCell->Value = DucoUtils::ConvertString(newTower.DoveId());
    theCells->Add(doveIdCell);

    if (newTower.Unringable())
    {
        nameCell->Style = unringableTowerStyle;
        countryColumn->Style = unringableTowerStyle;
        noOfBellsCell->Style = unringableTowerStyleNumbers;
        tenorCell->Style = unringableTowerStyle;
        distanceCell->Style = unringableTowerStyleNumbers;
        doveIdCell->Style = unringableTowerStyle;
    }

    backgroundWorker1->ReportProgress(0, newRow);
}

System::Void
SearchDoveUI::printBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    DucoUIUtils::Print(this, sender, gcnew System::Drawing::Printing::PrintPageEventHandler( this, &SearchDoveUI::printData ), "method circling", database->Database().Settings().UsePrintPreview(), false);
}

System::Void
SearchDoveUI::printData(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args)
{
    if (printUtil == nullptr)
    {
        printUtil = gcnew GenericTablePrintUtil(database, args, "Dove search results");
        printUtil->SetObjects(doveTowers, -1, TObjectType::ERinger, "", 2, 4);
    }

    System::Drawing::Printing::PrintDocument^ printDoc = static_cast<System::Drawing::Printing::PrintDocument^>(sender);
    printUtil->printObject(args, printDoc->PrinterSettings);
    if (!args->HasMorePages)
    {
        printUtil = nullptr;
    }
}
