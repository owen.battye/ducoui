﻿#include "RingingDatabaseObserver.h"
#include "DucoWindowState.h"
#include "DatabaseManager.h"
#include "OpenObjectCallback.h"
#include "PictureListControl.h"
#include "PictureControl.h"
#include <Ring.h>

#include "TowerUI.h"

#include "DucoUtils.h"
#include "DatabaseGenUtils.h"
#include "SystemDefaultIcon.h"
#include <Tower.h>
#include <RingingDatabase.h>
#include <DucoEngineUtils.h>
#include "InputIndexQueryUI.h"
#include <PealDatabase.h>
#include "SoundUtils.h"
#include <RingingDatabase.h>
#include <TowerDatabase.h>
#include "SingleObjectStatsUI.h"
#include "RenameBellsUI.h"
#include <algorithm>
#include "PictureEventArgs.h"
#include "DucoCommon.h"
#include <DucoConfiguration.h>

using namespace DucoUI;
using namespace Duco;
using namespace System;
using namespace System::Drawing;
using namespace System::Windows::Forms;
#define KDISABLEDRINGBELLSTOOLTIP L"Cannot change number of bells in this ring, as a peal already has already been rung on it"

TowerUI::TowerUI(DatabaseManager^ newDatabase, DucoUI::DucoWindowState newState)
    : database(newDatabase), state(newState), updatingNoOfBellsInTower(false),
      changingRing(false), newTowerRingNameChanged(false), newTowerRingChanged(false),
      currentTower(NULL), autoUpdatingRingName(false), resetLinkedTower(false)
{
    openWithRingId = new Duco::ObjectId();
    newPealUITowerId = new Duco::ObjectId();
    allTowerIds = gcnew System::Collections::Generic::List<System::Int16>();

    changingRing = true;
    InitializeComponent();
    InitializePictureControl();
    this->firstRungDate->MaxDate = DateTime::Today;
    this->firstRungDate->MinDate = *DucoUtils::FirstDate();
    this->numberOfBellsEditor->Maximum = database->HighestValidOrderNumber();
    this->numberOfBellsEditor->Minimum = database->LowestValidOrderNumber();
    changingRing = false;
    DatabaseGenUtils::GetAllPlaceOptions(countyEditor, townEditor, nameEditor, database);
}

TowerUI::TowerUI(DatabaseManager^ newDatabase, const Duco::ObjectId& towerId, const Duco::ObjectId& ringId)
:   database(newDatabase), state(DucoWindowState::EViewMode), autoUpdatingRingName(false), resetLinkedTower(false)
{
    openWithRingId = new Duco::ObjectId(ringId);
    newPealUITowerId = new Duco::ObjectId();
    allTowerIds = gcnew System::Collections::Generic::List<System::Int16>();

    changingRing = true;
    pictureCtrl = (gcnew DucoUI::PictureControl(database));
    InitializeComponent();
    InitializePictureControl();
    this->firstRungDate->MaxDate = DateTime::Today;
    this->firstRungDate->MinDate = *DucoUtils::FirstDate();
    this->numberOfBellsEditor->Maximum = database->HighestValidOrderNumber();
    this->numberOfBellsEditor->Minimum = database->LowestValidOrderNumber();
    changingRing = false;
    currentTower = new Tower();
    database->FindTower(towerId, *currentTower, false);
    DatabaseGenUtils::GetAllPlaceOptions(countyEditor, townEditor, nameEditor, database);
}

TowerUI::!TowerUI()
{
    delete openWithRingId;
    delete newPealUITowerId;
    delete currentTower;
}

TowerUI::~TowerUI()
{
    this->!TowerUI();
    if (components)
    {
        delete components;
    }
}

void
TowerUI::InitializePictureControl()
{
    pictureCtrl = (gcnew DucoUI::PictureControl(database));
    pictureCtrl->Dock = System::Windows::Forms::DockStyle::Fill;
    pictureCtrl->Location = System::Drawing::Point(39, 55);
    pictureCtrl->Name = L"pictureCtrl";
    pictureCtrl->Size = System::Drawing::Size(100, 100);
    pictureCtrl->TabIndex = 1;
    pictureCtrl->pictureEventHandler += gcnew System::EventHandler(this, &TowerUI::PictureEvent);
    pictureCtrl->DisableSelection();
    this->picturePage->Controls->Add(pictureCtrl);
}

Duco::ObjectId
TowerUI::CreateNewTower(DatabaseManager^ newDatabase, String^ newTowerName, String^ newRingName, String^ newTenorWeight, String^ newTenorKey, String^ newTowerbaseId, String^ newDoveId, unsigned int noOfBells, System::Boolean handbell)
{
    TowerUI^ ui = gcnew TowerUI(newDatabase, DucoWindowState::EAutoNewMode);
    std::wstring newTowerNameTemp;
    DucoUtils::ConvertString(newTowerName, newTowerNameTemp);
    ui->currentTower = new Tower();
    ui->currentTower->SetFullName(newTowerNameTemp);
    std::wstring newRingNameTemp;
    if (newRingName->Length == 0)
    {
        newRingNameTemp = Tower::DefaultRingName(noOfBells);
    }
    else
    {
        DucoUtils::ConvertString(newRingName, newRingNameTemp);
    }
    ui->currentTower->SetHandbell(handbell);
    ui->currentTower->SetNoOfBells(noOfBells);
    ui->currentTower->AddDefaultRing(newRingNameTemp);
    ui->ringTenorWeightEditor->Text = newTenorWeight;
    ui->ringTenorKeyEditor->Text = newTenorKey;
    ui->doveRef->Text = newDoveId;
    ui->towerbaseRef->Text = newTowerbaseId;
    ui->autoGeneratingFromPealDialog = true;
    ui->ShowDialog();

    Duco::ObjectId newTowerId = ui->CurrentTowerId();
    delete ui;
    return newTowerId;
}

System::Void
TowerUI::CloneTower(DatabaseManager^ newDatabase, const Duco::Tower& newTower, System::Windows::Forms::Form^ parent)
{
    TowerUI^ ui = gcnew TowerUI(newDatabase, DucoWindowState::EAutoNewMode);
    ui->currentTower = new Tower(newTower);
    ui->MdiParent = parent;
    ui->Show();
}

void 
TowerUI::ShowTower(DatabaseManager^ newDatabase, System::Windows::Forms::Form^ parent, const Duco::ObjectId& towerId, const Duco::ObjectId& ringId)
{
    TowerUI^ ui = gcnew TowerUI(newDatabase, towerId, ringId);
    ui->MdiParent = parent;
    ui->Show();
}

System::Void 
TowerUI::TowerUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    this->Icon = SystemDefaultIcon::DefaultSystemIcon();
    this->Closing += gcnew System::ComponentModel::CancelEventHandler(this, &TowerUI::ClosingEvent);
    database->AddObserver(this);
    DatabaseGenUtils::GenerateTowerOptions(linkedTower, allTowerIds, database, -1, false, false, true);

    bool viewTower = false;
    if (currentTower == NULL)
    {
        viewTower = true;
        currentTower = new Tower();
    }

    switch (state)
    {
    case DucoWindowState::EViewMode:
    default:
        {
            if (viewTower)
            {
                Duco::ObjectId towerId;
                if (database->FirstTower(towerId, false))
                {
                    PopulateViewWithId(towerId);
                }
            }
            else
            {
                PopulateView();
            }
            SetViewState();
        }
        break;
    case DucoWindowState::EAutoNewMode:
    case DucoWindowState::ENewMode:
        currentTower->CheckForDefaultRing();
        PopulateView();
        DucoWindowState oldState = state;
        SetEditState();
        if (state == DucoWindowState::EAutoNewMode)
        {
            dataChanged = true;
        }
        state = oldState;
        break;
    }
    SetControlColour(nameEditor);
    SetControlColour(townEditor);
    SetControlColour(countyEditor);
    SetControlColour(ringNameEditor);
    SetControlColour(ringTenorWeightEditor);
    SetControlColour(ringTenorKeyEditor);
    SetBellSelectorColour(0);
}

System::Void 
TowerUI::ClosingEvent(System::Object^ object, System::ComponentModel::CancelEventArgs^ e)
{
    if ( dataChanged )
    {
        if (DucoUtils::ConfirmLooseChanges("tower", this))
        {
            e->Cancel = true;
        }
    }
    if (!e->Cancel)
    {
        database->CancelEdit(TObjectType::ETower, currentTower->Id());
        database->RemoveObserver(this);
    }
}

System::Void
TowerUI::TowerUI_Activated(System::Object^  sender, System::EventArgs^  e)
{
    delayUpdating = false;
    if (databaseUpdated)
    {
        databaseUpdated = false;

        DatabaseGenUtils::GenerateTowerOptions(linkedTower, allTowerIds, database, -1, false, false, true);
        DatabaseGenUtils::GetAllPlaceOptions(countyEditor, townEditor, nameEditor, database);
        if (!InEditState())
            PopulateViewWithId(currentTower->Id());
    }
}

System::Void
TowerUI::TowerUI_Deactivate(System::Object^  sender, System::EventArgs^  e)
{
    delayUpdating = true;
}

const Duco::ObjectId&
TowerUI::CurrentTowerId()
{
    return *newPealUITowerId;
}

void 
TowerUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    if (type != TObjectType::ETower)
        return;
    if (delayUpdating)
    {
        databaseUpdated = true;
        return;
    }

    switch (eventId)
    {
    case EDeleted:
        UpdateIdLabel();
        DatabaseGenUtils::RemoveTowerOption(linkedTower, id, allTowerIds);
        DatabaseGenUtils::GetAllPlaceOptions(countyEditor, townEditor, nameEditor, database);
        break;

    case EAdded:
        UpdateIdLabel();
        DatabaseGenUtils::AddTowerOption(linkedTower, id, allTowerIds, database);
        DatabaseGenUtils::GetAllPlaceOptions(countyEditor, townEditor, nameEditor, database);
        break;

    case EUpdated:
        DatabaseGenUtils::UpdateTowerOption(linkedTower, id, allTowerIds, database);
        DatabaseGenUtils::GetAllPlaceOptions(countyEditor, townEditor, nameEditor, database);
        if (!InEditState())
        {
            PopulateView();
        }
        break;
    default:
        break;
    }
}

void 
TowerUI::SetViewState()
{
    tableLayoutPanel1->SuspendLayout();
    nameEditor->Enabled = false;
    townEditor->Enabled = false;
    countyEditor->Enabled = false;
    doveRef->Enabled = false;
    latitude->ReadOnly = true;
    longitude->ReadOnly = true;
    towerbaseRef->ReadOnly = true;
    openFelsteadBtn->Visible = true;
    openPealbaseBtn->Visible = true;
    notes->ReadOnly = true;
    aka->ReadOnly = true;
    firstRungDate->Enabled = false;
    numberOfBellsEditor->Enabled = false;

    numberOfBellsEditor->Minimum = database->LowestValidOrderNumber();
    ringNameEditor->ReadOnly = true;
    ringTenorWeightEditor->ReadOnly = true;
    ringTenorKeyEditor->ReadOnly = true;
    noOfBellsInRingEditor->Enabled = false;
    this->toolTip1->SetToolTip(this->noOfBellsInRingEditor, L"");
    bellsInRing->Enabled = false;
    ringSelector->Enabled = true;
    removed->Enabled = false;
    handbells->Enabled = false;
    linkedTower->Enabled = false;
    antiClockwise->Enabled = false;
    pictureCtrl->SetViewMode();

    addRingBtn->Visible = false;
    startBtn->Visible = true;
    back10Btn->Visible = true;
    previousBtn->Visible = true;
    nextBtn->Visible = true;
    forward10Btn->Visible = true;
    endBtn->Visible = true;
    saveBtn->Visible = false;
    cancelBtn->Visible = false;
    SwapBtn->Visible = false;
    editBtn->Visible = currentTower->Id() != -1;
    closeBtn->Visible = true;
    statsBtn->Visible = true;
    cloneBtn->Visible = true;
    renameBellsBtn->Visible = false;
    doveBtn->Visible = true;
    doveToolBarBtn->Visible = currentTower->DoveRefSet();
    mapBtn->Visible = true;
    openLinkedTowerBtn->Visible = currentTower->LinkedTowerId().ValidId();
    state = DucoWindowState::EViewMode;
    tableLayoutPanel1->ResumeLayout(true);

}

void 
TowerUI::SetEditState()
{
    tableLayoutPanel1->SuspendLayout();
    if (state != DucoWindowState::EAutoNewMode && state != DucoWindowState::ENewMode)
    {
        state = DucoWindowState::EEditMode;
    }
    nameEditor->Enabled = true;
    townEditor->Enabled = true;
    countyEditor->Enabled = true;
    doveRef->Enabled = true;
    latitude->ReadOnly = false;
    longitude->ReadOnly = false;
    towerbaseRef->ReadOnly = false;
    openFelsteadBtn->Visible = true;
    openPealbaseBtn->Visible = true;
    notes->ReadOnly = false;
    aka->ReadOnly = false;
    firstRungDate->Enabled = true;
    firstRungDate->Visible = true;
    firstRungLbl->Visible = true;
    numberOfBellsEditor->Enabled = true;
    linkedTower->Enabled = true;
    antiClockwise->Enabled = true;

    if (currentTower->NoOfRings() == 0)
    {
        bellsInRing->Enabled = false;
        ringSelector->Enabled = false;
        ringSelectorLabel->Visible = false;
        noOfBellsInRingEditor->Enabled = false;
        noOfBellsInRingEditor->Value = database->LowestValidOrderNumber();
        ringNameEditor->ReadOnly = true;
        ringTenorWeightEditor->ReadOnly = true;
        ringTenorKeyEditor->ReadOnly = true;
        toolTip1->SetToolTip(ringNameEditor, "No rings specified");
    }
    else
    {
        const Ring* const ring = currentTower->FindRingByIndex(ringSelector->Value);
        Duco::ObjectId ringId;
        if (ring != NULL)
        {
            ringId = ring->Id();
        }
        numberOfBellsEditor->Minimum = database->NumberOfBellsRungInTower(currentTower->Id());
        ringNameEditor->ReadOnly = false;
        ringTenorWeightEditor->ReadOnly = false;
        ringTenorKeyEditor->ReadOnly = false;
        bool ringNotUsed = database->NumberOfPealsRungOnRing(currentTower->Id(), ringId) == 0;
        if (!InEditState())
        {
            noOfBellsInRingEditor->Enabled = false;
            noOfBellsInRingEditor->ReadOnly = true;
            toolTip1->SetToolTip(ringNameEditor, KDISABLEDRINGBELLSTOOLTIP);
        }
        else if (ringNotUsed)
        {
            noOfBellsInRingEditor->Enabled = true;
            noOfBellsInRingEditor->ReadOnly = false;
            toolTip1->SetToolTip(ringNameEditor, L"");
        }
        else
        {
            noOfBellsInRingEditor->Enabled = false;
            noOfBellsInRingEditor->ReadOnly = true;
            toolTip1->SetToolTip(ringNameEditor, KDISABLEDRINGBELLSTOOLTIP);
        }
        bellsInRing->Enabled = true;
        ringSelector->Enabled = true;
        ringSelectorLabel->Visible = true;
    }
    removed->Enabled = true;
    handbells->Enabled = true;
    pictureCtrl->SetEditMode(currentTower->PictureId().ValidId());

    addRingBtn->Visible = true;
    startBtn->Visible = false;
    back10Btn->Visible = false;
    previousBtn->Visible = false;
    nextBtn->Visible = false;
    forward10Btn->Visible = false;
    endBtn->Visible = false;
    saveBtn->Visible = true;
    cancelBtn->Visible = true;
    editBtn->Visible = false;
    closeBtn->Visible = false;
    statsBtn->Visible = false;
    cloneBtn->Visible = false;
    renameBellsBtn->Visible = true;
    doveBtn->Visible = false;
    doveToolBarBtn->Visible = false;
    openLinkedTowerBtn->Visible = false;
    mapBtn->Visible = false;
    SwapBtn->Visible = true;

    if (InEditState())
    {
        nameEditor->BackColor = SystemColors::ControlLightLight;
        townEditor->BackColor = SystemColors::ControlLightLight;
        countyEditor->BackColor = SystemColors::ControlLightLight;
        ringNameEditor->BackColor = SystemColors::ControlLightLight;
        ringTenorWeightEditor->BackColor = SystemColors::ControlLightLight;
        bellsInRing->BackColor = SystemColors::ControlLightLight;
    }

    tableLayoutPanel1->ResumeLayout(true);
    dataChanged = false;
}

void 
TowerUI::ClearView()
{
    delete currentTower;
    currentTower = new Tower();
    nameEditor->Text = "";
    townEditor->Text = "";
    countyEditor->Text = "";
    notes->Text = "";
    aka->Text = "";
    firstRungDate->Value = DateTime::Today;
    doveRef->Text = "";
    towerbaseRef->Text = "";
    longitude->Text = "";
    latitude->Text = "";
    numberOfBellsEditor->Minimum = database->LowestValidOrderNumber();
    numberOfBellsEditor->Maximum = database->HighestValidOrderNumber();
    numberOfBellsEditor->Value = 10;
    ringSelector->Minimum = 0;
    ringSelector->Maximum = 0;
    ringSelector->Visible = false;
    ringSelectorLabel->Visible = false;
    ringSelector->Value = 0;
    ClearRing();
    linkedTower->Text = "";
    linkedTower->SelectedIndex = -1;
    removed->Checked = false;
    handbells->Checked = false;
    pictureCtrl->SetPicture(KNoId, false);
    antiClockwise->Checked = false;
    dataChanged = false;
}

void
TowerUI::ClearRing()
{
    ringNameEditor->Text = "";
    ringTenorWeightEditor->Text = "";
    ringTenorKeyEditor->Text = "";
    noOfBellsInRingEditor->Minimum = database->LowestValidOrderNumber();
    noOfBellsInRingEditor->Maximum = std::min(database->HighestValidOrderNumber(), DucoEngineUtils::MaxNumberOfBells());
    noOfBellsInRingEditor->Value = 10;
}

void 
TowerUI::PopulateViewWithId(const Duco::ObjectId& towerId)
{
    if (database->FindTower(towerId, *currentTower, false))
    {
        PopulateView();
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

void 
TowerUI::UpdateIdLabel()
{
    if (currentTower && currentTower->Id() != -1)
        towerIdLabel->Text = String::Format("{0:D}/{1:D}", currentTower->Id().Id(), database->NumberOfObjects(TObjectType::ETower));
    else
        towerIdLabel->Text = "New Tower";
}

void 
TowerUI::PopulateView()
{
    UpdateIdLabel();

    nameEditor->Text = DucoUtils::ConvertString(currentTower->Name());
    nameEditor->SelectionLength = 0;
    townEditor->Text = DucoUtils::ConvertString(currentTower->City());
    townEditor->SelectionLength = 0;
    countyEditor->Text = DucoUtils::ConvertString(currentTower->County());
    countyEditor->SelectionLength = 0;
    notes->Text = DucoUtils::ConvertString(currentTower->Notes());
    doveRef->Text = DucoUtils::ConvertString(currentTower->DoveRef());
    towerbaseRef->Text = DucoUtils::ConvertString(currentTower->TowerbaseId());
    longitude->Text = DucoUtils::PadWithChar(DucoUtils::ConvertString(currentTower->Longitude()), ' ', 3);
    latitude->Text = DucoUtils::PadWithChar(DucoUtils::ConvertString(currentTower->Latitude()), ' ', 3);
    unsigned int initialValueForNoOfBells = currentTower->Bells() < database->LowestValidOrderNumber() ? database->LowestValidOrderNumber() : currentTower->Bells();
    if (numberOfBellsEditor->Minimum > initialValueForNoOfBells)
        numberOfBellsEditor->Minimum = initialValueForNoOfBells;
    if (numberOfBellsEditor->Maximum < initialValueForNoOfBells)
        numberOfBellsEditor->Maximum = initialValueForNoOfBells;
    numberOfBellsEditor->Value = initialValueForNoOfBells;
    ringSelector->Minimum = 0;
    ringSelector->Maximum = currentTower->NoOfRings() - 1;
    ringSelector->Visible = currentTower->NoOfRings() > 1;
    ringSelectorLabel->Visible = currentTower->NoOfRings() > 1;
    bellsInRing->Items->Clear();
    removed->Checked = currentTower->Removed();
    handbells->Checked = currentTower->Handbell();
    antiClockwise->Checked = currentTower->AntiClockwise();
    if (currentTower->NoOfRings() <= 0)
    {
        ClearRing();
    }
    else if (openWithRingId->ValidId())
    {
        PopulateRing(*openWithRingId);
        openWithRingId->ClearId();
    }
    else
    {
        PopulateRing(0);
    }

    aka->Text = DucoUtils::ConvertString(currentTower->AlsoKnownAs());
    bool byPeal = false;
    if (currentTower->FirstRungDateSet(database->Database(), byPeal))
    {
        PerformanceDate earliestDate = currentTower->FirstRungDate();
        firstRungLbl->Visible = true;
        firstRungDate->Visible = true;
        firstRungDate->MaxDate = DateTime::Today;
        firstRungDate->Value = *DucoUtils::ConvertDate(earliestDate);
        firstRungDate->MaxDate = *DucoUtils::ConvertDate(earliestDate);
    }
    else
    {
        firstRungDate->MaxDate = DateTime::Today;
        firstRungDate->Value = DateTime::Today;
        firstRungDate->Visible = false;
        firstRungLbl->Visible = false;
    }
    Int32 newTowerIndex = DatabaseGenUtils::FindIndex(allTowerIds, currentTower->LinkedTowerId());
    linkedTower->SelectedIndex = newTowerIndex;
    if (newTowerIndex == -1)
    {
        linkedTower->Text = database->TowerName(currentTower->LinkedTowerId());
    }
    pictureCtrl->SetPicture(currentTower->PictureId(), false);
    doveToolBarBtn->Visible = currentTower->DoveRefSet();
    openLinkedTowerBtn->Visible = currentTower->LinkedTowerId().ValidId();
    std::set<Duco::ObjectId> linkedTowerIds;
    database->Database().TowersDatabase().GetLinkedIds(currentTower->Id(), linkedTowerIds);
    if (linkedTowerIds.size() == 1)
    {
        linkedTowersLbl->Text = "No linked towers";
    }
    else if (linkedTowerIds.size() == 2 && currentTower->LinkedTowerId().ValidId())
    {
        linkedTowersLbl->Text = "Only one tower link as above";
    }
    else
    {
        linkedTowersLbl->Text = "";
        for (const auto& it : linkedTowerIds)
        {
            if (it != currentTower->Id())
            {
                if (linkedTowersLbl->Text->Length > 0)
                {
                    linkedTowersLbl->Text += KNewlineChar;
                }
                linkedTowersLbl->Text += database->TowerName(it);
            }
        }
    }


    dataChanged = false;
}

void
TowerUI::PopulateRing(const Duco::Ring& theRing)
{
    changingRing = true;
    ringNameEditor->Text = DucoUtils::ConvertString(theRing.Name());
    ringTenorWeightEditor->Text = DucoUtils::ConvertString(theRing.TenorWeight());
    ringTenorKeyEditor->Text = DucoUtils::ConvertString(theRing.TenorKey());
    unsigned int lowestNumberOfBells = std::min(database->LowestValidOrderNumber(), currentTower->Bells());
    lowestNumberOfBells = std::min(lowestNumberOfBells, theRing.NoOfBells());

    bool ringNotUsed = database->NumberOfPealsRungOnRing(currentTower->Id(), theRing.Id()) == 0;
    noOfBellsInRingEditor->Minimum = lowestNumberOfBells;
    if (!InEditState())
    {
        noOfBellsInRingEditor->Enabled = false;
        noOfBellsInRingEditor->ReadOnly = true;
        toolTip1->SetToolTip(ringNameEditor, KDISABLEDRINGBELLSTOOLTIP);
    }
    else if (ringNotUsed)
    {
        noOfBellsInRingEditor->Enabled = true;
        noOfBellsInRingEditor->ReadOnly = false;
        toolTip1->SetToolTip(ringNameEditor, L"");
    }
    else
    {
        noOfBellsInRingEditor->Enabled = false;
        noOfBellsInRingEditor->ReadOnly = true;
        toolTip1->SetToolTip(ringNameEditor, KDISABLEDRINGBELLSTOOLTIP);
    }
    if (theRing.NoOfBells() > currentTower->Bells())
    {
        currentTower->SetNoOfBells(theRing.NoOfBells());
        numberOfBellsEditor->Value = currentTower->Bells();
        dataChanged = true;
    }
    noOfBellsInRingEditor->Maximum = database->HighestValidOrderNumber();
    noOfBellsInRingEditor->Value = theRing.NoOfBells();
    ringSelector->Value = currentTower->RingIndex(theRing.Id());

    SetBellsInRing(theRing);
    changingRing = false;
}

void
TowerUI::PopulateRing(const Duco::ObjectId& ringId)
{
    const Duco::Ring* theRing = currentTower->FindRing(ringId);
    if (theRing != NULL)
    {
        PopulateRing(*theRing);
    }
}

void 
TowerUI::PopulateRing(int ringIndex)
{
    const Ring* const ring = currentTower->FindRingByIndex(ringIndex);
    if (ring != NULL)
    {
        PopulateRing(*ring);
    }
}

void 
TowerUI::SetBellsInRing(const Ring& ring)
{
    bellsInRing->Items->Clear();
    unsigned int displayBellNumber = 0;
    unsigned int numberOfSpecialBells = 0;

    std::vector<std::wstring> allBellNames;
    currentTower->AllBellNames(allBellNames, KNoId);
    std::vector<std::wstring>::const_iterator it = allBellNames.begin();
    unsigned int i = 1;
    while (it != allBellNames.end())
    {
        CheckState state = CheckState::Unchecked;

        Duco::TRenameBellType bellType = currentTower->RenamedBell(i);
        if (ring.BellNumberInRing(i))
        {
            state = CheckState::Checked;
        }

        if (displayBellNumber == 1)
        {
            bellsInRing->Items->Add("Treble", state);
        }
        else if ((displayBellNumber + numberOfSpecialBells) == (currentTower->Bells()))
        {
            bellsInRing->Items->Add("Tenor", state);
        }
        else
        {
            bellsInRing->Items->Add(DucoUtils::ConvertString(*it), state);
        }
        ++i;
        ++it;
    }
    SetBellSelectorColour(0);
}

bool 
TowerUI::NextTower(bool forwards, Duco::ObjectId& towerId)
{
    Duco::ObjectId newTowerId = currentTower->Id();
    if (database->NextTower(newTowerId, forwards, false))
    {
        towerId = newTowerId;
        return true;
    }

    return false;
}

System::Void 
TowerUI::NextBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId towerId = currentTower->Id();
    if (NextTower(true, towerId))
    {
        PopulateViewWithId(towerId);
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void 
TowerUI::PreviousBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId towerId = currentTower->Id();
    if (NextTower(false, towerId))
    {
        PopulateViewWithId(towerId);
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void 
TowerUI::Back10Btn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId towerId = currentTower->Id();
    if (database->ForwardMultipleTowers(towerId, false))
    {
        PopulateViewWithId(towerId);
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void 
TowerUI::Forward10Btn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId towerId = currentTower->Id();
    if (database->ForwardMultipleTowers(towerId, true))
    {
        PopulateViewWithId(towerId);
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void 
TowerUI::EndBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId towerId;
    if (database->LastTower(towerId))
    {
        SetViewState();
        PopulateViewWithId(towerId);
    }
}

System::Void 
TowerUI::StartBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId towerId;
    if (database->FirstTower(towerId, false))
    {
        SetViewState();
        PopulateViewWithId(towerId);
    }
}

System::Void 
TowerUI::AddRingBtn_Click(System::Object^  sender, System::EventArgs^  e)
{ //owen
    ObjectId nextRingId = currentTower->NextFreeRingId();
    newTowerRingChanged = true;

    const std::set<unsigned int> bells;
    Ring newRing(nextRingId, currentTower->Bells(), L"", bells, L"", L"");
    currentTower->AddRing(newRing);
    ringSelector->Enabled = currentTower->NoOfRings() > 1;
    ringSelector->Visible = true;
    ringSelector->Maximum = currentTower->NoOfRings() -1;
    ringSelectorLabel->Visible = true;

    changingRing = true;
    PopulateRing(nextRingId);
    changingRing = false;

    // These are required if this is the first ring
    noOfBellsInRingEditor->Enabled = true;
    noOfBellsInRingEditor->ReadOnly = false;
    ringNameEditor->Enabled = true;
    ringNameEditor->ReadOnly = false;
    ringTenorWeightEditor->Enabled = true;
    ringTenorWeightEditor->ReadOnly = false;
    ringTenorKeyEditor->Enabled = true;
    ringTenorKeyEditor->ReadOnly = false;
    bellsInRing->Enabled = true;
}

System::Void 
TowerUI::EditBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->StartEdit(TObjectType::ETower, currentTower->Id()))
        SetEditState();
    else
        SoundUtils::PlayErrorSound();
}

System::Void 
TowerUI::CancelBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    switch (state)
    {
    default:
        if (dataChanged)
        {
            if (DucoUtils::ConfirmLooseChanges("tower", this))
            {
                return;
            }
            dataChanged = false;
            // set data changed to false, so the closing event will not prompt the user again
        }
        database->CancelEdit(TObjectType::ETower, currentTower->Id());
        SetViewState();
        PopulateViewWithId(currentTower->Id());
        break;
    case DucoWindowState::EAutoNewMode:
        if (dataChanged)
        {
            if (DucoUtils::ConfirmLooseChanges("tower", this))
            {
                Close();
                return;
            }
            dataChanged = false;
            // set data changed to false, so the closing event will not prompt the user again
        }
    case DucoWindowState::ENewMode:
        Close();
        break;
    }
}

System::Void 
TowerUI::SaveBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (autoGeneratingFromPealDialog && !dataChanged)
        dataChanged = true;
    if (dataChanged)
        currentTower->SetAllRingsMaxBellCount();

    if (!currentTower->Valid(database->Database(), false, true))
    {
        Duco::ObjectId ringId (ringSelector->Value);
        if (currentTower->FirstInvalidRingId(ringId) && ringSelector->Value != ringId.Id())
        {
            changingRing = true;
            ringsTab->Visible = true;
            PopulateRing(ringId);
            changingRing = false;
        }
        if (MessageBox::Show(this, DucoUtils::ConvertString(currentTower->ErrorString(database->Database().Settings(), database->Config().IncludeWarningsInValidation())),
            "Tower data incomplete - Cancel", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == ::DialogResult::Yes )
        {
            return;
        }
    }

    if (dataChanged)
    {
        switch (state)
        {
        default:
            if (database->UpdateTower(*currentTower, true))
            {
                dataChanged = false;
                SetViewState();
                PopulateView();
            }
            break;
        case DucoWindowState::ENewMode:
        case DucoWindowState::EAutoNewMode:
            Duco::ObjectId newTowerId = database->AddNewTower(*currentTower);
            if (!newTowerId.ValidId())
            {
                return;
            }
            *newPealUITowerId = newTowerId;
            dataChanged = false;
            if (state == DucoWindowState::EAutoNewMode)
            {
                state = DucoWindowState::EViewMode;
                Close();
            }
            else
            {
                PopulateViewWithId(newTowerId);
                SetViewState();
            }
            break;
        }
    }
}

System::Void
TowerUI::DoveBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (currentTower->DoveRef().length() > 0)
    {
        String^ fullUrl = DucoUtils::ConvertString(currentTower->DoveUrl(database->Settings()));
        try
        {
            System::Diagnostics::Process::Start(fullUrl);
        }
        catch (System::ComponentModel::Win32Exception^ noBrowserEx) 
        {
            SoundUtils::PlayErrorSound();
            System::Windows::Forms::MessageBox::Show(this, noBrowserEx->Message);
        }
        catch (System::Exception^ otherEx)
        {
            SoundUtils::PlayErrorSound();
            System::Windows::Forms::MessageBox::Show(this, otherEx->Message);
        } 
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
TowerUI::openPealbasebtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (state == DucoWindowState::EViewMode && currentTower->ValidTowerbaseId())
    {
        String^ fullUrl = DucoUtils::ConvertString(database->Settings().PealbaseTowerURL());
        fullUrl += DucoUtils::ConvertString(currentTower->TowerbaseId());
        try
        {
            System::Diagnostics::Process::Start(fullUrl);
        }
        catch (System::ComponentModel::Win32Exception^ noBrowserEx) 
        {
            SoundUtils::PlayErrorSound();
            System::Windows::Forms::MessageBox::Show(this, noBrowserEx->Message);
        }
        catch (System::Exception^ otherEx)
        {
            SoundUtils::PlayErrorSound();
            System::Windows::Forms::MessageBox::Show(this, otherEx->Message);
        } 
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
TowerUI::openFelsteadBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::EViewMode && currentTower->ValidTowerbaseId())
    {
        String^ fullUrl = DucoUtils::ConvertString(database->Settings().FelsteadTowerURL());
        fullUrl += DucoUtils::ConvertString(currentTower->TowerbaseId());
        try
        {
            System::Diagnostics::Process::Start(fullUrl);
        }
        catch (System::ComponentModel::Win32Exception^ noBrowserEx) 
        {
            SoundUtils::PlayErrorSound();
            System::Windows::Forms::MessageBox::Show(this, noBrowserEx->Message);
        }
        catch (System::Exception^ otherEx)
        {
            SoundUtils::PlayErrorSound();
            System::Windows::Forms::MessageBox::Show(this, otherEx->Message);
        } 
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
TowerUI::MapBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->Settings().GoogleURL().length() > 0 && currentTower->Latitude().length() > 0 && currentTower->Longitude().length() > 0)
    {
        
        String^ fullUrl = DucoUtils::ConvertString(database->Settings().GoogleURL());
        fullUrl += DucoUtils::ConvertString(currentTower->Latitude());
        fullUrl += ",";
        fullUrl += DucoUtils::ConvertString(currentTower->Longitude());
        try
        {
            System::Diagnostics::Process::Start(fullUrl);
        }
        catch (System::ComponentModel::Win32Exception^ noBrowserEx) 
        {
            SoundUtils::PlayErrorSound();
            System::Windows::Forms::MessageBox::Show(this, noBrowserEx->Message);
        }
        catch (System::Exception^ otherEx)
        {
            SoundUtils::PlayErrorSound();
            System::Windows::Forms::MessageBox::Show(this, otherEx->Message);
        } 
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void 
TowerUI::CloseBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void
TowerUI::openLinkedTowerBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (currentTower->LinkedTowerId().ValidId())
    {
        TowerUI::ShowTower(database, MdiParent, currentTower->LinkedTowerId(), KNoId);
    }
}

System::Void 
TowerUI::NameEditor_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (InEditState())
    {
        std::wstring newName;
        DucoUtils::ConvertString(nameEditor->Text, newName);
        if (newName.compare(currentTower->Name()) != 0)
        {
            currentTower->SetName(newName);
            dataChanged = true;
        }
    }
    SetControlColour(nameEditor);
}

System::Void
TowerUI::TownEditor_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (InEditState())
    {
        std::wstring newTown;
        DucoUtils::ConvertString(townEditor->Text, newTown);
        currentTower->SetCity(newTown);
        dataChanged = true;
    }
    SetControlColour(townEditor);
}

System::Void 
TowerUI::CountyEditor_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (InEditState())
    {
        std::wstring newCounty;
        DucoUtils::ConvertString(countyEditor->Text, newCounty);
        currentTower->SetCounty(newCounty);
        dataChanged = true;
    }
    SetControlColour(countyEditor);
}

System::Void
TowerUI::SetControlColour(System::Windows::Forms::Control^ cntrl)
{
    if (cntrl->Text->Length == 0)
        cntrl->BackColor = KErrorColour;
    else
        cntrl->BackColor = SystemColors::ControlLightLight;
}

System::Void
TowerUI::SetBellSelectorColour(int noToAdd)
{
    int noOfSelectedBells = bellsInRing->CheckedItems->Count + noToAdd;

    int noOfBellsInRing = Convert::ToInt16(noOfBellsInRingEditor->Value);
    if (noOfSelectedBells != noOfBellsInRing)
        bellsInRing->BackColor = KErrorColour;
    else
        bellsInRing->BackColor = SystemColors::ControlLightLight;
}

System::Void 
TowerUI::RingSelector_Scroll(System::Object^  sender, System::EventArgs^  e)
{
    changingRing = true;
    PopulateRing(ringSelector->Value);
    changingRing = false;
    newTowerRingChanged = true;
}

System::Void
TowerUI::RingNameEditor_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (InEditState() && !changingRing && !(updatingNoOfBellsInTower && state == DucoWindowState::ENewMode))
    {
        if (!autoUpdatingRingName)
            newTowerRingNameChanged = true;
        const Ring* const ring = currentTower->FindRingByIndex(ringSelector->Value);
        if (ring != NULL)
        {
            std::wstring newName;
            DucoUtils::ConvertString(ringNameEditor->Text, newName);
            Ring updatedRing (*ring);
            updatedRing.SetName(newName);
            if (currentTower->UpdateRing(updatedRing.Id(), updatedRing))
            {
                SetControlColour(ringNameEditor);
                dataChanged = true;
            }
        }
        newTowerRingChanged = true;
    }
    SetControlColour(ringNameEditor);
}

System::Void
TowerUI::RingTenorWeightEditor_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (InEditState() && !changingRing)
    {
        newTowerRingChanged = true;
        const Ring* const ring = currentTower->FindRingByIndex(ringSelector->Value);
        if (ring != NULL)
        {
            std::wstring tempWeight;
            DucoUtils::ConvertString(ringTenorWeightEditor->Text, tempWeight);
            Ring updatedRing (*ring);
            updatedRing.SetTenorWeight(tempWeight);
            if (currentTower->UpdateRing(updatedRing.Id(), updatedRing))
            {
                dataChanged = true;
            }
        }
    }
    SetControlColour(ringTenorWeightEditor);
}

System::Void
TowerUI::RingTenorKeyEditor_TextChanged(System::Object^ sender, System::EventArgs^ e)
{
    if (InEditState() && !changingRing)
    {
        newTowerRingChanged = true;
        const Ring* const ring = currentTower->FindRingByIndex(ringSelector->Value);
        if (ring != NULL)
        {
            std::wstring tempKey;
            DucoUtils::ConvertString(ringTenorKeyEditor->Text, tempKey);
            Ring updatedRing(*ring);
            updatedRing.SetTenorKey(tempKey);
            if (currentTower->UpdateRing(updatedRing.Id(), updatedRing))
            {
                dataChanged = true;
            }
        }
    }
    SetControlColour(ringTenorKeyEditor);
}

System::Void 
TowerUI::NumberOfBellsEditor_ValueChanged(System::Object^  sender, System::EventArgs^  e)
{
    bool forceChangeInNewTower (false);
    if ((InEditState() && !changingRing))
    {
        if (state == DucoWindowState::ENewMode)
        {
            if (!newTowerRingChanged)
            {
                if (!currentTower->ReduceNoOfBellsInNewTower(Convert::ToInt16(numberOfBellsEditor->Value)))
                {
                    SoundUtils::PlayErrorSound();
                    if (numberOfBellsEditor->Value < currentTower->Bells())
                    {
                        numberOfBellsEditor->Value = currentTower->Bells();
                    }
                    return;
                }
                forceChangeInNewTower = true;
            }

        }
        if (!forceChangeInNewTower && !currentTower->SetNoOfBells(Convert::ToInt16(numberOfBellsEditor->Value)))
        {
            SoundUtils::PlayErrorSound();
            if (numberOfBellsEditor->Value < currentTower->Bells())
            {
                numberOfBellsEditor->Value = currentTower->Bells();
            }
            return;
        }
        updatingNoOfBellsInTower = true;
    }

    if (forceChangeInNewTower)
    {
        changingRing = true;
        noOfBellsInRingEditor->Maximum = std::min(currentTower->Bells(), database->HighestValidOrderNumber());
        PopulateRing(0);
        changingRing = false;
        updatingNoOfBellsInTower = false;
    }
    else if (InEditState() && !changingRing)
    {
        noOfBellsInRingEditor->Maximum = std::min(currentTower->Bells(), database->HighestValidOrderNumber());
        const Ring* const ring = currentTower->FindRingByIndex(ringSelector->Value);
        if (ring != NULL)
        {
            SetBellsInRing(*ring);
            dataChanged = true;
        }
        updatingNoOfBellsInTower = false;
    }
}

System::Void 
TowerUI::BellsInRing_ValueChanged(System::Object^  sender, System::Windows::Forms::ItemCheckEventArgs^  e)
{
    if (InEditState() && !updatingNoOfBellsInTower && !changingRing)
    {
        newTowerRingChanged = true;
        const Ring* const ring = currentTower->FindRingByIndex(ringSelector->Value);
        if (ring != NULL)
        {
            Ring updatedRing (*ring);
            updatedRing.ChangeBell(e->Index+1, e->NewValue == System::Windows::Forms::CheckState::Checked);
            if (currentTower->UpdateRing(updatedRing.Id(), updatedRing))
            {
                SetBellSelectorColour(e->NewValue == CheckState::Checked ? 1 : -1);
                dataChanged = true;
            }
        }
    }
}

System::Void
TowerUI::NoOfBellsInRingEditor_ValueChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (InEditState() && !changingRing)
    {
        newTowerRingChanged = true;
        const Ring* const ring = currentTower->FindRingByIndex(ringSelector->Value);
        if (ring != NULL)
        {
            unsigned int noOfBellsInRing = Convert::ToInt16(noOfBellsInRingEditor->Value);
            Ring updatedRing (*ring);
            updatedRing.SetNoOfBells(noOfBellsInRing);
            if (currentTower->UpdateRing(updatedRing.Id(), updatedRing))
            {
                dataChanged = true;
                SetBellsInRing(updatedRing);

                if (!newTowerRingNameChanged && (state == DucoWindowState::ENewMode || state == DucoWindowState::EAutoNewMode) )
                {
                    autoUpdatingRingName = true;
                    std::wstring newRingName = DucoEngineUtils::ToString(Convert::ToInt16(noOfBellsInRingEditor->Value));
                    newRingName.append(KRingNameSuffix);
                    ringNameEditor->Text = DucoUtils::ConvertString(newRingName);
                    autoUpdatingRingName = false;
                }
                if (noOfBellsInRing > currentTower->Bells())
                {
                    numberOfBellsEditor->Value = noOfBellsInRing;
                }
            }
        }
    }
    SetBellSelectorColour(0);
}

System::Void
TowerUI::antiClockwise_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
{
    if (InEditState())
    {
        currentTower->SetAntiClockwise(antiClockwise->Checked);
        dataChanged = true;

    }
}

System::Void
TowerUI::linkedTower_SelectedValueChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (resetLinkedTower)
    {
        return;
    }
    if (InEditState())
    {
        Duco::ObjectId newTowerId = DatabaseGenUtils::FindId(allTowerIds, linkedTower->SelectedIndex);
        if (newTowerId != currentTower->Id() && !resetLinkedTower)
        {
            if (newTowerId.ValidId())
            {
                std::wstring errorString;
                if (database->Database().TowersDatabase().CheckLinkedTowerId(currentTower->Id(), newTowerId, errorString))
                {
                    currentTower->SetLinkedTowerId(newTowerId);
                    dataChanged = true;
                }
                else
                {
                    resetLinkedTower = true;
                    MessageBox::Show(this, DucoUtils::ConvertString(errorString), "Cannot link those towers");
                }
            }
            else
            {
                currentTower->SetLinkedTowerId(KNoId);
                dataChanged = true;
            }
        }
        else if (newTowerId != currentTower->LinkedTowerId())
        {
            resetLinkedTower = true;
        }
        if (resetLinkedTower)
        {
            currentTower->SetLinkedTowerId(KNoId);
            linkedTower->Text = "";
            linkedTower->SelectedIndex = -1;
            resetLinkedTower = false;
        }
    }
}

System::Void
TowerUI::Removed_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (InEditState())
    {
        currentTower->SetRemoved(removed->Checked);
        dataChanged = true;
    }
}

System::Void
TowerUI::handbells_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
{
    if (InEditState())
    {
        currentTower->SetHandbell(handbells->Checked);
        dataChanged = true;
    }
}

System::Void 
TowerUI::TowerIdLabel_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::EViewMode)
    {
        System::Int16 id = -1;
        InputIndexQueryUI^ findIdDialog = gcnew InputIndexQueryUI("tower", id);
        System::Windows::Forms::DialogResult returnVal = findIdDialog->ShowDialog();
        if (returnVal == ::DialogResult::OK)
        {
            PopulateViewWithId(id);
        }
    }
}

System::Void
TowerUI::StatsBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    SingleObjectStatsUI^ statsUI = gcnew SingleObjectStatsUI(database, currentTower->Id(), TObjectType::ETower);
    statsUI->MdiParent = this->MdiParent;
    statsUI->Show();
}

System::Void 
TowerUI::cloneBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    Tower newTower = Tower(KNoId, *currentTower);
    newTower.SetName(currentTower->Name() + L" (Old bells)");
    newTower.SetLinkedTowerId(currentTower->Id());
    newTower.SetRemoved(true);
    TowerUI::CloneTower(database, newTower, this->MdiParent);
}

System::Void
TowerUI::RenameBellsBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    bool changesMade = false;
    RenameBellsUI^ bellsUI = gcnew RenameBellsUI(database, *currentTower, changesMade);
    bellsUI->ShowDialog();
    if (changesMade)
    {
        PopulateView();
        dataChanged = true;
    }
    delete bellsUI;
}

System::Void
TowerUI::SwapBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    String^ temp = latitude->Text;
    latitude->Text = longitude->Text;
    longitude->Text = temp;
    dataChanged = true;
}

System::Void
TowerUI::DoveRef_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (InEditState())
    {
        std::wstring newDoveRef;
        DucoUtils::ConvertString(doveRef->Text, newDoveRef);
        currentTower->SetDoveRef(newDoveRef);
        dataChanged = true;
    }
}

System::Void
TowerUI::Longitude_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (currentTower != NULL && InEditState())
    {
        std::wstring newPosition;
        DucoUtils::ConvertString(longitude->Text, newPosition);
        currentTower->SetLongitude(DucoEngineUtils::RemoveAllButValidChars(newPosition, L"0123456789.-+"));
        dataChanged = true;
    }
}

System::Void
TowerUI::Latitude_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (currentTower != NULL && InEditState())
    {
        std::wstring newPosition;
        DucoUtils::ConvertString(latitude->Text, newPosition);
        currentTower->SetLatitude(DucoEngineUtils::RemoveAllButValidChars(newPosition, L"0123456789.-+"));
        dataChanged = true;
    }
}

System::Void
TowerUI::TowerBaseRef_TextChanged(System::Object^ sender, System::EventArgs^ e)
{
    if (InEditState())
    {
        std::wstring newRef;
        DucoUtils::ConvertString(towerbaseRef->Text, newRef);
        currentTower->SetTowerbaseId(newRef);
        dataChanged = true;
    }
}

System::Void
TowerUI::Aka_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (InEditState())
    {
        std::wstring newAka;
        DucoUtils::ConvertString(aka->Text, newAka);
        currentTower->SetAlsoKnownAs(newAka);
        dataChanged = true;
    }
}

System::Void
TowerUI::FirstRungDate_ValueChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (InEditState() && currentTower != NULL)
    {
        currentTower->SetFirstRungDate(DucoUtils::ConvertDate(firstRungDate->Value));
        dataChanged = true;
    }
}

System::Void
TowerUI::Notes_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (InEditState())
    {
        std::wstring newNotes;
        DucoUtils::ConvertString(notes->Text, newNotes);
        currentTower->SetNotes(newNotes);
        dataChanged = true;
    }
}

System::Void
TowerUI::Notes_LinkClicked(System::Object^  sender, System::Windows::Forms::LinkClickedEventArgs^  e)
{
    System::Diagnostics::Process::Start(e->LinkText);
}

Boolean
TowerUI::InEditState()
{
    switch (state)
    {
    case DucoWindowState::EViewMode:
        return false;
    case DucoWindowState::EEditMode:
    case DucoWindowState::ENewMode:
    case DucoWindowState::EAutoNewMode:
    default:
        return true;
    }
}

System::Void
TowerUI::PictureEvent(System::Object^  sender, System::EventArgs^  e)
{
    DucoUI::PictureEventArgs^  args = static_cast<DucoUI::PictureEventArgs^>(e);

    if (args->pictureId != NULL)
    {
        currentTower->SetPictureId(*args->pictureId);
    }
    else
    {
        currentTower->SetPictureId(KNoId);
    }
    pictureCtrl->SetPicture(currentTower->PictureId(), false);
    dataChanged = true;
}
