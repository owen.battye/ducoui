#include "DatabaseManager.h"
#include "CheckLatestVersion.h"
#include "DucoUtils.h"
#include "DownloadNewVersionUI.h"
#include <DucoVersionNumber.h>
#include "DucoCommon.h"
#include <RingingDatabase.h>
#include <DucoConfiguration.h>

using namespace Duco;
using namespace DucoUI;
using namespace System::IO;
using namespace System;
using namespace System::Net;
using namespace System::Net::Http;
using namespace System::Threading::Tasks;
using namespace System::Windows::Forms;

#ifdef _DEBUG
#define KOnlineVersionFile "/latestversiond.txt"
#else
#define KOnlineVersionFile "/latestversion.txt"
#endif

#ifdef _X64
#define KOnlineDownloadFilePrefix "DucoSetup_x64_v"
#else
#define KOnlineDownloadFilePrefix "DucoSetup_v"
#endif


CheckLatestVersion::CheckLatestVersion(System::Windows::Forms::Form^ newOwner, DucoUI::DatabaseManager^ theDatabase, System::Boolean newUserForced)
    : owner(newOwner), database(theDatabase), showingUpgradeDialog(false), userForced(newUserForced)
{
    InitializeComponent();
    downloadingInstaller = false;
    downloadingVersion = true;
    System::Uri^ fileLocation = gcnew System::Uri("http://" + KWebSite + KOnlineVersionFile);
    versionTemporaryfile = Path::GetTempFileName();
    client->DownloadFileAsync(fileLocation, versionTemporaryfile);
}

CheckLatestVersion::CheckLatestVersion(System::ComponentModel::IContainer ^container,
                                       System::Windows::Forms::Form^ newOwner,
    DucoUI::DatabaseManager^ theDatabase)
    : owner(newOwner), database(theDatabase), showingUpgradeDialog(true)
{
    downloadingInstaller = false;
    downloadingVersion = true;
    container->Add(this);
    InitializeComponent();
    System::Uri^ fileLocation = gcnew System::Uri("http://" + KWebSite + KOnlineVersionFile);
    versionTemporaryfile = Path::GetTempFileName();
    client->DownloadFileAsync(fileLocation, versionTemporaryfile);
}

CheckLatestVersion::~CheckLatestVersion()
{
    client->CancelAsync();
    if (components)
    {
        delete components;
    }
}

System::Boolean
CheckLatestVersion::ShowingUpgradeDialog()
{
    return showingUpgradeDialog;
}

System::Void
CheckLatestVersion::downloadedVersion(System::ComponentModel::AsyncCompletedEventArgs^  e)
{
    if (!e->Cancelled  && e->Error == nullptr && !System::String::IsNullOrEmpty(versionTemporaryfile) && File::Exists(versionTemporaryfile))
    {
        System::String^ latestVersionNumber = File::ReadAllText(versionTemporaryfile);
        std::wstring versionNoStr;
        if (latestVersionNumber->Length > 10)
        {
            DucoUtils::ConvertString(latestVersionNumber->Substring(0, 500), versionNoStr);
        }
        else
        {
            DucoUtils::ConvertString(latestVersionNumber, versionNoStr);
        }
        DucoVersionNumber latestVersion(versionNoStr);
        DucoVersionNumber currentVersion(KUIVersionNumber);

        if (!database->Database().RebuildInProgress() && database->Config().CheckForNextVersion()
            && currentVersion < latestVersion && owner->Visible && owner->CanFocus)
        {
            System::String^ downloadFilename = KOnlineDownloadFilePrefix + DucoUtils::ConvertString(latestVersion.Str()) + ".msi";

            downloadedFilePath = Path::GetTempPath();
            downloadedFilePath += downloadFilename;
            showingUpgradeDialog = true;

            downloadingInstaller = true;
            DucoUI::DownloadNewVersionUI::Show(owner, client, latestVersion, downloadedFilePath, database->Config());
            showingUpgradeDialog = false;
        }
        else
        {
            if (userForced && currentVersion == latestVersion)
            {
                System::Windows::Forms::IWin32Window^ mdiParent = owner;
                MessageBox::Show(mdiParent, "You already have the latest version: " + DucoUtils::ConvertString(currentVersion.Str()), "Duco version check");
            }
            showingUpgradeDialog = false;
        }
    }
    else
    {
        showingUpgradeDialog = false;
    }
}

System::Void
CheckLatestVersion::client_DownloadFileCompleted(System::Object^  sender, System::ComponentModel::AsyncCompletedEventArgs^  e)
{
    try {
        if (downloadingVersion)
        {
            downloadingVersion = false;
            downloadedVersion(e);
        }
        else if (downloadingInstaller)
        {
            downloadingInstaller = false;
            downloadedInstaller(e);
        }
    }
    catch (Exception^ ex)
    {
        showingUpgradeDialog = false;
        MessageBox::Show(owner, "Would you like to install the downloaded version of Duco now?", ex->ToString(), MessageBoxButtons::OK, MessageBoxIcon::Error);
    }
}

System::Void
CheckLatestVersion::downloadedInstaller(System::ComponentModel::AsyncCompletedEventArgs^  e)
{
    try
    {
        if (!e->Cancelled  && e->Error == nullptr && !System::String::IsNullOrEmpty(downloadedFilePath) && File::Exists(downloadedFilePath))
        {
            if (owner->Visible && owner->CanFocus)
            {
                // modal child windows are not open
                switch (MessageBox::Show(owner, "Would you like to install the downloaded version of Duco now?", "Install the new version of Duco now?", MessageBoxButtons::YesNo, MessageBoxIcon::Question, MessageBoxDefaultButton::Button1))
                {
                case ::DialogResult::Yes:
                    System::Diagnostics::Process::Start(downloadedFilePath);
                    break;
                default:
                    File::Delete(downloadedFilePath);
                    break;
                }
            }
        }
        else
        {
            if (e->Error != nullptr)
            {
                MessageBox::Show(owner, "Could not download the update.\n" + e->Error->Message + ".\nPlease visit the website: http://" + KWebSite +", to download file manually", "Error downloading update!", MessageBoxButtons::OK, MessageBoxIcon::Error);
            }
            File::Delete(downloadedFilePath);
        }
    }
    catch (System::Exception^)
    {
    }
}
