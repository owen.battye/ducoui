#pragma once
#include "DucoWindowState.h"

namespace Duco
{
    class Peal;
}

namespace DucoUI
{
    ref class RingerEventArgs;
    ref class RingerObjectBase;
    ref class RingerObject;
    ref class DatabaseManager;

    public ref class RingerList : public System::Windows::Forms::Panel, public DucoUI::RingingDatabaseObserver
    {
    public:
        RingerList(DucoUI::DatabaseManager^ theManager);

        System::Void SetPeal(Duco::Peal* thePeal);
        System::Void SetState(DucoWindowState state);
        System::Void SetHandbell(bool handbell);

        // current peal must have been updated first for these to work.
        System::Void UpdateNoOfBells(unsigned int noOfBells);
        System::Void RefreshConductorControls();
        System::Void PealDateChanged(const Duco::PerformanceDate& newDate);

        event System::EventHandler^ dataChangedEventHandler;

        System::Boolean CreateNewRingers(System::Windows::Forms::TabControl^ pealTabControl);
        System::Void AddMissingRinger(int bellNumber, System::String^ name, System::Boolean conductor, System::Boolean strapper);
        System::Void SetConductor(int bellNumber, bool conductor);
        System::Void StrapperStatusChanged(unsigned int bellNo, bool strapper);
        System::Boolean ShowStrapperCheckBox(unsigned int bellNo);
        System::Void RingerChanged(DucoUI::RingerItem^ object, System::Boolean conductor, unsigned int bellNo, System::Boolean strapper);
        System::Void RingerChanged(System::String^ newRingerName, System::Boolean conductor, unsigned int bellNo, System::Boolean strapper);
        System::Int32 RingerChangedToUnknown(System::String^ newRingerName, System::Boolean conductor, unsigned int bellNo, System::Boolean strapper);
        System::Void ConductorChanged(System::Boolean conductor, unsigned int bellNumber, System::Boolean isStrapper);

        System::Void Relayout();

        // from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

    protected:
        virtual System::Void OnRingerDataChanged(DucoUI::RingerEventArgs^ e);
        System::Boolean CreateNewRinger(DucoUI::RingerObjectBase^ nextRinger, System::Windows::Forms::TabControl^ pealTabControl);

        ~RingerList();
        !RingerList();
        System::Void InitializeComponent();

        RingerObject^ CreateRingerControl(unsigned int bellnumber, System::Boolean setRinger);
        System::Void SetRinger(RingerObject^ currentRinger, unsigned int bellNumber);
        System::Void BeginUpdate(System::Boolean saveOldValues);
        System::Void EndUpdate();

    private:
        DucoUI::RingerDisplayCache^         ringerCache;
        DucoUI::DatabaseManager^            database;
        System::ComponentModel::Container^  components;
        DucoWindowState                     state;
        Duco::Peal*                         currentPeal;
    };
}
