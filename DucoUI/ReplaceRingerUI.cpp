#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"

#include "ReplaceRingerUI.h"

#include "SoundUtils.h"
#include <RingingDatabase.h>
#include <StatisticFilters.h>
#include <PealDatabase.h>
#include "SystemDefaultIcon.h"
#include "DucoUtils.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;

ReplaceRingerUI::ReplaceRingerUI(DatabaseManager^ theDatabase)
:   database(theDatabase)
{
    ringerCache = RingerDisplayCache::CreateDisplayCacheWithRingers(database, false, true);
    InitializeComponent();
    oldRingerPerfCountLbl->Text = "";
    oldRingerFirstLbl->Text = "";
    oldRingerLastLbl->Text = "";
    newRingerPerfCountLbl->Text = "";
    newRingerFirstLbl->Text = "";
    newRingerLastLbl->Text = "";
    database->AddObserver(this);
    idOne = new Duco::ObjectId();
    idTwo = new Duco::ObjectId();
}

ReplaceRingerUI::ReplaceRingerUI(DatabaseManager^ theDatabase, const Duco::ObjectId& id, const Duco::ObjectId& id2)
    : database(theDatabase)
{
    ringerCache = RingerDisplayCache::CreateDisplayCacheWithRingers(database, false, true);
    InitializeComponent();
    oldRingerPerfCountLbl->Text = "";
    oldRingerFirstLbl->Text = "";
    oldRingerLastLbl->Text = "";
    newRingerPerfCountLbl->Text = "";
    newRingerFirstLbl->Text = "";
    newRingerLastLbl->Text = "";
    database->AddObserver(this);
    idOne = new Duco::ObjectId(id);
    idTwo = new Duco::ObjectId(id2);
}

ReplaceRingerUI::~ReplaceRingerUI()
{
    database->RemoveObserver(this);
    if (components)
    {
        delete components;
    }
}

ReplaceRingerUI::!ReplaceRingerUI()
{
    delete idOne;
    delete idTwo;
}

System::Void
ReplaceRingerUI::ReplaceBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (oldRinger->SelectedIndex == -1 || oldRinger->SelectedIndex == -1)
    {
        SoundUtils::PlayErrorSound();
        return;
    }
    Duco::ObjectId oldRingerId = ((RingerItem^)(oldRinger)->SelectedItem)->Id();
    Duco::ObjectId newRingerId = ((RingerItem^)(newRinger)->SelectedItem)->Id();
    if (database->Database().PealsDatabase().AnyPealContainingBothRingers(oldRingerId, newRingerId, true))
    {
        if (MessageBox::Show(this, "Are you sure you want to replace this ringer? Both ringers already exist in the same peal!", "Both ringers already exist in single peal", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning, MessageBoxDefaultButton::Button2) == ::DialogResult::Cancel)
        {
            return;
        }
    }
    if (database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
    }
    else
    {
        String^ oldRingerName = database->FullName(TObjectType::ERinger, oldRingerId);
        if (MessageBox::Show(this, "Are you sure you want to remove \"" + oldRingerName + "\" from all peals?", "Warning", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning) == ::DialogResult::OK)
        {
            database->ReplaceRinger(oldRingerId, newRingerId, deleteRinger->Checked);

            oldRinger->SelectedIndex = -1;
            oldRinger->Text = "";
            newRinger->SelectedIndex = -1;
            newRinger->Text = "";

            Duco::StatisticFilters filters(database->Database());
            filters.SetRinger(true, newRingerId);
            Duco::PealLengthInfo newPerfInfo = database->Database().PealsDatabase().PerformanceInfo(filters);
            String^ newRingerName = database->FullName(TObjectType::ERinger, newRingerId);
            statusLabel->Text = "\"" + newRingerName + "\" is now in " + Convert::ToString(newPerfInfo.TotalPeals()) + " performances";

            SoundUtils::PlayFinishedSound();
        }
    }
}

System::Void
ReplaceRingerUI::CloseBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void
ReplaceRingerUI::ReplaceRingerUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    ringerCache->PopulateControl(oldRinger, false);
    ringerCache->PopulateControl(newRinger, false);

    if (idOne->ValidId())
    {
        oldRinger->SelectedIndex = ringerCache->FindIndexOrAdd(*idOne);
    }
    if (idTwo->ValidId())
    {
        newRinger->SelectedIndex = ringerCache->FindIndexOrAdd(*idTwo);
    }
}

System::Void 
ReplaceRingerUI::OldRinger_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (oldRinger->SelectedIndex != -1)
    {
        Duco::StatisticFilters filters(database->Database());
        filters.SetRinger(true, ((RingerItem^)oldRinger->SelectedItem)->Id());
        Duco::PealLengthInfo pealCount = database->Database().PealsDatabase().PerformanceInfo(filters);
        oldRingerPerfCountLbl->Text = "Exists in " + Convert::ToString(pealCount.TotalPeals()) + " " + database->PerformanceString(true);
        oldRingerFirstLbl->Text = DucoUtils::ConvertString(pealCount.DateOfFirstPealString());
        oldRingerLastLbl->Text = DucoUtils::ConvertString(pealCount.DateOfLastPealString());
    }
    else
    {
        oldRingerPerfCountLbl->Text = "";
        oldRingerFirstLbl->Text = "";
        oldRingerLastLbl->Text = "";
    }
    EnableButtons();
}

System::Void 
ReplaceRingerUI::NewRinger_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (newRinger->SelectedIndex != -1)
    {
        Duco::StatisticFilters filters(database->Database());
        filters.SetRinger(true, ((RingerItem^)newRinger->SelectedItem)->Id());
        Duco::PealLengthInfo  pealCount = database->Database().PealsDatabase().PerformanceInfo(filters);
        newRingerPerfCountLbl->Text = "Exists in " + Convert::ToString(pealCount.TotalPeals()) + " " + database->PerformanceString(true);
        newRingerFirstLbl->Text = DucoUtils::ConvertString(pealCount.DateOfFirstPealString());
        newRingerLastLbl->Text = DucoUtils::ConvertString(pealCount.DateOfLastPealString());
    }
    else
    {
        newRingerPerfCountLbl->Text = "";
        newRingerFirstLbl->Text = "";
        newRingerLastLbl->Text = "";
    }
    EnableButtons();
}

System::Void
ReplaceRingerUI::EnableButtons()
{
    replaceBtn->Enabled = newRinger->SelectedIndex != -1 && oldRinger->SelectedIndex != -1 && newRinger->SelectedIndex != oldRinger->SelectedIndex;
    if (replaceBtn->Enabled)
    {
        RingerItem^ newRingerItem = (RingerItem^)newRinger->SelectedItem;
        RingerItem^ oldRingerItem = (RingerItem^)oldRinger->SelectedItem;
        statusLabel->Text = "Replace ringer \'" + oldRingerItem->DisplayName() + "\' with \'" + newRingerItem->DisplayName() + "\'";
    }
    else if (newRinger->SelectedIndex != oldRinger->SelectedIndex)
    {
        statusLabel->Text = "Same ringer selected";
    }
}

System::Void
ReplaceRingerUI::swapBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    int oldIndex = oldRinger->SelectedIndex;
    oldRinger->SelectedIndex = newRinger->SelectedIndex;
    newRinger->SelectedIndex = oldIndex;
}


System::Void
ReplaceRingerUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    switch (type)
    {
    case TObjectType::ERinger:
    {
        //BeginUpdate();
        switch (eventId)
        {
        case EAdded:
            ringerCache->AddRingerOption(id);
            break;
        case EUpdated:
            if (!id2.ValidId())
            {
                tableLayoutPanel1->SuspendLayout();
                int oldRingerIndex = oldRinger->SelectedIndex;
                int newRingerIndex = newRinger->SelectedIndex;
                oldRinger->BeginUpdate();
                newRinger->BeginUpdate();
                oldRinger->DataSource = nullptr;
                newRinger->DataSource = nullptr;
                ringerCache->UpdateRingerOption(id);
                oldRinger->EndUpdate();
                newRinger->EndUpdate();

                ringerCache->PopulateControl(newRinger, false);
                ringerCache->PopulateControl(oldRinger, false);
                oldRinger->SelectedIndex = oldRingerIndex;
                newRinger->SelectedIndex = newRingerIndex;
                tableLayoutPanel1->ResumeLayout();
            }
            break;
        case EDeleted:
            ringerCache->RemoveRingerOption(id);
            break;
        }
        //EndUpdate();
    }
    break;

    default:
        break;
    }
}
