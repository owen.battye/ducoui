#pragma once

#include <ObjectType.h>

namespace Duco
{
    class ObjectId;
}

namespace DucoUI
{
    ref class WindowsSettings;

ref class DatabaseCallbackArgs
{
public:
    DatabaseCallbackArgs(System::Boolean newInternalising);
    !DatabaseCallbackArgs();
    ~DatabaseCallbackArgs();

    inline System::Boolean Internalising();

    size_t                              totalObjects;
    size_t                              objectsOfThisType;
    Duco::ObjectId*                     currentObjectId;
    unsigned int                        databaseVersion;
    System::String^                     fileName;
    Duco::TObjectType                   objectType;
    int                                 error;

protected:
    const System::Boolean               internalising;
};

System::Boolean
DatabaseCallbackArgs::Internalising()
{
    return internalising;
}

}
