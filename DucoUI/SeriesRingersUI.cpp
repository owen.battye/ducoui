#include "DatabaseManager.h"

#include "SeriesRingersUI.h"

#include "PrintSeriesCirclingUtil.h"
#include "SystemDefaultIcon.h"
#include "DatabaseGenUtils.h"
#include <Method.h>
#include <Ringer.h>
#include <MethodSeries.h>
#include "DucoUtils.h"
#include "DucoTableSorter.h"
#include "DucoUIUtils.h"
#include <RingingDatabase.h>
#include <PealDatabase.h>
#include "SoundUtils.h"
#include <DatabaseSettings.h>
#include <DucoEngineUtils.h>

using namespace Duco;
using namespace DucoUI;
using namespace System::Drawing::Printing;
using namespace System;
using namespace System::Windows::Forms;
using namespace System::Drawing;

const float KColumnHeaderCellHeight = 100;

SeriesRingersUI::SeriesRingersUI(DatabaseManager^ theDatabase, const Duco::ObjectId& newSeriesId)
    : database(theDatabase)
{
    currentSeriesId = new ObjectId(newSeriesId);
    allSeriesNames = gcnew System::Collections::Generic::List<System::Int16>();
    InitializeComponent();
}

SeriesRingersUI::!SeriesRingersUI()
{
    delete currentSeriesId;
}

SeriesRingersUI::~SeriesRingersUI()
{
    this->!SeriesRingersUI();
    if (components)
    {
        delete components;
    }
}

System::Void
SeriesRingersUI::SeriesRingersUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    DatabaseGenUtils::GenerateMethodSeriesOptions(seriesSelector, allSeriesNames, database, *currentSeriesId, -1, false);
}

System::Void
SeriesRingersUI::seriesSelector_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    *currentSeriesId = DatabaseGenUtils::FindId(allSeriesNames, seriesSelector->SelectedIndex);
    StartGenerator();
}

System::Void
SeriesRingersUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void
SeriesRingersUI::StartGenerator()
{
    dataGridView1->Rows->Clear();
    dataGridView1->Columns->Clear();

    MethodSeries theSeries;
    if (database->FindMethodSeries(*currentSeriesId, theSeries, false))
    {
        DataGridViewCellStyle^ defaultStyle = gcnew DataGridViewCellStyle;
        defaultStyle->Alignment = DataGridViewContentAlignment::BottomLeft;
        dataGridView1->ColumnHeadersHeight = int(KColumnHeaderCellHeight);

        DataGridViewCell^ cellTemplate = gcnew DataGridViewTextBoxCell;

        // Add default columns
        DataGridViewColumn^ ringerNameColumn = gcnew DataGridViewColumn();
        ringerNameColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
        ringerNameColumn->HeaderCell->Value = "Ringer";
        ringerNameColumn->HeaderCell->Style = defaultStyle;
        ringerNameColumn->CellTemplate = cellTemplate;
        ringerNameColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Automatic;
        dataGridView1->Columns->Add(ringerNameColumn);
        DataGridViewColumn^ circledDateColumn = gcnew DataGridViewColumn();
        circledDateColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
        circledDateColumn->HeaderCell->Value = "Completed";
        circledDateColumn->HeaderCell->Style = defaultStyle;
        circledDateColumn->CellTemplate = cellTemplate;
        circledDateColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
        dataGridView1->Columns->Add(circledDateColumn);
        DataGridViewColumn^ remainingColumn = gcnew DataGridViewColumn();
        remainingColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
        remainingColumn->HeaderCell->Value = "Remaining";
        remainingColumn->HeaderCell->Style = defaultStyle;
        remainingColumn->CellTemplate = cellTemplate;
        remainingColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
        dataGridView1->Columns->Add(remainingColumn);

        const std::set<Duco::ObjectId>& methodIds = theSeries.Methods();
        for (std::set<Duco::ObjectId>::const_iterator methodIt = methodIds.begin(); methodIt != methodIds.end(); ++methodIt)
        {
            Duco::Method theMethod;
            if (database->FindMethod(*methodIt, theMethod, false))
            {
                DataGridViewColumn^ newColumn = gcnew DataGridViewColumn();
                newColumn->HeaderCell->Value = DucoUtils::ConvertString(theMethod.FullName(database->Database()));
                newColumn->CellTemplate = cellTemplate;
                newColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
                newColumn->AutoSizeMode = DataGridViewAutoSizeColumnMode::None;
                newColumn->Width = 30;
                dataGridView1->Columns->Add(newColumn);
            }
        }
        DataGridViewColumn^ totalColumn = gcnew DataGridViewColumn();
        totalColumn->HeaderCell->Value = "Total";
        totalColumn->CellTemplate = cellTemplate;
        totalColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
        totalColumn->AutoSizeMode = DataGridViewAutoSizeColumnMode::None;
        totalColumn->Width = 30;
        dataGridView1->Columns->Add(totalColumn);

        dataGridView1->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::EnableResizing;
        std::set<Duco::ObjectId> ringerIds;
        database->Database().PealsDatabase().GetRingersInMethodSeries(*currentSeriesId, ringerIds);
        for (std::set<Duco::ObjectId>::const_iterator ringerIt = ringerIds.begin(); ringerIt != ringerIds.end(); ++ringerIt)
        {
            std::map<Duco::ObjectId, unsigned int> methodIdsAndCount;
            unsigned int totalPealsForThisRinger (0);
            PerformanceDate completedDate;
            database->Database().PealsDatabase().GetPealCountInMethodSeriesForRinger(*ringerIt, *currentSeriesId, methodIdsAndCount, completedDate);

            DataGridViewRow^ newRow = gcnew DataGridViewRow();
            Duco::Ringer nextRinger;
            DataGridViewCellCollection^ theCells = newRow->Cells;
            DataGridViewTextBoxCell^ ringerCell = gcnew DataGridViewTextBoxCell();
            DataGridViewTextBoxCell^ dateCell = gcnew DataGridViewTextBoxCell();
            DataGridViewTextBoxCell^ remainingCell = gcnew DataGridViewTextBoxCell();
            theCells->Add(ringerCell);
            theCells->Add(dateCell);
            theCells->Add(remainingCell);

            if (database->FindRinger(*ringerIt, nextRinger, false))
            {
                newRow->HeaderCell->Value = DucoUtils::ConvertString(nextRinger.Id());

                ringerCell->Value = DucoUtils::ConvertString(nextRinger.FullName(database->Settings().LastNameFirst()));

                size_t remainingMethods = theSeries.NoOfUndefinedMethods();
                std::map<Duco::ObjectId, unsigned int>::const_iterator it = methodIdsAndCount.begin();
                while (it != methodIdsAndCount.end())
                {
                    DataGridViewTextBoxCell^ pealCountCell = gcnew DataGridViewTextBoxCell();
                    if (it->second == 0)
                    {
                        ++remainingMethods;
                    }
                    else
                    {
                        pealCountCell->Value = Convert::ToString(it->second);
                        totalPealsForThisRinger += it->second;
                    }
                    theCells->Add(pealCountCell);
                    ++it;
                }

                remainingCell->Value = Convert::ToString(remainingMethods);
                if (remainingMethods == 0)
                    dateCell->Value = DucoUtils::ConvertString(completedDate, false);

                DataGridViewTextBoxCell^ totalCell = gcnew DataGridViewTextBoxCell();
                theCells->Add(totalCell);
                totalCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(totalPealsForThisRinger, true));
            }

            dataGridView1->Rows->Add(newRow);
        }

        dataGridView1->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;

        DucoTableSorter^ sorter = gcnew DucoTableSorter(true, EString);
        sorter->AddSortObject(ECount, true, 2);
        sorter->AddSortObject(EDate, false, 1);
        sorter->AddSortObject(EString, true, 0);
        dataGridView1->Sort(sorter);
        dataGridView1->Columns[2]->HeaderCell->SortGlyphDirection = SortOrder::Ascending;
    }
}

System::Void
SeriesRingersUI::dataGridView_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
    DataGridViewColumn^ newColumn = dataGridView1->Columns[e->ColumnIndex];
    SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;

    ResetProgrammaticColumnsSortGlyphs();
    if (newColumn->SortMode != System::Windows::Forms::DataGridViewColumnSortMode::Programmatic)
    {
        return;
    }
    ResetNonProgrammaticColumnsSortGlyphs();

    if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
        newSortOrder = SortOrder::Ascending;
    else
        newSortOrder = SortOrder::Descending;

    newColumn->HeaderCell->SortGlyphDirection = newSortOrder;

    DucoTableSorter^ sorter = gcnew DucoTableSorter(true, EString);

    switch (e->ColumnIndex)
    {
    case 1:
        {
        sorter->AddSortObject(EDate, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
        sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, 2);
        }
        break;
    default:
        {
        sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
        sorter->AddSortObject(EString, true, 0);
        }
        break;
    }
    dataGridView1->Sort(sorter);
}

System::Void
SeriesRingersUI::ResetNonProgrammaticColumnsSortGlyphs()
{
    dataGridView1->Columns[0]->HeaderCell->SortGlyphDirection = SortOrder::None;
}

System::Void
SeriesRingersUI::ResetProgrammaticColumnsSortGlyphs()
{
    int noOfColumns = dataGridView1->Columns->Count;

    for (int i = 1; i < noOfColumns; ++i)
    {
        DataGridViewColumn^ column = dataGridView1->Columns[i];
        column->HeaderCell->SortGlyphDirection = SortOrder::None;
    }
}

System::Void
SeriesRingersUI::dataGridView1_CellPainting(System::Object^ sender, DataGridViewCellPaintingEventArgs^ e)
{
    if (e->RowIndex == -1 && e->ColumnIndex > 2)
    {
        e->PaintBackground(e->CellBounds, true);

        SizeF layoutArea (KColumnHeaderCellHeight, 1000);
        SizeF stringSize = e->Graphics->MeasureString(e->FormattedValue->ToString(), e->CellStyle->Font, layoutArea);

        RectangleF insideBox (0,0, stringSize.Width, stringSize.Height);
        e->Graphics->TranslateTransform(float(e->CellBounds.Left), float(e->CellBounds.Bottom));
        e->Graphics->RotateTransform(270);
        e->Graphics->DrawString(e->FormattedValue->ToString(),e->CellStyle->Font,Brushes::Black,insideBox);
        e->Graphics->ResetTransform();
        if (dataGridView1->Columns[e->ColumnIndex]->Width < stringSize.Height + 5)
        {
            dataGridView1->Columns[e->ColumnIndex]->Width = int(stringSize.Height) + 5;
        }
        e->Handled = true;
    }
}

System::Void 
SeriesRingersUI::printSeries(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args)
{
    PrintDocument^ printDoc = static_cast<PrintDocument^>(sender);
    PrintSeriesCirclingUtil^ printUtil = gcnew PrintSeriesCirclingUtil(database, args);
    printUtil->SetObjects(dataGridView1, *currentSeriesId);
    printUtil->printObject(args, printDoc->PrinterSettings);
}

System::Void
SeriesRingersUI::printBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (dataGridView1->Rows->Count <= 1)
    {
        SoundUtils::PlayErrorSound();
        return;
    }
    DucoUIUtils::Print(this, sender, gcnew System::Drawing::Printing::PrintPageEventHandler( this, &SeriesRingersUI::printSeries ), "series ringers", database->Database().Settings().UsePrintPreview(), true);
}