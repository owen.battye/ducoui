#pragma once
#include <vcclr.h>
#include <DoveSearchCallback.h>

namespace DucoUI
{
interface class DoveSearchCallbackUI
{
public:
    virtual void Initialised() = 0;
    virtual void Step(int progressPercent) = 0;
    virtual void Complete() = 0;
    virtual void TowerFound(const Duco::DoveObject& newTower) = 0;
};

class DoveSearchWrapper : public Duco::DoveSearchCallback
{
public:
    DoveSearchWrapper(DucoUI::DoveSearchCallbackUI^ theCallback);

    virtual void Initialised();
    virtual void Step(int progressPercent);
    virtual void Complete(bool error);
    virtual void TowerFound(const Duco::DoveObject& newTower);

protected:
    gcroot<DoveSearchCallbackUI^> uiCallback;
};

} // end namespace DucoUI

