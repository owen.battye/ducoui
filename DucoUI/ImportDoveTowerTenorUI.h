#pragma once
namespace DucoUI
{
    public ref class ImportDoveTowerTenorUI : public System::Windows::Forms::Form, public DucoUI::ProgressCallbackUI
    {
    public:
        ImportDoveTowerTenorUI(DucoUI::DatabaseManager^ theDatabase);

        // from ProgressCallbackUI
        virtual void Initialised();
        virtual void Step(int progressPercent);
        virtual void Complete();

    protected:
        ~ImportDoveTowerTenorUI();
        !ImportDoveTowerTenorUI();
        System::Void findTowersBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void chooseFileBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void importbtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void dataGridView_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);

        System::Void backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void backgroundWorker1_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void backgroundWorker1_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);
        System::Void ImportDoveTowerTenorUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void ImportDoveTowerTenorUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e);
        System::Void StartGeneration(System::Boolean importTowers);

    private:
        System::Windows::Forms::Button^                 closeBtn;
        System::Windows::Forms::Button^                 chooseFileBtn;
        System::Windows::Forms::Button^                 importbtn;
        System::Windows::Forms::ToolStripProgressBar^   progressBar;

        System::Windows::Forms::OpenFileDialog^         openFileDialog1;
        System::ComponentModel::BackgroundWorker^       backgroundWorker1;

        DucoUI::ProgressCallbackWrapper*                callback;
        Duco::DoveDatabase*                             doveDatabase;
        DucoUI::DatabaseManager^                        database;
        System::Boolean                                 importTenors;

    private: System::Windows::Forms::ToolTip^                   toolTip1;
    private: System::Windows::Forms::DataGridView^              dataGridView;
    private: System::Windows::Forms::ToolStripStatusLabel^      statusLbl;
    private: System::ComponentModel::IContainer^  components;
    private: System::Windows::Forms::ToolStripStatusLabel^  doveFilenameLbl;
    private: System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ towerId;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ towerNameColumn;
    private: System::Windows::Forms::StatusStrip^ statusStrip1;
    private: System::Windows::Forms::Button^ findTowersBtn;
    private: System::Windows::Forms::DataGridViewCellStyle^ towerAlreadyUsedCellStyle;

    ref class DoveUpdateTenorUiArgs
    {
    public:
        System::String^ doveFile;
        System::Boolean importTenors;
    };

#pragma region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        void InitializeComponent(void)
        {
            this->components = (gcnew System::ComponentModel::Container());
            this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            this->dataGridView = (gcnew System::Windows::Forms::DataGridView());
            this->towerId = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->towerNameColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->importbtn = (gcnew System::Windows::Forms::Button());
            this->closeBtn = (gcnew System::Windows::Forms::Button());
            this->chooseFileBtn = (gcnew System::Windows::Forms::Button());
            this->findTowersBtn = (gcnew System::Windows::Forms::Button());
            this->statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            this->progressBar = (gcnew System::Windows::Forms::ToolStripProgressBar());
            this->statusLbl = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->doveFilenameLbl = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
            this->backgroundWorker1 = (gcnew System::ComponentModel::BackgroundWorker());
            this->toolTip1 = (gcnew System::Windows::Forms::ToolTip(this->components));
            this->tableLayoutPanel1->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->BeginInit();
            this->statusStrip1->SuspendLayout();
            this->SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this->tableLayoutPanel1->ColumnCount = 4;
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
                100)));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
                20)));
            this->tableLayoutPanel1->Controls->Add(this->dataGridView, 0, 0);
            this->tableLayoutPanel1->Controls->Add(this->importbtn, 2, 1);
            this->tableLayoutPanel1->Controls->Add(this->closeBtn, 3, 1);
            this->tableLayoutPanel1->Controls->Add(this->chooseFileBtn, 1, 1);
            this->tableLayoutPanel1->Controls->Add(this->findTowersBtn, 0, 1);
            this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            this->tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
            this->tableLayoutPanel1->RowCount = 2;
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 51)));
            this->tableLayoutPanel1->Size = System::Drawing::Size(542, 536);
            this->tableLayoutPanel1->TabIndex = 0;
            // 
            // dataGridView
            // 
            this->dataGridView->AllowUserToAddRows = false;
            this->dataGridView->AllowUserToDeleteRows = false;
            this->dataGridView->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::AllCells;
            this->dataGridView->AutoSizeRowsMode = System::Windows::Forms::DataGridViewAutoSizeRowsMode::AllCells;
            this->dataGridView->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->dataGridView->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(2) {
                this->towerId,
                    this->towerNameColumn
            });
            this->tableLayoutPanel1->SetColumnSpan(this->dataGridView, 4);
            this->dataGridView->Dock = System::Windows::Forms::DockStyle::Fill;
            this->dataGridView->Location = System::Drawing::Point(3, 3);
            this->dataGridView->Name = L"dataGridView";
            this->dataGridView->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
            this->dataGridView->Size = System::Drawing::Size(536, 479);
            this->dataGridView->TabIndex = 4;
            this->dataGridView->CellContentDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &ImportDoveTowerTenorUI::dataGridView_CellContentDoubleClick);
            // 
            // towerId
            // 
            this->towerId->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            this->towerId->HeaderText = L"Id";
            this->towerId->Name = L"towerId";
            this->towerId->ReadOnly = true;
            this->towerId->Width = 41;
            // 
            // towerNameColumn
            // 
            this->towerNameColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            this->towerNameColumn->HeaderText = L"Tower";
            this->towerNameColumn->Name = L"towerNameColumn";
            this->towerNameColumn->ReadOnly = true;
            this->towerNameColumn->Width = 62;
            // 
            // importbtn
            // 
            this->importbtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->importbtn->Enabled = false;
            this->importbtn->Location = System::Drawing::Point(383, 488);
            this->importbtn->Name = L"importbtn";
            this->importbtn->Size = System::Drawing::Size(75, 23);
            this->importbtn->TabIndex = 3;
            this->importbtn->Text = L"Import";
            this->toolTip1->SetToolTip(this->importbtn, L"With update tenor keys, where the weight exactly matches");
            this->importbtn->UseVisualStyleBackColor = true;
            this->importbtn->Click += gcnew System::EventHandler(this, &ImportDoveTowerTenorUI::importbtn_Click);
            // 
            // closeBtn
            // 
            this->closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            this->closeBtn->Location = System::Drawing::Point(464, 488);
            this->closeBtn->Name = L"closeBtn";
            this->closeBtn->Size = System::Drawing::Size(75, 23);
            this->closeBtn->TabIndex = 0;
            this->closeBtn->Text = L"Close";
            this->closeBtn->UseVisualStyleBackColor = true;
            this->closeBtn->Click += gcnew System::EventHandler(this, &ImportDoveTowerTenorUI::closeBtn_Click);
            // 
            // chooseFileBtn
            // 
            this->chooseFileBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->chooseFileBtn->Location = System::Drawing::Point(302, 488);
            this->chooseFileBtn->Name = L"chooseFileBtn";
            this->chooseFileBtn->Size = System::Drawing::Size(75, 23);
            this->chooseFileBtn->TabIndex = 1;
            this->chooseFileBtn->Text = L"Choose file";
            this->toolTip1->SetToolTip(this->chooseFileBtn, L"Download the latest Dove datafile from \"http://dove.cccbr.org.uk/home.php\" and th"
                L"en tell Duco where to find it");
            this->chooseFileBtn->UseVisualStyleBackColor = true;
            this->chooseFileBtn->Click += gcnew System::EventHandler(this, &ImportDoveTowerTenorUI::chooseFileBtn_Click);
            // 
            // findTowersBtn
            // 
            this->findTowersBtn->Location = System::Drawing::Point(3, 488);
            this->findTowersBtn->Name = L"findTowersBtn";
            this->findTowersBtn->Size = System::Drawing::Size(75, 23);
            this->findTowersBtn->TabIndex = 5;
            this->findTowersBtn->Text = L"Find towers";
            this->findTowersBtn->UseVisualStyleBackColor = true;
            this->findTowersBtn->Click += gcnew System::EventHandler(this, &ImportDoveTowerTenorUI::findTowersBtn_Click);
            // 
            // statusStrip1
            // 
            this->statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
                this->progressBar, this->statusLbl,
                    this->doveFilenameLbl
            });
            this->statusStrip1->Location = System::Drawing::Point(0, 514);
            this->statusStrip1->Name = L"statusStrip1";
            this->statusStrip1->Size = System::Drawing::Size(542, 22);
            this->statusStrip1->TabIndex = 1;
            this->statusStrip1->Text = L"statusStrip1";
            // 
            // progressBar
            // 
            this->progressBar->Name = L"progressBar";
            this->progressBar->Size = System::Drawing::Size(100, 16);
            // 
            // statusLbl
            // 
            this->statusLbl->Name = L"statusLbl";
            this->statusLbl->Size = System::Drawing::Size(375, 17);
            this->statusLbl->Spring = true;
            this->statusLbl->Text = L"status text";
            // 
            // doveFilenameLbl
            // 
            this->doveFilenameLbl->Name = L"doveFilenameLbl";
            this->doveFilenameLbl->Size = System::Drawing::Size(50, 17);
            this->doveFilenameLbl->Text = L"dove.csv";
            // 
            // openFileDialog1
            // 
            this->openFileDialog1->DefaultExt = L"*.csv";
            this->openFileDialog1->Filter = L"CSV files|*.csv|All files|*.*";;
            // 
            // backgroundWorker1
            // 
            this->backgroundWorker1->WorkerReportsProgress = true;
            this->backgroundWorker1->WorkerSupportsCancellation = true;
            this->backgroundWorker1->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &ImportDoveTowerTenorUI::backgroundWorker1_DoWork);
            this->backgroundWorker1->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &ImportDoveTowerTenorUI::backgroundWorker1_ProgressChanged);
            this->backgroundWorker1->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &ImportDoveTowerTenorUI::backgroundWorker1_RunWorkerCompleted);
            // 
            // ImportDoveTowerTenorUI
            // 
            this->AcceptButton = this->importbtn;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->CancelButton = this->closeBtn;
            this->ClientSize = System::Drawing::Size(542, 536);
            this->Controls->Add(this->statusStrip1);
            this->Controls->Add(this->tableLayoutPanel1);
            this->MinimumSize = System::Drawing::Size(459, 157);
            this->Name = L"ImportDoveTowerTenorUI";
            this->Text = L"Update tower tenor keys";
            this->toolTip1->SetToolTip(this, L"Updates the tenor in the tower from Dove, not rings how ever which dont incllude "
                L"the heaviest bell in the tower");
            this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &ImportDoveTowerTenorUI::ImportDoveTowerTenorUI_FormClosing);
            this->Load += gcnew System::EventHandler(this, &ImportDoveTowerTenorUI::ImportDoveTowerTenorUI_Load);
            this->tableLayoutPanel1->ResumeLayout(false);
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->EndInit();
            this->statusStrip1->ResumeLayout(false);
            this->statusStrip1->PerformLayout();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion
    };
}
