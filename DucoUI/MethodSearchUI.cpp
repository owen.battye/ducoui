#pragma once
#include "ProgressWrapper.h"
#include <DatabaseSearch.h>
#include "DatabaseManager.h"
#include "GenericTablePrintUtil.h"
#include "RingingDatabaseObserver.h"
#include "DucoWindowState.h"
#include <Method.h>

#include "MethodSearchUI.h"

#include "SoundUtils.h"
#include <DucoConfiguration.h>
#include <DatabaseSettings.h>
#include "MethodUI.h"
#include "DucoUtils.h"
#include "SystemDefaultIcon.h"
#include "DatabaseGenUtils.h"
#include "DucoUIUtils.h"
#include "SearchAndReplaceUI.h"
#include <RingingDatabase.h>
#include "SearchUtils.h"
#include <SearchValidObject.h>
#include <SearchFieldBoolArgument.h>
#include <SearchFieldNumberArgument.h>
#include <SearchFieldStringArgument.h>

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::Windows::Forms;

MethodSearchUI::MethodSearchUI(DatabaseManager^ theDatabase, bool setStartSearchForInvalid)
: database(theDatabase), startSearchForInvalid(setStartSearchForInvalid)
{
    foundMethodIds = gcnew System::Collections::Generic::List<System::UInt64>;
    progressWrapper = new ProgressCallbackWrapper(this);
    updateWrapper = new ProgressCallbackWrapper(this);
    InitializeComponent();
}

MethodSearchUI::!MethodSearchUI()
{
    delete progressWrapper;
    delete updateWrapper;
}

MethodSearchUI::~MethodSearchUI()
{
    this->!MethodSearchUI();
    if (components)
    {
        delete components;
    }
}

System::Void
MethodSearchUI::MethodSearchUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    DatabaseGenUtils::GenerateMethodNameOptions(nameEditor, database);
    DatabaseGenUtils::GenerateMethodTypeOptions(typeEditor, database);
    DatabaseGenUtils::GenerateMethodNotationOptions(placeNotationEditor, database);
    if (startSearchForInvalid)
    {
        invalid->Checked = true;
        searchBtn_Click(sender, e);
    }
}

System::Void
MethodSearchUI::ClosingEvent( Object^ /*sender*/, System::ComponentModel::CancelEventArgs^ e )
{
    if (backgroundWorker->IsBusy)
    {
        backgroundWorker->CancelAsync();
        e->Cancel = true;
    }
}

System::Void
MethodSearchUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void
MethodSearchUI::clearBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (!backgroundWorker->IsBusy)
    {
        resultCount->Text = "";
        bellsEditor->Text = "";
        bellsEqualTo->Checked = true;

        placeNotationEditor->Text = "";
        typeEditor->Text = "";
        nameEditor->Text = "";
        dualOrder->Checked = false;
        blankNotation->Checked = false;
        invalid->Checked = false;
        splicedMethod->Checked = false;
        fullMethodName->Checked = false;
        emptyMethodName->Checked = false;
        emptyMethodType->Checked = false;

        methodData->Rows->Clear();
    }
}

System::Void 
MethodSearchUI::copyMethodDataCmd_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (methodData->GetCellCount(DataGridViewElementStates::Selected) > 0)
    {
        // Add the selection to the clipboard.
        Clipboard::SetDataObject(methodData->GetClipboardContent());
    }
}

System::Void
MethodSearchUI::printBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (!backgroundWorker->IsBusy)
    {
        DucoUIUtils::Print(this, sender, gcnew System::Drawing::Printing::PrintPageEventHandler( this, &MethodSearchUI::printMethods ), "towers summary", database->Database().Settings().UsePrintPreview(), false);
    }
}

System::Void
MethodSearchUI::updateBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (!backgroundWorker->IsBusy)
    {
        std::set<Duco::ObjectId> methodIds;
        DucoUtils::Convert(foundMethodIds, methodIds);

        SearchAndReplaceUI^ uiForm = gcnew SearchAndReplaceUI(database, updateWrapper, methodIds, TObjectType::EMethod);
        uiForm->ShowDialog();
    }
}


System::Void
MethodSearchUI::printMethods(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args)
{
    if (!backgroundWorker->IsBusy)
    {
        if (printUtil == nullptr)
        {
            printUtil = gcnew GenericTablePrintUtil(database, args, "Results of method search");
            printUtil->SetObjects(methodData, false);
        }
    
        System::Drawing::Printing::PrintDocument^ printDoc = static_cast<System::Drawing::Printing::PrintDocument^>(sender);
        printUtil->printObject(args, printDoc->PrinterSettings);
        if (!args->HasMorePages)
        {
            printUtil = nullptr;
        }
    }
}

System::Void
MethodSearchUI::BellsEditor_KeyDown( Object^ sender, System::Windows::Forms::KeyEventArgs^ e )
{
    if (!backgroundWorker->IsBusy)
    {
        nonNumberEntered = !DucoUIUtils::NumericKey(e, Control::ModifierKeys);
    }
}

System::Void
MethodSearchUI::BellsEditor_KeyPressed(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e)
{
    // Check for the flag being set in the KeyDown event.
    if ( nonNumberEntered == true )
    {
        // Stop the character from being entered into the control since it is non-numerical.
        e->Handled = true;
    }
}

System::Void
MethodSearchUI::blankNotation_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (!backgroundWorker->IsBusy)
    {
        placeNotationEditor->Enabled = !blankNotation->Checked;
        if (blankNotation->Checked)
        {
            placeNotationEditor->Text = "";
        }
    }
}

System::Void
MethodSearchUI::emptyMethodName_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (!backgroundWorker->IsBusy)
    {
        nameEditor->Enabled = !emptyMethodName->Checked;
        if (emptyMethodName->Checked)
        {
            nameEditor->Text = "";
        }
    }
}

System::Void
MethodSearchUI::emptyMethodType_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (!backgroundWorker->IsBusy)
    {
        typeEditor->Enabled = !emptyMethodType->Checked;
        if (emptyMethodType->Checked)
        {
            typeEditor->Text = "";
        }
    }
}

System::Void 
MethodSearchUI::methodData_CellDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    try
    {
        if (!backgroundWorker->IsBusy)
        {
            unsigned int rowIndex = e->RowIndex;
            DataGridViewRow^ currentRow = methodData->Rows[rowIndex];
            Duco::ObjectId methodId = Convert::ToInt16(currentRow->HeaderCell->Value);
            MethodUI::ShowMethod(database,  this->MdiParent, methodId);
        }
    }
    catch (Exception^)
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
MethodSearchUI::methodData_CellValueNeeded(System::Object^  sender, System::Windows::Forms::DataGridViewCellValueEventArgs^  e)
{
    if (e->RowIndex >= foundMethodIds->Count)
        return;

    Duco::ObjectId methodIdToFind(foundMethodIds[e->RowIndex]);
    Duco::Method foundMethod;
    if (database->FindMethod(methodIdToFind, foundMethod, true))
    {
        switch (e->ColumnIndex)
        {
        default:
            break;

        case 0: // Name column;
            {
            DataGridViewRow^ currentRow = methodData->Rows[e->RowIndex];
            currentRow->HeaderCell->Value = Convert::ToString(methodIdToFind.Id());
            e->Value = DucoUtils::ConvertString(foundMethod.Name());
            }
            break;

        case 1: //Type Column;
            e->Value = DucoUtils::ConvertString(foundMethod.Type());
            break;

        case 2: //Order Column;
            e->Value = DucoUtils::ConvertString(foundMethod.OrderName(database->Database(), true));
            break;

        case 3: //Place Notation Column;
            e->Value = DucoUtils::ConvertString(foundMethod.PlaceNotation());
            break;

        case 4://First peal column
            {
                PerformanceDate tempDate;
                if (database->FirstPealInMethod(foundMethod.Id(), tempDate))
                {
                    e->Value = DucoUtils::ConvertString(tempDate.Str());
                }
            }
            break;

        case 5:
            e->Value = DucoUtils::ConvertString(foundMethod.ErrorString(database->Database().Settings(), database->Config().IncludeWarningsInValidation()));
            break;
        }
    }
}

System::Void
MethodSearchUI::MethodSearchUI_Resize(System::Object^  sender, System::EventArgs^  e)
{
    methodData_Scroll(sender, gcnew ScrollEventArgs(ScrollEventType::EndScroll, 0));
}

System::Void
MethodSearchUI::methodData_Scroll(System::Object^  sender, System::Windows::Forms::ScrollEventArgs^  e)
{
    methodData->AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode::None;

    for each (DataGridViewColumn^ column in methodData->Columns)
    {
        int newColumnWidth = column->GetPreferredWidth(DataGridViewAutoSizeColumnMode::DisplayedCells, true);
        int currentColumnWidth = column->Width;
        if (newColumnWidth > currentColumnWidth)
        {
            column->AutoSizeMode = DataGridViewAutoSizeColumnMode::None;
            column->Width = newColumnWidth;
            _ASSERT(column->Width == newColumnWidth);
        }
    }
}

System::Void
MethodSearchUI::searchBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (!backgroundWorker->IsBusy)
    {
        searchBtn->Enabled = false;
        foundMethodIds->Clear();
        methodData->Rows->Clear();
        resultCount->Text = "";
        DatabaseSearch* search = new DatabaseSearch(database->Database());
        if (CreateSearchParameters(*search))
        {
            SearchUIArgs^ args = gcnew SearchUIArgs();
            args->search = search;
            backgroundWorker->RunWorkerAsync(args);
        }
        else
        {
            SoundUtils::PlayErrorSound();
            delete search;
            searchBtn->Enabled = true;
        }
    }
}

bool
MethodSearchUI::CreateSearchParameters(Duco::DatabaseSearch& search)
{
    try
    {
        unsigned int noOfBells (0);
        if (DucoUtils::ConvertTextToNumber(bellsEditor->Text, noOfBells))
        {
            TSearchType searchtype (EEqualTo);
            if (bellsMoreThan->Checked)
                searchtype = EMoreThan;
            else if (bellsLessThan->Checked)
                searchtype = ELessThan;
            TSearchFieldNumberArgument* newNumber = new TSearchFieldNumberArgument(Duco::ENoOfBells, noOfBells, searchtype);
            search.AddArgument(newNumber);
        }
        if (emptyMethodName->Checked)
        {
            Duco::TSearchFieldStringArgument* newStr = new Duco::TSearchFieldStringArgument(EMethodName, L"", false);
            search.AddArgument(newStr);
        }
        else 
        {
            if (fullMethodName->Checked)
            {
                SearchUtils::AddStringSearchArgument(search, nameEditor->Text, EFullMethodName, false);
            }
            else
            {
                SearchUtils::AddStringSearchArgument(search, nameEditor->Text, EMethodName, false);
            }
        }
        if (emptyMethodType->Checked)
        {
            Duco::TSearchFieldStringArgument* newStr = new Duco::TSearchFieldStringArgument(EMethodType, L"", false);
            search.AddArgument(newStr);
        }
        else
        {
            SearchUtils::AddStringSearchArgument(search, typeEditor->Text, EMethodType, false);
        }
        if (blankNotation->Checked)
        {
            TSearchFieldBoolArgument* newArg = new TSearchFieldBoolArgument(Duco::EMethodPN, false);
            search.AddArgument(newArg);
        }
        else
        {
            SearchUtils::AddStringSearchArgument(search, placeNotationEditor->Text, EMethodPN, false);
        }
        if (dualOrder->Checked)
        {
            TSearchFieldBoolArgument* newDualOrder = new TSearchFieldBoolArgument(Duco::EMethodDualOrder, true);
            search.AddArgument(newDualOrder);
        }
        if (invalid->Checked)
        {
            TSearchValidObject* newArg = new TSearchValidObject(database->Config().IncludeWarningsInValidation());
            search.AddValidationArgument(newArg);
        }
        if (splicedMethod->Checked)
        {
            TSearchFieldBoolArgument* newId = new TSearchFieldBoolArgument(Duco::EMethodIsSpliced);
            search.AddArgument(newId);
        }
    }
    catch (Exception^)
    {
        return false;
    }
    return true;
}

System::Void
MethodSearchUI::backgroundWorker_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
    SearchUIArgs^ args = static_cast<SearchUIArgs^>(e->Argument);
    System::ComponentModel::BackgroundWorker^ bgworker = static_cast<System::ComponentModel::BackgroundWorker^>(sender);
    std::set<Duco::ObjectId> methodIds;
    args->search->Search(*progressWrapper, methodIds, TObjectType::EMethod);
    delete args->search;

    DucoUtils::Convert(methodIds, foundMethodIds);
}

System::Void
MethodSearchUI::backgroundWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    progressBar->Value = e->ProgressPercentage;
}

System::Void
MethodSearchUI::backgroundWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    searchBtn->Enabled = true;
    progressBar->Value = 0;
    resultCount->Text = Convert::ToString(foundMethodIds->Count);
    methodData->RowCount = foundMethodIds->Count;
    if (foundMethodIds->Count == 0)
        SoundUtils::PlayErrorSound();
}

void
MethodSearchUI::Initialised()
{
    if (backgroundWorker->IsBusy)
        backgroundWorker->ReportProgress(0);
    else
        progressBar->Value = 0;
}

void
MethodSearchUI::Step(int progressPercent)
{
    if (backgroundWorker->IsBusy)
        backgroundWorker->ReportProgress(progressPercent/2);
    else
        progressBar->Value = progressPercent;
}

void
MethodSearchUI::Complete()
{
    if (!backgroundWorker->IsBusy)
    {
        progressBar->Value = 0;
    }
}
