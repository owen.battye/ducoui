#include "DatabaseManager.h"
#include "PealMapUI.h"
#include "SystemDefaultIcon.h"
#include "DucoUtils.h"
#include <DucoConfiguration.h>
#include <DatabaseSettings.h>
#include <Tower.h>
#include "DatabaseGenUtils.h"
#include "WindowsSettings.h"
#include <RingingDatabase.h>
#include <TowerDatabase.h>

using namespace DucoUI;
using namespace Duco;
using namespace System;
using namespace System::IO;

PealMapUI::PealMapUI(DatabaseManager^ theManager)
    :   database(theManager)
{
    InitializeComponent();
    InitializeCEFSharp();
    startDate = gcnew DateTime();
    endDate = gcnew DateTime();
    database->GetDateRange(startDate, endDate);
    startDateLabel->Text = startDate->ToLongDateString();
    endDateLabel->Text = endDate->ToLongDateString();
}

PealMapUI::~PealMapUI()
{
    if (components)
    {
        delete components;
    }
}

System::Void
PealMapUI::PealMapUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    DatabaseGenUtils::GetAlCountyOptions(countySelector, database);
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    size_t numberOfTowers = 0;
    if (database->Config().DisableFeaturesForPerformance() && database->Config().DisableFeaturesForPerformance(numberOfTowers))
    {
        zeroPerformances->Checked = false;
        onePerformance->Checked = false;
        moreThanOnePerformance->Checked = true;
    }
    else
    {
        zeroPerformances->Checked = database->Database().TowersDatabase().NumberOfObjects() < 100;
        onePerformance->Checked = true;
        moreThanOnePerformance->Checked = true;
    }
    combineLinked->Visible = database->Settings().UseMapBox();

    GoToMap();
}

System::Void
PealMapUI::resetDatesBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    database->GetDateRange(startDate, endDate);
    CheckDateRange();
}

System::Void
PealMapUI::InitializeCEFSharp()
{
    this->chromeBrowser = gcnew CefSharp::WinForms::ChromiumWebBrowser();
    this->layoutPanel->Controls->Add(this->chromeBrowser, 0, 2);
    // 
    // chromeBrowser
    // 
    this->layoutPanel->SetColumnSpan(this->chromeBrowser, 4);
    this->layoutPanel->SetRowSpan(this->chromeBrowser, 1);
    this->chromeBrowser->Dock = System::Windows::Forms::DockStyle::Fill;
    this->chromeBrowser->Location = System::Drawing::Point(3, 50);
    this->chromeBrowser->Margin = System::Windows::Forms::Padding(3, 0, 3, 0);
    this->chromeBrowser->Name = L"chromeBrowser";
    this->chromeBrowser->TabIndex = 5;
}

System::Void
PealMapUI::setting_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    zeroPerformances->Enabled = !useFelstead->Checked;
    onePerformance->Enabled = !useFelstead->Checked;
    moreThanOnePerformance->Enabled = !useFelstead->Checked;
    GoToMap();
}

System::Void
PealMapUI::GoToMap()
{
    size_t numberOfTowersPlotted = 0;
    size_t numberOfTowersNotPlotted = 0;
    System::String^ pealMapUrl = database->PealMap(numberOfTowersPlotted, numberOfTowersNotPlotted, zeroPerformances->Checked, onePerformance->Checked, moreThanOnePerformance->Checked, europeOnly->Checked, combineLinked->Checked, useFelstead->Checked, startDate, endDate, countySelector->Text);
    chromeBrowser->Load(pealMapUrl);

    // The longest length of the url is limited by google
    // https://developers.google.com/maps/documentation/maps-static/dev-guide?hl=en_GB
    if (pealMapUrl->Length > 8000 && !database->Settings().UseMapBox())
    {
        statusLabel->Text = "Towers may not render correctly - Google limits the number of markers";
    }
    else
    {
        statusLabel->Text = "Plotting " + Convert::ToString(numberOfTowersPlotted) + " towers";
        if (numberOfTowersNotPlotted > 0)
        {
            statusLabel->Text += " (" + Convert::ToString(numberOfTowersNotPlotted) + " without location)";
        }
    }
}

System::Void
PealMapUI::urlBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    size_t numberOfTowersPlotted = 0;
    size_t numberOfTowersNotPlotted = 0;
    System::String^ pealMapUrl = database->PealMap(numberOfTowersPlotted, numberOfTowersNotPlotted, zeroPerformances->Checked, onePerformance->Checked, moreThanOnePerformance->Checked, europeOnly->Checked, combineLinked->Checked, useFelstead->Checked, startDate, endDate, countySelector->Text);
    System::Diagnostics::Process::Start(pealMapUrl);
}

System::Void
PealMapUI::startDateBackButton_Click(System::Object^ sender, System::EventArgs^ e)
{
    TimeSpan oneYear = TimeSpan::FromDays(365);
    startDate = startDate->Subtract(oneYear);

    CheckDateRange();
}

System::Void
PealMapUI::startDateForwardsButton_Click(System::Object^ sender, System::EventArgs^ e)
{
    startDate = startDate->AddDays(365);

    CheckDateRange();
}

System::Void
PealMapUI::endDateBackButton_Click(System::Object^  sender, System::EventArgs^  e)
{
    TimeSpan oneYear = TimeSpan::FromDays(365);
    endDate = endDate->Subtract(oneYear);

    CheckDateRange();
}

System::Void
PealMapUI::endDateForwardsButton_Click(System::Object^ sender, System::EventArgs^ e)
{
    endDate = endDate->AddDays(365);

    CheckDateRange();
}

System::Void
PealMapUI::CheckDateRange()
{
    DateTime^ databaseStartDate = gcnew DateTime();
    DateTime^ databaseEndDate = gcnew DateTime();

    database->GetDateRange(databaseStartDate, databaseEndDate);
    //bool ignoreStartDate = false;
    //bool ignoreEndDate = false;
    if ((*startDate) >= (*databaseEndDate))
    {
        startDate = databaseEndDate;
    }
    else if ((*startDate) <= (*databaseStartDate))
    {
        startDate = *databaseStartDate + TimeSpan(7, 0, 0, 0);
    }
    if ((*endDate) >= (*databaseEndDate))
    {
        endDate = databaseEndDate;
    }
    else if ((*endDate) <= (*databaseStartDate))
    {
        endDate = *databaseStartDate + TimeSpan(7, 0, 0, 0);
    }

    startDateLabel->Text = startDate->ToLongDateString();
    endDateLabel->Text = endDate->ToLongDateString();

    GoToMap();
}
