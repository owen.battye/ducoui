#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"
#include <MilestoneData.h>
#include <StatisticFilters.h>

#include "MilestoneUI.h"

#include "FiltersUI.h"
#include "SystemDefaultIcon.h"
#include "DucoUtils.h"
#include <RingingDatabase.h>
#include "SoundUtils.h"
#include "RingingDatabaseObserver.h"
#include "DucoWindowState.h"
#include "OpenObjectCallback.h"
#include "PictureListControl.h"
#include "PictureControl.h"
#include "RingerList.h"
#include <Tower.h>
#include "PrintBandSummaryUtil.h"
#include <DownloadedPeal.h>
#include "PealUI.h"
#include <PealDatabase.h>

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Windows::Forms;
using namespace System::Windows::Forms::DataVisualization::Charting;

MilestoneUI::MilestoneUI(DatabaseManager^ theDatabase)
: database(theDatabase), isLoading(true)
{
    InitializeComponent();
    filters = new Duco::StatisticFilters(database->Database());
    filters->SetDefaultRinger();
}

MilestoneUI::!MilestoneUI()
{
    database->RemoveObserver(this);
    delete filters;
}

MilestoneUI::~MilestoneUI()
{
    this->!MilestoneUI();
	if (components)
	{
		delete components;
	}
}

System::Void
MilestoneUI::filtersBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    FiltersUI^ filtersDialog = gcnew FiltersUI(*filters, database);
    System::Windows::Forms::DialogResult returnVal = filtersDialog->ShowDialog(this);
    if (returnVal == System::Windows::Forms::DialogResult::OK)
    {
        GenerateMilestones();
    }
}

System::Void
MilestoneUI::MilestoneUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    database->AddObserver(this);
    isLoading = false;
    GenerateMilestones();
}

System::Void
MilestoneUI::GenerateMilestones()
{
    if (!isLoading)
    {
        completedMilestones->Rows->Clear();
        milestonePredictions->Rows->Clear();
        trendDisplay->Series[0]->Points->Clear();
        trendDisplay->Series[1]->Points->Clear();
        trendDisplay->Series[2]->Points->Clear();
        trendDisplay->Series[3]->Points->Clear();
        std::set<Duco::MilestoneData> mileStones;

        if (database->Database().PealsDatabase().Milestones(*filters, mileStones))
        {
            std::set<Duco::MilestoneData>::const_iterator it = mileStones.begin();
            while (it != mileStones.end())
            {
                AddMilestoneToTables(*it);
                if (it->LastPeal())
                {
                    trendDisplay->Series[0]->Points->AddXY(DucoUtils::ConvertDate(it->DateAchieved()), it->Milestone());
                    trendDisplay->Series[1]->Points->AddXY(DucoUtils::ConvertDate(it->DateAchieved()), it->Milestone());
                    trendDisplay->Series[2]->Points->AddXY(DucoUtils::ConvertDate(it->DateAchieved()), it->Milestone());
                    trendDisplay->Series[3]->Points->AddXY(DucoUtils::ConvertDate(it->DateAchieved()), it->Milestone());
                }
                else if (it->Prediction())
                {
                    trendDisplay->Series[2]->IsValueShownAsLabel = true;
                    if (it->DateOfMilestoneUsingAllTimeAverage().Valid())
                        trendDisplay->Series[1]->Points->AddXY(DucoUtils::ConvertDate(it->DateOfMilestoneUsingAllTimeAverage()), it->Milestone());
                    if (it->DateOfMilestoneUsing12MonthAverage().Valid())
                        trendDisplay->Series[2]->Points->AddXY(DucoUtils::ConvertDate(it->DateOfMilestoneUsing12MonthAverage()), it->Milestone());
                    if (it->DateOfMilestoneUsing6MonthAverage().Valid())
                        trendDisplay->Series[3]->Points->AddXY(DucoUtils::ConvertDate(it->DateOfMilestoneUsing6MonthAverage()), it->Milestone());
                }
                else if (trendDisplay->Series->Count > 0)
                {
                    trendDisplay->Series[0]->IsValueShownAsLabel = true;
                    trendDisplay->Series[0]->Points->AddXY(DucoUtils::ConvertDate(it->DateAchieved()), it->Milestone());
                }
                ++it;
            }
        }
    }
}

System::Void
MilestoneUI::AddMilestoneToTables(const Duco::MilestoneData& data)
{
    DataGridViewRow^ newRow = gcnew DataGridViewRow();

    DataGridViewTextBoxCell^ pealNoCell = gcnew DataGridViewTextBoxCell();
    pealNoCell->Value = Convert::ToString(data.Milestone());
    newRow->Cells->Add(pealNoCell);

    if (data.Prediction())
    {
        DataGridViewTextBoxCell^ sixMonthAvgCell = gcnew DataGridViewTextBoxCell();
        DataGridViewTextBoxCell^ twelveMonthAvgCell = gcnew DataGridViewTextBoxCell();
        DataGridViewTextBoxCell^ allTimeAvgCell = gcnew DataGridViewTextBoxCell();
        PerformanceDate currentDate;
        if (!data.DaysToMilestoneUsing6MonthAverage() == 0 && data.DateOfMilestoneUsing6MonthAverage().Valid())
            sixMonthAvgCell->Value = DucoUtils::ConvertString(data.DateOfMilestoneUsing6MonthAverage(), false);
        else
        {
            sixMonthAvgCell->Value = "Can't calculate";
            sixMonthAvgCell->ToolTipText = "Either there arent enough performances in the calculation window, or its after 31st December 3999!";
        }

        if (!data.DaysToMilestoneUsing12MonthAverage() == 0 && data.DateOfMilestoneUsing12MonthAverage().Valid())
            twelveMonthAvgCell->Value = DucoUtils::ConvertString(data.DateOfMilestoneUsing12MonthAverage(), false);
        else
        {
            twelveMonthAvgCell->Value = "Can't calculate";
            twelveMonthAvgCell->ToolTipText = "Either there arent enough performances in the calculation window, or its after 31st December 3999!";
        }

        if (!data.DaysToMilestoneUsingAllTimeAverage() == 0 && data.DateOfMilestoneUsingAllTimeAverage().Valid())
            allTimeAvgCell->Value = DucoUtils::ConvertString(data.DateOfMilestoneUsingAllTimeAverage(), false);
        else
        {
            allTimeAvgCell->Value = "Can't calculate";
            allTimeAvgCell->ToolTipText = "Either there arent enough performances in the calculation window, or its after 31st December 3999!";
        }

        newRow->Cells->Add(sixMonthAvgCell);
        newRow->Cells->Add(twelveMonthAvgCell);
        newRow->Cells->Add(allTimeAvgCell);
    }
    else
    {
        DataGridViewTextBoxCell^ dateCell = gcnew DataGridViewTextBoxCell();
        DataGridViewTextBoxCell^ pealIdCell = gcnew DataGridViewTextBoxCell();
        newRow->Cells->Add(pealIdCell);
        pealIdCell->Value = DucoUtils::ConvertString(data.PealId().Str());
        Duco::Peal thePeal;
        if (database->FindPeal(data.PealId(), thePeal, false))
        {
            dateCell->Value = DucoUtils::ConvertString(thePeal.Date(), false);
        }
        newRow->Cells->Add(dateCell);
    }

    if (data.Prediction())
    {
        milestonePredictions->Rows->Add(newRow);
    }
    else
    {
        completedMilestones->Rows->Add(newRow);
    }
    
}

System::Void
MilestoneUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    switch (type)
    {
    case TObjectType::EPeal:
        GenerateMilestones();
        break;
    default:
        break;
    }
}

System::Void
MilestoneUI::dataGridView_CellClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    try
    {
        System::Windows::Forms::DataGridView^ gridView = static_cast<System::Windows::Forms::DataGridView^>(sender);
        unsigned int rowIndex = e->RowIndex;
        DataGridViewRow^ currentRow = gridView->Rows[rowIndex];
        if (currentRow->Cells[0]->Value != nullptr)
        {
            unsigned int pealId = Convert::ToInt16(currentRow->Cells[0]->Value);
            PealUI::ShowPeal(database, this->MdiParent, pealId);
        }
    }
    catch (Exception^)
    {
        SoundUtils::PlayErrorSound();
    }
}
