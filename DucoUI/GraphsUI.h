#pragma once
namespace DucoUI
{
    public ref class GraphsUI : public System::Windows::Forms::Form
    {
    public:
        GraphsUI(DucoUI::DatabaseManager^ theDatabase);

    protected:
        !GraphsUI();
        ~GraphsUI();
        System::Void GraphsUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void yearGraph_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void ringers_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void stages_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void towers_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void uniqueCounts_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void filtersBtn_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void DrawYears();
        System::Void DrawRingers();
        System::Void DrawTowers();
        System::Void DrawStages();
        System::Void DrawUniqueCounts();

        System::Void UniqueSeriesName(System::String^% seriesName);

    private:
        System::Windows::Forms::DataVisualization::Charting::Chart^  chart;
        System::ComponentModel::Container^          components;
        System::Windows::Forms::ToolStripButton^    filtersBtn;
        DucoUI::DatabaseManager^                    database;
        System::Windows::Forms::ToolStripMenuItem^  ringers;
        System::Windows::Forms::ToolStripMenuItem^  towers;
        System::Windows::Forms::ToolStripMenuItem^  stages;
        System::Windows::Forms::ToolStripMenuItem^  years;
        System::Windows::Forms::ToolStripMenuItem^  uniqueCounts;
        System::Windows::Forms::ToolStripStatusLabel^  currentDisplayLabel;
        Duco::StatisticFilters*                     filters;

        void InitializeComponent()
        {
            System::Windows::Forms::StatusStrip^ statusStrip1;
            System::Windows::Forms::ToolStrip^ toolStrip1;
            System::Windows::Forms::ToolStripDropDownButton^ graphsBtn;
            System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(GraphsUI::typeid));
            System::Windows::Forms::DataVisualization::Charting::ChartArea^ chartArea1 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
            System::Windows::Forms::DataVisualization::Charting::Legend^ legend1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Legend());
            System::Windows::Forms::DataVisualization::Charting::Series^ series1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
            this->currentDisplayLabel = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->ringers = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->towers = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->stages = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->years = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->uniqueCounts = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->filtersBtn = (gcnew System::Windows::Forms::ToolStripButton());
            this->chart = (gcnew System::Windows::Forms::DataVisualization::Charting::Chart());
            statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
            graphsBtn = (gcnew System::Windows::Forms::ToolStripDropDownButton());
            statusStrip1->SuspendLayout();
            toolStrip1->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chart))->BeginInit();
            this->SuspendLayout();
            // 
            // statusStrip1
            // 
            statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->currentDisplayLabel });
            statusStrip1->Location = System::Drawing::Point(0, 353);
            statusStrip1->Name = L"statusStrip1";
            statusStrip1->Size = System::Drawing::Size(847, 22);
            statusStrip1->TabIndex = 0;
            statusStrip1->Text = L"statusStrip1";
            // 
            // currentDisplayLabel
            // 
            this->currentDisplayLabel->Name = L"currentDisplayLabel";
            this->currentDisplayLabel->Size = System::Drawing::Size(125, 17);
            this->currentDisplayLabel->Text = L"Performances per year";
            // 
            // toolStrip1
            // 
            toolStrip1->GripStyle = System::Windows::Forms::ToolStripGripStyle::Hidden;
            toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) { graphsBtn, this->filtersBtn });
            toolStrip1->Location = System::Drawing::Point(0, 0);
            toolStrip1->Name = L"toolStrip1";
            toolStrip1->Size = System::Drawing::Size(847, 25);
            toolStrip1->TabIndex = 2;
            toolStrip1->Text = L"toolStrip1";
            // 
            // graphsBtn
            // 
            graphsBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
            graphsBtn->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(5) {
                this->ringers, this->towers,
                    this->stages, this->years, this->uniqueCounts
            });
            graphsBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"graphsBtn.Image")));
            graphsBtn->ImageTransparentColor = System::Drawing::Color::Magenta;
            graphsBtn->Name = L"graphsBtn";
            graphsBtn->Size = System::Drawing::Size(57, 22);
            graphsBtn->Text = L"&Graphs";
            graphsBtn->ToolTipText = L"Pick a graph";
            // 
            // ringers
            // 
            this->ringers->CheckOnClick = true;
            this->ringers->Name = L"ringers";
            this->ringers->Size = System::Drawing::Size(180, 22);
            this->ringers->Text = L"Top &ringers";
            this->ringers->CheckedChanged += gcnew System::EventHandler(this, &GraphsUI::ringers_CheckedChanged);
            // 
            // towers
            // 
            this->towers->CheckOnClick = true;
            this->towers->Name = L"towers";
            this->towers->Size = System::Drawing::Size(180, 22);
            this->towers->Text = L"Top &towers";
            this->towers->CheckedChanged += gcnew System::EventHandler(this, &GraphsUI::towers_CheckedChanged);
            // 
            // stages
            // 
            this->stages->CheckOnClick = true;
            this->stages->Name = L"stages";
            this->stages->Size = System::Drawing::Size(180, 22);
            this->stages->Text = L"&Stages";
            this->stages->CheckedChanged += gcnew System::EventHandler(this, &GraphsUI::stages_CheckedChanged);
            // 
            // years
            // 
            this->years->Checked = true;
            this->years->CheckOnClick = true;
            this->years->CheckState = System::Windows::Forms::CheckState::Checked;
            this->years->Name = L"years";
            this->years->Size = System::Drawing::Size(180, 22);
            this->years->Text = L"&Years";
            this->years->CheckedChanged += gcnew System::EventHandler(this, &GraphsUI::yearGraph_CheckedChanged);
            // 
            // uniqueCounts
            // 
            this->uniqueCounts->CheckOnClick = true;
            this->uniqueCounts->Name = L"uniqueCounts";
            this->uniqueCounts->Size = System::Drawing::Size(180, 22);
            this->uniqueCounts->Text = L"&Unique counts";
            this->uniqueCounts->CheckedChanged += gcnew System::EventHandler(this, &GraphsUI::uniqueCounts_CheckedChanged);
            // 
            // filtersBtn
            // 
            this->filtersBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
            this->filtersBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"filtersBtn.Image")));
            this->filtersBtn->ImageTransparentColor = System::Drawing::Color::Magenta;
            this->filtersBtn->Name = L"filtersBtn";
            this->filtersBtn->Size = System::Drawing::Size(42, 22);
            this->filtersBtn->Text = L"&Filters";
            this->filtersBtn->Click += gcnew System::EventHandler(this, &GraphsUI::filtersBtn_Click);
            // 
            // chart
            // 
            chartArea1->Name = L"ChartArea1";
            this->chart->ChartAreas->Add(chartArea1);
            this->chart->Dock = System::Windows::Forms::DockStyle::Fill;
            legend1->Name = L"Legend1";
            this->chart->Legends->Add(legend1);
            this->chart->Location = System::Drawing::Point(0, 25);
            this->chart->Name = L"chart";
            series1->ChartArea = L"ChartArea1";
            series1->Legend = L"Legend1";
            series1->Name = L"Series1";
            this->chart->Series->Add(series1);
            this->chart->Size = System::Drawing::Size(847, 328);
            this->chart->TabIndex = 0;
            this->chart->Text = L"chart1";
            // 
            // GraphsUI
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(847, 375);
            this->Controls->Add(this->chart);
            this->Controls->Add(toolStrip1);
            this->Controls->Add(statusStrip1);
            this->Name = L"GraphsUI";
            this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Show;
            this->Text = L"Graphs";
            this->Load += gcnew System::EventHandler(this, &GraphsUI::GraphsUI_Load);
            statusStrip1->ResumeLayout(false);
            statusStrip1->PerformLayout();
            toolStrip1->ResumeLayout(false);
            toolStrip1->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chart))->EndInit();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
    };
}
