#include "PealFeeList.h"
#include "DatabaseManager.h"

#include <Peal.h>
#include "PealFeeObject.h"
#include "DucoCommon.h"
#include "RingerEventArgs.h"

using namespace Duco;
using namespace DucoUI;

using namespace System;
using namespace System::Drawing;
using namespace System::ComponentModel;

PealFeeList::PealFeeList(DatabaseManager^ theManager, Duco::Peal* const thePeal)
: System::Windows::Forms::Panel(), database(theManager), currentPeal(thePeal)
{
	InitializeComponent();
    SetPeal();
}

PealFeeList::~PealFeeList()
{
    this->!PealFeeList();
	if (components)
	{
		delete components;
	}
}

PealFeeList::!PealFeeList()
{
}

System::Void
PealFeeList::SetPeal()
{
    UpdateNoOfBells(-1);
}

System::Void
PealFeeList::UpdateNoOfBells(unsigned int noOfBells)
{
    bool noBellsSpecified (noOfBells == -1);
    if (noOfBells == -1)
    {
        noOfBells = currentPeal->NoOfBellsRung(database->Database());
    }
    if (currentPeal->Handbell())
        noOfBells /= 2;

    while (unsigned int (components->Components->Count) < noOfBells)
    {
        CreateRingerControl(components->Components->Count+1);
    }
}

PealFeeObject^
PealFeeList::CreateRingerControl(unsigned int bellnumber)
{
    PealFeeObject^ newRinger = gcnew PealFeeObject(database, bellnumber, currentPeal);
    newRinger->dataChangedEventHandler += gcnew System::EventHandler(this, &PealFeeList::PealFeeObjectChanged);
    newRinger->Location = System::Drawing::Point(0, components->Components->Count*KRingerLineHeight + KFirstControlYOffSet +  AutoScrollPosition.Y);
    Controls->AddRange(gcnew cli::array< System::Windows::Forms::Control^>(1){newRinger});
    components->Add(newRinger);
    newRinger->Visible = true;
    return newRinger;
}

System::Void
PealFeeList::PealFeeObjectChanged(System::Object^  sender, System::EventArgs^  e)
{
    OnRingerDataChanged(nullptr);
}

void
PealFeeList::InitializeComponent()
{
	components = gcnew System::ComponentModel::Container();
    this->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;

    System::Windows::Forms::Label^ pealFeeLbl;

    this->SuspendLayout();
    pealFeeLbl = (gcnew System::Windows::Forms::Label());

    // 
    // pealFeeLbl
    // 
    pealFeeLbl->AutoSize = true;
    pealFeeLbl->Location = System::Drawing::Point(255, 4);
    pealFeeLbl->Name = L"pealFeeLbl";
    pealFeeLbl->Size = System::Drawing::Size(35, 13);
    pealFeeLbl->TabIndex = 0;
    pealFeeLbl->Text = L"Peal fee paid";
    pealFeeLbl->TextAlign = ContentAlignment::MiddleRight;

    Controls->AddRange(gcnew cli::array< System::Windows::Forms::Control^  >(1){pealFeeLbl});
    this->ResumeLayout(false);
}

void
PealFeeList::OnRingerDataChanged(RingerEventArgs^ e)
{
    dataChangedEventHandler(this, e);
}
