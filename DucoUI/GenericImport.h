#pragma once

#include "ProgressWrapper.h"
#include "DatabaseImportUtil.h"
#include "FileType.h"
namespace Duco
{
    class FileImporter;
}

namespace DucoUI
{
    ref class DatabaseManager;

ref class GenericImport : public DatabaseImportUtil, public DucoUI::ProgressCallbackUI
{
public:
    GenericImport(DucoUI::DatabaseManager^ theDatabase, DucoUI::WinRkImportProgressCallback^ newCallback, System::String^ theFileName, const std::string& theInstallDir, Duco::TFileType newFileType);
    !GenericImport();
    virtual ~GenericImport();
    virtual System::String^ FileType() override;

    virtual double TotalNoOfStages() override;
    virtual double StageNumber(DucoUI::WinRkImportProgressCallback::TImportStage currentStage) override;

public: // from ProgressCallbackUI
    virtual void Initialised();
    virtual void Step(int progressPercent);
    virtual void Complete();

protected:
    virtual void Import(System::ComponentModel::BackgroundWorker^ worker, System::ComponentModel::DoWorkEventArgs^  e) override;

private:
    Duco::TFileType                         fileType;
    DucoUI::ProgressCallbackWrapper*  engineCallback;
    std::string*                            installDir;
    Duco::FileImporter*                     fileImporter;
    System::ComponentModel::BackgroundWorker^ bgWorker;
};

}