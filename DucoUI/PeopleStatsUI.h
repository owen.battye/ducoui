#pragma once
namespace DucoUI
{
    public ref class PeopleStatsUI : public System::Windows::Forms::Form, public DucoUI::RingingDatabaseObserver
    {
    public:
        PeopleStatsUI(DucoUI::DatabaseManager^ theDatabase);

        // from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

    protected:
        !PeopleStatsUI();
        ~PeopleStatsUI();

        enum class TState
        {
            ENon = 0,
            ERingers,
            EConductors,
            EComposers
        };

        System::Void backgroundWorker_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void backgroundWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void backgroundWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);

        System::Void PeopleStatsUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void Restart(PeopleStatsUI::TState newState);

        System::Void tabbedControl1_Selected(System::Object^  sender, System::Windows::Forms::TabControlEventArgs^  e);

        System::Void ignoreBracketed_CheckedChanged(System::Object^  sender, System::EventArgs^  e);

        System::Void ringerDataCount_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);
        System::Void conductorsData_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);
        System::Void composersData_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);

        System::Void data_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);

        System::Void GenerateRingerCounts(System::ComponentModel::BackgroundWorker^ worker, System::ComponentModel::DoWorkEventArgs^ e);
        System::Void GenerateConductorCounts(System::ComponentModel::BackgroundWorker^ worker, System::ComponentModel::DoWorkEventArgs^ e);
        System::Void GenerateComposerCounts(System::ComponentModel::BackgroundWorker^ worker, System::ComponentModel::DoWorkEventArgs^ e);

        System::Void startGeneration(int startPage);
        System::Void StartGenerateRingers();

        System::Void filtersButton_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void printBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void printData(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args);

    private:
        System::ComponentModel::Container^                  components;
        DucoUI::GenericTablePrintUtil^                      printUtil;
        System::Windows::Forms::DataGridView^               ringerDataCount;
        System::Windows::Forms::DataGridView^               conductorsData;
        System::Windows::Forms::DataGridView^               composersData;
        System::Windows::Forms::DataGridViewTextBoxColumn^  PealCountConducted;
        System::Windows::Forms::DataGridViewTextBoxColumn^  PealCountConductedMethod;






        System::ComponentModel::BackgroundWorker^           backgroundWorker;

        TState                                              state;
        TState                                              nextState;
        System::Windows::Forms::ToolStripProgressBar^       progressBar;



        System::Windows::Forms::CheckBox^                   ignoreBracketed;
        DucoUI::DatabaseManager^                      database;
        Duco::StatisticFilters*                             filters;
        bool ringersGenerated;
        bool conductorsGenerated;
        bool composersGenerated;



        System::Windows::Forms::TabControl^             tabbedCtrl;





    private: System::Windows::Forms::DataGridViewTextBoxColumn^ IdColumnConductors;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ ConductorColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ PealCount;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ conductorsChangesColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ conductorsTimeColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ composersName;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ composersCount;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ composersChangesColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ composersTimeColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ RingerId;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ RingingNameColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ PealCountColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ ringerChangesColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ ringerTimeColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ DurationColumn;
    private: System::Windows::Forms::ToolStripDropDownButton^ settingsMenu;
    private: System::Windows::Forms::ToolStripMenuItem^ filtersBtn;
private: System::Windows::Forms::ToolStripMenuItem^ combineRingers;











           System::Windows::Forms::ToolStripStatusLabel^ peopleCountLbl;

        ref class PeopleStatsUIArgs
        {
        public:
            PeopleStatsUI::TState    newState;
            System::Boolean          ignoreBracketed;
        };

        void InitializeComponent()
        {
            System::Windows::Forms::TabPage^ ringersTab;
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle1 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle2 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::TabPage^ conductorsTab;
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle3 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle4 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::TabPage^ composersTab;
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel3;
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle5 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle6 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::StatusStrip^ statusStrip1;
            System::Windows::Forms::ToolStrip^ toolStrip1;
            System::Windows::Forms::ToolStripButton^ printBtn;
            System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(PeopleStatsUI::typeid));
            this->ringerDataCount = (gcnew System::Windows::Forms::DataGridView());
            this->RingerId = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->RingingNameColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->PealCountColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->ringerChangesColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->ringerTimeColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->DurationColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->conductorsData = (gcnew System::Windows::Forms::DataGridView());
            this->IdColumnConductors = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->ConductorColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->PealCount = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->conductorsChangesColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->conductorsTimeColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->composersData = (gcnew System::Windows::Forms::DataGridView());
            this->composersName = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->composersCount = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->composersChangesColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->composersTimeColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->ignoreBracketed = (gcnew System::Windows::Forms::CheckBox());
            this->progressBar = (gcnew System::Windows::Forms::ToolStripProgressBar());
            this->peopleCountLbl = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->tabbedCtrl = (gcnew System::Windows::Forms::TabControl());
            this->PealCountConducted = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->PealCountConductedMethod = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->backgroundWorker = (gcnew System::ComponentModel::BackgroundWorker());
            this->settingsMenu = (gcnew System::Windows::Forms::ToolStripDropDownButton());
            this->filtersBtn = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->combineRingers = (gcnew System::Windows::Forms::ToolStripMenuItem());
            ringersTab = (gcnew System::Windows::Forms::TabPage());
            conductorsTab = (gcnew System::Windows::Forms::TabPage());
            composersTab = (gcnew System::Windows::Forms::TabPage());
            tableLayoutPanel3 = (gcnew System::Windows::Forms::TableLayoutPanel());
            statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
            printBtn = (gcnew System::Windows::Forms::ToolStripButton());
            ringersTab->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ringerDataCount))->BeginInit();
            conductorsTab->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->conductorsData))->BeginInit();
            composersTab->SuspendLayout();
            tableLayoutPanel3->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->composersData))->BeginInit();
            statusStrip1->SuspendLayout();
            toolStrip1->SuspendLayout();
            this->tabbedCtrl->SuspendLayout();
            this->SuspendLayout();
            // 
            // ringersTab
            // 
            ringersTab->Controls->Add(this->ringerDataCount);
            ringersTab->Location = System::Drawing::Point(4, 22);
            ringersTab->Name = L"ringersTab";
            ringersTab->Padding = System::Windows::Forms::Padding(3);
            ringersTab->Size = System::Drawing::Size(730, 491);
            ringersTab->TabIndex = 0;
            ringersTab->Text = L"Ringers";
            ringersTab->UseVisualStyleBackColor = true;
            // 
            // ringerDataCount
            // 
            this->ringerDataCount->AllowUserToAddRows = false;
            this->ringerDataCount->AllowUserToDeleteRows = false;
            this->ringerDataCount->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::DisplayedCellsExceptHeader;
            this->ringerDataCount->AutoSizeRowsMode = System::Windows::Forms::DataGridViewAutoSizeRowsMode::DisplayedCells;
            this->ringerDataCount->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->ringerDataCount->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(6) {
                this->RingerId,
                    this->RingingNameColumn, this->PealCountColumn, this->ringerChangesColumn, this->ringerTimeColumn, this->DurationColumn
            });
            this->ringerDataCount->Dock = System::Windows::Forms::DockStyle::Fill;
            this->ringerDataCount->Location = System::Drawing::Point(3, 3);
            this->ringerDataCount->Name = L"ringerDataCount";
            this->ringerDataCount->ReadOnly = true;
            this->ringerDataCount->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToDisplayedHeaders;
            this->ringerDataCount->Size = System::Drawing::Size(724, 485);
            this->ringerDataCount->TabIndex = 0;
            this->ringerDataCount->CellContentDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &PeopleStatsUI::data_CellContentDoubleClick);
            this->ringerDataCount->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &PeopleStatsUI::ringerDataCount_ColumnHeaderMouseClick);
            // 
            // RingerId
            // 
            this->RingerId->HeaderText = L"Id";
            this->RingerId->Name = L"RingerId";
            this->RingerId->ReadOnly = true;
            this->RingerId->Visible = false;
            this->RingerId->Width = 5;
            // 
            // RingingNameColumn
            // 
            this->RingingNameColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
            this->RingingNameColumn->HeaderText = L"Ringer";
            this->RingingNameColumn->Name = L"RingingNameColumn";
            this->RingingNameColumn->ReadOnly = true;
            this->RingingNameColumn->ToolTipText = L"Ringer name";
            // 
            // PealCountColumn
            // 
            this->PealCountColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
            this->PealCountColumn->HeaderText = L"Count";
            this->PealCountColumn->Name = L"PealCountColumn";
            this->PealCountColumn->ReadOnly = true;
            this->PealCountColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->PealCountColumn->ToolTipText = L"Number of peals rung with this person";
            this->PealCountColumn->Width = 60;
            // 
            // ringerChangesColumn
            // 
            this->ringerChangesColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            dataGridViewCellStyle1->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->ringerChangesColumn->DefaultCellStyle = dataGridViewCellStyle1;
            this->ringerChangesColumn->HeaderText = L"Changes";
            this->ringerChangesColumn->Name = L"ringerChangesColumn";
            this->ringerChangesColumn->ReadOnly = true;
            this->ringerChangesColumn->Width = 74;
            // 
            // ringerTimeColumn
            // 
            this->ringerTimeColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            dataGridViewCellStyle2->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->ringerTimeColumn->DefaultCellStyle = dataGridViewCellStyle2;
            this->ringerTimeColumn->HeaderText = L"Time";
            this->ringerTimeColumn->Name = L"ringerTimeColumn";
            this->ringerTimeColumn->ReadOnly = true;
            this->ringerTimeColumn->Width = 55;
            // 
            // DurationColumn
            // 
            this->DurationColumn->HeaderText = L"Time period";
            this->DurationColumn->Name = L"DurationColumn";
            this->DurationColumn->ReadOnly = true;
            this->DurationColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->DurationColumn->ToolTipText = L"Days between first and last peal";
            this->DurationColumn->Width = 5;
            // 
            // conductorsTab
            // 
            conductorsTab->Controls->Add(this->conductorsData);
            conductorsTab->Location = System::Drawing::Point(4, 22);
            conductorsTab->Name = L"conductorsTab";
            conductorsTab->Padding = System::Windows::Forms::Padding(3);
            conductorsTab->Size = System::Drawing::Size(730, 491);
            conductorsTab->TabIndex = 4;
            conductorsTab->Text = L"Conductors";
            conductorsTab->UseVisualStyleBackColor = true;
            // 
            // conductorsData
            // 
            this->conductorsData->AllowUserToAddRows = false;
            this->conductorsData->AllowUserToDeleteRows = false;
            this->conductorsData->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::AllCells;
            this->conductorsData->AutoSizeRowsMode = System::Windows::Forms::DataGridViewAutoSizeRowsMode::DisplayedCells;
            this->conductorsData->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->conductorsData->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(5) {
                this->IdColumnConductors,
                    this->ConductorColumn, this->PealCount, this->conductorsChangesColumn, this->conductorsTimeColumn
            });
            this->conductorsData->Dock = System::Windows::Forms::DockStyle::Fill;
            this->conductorsData->Location = System::Drawing::Point(3, 3);
            this->conductorsData->Name = L"conductorsData";
            this->conductorsData->ReadOnly = true;
            this->conductorsData->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToDisplayedHeaders;
            this->conductorsData->Size = System::Drawing::Size(724, 485);
            this->conductorsData->TabIndex = 0;
            this->conductorsData->CellContentDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &PeopleStatsUI::data_CellContentDoubleClick);
            this->conductorsData->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &PeopleStatsUI::conductorsData_ColumnHeaderMouseClick);
            // 
            // IdColumnConductors
            // 
            this->IdColumnConductors->HeaderText = L"Id";
            this->IdColumnConductors->Name = L"IdColumnConductors";
            this->IdColumnConductors->ReadOnly = true;
            this->IdColumnConductors->Visible = false;
            this->IdColumnConductors->Width = 41;
            // 
            // ConductorColumn
            // 
            this->ConductorColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
            this->ConductorColumn->HeaderText = L"Conductor";
            this->ConductorColumn->Name = L"ConductorColumn";
            this->ConductorColumn->ReadOnly = true;
            this->ConductorColumn->ToolTipText = L"Conductors name";
            // 
            // PealCount
            // 
            this->PealCount->HeaderText = L"Count";
            this->PealCount->Name = L"PealCount";
            this->PealCount->ReadOnly = true;
            this->PealCount->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->PealCount->ToolTipText = L"Number of peals conducted by the person";
            this->PealCount->Width = 60;
            // 
            // conductorsChangesColumn
            // 
            this->conductorsChangesColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            dataGridViewCellStyle3->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->conductorsChangesColumn->DefaultCellStyle = dataGridViewCellStyle3;
            this->conductorsChangesColumn->HeaderText = L"Changes";
            this->conductorsChangesColumn->Name = L"conductorsChangesColumn";
            this->conductorsChangesColumn->ReadOnly = true;
            this->conductorsChangesColumn->Width = 74;
            // 
            // conductorsTimeColumn
            // 
            this->conductorsTimeColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            dataGridViewCellStyle4->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->conductorsTimeColumn->DefaultCellStyle = dataGridViewCellStyle4;
            this->conductorsTimeColumn->HeaderText = L"Time";
            this->conductorsTimeColumn->Name = L"conductorsTimeColumn";
            this->conductorsTimeColumn->ReadOnly = true;
            this->conductorsTimeColumn->Width = 55;
            // 
            // composersTab
            // 
            composersTab->Controls->Add(tableLayoutPanel3);
            composersTab->Location = System::Drawing::Point(4, 22);
            composersTab->Name = L"composersTab";
            composersTab->Padding = System::Windows::Forms::Padding(3);
            composersTab->Size = System::Drawing::Size(730, 491);
            composersTab->TabIndex = 5;
            composersTab->Text = L"Composers";
            composersTab->UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            tableLayoutPanel3->ColumnCount = 1;
            tableLayoutPanel3->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel3->Controls->Add(this->composersData, 0, 1);
            tableLayoutPanel3->Controls->Add(this->ignoreBracketed, 0, 0);
            tableLayoutPanel3->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel3->Location = System::Drawing::Point(3, 3);
            tableLayoutPanel3->Name = L"tableLayoutPanel3";
            tableLayoutPanel3->RowCount = 2;
            tableLayoutPanel3->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 24)));
            tableLayoutPanel3->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel3->Size = System::Drawing::Size(724, 485);
            tableLayoutPanel3->TabIndex = 1;
            // 
            // composersData
            // 
            this->composersData->AllowUserToAddRows = false;
            this->composersData->AllowUserToDeleteRows = false;
            this->composersData->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::AllCells;
            this->composersData->AutoSizeRowsMode = System::Windows::Forms::DataGridViewAutoSizeRowsMode::DisplayedCells;
            this->composersData->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->composersData->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(4) {
                this->composersName,
                    this->composersCount, this->composersChangesColumn, this->composersTimeColumn
            });
            this->composersData->Dock = System::Windows::Forms::DockStyle::Fill;
            this->composersData->Location = System::Drawing::Point(3, 27);
            this->composersData->Name = L"composersData";
            this->composersData->ReadOnly = true;
            this->composersData->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToDisplayedHeaders;
            this->composersData->Size = System::Drawing::Size(718, 455);
            this->composersData->TabIndex = 0;
            this->composersData->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &PeopleStatsUI::composersData_ColumnHeaderMouseClick);
            // 
            // composersName
            // 
            this->composersName->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
            this->composersName->HeaderText = L"Composer";
            this->composersName->Name = L"composersName";
            this->composersName->ReadOnly = true;
            this->composersName->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            // 
            // composersCount
            // 
            this->composersCount->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            this->composersCount->HeaderText = L"Performances";
            this->composersCount->Name = L"composersCount";
            this->composersCount->ReadOnly = true;
            this->composersCount->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->composersCount->Width = 97;
            // 
            // composersChangesColumn
            // 
            this->composersChangesColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            dataGridViewCellStyle5->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->composersChangesColumn->DefaultCellStyle = dataGridViewCellStyle5;
            this->composersChangesColumn->HeaderText = L"Changes";
            this->composersChangesColumn->Name = L"composersChangesColumn";
            this->composersChangesColumn->ReadOnly = true;
            this->composersChangesColumn->Width = 74;
            // 
            // composersTimeColumn
            // 
            this->composersTimeColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            dataGridViewCellStyle6->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->composersTimeColumn->DefaultCellStyle = dataGridViewCellStyle6;
            this->composersTimeColumn->HeaderText = L"Time";
            this->composersTimeColumn->Name = L"composersTimeColumn";
            this->composersTimeColumn->ReadOnly = true;
            this->composersTimeColumn->Width = 55;
            // 
            // ignoreBracketed
            // 
            this->ignoreBracketed->AutoSize = true;
            this->ignoreBracketed->Location = System::Drawing::Point(3, 3);
            this->ignoreBracketed->Name = L"ignoreBracketed";
            this->ignoreBracketed->Size = System::Drawing::Size(131, 17);
            this->ignoreBracketed->TabIndex = 1;
            this->ignoreBracketed->Text = L"Ignore text in brackets";
            this->ignoreBracketed->UseVisualStyleBackColor = true;
            this->ignoreBracketed->CheckedChanged += gcnew System::EventHandler(this, &PeopleStatsUI::ignoreBracketed_CheckedChanged);
            // 
            // statusStrip1
            // 
            statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) { this->progressBar, this->peopleCountLbl });
            statusStrip1->Location = System::Drawing::Point(0, 542);
            statusStrip1->MinimumSize = System::Drawing::Size(0, 23);
            statusStrip1->Name = L"statusStrip1";
            statusStrip1->Size = System::Drawing::Size(738, 23);
            statusStrip1->TabIndex = 2;
            statusStrip1->Text = L"statusStrip1";
            // 
            // progressBar
            // 
            this->progressBar->Name = L"progressBar";
            this->progressBar->Size = System::Drawing::Size(100, 17);
            this->progressBar->Step = 1;
            // 
            // peopleCountLbl
            // 
            this->peopleCountLbl->Name = L"peopleCountLbl";
            this->peopleCountLbl->Size = System::Drawing::Size(621, 18);
            this->peopleCountLbl->Spring = true;
            this->peopleCountLbl->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // toolStrip1
            // 
            toolStrip1->GripStyle = System::Windows::Forms::ToolStripGripStyle::Hidden;
            toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) { this->settingsMenu, printBtn });
            toolStrip1->Location = System::Drawing::Point(0, 0);
            toolStrip1->Name = L"toolStrip1";
            toolStrip1->Size = System::Drawing::Size(738, 25);
            toolStrip1->TabIndex = 4;
            toolStrip1->Text = L"toolStrip1";
            // 
            // printBtn
            // 
            printBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
            printBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"printBtn.Image")));
            printBtn->ImageTransparentColor = System::Drawing::Color::Magenta;
            printBtn->Name = L"printBtn";
            printBtn->Size = System::Drawing::Size(36, 22);
            printBtn->Text = L"&Print";
            printBtn->Click += gcnew System::EventHandler(this, &PeopleStatsUI::printBtn_Click);
            // 
            // tabbedCtrl
            // 
            this->tabbedCtrl->Controls->Add(ringersTab);
            this->tabbedCtrl->Controls->Add(conductorsTab);
            this->tabbedCtrl->Controls->Add(composersTab);
            this->tabbedCtrl->Dock = System::Windows::Forms::DockStyle::Fill;
            this->tabbedCtrl->Location = System::Drawing::Point(0, 25);
            this->tabbedCtrl->Multiline = true;
            this->tabbedCtrl->Name = L"tabbedCtrl";
            this->tabbedCtrl->SelectedIndex = 0;
            this->tabbedCtrl->Size = System::Drawing::Size(738, 517);
            this->tabbedCtrl->TabIndex = 1;
            this->tabbedCtrl->Selected += gcnew System::Windows::Forms::TabControlEventHandler(this, &PeopleStatsUI::tabbedControl1_Selected);
            // 
            // PealCountConducted
            // 
            this->PealCountConducted->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->PealCountConducted->HeaderText = L"Peal count";
            this->PealCountConducted->Name = L"PealCountConducted";
            this->PealCountConducted->ReadOnly = true;
            // 
            // PealCountConductedMethod
            // 
            this->PealCountConductedMethod->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->PealCountConductedMethod->HeaderText = L"Peal count";
            this->PealCountConductedMethod->Name = L"PealCountConductedMethod";
            this->PealCountConductedMethod->ReadOnly = true;
            // 
            // backgroundWorker
            // 
            this->backgroundWorker->WorkerReportsProgress = true;
            this->backgroundWorker->WorkerSupportsCancellation = true;
            this->backgroundWorker->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &PeopleStatsUI::backgroundWorker_DoWork);
            this->backgroundWorker->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &PeopleStatsUI::backgroundWorker_ProgressChanged);
            this->backgroundWorker->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &PeopleStatsUI::backgroundWorker_RunWorkerCompleted);
            // 
            // settingsMenu
            // 
            this->settingsMenu->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
            this->settingsMenu->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
                this->filtersBtn,
                    this->combineRingers
            });
            this->settingsMenu->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"settingsMenu.Image")));
            this->settingsMenu->ImageTransparentColor = System::Drawing::Color::Magenta;
            this->settingsMenu->Name = L"settingsMenu";
            this->settingsMenu->Size = System::Drawing::Size(62, 22);
            this->settingsMenu->Text = L"&Settings";
            // 
            // filtersBtn
            // 
            this->filtersBtn->Name = L"filtersBtn";
            this->filtersBtn->Size = System::Drawing::Size(197, 22);
            this->filtersBtn->Text = L"&Filters";
            this->filtersBtn->Click += gcnew System::EventHandler(this, &PeopleStatsUI::filtersButton_Click);
            // 
            // combineRingers
            // 
            this->combineRingers->Checked = true;
            this->combineRingers->CheckOnClick = true;
            this->combineRingers->CheckState = System::Windows::Forms::CheckState::Checked;
            this->combineRingers->Name = L"combineRingers";
            this->combineRingers->Size = System::Drawing::Size(197, 22);
            this->combineRingers->Text = L"Combine linked &ringers";
            // 
            // PeopleStatsUI
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->AutoSize = true;
            this->ClientSize = System::Drawing::Size(738, 565);
            this->Controls->Add(this->tabbedCtrl);
            this->Controls->Add(toolStrip1);
            this->Controls->Add(statusStrip1);
            this->Name = L"PeopleStatsUI";
            this->Text = L"People";
            this->Load += gcnew System::EventHandler(this, &PeopleStatsUI::PeopleStatsUI_Load);
            ringersTab->ResumeLayout(false);
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ringerDataCount))->EndInit();
            conductorsTab->ResumeLayout(false);
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->conductorsData))->EndInit();
            composersTab->ResumeLayout(false);
            tableLayoutPanel3->ResumeLayout(false);
            tableLayoutPanel3->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->composersData))->EndInit();
            statusStrip1->ResumeLayout(false);
            statusStrip1->PerformLayout();
            toolStrip1->ResumeLayout(false);
            toolStrip1->PerformLayout();
            this->tabbedCtrl->ResumeLayout(false);
            this->ResumeLayout(false);
            this->PerformLayout();

        }
    };
}
