#pragma once
namespace DucoUI
{
    public ref class MethodUI : public System::Windows::Forms::Form, public DucoUI::RingingDatabaseObserver
    {
    public:
        MethodUI(DucoUI::DatabaseManager^ theDatabase, DucoUI::DucoWindowState state);
        static void ShowMethod(DucoUI::DatabaseManager^ theDatabase, System::Windows::Forms::Form^ parent, const Duco::ObjectId& methodId);
        
        // from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

    protected:
        virtual void OnPaint(System::Windows::Forms::PaintEventArgs^ e) override;

        MethodUI(DucoUI::DatabaseManager^ theDatabase, const Duco::ObjectId& methodId);
        ~MethodUI();
        !MethodUI();
        System::Void UpdateIdLabel();
        System::Void SetDualOrderCheckBoxName();

        System::Void MethodUI_Load(System::Object^ sender, System::EventArgs^ e);
        System::Void ClosingEvent(System::Object^ object, System::ComponentModel::CancelEventArgs^ e);

        System::Void EditBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void CloseBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void SaveBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void StartBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void Back10Btn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void PreviousBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void NextBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void Forward10Btn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void EndBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void MethodIdLabel_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void CancelBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void OnlineBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void PrintCmd_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void StatsCmd_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void CopyToClipboardCmd_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void compLibToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e);

        System::Void MethodDataChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void DualOrder_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void PrintMethod(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args);
        System::Void DrawFrom_ValueChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void PlaceNotationChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void AltBobNotationEditor_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void AltSingleNotationEditor_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void BobPlaceNotationChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void SinglePlaceNotationChanged(System::Object^ sender, System::EventArgs^ e);

        System::Void NotationEditor_KeyPressed(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e);
        System::Void NotationEditor_KeyDown( Object^ sender, System::Windows::Forms::KeyEventArgs^ e );

        void SetViewState();
        void SetEditState();
        void SetNewState();

        void PopulateViewWithId(const Duco::ObjectId& methodId);
        void PopulateView();
        void ClearView();
        void DrawLine(System::Object^ sender, System::ComponentModel::DoWorkEventArgs^ e);
        void DrawLineCompleted(System::Object^ sender, System::ComponentModel::RunWorkerCompletedEventArgs^ args);
        bool StartLineDrawing(unsigned int startingFromBell);
        void RemoveMethodDrawing();
        void CheckIsSplicedLabel();

    private:
        System::ComponentModel::IContainer^  components;
        System::ComponentModel::BackgroundWorker^  lineGenerator;

        System::Windows::Forms::ComboBox^   nameEditor;
        System::Windows::Forms::ComboBox^   typeEditor;
        System::Windows::Forms::ComboBox^   orderEditor;
        System::Windows::Forms::TextBox^    placeNotationEditor;
        System::Windows::Forms::PictureBox^ methodPicture;
        System::Windows::Forms::ToolStripStatusLabel^  methodIdLabel;
        System::Windows::Forms::Button^     startBtn;
        System::Windows::Forms::Button^     back10Btn;
        System::Windows::Forms::Button^     previousBtn;
        System::Windows::Forms::Button^     nextBtn;
        System::Windows::Forms::Button^     forward10Btn;
        System::Windows::Forms::Button^     endBtn;
        System::Windows::Forms::Button^     editBtn;
        System::Windows::Forms::Button^     cancelBtn;
        System::Windows::Forms::Button^     closeBtn;
        System::Windows::Forms::Button^     saveBtn;
        System::Windows::Forms::Button^     onlineBtn;
        System::Windows::Forms::ToolStripMenuItem^  printCmd;
        System::Windows::Forms::ToolStripMenuItem^  statsCmd;
        System::Windows::Forms::CheckBox^   dualOrder;
        System::Windows::Forms::ContextMenuStrip^   pictureMenu;
        System::Windows::Forms::ToolStripMenuItem^  copyToClipboardCmd;
        System::Windows::Forms::Panel^              methodDetailPanel;
        System::Windows::Forms::Label^              placeNotationLbl;
        System::Windows::Forms::Panel^              buttonPanel;
        System::Windows::Forms::ToolStripStatusLabel^  isSplicedLbl;
        System::Windows::Forms::TabPage^            detailsPage;



        System::Windows::Forms::TextBox^            altSingleNotationEditor;
        System::Windows::Forms::TextBox^            altBobNotationEditor;
        System::Windows::Forms::TextBox^            singleNotationEditor;
        System::Windows::Forms::TextBox^            bobNotationEditor;

        DucoUI::DatabaseManager^      database;
        Duco::Method*                       currentMethod;
        DucoWindowState                     windowState;
        bool                                dataChanged;
        bool                                restartLineDrawing;
        bool                                invalidCharEntered;
        bool                                populatingView;
        bool                                triedToStartDrawing;
private: System::Windows::Forms::NumericUpDown^ drawFrom;
private: System::Windows::Forms::Label^ drawFromLbl;
private: System::Windows::Forms::ToolStripMenuItem^ compLibToolStripMenuItem;
       System::Drawing::Imaging::Metafile^ methodDrawing;

#pragma region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        void InitializeComponent(void)
        {
            this->components = (gcnew System::ComponentModel::Container());
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
            System::Windows::Forms::TabControl^ tabControl1;
            System::Windows::Forms::TabPage^ callingsPage;
            System::Windows::Forms::Panel^ callingsPanel;
            System::Windows::Forms::GroupBox^ altCallingGroup;
            System::Windows::Forms::Label^ altSingleLbl;
            System::Windows::Forms::Label^ altBobLbl;
            System::Windows::Forms::GroupBox^ normalCallingGroup;
            System::Windows::Forms::Label^ singleLbl;
            System::Windows::Forms::Label^ bobLbl;
            System::Windows::Forms::StatusStrip^ statusStrip1;
            System::Windows::Forms::MenuStrip^ menuStrip1;
            this->detailsPage = (gcnew System::Windows::Forms::TabPage());
            this->methodDetailPanel = (gcnew System::Windows::Forms::Panel());
            this->drawFrom = (gcnew System::Windows::Forms::NumericUpDown());
            this->drawFromLbl = (gcnew System::Windows::Forms::Label());
            this->dualOrder = (gcnew System::Windows::Forms::CheckBox());
            this->typeEditor = (gcnew System::Windows::Forms::ComboBox());
            this->placeNotationEditor = (gcnew System::Windows::Forms::TextBox());
            this->placeNotationLbl = (gcnew System::Windows::Forms::Label());
            this->orderEditor = (gcnew System::Windows::Forms::ComboBox());
            this->nameEditor = (gcnew System::Windows::Forms::ComboBox());
            this->altSingleNotationEditor = (gcnew System::Windows::Forms::TextBox());
            this->altBobNotationEditor = (gcnew System::Windows::Forms::TextBox());
            this->singleNotationEditor = (gcnew System::Windows::Forms::TextBox());
            this->bobNotationEditor = (gcnew System::Windows::Forms::TextBox());
            this->buttonPanel = (gcnew System::Windows::Forms::Panel());
            this->saveBtn = (gcnew System::Windows::Forms::Button());
            this->closeBtn = (gcnew System::Windows::Forms::Button());
            this->startBtn = (gcnew System::Windows::Forms::Button());
            this->back10Btn = (gcnew System::Windows::Forms::Button());
            this->previousBtn = (gcnew System::Windows::Forms::Button());
            this->nextBtn = (gcnew System::Windows::Forms::Button());
            this->forward10Btn = (gcnew System::Windows::Forms::Button());
            this->endBtn = (gcnew System::Windows::Forms::Button());
            this->editBtn = (gcnew System::Windows::Forms::Button());
            this->cancelBtn = (gcnew System::Windows::Forms::Button());
            this->onlineBtn = (gcnew System::Windows::Forms::Button());
            this->methodPicture = (gcnew System::Windows::Forms::PictureBox());
            this->pictureMenu = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
            this->copyToClipboardCmd = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->isSplicedLbl = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->methodIdLabel = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->printCmd = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->statsCmd = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->lineGenerator = (gcnew System::ComponentModel::BackgroundWorker());
            this->compLibToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            tabControl1 = (gcnew System::Windows::Forms::TabControl());
            callingsPage = (gcnew System::Windows::Forms::TabPage());
            callingsPanel = (gcnew System::Windows::Forms::Panel());
            altCallingGroup = (gcnew System::Windows::Forms::GroupBox());
            altSingleLbl = (gcnew System::Windows::Forms::Label());
            altBobLbl = (gcnew System::Windows::Forms::Label());
            normalCallingGroup = (gcnew System::Windows::Forms::GroupBox());
            singleLbl = (gcnew System::Windows::Forms::Label());
            bobLbl = (gcnew System::Windows::Forms::Label());
            statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
            tableLayoutPanel1->SuspendLayout();
            tabControl1->SuspendLayout();
            this->detailsPage->SuspendLayout();
            this->methodDetailPanel->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->drawFrom))->BeginInit();
            callingsPage->SuspendLayout();
            callingsPanel->SuspendLayout();
            altCallingGroup->SuspendLayout();
            normalCallingGroup->SuspendLayout();
            this->buttonPanel->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->methodPicture))->BeginInit();
            this->pictureMenu->SuspendLayout();
            statusStrip1->SuspendLayout();
            menuStrip1->SuspendLayout();
            this->SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->AutoSize = true;
            tableLayoutPanel1->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            tableLayoutPanel1->ColumnCount = 1;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->Controls->Add(tabControl1, 0, 1);
            tableLayoutPanel1->Controls->Add(this->buttonPanel, 0, 2);
            tableLayoutPanel1->Controls->Add(this->methodPicture, 0, 3);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 5;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 25)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 115)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 28)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 25)));
            tableLayoutPanel1->Size = System::Drawing::Size(538, 195);
            tableLayoutPanel1->TabIndex = 22;
            // 
            // tabControl1
            // 
            tabControl1->Controls->Add(this->detailsPage);
            tabControl1->Controls->Add(callingsPage);
            tabControl1->Dock = System::Windows::Forms::DockStyle::Fill;
            tabControl1->Location = System::Drawing::Point(3, 28);
            tabControl1->Name = L"tabControl1";
            tabControl1->SelectedIndex = 0;
            tabControl1->Size = System::Drawing::Size(532, 109);
            tabControl1->TabIndex = 22;
            // 
            // detailsPage
            // 
            this->detailsPage->Controls->Add(this->methodDetailPanel);
            this->detailsPage->Location = System::Drawing::Point(4, 22);
            this->detailsPage->Name = L"detailsPage";
            this->detailsPage->Padding = System::Windows::Forms::Padding(3);
            this->detailsPage->Size = System::Drawing::Size(524, 83);
            this->detailsPage->TabIndex = 0;
            this->detailsPage->Text = L"Details";
            this->detailsPage->UseVisualStyleBackColor = true;
            // 
            // methodDetailPanel
            // 
            this->methodDetailPanel->AutoSize = true;
            this->methodDetailPanel->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            this->methodDetailPanel->Controls->Add(this->drawFrom);
            this->methodDetailPanel->Controls->Add(this->drawFromLbl);
            this->methodDetailPanel->Controls->Add(this->dualOrder);
            this->methodDetailPanel->Controls->Add(this->typeEditor);
            this->methodDetailPanel->Controls->Add(this->placeNotationEditor);
            this->methodDetailPanel->Controls->Add(this->placeNotationLbl);
            this->methodDetailPanel->Controls->Add(this->orderEditor);
            this->methodDetailPanel->Controls->Add(this->nameEditor);
            this->methodDetailPanel->Dock = System::Windows::Forms::DockStyle::Fill;
            this->methodDetailPanel->Location = System::Drawing::Point(3, 3);
            this->methodDetailPanel->Name = L"methodDetailPanel";
            this->methodDetailPanel->Size = System::Drawing::Size(518, 77);
            this->methodDetailPanel->TabIndex = 0;
            // 
            // drawFrom
            // 
            this->drawFrom->Location = System::Drawing::Point(451, 57);
            this->drawFrom->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 16, 0, 0, 0 });
            this->drawFrom->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
            this->drawFrom->Name = L"drawFrom";
            this->drawFrom->Size = System::Drawing::Size(38, 20);
            this->drawFrom->TabIndex = 17;
            this->drawFrom->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
            // 
            // drawFromLbl
            // 
            this->drawFromLbl->AutoSize = true;
            this->drawFromLbl->Location = System::Drawing::Point(391, 60);
            this->drawFromLbl->Name = L"drawFromLbl";
            this->drawFromLbl->Size = System::Drawing::Size(58, 13);
            this->drawFromLbl->TabIndex = 16;
            this->drawFromLbl->Text = L"Draw from:";
            // 
            // dualOrder
            // 
            this->dualOrder->AutoSize = true;
            this->dualOrder->Location = System::Drawing::Point(4, 60);
            this->dualOrder->Name = L"dualOrder";
            this->dualOrder->Size = System::Drawing::Size(15, 14);
            this->dualOrder->TabIndex = 10;
            this->dualOrder->UseVisualStyleBackColor = true;
            this->dualOrder->CheckedChanged += gcnew System::EventHandler(this, &MethodUI::DualOrder_CheckedChanged);
            // 
            // typeEditor
            // 
            this->typeEditor->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
            this->typeEditor->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->typeEditor->FormattingEnabled = true;
            this->typeEditor->Location = System::Drawing::Point(170, 3);
            this->typeEditor->Name = L"typeEditor";
            this->typeEditor->Size = System::Drawing::Size(160, 21);
            this->typeEditor->TabIndex = 2;
            this->typeEditor->TextChanged += gcnew System::EventHandler(this, &MethodUI::MethodDataChanged);
            // 
            // placeNotationEditor
            // 
            this->placeNotationEditor->Location = System::Drawing::Point(87, 30);
            this->placeNotationEditor->Name = L"placeNotationEditor";
            this->placeNotationEditor->Size = System::Drawing::Size(404, 20);
            this->placeNotationEditor->TabIndex = 5;
            this->placeNotationEditor->TextChanged += gcnew System::EventHandler(this, &MethodUI::PlaceNotationChanged);
            this->placeNotationEditor->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MethodUI::NotationEditor_KeyDown);
            this->placeNotationEditor->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &MethodUI::NotationEditor_KeyPressed);
            // 
            // placeNotationLbl
            // 
            this->placeNotationLbl->AutoSize = true;
            this->placeNotationLbl->Location = System::Drawing::Point(1, 33);
            this->placeNotationLbl->Name = L"placeNotationLbl";
            this->placeNotationLbl->Size = System::Drawing::Size(80, 13);
            this->placeNotationLbl->TabIndex = 4;
            this->placeNotationLbl->Text = L"Place Notation:";
            // 
            // orderEditor
            // 
            this->orderEditor->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
            this->orderEditor->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->orderEditor->FormattingEnabled = true;
            this->orderEditor->Location = System::Drawing::Point(336, 3);
            this->orderEditor->Name = L"orderEditor";
            this->orderEditor->Size = System::Drawing::Size(155, 21);
            this->orderEditor->TabIndex = 3;
            this->orderEditor->TextChanged += gcnew System::EventHandler(this, &MethodUI::MethodDataChanged);
            // 
            // nameEditor
            // 
            this->nameEditor->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
            this->nameEditor->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->nameEditor->FormattingEnabled = true;
            this->nameEditor->Location = System::Drawing::Point(4, 3);
            this->nameEditor->Name = L"nameEditor";
            this->nameEditor->Size = System::Drawing::Size(160, 21);
            this->nameEditor->TabIndex = 1;
            this->nameEditor->TextChanged += gcnew System::EventHandler(this, &MethodUI::MethodDataChanged);
            // 
            // callingsPage
            // 
            callingsPage->Controls->Add(callingsPanel);
            callingsPage->Location = System::Drawing::Point(4, 22);
            callingsPage->Name = L"callingsPage";
            callingsPage->Padding = System::Windows::Forms::Padding(3);
            callingsPage->Size = System::Drawing::Size(524, 83);
            callingsPage->TabIndex = 2;
            callingsPage->Text = L"Callings";
            callingsPage->UseVisualStyleBackColor = true;
            // 
            // callingsPanel
            // 
            callingsPanel->Controls->Add(altCallingGroup);
            callingsPanel->Controls->Add(normalCallingGroup);
            callingsPanel->Dock = System::Windows::Forms::DockStyle::Fill;
            callingsPanel->Location = System::Drawing::Point(3, 3);
            callingsPanel->Margin = System::Windows::Forms::Padding(0);
            callingsPanel->Name = L"callingsPanel";
            callingsPanel->Size = System::Drawing::Size(518, 77);
            callingsPanel->TabIndex = 0;
            // 
            // altCallingGroup
            // 
            altCallingGroup->Controls->Add(this->altSingleNotationEditor);
            altCallingGroup->Controls->Add(this->altBobNotationEditor);
            altCallingGroup->Controls->Add(altSingleLbl);
            altCallingGroup->Controls->Add(altBobLbl);
            altCallingGroup->Location = System::Drawing::Point(195, 3);
            altCallingGroup->Name = L"altCallingGroup";
            altCallingGroup->Size = System::Drawing::Size(186, 50);
            altCallingGroup->TabIndex = 15;
            altCallingGroup->TabStop = false;
            altCallingGroup->Text = L"Alternative calling";
            // 
            // altSingleNotationEditor
            // 
            this->altSingleNotationEditor->Location = System::Drawing::Point(124, 19);
            this->altSingleNotationEditor->Name = L"altSingleNotationEditor";
            this->altSingleNotationEditor->Size = System::Drawing::Size(40, 20);
            this->altSingleNotationEditor->TabIndex = 19;
            this->altSingleNotationEditor->TextChanged += gcnew System::EventHandler(this, &MethodUI::AltSingleNotationEditor_TextChanged);
            // 
            // altBobNotationEditor
            // 
            this->altBobNotationEditor->Location = System::Drawing::Point(39, 19);
            this->altBobNotationEditor->Name = L"altBobNotationEditor";
            this->altBobNotationEditor->Size = System::Drawing::Size(40, 20);
            this->altBobNotationEditor->TabIndex = 18;
            this->altBobNotationEditor->TextChanged += gcnew System::EventHandler(this, &MethodUI::AltBobNotationEditor_TextChanged);
            // 
            // altSingleLbl
            // 
            altSingleLbl->AutoSize = true;
            altSingleLbl->Location = System::Drawing::Point(84, 22);
            altSingleLbl->Name = L"altSingleLbl";
            altSingleLbl->Size = System::Drawing::Size(39, 13);
            altSingleLbl->TabIndex = 12;
            altSingleLbl->Text = L"Single:";
            // 
            // altBobLbl
            // 
            altBobLbl->AutoSize = true;
            altBobLbl->Location = System::Drawing::Point(11, 22);
            altBobLbl->Name = L"altBobLbl";
            altBobLbl->Size = System::Drawing::Size(29, 13);
            altBobLbl->TabIndex = 10;
            altBobLbl->Text = L"Bob:";
            // 
            // normalCallingGroup
            // 
            normalCallingGroup->Controls->Add(this->singleNotationEditor);
            normalCallingGroup->Controls->Add(this->bobNotationEditor);
            normalCallingGroup->Controls->Add(singleLbl);
            normalCallingGroup->Controls->Add(bobLbl);
            normalCallingGroup->Location = System::Drawing::Point(3, 3);
            normalCallingGroup->Name = L"normalCallingGroup";
            normalCallingGroup->Size = System::Drawing::Size(186, 50);
            normalCallingGroup->TabIndex = 14;
            normalCallingGroup->TabStop = false;
            normalCallingGroup->Text = L"Normal calling";
            // 
            // singleNotationEditor
            // 
            this->singleNotationEditor->Location = System::Drawing::Point(124, 19);
            this->singleNotationEditor->Name = L"singleNotationEditor";
            this->singleNotationEditor->Size = System::Drawing::Size(40, 20);
            this->singleNotationEditor->TabIndex = 17;
            this->singleNotationEditor->TextChanged += gcnew System::EventHandler(this, &MethodUI::SinglePlaceNotationChanged);
            // 
            // bobNotationEditor
            // 
            this->bobNotationEditor->Location = System::Drawing::Point(39, 19);
            this->bobNotationEditor->Name = L"bobNotationEditor";
            this->bobNotationEditor->Size = System::Drawing::Size(40, 20);
            this->bobNotationEditor->TabIndex = 16;
            this->bobNotationEditor->TextChanged += gcnew System::EventHandler(this, &MethodUI::BobPlaceNotationChanged);
            // 
            // singleLbl
            // 
            singleLbl->AutoSize = true;
            singleLbl->Location = System::Drawing::Point(84, 22);
            singleLbl->Name = L"singleLbl";
            singleLbl->Size = System::Drawing::Size(39, 13);
            singleLbl->TabIndex = 12;
            singleLbl->Text = L"Single:";
            // 
            // bobLbl
            // 
            bobLbl->AutoSize = true;
            bobLbl->Location = System::Drawing::Point(11, 22);
            bobLbl->Name = L"bobLbl";
            bobLbl->Size = System::Drawing::Size(29, 13);
            bobLbl->TabIndex = 10;
            bobLbl->Text = L"Bob:";
            // 
            // buttonPanel
            // 
            this->buttonPanel->AutoSize = true;
            this->buttonPanel->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            this->buttonPanel->Controls->Add(this->saveBtn);
            this->buttonPanel->Controls->Add(this->closeBtn);
            this->buttonPanel->Controls->Add(this->startBtn);
            this->buttonPanel->Controls->Add(this->back10Btn);
            this->buttonPanel->Controls->Add(this->previousBtn);
            this->buttonPanel->Controls->Add(this->nextBtn);
            this->buttonPanel->Controls->Add(this->forward10Btn);
            this->buttonPanel->Controls->Add(this->endBtn);
            this->buttonPanel->Controls->Add(this->editBtn);
            this->buttonPanel->Controls->Add(this->cancelBtn);
            this->buttonPanel->Controls->Add(this->onlineBtn);
            this->buttonPanel->Dock = System::Windows::Forms::DockStyle::Fill;
            this->buttonPanel->Location = System::Drawing::Point(0, 140);
            this->buttonPanel->Margin = System::Windows::Forms::Padding(0);
            this->buttonPanel->Name = L"buttonPanel";
            this->buttonPanel->Size = System::Drawing::Size(538, 28);
            this->buttonPanel->TabIndex = 1;
            // 
            // saveBtn
            // 
            this->saveBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->saveBtn->Location = System::Drawing::Point(373, 3);
            this->saveBtn->Name = L"saveBtn";
            this->saveBtn->Size = System::Drawing::Size(75, 23);
            this->saveBtn->TabIndex = 14;
            this->saveBtn->Text = L"&Save";
            this->saveBtn->UseVisualStyleBackColor = true;
            this->saveBtn->Click += gcnew System::EventHandler(this, &MethodUI::SaveBtn_Click);
            // 
            // closeBtn
            // 
            this->closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            this->closeBtn->Location = System::Drawing::Point(454, 3);
            this->closeBtn->Name = L"closeBtn";
            this->closeBtn->Size = System::Drawing::Size(75, 23);
            this->closeBtn->TabIndex = 15;
            this->closeBtn->Text = L"Cl&ose";
            this->closeBtn->UseVisualStyleBackColor = true;
            this->closeBtn->Click += gcnew System::EventHandler(this, &MethodUI::CloseBtn_Click);
            // 
            // startBtn
            // 
            this->startBtn->Location = System::Drawing::Point(1, 3);
            this->startBtn->Name = L"startBtn";
            this->startBtn->Size = System::Drawing::Size(31, 23);
            this->startBtn->TabIndex = 5;
            this->startBtn->Text = L"|<";
            this->startBtn->UseVisualStyleBackColor = true;
            this->startBtn->Click += gcnew System::EventHandler(this, &MethodUI::StartBtn_Click);
            // 
            // back10Btn
            // 
            this->back10Btn->Location = System::Drawing::Point(32, 3);
            this->back10Btn->Name = L"back10Btn";
            this->back10Btn->Size = System::Drawing::Size(31, 23);
            this->back10Btn->TabIndex = 6;
            this->back10Btn->Text = L"<<";
            this->back10Btn->UseVisualStyleBackColor = true;
            this->back10Btn->Click += gcnew System::EventHandler(this, &MethodUI::Back10Btn_Click);
            // 
            // previousBtn
            // 
            this->previousBtn->Location = System::Drawing::Point(63, 3);
            this->previousBtn->Name = L"previousBtn";
            this->previousBtn->Size = System::Drawing::Size(31, 23);
            this->previousBtn->TabIndex = 6;
            this->previousBtn->Text = L"<";
            this->previousBtn->UseVisualStyleBackColor = true;
            this->previousBtn->Click += gcnew System::EventHandler(this, &MethodUI::PreviousBtn_Click);
            // 
            // nextBtn
            // 
            this->nextBtn->Location = System::Drawing::Point(106, 3);
            this->nextBtn->Name = L"nextBtn";
            this->nextBtn->Size = System::Drawing::Size(31, 23);
            this->nextBtn->TabIndex = 7;
            this->nextBtn->Text = L">";
            this->nextBtn->UseVisualStyleBackColor = true;
            this->nextBtn->Click += gcnew System::EventHandler(this, &MethodUI::NextBtn_Click);
            // 
            // forward10Btn
            // 
            this->forward10Btn->Location = System::Drawing::Point(137, 3);
            this->forward10Btn->Name = L"forward10Btn";
            this->forward10Btn->Size = System::Drawing::Size(31, 23);
            this->forward10Btn->TabIndex = 8;
            this->forward10Btn->Text = L">>";
            this->forward10Btn->UseVisualStyleBackColor = true;
            this->forward10Btn->Click += gcnew System::EventHandler(this, &MethodUI::Forward10Btn_Click);
            // 
            // endBtn
            // 
            this->endBtn->Location = System::Drawing::Point(168, 3);
            this->endBtn->Name = L"endBtn";
            this->endBtn->Size = System::Drawing::Size(31, 23);
            this->endBtn->TabIndex = 11;
            this->endBtn->Text = L">|";
            this->endBtn->UseVisualStyleBackColor = true;
            this->endBtn->Click += gcnew System::EventHandler(this, &MethodUI::EndBtn_Click);
            // 
            // editBtn
            // 
            this->editBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->editBtn->Location = System::Drawing::Point(373, 3);
            this->editBtn->Name = L"editBtn";
            this->editBtn->Size = System::Drawing::Size(75, 23);
            this->editBtn->TabIndex = 13;
            this->editBtn->Text = L"&Edit";
            this->editBtn->UseVisualStyleBackColor = true;
            this->editBtn->Click += gcnew System::EventHandler(this, &MethodUI::EditBtn_Click);
            // 
            // cancelBtn
            // 
            this->cancelBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->cancelBtn->Location = System::Drawing::Point(454, 3);
            this->cancelBtn->Name = L"cancelBtn";
            this->cancelBtn->Size = System::Drawing::Size(75, 23);
            this->cancelBtn->TabIndex = 12;
            this->cancelBtn->Text = L"Can&cel";
            this->cancelBtn->UseVisualStyleBackColor = true;
            this->cancelBtn->Click += gcnew System::EventHandler(this, &MethodUI::CancelBtn_Click);
            // 
            // onlineBtn
            // 
            this->onlineBtn->Location = System::Drawing::Point(233, 3);
            this->onlineBtn->Name = L"onlineBtn";
            this->onlineBtn->Size = System::Drawing::Size(75, 23);
            this->onlineBtn->TabIndex = 21;
            this->onlineBtn->Text = L"Online";
            this->onlineBtn->UseVisualStyleBackColor = true;
            this->onlineBtn->Visible = false;
            this->onlineBtn->Click += gcnew System::EventHandler(this, &MethodUI::OnlineBtn_Click);
            // 
            // methodPicture
            // 
            this->methodPicture->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
            this->methodPicture->ContextMenuStrip = this->pictureMenu;
            this->methodPicture->Dock = System::Windows::Forms::DockStyle::Fill;
            this->methodPicture->ImageLocation = L"";
            this->methodPicture->Location = System::Drawing::Point(3, 171);
            this->methodPicture->Name = L"methodPicture";
            this->methodPicture->Size = System::Drawing::Size(532, 1);
            this->methodPicture->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
            this->methodPicture->TabIndex = 6;
            this->methodPicture->TabStop = false;
            // 
            // pictureMenu
            // 
            this->pictureMenu->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->copyToClipboardCmd });
            this->pictureMenu->Name = L"pictureMenu";
            this->pictureMenu->Size = System::Drawing::Size(103, 26);
            // 
            // copyToClipboardCmd
            // 
            this->copyToClipboardCmd->Name = L"copyToClipboardCmd";
            this->copyToClipboardCmd->Size = System::Drawing::Size(102, 22);
            this->copyToClipboardCmd->Text = L"Copy";
            this->copyToClipboardCmd->Click += gcnew System::EventHandler(this, &MethodUI::CopyToClipboardCmd_Click);
            // 
            // statusStrip1
            // 
            statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) { this->isSplicedLbl, this->methodIdLabel });
            statusStrip1->Location = System::Drawing::Point(0, 173);
            statusStrip1->Name = L"statusStrip1";
            statusStrip1->Size = System::Drawing::Size(538, 22);
            statusStrip1->TabIndex = 23;
            statusStrip1->Text = L"statusStrip1";
            // 
            // isSplicedLbl
            // 
            this->isSplicedLbl->Name = L"isSplicedLbl";
            this->isSplicedLbl->Size = System::Drawing::Size(0, 17);
            // 
            // methodIdLabel
            // 
            this->methodIdLabel->Name = L"methodIdLabel";
            this->methodIdLabel->Size = System::Drawing::Size(523, 17);
            this->methodIdLabel->Spring = true;
            this->methodIdLabel->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            this->methodIdLabel->Click += gcnew System::EventHandler(this, &MethodUI::MethodIdLabel_Click);
            // 
            // menuStrip1
            // 
            menuStrip1->AllowMerge = false;
            menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
                this->printCmd, this->statsCmd,
                    this->compLibToolStripMenuItem
            });
            menuStrip1->Location = System::Drawing::Point(0, 0);
            menuStrip1->Name = L"menuStrip1";
            menuStrip1->Size = System::Drawing::Size(538, 24);
            menuStrip1->TabIndex = 24;
            menuStrip1->Text = L"menuStrip1";
            // 
            // printCmd
            // 
            this->printCmd->Enabled = false;
            this->printCmd->Name = L"printCmd";
            this->printCmd->Size = System::Drawing::Size(44, 20);
            this->printCmd->Text = L"Print";
            this->printCmd->Click += gcnew System::EventHandler(this, &MethodUI::PrintCmd_Click);
            // 
            // statsCmd
            // 
            this->statsCmd->Name = L"statsCmd";
            this->statsCmd->Size = System::Drawing::Size(65, 20);
            this->statsCmd->Text = L"Statistics";
            this->statsCmd->Click += gcnew System::EventHandler(this, &MethodUI::StatsCmd_Click);
            // 
            // lineGenerator
            // 
            this->lineGenerator->WorkerSupportsCancellation = true;
            // 
            // compLibToolStripMenuItem
            // 
            this->compLibToolStripMenuItem->Name = L"compLibToolStripMenuItem";
            this->compLibToolStripMenuItem->Size = System::Drawing::Size(71, 20);
            this->compLibToolStripMenuItem->Text = L"Comp Lib";
            this->compLibToolStripMenuItem->Click += gcnew System::EventHandler(this, &MethodUI::compLibToolStripMenuItem_Click);
            // 
            // MethodUI
            // 
            this->AcceptButton = this->saveBtn;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->CancelButton = this->closeBtn;
            this->ClientSize = System::Drawing::Size(538, 195);
            this->Controls->Add(statusStrip1);
            this->Controls->Add(menuStrip1);
            this->Controls->Add(tableLayoutPanel1);
            this->MainMenuStrip = menuStrip1;
            this->MinimumSize = System::Drawing::Size(554, 228);
            this->Name = L"MethodUI";
            this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Show;
            this->Text = L"Method";
            this->Load += gcnew System::EventHandler(this, &MethodUI::MethodUI_Load);
            tableLayoutPanel1->ResumeLayout(false);
            tableLayoutPanel1->PerformLayout();
            tabControl1->ResumeLayout(false);
            this->detailsPage->ResumeLayout(false);
            this->detailsPage->PerformLayout();
            this->methodDetailPanel->ResumeLayout(false);
            this->methodDetailPanel->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->drawFrom))->EndInit();
            callingsPage->ResumeLayout(false);
            callingsPanel->ResumeLayout(false);
            altCallingGroup->ResumeLayout(false);
            altCallingGroup->PerformLayout();
            normalCallingGroup->ResumeLayout(false);
            normalCallingGroup->PerformLayout();
            this->buttonPanel->ResumeLayout(false);
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->methodPicture))->EndInit();
            this->pictureMenu->ResumeLayout(false);
            statusStrip1->ResumeLayout(false);
            statusStrip1->PerformLayout();
            menuStrip1->ResumeLayout(false);
            menuStrip1->PerformLayout();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
    #pragma endregion
    };
}
