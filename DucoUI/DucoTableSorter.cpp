#include "DucoTableSorter.h"

#include "TableSorterByCount.h"
#include "TableSorterByDate.h"
#include "TableSorterByDistance.h"
#include "TableSorterByFloat.h"
#include "TableSorterByTime.h"
#include "TableSorterByString.h"
#include "TableSorterByHeaderCell.h"

using namespace DucoUI;
using namespace System;
using namespace System::Windows::Forms;

DucoTableSorter::DucoTableSorter(bool headerCellAscending, TSortType newHeaderCellSortType)
{
    sorters = gcnew System::Collections::ArrayList();
    headerCellSort = CreateSortObject(newHeaderCellSortType, headerCellAscending, -1);
}

DucoTableSorter::~DucoTableSorter()
{
}

System::Collections::IComparer^
DucoTableSorter::AddSortObject(TSortType objectType, bool ascending, int cellNumber)
{
    System::Collections::IComparer^ newSorter = CreateSortObject(objectType, ascending, cellNumber);
    sorters->Add(newSorter);
    return newSorter;
}

System::Collections::IComparer^
DucoTableSorter::CreateSortObject(TSortType sortType, bool ascending, int cellNumber)
{
    System::Collections::IComparer^ newComparer = nullptr;
    switch (sortType)
    {
        case EHeaderCell:
            newComparer = gcnew TableSorterByHeaderCell(ascending);
            break;
        case ETime:
            newComparer = gcnew TableSorterByTime(ascending, cellNumber);
            break;
        case ECount:
            newComparer = gcnew TableSorterByCount(ascending, cellNumber);
            break;
        case EDate:
            newComparer = gcnew TableSorterByDate(ascending, cellNumber);
            break;
        case EFloat:
            newComparer = gcnew TableSorterByFloat(ascending, cellNumber);
            break;
        case EDistance:
            newComparer = gcnew TableSorterByDistance(ascending, cellNumber);
            break;
        case EString:
        default:
            newComparer = gcnew TableSorterByString(ascending, cellNumber);
            break;
    }

    return newComparer;
}

int 
DucoTableSorter::Compare(System::Object^ firstObject, System::Object^ secondObject)
{
    int result (0);

    System::Collections::IEnumerator^ myEnum = sorters->GetEnumerator();
    while (result == 0 && myEnum->MoveNext())
    {
        System::Collections::IComparer^ obj = safe_cast<System::Collections::IComparer^>(myEnum->Current);
        result = obj->Compare(firstObject, secondObject);
    }

    if (result == 0)
    {
        result = headerCellSort->Compare(firstObject, secondObject);
    }

    return result;
}
