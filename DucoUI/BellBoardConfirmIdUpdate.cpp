#include <DownloadedPeal.h>
#include <Peal.h>
#include "DatabaseManager.h"

#include "BellBoardConfirmIdUpdate.h"

#include "DatabaseSettings.h"
#include "DucoUtils.h"
#include <DucoEngineUtils.h>

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace DucoUI;
using namespace Duco;


BellBoardConfirmIdUpdate::BellBoardConfirmIdUpdate(DucoUI::DatabaseManager^ theDatabase)
    : database(theDatabase)
{
    InitializeComponent();
}

BellBoardConfirmIdUpdate::~BellBoardConfirmIdUpdate()
{
    if (components)
    {
        delete components;
    }
}

System::Void
BellBoardConfirmIdUpdate::PopulateView(const Duco::DownloadedPeal& downloadedPeal, const Duco::Peal& existingPeal)
{
    // Set up downloaded peal
    //AddLabel(DucoUtils::ConvertString(downloadedPeal.Id().Str()), 1, 0);
    AddLabel(DucoUtils::ConvertString(DucoEngineUtils::ToString(downloadedPeal.NoOfChanges())), 2, 0);
    AddLabel(database->FullName(Duco::TObjectType::EMethod, downloadedPeal.MethodId()), 3, 0);
    AddLabel(DucoUtils::PrintPealTime(downloadedPeal.Time(), false), 4, 0);
    AddLabel(DucoUtils::ConvertString(downloadedPeal.Date(), false), 5, 0);
    AddLabel(database->FullName(Duco::TObjectType::ETower, downloadedPeal.TowerId()), 6, 0);
    AddLabel(DucoUtils::ConvertString(downloadedPeal.ConductorName(database->Database())), 7, 0);
    AddClickableLabel(DucoUtils::ConvertString(downloadedPeal.BellBoardId()), 8, 0);
    AddLabel(DucoUtils::ConvertString(downloadedPeal.RingingWorldReference()), 9, 0);

    // Set up peal already in database
    AddLabel(DucoUtils::ConvertString(existingPeal.Id().Str()), 1, 1);
    AddLabel(DucoUtils::ConvertString(DucoEngineUtils::ToString(existingPeal.NoOfChanges())), 2, 1);
    AddLabel(database->FullName(Duco::TObjectType::EMethod, existingPeal.MethodId()), 3, 1);
    AddLabel(DucoUtils::PrintPealTime(existingPeal.Time(), false), 4, 1);
    AddLabel(DucoUtils::ConvertString(existingPeal.Date(), false), 5, 1);
    AddLabel(database->FullName(Duco::TObjectType::ETower, existingPeal.TowerId()), 6, 1);
    AddLabel(DucoUtils::ConvertString(existingPeal.ConductorName(database->Database())), 7, 1);
    AddClickableLabel(DucoUtils::ConvertString(existingPeal.BellBoardId()), 8, 1);
    AddLabel(DucoUtils::ConvertString(existingPeal.RingingWorldReference()), 9, 1);
}

System::Void
BellBoardConfirmIdUpdate::AddLabel(System::String^ text, System::Int32 column, System::Int32 row)
{
    Label^ label = (gcnew System::Windows::Forms::Label());
    layoutPanel->Controls->Add(label, column, row);
    label->AutoSize = true;
    label->Location = System::Drawing::Point(3, 32);
    label->Size = System::Drawing::Size(35, 13);
    label->Text = text;
    label->TextAlign = System::Drawing::ContentAlignment::TopRight;
}

System::Void
BellBoardConfirmIdUpdate::AddClickableLabel(System::String^ bellboardId, System::Int32 column, System::Int32 row)
{
    if (bellboardId->Length > 0)
    {
        LinkLabel^ label = (gcnew System::Windows::Forms::LinkLabel());
        layoutPanel->Controls->Add(label, column, row);
        label->AutoSize = true;
        label->Location = System::Drawing::Point(3, 32);
        label->Size = System::Drawing::Size(35, 13);
        label->Text = bellboardId;
        label->Text = bellboardId;
        label->TextAlign = System::Drawing::ContentAlignment::TopRight;
        label->LinkClicked += gcnew System::Windows::Forms::LinkLabelLinkClickedEventHandler(this, &BellBoardConfirmIdUpdate::linkLabel_LinkClicked);
    }
}

System::Void
BellBoardConfirmIdUpdate::linkLabel_LinkClicked(System::Object^ sender, System::Windows::Forms::LinkLabelLinkClickedEventArgs^ e)
{
    System::Windows::Forms::LinkLabel^ theLabel = static_cast<System::Windows::Forms::LinkLabel^>(sender);
    // Specify that the link was visited.
    theLabel->LinkVisited = true;

    System::String^ link = DucoUtils::ConvertString(database->Settings().BellBoardURL());
    link += DucoUtils::ConvertString(database->Settings().BellBoardViewSuffixURL());
    link += theLabel->Text;

    // Navigate to a URL.
    System::Diagnostics::Process::Start(link);
}

System::Void
BellBoardConfirmIdUpdate::cancelButton_Click(System::Object^ sender, System::EventArgs^ e)
{
    Close();
}
