#include <StatisticFilters.h>
#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"

#include "BellsRungUI.h"

#include "GenericTablePrintUtil.h"
#include "DucoUtils.h"
#include "DucoUIUtils.h"
#include "SystemDefaultIcon.h"
#include "FiltersUI.h"
#include <RingingDatabase.h>
#include <Peal.h>
#include <PealDatabase.h>
#include <DatabaseSettings.h>
#include <DucoEngineUtils.h>

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace DucoUI;
using namespace Duco;

BellsRungUI::BellsRungUI(DucoUI::DatabaseManager^ theDatabase)
: database(theDatabase), loading (true), restart(false)
{
    filters = new Duco::StatisticFilters(database->Database());
    filters->SetDefaultRinger();
    InitializeComponent();
}

BellsRungUI::!BellsRungUI()
{
    delete filters;
}

BellsRungUI::~BellsRungUI()
{
    if (components)
    {
        delete components;
    }
}

System::Void
BellsRungUI::BellsRungUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    loading = false;
    StartGeneration();
}

System::Void
BellsRungUI::stageCtrl_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    StartGeneration();
}

System::Void
BellsRungUI::ringerCtrl_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    StartGeneration();
}

System::Void
BellsRungUI::towerCtrl_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    StartGeneration();
}

System::Void
BellsRungUI::fromBack_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    StartGeneration();
}

System::Void
BellsRungUI::methodCtrl_TextUpdate(System::Object^  sender, System::EventArgs^  e)
{
    StartGeneration();
}

System::Void
BellsRungUI::filtersBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    FiltersUI^ filtersDialog = gcnew FiltersUI(*filters, database);
    System::Windows::Forms::DialogResult returnVal = filtersDialog->ShowDialog(this);
    if (returnVal == System::Windows::Forms::DialogResult::OK)
    {
        StartGeneration();
    }
}

System::Void 
BellsRungUI::BellsRungUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e)
{
    if (backgroundWorker1->IsBusy)
    {
        backgroundWorker1->CancelAsync();
        e->Cancel = true;
    }
}


ref class BellsRungUIArgs
{
public:
    System::Boolean            fromBack;
};

System::Void
BellsRungUI::StartGeneration()
{
    if (!loading)
    {
        if (backgroundWorker1->IsBusy)
        {
            backgroundWorker1->CancelAsync();
            restart = true;
        }
        else
        {
            restart = false;
            totalPeals = 0;
            dataGridView1->Rows->Clear();

            BellsRungUIArgs^ args = gcnew BellsRungUIArgs;
            args->fromBack = fromBackBtn->Checked;

            backgroundWorker1->RunWorkerAsync(args);
        }
    }
}

System::Void
BellsRungUI::backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
    BackgroundWorker^ worker = dynamic_cast<BackgroundWorker^>(sender);

    worker->ReportProgress(0);
    BellsRungUIArgs^ args = (BellsRungUIArgs^)e->Argument;

    std::map<unsigned int, std::map<unsigned int, Duco::PealLengthInfo> > bellRungCount;
    database->Database().PealsDatabase().GetBellsRung(*filters, bellRungCount);

    System::Collections::Generic::Dictionary<unsigned int, Boolean>^ displayColumns = gcnew System::Collections::Generic::Dictionary<unsigned int, Boolean>();
    for (unsigned int orderNo = 2; orderNo <= 20; orderNo += 2)
    {
        displayColumns->Add(orderNo, false);
    }

    const unsigned int maxBells = 20;
    for (int bellNo = 1; bellNo <= maxBells; ++bellNo)
    {
        size_t totalOnThisBell (0);
        DataGridViewRow^ newRow = gcnew DataGridViewRow();
        newRow->HeaderCell->Value = Convert::ToString(bellNo);
        DataGridViewCellCollection^ theCells = newRow->Cells;
        DataGridViewTextBoxCell^ percentageCell = gcnew DataGridViewTextBoxCell();
        theCells->Add(percentageCell);
        DataGridViewTextBoxCell^ summaryCell = gcnew DataGridViewTextBoxCell();
        theCells->Add(summaryCell);
        for (int order = 6; order <= maxBells; order += 2)
        {
            DataGridViewTextBoxCell^ nextBellCell = gcnew DataGridViewTextBoxCell();
            int offset (0);
            if (args->fromBack)
            {
                offset = maxBells - order;
            }
            if ((!args->fromBack && bellNo <= order) || (args->fromBack && (bellNo - offset) > 0) )
            {
                std::map<unsigned int, Duco::PealLengthInfo> it = bellRungCount[order];
                Duco::PealLengthInfo noOfThisBell = it[bellNo-offset];
                nextBellCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(noOfThisBell.TotalPeals(), true));
                if (noOfThisBell.TotalPeals() > 0)
                {
                    totalOnThisBell += noOfThisBell.TotalPeals();
                    if (!args->fromBack)
                    {
                        displayColumns[order] = true;
                    }
                    else
                    {
                        displayColumns[order] = true;
                    }
                }
            }
            theCells->Add(nextBellCell);
        }
        summaryCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(totalOnThisBell));
        worker->ReportProgress(bellNo / 20 * 100, newRow);
    }
    e->Result = displayColumns;
}

System::Void
BellsRungUI::backgroundWorker1_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    progressBar->Value = e->ProgressPercentage;
    if (e->UserState != nullptr)
    {
        DataGridViewRow^ nextRow = (DataGridViewRow^)e->UserState;
        dataGridView1->Rows->Add(nextRow);
        totalPeals += Convert::ToInt16(nextRow->Cells[1]->Value);
    }
}

System::Void
BellsRungUI::backgroundWorker1_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    progressBar->Value = 0;
    if (restart)
    {
        StartGeneration();
        return;
    }

    System::Collections::Generic::Dictionary<unsigned int, Boolean>^ displayColumns = (System::Collections::Generic::Dictionary<unsigned int, Boolean>^)e->Result;
    if (fromBackBtn->Checked)
    {
        bool display = displayColumns[20];
        twentyColumn->Visible = display;
        dataGridView1->Rows[0]->Visible = display;
        dataGridView1->Rows[1]->Visible = display;
        display |= displayColumns[18];
        eighteenColumn->Visible = display;
        dataGridView1->Rows[2]->Visible = display;
        dataGridView1->Rows[3]->Visible = display;
        display |= displayColumns[16];
        sixteenColumn->Visible = display;
        dataGridView1->Rows[4]->Visible = display;
        dataGridView1->Rows[5]->Visible = display;
        display |= displayColumns[14];
        fourteenColumn->Visible = display;
        dataGridView1->Rows[6]->Visible = display;
        dataGridView1->Rows[7]->Visible = display;
        display |= displayColumns[12];
        maximusColumn->Visible = display;
        dataGridView1->Rows[8]->Visible = display;
        dataGridView1->Rows[9]->Visible = display;
        display |= displayColumns[10];
        royalColumn->Visible = display;
        dataGridView1->Rows[10]->Visible = display;
        dataGridView1->Rows[11]->Visible = display;
        display |= displayColumns[8];
        majorColumn->Visible = display;
        dataGridView1->Rows[12]->Visible = display;
        dataGridView1->Rows[13]->Visible = display;
        display |= displayColumns[6];
        minorColumn->Visible = display;
        dataGridView1->Rows[14]->Visible = display;
        dataGridView1->Rows[15]->Visible = display;

        unsigned int count = dataGridView1->Rows->Count-1;
        unsigned int count2 = 1;
        dataGridView1->Rows[count--]->HeaderCell->Value = "n";
        for (; count > 0; --count)
        {
            dataGridView1->Rows[count]->HeaderCell->Value = "n-" + Convert::ToString(count2);
            ++count2;
        }
    }
    else
    {
        bool display = displayColumns[20];
        twentyColumn->Visible = display;
        dataGridView1->Rows[19]->Visible = display;
        dataGridView1->Rows[18]->Visible = display;
        display |= displayColumns[18];
        eighteenColumn->Visible = display;
        dataGridView1->Rows[17]->Visible = display;
        dataGridView1->Rows[16]->Visible = display;
        display |= displayColumns[16];
        sixteenColumn->Visible = display;
        dataGridView1->Rows[15]->Visible = display;
        dataGridView1->Rows[14]->Visible = display;
        display |= displayColumns[14];
        fourteenColumn->Visible = display;
        dataGridView1->Rows[13]->Visible = display;
        dataGridView1->Rows[12]->Visible = display;
        display |= displayColumns[12];
        maximusColumn->Visible = display;
        dataGridView1->Rows[11]->Visible = display;
        dataGridView1->Rows[10]->Visible = display;
        display |= displayColumns[10];
        royalColumn->Visible = display;
        dataGridView1->Rows[9]->Visible = display;
        dataGridView1->Rows[8]->Visible = display;
        display |= displayColumns[8];
        majorColumn->Visible = display;
        dataGridView1->Rows[7]->Visible = display;
        dataGridView1->Rows[6]->Visible = display;
        display |= displayColumns[6];
        minorColumn->Visible = display;
        dataGridView1->Rows[5]->Visible = display;
        dataGridView1->Rows[4]->Visible = display;
    }

    //Calculate percentages
    if (totalPeals > 0)
    {
        for (int rowNo = 0; rowNo <= 19; ++rowNo)
        {
            Double bellTotal = Convert::ToDouble(dataGridView1->Rows[rowNo]->Cells[1]->Value);
            Double bellPercent = bellTotal / totalPeals * 100;
            dataGridView1->Rows[rowNo]->Cells[0]->Value = DucoUI::DucoUtils::FormatDoubleToString(bellPercent, 0) +  "%";
        }
    }

    //Calculate summary row.
    DataGridViewRow^ newRow = gcnew DataGridViewRow();
    newRow->HeaderCell->Value = "Total";
    DataGridViewCellCollection^ theCells = newRow->Cells;
    DataGridViewTextBoxCell^ percentCell = gcnew DataGridViewTextBoxCell();
    theCells->Add(percentCell);
    DataGridViewTextBoxCell^ totalCell = gcnew DataGridViewTextBoxCell();
    totalCell->Value = DucoUI::DucoUtils::FormatDoubleToString(totalPeals, 0);
    theCells->Add(totalCell);
    for (Int16 i = 2; i < dataGridView1->Columns->Count; ++i)
    {
        DataGridViewTextBoxCell^ nextColumnCell = gcnew DataGridViewTextBoxCell();
        unsigned int pealCount (0);
        for (Int16 j = 0; j < dataGridView1->Rows->Count; ++j)
        {
            DataGridViewCell^ nextColumn = dataGridView1[i, j];
            pealCount += Convert::ToUInt16(nextColumn->Value);
        }
        nextColumnCell->Value = DucoUI::DucoUtils::ConvertString(DucoEngineUtils::ToString(pealCount));
        theCells->Add(nextColumnCell);
    }
    dataGridView1->Rows->Add(newRow);
}

System::Void
BellsRungUI::printBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    DucoUIUtils::Print(this, sender, gcnew System::Drawing::Printing::PrintPageEventHandler( this, &BellsRungUI::printData ), "peal speed", database->Database().Settings().UsePrintPreview(), false);
}

System::Void
BellsRungUI::printData(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args)
{
    System::Drawing::Printing::PrintDocument^ printDoc = static_cast<System::Drawing::Printing::PrintDocument^>(sender);

    String^ title = "Bells rung history for ";
    title += database->Filename();
    GenericTablePrintUtil^ printUtil = gcnew GenericTablePrintUtil(database, args, title);
    printUtil->SetObjects(dataGridView1, false);
    printUtil->printObject(args, printDoc->PrinterSettings);
}
