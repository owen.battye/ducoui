#pragma once
namespace DucoUI
{
    public ref class CompositionUI : public System::Windows::Forms::Form, public DucoUI::ProgressCallbackUI, public DucoUI::RingingDatabaseObserver
    {
    public:
        CompositionUI(DucoUI::DatabaseManager^ theDatabase);
        CompositionUI(DucoUI::DatabaseManager^ theDatabase, const Duco::ObjectId& compositionToShowId);
        static void ShowComposition(DucoUI::DatabaseManager^ theDatabase, System::Windows::Forms::Form^ parent, const Duco::ObjectId& compositionId);

        // from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

        // from ProgressCallback
        virtual void Step(int progressPercent);
        virtual void Initialised();
        virtual void Complete();

    protected:
        !CompositionUI();
        ~CompositionUI();
        System::Void ClosingEvent(System::Object^ object, System::ComponentModel::CancelEventArgs^ e);
        System::Void FormLoad(System::Object^  sender, System::EventArgs^  e);

        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void startBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void back10Btn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void previousBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void nextBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void forward10Btn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void endBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void saveBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void editBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void newBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void compositionIdLbl_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void proveBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void printBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void printComposition(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args);

        System::Void RenameColumns();
        System::Void PopulateComposition();
        System::Void GenerateComposition();

        System::Void dataGridView1_CellEndEdit(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);

        bool AddCallToComposition(System::Windows::Forms::DataGridViewCell^ currentCell, wchar_t position);
        System::Void methodSelector_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void composerSelector_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void nameEditor_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void notesEditor_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void repeatsEditor_ValueChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void snapStart_ValueChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void showAll_CheckedChanged(System::Object^  sender, System::EventArgs^  e);

        System::Void backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void backgroundWorker1_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void backgroundWorker1_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);

        void SetViewState();
        void SetEditState();
        void SetNewState();

        void ClearView();
        void PopulateView();
        void PopulateViewWithId(const Duco::ObjectId& composerId);
        System::Void UpdateIdLabel();
        void ClearAnalysis();
        void CheckSnapFinish();
        void HideColumns();

    private:
        DucoUI::ProgressCallbackWrapper*    progressWrapper;
        Duco::ObjectId*                     preNewId;
        bool                                populatingView;
        bool                                dataChanged;
        DucoUI::DatabaseManager^            database;
        System::ComponentModel::Container^  components;
        System::Windows::Forms::ComboBox^   methodSelector;
        Duco::Composition*                  composition;
        DucoUI::PrintCompositionUtil^       printUtil;
        std::set<unsigned int>*             startingChangeBells;

    private: System::Windows::Forms::CheckBox^  showAll;
    private: System::Windows::Forms::DataGridView^  dataGridView1;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  leadEndColumn;
    private: System::Windows::Forms::Button^  saveBtn;
    private: System::Windows::Forms::Button^  editBtn;
    private: System::Windows::Forms::Button^  startBtn;
    private: System::Windows::Forms::Button^  back10Btn;
    private: System::Windows::Forms::Button^  previousBtn;
    private: System::Windows::Forms::Button^  nextBtn;
    private: System::Windows::Forms::Button^  forward10Btn;
    private: System::Windows::Forms::Button^  endBtn;
    private: System::Windows::Forms::Button^  newBtn;
    private: System::Windows::Forms::TextBox^  notesEditor;
    private: System::Windows::Forms::ComboBox^  composerSelector;
    private: System::Windows::Forms::TextBox^  nameEditor;
             System::Collections::Generic::List<System::Int16>^ allMethodIds;
             DucoUI::RingerDisplayCache^ ringerCache;
    private: System::Windows::Forms::ToolStripStatusLabel^  compositionIdLbl;
    private: System::Windows::Forms::Button^  closeBtn;
    private: System::Windows::Forms::NumericUpDown^  repeatsEditor;
    private: System::Windows::Forms::Button^  proveBtn;
    private: System::Windows::Forms::Label^  changesCount;
    private: System::ComponentModel::BackgroundWorker^  generator;
    private: System::Windows::Forms::Label^  trueLbl;
    private: System::Windows::Forms::TextBox^  musicInfo;
    private: System::Windows::Forms::ToolStripProgressBar^  progressBar;
    private: System::Windows::Forms::Button^  printBtn;
    private: System::Windows::Forms::CheckBox^  snapFinish;
    private: System::Windows::Forms::TabPage^  advancedPage;
    private: System::Windows::Forms::NumericUpDown^  snapStart;
    private: System::Windows::Forms::Label^  finishingRoundsLbl;
    private: System::Windows::Forms::TabControl^  tabControl1;

         DucoWindowState                                    windowState;

#pragma region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        void InitializeComponent(void)
        {
            System::Windows::Forms::Label^  methodLbl;
            System::Windows::Forms::GroupBox^  groupBox1;
            System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
            System::Windows::Forms::Panel^  panel1;
            System::Windows::Forms::Panel^  panel2;
            System::Windows::Forms::Label^  changesLbl;
            System::Windows::Forms::Label^  repeatsLbl;
            System::Windows::Forms::Label^  composerLbl;
            System::Windows::Forms::Label^  nameLbl;
            System::Windows::Forms::TabPage^  compositionPage;
            System::Windows::Forms::TabPage^  analysisPage;
            System::Windows::Forms::TableLayoutPanel^  analysisLayout;
            System::Windows::Forms::Label^  label1;
            System::Windows::Forms::Label^  snapFinishRoundsLbl;
            System::Windows::Forms::Label^  snapFinishLbl;
            System::Windows::Forms::Label^  snapStartRoundsLbl;
            System::Windows::Forms::Label^  snapStartChangesLbl;
            System::Windows::Forms::Label^  snapStartLbl;
            System::Windows::Forms::StatusStrip^  statusStrip1;
            System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(CompositionUI::typeid));
            this->showAll = (gcnew System::Windows::Forms::CheckBox());
            this->methodSelector = (gcnew System::Windows::Forms::ComboBox());
            this->notesEditor = (gcnew System::Windows::Forms::TextBox());
            this->printBtn = (gcnew System::Windows::Forms::Button());
            this->proveBtn = (gcnew System::Windows::Forms::Button());
            this->newBtn = (gcnew System::Windows::Forms::Button());
            this->saveBtn = (gcnew System::Windows::Forms::Button());
            this->startBtn = (gcnew System::Windows::Forms::Button());
            this->back10Btn = (gcnew System::Windows::Forms::Button());
            this->previousBtn = (gcnew System::Windows::Forms::Button());
            this->nextBtn = (gcnew System::Windows::Forms::Button());
            this->forward10Btn = (gcnew System::Windows::Forms::Button());
            this->endBtn = (gcnew System::Windows::Forms::Button());
            this->editBtn = (gcnew System::Windows::Forms::Button());
            this->closeBtn = (gcnew System::Windows::Forms::Button());
            this->snapFinish = (gcnew System::Windows::Forms::CheckBox());
            this->changesCount = (gcnew System::Windows::Forms::Label());
            this->repeatsEditor = (gcnew System::Windows::Forms::NumericUpDown());
            this->composerSelector = (gcnew System::Windows::Forms::ComboBox());
            this->nameEditor = (gcnew System::Windows::Forms::TextBox());
            this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
            this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
            this->leadEndColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->musicInfo = (gcnew System::Windows::Forms::TextBox());
            this->trueLbl = (gcnew System::Windows::Forms::Label());
            this->advancedPage = (gcnew System::Windows::Forms::TabPage());
            this->finishingRoundsLbl = (gcnew System::Windows::Forms::Label());
            this->snapStart = (gcnew System::Windows::Forms::NumericUpDown());
            this->progressBar = (gcnew System::Windows::Forms::ToolStripProgressBar());
            this->compositionIdLbl = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->generator = (gcnew System::ComponentModel::BackgroundWorker());
            methodLbl = (gcnew System::Windows::Forms::Label());
            groupBox1 = (gcnew System::Windows::Forms::GroupBox());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            panel1 = (gcnew System::Windows::Forms::Panel());
            panel2 = (gcnew System::Windows::Forms::Panel());
            changesLbl = (gcnew System::Windows::Forms::Label());
            repeatsLbl = (gcnew System::Windows::Forms::Label());
            composerLbl = (gcnew System::Windows::Forms::Label());
            nameLbl = (gcnew System::Windows::Forms::Label());
            compositionPage = (gcnew System::Windows::Forms::TabPage());
            analysisPage = (gcnew System::Windows::Forms::TabPage());
            analysisLayout = (gcnew System::Windows::Forms::TableLayoutPanel());
            label1 = (gcnew System::Windows::Forms::Label());
            snapFinishRoundsLbl = (gcnew System::Windows::Forms::Label());
            snapFinishLbl = (gcnew System::Windows::Forms::Label());
            snapStartRoundsLbl = (gcnew System::Windows::Forms::Label());
            snapStartChangesLbl = (gcnew System::Windows::Forms::Label());
            snapStartLbl = (gcnew System::Windows::Forms::Label());
            statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            groupBox1->SuspendLayout();
            tableLayoutPanel1->SuspendLayout();
            panel1->SuspendLayout();
            panel2->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->repeatsEditor))->BeginInit();
            this->tabControl1->SuspendLayout();
            compositionPage->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
            analysisPage->SuspendLayout();
            analysisLayout->SuspendLayout();
            this->advancedPage->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->snapStart))->BeginInit();
            statusStrip1->SuspendLayout();
            this->SuspendLayout();
            // 
            // methodLbl
            // 
            methodLbl->AutoSize = true;
            methodLbl->Location = System::Drawing::Point(3, 7);
            methodLbl->Margin = System::Windows::Forms::Padding(3, 7, 3, 0);
            methodLbl->Name = L"methodLbl";
            methodLbl->Size = System::Drawing::Size(43, 13);
            methodLbl->TabIndex = 2;
            methodLbl->Text = L"Method";
            // 
            // groupBox1
            // 
            groupBox1->Controls->Add(this->showAll);
            groupBox1->Dock = System::Windows::Forms::DockStyle::Fill;
            groupBox1->Location = System::Drawing::Point(412, 29);
            groupBox1->Name = L"groupBox1";
            groupBox1->Size = System::Drawing::Size(207, 43);
            groupBox1->TabIndex = 3;
            groupBox1->TabStop = false;
            groupBox1->Text = L"Settings";
            // 
            // showAll
            // 
            this->showAll->AutoSize = true;
            this->showAll->Location = System::Drawing::Point(6, 19);
            this->showAll->Name = L"showAll";
            this->showAll->Size = System::Drawing::Size(110, 17);
            this->showAll->TabIndex = 2;
            this->showAll->Text = L"Show all positions";
            this->showAll->UseVisualStyleBackColor = true;
            this->showAll->CheckedChanged += gcnew System::EventHandler(this, &CompositionUI::showAll_CheckedChanged);
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 3;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute, 50)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute, 213)));
            tableLayoutPanel1->Controls->Add(this->methodSelector, 1, 0);
            tableLayoutPanel1->Controls->Add(methodLbl, 0, 0);
            tableLayoutPanel1->Controls->Add(groupBox1, 2, 1);
            tableLayoutPanel1->Controls->Add(this->notesEditor, 2, 3);
            tableLayoutPanel1->Controls->Add(panel1, 0, 4);
            tableLayoutPanel1->Controls->Add(panel2, 2, 2);
            tableLayoutPanel1->Controls->Add(this->tabControl1, 0, 1);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 5;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 26)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 49)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 103)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 48)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
            tableLayoutPanel1->Size = System::Drawing::Size(622, 350);
            tableLayoutPanel1->TabIndex = 1;
            // 
            // methodSelector
            // 
            this->methodSelector->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
            this->methodSelector->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            tableLayoutPanel1->SetColumnSpan(this->methodSelector, 2);
            this->methodSelector->Dock = System::Windows::Forms::DockStyle::Fill;
            this->methodSelector->FormattingEnabled = true;
            this->methodSelector->Location = System::Drawing::Point(53, 3);
            this->methodSelector->Name = L"methodSelector";
            this->methodSelector->Size = System::Drawing::Size(566, 21);
            this->methodSelector->TabIndex = 1;
            this->methodSelector->SelectedIndexChanged += gcnew System::EventHandler(this, &CompositionUI::methodSelector_SelectedIndexChanged);
            // 
            // notesEditor
            // 
            this->notesEditor->Dock = System::Windows::Forms::DockStyle::Fill;
            this->notesEditor->Location = System::Drawing::Point(412, 181);
            this->notesEditor->Multiline = true;
            this->notesEditor->Name = L"notesEditor";
            this->notesEditor->Size = System::Drawing::Size(207, 118);
            this->notesEditor->TabIndex = 7;
            this->notesEditor->TextChanged += gcnew System::EventHandler(this, &CompositionUI::notesEditor_TextChanged);
            // 
            // panel1
            // 
            tableLayoutPanel1->SetColumnSpan(panel1, 3);
            panel1->Controls->Add(this->printBtn);
            panel1->Controls->Add(this->proveBtn);
            panel1->Controls->Add(this->newBtn);
            panel1->Controls->Add(this->saveBtn);
            panel1->Controls->Add(this->startBtn);
            panel1->Controls->Add(this->back10Btn);
            panel1->Controls->Add(this->previousBtn);
            panel1->Controls->Add(this->nextBtn);
            panel1->Controls->Add(this->forward10Btn);
            panel1->Controls->Add(this->endBtn);
            panel1->Controls->Add(this->editBtn);
            panel1->Controls->Add(this->closeBtn);
            panel1->Dock = System::Windows::Forms::DockStyle::Fill;
            panel1->Location = System::Drawing::Point(0, 302);
            panel1->Margin = System::Windows::Forms::Padding(0);
            panel1->Name = L"panel1";
            panel1->Size = System::Drawing::Size(622, 48);
            panel1->TabIndex = 5;
            // 
            // printBtn
            // 
            this->printBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->printBtn->Location = System::Drawing::Point(212, 2);
            this->printBtn->Name = L"printBtn";
            this->printBtn->Size = System::Drawing::Size(75, 23);
            this->printBtn->TabIndex = 12;
            this->printBtn->Text = L"Print";
            this->printBtn->UseVisualStyleBackColor = true;
            this->printBtn->Click += gcnew System::EventHandler(this, &CompositionUI::printBtn_Click);
            // 
            // proveBtn
            // 
            this->proveBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->proveBtn->Location = System::Drawing::Point(293, 2);
            this->proveBtn->Name = L"proveBtn";
            this->proveBtn->Size = System::Drawing::Size(75, 23);
            this->proveBtn->TabIndex = 12;
            this->proveBtn->Text = L"Prove";
            this->proveBtn->UseVisualStyleBackColor = true;
            this->proveBtn->Click += gcnew System::EventHandler(this, &CompositionUI::proveBtn_Click);
            // 
            // newBtn
            // 
            this->newBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->newBtn->Location = System::Drawing::Point(388, 2);
            this->newBtn->Name = L"newBtn";
            this->newBtn->Size = System::Drawing::Size(75, 23);
            this->newBtn->TabIndex = 13;
            this->newBtn->Text = L"New";
            this->newBtn->UseVisualStyleBackColor = true;
            this->newBtn->Click += gcnew System::EventHandler(this, &CompositionUI::newBtn_Click);
            // 
            // saveBtn
            // 
            this->saveBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->saveBtn->Location = System::Drawing::Point(469, 2);
            this->saveBtn->Name = L"saveBtn";
            this->saveBtn->Size = System::Drawing::Size(75, 23);
            this->saveBtn->TabIndex = 14;
            this->saveBtn->Text = L"Save";
            this->saveBtn->UseVisualStyleBackColor = true;
            this->saveBtn->Visible = false;
            this->saveBtn->Click += gcnew System::EventHandler(this, &CompositionUI::saveBtn_Click);
            // 
            // startBtn
            // 
            this->startBtn->Location = System::Drawing::Point(1, 2);
            this->startBtn->Name = L"startBtn";
            this->startBtn->Size = System::Drawing::Size(31, 23);
            this->startBtn->TabIndex = 8;
            this->startBtn->Text = L"|<";
            this->startBtn->UseVisualStyleBackColor = true;
            this->startBtn->Click += gcnew System::EventHandler(this, &CompositionUI::startBtn_Click);
            // 
            // back10Btn
            // 
            this->back10Btn->Location = System::Drawing::Point(32, 2);
            this->back10Btn->Name = L"back10Btn";
            this->back10Btn->Size = System::Drawing::Size(31, 23);
            this->back10Btn->TabIndex = 9;
            this->back10Btn->Text = L"<<";
            this->back10Btn->UseVisualStyleBackColor = true;
            this->back10Btn->Click += gcnew System::EventHandler(this, &CompositionUI::back10Btn_Click);
            // 
            // previousBtn
            // 
            this->previousBtn->Location = System::Drawing::Point(63, 2);
            this->previousBtn->Name = L"previousBtn";
            this->previousBtn->Size = System::Drawing::Size(31, 23);
            this->previousBtn->TabIndex = 9;
            this->previousBtn->Text = L"<";
            this->previousBtn->UseVisualStyleBackColor = true;
            this->previousBtn->Click += gcnew System::EventHandler(this, &CompositionUI::previousBtn_Click);
            // 
            // nextBtn
            // 
            this->nextBtn->Location = System::Drawing::Point(106, 2);
            this->nextBtn->Name = L"nextBtn";
            this->nextBtn->Size = System::Drawing::Size(31, 23);
            this->nextBtn->TabIndex = 10;
            this->nextBtn->Text = L">";
            this->nextBtn->UseVisualStyleBackColor = true;
            this->nextBtn->Click += gcnew System::EventHandler(this, &CompositionUI::nextBtn_Click);
            // 
            // forward10Btn
            // 
            this->forward10Btn->Location = System::Drawing::Point(137, 2);
            this->forward10Btn->Name = L"forward10Btn";
            this->forward10Btn->Size = System::Drawing::Size(31, 23);
            this->forward10Btn->TabIndex = 10;
            this->forward10Btn->Text = L">>";
            this->forward10Btn->UseVisualStyleBackColor = true;
            this->forward10Btn->Click += gcnew System::EventHandler(this, &CompositionUI::forward10Btn_Click);
            // 
            // endBtn
            // 
            this->endBtn->Location = System::Drawing::Point(168, 2);
            this->endBtn->Name = L"endBtn";
            this->endBtn->Size = System::Drawing::Size(31, 23);
            this->endBtn->TabIndex = 11;
            this->endBtn->Text = L">|";
            this->endBtn->UseVisualStyleBackColor = true;
            this->endBtn->Click += gcnew System::EventHandler(this, &CompositionUI::endBtn_Click);
            // 
            // editBtn
            // 
            this->editBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->editBtn->Location = System::Drawing::Point(469, 2);
            this->editBtn->Name = L"editBtn";
            this->editBtn->Size = System::Drawing::Size(75, 23);
            this->editBtn->TabIndex = 15;
            this->editBtn->Text = L"Edit";
            this->editBtn->UseVisualStyleBackColor = true;
            this->editBtn->Click += gcnew System::EventHandler(this, &CompositionUI::editBtn_Click);
            // 
            // closeBtn
            // 
            this->closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->closeBtn->Location = System::Drawing::Point(544, 2);
            this->closeBtn->Name = L"closeBtn";
            this->closeBtn->Size = System::Drawing::Size(75, 23);
            this->closeBtn->TabIndex = 16;
            this->closeBtn->Text = L"Close";
            this->closeBtn->UseVisualStyleBackColor = true;
            this->closeBtn->Click += gcnew System::EventHandler(this, &CompositionUI::closeBtn_Click);
            // 
            // panel2
            // 
            panel2->Controls->Add(this->snapFinish);
            panel2->Controls->Add(this->changesCount);
            panel2->Controls->Add(changesLbl);
            panel2->Controls->Add(repeatsLbl);
            panel2->Controls->Add(this->repeatsEditor);
            panel2->Controls->Add(composerLbl);
            panel2->Controls->Add(nameLbl);
            panel2->Controls->Add(this->composerSelector);
            panel2->Controls->Add(this->nameEditor);
            panel2->Dock = System::Windows::Forms::DockStyle::Fill;
            panel2->Location = System::Drawing::Point(409, 75);
            panel2->Margin = System::Windows::Forms::Padding(0);
            panel2->Name = L"panel2";
            panel2->Size = System::Drawing::Size(213, 103);
            panel2->TabIndex = 6;
            // 
            // snapFinish
            // 
            this->snapFinish->AutoSize = true;
            this->snapFinish->Enabled = false;
            this->snapFinish->Location = System::Drawing::Point(6, 82);
            this->snapFinish->Name = L"snapFinish";
            this->snapFinish->Size = System::Drawing::Size(81, 17);
            this->snapFinish->TabIndex = 9;
            this->snapFinish->Text = L"Snap Finish";
            this->snapFinish->UseVisualStyleBackColor = true;
            // 
            // changesCount
            // 
            this->changesCount->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->changesCount->Location = System::Drawing::Point(161, 58);
            this->changesCount->Name = L"changesCount";
            this->changesCount->Size = System::Drawing::Size(48, 13);
            this->changesCount->TabIndex = 8;
            this->changesCount->Text = L"0";
            this->changesCount->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // changesLbl
            // 
            changesLbl->AutoSize = true;
            changesLbl->Location = System::Drawing::Point(103, 58);
            changesLbl->Name = L"changesLbl";
            changesLbl->Size = System::Drawing::Size(52, 13);
            changesLbl->TabIndex = 7;
            changesLbl->Text = L"Changes:";
            // 
            // repeatsLbl
            // 
            repeatsLbl->AutoSize = true;
            repeatsLbl->Location = System::Drawing::Point(10, 58);
            repeatsLbl->Name = L"repeatsLbl";
            repeatsLbl->Size = System::Drawing::Size(47, 13);
            repeatsLbl->TabIndex = 6;
            repeatsLbl->Text = L"Repeats";
            // 
            // repeatsEditor
            // 
            this->repeatsEditor->Location = System::Drawing::Point(63, 56);
            this->repeatsEditor->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10, 0, 0, 0 });
            this->repeatsEditor->Name = L"repeatsEditor";
            this->repeatsEditor->Size = System::Drawing::Size(34, 20);
            this->repeatsEditor->TabIndex = 6;
            this->repeatsEditor->ValueChanged += gcnew System::EventHandler(this, &CompositionUI::repeatsEditor_ValueChanged);
            // 
            // composerLbl
            // 
            composerLbl->AutoSize = true;
            composerLbl->Location = System::Drawing::Point(3, 6);
            composerLbl->Name = L"composerLbl";
            composerLbl->Size = System::Drawing::Size(54, 13);
            composerLbl->TabIndex = 4;
            composerLbl->Text = L"Composer";
            // 
            // nameLbl
            // 
            nameLbl->AutoSize = true;
            nameLbl->Location = System::Drawing::Point(22, 33);
            nameLbl->Name = L"nameLbl";
            nameLbl->Size = System::Drawing::Size(35, 13);
            nameLbl->TabIndex = 3;
            nameLbl->Text = L"Name";
            // 
            // composerSelector
            // 
            this->composerSelector->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
            this->composerSelector->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->composerSelector->FormattingEnabled = true;
            this->composerSelector->Location = System::Drawing::Point(63, 3);
            this->composerSelector->Name = L"composerSelector";
            this->composerSelector->Size = System::Drawing::Size(146, 21);
            this->composerSelector->TabIndex = 4;
            this->composerSelector->SelectedIndexChanged += gcnew System::EventHandler(this, &CompositionUI::composerSelector_SelectedIndexChanged);
            this->composerSelector->TextChanged += gcnew System::EventHandler(this, &CompositionUI::composerSelector_SelectedIndexChanged);
            // 
            // nameEditor
            // 
            this->nameEditor->Location = System::Drawing::Point(63, 30);
            this->nameEditor->Name = L"nameEditor";
            this->nameEditor->Size = System::Drawing::Size(146, 20);
            this->nameEditor->TabIndex = 5;
            this->nameEditor->TextChanged += gcnew System::EventHandler(this, &CompositionUI::nameEditor_TextChanged);
            // 
            // tabControl1
            // 
            tableLayoutPanel1->SetColumnSpan(this->tabControl1, 2);
            this->tabControl1->Controls->Add(compositionPage);
            this->tabControl1->Controls->Add(analysisPage);
            this->tabControl1->Controls->Add(this->advancedPage);
            this->tabControl1->Dock = System::Windows::Forms::DockStyle::Fill;
            this->tabControl1->Location = System::Drawing::Point(3, 29);
            this->tabControl1->Name = L"tabControl1";
            tableLayoutPanel1->SetRowSpan(this->tabControl1, 3);
            this->tabControl1->SelectedIndex = 0;
            this->tabControl1->Size = System::Drawing::Size(403, 270);
            this->tabControl1->TabIndex = 16;
            // 
            // compositionPage
            // 
            compositionPage->Controls->Add(this->dataGridView1);
            compositionPage->Location = System::Drawing::Point(4, 22);
            compositionPage->Name = L"compositionPage";
            compositionPage->Padding = System::Windows::Forms::Padding(3);
            compositionPage->Size = System::Drawing::Size(395, 244);
            compositionPage->TabIndex = 0;
            compositionPage->Text = L"Composition";
            compositionPage->UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->dataGridView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(1) { this->leadEndColumn });
            this->dataGridView1->Dock = System::Windows::Forms::DockStyle::Fill;
            this->dataGridView1->Location = System::Drawing::Point(3, 3);
            this->dataGridView1->Name = L"dataGridView1";
            this->dataGridView1->Size = System::Drawing::Size(389, 238);
            this->dataGridView1->TabIndex = 19;
            this->dataGridView1->CellEndEdit += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &CompositionUI::dataGridView1_CellEndEdit);
            // 
            // leadEndColumn
            // 
            this->leadEndColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->leadEndColumn->Frozen = true;
            this->leadEndColumn->HeaderText = L"";
            this->leadEndColumn->Name = L"leadEndColumn";
            this->leadEndColumn->ReadOnly = true;
            this->leadEndColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->leadEndColumn->Width = 21;
            // 
            // analysisPage
            // 
            analysisPage->Controls->Add(analysisLayout);
            analysisPage->Location = System::Drawing::Point(4, 22);
            analysisPage->Name = L"analysisPage";
            analysisPage->Padding = System::Windows::Forms::Padding(3);
            analysisPage->Size = System::Drawing::Size(395, 244);
            analysisPage->TabIndex = 1;
            analysisPage->Text = L"Analysis";
            analysisPage->UseVisualStyleBackColor = true;
            // 
            // analysisLayout
            // 
            analysisLayout->ColumnCount = 2;
            analysisLayout->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute, 105)));
            analysisLayout->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            analysisLayout->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute, 20)));
            analysisLayout->Controls->Add(label1, 0, 0);
            analysisLayout->Controls->Add(this->musicInfo, 0, 1);
            analysisLayout->Controls->Add(this->trueLbl, 1, 0);
            analysisLayout->Dock = System::Windows::Forms::DockStyle::Fill;
            analysisLayout->Location = System::Drawing::Point(3, 3);
            analysisLayout->Name = L"analysisLayout";
            analysisLayout->RowCount = 2;
            analysisLayout->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 19)));
            analysisLayout->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            analysisLayout->Size = System::Drawing::Size(389, 238);
            analysisLayout->TabIndex = 6;
            // 
            // label1
            // 
            label1->AutoSize = true;
            label1->Location = System::Drawing::Point(3, 3);
            label1->Margin = System::Windows::Forms::Padding(3);
            label1->Name = L"label1";
            label1->Size = System::Drawing::Size(99, 13);
            label1->TabIndex = 0;
            label1->Text = L"This composition is: ";
            label1->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // musicInfo
            // 
            analysisLayout->SetColumnSpan(this->musicInfo, 2);
            this->musicInfo->Dock = System::Windows::Forms::DockStyle::Fill;
            this->musicInfo->Location = System::Drawing::Point(3, 22);
            this->musicInfo->Multiline = true;
            this->musicInfo->Name = L"musicInfo";
            this->musicInfo->ReadOnly = true;
            this->musicInfo->ScrollBars = System::Windows::Forms::ScrollBars::Both;
            this->musicInfo->Size = System::Drawing::Size(383, 213);
            this->musicInfo->TabIndex = 5;
            this->musicInfo->WordWrap = false;
            // 
            // trueLbl
            // 
            this->trueLbl->AutoSize = true;
            this->trueLbl->Location = System::Drawing::Point(108, 3);
            this->trueLbl->Margin = System::Windows::Forms::Padding(3);
            this->trueLbl->Name = L"trueLbl";
            this->trueLbl->Size = System::Drawing::Size(54, 13);
            this->trueLbl->TabIndex = 1;
            this->trueLbl->Text = L"Unproven";
            this->trueLbl->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
            // 
            // advancedPage
            // 
            this->advancedPage->Controls->Add(this->finishingRoundsLbl);
            this->advancedPage->Controls->Add(snapFinishRoundsLbl);
            this->advancedPage->Controls->Add(snapFinishLbl);
            this->advancedPage->Controls->Add(snapStartRoundsLbl);
            this->advancedPage->Controls->Add(snapStartChangesLbl);
            this->advancedPage->Controls->Add(this->snapStart);
            this->advancedPage->Controls->Add(snapStartLbl);
            this->advancedPage->Location = System::Drawing::Point(4, 22);
            this->advancedPage->Name = L"advancedPage";
            this->advancedPage->Padding = System::Windows::Forms::Padding(3);
            this->advancedPage->Size = System::Drawing::Size(395, 244);
            this->advancedPage->TabIndex = 2;
            this->advancedPage->Text = L"Advanced";
            this->advancedPage->UseVisualStyleBackColor = true;
            // 
            // finishingRoundsLbl
            // 
            this->finishingRoundsLbl->AutoSize = true;
            this->finishingRoundsLbl->Location = System::Drawing::Point(100, 80);
            this->finishingRoundsLbl->Name = L"finishingRoundsLbl";
            this->finishingRoundsLbl->Size = System::Drawing::Size(66, 13);
            this->finishingRoundsLbl->TabIndex = 6;
            this->finishingRoundsLbl->Text = L"the lead end";
            // 
            // snapFinishRoundsLbl
            // 
            snapFinishRoundsLbl->AutoSize = true;
            snapFinishRoundsLbl->Location = System::Drawing::Point(7, 80);
            snapFinishRoundsLbl->Name = L"snapFinishRoundsLbl";
            snapFinishRoundsLbl->Size = System::Drawing::Size(95, 13);
            snapFinishRoundsLbl->TabIndex = 5;
            snapFinishRoundsLbl->Text = L"Finishing rounds at";
            // 
            // snapFinishLbl
            // 
            snapFinishLbl->AutoSize = true;
            snapFinishLbl->Location = System::Drawing::Point(7, 61);
            snapFinishLbl->Name = L"snapFinishLbl";
            snapFinishLbl->Size = System::Drawing::Size(59, 13);
            snapFinishLbl->TabIndex = 4;
            snapFinishLbl->Text = L"Snap finish";
            // 
            // snapStartRoundsLbl
            // 
            snapStartRoundsLbl->AutoSize = true;
            snapStartRoundsLbl->Location = System::Drawing::Point(7, 26);
            snapStartRoundsLbl->Name = L"snapStartRoundsLbl";
            snapStartRoundsLbl->Size = System::Drawing::Size(90, 13);
            snapStartRoundsLbl->TabIndex = 3;
            snapStartRoundsLbl->Text = L"Starting rounds at";
            // 
            // snapStartChangesLbl
            // 
            snapStartChangesLbl->AutoSize = true;
            snapStartChangesLbl->Location = System::Drawing::Point(148, 26);
            snapStartChangesLbl->Name = L"snapStartChangesLbl";
            snapStartChangesLbl->Size = System::Drawing::Size(115, 13);
            snapStartChangesLbl->TabIndex = 2;
            snapStartChangesLbl->Text = L"changes from lead end";
            // 
            // snapStart
            // 
            this->snapStart->Location = System::Drawing::Point(103, 24);
            this->snapStart->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 16, 0, 0, 0 });
            this->snapStart->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 16, 0, 0, System::Int32::MinValue });
            this->snapStart->Name = L"snapStart";
            this->snapStart->ReadOnly = true;
            this->snapStart->Size = System::Drawing::Size(39, 20);
            this->snapStart->TabIndex = 1;
            this->snapStart->Tag = L"Rounds at x changes before or after lead end";
            this->snapStart->ValueChanged += gcnew System::EventHandler(this, &CompositionUI::snapStart_ValueChanged);
            // 
            // snapStartLbl
            // 
            snapStartLbl->AutoSize = true;
            snapStartLbl->Location = System::Drawing::Point(7, 7);
            snapStartLbl->Name = L"snapStartLbl";
            snapStartLbl->Size = System::Drawing::Size(55, 13);
            snapStartLbl->TabIndex = 0;
            snapStartLbl->Text = L"Snap start";
            // 
            // statusStrip1
            // 
            statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) { this->progressBar, this->compositionIdLbl });
            statusStrip1->Location = System::Drawing::Point(0, 328);
            statusStrip1->Name = L"statusStrip1";
            statusStrip1->Size = System::Drawing::Size(622, 22);
            statusStrip1->TabIndex = 2;
            statusStrip1->Text = L"statusStrip1";
            // 
            // progressBar
            // 
            this->progressBar->Name = L"progressBar";
            this->progressBar->Size = System::Drawing::Size(100, 16);
            // 
            // compositionIdLbl
            // 
            this->compositionIdLbl->Name = L"compositionIdLbl";
            this->compositionIdLbl->Size = System::Drawing::Size(505, 17);
            this->compositionIdLbl->Spring = true;
            this->compositionIdLbl->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            this->compositionIdLbl->Click += gcnew System::EventHandler(this, &CompositionUI::compositionIdLbl_Click);
            // 
            // generator
            // 
            this->generator->WorkerReportsProgress = true;
            this->generator->WorkerSupportsCancellation = true;
            this->generator->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &CompositionUI::backgroundWorker1_DoWork);
            this->generator->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &CompositionUI::backgroundWorker1_ProgressChanged);
            this->generator->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &CompositionUI::backgroundWorker1_RunWorkerCompleted);
            // 
            // CompositionUI
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(622, 350);
            this->Controls->Add(statusStrip1);
            this->Controls->Add(tableLayoutPanel1);
            this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
            this->MinimumSize = System::Drawing::Size(630, 200);
            this->Name = L"CompositionUI";
            this->Text = L"Composition";
            this->Closing += gcnew System::ComponentModel::CancelEventHandler(this, &CompositionUI::ClosingEvent);
            this->Load += gcnew System::EventHandler(this, &CompositionUI::FormLoad);
            groupBox1->ResumeLayout(false);
            groupBox1->PerformLayout();
            tableLayoutPanel1->ResumeLayout(false);
            tableLayoutPanel1->PerformLayout();
            panel1->ResumeLayout(false);
            panel2->ResumeLayout(false);
            panel2->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->repeatsEditor))->EndInit();
            this->tabControl1->ResumeLayout(false);
            compositionPage->ResumeLayout(false);
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
            analysisPage->ResumeLayout(false);
            analysisLayout->ResumeLayout(false);
            analysisLayout->PerformLayout();
            this->advancedPage->ResumeLayout(false);
            this->advancedPage->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->snapStart))->EndInit();
            statusStrip1->ResumeLayout(false);
            statusStrip1->PerformLayout();
            this->ResumeLayout(false);
            this->PerformLayout();
        }
#pragma endregion
    };
}
