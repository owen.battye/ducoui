#pragma once
namespace DucoUI
{

    public ref class PealSpeedUI : public System::Windows::Forms::Form, public DucoUI::RingingDatabaseObserver
    {
    public:
        PealSpeedUI(DucoUI::DatabaseManager^ theDatabase);

        // from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

    protected:
        ~PealSpeedUI();
        !PealSpeedUI();
        System::Void PealSpeedUI_Load(System::Object^  sender, System::EventArgs^  e);

        System::Void GenerateData();
        System::Void PopulateResults(const std::multimap<float, Duco::ObjectId>& pealsIds, bool fastest);
        System::Void PopulateResult(const Duco::ObjectId& pealId, unsigned int position, bool draw);

        System::Void fastestPeals_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);
        System::Void dataGridView_CellDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);
        System::Void fastest_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void printBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void filtersBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void printData(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args);


    private:
        DucoUI::DatabaseManager^                database;
        Duco::StatisticFilters*                 filters;
        System::Boolean                         loading;
        System::ComponentModel::Container^      components;
    private: System::Windows::Forms::DataGridView^  pealsDisplay;









    private: System::Windows::Forms::ToolStripMenuItem^ realBellNumberToolStripMenuItem;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ PealId;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ FastestCpm;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ FastestDateColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ FastestChanges;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ FastestMethod;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ FastestTower;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ FastestTime;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ FastestBellRung;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ FastestTenor;
    private: System::Windows::Forms::ToolStripMenuItem^ fastestBtn;

#pragma region Windows Form Designer generated code
void InitializeComponent(void)
{
    System::Windows::Forms::ToolStrip^ toolStrip1;
    System::Windows::Forms::ToolStripDropDownButton^ toolStripDropDownButton1;
    System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(PealSpeedUI::typeid));
    System::Windows::Forms::ToolStripMenuItem^ filtersBtn;
    System::Windows::Forms::ToolStripButton^ printBtn;
    this->fastestBtn = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->realBellNumberToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->pealsDisplay = (gcnew System::Windows::Forms::DataGridView());
    this->PealId = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->FastestCpm = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->FastestDateColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->FastestChanges = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->FastestMethod = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->FastestTower = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->FastestTime = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->FastestBellRung = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->FastestTenor = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
    toolStripDropDownButton1 = (gcnew System::Windows::Forms::ToolStripDropDownButton());
    filtersBtn = (gcnew System::Windows::Forms::ToolStripMenuItem());
    printBtn = (gcnew System::Windows::Forms::ToolStripButton());
    toolStrip1->SuspendLayout();
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pealsDisplay))->BeginInit();
    this->SuspendLayout();
    // 
    // toolStrip1
    // 
    toolStrip1->GripStyle = System::Windows::Forms::ToolStripGripStyle::Hidden;
    toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) { toolStripDropDownButton1, printBtn });
    toolStrip1->Location = System::Drawing::Point(0, 0);
    toolStrip1->Name = L"toolStrip1";
    toolStrip1->Size = System::Drawing::Size(1145, 25);
    toolStrip1->TabIndex = 2;
    toolStrip1->Text = L"toolStrip1";
    // 
    // toolStripDropDownButton1
    // 
    toolStripDropDownButton1->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
    toolStripDropDownButton1->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
        filtersBtn,
            this->fastestBtn, this->realBellNumberToolStripMenuItem
    });
    toolStripDropDownButton1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripDropDownButton1.Image")));
    toolStripDropDownButton1->ImageTransparentColor = System::Drawing::Color::Magenta;
    toolStripDropDownButton1->Name = L"toolStripDropDownButton1";
    toolStripDropDownButton1->Size = System::Drawing::Size(62, 22);
    toolStripDropDownButton1->Text = L"&Settings";
    // 
    // filtersBtn
    // 
    filtersBtn->Name = L"filtersBtn";
    filtersBtn->Size = System::Drawing::Size(163, 22);
    filtersBtn->Text = L"F&ilters";
    filtersBtn->Click += gcnew System::EventHandler(this, &PealSpeedUI::filtersBtn_Click);
    // 
    // fastestBtn
    // 
    this->fastestBtn->CheckOnClick = true;
    this->fastestBtn->Name = L"fastestBtn";
    this->fastestBtn->Size = System::Drawing::Size(163, 22);
    this->fastestBtn->Text = L"&Fastest";
    this->fastestBtn->Click += gcnew System::EventHandler(this, &PealSpeedUI::fastest_CheckedChanged);
    // 
    // realBellNumberToolStripMenuItem
    // 
    this->realBellNumberToolStripMenuItem->CheckOnClick = true;
    this->realBellNumberToolStripMenuItem->Name = L"realBellNumberToolStripMenuItem";
    this->realBellNumberToolStripMenuItem->Size = System::Drawing::Size(163, 22);
    this->realBellNumberToolStripMenuItem->Text = L"&Real bell number";
    this->realBellNumberToolStripMenuItem->ToolTipText = L"Shows the bell number in the tower, rather than in the ring";
    this->realBellNumberToolStripMenuItem->Click += gcnew System::EventHandler(this, &PealSpeedUI::fastest_CheckedChanged);
    // 
    // printBtn
    // 
    printBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
    printBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"printBtn.Image")));
    printBtn->ImageTransparentColor = System::Drawing::Color::Magenta;
    printBtn->Name = L"printBtn";
    printBtn->Size = System::Drawing::Size(36, 22);
    printBtn->Text = L"&Print";
    printBtn->Click += gcnew System::EventHandler(this, &PealSpeedUI::printBtn_Click);
    // 
    // pealsDisplay
    // 
    this->pealsDisplay->AllowUserToAddRows = false;
    this->pealsDisplay->AllowUserToDeleteRows = false;
    this->pealsDisplay->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
    this->pealsDisplay->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(9) {
        this->PealId,
            this->FastestCpm, this->FastestDateColumn, this->FastestChanges, this->FastestMethod, this->FastestTower, this->FastestTime,
            this->FastestBellRung, this->FastestTenor
    });
    this->pealsDisplay->Dock = System::Windows::Forms::DockStyle::Fill;
    this->pealsDisplay->Location = System::Drawing::Point(0, 25);
    this->pealsDisplay->Name = L"pealsDisplay";
    this->pealsDisplay->ReadOnly = true;
    this->pealsDisplay->Size = System::Drawing::Size(1145, 366);
    this->pealsDisplay->TabIndex = 3;
    this->pealsDisplay->CellContentDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &PealSpeedUI::dataGridView_CellDoubleClick);
    this->pealsDisplay->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &PealSpeedUI::fastestPeals_ColumnHeaderMouseClick);
    // 
    // PealId
    // 
    this->PealId->HeaderText = L"Id";
    this->PealId->Name = L"PealId";
    this->PealId->ReadOnly = true;
    this->PealId->Visible = false;
    // 
    // FastestCpm
    // 
    this->FastestCpm->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
    this->FastestCpm->HeaderText = L"Changes Per Minute";
    this->FastestCpm->Name = L"FastestCpm";
    this->FastestCpm->ReadOnly = true;
    this->FastestCpm->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
    this->FastestCpm->Width = 88;
    // 
    // FastestDateColumn
    // 
    this->FastestDateColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
    this->FastestDateColumn->HeaderText = L"Date";
    this->FastestDateColumn->Name = L"FastestDateColumn";
    this->FastestDateColumn->ReadOnly = true;
    this->FastestDateColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
    this->FastestDateColumn->Width = 55;
    // 
    // FastestChanges
    // 
    this->FastestChanges->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
    this->FastestChanges->HeaderText = L"Changes";
    this->FastestChanges->Name = L"FastestChanges";
    this->FastestChanges->ReadOnly = true;
    this->FastestChanges->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
    this->FastestChanges->Width = 74;
    // 
    // FastestMethod
    // 
    this->FastestMethod->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
    this->FastestMethod->HeaderText = L"Method";
    this->FastestMethod->Name = L"FastestMethod";
    this->FastestMethod->ReadOnly = true;
    this->FastestMethod->Width = 68;
    // 
    // FastestTower
    // 
    this->FastestTower->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
    this->FastestTower->HeaderText = L"Tower";
    this->FastestTower->Name = L"FastestTower";
    this->FastestTower->ReadOnly = true;
    // 
    // FastestTime
    // 
    this->FastestTime->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
    this->FastestTime->HeaderText = L"Time";
    this->FastestTime->Name = L"FastestTime";
    this->FastestTime->ReadOnly = true;
    this->FastestTime->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
    this->FastestTime->Width = 55;
    // 
    // FastestBellRung
    // 
    this->FastestBellRung->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
    this->FastestBellRung->HeaderText = L"Bell rung";
    this->FastestBellRung->Name = L"FastestBellRung";
    this->FastestBellRung->ReadOnly = true;
    this->FastestBellRung->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
    this->FastestBellRung->Visible = false;
    this->FastestBellRung->Width = 68;
    // 
    // FastestTenor
    // 
    this->FastestTenor->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
    this->FastestTenor->HeaderText = L"Tenor";
    this->FastestTenor->Name = L"FastestTenor";
    this->FastestTenor->ReadOnly = true;
    this->FastestTenor->Width = 60;
    // 
    // PealSpeedUI
    // 
    this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
    this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
    this->ClientSize = System::Drawing::Size(1145, 391);
    this->Controls->Add(this->pealsDisplay);
    this->Controls->Add(toolStrip1);
    this->Name = L"PealSpeedUI";
    this->Text = L"Fastest and Slowest performances";
    this->Load += gcnew System::EventHandler(this, &PealSpeedUI::PealSpeedUI_Load);
    toolStrip1->ResumeLayout(false);
    toolStrip1->PerformLayout();
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pealsDisplay))->EndInit();
    this->ResumeLayout(false);
    this->PerformLayout();

}
#pragma endregion
};
}
