#pragma once
#include "PrintUtilBase.h"

namespace Duco
{
    class ObjectId;
    class PealLengthInfo;
}

namespace DucoUI
{
public ref class PrintSummaryUtil : public PrintUtilBase
{
public:
    PrintSummaryUtil(DucoUI::DatabaseManager^ theDatabase, System::Drawing::Printing::PrintPageEventArgs^ printArgs);
    virtual System::Void printObject(System::Drawing::Printing::PrintPageEventArgs^ args, System::Drawing::Printing::PrinterSettings^ printSettings) override;

protected:
    System::Void PrintPealTitle(System::String^ title, const Duco::ObjectId& pealId, bool includeSpeed, System::Drawing::Printing::PrintPageEventArgs^ printArgs);
    System::Void PrintRinger(const Duco::ObjectId& ringerId, const Duco::PealLengthInfo& noOfPeals, System::Drawing::Printing::PrintPageEventArgs^ printArgs);
    System::Void PrintTower(const Duco::ObjectId& towerId, const Duco::PealLengthInfo& noOfPeals, System::Drawing::Printing::PrintPageEventArgs^ printArgs);
    System::Void PrintMethod(const Duco::ObjectId& methodId, const Duco::PealLengthInfo& noOfPeals, System::Drawing::Printing::PrintPageEventArgs^ printArgs);
    System::Void PrintPeal(const Duco::ObjectId& pealId, System::String^ title, System::Drawing::Printing::PrintPageEventArgs^ printArgs);
    Duco::ObjectId GetTowerId(const Duco::ObjectId& pealId);
    bool CheckForDifferentTowerIds(const Duco::ObjectId& mostNortherly, const Duco::ObjectId& mostSoutherly, const Duco::ObjectId& mostEasterly, const Duco::ObjectId& mostWesterly);

    System::Drawing::Font^      smallBoldFont;
};

}