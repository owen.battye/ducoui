#include "LongMethodLineDrawer.h"

#include <Lead.h>
#include <Method.h>
#include <DucoEngineUtils.h>
#include "DucoUtils.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::Drawing;
using namespace System::Drawing::Drawing2D;

LongMethodLineDrawer::LongMethodLineDrawer(System::Drawing::Graphics^ lineDrwr, Duco::Method& theMethod, DucoUI::DatabaseManager^ theDatabase, System::ComponentModel::BackgroundWorker^ bgWorker)
: MethodLineDrawer(lineDrwr, theMethod, theDatabase, bgWorker), secondHalfOnSecondLine(true)
{
}

LongMethodLineDrawer::~LongMethodLineDrawer()
{
}

void
LongMethodLineDrawer::SetSecondHalfOnSecondLine(bool wrapHalfWay)
{
    secondHalfOnSecondLine = wrapHalfWay;
}

void 
LongMethodLineDrawer::DrawGridLines()
{
    float lineX (KPositionOffset + KPositionGap);
    float finalYPosition = float(singleLead->BlowsInLead()) * KBlowGap * float(currentMethod.HalfWay());
    for (unsigned int j(1); j < currentMethod.Order(); j += 2)
    {
        lineDrawer->FillRectangle(linesDrawingBrush, lineX, KBlowOffset, KPositionGap, finalYPosition);
        lineX += 2*KPositionGap;
    }
    if (!DucoEngineUtils::IsEven(currentMethod.Order()))
    {
        lineX += KPositionGap;
    }
    finalYPosition = (singleLead->BlowsInLead() * KBlowGap) * (currentMethod.NoOfLeads() - currentMethod.HalfWay());
    lineX += KPositionOffset;
    for (unsigned int j(1); j < currentMethod.Order(); j += 2)
    {
        lineDrawer->FillRectangle(linesDrawingBrush, lineX, KBlowOffset, KPositionGap, finalYPosition);
        lineX += 2*KPositionGap;
    }
}

bool 
LongMethodLineDrawer::DrawLine()
{
    PointF originatingPosition(KPositionOffset + (drawingBell*KPositionGap) + xPosWrapOffset, KBlowOffset);
    leadLine->Add(originatingPosition);
    longLineResetPosition = 0;

    bool endedWithRounds (false);
    while (!endedWithRounds && (worker == nullptr || !worker->CancellationPending))
    {
        DrawLead(lineDrawingPen);

        if (secondHalfOnSecondLine && leadNumber == currentMethod.HalfWay())
        {
            longLineResetPosition = float(leadNumber * singleLead->BlowsInLead()) * KBlowGap;

            xPosWrapOffset += perLeadXOffsetValue;
            if (leadLine->Count > 1)
            {
                PointF lastPos = (*leadLine)[leadLine->Count-1];
                lineDrawer->DrawLines(lineDrawingPen, leadLine->ToArray());
                leadLine->Clear();
            }

            // Move the treble line.
            Matrix^ translateMatrix = gcnew Matrix;
            translateMatrix->Translate( perLeadXOffsetValue, -((float(singleLead->BlowsInLead())*KBlowGap)*float(currentMethod.HalfWay())) );
            treblePath->Transform(translateMatrix);
        }
        endedWithRounds = singleLead->EndsWithRounds();
        CreateNextLead();
    }
    if (leadLine->Count > 1)
        lineDrawer->DrawLines(lineDrawingPen, leadLine->ToArray());
    xPosWrapOffset += perLeadXOffsetValue;
    return true;
}

System::Void 
LongMethodLineDrawer::DrawLead(System::Drawing::Pen^% penToUse)
{
    bool generateTreble (treblePath->PointCount <= 0);
    System::Collections::Generic::List<System::Drawing::PointF>^ trebleLine = nullptr;
    if (generateTreble && ShowTreble())
    {
        trebleLine = gcnew System::Collections::Generic::List<System::Drawing::PointF>(singleLead->BlowsInLead()+1);
        PointF originatingTreblePosition((KPositionOffset + KPositionGap + xPosWrapOffset), KBlowOffset);
        trebleLine->Add(originatingTreblePosition);
    }
    size_t changeNumber(0);
    unsigned int currentPosition(0);

    while (changeNumber <= singleLead->BlowsInLead() && (worker == nullptr || !worker->CancellationPending))
    {
        if (singleLead->Position(changeNumber, drawingBell, currentPosition))
        {
            float nextXPos (KPositionOffset + (currentPosition*KPositionGap) + xPosWrapOffset);
            size_t changeNumberInCourse (changeNumber);
            changeNumberInCourse += (leadNumber * singleLead->BlowsInLead());
            float nextYPos (KBlowOffset + (changeNumberInCourse*KBlowGap) - longLineResetPosition);
            PointF nextPosition(nextXPos, nextYPos);
            leadLine->Add(nextPosition);

            if (changeNumber == 0)
            {
                // Draw Lead start number and point.
                PointF lastPos = (*leadLine)[leadLine->Count-1];
                float leadStartYPos (lastPos.Y - (float(leadStartFont->Height)/2));
                String^ leadStartText = DucoUtils::ConvertString(DucoEngineUtils::ToChar(currentPosition));
                SizeF leadStartStrSize = lineDrawer->MeasureString(leadStartText, leadStartFont);
                lineDrawer->DrawString(leadStartText, leadStartFont, leadStartBrush, xPosWrapOffset + KPositionOffset + KPositionGap - leadStartStrSize.Width, leadStartYPos);
                lineDrawer->FillEllipse(leadStartBrush, lastPos.X-KLeadStartSize, lastPos.Y-KLeadStartSize, KLeadStartSize*2, KLeadStartSize*2);
            }

            if (generateTreble && ShowTreble() && singleLead->TreblePosition(changeNumber, currentPosition))
            {
                float nextTrebleXPos (KPositionOffset + (currentPosition*KPositionGap) + int(xPosWrapOffset));
                PointF nextTreblePosition(nextTrebleXPos, nextYPos);
                trebleLine->Add(nextTreblePosition);
                CheckHighestPosition(nextTreblePosition);
            }
        }
        ++changeNumber;
    }
    if (generateTreble && ShowTreble())
    {
        treblePath->AddLines(trebleLine->ToArray());
        delete trebleLine;
    }
    MoveTrebleLineAndDraw(trebleDrawingPen);
}


System::Void
LongMethodLineDrawer::MoveTrebleLineAndDraw(System::Drawing::Pen^% penToUse)
{
    if (ShowTreble())
    {
        if (leadNumber > 0)
        {
            Matrix^ translateMatrix = gcnew Matrix;
            translateMatrix->Translate( 0, float(singleLead->BlowsInLead())*KBlowGap );
            treblePath->Transform(translateMatrix);
        }
        lineDrawer->DrawPath(penToUse, treblePath);
    }
    ++leadNumber;
}
