#pragma once

#include <string>

namespace DucoUI
{
    public ref class TableSorterByHeaderCell :  public System::Collections::IComparer
    {
    public:
        TableSorterByHeaderCell(bool newAscending);
        virtual int Compare(System::Object^, System::Object^);

    protected:
        ~TableSorterByHeaderCell();

    private:
        bool    ascending;
    };
}
