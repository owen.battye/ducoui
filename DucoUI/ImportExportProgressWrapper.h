#pragma once

#include <ObjectId.h>
#include <ObjectType.h>
#include <ImportExportProgressCallback.h>
#include <vcclr.h>

namespace DucoUI
{
    interface class ImportExportProgressCallbackUI
    {
    public:
        virtual System::Void InitialisingImportExport(System::Boolean internalising, unsigned int versionNumber, size_t totalNumberOfObjects) = 0;
        virtual System::Void ObjectProcessed(System::Boolean internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase) = 0;
        virtual System::Void ImportExportComplete(System::Boolean internalising) = 0;
        virtual System::Void ImportExportFailed(System::Boolean internalising, int errorCode) = 0;
    };

    class ImportExportProgressWrapper : public Duco::ImportExportProgressCallback
    {
    public:
        ImportExportProgressWrapper(DucoUI::ImportExportProgressCallbackUI^ theDbManager);

        virtual System::Void InitialisingImportExport(System::Boolean internalising, unsigned int versionNumber, size_t totalNumberOfObjects);
        virtual System::Void ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase);
        virtual System::Void ImportExportComplete(System::Boolean internalising);
        virtual System::Void ImportExportFailed(System::Boolean internalising, int errorCode);

    protected:
        gcroot<ImportExportProgressCallbackUI^> uiCallback;
    };
}
