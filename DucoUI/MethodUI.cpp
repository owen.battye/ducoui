#include "RingingDatabaseObserver.h"
#include "DucoWindowState.h"
#include "DatabaseManager.h"
#include "RingerDisplayCache.h"
#include <Method.h>

#include "MethodUI.h"

#include <Method.h>
#include "SystemDefaultIcon.h"
#include "SoundUtils.h"
#include "DatabaseGenUtils.h"
#include "DucoUtils.h"
#include <DatabaseSettings.h>
#include <RingingDatabase.h>
#include <PlaceNotation.h>
#include <PealLengthInfo.h>
#include "SingleObjectStatsUI.h"
#include "PrintMethodUtil.h"
#include "MethodLineDrawer.h"
#include "LongMethodLineDrawer.h"
#include "AnnotatedMethodLineDrawer.h"
#include "InputIndexQueryUI.h"
#include "FiltersUI.h"
#include "DucoUIUtils.h"
#include "SoundUtils.h"
#include "DucoEngineUtils.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Drawing;
using namespace System::Drawing::Printing;
using namespace System::Windows::Forms;
const int KDefaultDialogHeight = 228;

MethodUI::MethodUI(DatabaseManager^ theDatabase, DucoWindowState newState)
: database(theDatabase), windowState(newState), populatingView(false), triedToStartDrawing(false)
{
    InitializeComponent();
}

MethodUI::MethodUI(DatabaseManager^ theDatabase, const Duco::ObjectId& methodId)
: database(theDatabase), windowState(DucoWindowState::EViewMode), populatingView(false)
{
    currentMethod = new Method();
    database->FindMethod(methodId, *currentMethod, false);
    InitializeComponent();
}

void
MethodUI::ShowMethod(DatabaseManager^ theDatabase, System::Windows::Forms::Form^ parent, const Duco::ObjectId& methodId)
{
    if (methodId.ValidId())
    {
        MethodUI^ methodUI = gcnew MethodUI(theDatabase, methodId);
        methodUI->MdiParent = parent;
        methodUI->Show();
    }
}

void
MethodUI::OnPaint(PaintEventArgs^ e)
{
   Form::OnPaint(e);
   if (methodDrawing == nullptr)
   {
       try
       {
            IntPtr hdc = e->Graphics->GetHdc();
            methodDrawing = (gcnew System::Drawing::Imaging::Metafile(hdc, System::Drawing::Imaging::EmfType::EmfOnly));
            e->Graphics->ReleaseHdc(hdc);
            if (triedToStartDrawing && currentMethod != NULL)
            {
                StartLineDrawing(currentMethod->PrintFromBell());
            }
       }
       catch (Exception^ ex)
       {
           MessageBox::Show(ex->ToString(), "Uncaught exception trying to start line drawing");
       }
   }
}

MethodUI::!MethodUI()
{
    delete currentMethod;
}

MethodUI::~MethodUI()
{
    this->!MethodUI();

    if (lineGenerator->IsBusy)
    {
        lineGenerator->CancelAsync();
    }

	if (components)
	{
		delete components;
	}
}

void 
MethodUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    if (type != TObjectType::EMethod && type != TObjectType::ESettings)
        return;

    switch (eventId)
    {
    case EDeleted:
    case EAdded:
        UpdateIdLabel();
        break;
    case EUpdated:
        if (windowState != DucoWindowState::EEditMode && ( (id == currentMethod->Id() && type == TObjectType::EMethod) || type == TObjectType::ESettings) )
        {
            PopulateView();
        }

    default:
        break;
    }
}

System::Void
MethodUI::MethodUI_Load(System::Object^ sender, System::EventArgs^ e)
{
    this->Icon = SystemDefaultIcon::DefaultSystemIcon();
    this->Closing += gcnew System::ComponentModel::CancelEventHandler(this, &MethodUI::ClosingEvent);

    DatabaseGenUtils::GenerateStageOptions(orderEditor, database, false);
    DatabaseGenUtils::GenerateMethodNameOptions(nameEditor, database);
    DatabaseGenUtils::GenerateMethodTypeOptions(typeEditor, database);

    lineGenerator->DoWork += gcnew DoWorkEventHandler( this, &MethodUI::DrawLine);
    lineGenerator->RunWorkerCompleted += gcnew RunWorkerCompletedEventHandler( this, &MethodUI::DrawLineCompleted);
    
    switch (windowState)
    {
    case DucoWindowState::EViewMode:
    default:
        {
            SetViewState();
            if (currentMethod == NULL)
            {
                currentMethod = new Method();
                Duco::ObjectId methodId;
                if (database->FirstMethod(methodId, false))
                    PopulateViewWithId(methodId);
                else
                    editBtn->Visible = false;
            }
            else
                PopulateView();
        }
        break;

    case DucoWindowState::ENewMode:
        SetNewState();
        windowState = DucoWindowState::ENewMode;
        ClearView();
        methodIdLabel->Text = "New method";
        RemoveMethodDrawing();
        break;
    }
    database->AddObserver(this);
}

System::Void
MethodUI::ClosingEvent(System::Object^ object, System::ComponentModel::CancelEventArgs^ e)
{
    if ( dataChanged )
    {
        if ( DucoUtils::ConfirmLooseChanges("method", this) )
        {
            e->Cancel = true;
        }
    }
    if (!e->Cancel)
    {
        database->CancelEdit(TObjectType::EMethod, currentMethod->Id());
        database->RemoveObserver(this);
    }
}

System::Void
MethodUI::EditBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (database->StartEdit(TObjectType::EMethod, currentMethod->Id()))
        SetEditState();
    else
        SoundUtils::PlayErrorSound();
}

System::Void 
MethodUI::CloseBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    switch (windowState)
    {
    case DucoWindowState::EEditMode:
        database->CancelEdit(TObjectType::EMethod, currentMethod->Id());
        // no break on purpose
    default:
        SetViewState();
        PopulateView();
        break;

    case DucoWindowState::EViewMode:
        Close();
        break;
    }
}

System::Void 
MethodUI::SaveBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (!currentMethod->Valid(database->Database(), false, true))
    {
        if (!currentMethod->ValidNotation(false, false, database->Database().Settings(), false))
        {
            if (MessageBox::Show(this, DucoUtils::ConvertString(currentMethod->ErrorString(database->Database().Settings(), false)) + ". Do you want to continue?", "Method notation invalid", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == ::DialogResult::No)
            {
                return;
            }
        }
        else
        {
            if (MessageBox::Show(this, DucoUtils::ConvertString(currentMethod->ErrorString(database->Database().Settings(), false)) + ". Do you want to continue?", "Invalid method", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == ::DialogResult::No)
            {
                return;
            }
        }
    }
    if (windowState == DucoWindowState::ENewMode)
    {
        // Check peal is unique;
        if (!database->UniqueObject(*currentMethod, TObjectType::EMethod))
        {
            if (MessageBox::Show(this, "This method is very similar to another one with the same name or notation. Do you want to continue?", "Duplicate method", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == ::DialogResult::No)
                return;
        }
    }
    if (dataChanged)
    {
        if (windowState == DucoWindowState::EEditMode)
            database->UpdateMethod(*currentMethod, true);
        else
        {
            Duco::ObjectId newMethodId = database->AddMethod(*currentMethod);
            if (newMethodId.ValidId())
            {
                SetViewState();
                PopulateViewWithId(newMethodId);
            }
            return;
        }
        dataChanged = false;
    }
    else
    {
        database->CancelEdit(TObjectType::EMethod, currentMethod->Id());
    }

    switch (windowState)
    {
    default:
        if (dataChanged)
        {
			DatabaseGenUtils::GenerateStageOptions(orderEditor, database, false);
			DatabaseGenUtils::GenerateMethodNameOptions(nameEditor, database);
			DatabaseGenUtils::GenerateMethodTypeOptions(typeEditor, database);
        }
        SetViewState();
        PopulateView();
        break;
    case DucoWindowState::ENewMode:
        break;
    }
}

System::Void 
MethodUI::CancelBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    switch (windowState)
    {
    default:
        if (windowState == DucoWindowState::EEditMode)
            database->CancelEdit(TObjectType::EMethod, currentMethod->Id());

        if (dataChanged)
        {
            if (DucoUtils::ConfirmLooseChanges("method", this))
                return;
            dataChanged = false;
            // set data changed to false, so the closing event will not prompt the user again
        }
        SetViewState();
        PopulateViewWithId(currentMethod->Id());
        break;

    case DucoWindowState::ENewMode:
        Close();
        break;
    }
}

System::Void 
MethodUI::NextBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    Duco::ObjectId nextMethodId = currentMethod->Id();
    if (database->NextMethod(nextMethodId, true, false))
    {
        PopulateViewWithId(nextMethodId);
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void 
MethodUI::PreviousBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    Duco::ObjectId nextMethodId = currentMethod->Id();
    if (database->NextMethod(nextMethodId, false, false))
    {
        PopulateViewWithId(nextMethodId);
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void 
MethodUI::Forward10Btn_Click(System::Object^ sender, System::EventArgs^ e)
{
    Duco::ObjectId nextMethodId = currentMethod->Id();
    if (database->ForwardMultipleMethods(nextMethodId, true))
    {
        PopulateViewWithId(nextMethodId);
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void 
MethodUI::Back10Btn_Click(System::Object^ sender, System::EventArgs^ e)
{
    Duco::ObjectId nextMethodId = currentMethod->Id();
    if (database->ForwardMultipleMethods(nextMethodId, false))
    {
        PopulateViewWithId(nextMethodId);
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void 
MethodUI::StartBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId methodId;
    if (database->FirstMethod(methodId, false))
    {
        PopulateViewWithId(methodId);
    }
}

System::Void 
MethodUI::EndBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId methodId;
    if (database->LastMethod(methodId))
    {
        PopulateViewWithId(methodId);
    }
}

System::Void 
MethodUI::OnlineBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    PlaceNotation notation (*currentMethod, 0);

    String^ fullUrl;
    if (database->Settings().BlueLineWebsite() != DatabaseSettings::EVisualMethodArchive)
    {
        String^ url = "http://ringing.org/main/pages/method?notation=";
        url += DucoUtils::ConvertString(notation.FullNotation(false, false));
        url += "&pn-query=Display&stage=";
        url += Convert::ToString(notation.Order());
        url += "&le=";
        url += DucoUtils::ConvertString(notation.PlainLeadEndNotation());
        fullUrl = url;
    }
    else
    {
        String^ url = "http://www.vismeth.co.uk/methview2.php?P=";
        String^ urlSuffix = "&T=";

        fullUrl = url + DucoUtils::ConvertString(notation.FullNotation()) + urlSuffix + DucoUtils::ConvertString(currentMethod->NameForWeb(database->Database()));

        fullUrl += "&S=";
        std::wstring numberOfBellsStr = currentMethod->OrderName(database->Database());
        fullUrl += DucoUtils::ConvertString(numberOfBellsStr);
        fullUrl += "&R=Rung";
    }

    try
    {
        System::Diagnostics::Process::Start(fullUrl);
    }
    catch (System::ComponentModel::Win32Exception^ noBrowserEx) 
    {
        SoundUtils::PlayErrorSound();
        System::Windows::Forms::MessageBox::Show(this, noBrowserEx->Message);
    }
    catch (System::Exception^ otherEx)
    {
        SoundUtils::PlayErrorSound();
        System::Windows::Forms::MessageBox::Show(this, otherEx->Message);
    } 
}

System::Void 
MethodUI::StatsCmd_Click(System::Object^  sender, System::EventArgs^  e)
{
    SingleObjectStatsUI^ statsUI = gcnew SingleObjectStatsUI(database, currentMethod->Id(), TObjectType::EMethod);
    statsUI->MdiParent = this->MdiParent;
    statsUI->Show();
}

System::Void 
MethodUI::PrintCmd_Click(System::Object^  sender, System::EventArgs^  e)
{
    try
    {
        if (database->Settings().UsePrintPreview())
        {
            PrintPreviewDialog^ printPreviewDialog = gcnew PrintPreviewDialog();
            printPreviewDialog->Icon = this->Icon;
            printPreviewDialog->Document = gcnew PrintDocument;
            switch (database->Settings().LineType())
            {
            case Duco::DatabaseSettings::ELongLine:
                break;

            default:
                printPreviewDialog->Document->DefaultPageSettings->Landscape = true;
                break;
            }
            printPreviewDialog->Document->PrintPage += gcnew System::Drawing::Printing::PrintPageEventHandler( this, &MethodUI::PrintMethod );
            printPreviewDialog->ShowDialog();
        }
        else
        {
            PrintDialog^ printDialog = gcnew PrintDialog();
            printDialog->Document = gcnew PrintDocument;
            printDialog->UseEXDialog = true;
            switch (database->Settings().LineType())
            {
            case Duco::DatabaseSettings::ELongLine:
                break;

            default:
                printDialog->Document->DefaultPageSettings->Landscape = true;
                break;
            }
            printDialog->Document->PrintPage += gcnew System::Drawing::Printing::PrintPageEventHandler( this, &MethodUI::PrintMethod );
            if (printDialog->ShowDialog() == ::DialogResult::OK )
            {
                printDialog->Document->Print();
            }
        }
    }
    catch (Exception^ ex)
    {
        MessageBox::Show(this, ex->Message, "Error printing method", MessageBoxButtons::OK, MessageBoxIcon::Error);
    }
}

System::Void
MethodUI::CopyToClipboardCmd_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (methodPicture->Image != nullptr)
    {
        Clipboard::SetImage(methodPicture->Image);
    }
}

System::Void 
MethodUI::PrintMethod(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args)
{
    if (methodPicture->Image != nullptr)
    {
        PrintDocument^ printDoc = static_cast<PrintDocument^>(sender);
        PrintMethodUtil printUtil(database, args);
        printUtil.SetObjects(currentMethod, methodPicture->Image);
        printUtil.printObject(args, printDoc->PrinterSettings);
    }
}

void 
MethodUI::SetViewState()
{
    lineGenerator->CancelAsync();
    dataChanged = false;
    windowState = DucoWindowState::EViewMode;
    closeBtn->Text = "Close";

    nameEditor->Enabled = false;
    typeEditor->Enabled = false;
    orderEditor->Enabled = false;
    placeNotationEditor->ReadOnly = true;
    bobNotationEditor->ReadOnly = true;
    singleNotationEditor->ReadOnly = true;
    altBobNotationEditor->ReadOnly = true;
    altSingleNotationEditor->ReadOnly = true;
    dualOrder->Enabled = false;

    startBtn->Visible = true;
    back10Btn->Visible = true;
    nextBtn->Visible = true;
    previousBtn->Visible = true;
    forward10Btn->Visible = true;
    endBtn->Visible = true;
    editBtn->Visible = true;
    cancelBtn->Visible = false;
    closeBtn->Visible = true;
    saveBtn->Visible = false;
    printCmd->Visible = true;
    statsCmd->Visible = true;
}

void 
MethodUI::SetEditState()
{
    lineGenerator->CancelAsync();
    windowState = DucoWindowState::EEditMode;

    nameEditor->Enabled = true;
    typeEditor->Enabled = true;
    orderEditor->Enabled = true;
    placeNotationEditor->ReadOnly = false;
    bobNotationEditor->ReadOnly = false;
    singleNotationEditor->ReadOnly = false;
    altBobNotationEditor->ReadOnly = false;
    altSingleNotationEditor->ReadOnly = false;
    dualOrder->Enabled = true;

    startBtn->Visible = false;
    back10Btn->Visible = false;
    previousBtn->Visible = false;
    nextBtn->Visible = false;
    forward10Btn->Visible = false;
    endBtn->Visible = false;
    editBtn->Visible = false;
    cancelBtn->Visible = true;
    closeBtn->Visible = false;
    saveBtn->Visible = true; 
    onlineBtn->Visible = false;
    printCmd->Visible = false;
    statsCmd->Visible = false;
    if (currentMethod->PrintFromBell() > 0 && currentMethod->PrintFromBell() <= currentMethod->Order())
        drawFrom->Value = currentMethod->PrintFromBell();
    else
        RemoveMethodDrawing();
    dataChanged = false;
}

void 
MethodUI::SetNewState()
{
    lineGenerator->CancelAsync();
    windowState = DucoWindowState::ENewMode;

    nameEditor->Enabled = true;
    typeEditor->Enabled = true;
    orderEditor->Enabled = true;
    placeNotationEditor->ReadOnly = false;
    bobNotationEditor->ReadOnly = false;
    singleNotationEditor->ReadOnly = false;
    altBobNotationEditor->ReadOnly = false;
    altSingleNotationEditor->ReadOnly = false;
    drawFrom->Enabled = false;
    dualOrder->Enabled = true;

    startBtn->Visible = false;
    back10Btn->Visible = false;
    nextBtn->Visible = false;
    previousBtn->Visible = false;
    forward10Btn->Visible = false;
    endBtn->Visible = false;
    editBtn->Visible = false;
    cancelBtn->Visible = true;
    closeBtn->Visible = false;
    saveBtn->Visible = true; 
    onlineBtn->Visible = false;
    printCmd->Visible = false;
    statsCmd->Visible = false;
    dataChanged = false;
}

void 
MethodUI::PopulateViewWithId(const Duco::ObjectId& methodId)
{
    if (database->FindMethod(methodId, *currentMethod, false))
    {
        PopulateView();
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
MethodUI::UpdateIdLabel()
{
    if (currentMethod->Id() != -1)
        methodIdLabel->Text = String::Format("{0:D}/{1:D}", currentMethod->Id().Id(), database->NumberOfObjects(TObjectType::EMethod));
    else
        methodIdLabel->Text = "New Method";
}

void 
MethodUI::PopulateView()
{
    if (lineGenerator->IsBusy)
    {
        lineGenerator->CancelAsync();
        restartLineDrawing = true;
    }
    populatingView = true;
    RemoveMethodDrawing();
    UpdateIdLabel();
    nameEditor->Text = DucoUtils::ConvertString(currentMethod->Name());
    typeEditor->Text = DucoUtils::ConvertString(currentMethod->Type());
    placeNotationEditor->Text = DucoUtils::ConvertString(currentMethod->PlaceNotation());
    std::wstring placeNotationTemp;
    currentMethod->BobPlaceNotation(placeNotationTemp);
    bobNotationEditor->Text = DucoUtils::ConvertString(placeNotationTemp);
    currentMethod->SinglePlaceNotation(placeNotationTemp);
    singleNotationEditor->Text = DucoUtils::ConvertString(placeNotationTemp);
    currentMethod->BobPlaceNotation(placeNotationTemp, 1);
    altBobNotationEditor->Text = DucoUtils::ConvertString(placeNotationTemp);
    currentMethod->SinglePlaceNotation(placeNotationTemp, 1);
    altSingleNotationEditor->Text = DucoUtils::ConvertString(placeNotationTemp);
    orderEditor->Text = DucoUtils::ConvertString(currentMethod->OrderName(database->Database(), false));
    dualOrder->Checked = currentMethod->DualOrder();
    SetDualOrderCheckBoxName();
    if (currentMethod->PlaceNotation().length() > 0)
    {
        drawFrom->Enabled = true;
        drawFrom->Maximum = currentMethod->Order();
        if (currentMethod->PrintFromBell() > 0 && currentMethod->PrintFromBell() <= currentMethod->Order())
        {
            if (drawFrom->Value != currentMethod->PrintFromBell())
            {
                drawFrom->Value = currentMethod->PrintFromBell();
            }
        }
        StartLineDrawing(currentMethod->PrintFromBell());
    }
    else
    {
        drawFrom->Enabled = false;
        this->Size = this->GetPreferredSize(System::Drawing::Size(554, 228));
    }
    onlineBtn->Visible = windowState == DucoWindowState::EViewMode && currentMethod->PlaceNotation().length() > 0;
    CheckIsSplicedLabel();
    populatingView = false;
}

void 
MethodUI::CheckIsSplicedLabel()
{
    if (currentMethod->Spliced() && currentMethod->Order() > 0 && currentMethod->Order() != -1)
        isSplicedLbl->Text = "Spliced / multi methods";
    else
        isSplicedLbl->Text = "";
}

void 
MethodUI::ClearView()
{
    delete currentMethod;
    currentMethod = new Method();
    isSplicedLbl->Text = "";
    nameEditor->Text = "";
    typeEditor->Text = "";
    placeNotationEditor->Text = "";
    bobNotationEditor->Text = "";
    singleNotationEditor->Text = "";
    altBobNotationEditor->Text = "";
    altSingleNotationEditor->Text = "";
    orderEditor->Text = "";
    dualOrder->Checked = false;
    dualOrder->Text = "Spliced stages";
    dataChanged = false;
}

System::Void 
MethodUI::DualOrder_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        currentMethod->SetDualOrder(dualOrder->Checked);
        dataChanged = true;
    }
}

System::Void
MethodUI::PlaceNotationChanged(System::Object^ sender, System::EventArgs^ e)
{
    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        if (!populatingView)
        {
            populatingView = true;
            dataChanged = true;
            std::wstring tempString;
            std::wstring updatedNotation;
            size_t currentCursorPos = placeNotationEditor->SelectionStart;
            DucoUtils::ConvertString(placeNotationEditor->Text, tempString);
            if (currentMethod->SetPlaceNotation(tempString, updatedNotation))
            {
                if (currentCursorPos > tempString.length())
                    currentCursorPos = tempString.length() -1;
                placeNotationEditor->Text = DucoUtils::ConvertString(updatedNotation);
                placeNotationEditor->Select(currentCursorPos, 0);
            }
            populatingView = false;
        }
    }
}

System::Void
MethodUI::BobPlaceNotationChanged(System::Object^ sender, System::EventArgs^ e)
{
    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        if (!populatingView)
        {
            populatingView = true;
            dataChanged = true;
            std::wstring tempString;
            std::wstring updatedNotation;
            DucoUtils::ConvertString (bobNotationEditor->Text, tempString);
            if (currentMethod->SetBobPlaceNotation(tempString, 0, updatedNotation))
            {
                bobNotationEditor->Text = DucoUtils::ConvertString (updatedNotation);
            }
            populatingView = false;
        }
    }
}

System::Void
MethodUI::SinglePlaceNotationChanged(System::Object^ sender, System::EventArgs^ e)
{
    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        if (!populatingView)
        {
            populatingView = true;
            dataChanged = true;
            std::wstring tempString;
            std::wstring updatedNotation;
            DucoUtils::ConvertString (singleNotationEditor->Text, tempString);
            if (currentMethod->SetSinglePlaceNotation(tempString, 0, updatedNotation))
            {
                singleNotationEditor->Text = DucoUtils::ConvertString (updatedNotation);
            }
            populatingView = false;
        }
    }
}

System::Void
MethodUI::AltBobNotationEditor_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        if (!populatingView)
        {
            populatingView = true;
            dataChanged = true;
            std::wstring tempString;
            std::wstring updatedNotation;
            DucoUtils::ConvertString (altBobNotationEditor->Text, tempString);
            if (currentMethod->SetBobPlaceNotation(tempString, 1, updatedNotation))
            {
                altBobNotationEditor->Text = DucoUtils::ConvertString (updatedNotation);
            }
            populatingView = false;
        }
    }
}

System::Void
MethodUI::AltSingleNotationEditor_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        if (!populatingView)
        {
            populatingView = true;
            dataChanged = true;
            std::wstring tempString;
            std::wstring updatedNotation;
            DucoUtils::ConvertString (altSingleNotationEditor->Text, tempString);
            if (currentMethod->SetSinglePlaceNotation(tempString, 1, updatedNotation))
            {
                altSingleNotationEditor->Text = DucoUtils::ConvertString (updatedNotation);
            }
            populatingView = false;
        }
    }
}

System::Void
MethodUI::MethodDataChanged(System::Object^ sender, System::EventArgs^ e)
{
    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        dataChanged = true;
        if (sender == orderEditor)
        {
            unsigned int orderNumber = 0;
            bool isDualOrder (false);
            if (database->FindOrderNumber(orderEditor->Text, orderNumber, isDualOrder))
            {
                currentMethod->SetOrder(orderNumber);
                currentMethod->SetDualOrder(isDualOrder);
            }
            else
            {
                currentMethod->SetOrder(0);
            }
            SetDualOrderCheckBoxName();
            if (windowState != DucoWindowState::ENewMode)
            {
                if (drawFrom->Value > orderNumber)
                {
                    drawFrom->Value = orderNumber;
                }
                drawFrom->Maximum = orderNumber;
            }
        }
        else if (sender == typeEditor)
        {
            std::wstring tempString;
            DucoUtils::ConvertString (typeEditor->Text, tempString);
            currentMethod->SetType(tempString);
        }
        else if (sender == nameEditor)
        {
            std::wstring tempString;
            DucoUtils::ConvertString (nameEditor->Text, tempString);
            currentMethod->SetName(tempString);
        }
        else
            dataChanged = false;

        CheckIsSplicedLabel();
    }
}

bool 
MethodUI::StartLineDrawing(unsigned int startingFromBell)
{
    if (methodDrawing == nullptr)
    {
        triedToStartDrawing = true;
        return false;
    }
    restartLineDrawing = false;
    triedToStartDrawing = false;
    printCmd->Enabled = false;

    if (!lineGenerator->IsBusy)
    {
        lineGenerator->RunWorkerAsync(startingFromBell);
        return true;
    }
    else
    {
        lineGenerator->CancelAsync();
    }
    restartLineDrawing = true;
    return false;
}

namespace DucoUI
{
	ref class LineDrawingResultArgs
	{
	public:
		bool    completed;
        Image^  image;
	};
}

void 
MethodUI::DrawLine(System::Object^ sender, System::ComponentModel::DoWorkEventArgs^ e)
{
    BackgroundWorker^ worker = dynamic_cast<BackgroundWorker^>(sender);

    unsigned int startingLead = static_cast<unsigned int>(e->Argument);

    LineDrawingResultArgs^ args = gcnew LineDrawingResultArgs();
    System::Drawing::Graphics^ lineDrawer = Graphics::FromImage(methodDrawing);

    MethodLineDrawer^ drawer = nullptr;
    switch (database->Settings().LineType())
    {
    case Duco::DatabaseSettings::ELongLine:
        drawer = gcnew LongMethodLineDrawer(lineDrawer, *currentMethod, database, worker);
        break;
    case Duco::DatabaseSettings::EAnnotated:
        drawer = gcnew AnnotatedMethodLineDrawer(lineDrawer, *currentMethod, database, worker);
        break;
    case Duco::DatabaseSettings::ENormalLine:
    default:
        drawer = gcnew NormalMethodLineDrawer(lineDrawer, *currentMethod, database, worker);
        break;
    }

    args->completed = drawer->StartDrawing(startingLead);
    args->image = methodDrawing;
    methodDrawing = nullptr;
    e->Result = args;

    delete drawer;
    drawer = nullptr;
    delete lineDrawer;
    lineDrawer = nullptr;
    System::GC::Collect();
}

void
MethodUI::RemoveMethodDrawing()
{
    methodPicture->Visible = false;
    methodPicture->Image = nullptr;
    printCmd->Enabled = false;
}

void
MethodUI::DrawLineCompleted(System::Object^ sender, System::ComponentModel::RunWorkerCompletedEventArgs^ args)
{
    if (args->Error != nullptr)
    {
        MessageBox::Show(this, args->Error->ToString(), "Error drawing method");
    }

    if (restartLineDrawing)
    {
        restartLineDrawing = false;
        StartLineDrawing(Convert::ToUInt16(drawFrom->Value));
    }
    else
    {
        bool error = false;
        if (args->Result == nullptr)
        {
            error = true;
        }
        else
        {
            LineDrawingResultArgs^ resultArgs = dynamic_cast<LineDrawingResultArgs^>(args->Result);
            if (resultArgs->completed)
            {
                methodPicture->Image = resultArgs->image;
                methodPicture->Visible = true;
                printCmd->Enabled = true;
                this->Size = this->GetPreferredSize(System::Drawing::Size(554, 228));
                this->Invalidate(false); // Required to generate the next Metafile
            }
            else
            {
                error = true;
            }
        }
        if (error)
        {
            RemoveMethodDrawing();
        }
    }
}

System::Void
MethodUI::MethodIdLabel_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (windowState == DucoWindowState::EViewMode)
    {
        System::Int16 id = -1;
        InputIndexQueryUI^ findIdDialog = gcnew InputIndexQueryUI("method", id);
        System::Windows::Forms::DialogResult returnVal = findIdDialog->ShowDialog();
        if (returnVal == ::DialogResult::OK)
        {
            PopulateViewWithId(id);
        }
    }
}

System::Void
MethodUI::SetDualOrderCheckBoxName()
{
    bool setVisible = currentMethod->Order() > database->LowestValidOrderNumber() && database->LowestValidOrderNumber() != -1;
    dualOrder->Visible = setVisible;
    dualOrder->Text = DucoUtils::ConvertString(L"Spliced " + Method::AnotherOrderName(database->Database(), currentMethod->Order(), true));
}

System::Void
MethodUI::DrawFrom_ValueChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (windowState == DucoWindowState::EEditMode || windowState == DucoWindowState::ENewMode)
    {
        currentMethod->SetPrintFromBell(Convert::ToInt16(drawFrom->Value));
        dataChanged = true;
    }    
    else
    {
        if (!populatingView)
        {
            StartLineDrawing(Convert::ToUInt16(drawFrom->Value));
        }
    }
}

void
MethodUI::NotationEditor_KeyDown( Object^ sender, System::Windows::Forms::KeyEventArgs^ e )
{
    // Initialize the flag to false.
	Keys currentKeys = Control::ModifierKeys;
    invalidCharEntered = !DucoUIUtils::NotationKey(e, currentKeys, placeNotationEditor == sender, currentMethod->Order());
}

void
MethodUI::NotationEditor_KeyPressed(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e)
{
    // Check for the flag being set in the KeyDown event.
    if ( invalidCharEntered == true )
    {
    // Stop the character from being entered into the control since it is non-numerical.
        e->Handled = true;
    }
}

System::Void
MethodUI::compLibToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (currentMethod->Name().length() == 0)
    {
        SoundUtils::PlayErrorSound();
        return;
    }
    String^ fullUrl = DucoUtils::ConvertString(database->Settings().CompLibURL() + L"?keywords=" + currentMethod->Name() + L"&stage=" + DucoEngineUtils::ToString(currentMethod->Order()));
    try
    {
        System::Diagnostics::Process::Start(fullUrl);
    }
    catch (System::ComponentModel::Win32Exception^ noBrowserEx)
    {
        SoundUtils::PlayErrorSound();
        System::Windows::Forms::MessageBox::Show(this, noBrowserEx->Message);
    }
    catch (System::Exception^ otherEx)
    {
        SoundUtils::PlayErrorSound();
        System::Windows::Forms::MessageBox::Show(this, otherEx->Message);
    }
}
