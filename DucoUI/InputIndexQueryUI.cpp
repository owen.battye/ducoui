#include "InputIndexQueryUI.h"
#include "SystemDefaultIcon.h"
#include "SoundUtils.h"
#include "DucoUIUtils.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace DucoUI;

InputIndexQueryUI::InputIndexQueryUI(System::String^ promptStr, System::Int16& newId)
: id(newId)
{
    InitializeComponent();
    this->Text = L"Find " + promptStr;
}

InputIndexQueryUI::~InputIndexQueryUI()
{
    if (components)
    {
        delete components;
    }
}

System::Void 
InputIndexQueryUI::InputIndexQueryUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    this->idEditor->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &InputIndexQueryUI::KeyDown);
    this->idEditor->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &InputIndexQueryUI::KeyPressed);
}

System::Void 
InputIndexQueryUI::okBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    bool suceeded = false;
    if (idEditor->Text->Length > 0)
    {
        try
        {
            id = Convert::ToInt16(idEditor->Text);
            this->DialogResult = ::DialogResult::OK;
            suceeded = true;
        }
        catch (Exception^)
        {
            SoundUtils::PlayErrorSound();
            idEditor->Text = "";
        }
    }
    
    if (suceeded)
        Close();
}

System::Void
InputIndexQueryUI::cancelBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    id = -1;
    Close();
}

void 
InputIndexQueryUI::KeyDown( Object^ sender, System::Windows::Forms::KeyEventArgs^ e )
{
    nonNumberEntered = !DucoUIUtils::NumericKey(e, Control::ModifierKeys);
}

void
InputIndexQueryUI::KeyPressed(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e)
{
    // Check for the flag being set in the KeyDown event.
    if ( nonNumberEntered == true )
    {
    // Stop the character from being entered into the control since it is non-numerical.
        e->Handled = true;
    }
}
