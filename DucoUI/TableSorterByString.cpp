#include "TableSorterByString.h"

using namespace DucoUI;
using namespace System;
using namespace System::Windows::Forms;

TableSorterByString::TableSorterByString(bool newAscending, int newColumnId)
:   ascending(newAscending), columnId(newColumnId)
{
}

TableSorterByString::~TableSorterByString()
{

}

int 
TableSorterByString::Compare(System::Object^ firstObject, System::Object^ secondObject)
{
    DataGridViewRow^ row1 = static_cast<DataGridViewRow^>(firstObject);
    DataGridViewRow^ row2 = static_cast<DataGridViewRow^>(secondObject);
    if (row1 == row2)
        return 0;

    System::String^ cellOne = ConvertValueToString(row1, columnId);
    System::String^ cellTwo = ConvertValueToString(row2, columnId);

    if (ascending)
    {
        return System::String::Compare(cellOne, cellTwo);
    }
    return System::String::Compare(cellTwo, cellOne);
}

System::String^
TableSorterByString::ConvertValueToString(DataGridViewRow^ row, int cellNumber)
{
    System::String^ returnValue = "";
    try
    {
        Object^ object = nullptr;
        if (cellNumber != -1)
        {
            object = row->Cells[cellNumber]->Value;
        }
        else
        {
            object = row->HeaderCell->Value;
        }

        if (object != nullptr)
            returnValue = Convert::ToString(object);
    }
    catch (Exception^)
    {
        returnValue = "";
    }
    return returnValue;
}