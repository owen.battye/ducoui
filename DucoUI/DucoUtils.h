#pragma once

#include <PerformanceDate.h>
#include <PerformanceTime.h>
#include <set>
#include <list>

namespace Duco
{
    class DatabaseSettings;
    class ObjectId;
    class Peal;
}

namespace DucoUI
{
ref class DucoUtils
{
public:
    static void ConvertString(System::String^ s, std::string& os);
    static void ConvertString(System::String^ s, std::wstring& os);
    static System::String^ ConvertString(const std::string& os);
    static System::String^ ConvertString(const std::wstring& os);
    static System::String^ ConvertString(wchar_t singleChar);
    static System::String^ ConvertString(const Duco::PerformanceDate& date, bool longDate);
    static System::String^ ConvertString(const Duco::ObjectId& id);
    static System::String^ ConvertString(System::Windows::Forms::DataGridViewCell^ currentCell);
    static System::String^ ConvertString(System::Collections::Generic::List<System::UInt64>^ idList);

    static System::String^ EncodeString(const std::wstring& os); // prepare for javascript processing

    static System::String^ ParseUrl (System::String^ str);
    static bool CheckUrlForDomain(System::String^ url, const Duco::DatabaseSettings& settings);

    static System::String^ PrintPealTime(const Duco::PerformanceTime& time, bool longFormat);
    static System::String^ PrintPealTime(size_t timeInMinutes, bool longFormat);

    static System::DateTime^ FirstDate();
    static System::DateTime^ ConvertDate(const Duco::PerformanceDate& date);
    static Duco::PerformanceDate ConvertDate(System::DateTime^ date);
    static System::DateTime^ ConvertDate(time_t newDate);

    static Duco::PerformanceTime ToTime(System::String^ timeString);

    static System::String^ FormatDoubleToString(double number, unsigned int digits);
    static System::String^ FormatFloatToString(float number);
    static System::String^ FormatUIntToPounds(unsigned int pence);

    static System::String^ FormatFilename(System::String^ originalFilename);

    static unsigned int ConvertString(System::String^ numberString);
    static short ToInt(System::Windows::Forms::DataGridViewCell^ currentCell);
    static bool ConvertTextToNumber(System::String^  str, unsigned int& returnNumber);
    static System::String^ PadWithChar(System::String^ str, System::Char paddingChar, System::Int16 noOfChars);

    static bool ConfirmChanges(System::Windows::Forms::IWin32Window^ owner);
    static bool ConfirmLooseChanges(System::String^ objectName, System::Windows::Forms::IWin32Window^ owner);
    static bool ShowInvalidPealDataDialog(Duco::DatabaseSettings& settings, const Duco::Peal& currentPeal, System::Windows::Forms::IWin32Window^ owner);

    static bool CompareString (System::String^ lhs, const std::wstring& rhs);
    static System::Void RemoveAllExceptNumbers(System::String^% idNumber);

    static System::Collections::Generic::List<System::UInt64>^ ConvertCommaSeperatedIdStringToList(System::String^ idString);

    static System::Void Convert(const std::list<Duco::ObjectId>& inList, System::Collections::Generic::List<System::UInt64>^% outList);
    static System::Void Convert(const std::set<Duco::ObjectId>& inList, System::Collections::Generic::List<System::UInt64>^% outList);
    static System::Void Convert(System::Collections::Generic::List<System::UInt64>^% inList, std::set<Duco::ObjectId>& outList);
    };
}
