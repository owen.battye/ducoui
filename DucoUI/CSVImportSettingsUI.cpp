#include <SettingsFile.h>
#include "WindowsSettings.h"

#include "CSVImportSettingsUI.h"

#include "DucoUtils.h"
#include <SettingsFile.h>
#include "SystemDefaultIcon.h"
#include <CsvImporter.h>
#include "SoundUtils.h"
#include <CsvImportFields.h>

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Windows::Forms;

CSVImportSettingsUI::CSVImportSettingsUI(DucoUI::WindowsSettings^ theSettings, System::String^ fileNameOnly)
    : populatingData(false)
{
    std::string settingsFileName;
    DucoUtils::ConvertString(fileNameOnly, settingsFileName);
    settings = new Duco::SettingsFile(theSettings->ConfigDir(), settingsFileName);
    if (settings->NoOfSettings() <= 0)
    {
        CsvImporter::CreateDefaultSettings(*settings);
    }
    InitializeComponent();
}

CSVImportSettingsUI::!CSVImportSettingsUI()
{
    delete settings;
}

CSVImportSettingsUI::~CSVImportSettingsUI()
{
    this->!CSVImportSettingsUI();
    if (components)
    {
        delete components;
    }
}

System::Void
CSVImportSettingsUI::CSVImportSettingsUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    this->Closing += gcnew System::ComponentModel::CancelEventHandler(this, &CSVImportSettingsUI::ClosingEvent);
    PopulateView();
}

System::Void
CSVImportSettingsUI::ClosingEvent(System::Object^ object, System::ComponentModel::CancelEventArgs^ e)
{
    if ( dataChanged )
    {
        if ( DucoUtils::ConfirmLooseChanges("settings", this) )
        {
            e->Cancel = true;
        }
    }
}

System::Void
CSVImportSettingsUI::cancelBtn_Click(System::Object^  sender, System::EventArgs^  e) 
{
    settings->ClearSettingsChanged();
    Close();
}

System::Void
CSVImportSettingsUI::saveBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    SaveSettings();
    Close();
}

System::Void
CSVImportSettingsUI::pealBookSettingsBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    CsvImporter::CreateDefaultPealBookSettings(*settings);
    PopulateView();
    SetDataChanged();
}

System::Void
CSVImportSettingsUI::resetSettingsBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    settings->ClearAllSettings();
    PopulateView();
    SetDataChanged();
}

System::Void
CSVImportSettingsUI::defaultsBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    CsvImporter::CreateDefaultSettings(*settings);
    PopulateView();
    SetDataChanged();
}

System::Void
CSVImportSettingsUI::SetDataChanged()
{
    if (!populatingData)
    {
        dataChanged = true;
        saveBtn->Enabled = true;
    }
}

System::Void
CSVImportSettingsUI::ValueChanged(System::Object^  sender, System::EventArgs^  e)
{
    SetDataChanged();
}

System::Void
CSVImportSettingsUI::CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    SetDataChanged();
}

System::Void
CSVImportSettingsUI::singleDateField_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    SetDataChanged();
    dayLbl->Text = singleDateField->Checked ? "Date" : "Day";
    month->Enabled = !singleDateField->Checked;
    year->Enabled = !singleDateField->Checked;
}

System::Void
CSVImportSettingsUI::singleMethodField_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    SetDataChanged();
    methodType->Enabled = !singleMethodField->Checked;
    methodStage->Enabled = !singleMethodField->Checked;
}

System::Void
CSVImportSettingsUI::singleTimeField_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    SetDataChanged();
    hoursLbl->Text = singleTimeField->Checked ? "Time" : "Hours";
    minutes->Enabled = !singleTimeField->Checked;
}

System::Void
CSVImportSettingsUI::TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    SetDataChanged();
}

System::Void
CSVImportSettingsUI::PopulateSetting(System::Windows::Forms::NumericUpDown^ field, const std::wstring& settingId)
{
    int setting (0);
    if (settings->Setting(settingId, setting))
    {
        field->Value = setting;
    }
    else
    {
        field->Value = -1;
    }
}

System::Void
CSVImportSettingsUI::PopulateSetting(System::Windows::Forms::TextBox^ field, const std::wstring& settingId)
{
    std::wstring setting (L"");
    if (settings->Setting(settingId, setting))
    {
        field->Text = DucoUtils::ConvertString(setting);
    }
    else
    {
        field->Text = "";
    }
}

System::Void
CSVImportSettingsUI::PopulateView()
{
    populatingData = true;
    PopulateSetting(countyField, KTowerCountyFieldNo);
    PopulateSetting(cityField, KTowerTownFieldNo);
    PopulateSetting(nameField, KTowerNameFieldNo);
    PopulateSetting(lbs, KTenorWeightFieldNo3);
    PopulateSetting(quarters, KTenorWeightFieldNo2);
    PopulateSetting(cwt, KTenorWeightFieldNo1);
    PopulateSetting(year, KYearFieldNo);
    PopulateSetting(month, KMonthFieldNo);
    PopulateSetting(day, KDayFieldNo);
    PopulateSetting(conductor, KConductorFieldNo);
    PopulateSetting(composer, KComposerFieldNo);
    
    PopulateSetting(lastRinger, KLastRingerFieldNo);
    PopulateSetting(firstRinger, KFirstRingerFieldNo);
    PopulateSetting(methodStage, KStageFieldNo);
    PopulateSetting(methodType, KMethodTypeFieldNo);
    PopulateSetting(methodName, KMethodFieldNo);
    PopulateSetting(splicedMethods, KSplicedMethodsFieldNo);
    PopulateSetting(minutes, KMinutesFieldNo);
    PopulateSetting(hours, KHoursFieldNo);
    PopulateSetting(association, KAssociationFieldNo);
    PopulateSetting(footnotes, KFootnotesFieldNo);
    PopulateSetting(tenorKey, KTenorKeyFieldNo);
    PopulateSetting(changes, KChangesFieldNo);
    PopulateSetting(handbellField, KHandbellFieldNo);
    PopulateSetting(handbellStr, KHandbellStringFieldNo);
    PopulateSetting(ringingWorldReference, KRingingWorldFieldNo);
    PopulateSetting(bellBoardReference, KBellBoardFieldNo);

    int setting (0);
    bool boolSetting;
    if (settings->Setting(KConductorTypeFieldNo, setting))
    {
        switch (setting)
        {
        case 0:
            conductorBellNo->Checked = false;
            conductorName->Checked = true;
            break;
        case 1:
            conductorBellNo->Checked = true;
            conductorName->Checked = false;
            break;
        default:
            conductorBellNo->Checked = false;
            conductorName->Checked = false;
            break;
        }
    }
    else
    {
        conductorBellNo->Checked = false;
        conductorName->Checked = false;
    }
    if (settings->Setting(KHandbellTypeFieldNo, setting))
    {
        switch (setting)
        {
        case 1: // Alternate bells
            handbellAlternate->Checked = true;
            break;
        default:
            handbellAlternate->Checked = false;
            break;
        }
    }
    else
    {
        handbellAlternate->Checked = false;
    }
    if (settings->Setting(KDateSingleFieldNo, boolSetting))
    {
        dayLbl->Text = boolSetting ? "Date" : "Day";
        month->Enabled = !boolSetting;
        year->Enabled = !boolSetting;
        singleDateField->Checked = boolSetting;
    }
    else
    {
        dayLbl->Text = "Day";
        singleDateField->Checked = false;
    }
    if (settings->Setting(KMethodSingleFieldNo, boolSetting))
    {
        methodType->Enabled = !boolSetting;
        methodStage->Enabled = !boolSetting;
        singleMethodField->Checked = boolSetting;
    }
    else
    {
        singleMethodField->Checked = false;
    }
    if (settings->Setting(KTimeSingleFieldNo, boolSetting))
    {
        minutes->Enabled = !boolSetting;
        singleTimeField->Checked = boolSetting;
    }
    else
    {
        singleTimeField->Checked = false;
    }
    if (settings->Setting(KRemoveBellNumberFromRingersFieldNo, boolSetting))
    {
        removeBellNoFromRingers->Checked = boolSetting;
    }
    else
    {
        removeBellNoFromRingers->Checked = false;
    }
    if (settings->Setting(KIgnoreFirstLineFieldNo, boolSetting))
    {
        ignoreFirstLine->Checked = boolSetting;
    }
    else
    {
        ignoreFirstLine->Checked = true;
    }
    if (settings->Setting(KEnforceQuotesFieldNo, boolSetting))
    {
        enforceQuotes->Checked = boolSetting;
    }
    else
    {
        enforceQuotes->Checked = false;
    }
    if (settings->Setting(KDecodeMultiByteFieldNo, boolSetting))
    {
        decodeCheckBox->Checked = boolSetting;
    }
    else
    {
        decodeCheckBox->Checked = false;
    }
   
    populatingData = false;
}

System::Void
CSVImportSettingsUI::SaveSetting(System::Windows::Forms::NumericUpDown^ field, const std::wstring& settingId, Duco::SettingsFile& settings)
{
    int settingVal (field->Value);
    settings.Set(settingId, settingVal);
}

System::Void
CSVImportSettingsUI::SaveSetting(System::Windows::Forms::TextBox^ field, const std::wstring& settingId, Duco::SettingsFile& settings)
{
    std::wstring settingVal (L"");
    DucoUtils::ConvertString(field->Text, settingVal);
    settings.Set(settingId, settingVal);
}

System::Void
CSVImportSettingsUI::SaveSettings()
{
    Duco::SettingsFile copySettings(*settings);
    SaveSetting(countyField, KTowerCountyFieldNo, copySettings);
    SaveSetting(cityField, KTowerTownFieldNo, copySettings);
    SaveSetting(nameField, KTowerNameFieldNo, copySettings);
    SaveSetting(lbs, KTenorWeightFieldNo3, copySettings);
    SaveSetting(quarters, KTenorWeightFieldNo2, copySettings);
    SaveSetting(cwt, KTenorWeightFieldNo1, copySettings);
    SaveSetting(year, KYearFieldNo, copySettings);
    SaveSetting(month, KMonthFieldNo, copySettings);
    SaveSetting(day, KDayFieldNo, copySettings);
    SaveSetting(conductor, KConductorFieldNo, copySettings);
    SaveSetting(composer, KComposerFieldNo, copySettings);
    copySettings.Set(KRemoveBellNumberFromRingersFieldNo, removeBellNoFromRingers->Checked);
    SaveSetting(lastRinger, KLastRingerFieldNo, copySettings);
    SaveSetting(firstRinger, KFirstRingerFieldNo, copySettings);
    SaveSetting(methodStage, KStageFieldNo, copySettings);
    SaveSetting(methodType, KMethodTypeFieldNo, copySettings);
    SaveSetting(methodName, KMethodFieldNo, copySettings);
    SaveSetting(splicedMethods, KSplicedMethodsFieldNo, copySettings);
    SaveSetting(minutes, KMinutesFieldNo, copySettings);
    SaveSetting(hours, KHoursFieldNo, copySettings);
    SaveSetting(association, KAssociationFieldNo, copySettings);
    SaveSetting(footnotes, KFootnotesFieldNo, copySettings);
    SaveSetting(tenorKey, KTenorKeyFieldNo, copySettings);
    SaveSetting(changes, KChangesFieldNo, copySettings);
    SaveSetting(handbellField, KHandbellFieldNo, copySettings);
    SaveSetting(handbellStr, KHandbellStringFieldNo, copySettings);
    SaveSetting(ringingWorldReference, KRingingWorldFieldNo, copySettings);
    SaveSetting(bellBoardReference, KBellBoardFieldNo, copySettings);
    copySettings.Set(KDateSingleFieldNo, singleDateField->Checked);
    copySettings.Set(KMethodSingleFieldNo, singleMethodField->Checked);
    copySettings.Set(KTimeSingleFieldNo, singleTimeField->Checked);

    if (conductorBellNo->Checked)
        copySettings.Set(KConductorTypeFieldNo, 1);
    else if (conductorName->Checked)
        copySettings.Set(KConductorTypeFieldNo, 0);
    else
        copySettings.Set(KConductorTypeFieldNo, -1);
    copySettings.Set(KHandbellTypeFieldNo, handbellAlternate->Checked);
    copySettings.Set(KIgnoreFirstLineFieldNo, ignoreFirstLine->Checked);
    copySettings.Set(KEnforceQuotesFieldNo, enforceQuotes->Checked);
    copySettings.Set(KDecodeMultiByteFieldNo, decodeCheckBox->Checked);

    if (copySettings != *settings)
    {
        if (!copySettings.Save())
        {
            SoundUtils::PlayErrorSound();
        }
        else
        {
            dataChanged = false;
        }
    }
    else
    {
        dataChanged = false;
    }
}
