#pragma once

namespace DucoUI
{
    public ref class CompositionSearchUI : public System::Windows::Forms::Form,
                                           public DucoUI::ProgressCallbackUI
    {
    public:
        CompositionSearchUI(DucoUI::DatabaseManager^ theDatabase, bool setStartSearchForInvalid);

        // from ProgressCallbackUI
        virtual void Initialised();
        virtual void Step(int progressPercent);
        virtual void Complete();

    protected:
        !CompositionSearchUI();
        ~CompositionSearchUI();

        System::Void CompositionSearchUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void ClosingEvent( Object^ /*sender*/, System::ComponentModel::CancelEventArgs^ e );

        System::Void backgroundWorker_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void backgroundWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void backgroundWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);
        System::Void PopulateResults(System::ComponentModel::BackgroundWorker^ bgworker);
        bool CreateSearchParameters(Duco::DatabaseSearch& search);

        System::Void dataGridView_CellDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);
        System::Void dataGridView_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);
        System::Void clearBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void searchBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);

    private: System::Windows::Forms::CheckBox^  snapFinish;
    private: System::Windows::Forms::CheckBox^  snapStart;
    private: System::Windows::Forms::ComboBox^  methodEditor;
    private: System::Windows::Forms::TextBox^  notesEditor;
    private: System::Windows::Forms::ComboBox^  composerEditor;
    private: System::Windows::Forms::TextBox^  nameEditor;
    private: System::Windows::Forms::DataGridView^  dataGridView;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  idColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  nameColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  lengthColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  methodColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  composerColumn;
    private: System::Windows::Forms::ProgressBar^  progressBar;
    private: System::Windows::Forms::Button^  closeBtn;
    private: System::Windows::Forms::Button^  searchBtn;
    private: System::Windows::Forms::Button^  clearBtn;
    private: System::Windows::Forms::TextBox^  results;
    private: System::ComponentModel::BackgroundWorker^  backgroundWorker;

    private:
        DucoUI::DatabaseManager^ database;
        bool startSearchForInvalid;
        System::ComponentModel::IContainer^  components;

        std::set<Duco::ObjectId>*                           foundCompositionIds;
        System::Collections::Generic::List<System::Int16>^  allMethodIds;
        DucoUI::RingerDisplayCache^ ringerCache;
        System::Windows::Forms::CheckBox^                   error;
        System::Windows::Forms::CheckBox^                   provenFalse;

        DucoUI::ProgressCallbackWrapper* progressWrapper;

#pragma region Windows Form Designer generated code
        void InitializeComponent()
        {
            this->components = (gcnew System::ComponentModel::Container());
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
            System::Windows::Forms::Panel^ btnPanel;
            System::Windows::Forms::Label^ resultsLbl;
            System::Windows::Forms::Panel^ settingsPanel;
            System::Windows::Forms::GroupBox^ otherGroup;
            System::Windows::Forms::GroupBox^ methodGroup;
            System::Windows::Forms::GroupBox^ notesGroup;
            System::Windows::Forms::GroupBox^ composerGroup;
            System::Windows::Forms::GroupBox^ nameGroup;
            System::Windows::Forms::ToolTip^ toolTip;
            this->closeBtn = (gcnew System::Windows::Forms::Button());
            this->searchBtn = (gcnew System::Windows::Forms::Button());
            this->clearBtn = (gcnew System::Windows::Forms::Button());
            this->results = (gcnew System::Windows::Forms::TextBox());
            this->progressBar = (gcnew System::Windows::Forms::ProgressBar());
            this->provenFalse = (gcnew System::Windows::Forms::CheckBox());
            this->error = (gcnew System::Windows::Forms::CheckBox());
            this->snapFinish = (gcnew System::Windows::Forms::CheckBox());
            this->snapStart = (gcnew System::Windows::Forms::CheckBox());
            this->methodEditor = (gcnew System::Windows::Forms::ComboBox());
            this->notesEditor = (gcnew System::Windows::Forms::TextBox());
            this->composerEditor = (gcnew System::Windows::Forms::ComboBox());
            this->nameEditor = (gcnew System::Windows::Forms::TextBox());
            this->dataGridView = (gcnew System::Windows::Forms::DataGridView());
            this->idColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->nameColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->lengthColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->methodColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->composerColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->backgroundWorker = (gcnew System::ComponentModel::BackgroundWorker());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            btnPanel = (gcnew System::Windows::Forms::Panel());
            resultsLbl = (gcnew System::Windows::Forms::Label());
            settingsPanel = (gcnew System::Windows::Forms::Panel());
            otherGroup = (gcnew System::Windows::Forms::GroupBox());
            methodGroup = (gcnew System::Windows::Forms::GroupBox());
            notesGroup = (gcnew System::Windows::Forms::GroupBox());
            composerGroup = (gcnew System::Windows::Forms::GroupBox());
            nameGroup = (gcnew System::Windows::Forms::GroupBox());
            toolTip = (gcnew System::Windows::Forms::ToolTip(this->components));
            tableLayoutPanel1->SuspendLayout();
            btnPanel->SuspendLayout();
            settingsPanel->SuspendLayout();
            otherGroup->SuspendLayout();
            methodGroup->SuspendLayout();
            notesGroup->SuspendLayout();
            composerGroup->SuspendLayout();
            nameGroup->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->BeginInit();
            this->SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 1;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->Controls->Add(btnPanel, 0, 2);
            tableLayoutPanel1->Controls->Add(settingsPanel, 0, 0);
            tableLayoutPanel1->Controls->Add(this->dataGridView, 0, 1);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 3;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 118)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 27)));
            tableLayoutPanel1->Size = System::Drawing::Size(654, 294);
            tableLayoutPanel1->TabIndex = 0;
            // 
            // btnPanel
            // 
            btnPanel->Controls->Add(this->closeBtn);
            btnPanel->Controls->Add(this->searchBtn);
            btnPanel->Controls->Add(this->clearBtn);
            btnPanel->Controls->Add(this->results);
            btnPanel->Controls->Add(resultsLbl);
            btnPanel->Controls->Add(this->progressBar);
            btnPanel->Dock = System::Windows::Forms::DockStyle::Fill;
            btnPanel->Location = System::Drawing::Point(0, 267);
            btnPanel->Margin = System::Windows::Forms::Padding(0);
            btnPanel->Name = L"btnPanel";
            btnPanel->Size = System::Drawing::Size(654, 27);
            btnPanel->TabIndex = 0;
            // 
            // closeBtn
            // 
            this->closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
            this->closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            this->closeBtn->Location = System::Drawing::Point(576, 1);
            this->closeBtn->Name = L"closeBtn";
            this->closeBtn->Size = System::Drawing::Size(75, 23);
            this->closeBtn->TabIndex = 5;
            this->closeBtn->Text = L"Close";
            this->closeBtn->UseVisualStyleBackColor = true;
            this->closeBtn->Click += gcnew System::EventHandler(this, &CompositionSearchUI::closeBtn_Click);
            // 
            // searchBtn
            // 
            this->searchBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
            this->searchBtn->Location = System::Drawing::Point(495, 1);
            this->searchBtn->Name = L"searchBtn";
            this->searchBtn->Size = System::Drawing::Size(75, 23);
            this->searchBtn->TabIndex = 4;
            this->searchBtn->Text = L"Search";
            this->searchBtn->UseVisualStyleBackColor = true;
            this->searchBtn->Click += gcnew System::EventHandler(this, &CompositionSearchUI::searchBtn_Click);
            // 
            // clearBtn
            // 
            this->clearBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
            this->clearBtn->Location = System::Drawing::Point(414, 1);
            this->clearBtn->Name = L"clearBtn";
            this->clearBtn->Size = System::Drawing::Size(75, 23);
            this->clearBtn->TabIndex = 3;
            this->clearBtn->Text = L"Clear";
            this->clearBtn->UseVisualStyleBackColor = true;
            this->clearBtn->Click += gcnew System::EventHandler(this, &CompositionSearchUI::clearBtn_Click);
            // 
            // results
            // 
            this->results->Location = System::Drawing::Point(158, 3);
            this->results->Name = L"results";
            this->results->ReadOnly = true;
            this->results->Size = System::Drawing::Size(37, 20);
            this->results->TabIndex = 2;
            // 
            // resultsLbl
            // 
            resultsLbl->AutoSize = true;
            resultsLbl->Location = System::Drawing::Point(110, 6);
            resultsLbl->Name = L"resultsLbl";
            resultsLbl->Size = System::Drawing::Size(42, 13);
            resultsLbl->TabIndex = 1;
            resultsLbl->Text = L"Results";
            // 
            // progressBar
            // 
            this->progressBar->Location = System::Drawing::Point(4, 3);
            this->progressBar->Name = L"progressBar";
            this->progressBar->Size = System::Drawing::Size(100, 17);
            this->progressBar->TabIndex = 0;
            // 
            // settingsPanel
            // 
            settingsPanel->Controls->Add(otherGroup);
            settingsPanel->Controls->Add(methodGroup);
            settingsPanel->Controls->Add(notesGroup);
            settingsPanel->Controls->Add(composerGroup);
            settingsPanel->Controls->Add(nameGroup);
            settingsPanel->Dock = System::Windows::Forms::DockStyle::Fill;
            settingsPanel->Location = System::Drawing::Point(0, 0);
            settingsPanel->Margin = System::Windows::Forms::Padding(0);
            settingsPanel->Name = L"settingsPanel";
            settingsPanel->Size = System::Drawing::Size(654, 118);
            settingsPanel->TabIndex = 1;
            // 
            // otherGroup
            // 
            otherGroup->Controls->Add(this->provenFalse);
            otherGroup->Controls->Add(this->error);
            otherGroup->Controls->Add(this->snapFinish);
            otherGroup->Controls->Add(this->snapStart);
            otherGroup->Location = System::Drawing::Point(547, 4);
            otherGroup->Name = L"otherGroup";
            otherGroup->Size = System::Drawing::Size(99, 110);
            otherGroup->TabIndex = 4;
            otherGroup->TabStop = false;
            otherGroup->Text = L"Other";
            // 
            // provenFalse
            // 
            this->provenFalse->AutoSize = true;
            this->provenFalse->Location = System::Drawing::Point(7, 91);
            this->provenFalse->Name = L"provenFalse";
            this->provenFalse->Size = System::Drawing::Size(51, 17);
            this->provenFalse->TabIndex = 3;
            this->provenFalse->Text = L"False";
            toolTip->SetToolTip(this->provenFalse, L"Peals proven false or doesn\'t ends with rounds, warning this could extend the tim"
                L"e your search takes to complete significantly.");
            this->provenFalse->UseVisualStyleBackColor = true;
            // 
            // error
            // 
            this->error->AutoSize = true;
            this->error->Location = System::Drawing::Point(7, 68);
            this->error->Name = L"error";
            this->error->Size = System::Drawing::Size(91, 17);
            this->error->TabIndex = 2;
            this->error->Text = L"Contains error";
            this->error->UseVisualStyleBackColor = true;
            // 
            // snapFinish
            // 
            this->snapFinish->AutoSize = true;
            this->snapFinish->Location = System::Drawing::Point(7, 44);
            this->snapFinish->Name = L"snapFinish";
            this->snapFinish->Size = System::Drawing::Size(78, 17);
            this->snapFinish->TabIndex = 1;
            this->snapFinish->Text = L"Snap finish";
            this->snapFinish->UseVisualStyleBackColor = true;
            // 
            // snapStart
            // 
            this->snapStart->AutoSize = true;
            this->snapStart->Location = System::Drawing::Point(7, 20);
            this->snapStart->Name = L"snapStart";
            this->snapStart->Size = System::Drawing::Size(74, 17);
            this->snapStart->TabIndex = 0;
            this->snapStart->Text = L"Snap start";
            this->snapStart->UseVisualStyleBackColor = true;
            // 
            // methodGroup
            // 
            methodGroup->Controls->Add(this->methodEditor);
            methodGroup->Location = System::Drawing::Point(275, 4);
            methodGroup->Name = L"methodGroup";
            methodGroup->Size = System::Drawing::Size(265, 52);
            methodGroup->TabIndex = 2;
            methodGroup->TabStop = false;
            methodGroup->Text = L"Method";
            // 
            // methodEditor
            // 
            this->methodEditor->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
            this->methodEditor->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->methodEditor->FormattingEnabled = true;
            this->methodEditor->Location = System::Drawing::Point(7, 20);
            this->methodEditor->Name = L"methodEditor";
            this->methodEditor->Size = System::Drawing::Size(252, 21);
            this->methodEditor->TabIndex = 1;
            // 
            // notesGroup
            // 
            notesGroup->Controls->Add(this->notesEditor);
            notesGroup->Location = System::Drawing::Point(275, 63);
            notesGroup->Name = L"notesGroup";
            notesGroup->Size = System::Drawing::Size(265, 51);
            notesGroup->TabIndex = 3;
            notesGroup->TabStop = false;
            notesGroup->Text = L"Notes";
            // 
            // notesEditor
            // 
            this->notesEditor->Location = System::Drawing::Point(7, 20);
            this->notesEditor->Name = L"notesEditor";
            this->notesEditor->Size = System::Drawing::Size(252, 20);
            this->notesEditor->TabIndex = 1;
            // 
            // composerGroup
            // 
            composerGroup->Controls->Add(this->composerEditor);
            composerGroup->Location = System::Drawing::Point(4, 63);
            composerGroup->Name = L"composerGroup";
            composerGroup->Size = System::Drawing::Size(265, 51);
            composerGroup->TabIndex = 1;
            composerGroup->TabStop = false;
            composerGroup->Text = L"Composer";
            // 
            // composerEditor
            // 
            this->composerEditor->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
            this->composerEditor->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->composerEditor->FormattingEnabled = true;
            this->composerEditor->Location = System::Drawing::Point(7, 20);
            this->composerEditor->Name = L"composerEditor";
            this->composerEditor->Size = System::Drawing::Size(252, 21);
            this->composerEditor->TabIndex = 0;
            // 
            // nameGroup
            // 
            nameGroup->Controls->Add(this->nameEditor);
            nameGroup->Location = System::Drawing::Point(4, 4);
            nameGroup->Name = L"nameGroup";
            nameGroup->Size = System::Drawing::Size(265, 52);
            nameGroup->TabIndex = 0;
            nameGroup->TabStop = false;
            nameGroup->Text = L"Name";
            // 
            // nameEditor
            // 
            this->nameEditor->Location = System::Drawing::Point(7, 20);
            this->nameEditor->Name = L"nameEditor";
            this->nameEditor->Size = System::Drawing::Size(252, 20);
            this->nameEditor->TabIndex = 0;
            // 
            // dataGridView
            // 
            this->dataGridView->AllowUserToAddRows = false;
            this->dataGridView->AllowUserToDeleteRows = false;
            this->dataGridView->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->dataGridView->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(5) {
                this->idColumn,
                    this->nameColumn, this->lengthColumn, this->methodColumn, this->composerColumn
            });
            this->dataGridView->Dock = System::Windows::Forms::DockStyle::Fill;
            this->dataGridView->Location = System::Drawing::Point(3, 121);
            this->dataGridView->Name = L"dataGridView";
            this->dataGridView->ReadOnly = true;
            this->dataGridView->Size = System::Drawing::Size(648, 143);
            this->dataGridView->TabIndex = 2;
            this->dataGridView->CellDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &CompositionSearchUI::dataGridView_CellDoubleClick);
            this->dataGridView->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &CompositionSearchUI::dataGridView_ColumnHeaderMouseClick);
            // 
            // idColumn
            // 
            this->idColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->idColumn->HeaderText = L"ID";
            this->idColumn->Name = L"idColumn";
            this->idColumn->ReadOnly = true;
            this->idColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->idColumn->Visible = false;
            // 
            // nameColumn
            // 
            this->nameColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->nameColumn->HeaderText = L"Name";
            this->nameColumn->Name = L"nameColumn";
            this->nameColumn->ReadOnly = true;
            this->nameColumn->Width = 60;
            // 
            // lengthColumn
            // 
            this->lengthColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->lengthColumn->HeaderText = L"Length";
            this->lengthColumn->Name = L"lengthColumn";
            this->lengthColumn->ReadOnly = true;
            this->lengthColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->lengthColumn->Width = 65;
            // 
            // methodColumn
            // 
            this->methodColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->methodColumn->HeaderText = L"Method";
            this->methodColumn->Name = L"methodColumn";
            this->methodColumn->ReadOnly = true;
            this->methodColumn->Width = 68;
            // 
            // composerColumn
            // 
            this->composerColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->composerColumn->HeaderText = L"Composer";
            this->composerColumn->Name = L"composerColumn";
            this->composerColumn->ReadOnly = true;
            this->composerColumn->Width = 79;
            // 
            // backgroundWorker
            // 
            this->backgroundWorker->WorkerReportsProgress = true;
            this->backgroundWorker->WorkerSupportsCancellation = true;
            this->backgroundWorker->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &CompositionSearchUI::backgroundWorker_DoWork);
            this->backgroundWorker->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &CompositionSearchUI::backgroundWorker_ProgressChanged);
            this->backgroundWorker->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &CompositionSearchUI::backgroundWorker_RunWorkerCompleted);
            // 
            // CompositionSearchUI
            // 
            this->AcceptButton = this->searchBtn;
            this->CancelButton = this->closeBtn;
            this->ClientSize = System::Drawing::Size(654, 294);
            this->Controls->Add(tableLayoutPanel1);
            this->MinimumSize = System::Drawing::Size(670, 329);
            this->Name = L"CompositionSearchUI";
            this->Text = L"Search compositions";
            this->Closing += gcnew System::ComponentModel::CancelEventHandler(this, &CompositionSearchUI::ClosingEvent);
            this->Load += gcnew System::EventHandler(this, &CompositionSearchUI::CompositionSearchUI_Load);
            tableLayoutPanel1->ResumeLayout(false);
            btnPanel->ResumeLayout(false);
            btnPanel->PerformLayout();
            settingsPanel->ResumeLayout(false);
            otherGroup->ResumeLayout(false);
            otherGroup->PerformLayout();
            methodGroup->ResumeLayout(false);
            notesGroup->ResumeLayout(false);
            notesGroup->PerformLayout();
            composerGroup->ResumeLayout(false);
            nameGroup->ResumeLayout(false);
            nameGroup->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->EndInit();
            this->ResumeLayout(false);

        }
#pragma endregion
    };
}
