#include "RingingDatabaseObserver.h"
#include <Composition.h>
#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"
#include "PrintCompositionUtil.h"
#include "DucoWindowState.h"
#include "ProgressWrapper.h"
#include <DatabaseSearch.h>

#include "CompositionSearchUI.h"

#include "CompositionUI.h"
#include "SystemDefaultIcon.h"
#include "DatabaseGenUtils.h"
#include "SoundUtils.h"
#include "DucoTableSorter.h"
#include <DatabaseSettings.h>
#include "DucoUtils.h"
#include <RingingDatabase.h>
#include <DucoConfiguration.h>
#include "DucoUIUtils.h"
#include <Method.h>
#include <Ringer.h>
#include <DucoEngineUtils.h>

#include "SearchUtils.h"
#include <SearchFieldBoolArgument.h>
#include <SearchFieldIdArgument.h>
#include <SearchValidObject.h>

using namespace Duco;
using namespace DucoUI;
using namespace System::Windows::Forms;

CompositionSearchUI::CompositionSearchUI(DucoUI::DatabaseManager^ theDatabase, bool setStartSearchForInvalid)
: database(theDatabase), startSearchForInvalid(setStartSearchForInvalid)
{
    InitializeComponent();
    foundCompositionIds = new std::set<Duco::ObjectId>;
    progressWrapper = new DucoUI::ProgressCallbackWrapper(this);
    allMethodIds = gcnew System::Collections::Generic::List<System::Int16>();
    ringerCache = DucoUI::RingerDisplayCache::CreateDisplayCacheWithConductors(database, true);
}

CompositionSearchUI::!CompositionSearchUI()
{
    delete foundCompositionIds;
    delete progressWrapper;
}

CompositionSearchUI::~CompositionSearchUI()
{
    this->!CompositionSearchUI();
    if (components)
    {
        delete components;
    }
}

System::Void
CompositionSearchUI::CompositionSearchUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    DatabaseGenUtils::GenerateMethodOptions(allMethodIds, methodEditor, database, -1, -1, true, true, true);
    ringerCache->PopulateControl(composerEditor, false);
    if (startSearchForInvalid)
    {
        error->Checked = true;
        searchBtn_Click(sender, e);
    }
}

System::Void
CompositionSearchUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void
CompositionSearchUI::ClosingEvent( Object^ /*sender*/, System::ComponentModel::CancelEventArgs^ e )
{
    if (backgroundWorker->IsBusy)
    {
        backgroundWorker->CancelAsync();
        e->Cancel = true;
    }
}

System::Void
CompositionSearchUI::dataGridView_CellDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    try
    {
        if (!backgroundWorker->IsBusy)
        {
            unsigned int rowIndex = e->RowIndex;
            DataGridViewRow^ currentRow = dataGridView->Rows[rowIndex];
            unsigned int compositionId = System::Convert::ToInt16(currentRow->HeaderCell->Value);
            CompositionUI::ShowComposition(database, this->MdiParent, compositionId);
        }
    }
    catch (System::Exception^)
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
CompositionSearchUI::dataGridView_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
    DataGridViewColumn^ newColumn = dataGridView->Columns[e->ColumnIndex];
    if (newColumn->SortMode != DataGridViewColumnSortMode::Programmatic)
    {
        dataGridView->Columns[2]->HeaderCell->SortGlyphDirection = SortOrder::None;
        return;
    }

    DataGridViewColumn^ oldColumn = dataGridView->SortedColumn;

    SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;
    if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
        newSortOrder = SortOrder::Ascending;
    else
        newSortOrder = SortOrder::Descending;

    if (oldColumn != nullptr && oldColumn != newColumn)
        oldColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
    else
    {
        dataGridView->Columns[2]->HeaderCell->SortGlyphDirection = SortOrder::None;
    }

    newColumn->HeaderCell->SortGlyphDirection = newSortOrder;

    DucoTableSorter^ sorter = gcnew DucoTableSorter(newSortOrder == SortOrder::Ascending, EString);
    sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
    dataGridView->Sort(sorter);

}

System::Void
CompositionSearchUI::clearBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (!backgroundWorker->IsBusy)
    {
        results->Text = "";
        progressBar->Value = 0;
        dataGridView->Rows->Clear();
        searchBtn->Enabled = true;
        snapStart->Checked = false;
        snapFinish->Checked = false;
        error->Checked = false;
        notesEditor->Text = "";
        nameEditor->Text = "";
        methodEditor->Text = "";
        methodEditor->SelectedIndex = -1;
        composerEditor->Text = "";
        composerEditor->SelectedIndex = -1;
    }
}

bool
CompositionSearchUI::CreateSearchParameters(Duco::DatabaseSearch& search)
{
    try
    {
        if (snapFinish->Checked)
        {
            TSearchFieldBoolArgument* newArg = new TSearchFieldBoolArgument(Duco::ECompositionSnapFinish, true);
            search.AddArgument(newArg);
        }
        if (snapStart->Checked)
        {
            TSearchFieldBoolArgument* newArg = new TSearchFieldBoolArgument(Duco::ECompositionSnapStart, true);
            search.AddArgument(newArg);
        }
        SearchUtils::AddStringSearchArgument(search, notesEditor->Text, ENotes, false);
        SearchUtils::AddStringSearchArgument(search, nameEditor->Text, EComposition_Name, false);
        if (methodEditor->SelectedIndex == -1)
        {
            SearchUtils::AddStringSearchArgument(search, methodEditor->Text, EFullMethodName, false);
        }
        else
        {
            Duco::ObjectId methodId = DatabaseGenUtils::FindId(allMethodIds, methodEditor->SelectedIndex);
            TSearchFieldIdArgument* newId = new TSearchFieldIdArgument(Duco::EMethodId, methodId);
            search.AddArgument(newId);
        }
        if (composerEditor->SelectedIndex == -1)
        {
            SearchUtils::AddStringSearchArgument(search, composerEditor->Text, EComposer, false);
        }
        else
        {
            RingerItem^ composer = (RingerItem^)composerEditor->SelectedItem;
            TSearchFieldIdArgument* newId = new TSearchFieldIdArgument(Duco::EComposerId, composer->Id());
            search.AddArgument(newId);
        }
        if (error->Checked)
        {
            TSearchValidObject* newArg = new TSearchValidObject(database->Config().IncludeWarningsInValidation());
            search.AddValidationArgument(newArg);
        }
        if (provenFalse->Checked)
        {
            TSearchFieldBoolArgument* newArg = new TSearchFieldBoolArgument(Duco::ETrueCompositionAndEndsWithRounds, false);
            search.AddArgument(newArg);
        }
    }
    catch (System::Exception^)
    {
        return false;
    }
    return true;
}


System::Void
CompositionSearchUI::searchBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (!backgroundWorker->IsBusy)
    {
        searchBtn->Enabled = false;
        foundCompositionIds->clear();
        nameColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
        lengthColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
        methodColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
        composerColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
        dataGridView->Rows->Clear();
        results->Text = "";
        dataGridView->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::EnableResizing;
        dataGridView->ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode::EnableResizing;

        DatabaseSearch* search = new DatabaseSearch(database->Database());
        if (CreateSearchParameters(*search))
        {
            SearchUIArgs^ args = gcnew SearchUIArgs();
            args->search = search;
            backgroundWorker->RunWorkerAsync(args);
        }
        else
        {
            SoundUtils::PlayErrorSound();
            delete search;
            searchBtn->Enabled = true;
        }
    }
}

System::Void
CompositionSearchUI::backgroundWorker_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
    SearchUIArgs^ args = static_cast<SearchUIArgs^>(e->Argument);
    System::ComponentModel::BackgroundWorker^ bgworker = static_cast<System::ComponentModel::BackgroundWorker^>(sender);
    args->search->Search(*progressWrapper, *foundCompositionIds, TObjectType::EComposition);
    PopulateResults(bgworker);
    delete args->search;
}

System::Void
CompositionSearchUI::backgroundWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    progressBar->Value = e->ProgressPercentage;
    if (e->UserState != nullptr)
    {
        DataGridViewRow^ newRow = static_cast<DataGridViewRow^>(e->UserState);
        dataGridView->Rows->Add(newRow);
    }
}

System::Void
CompositionSearchUI::backgroundWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    dataGridView->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
    dataGridView->ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode::AutoSize;
    searchBtn->Enabled = true;
    progressBar->Value = 0;
    results->Text = System::Convert::ToString(foundCompositionIds->empty());
    if (foundCompositionIds->empty())
        SoundUtils::PlayErrorSound();
}

void
CompositionSearchUI::Initialised()
{
    backgroundWorker->ReportProgress(0);
}

void
CompositionSearchUI::Step(int progressPercent)
{
    backgroundWorker->ReportProgress(progressPercent/2);
}

void
CompositionSearchUI::Complete()
{
}

System::Void
CompositionSearchUI::PopulateResults(System::ComponentModel::BackgroundWorker^ bgworker)
{
//    bool errorFound (false);
    float total = float(foundCompositionIds->size());
    float count (0);
    std::set<Duco::ObjectId>::const_iterator it = foundCompositionIds->begin();
    while (it != foundCompositionIds->end() && !bgworker->CancellationPending)
    {
        Duco::Composition theComposition(-1, database->Database().MethodsDatabase());
        if (database->FindComposition(*it, theComposition, false))
        {
            DataGridViewRow^ newRow = gcnew DataGridViewRow();
            newRow->HeaderCell->Value = DucoUtils::ConvertString(*it);
            DataGridViewCellCollection^ theCells = newRow->Cells;
            //id Column;
            DataGridViewTextBoxCell^ idCell = gcnew DataGridViewTextBoxCell();
            idCell->Value = DucoUtils::ConvertString(*it);
            theCells->Add(idCell);
            //Name Column;
            DataGridViewTextBoxCell^ nameCell = gcnew DataGridViewTextBoxCell();
            nameCell->Value = DucoUtils::ConvertString(theComposition.Name());
            theCells->Add(nameCell);
            //Length Column;
            DataGridViewTextBoxCell^ lengthCell = gcnew DataGridViewTextBoxCell();
            lengthCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(theComposition.NoOfChanges(), true));
            theCells->Add(lengthCell);
            //Method Column;
            DataGridViewTextBoxCell^ methodCell = gcnew DataGridViewTextBoxCell();
            Duco::Method theMethod;
            if (database->FindMethod(theComposition.MethodId(), theMethod, false))
            {
                methodCell->Value = DucoUtils::ConvertString(theMethod.FullName(database->Database()));
            }
            theCells->Add(methodCell);
            //Composer column
            DataGridViewTextBoxCell^ composerCell = gcnew DataGridViewTextBoxCell();
            Duco::Ringer theRinger;
            if (database->FindRinger(theComposition.ComposerId(), theRinger, false))
            {
                composerCell->Value = DucoUtils::ConvertString(theRinger.FullName(database->Database().Settings().LastNameFirst()));
            }
            theCells->Add(composerCell);

            ++count;
            backgroundWorker->ReportProgress(int(((count / total) * 50)+50), newRow);
        }
        ++it;
    }
//    errorColumn->Visible = errorFound;
}
