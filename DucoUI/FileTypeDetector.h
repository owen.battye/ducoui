#pragma once
#include <FileType.h>

namespace DucoUI
{
ref class FileTypeDetector
{
public:
    FileTypeDetector();
    ~FileTypeDetector();
    Duco::TFileType FileType(System::String^ fileName);

protected:
    bool CheckTableExists(System::String^ fileName);

    // member data
    System::Data::Odbc::OdbcConnection^ connection;
};

}