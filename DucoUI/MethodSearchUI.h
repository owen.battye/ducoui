#pragma once

namespace DucoUI
{
    public ref class MethodSearchUI :   public System::Windows::Forms::Form,
                                        public DucoUI::ProgressCallbackUI
    {
    public:
        MethodSearchUI(DucoUI::DatabaseManager^ theDatabase, bool setStartSearchForInvalid);

        // from ProgressCallbackUI
        virtual void Initialised();
        virtual void Step(int progressPercent);
        virtual void Complete();

    protected:
        ~MethodSearchUI();
        !MethodSearchUI();
        System::Void MethodSearchUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void ClosingEvent( Object^ /*sender*/, System::ComponentModel::CancelEventArgs^ e );

        System::Void searchBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void clearBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void copyMethodDataCmd_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void printBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void updateBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void printMethods(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args);

        System::Void BellsEditor_KeyDown( Object^ sender, System::Windows::Forms::KeyEventArgs^ e );
        System::Void BellsEditor_KeyPressed(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e);
        System::Void blankNotation_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void emptyMethodName_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void emptyMethodType_CheckedChanged(System::Object^  sender, System::EventArgs^  e);

        System::Void methodData_CellDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);
        System::Void methodData_CellValueNeeded(System::Object^  sender, System::Windows::Forms::DataGridViewCellValueEventArgs^  e);
        System::Void methodData_Scroll(System::Object^  sender, System::Windows::Forms::ScrollEventArgs^  e);
        System::Void MethodSearchUI_Resize(System::Object^  sender, System::EventArgs^  e);

        System::Void backgroundWorker_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void backgroundWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void backgroundWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);

        bool CreateSearchParameters(Duco::DatabaseSearch& search);

    private:
        DucoUI::DatabaseManager^                database;
        DucoUI::ProgressCallbackWrapper*        progressWrapper;
        DucoUI::ProgressCallbackWrapper*        updateWrapper;
        GenericTablePrintUtil^                  printUtil;
        System::ComponentModel::BackgroundWorker^   backgroundWorker;

        System::Boolean                         nonNumberEntered;
        System::Boolean                         startSearchForInvalid;
        System::Collections::Generic::List<System::UInt64>^         foundMethodIds;
        System::Windows::Forms::ProgressBar^    progressBar;
        System::ComponentModel::IContainer^     components;
        System::Windows::Forms::TextBox^        resultCount;
        System::Windows::Forms::TextBox^        bellsEditor;
        System::Windows::Forms::RadioButton^    bellsMoreThan;
        System::Windows::Forms::RadioButton^    bellsEqualTo;
        System::Windows::Forms::RadioButton^    bellsLessThan;
        System::Windows::Forms::ComboBox^       placeNotationEditor;
        System::Windows::Forms::ComboBox^       typeEditor;
        System::Windows::Forms::ComboBox^       nameEditor;
        System::Windows::Forms::CheckBox^       dualOrder;
        System::Windows::Forms::CheckBox^       invalid;
        System::Windows::Forms::CheckBox^       blankNotation;
        System::Windows::Forms::DataGridView^   methodData;
        System::Windows::Forms::Button^         updateBtn;
        System::Windows::Forms::Button^         printBtn;
        System::Windows::Forms::DataGridViewTextBoxColumn^  NameColumn;
        System::Windows::Forms::DataGridViewTextBoxColumn^  TypeColumn;
        System::Windows::Forms::DataGridViewTextBoxColumn^  BellsColumn;
        System::Windows::Forms::DataGridViewTextBoxColumn^  PlaceNotationColumn;
        System::Windows::Forms::DataGridViewTextBoxColumn^  FirstPealColumn;
        System::Windows::Forms::DataGridViewTextBoxColumn^  errorColumn;
        System::Windows::Forms::CheckBox^       emptyMethodType;
        System::Windows::Forms::CheckBox^       fullMethodName;
        System::Windows::Forms::CheckBox^       emptyMethodName;
        System::Windows::Forms::CheckBox^       splicedMethod;
        System::Windows::Forms::Button^         searchBtn;

        System::Void InitializeComponent()
        {
            this->components = (gcnew System::ComponentModel::Container());
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
            System::Windows::Forms::Panel^ controlsPanel;
            System::Windows::Forms::GroupBox^ otherGroup;
            System::Windows::Forms::GroupBox^ placeNotationGrp;
            System::Windows::Forms::GroupBox^ methodTypeGrp;
            System::Windows::Forms::GroupBox^ methodNameGrp;
            System::Windows::Forms::GroupBox^ bellsGrp;
            System::Windows::Forms::ContextMenuStrip^ methodDataMenu;
            System::Windows::Forms::ToolStripMenuItem^ copyMethodDataCmd;
            System::Windows::Forms::Panel^ buttonPanel;
            System::Windows::Forms::Button^ clearBtn;
            System::Windows::Forms::Label^ resultsLbl;
            System::Windows::Forms::Button^ closeBtn;
            this->dualOrder = (gcnew System::Windows::Forms::CheckBox());
            this->invalid = (gcnew System::Windows::Forms::CheckBox());
            this->blankNotation = (gcnew System::Windows::Forms::CheckBox());
            this->placeNotationEditor = (gcnew System::Windows::Forms::ComboBox());
            this->emptyMethodType = (gcnew System::Windows::Forms::CheckBox());
            this->typeEditor = (gcnew System::Windows::Forms::ComboBox());
            this->fullMethodName = (gcnew System::Windows::Forms::CheckBox());
            this->emptyMethodName = (gcnew System::Windows::Forms::CheckBox());
            this->nameEditor = (gcnew System::Windows::Forms::ComboBox());
            this->bellsEditor = (gcnew System::Windows::Forms::TextBox());
            this->bellsMoreThan = (gcnew System::Windows::Forms::RadioButton());
            this->bellsEqualTo = (gcnew System::Windows::Forms::RadioButton());
            this->bellsLessThan = (gcnew System::Windows::Forms::RadioButton());
            this->methodData = (gcnew System::Windows::Forms::DataGridView());
            this->NameColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->TypeColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->BellsColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->PlaceNotationColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->FirstPealColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->errorColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->updateBtn = (gcnew System::Windows::Forms::Button());
            this->printBtn = (gcnew System::Windows::Forms::Button());
            this->progressBar = (gcnew System::Windows::Forms::ProgressBar());
            this->resultCount = (gcnew System::Windows::Forms::TextBox());
            this->searchBtn = (gcnew System::Windows::Forms::Button());
            this->backgroundWorker = (gcnew System::ComponentModel::BackgroundWorker());
            this->splicedMethod = (gcnew System::Windows::Forms::CheckBox());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            controlsPanel = (gcnew System::Windows::Forms::Panel());
            otherGroup = (gcnew System::Windows::Forms::GroupBox());
            placeNotationGrp = (gcnew System::Windows::Forms::GroupBox());
            methodTypeGrp = (gcnew System::Windows::Forms::GroupBox());
            methodNameGrp = (gcnew System::Windows::Forms::GroupBox());
            bellsGrp = (gcnew System::Windows::Forms::GroupBox());
            methodDataMenu = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
            copyMethodDataCmd = (gcnew System::Windows::Forms::ToolStripMenuItem());
            buttonPanel = (gcnew System::Windows::Forms::Panel());
            clearBtn = (gcnew System::Windows::Forms::Button());
            resultsLbl = (gcnew System::Windows::Forms::Label());
            closeBtn = (gcnew System::Windows::Forms::Button());
            tableLayoutPanel1->SuspendLayout();
            controlsPanel->SuspendLayout();
            otherGroup->SuspendLayout();
            placeNotationGrp->SuspendLayout();
            methodTypeGrp->SuspendLayout();
            methodNameGrp->SuspendLayout();
            bellsGrp->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->methodData))->BeginInit();
            methodDataMenu->SuspendLayout();
            buttonPanel->SuspendLayout();
            this->SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 1;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->Controls->Add(controlsPanel, 0, 0);
            tableLayoutPanel1->Controls->Add(this->methodData, 0, 1);
            tableLayoutPanel1->Controls->Add(buttonPanel, 0, 2);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 3;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 185)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 34)));
            tableLayoutPanel1->Size = System::Drawing::Size(791, 424);
            tableLayoutPanel1->TabIndex = 0;
            // 
            // controlsPanel
            // 
            controlsPanel->Controls->Add(otherGroup);
            controlsPanel->Controls->Add(placeNotationGrp);
            controlsPanel->Controls->Add(methodTypeGrp);
            controlsPanel->Controls->Add(methodNameGrp);
            controlsPanel->Controls->Add(bellsGrp);
            controlsPanel->Dock = System::Windows::Forms::DockStyle::Fill;
            controlsPanel->Location = System::Drawing::Point(3, 3);
            controlsPanel->Name = L"controlsPanel";
            controlsPanel->Size = System::Drawing::Size(785, 179);
            controlsPanel->TabIndex = 0;
            // 
            // otherGroup
            // 
            otherGroup->Controls->Add(this->splicedMethod);
            otherGroup->Controls->Add(this->dualOrder);
            otherGroup->Controls->Add(this->invalid);
            otherGroup->Location = System::Drawing::Point(336, 82);
            otherGroup->Name = L"otherGroup";
            otherGroup->Size = System::Drawing::Size(290, 94);
            otherGroup->TabIndex = 7;
            otherGroup->TabStop = false;
            otherGroup->Text = L"Other";
            // 
            // dualOrder
            // 
            this->dualOrder->AutoSize = true;
            this->dualOrder->Location = System::Drawing::Point(7, 19);
            this->dualOrder->Name = L"dualOrder";
            this->dualOrder->Size = System::Drawing::Size(228, 17);
            this->dualOrder->TabIndex = 5;
            this->dualOrder->Text = L"Spliced stages (e.g. Cinques and Maximus)";
            this->dualOrder->UseVisualStyleBackColor = true;
            // 
            // invalid
            // 
            this->invalid->AutoSize = true;
            this->invalid->Location = System::Drawing::Point(7, 42);
            this->invalid->Name = L"invalid";
            this->invalid->Size = System::Drawing::Size(91, 17);
            this->invalid->TabIndex = 6;
            this->invalid->Text = L"Contains error";
            this->invalid->UseVisualStyleBackColor = true;
            // 
            // placeNotationGrp
            // 
            placeNotationGrp->Controls->Add(this->blankNotation);
            placeNotationGrp->Controls->Add(this->placeNotationEditor);
            placeNotationGrp->Location = System::Drawing::Point(336, 3);
            placeNotationGrp->Name = L"placeNotationGrp";
            placeNotationGrp->Size = System::Drawing::Size(290, 73);
            placeNotationGrp->TabIndex = 4;
            placeNotationGrp->TabStop = false;
            placeNotationGrp->Text = L"Place notation";
            // 
            // blankNotation
            // 
            this->blankNotation->AutoSize = true;
            this->blankNotation->Location = System::Drawing::Point(7, 46);
            this->blankNotation->Name = L"blankNotation";
            this->blankNotation->Size = System::Drawing::Size(123, 17);
            this->blankNotation->TabIndex = 1;
            this->blankNotation->Text = L"Blank place notation";
            this->blankNotation->UseVisualStyleBackColor = true;
            this->blankNotation->CheckedChanged += gcnew System::EventHandler(this, &MethodSearchUI::blankNotation_CheckedChanged);
            // 
            // placeNotationEditor
            // 
            this->placeNotationEditor->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
            this->placeNotationEditor->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->placeNotationEditor->FormattingEnabled = true;
            this->placeNotationEditor->Location = System::Drawing::Point(7, 19);
            this->placeNotationEditor->Name = L"placeNotationEditor";
            this->placeNotationEditor->Size = System::Drawing::Size(277, 21);
            this->placeNotationEditor->TabIndex = 0;
            // 
            // methodTypeGrp
            // 
            methodTypeGrp->Controls->Add(this->emptyMethodType);
            methodTypeGrp->Controls->Add(this->typeEditor);
            methodTypeGrp->Location = System::Drawing::Point(94, 103);
            methodTypeGrp->Name = L"methodTypeGrp";
            methodTypeGrp->Size = System::Drawing::Size(236, 73);
            methodTypeGrp->TabIndex = 3;
            methodTypeGrp->TabStop = false;
            methodTypeGrp->Text = L"Method type";
            // 
            // emptyMethodType
            // 
            this->emptyMethodType->AutoSize = true;
            this->emptyMethodType->Location = System::Drawing::Point(7, 46);
            this->emptyMethodType->Name = L"emptyMethodType";
            this->emptyMethodType->Size = System::Drawing::Size(116, 17);
            this->emptyMethodType->TabIndex = 1;
            this->emptyMethodType->Text = L"Empty method type";
            this->emptyMethodType->UseVisualStyleBackColor = true;
            this->emptyMethodType->CheckedChanged += gcnew System::EventHandler(this, &MethodSearchUI::emptyMethodType_CheckedChanged);
            // 
            // typeEditor
            // 
            this->typeEditor->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
            this->typeEditor->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->typeEditor->FormattingEnabled = true;
            this->typeEditor->Location = System::Drawing::Point(7, 19);
            this->typeEditor->Name = L"typeEditor";
            this->typeEditor->Size = System::Drawing::Size(223, 21);
            this->typeEditor->TabIndex = 0;
            // 
            // methodNameGrp
            // 
            methodNameGrp->Controls->Add(this->fullMethodName);
            methodNameGrp->Controls->Add(this->emptyMethodName);
            methodNameGrp->Controls->Add(this->nameEditor);
            methodNameGrp->Location = System::Drawing::Point(94, 3);
            methodNameGrp->Name = L"methodNameGrp";
            methodNameGrp->Size = System::Drawing::Size(236, 96);
            methodNameGrp->TabIndex = 2;
            methodNameGrp->TabStop = false;
            methodNameGrp->Text = L"Method name";
            // 
            // fullMethodName
            // 
            this->fullMethodName->AutoSize = true;
            this->fullMethodName->Location = System::Drawing::Point(7, 46);
            this->fullMethodName->Name = L"fullMethodName";
            this->fullMethodName->Size = System::Drawing::Size(143, 17);
            this->fullMethodName->TabIndex = 2;
            this->fullMethodName->Text = L"Search full method name";
            this->fullMethodName->UseVisualStyleBackColor = true;
            // 
            // emptyMethodName
            // 
            this->emptyMethodName->AutoSize = true;
            this->emptyMethodName->Location = System::Drawing::Point(6, 73);
            this->emptyMethodName->Name = L"emptyMethodName";
            this->emptyMethodName->Size = System::Drawing::Size(122, 17);
            this->emptyMethodName->TabIndex = 1;
            this->emptyMethodName->Text = L"Empty method name";
            this->emptyMethodName->UseVisualStyleBackColor = true;
            this->emptyMethodName->CheckedChanged += gcnew System::EventHandler(this, &MethodSearchUI::emptyMethodName_CheckedChanged);
            // 
            // nameEditor
            // 
            this->nameEditor->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
            this->nameEditor->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->nameEditor->FormattingEnabled = true;
            this->nameEditor->Location = System::Drawing::Point(7, 19);
            this->nameEditor->Name = L"nameEditor";
            this->nameEditor->Size = System::Drawing::Size(223, 21);
            this->nameEditor->TabIndex = 0;
            // 
            // bellsGrp
            // 
            bellsGrp->Controls->Add(this->bellsEditor);
            bellsGrp->Controls->Add(this->bellsMoreThan);
            bellsGrp->Controls->Add(this->bellsEqualTo);
            bellsGrp->Controls->Add(this->bellsLessThan);
            bellsGrp->Location = System::Drawing::Point(3, 3);
            bellsGrp->Margin = System::Windows::Forms::Padding(1);
            bellsGrp->Name = L"bellsGrp";
            bellsGrp->Size = System::Drawing::Size(88, 123);
            bellsGrp->TabIndex = 1;
            bellsGrp->TabStop = false;
            bellsGrp->Text = L"Bells";
            // 
            // bellsEditor
            // 
            this->bellsEditor->Location = System::Drawing::Point(6, 19);
            this->bellsEditor->Name = L"bellsEditor";
            this->bellsEditor->Size = System::Drawing::Size(44, 20);
            this->bellsEditor->TabIndex = 0;
            this->bellsEditor->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MethodSearchUI::BellsEditor_KeyDown);
            this->bellsEditor->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &MethodSearchUI::BellsEditor_KeyPressed);
            // 
            // bellsMoreThan
            // 
            this->bellsMoreThan->AutoSize = true;
            this->bellsMoreThan->Location = System::Drawing::Point(6, 91);
            this->bellsMoreThan->Name = L"bellsMoreThan";
            this->bellsMoreThan->Size = System::Drawing::Size(73, 17);
            this->bellsMoreThan->TabIndex = 3;
            this->bellsMoreThan->Text = L"More than";
            this->bellsMoreThan->UseVisualStyleBackColor = true;
            // 
            // bellsEqualTo
            // 
            this->bellsEqualTo->AutoSize = true;
            this->bellsEqualTo->Checked = true;
            this->bellsEqualTo->Location = System::Drawing::Point(6, 68);
            this->bellsEqualTo->Name = L"bellsEqualTo";
            this->bellsEqualTo->Size = System::Drawing::Size(64, 17);
            this->bellsEqualTo->TabIndex = 2;
            this->bellsEqualTo->TabStop = true;
            this->bellsEqualTo->Text = L"Equal to";
            this->bellsEqualTo->UseVisualStyleBackColor = true;
            // 
            // bellsLessThan
            // 
            this->bellsLessThan->AutoSize = true;
            this->bellsLessThan->Location = System::Drawing::Point(6, 45);
            this->bellsLessThan->Name = L"bellsLessThan";
            this->bellsLessThan->Size = System::Drawing::Size(71, 17);
            this->bellsLessThan->TabIndex = 1;
            this->bellsLessThan->Text = L"Less than";
            this->bellsLessThan->UseVisualStyleBackColor = true;
            // 
            // methodData
            // 
            this->methodData->AllowUserToAddRows = false;
            this->methodData->AllowUserToDeleteRows = false;
            this->methodData->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
            this->methodData->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(6) {
                this->NameColumn,
                    this->TypeColumn, this->BellsColumn, this->PlaceNotationColumn, this->FirstPealColumn, this->errorColumn
            });
            this->methodData->ContextMenuStrip = methodDataMenu;
            this->methodData->Dock = System::Windows::Forms::DockStyle::Fill;
            this->methodData->Location = System::Drawing::Point(3, 188);
            this->methodData->Name = L"methodData";
            this->methodData->ReadOnly = true;
            this->methodData->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToDisplayedHeaders;
            this->methodData->Size = System::Drawing::Size(785, 199);
            this->methodData->TabIndex = 6;
            this->methodData->VirtualMode = true;
            this->methodData->CellContentDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &MethodSearchUI::methodData_CellDoubleClick);
            this->methodData->CellValueNeeded += gcnew System::Windows::Forms::DataGridViewCellValueEventHandler(this, &MethodSearchUI::methodData_CellValueNeeded);
            this->methodData->Scroll += gcnew System::Windows::Forms::ScrollEventHandler(this, &MethodSearchUI::methodData_Scroll);
            // 
            // NameColumn
            // 
            this->NameColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            this->NameColumn->HeaderText = L"Name";
            this->NameColumn->Name = L"NameColumn";
            this->NameColumn->ReadOnly = true;
            this->NameColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->NameColumn->Width = 41;
            // 
            // TypeColumn
            // 
            this->TypeColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            this->TypeColumn->HeaderText = L"Type";
            this->TypeColumn->Name = L"TypeColumn";
            this->TypeColumn->ReadOnly = true;
            this->TypeColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->TypeColumn->Width = 37;
            // 
            // BellsColumn
            // 
            this->BellsColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            this->BellsColumn->HeaderText = L"Bells";
            this->BellsColumn->Name = L"BellsColumn";
            this->BellsColumn->ReadOnly = true;
            this->BellsColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->BellsColumn->Width = 35;
            // 
            // PlaceNotationColumn
            // 
            this->PlaceNotationColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            this->PlaceNotationColumn->HeaderText = L"Place Notation";
            this->PlaceNotationColumn->Name = L"PlaceNotationColumn";
            this->PlaceNotationColumn->ReadOnly = true;
            this->PlaceNotationColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->PlaceNotationColumn->Width = 83;
            // 
            // FirstPealColumn
            // 
            this->FirstPealColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            this->FirstPealColumn->HeaderText = L"First Peal";
            this->FirstPealColumn->MinimumWidth = 25;
            this->FirstPealColumn->Name = L"FirstPealColumn";
            this->FirstPealColumn->ReadOnly = true;
            this->FirstPealColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->FirstPealColumn->Width = 56;
            // 
            // errorColumn
            // 
            this->errorColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            this->errorColumn->HeaderText = L"Error";
            this->errorColumn->MinimumWidth = 25;
            this->errorColumn->Name = L"errorColumn";
            this->errorColumn->ReadOnly = true;
            this->errorColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->errorColumn->Width = 35;
            // 
            // methodDataMenu
            // 
            methodDataMenu->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { copyMethodDataCmd });
            methodDataMenu->Name = L"methodDataMenu";
            methodDataMenu->Size = System::Drawing::Size(103, 26);
            // 
            // copyMethodDataCmd
            // 
            copyMethodDataCmd->Name = L"copyMethodDataCmd";
            copyMethodDataCmd->Size = System::Drawing::Size(102, 22);
            copyMethodDataCmd->Text = L"Copy";
            copyMethodDataCmd->Click += gcnew System::EventHandler(this, &MethodSearchUI::copyMethodDataCmd_Click);
            // 
            // buttonPanel
            // 
            buttonPanel->Controls->Add(this->updateBtn);
            buttonPanel->Controls->Add(this->printBtn);
            buttonPanel->Controls->Add(clearBtn);
            buttonPanel->Controls->Add(this->progressBar);
            buttonPanel->Controls->Add(this->resultCount);
            buttonPanel->Controls->Add(resultsLbl);
            buttonPanel->Controls->Add(this->searchBtn);
            buttonPanel->Controls->Add(closeBtn);
            buttonPanel->Dock = System::Windows::Forms::DockStyle::Fill;
            buttonPanel->Location = System::Drawing::Point(3, 393);
            buttonPanel->Name = L"buttonPanel";
            buttonPanel->Size = System::Drawing::Size(785, 28);
            buttonPanel->TabIndex = 7;
            // 
            // updateBtn
            // 
            this->updateBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->updateBtn->Location = System::Drawing::Point(395, 3);
            this->updateBtn->Name = L"updateBtn";
            this->updateBtn->Size = System::Drawing::Size(75, 23);
            this->updateBtn->TabIndex = 2;
            this->updateBtn->Text = L"Update";
            this->updateBtn->UseVisualStyleBackColor = true;
            this->updateBtn->Click += gcnew System::EventHandler(this, &MethodSearchUI::updateBtn_Click);
            // 
            // printBtn
            // 
            this->printBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->printBtn->Location = System::Drawing::Point(470, 3);
            this->printBtn->Name = L"printBtn";
            this->printBtn->Size = System::Drawing::Size(75, 23);
            this->printBtn->TabIndex = 3;
            this->printBtn->Text = L"Print";
            this->printBtn->UseVisualStyleBackColor = true;
            this->printBtn->Click += gcnew System::EventHandler(this, &MethodSearchUI::printBtn_Click);
            // 
            // clearBtn
            // 
            clearBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            clearBtn->Location = System::Drawing::Point(551, 3);
            clearBtn->Margin = System::Windows::Forms::Padding(3, 3, 0, 3);
            clearBtn->Name = L"clearBtn";
            clearBtn->Size = System::Drawing::Size(75, 23);
            clearBtn->TabIndex = 4;
            clearBtn->Text = L"Clear";
            clearBtn->UseVisualStyleBackColor = true;
            clearBtn->Click += gcnew System::EventHandler(this, &MethodSearchUI::clearBtn_Click);
            // 
            // progressBar
            // 
            this->progressBar->Location = System::Drawing::Point(3, 5);
            this->progressBar->Name = L"progressBar";
            this->progressBar->Size = System::Drawing::Size(88, 18);
            this->progressBar->Step = 1;
            this->progressBar->TabIndex = 0;
            // 
            // resultCount
            // 
            this->resultCount->Location = System::Drawing::Point(145, 5);
            this->resultCount->Name = L"resultCount";
            this->resultCount->ReadOnly = true;
            this->resultCount->Size = System::Drawing::Size(43, 20);
            this->resultCount->TabIndex = 1;
            // 
            // resultsLbl
            // 
            resultsLbl->AutoSize = true;
            resultsLbl->Location = System::Drawing::Point(97, 8);
            resultsLbl->Name = L"resultsLbl";
            resultsLbl->Size = System::Drawing::Size(42, 13);
            resultsLbl->TabIndex = 0;
            resultsLbl->Text = L"Results";
            // 
            // searchBtn
            // 
            this->searchBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->searchBtn->Location = System::Drawing::Point(626, 3);
            this->searchBtn->Margin = System::Windows::Forms::Padding(0, 3, 3, 3);
            this->searchBtn->Name = L"searchBtn";
            this->searchBtn->Size = System::Drawing::Size(75, 23);
            this->searchBtn->TabIndex = 5;
            this->searchBtn->Text = L"Search";
            this->searchBtn->UseVisualStyleBackColor = true;
            this->searchBtn->Click += gcnew System::EventHandler(this, &MethodSearchUI::searchBtn_Click);
            // 
            // closeBtn
            // 
            closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            closeBtn->Location = System::Drawing::Point(707, 3);
            closeBtn->Name = L"closeBtn";
            closeBtn->Size = System::Drawing::Size(75, 23);
            closeBtn->TabIndex = 6;
            closeBtn->Text = L"Close";
            closeBtn->UseVisualStyleBackColor = true;
            closeBtn->Click += gcnew System::EventHandler(this, &MethodSearchUI::closeBtn_Click);
            // 
            // backgroundWorker
            // 
            this->backgroundWorker->WorkerReportsProgress = true;
            this->backgroundWorker->WorkerSupportsCancellation = true;
            this->backgroundWorker->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &MethodSearchUI::backgroundWorker_DoWork);
            this->backgroundWorker->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &MethodSearchUI::backgroundWorker_ProgressChanged);
            this->backgroundWorker->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &MethodSearchUI::backgroundWorker_RunWorkerCompleted);
            // 
            // splicedMethod
            // 
            this->splicedMethod->AutoSize = true;
            this->splicedMethod->Location = System::Drawing::Point(7, 65);
            this->splicedMethod->Name = L"splicedMethod";
            this->splicedMethod->Size = System::Drawing::Size(118, 17);
            this->splicedMethod->TabIndex = 7;
            this->splicedMethod->Text = L"Any spliced method";
            this->splicedMethod->UseVisualStyleBackColor = true;
            // 
            // MethodSearchUI
            // 
            this->AcceptButton = this->searchBtn;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->CancelButton = closeBtn;
            this->ClientSize = System::Drawing::Size(791, 424);
            this->Controls->Add(tableLayoutPanel1);
            this->MinimumSize = System::Drawing::Size(624, 293);
            this->Name = L"MethodSearchUI";
            this->Text = L"Search methods";
            this->Closing += gcnew System::ComponentModel::CancelEventHandler(this, &MethodSearchUI::ClosingEvent);
            this->Load += gcnew System::EventHandler(this, &MethodSearchUI::MethodSearchUI_Load);
            this->Resize += gcnew System::EventHandler(this, &MethodSearchUI::MethodSearchUI_Resize);
            tableLayoutPanel1->ResumeLayout(false);
            controlsPanel->ResumeLayout(false);
            otherGroup->ResumeLayout(false);
            otherGroup->PerformLayout();
            placeNotationGrp->ResumeLayout(false);
            placeNotationGrp->PerformLayout();
            methodTypeGrp->ResumeLayout(false);
            methodTypeGrp->PerformLayout();
            methodNameGrp->ResumeLayout(false);
            methodNameGrp->PerformLayout();
            bellsGrp->ResumeLayout(false);
            bellsGrp->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->methodData))->EndInit();
            methodDataMenu->ResumeLayout(false);
            buttonPanel->ResumeLayout(false);
            buttonPanel->PerformLayout();
            this->ResumeLayout(false);

        }
    };
}
