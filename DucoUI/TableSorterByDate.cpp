#include "TableSorterByDate.h"
#include "DucoUtils.h"

using namespace DucoUI;
using namespace System::Windows::Forms;
using namespace System;
using namespace std;

TableSorterByDate::TableSorterByDate(bool newAscending, int newColumnId)
:   ascending(newAscending), columnId(newColumnId)
{
}

TableSorterByDate::~TableSorterByDate()
{
}

int
TableSorterByDate::Compare(System::Object^ firstObject, System::Object^ secondObject)
{
    DataGridViewRow^ row1 = static_cast<DataGridViewRow^>(firstObject);
    DataGridViewRow^ row2 = static_cast<DataGridViewRow^>(secondObject);

    if (!ascending)
        return Compare(row1, row2);

    return Compare(row2, row1);
}


int
TableSorterByDate::Compare(DataGridViewRow^ firstRow, DataGridViewRow^ secondRow)
{
    String^ row1Value = Convert::ToString(firstRow->Cells[columnId]->Value);
    String^ row2Value = Convert::ToString(secondRow->Cells[columnId]->Value);

    if (row2Value != row1Value)
    {
        int row1Day (0);
        int row1Month (0);
        int row1Year (0);
        int row2Day (0);
        int row2Month (0);
        int row2Year (0);
        ConvertStringToDate(row1Value, row1Year, row1Month, row1Day);
        ConvertStringToDate(row2Value, row2Year, row2Month, row2Day);
        if (row2Year != row1Year)
            return row2Year - row1Year;
        else if (row2Month != row1Month)
            return row2Month - row1Month;
        else if (row2Day != row1Day)
            return row2Day - row1Day;
    }
    return 0;
}

void
TableSorterByDate::ConvertStringToDate(System::String^ dateString, int& year, int& month, int& day)
{
    year = 9999;
    month = 12;
    day = 31;
    std::string dateStr = "";
    DucoUtils::ConvertString (dateString->ToLower()->Trim(), dateStr);

    size_t seperator = dateStr.find_first_not_of("0123456789");
    if (seperator == -1)
        return;

    std::string dayStr = dateStr.substr(0, seperator);
    day = atoi(dayStr.c_str());
    size_t monthStart = dateStr.find_first_of("0123456789abcdefghijlkmnopqrstuvwxyz", seperator);
    if (monthStart == -1)
        return;
    size_t monthEnd = dateStr.find_first_not_of("0123456789abcdefghijlkmnopqrstuvwxyz", monthStart);
    if (monthEnd == -1)
        return;
    std::string monthStr = dateStr.substr(monthStart, monthEnd - monthStart);
    month = atoi(monthStr.c_str());
    if (month == 0)
    {
        month = ConvertStrToMonth(monthStr);
    }
    string::size_type yearStart = dateStr.find_first_of("0123456789", monthEnd+1);
    if (yearStart == string::npos)
        return;
    std::string yearStr = dateStr.substr(yearStart);
    year = atoi(yearStr.c_str());
}

int
TableSorterByDate::ConvertStrToMonth(const std::string& monthStr)
{
    if (monthStr.size() < 3)
        return 1;

    switch (monthStr[0])
    {
    case 'a':
        {
            switch (monthStr[1])
            {
            case 'p':
                return 4; // Apr
            case 'u':
                return 8; //Aug
            }
        }
        break;
    case 'd':
        return 12; // Dec
    case 'f':
        return 2; // Feb
    case 'j':
        {
        switch (monthStr[1])
            {
            case 'a': // Jan
                return 1;
            case 'u':
                {
                switch (monthStr[2])
                    {
                    case 'n':
                        return 6; //Jun
                    case 'l':
                        return 7; // Jul
                    default:
                        break;
                    }
                }
            }
        }
    case 'm':
        {
            if (monthStr[1] == 'a')
            {
                switch (monthStr[2])
                {
                case 'r':
                    return 3; //Mar
                case 'y':
                    return 5; // May
                default:
                    break;
                }
            }
        }
    case 'n':
        return 11; //Nov
    case 'o':
        return 10; // Oct
    case 's':
        return 9; //sep
    default:
        break;
    }
    return 1;
}