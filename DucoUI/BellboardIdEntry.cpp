#include "BellboardParserWrapper.h"
#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"
#include <BellboardPerformanceParser.h>
#include "DucoWindowState.h"

#include "BellboardIdEntry.h"

#include "RingerList.h"
#include "OpenObjectCallback.h"
#include "PictureListControl.h"
#include "PictureControl.h"
#include "SystemDefaultIcon.h"
#include "PrintBandSummaryUtil.h"

#include <Peal.h>
#include "DucoUtils.h"
#include "SoundUtils.h"
#include "PealUI.h"
#include <RingingDatabase.h>
#include <PealDatabase.h>
#include <DatabaseSettings.h>
#include "DucoUILog.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::Threading::Tasks;
using namespace System::IO;
using namespace System::Windows::Forms;


BellboardIdEntry::BellboardIdEntry(DucoUI::DatabaseManager^ theDatabase)
    :database (theDatabase)
{
    InitializeComponent();
    callbackWrapper = new BellboardParserWrapper(this);
    lastDownloadedPealId = new Duco::ObjectId();
    downloadHandler = gcnew System::Net::Http::HttpClient();
    parser = new BellboardPerformanceParser(database->Database(), *callbackWrapper, NULL);
}

BellboardIdEntry::~BellboardIdEntry()
{
    delete lastDownloadedPealId;
    delete callbackWrapper;
    delete parser;
    if (components)
    {
        delete components;
    }
}

System::Void
BellboardIdEntry::bellboardIdEditor_TextChanged(System::Object^ sender, System::EventArgs^ e)
{
    if (bellboardIdEditor->Text->Length > 0)
    {
        downloadBtn->Enabled = true;
    }
    else
    {
        downloadBtn->Enabled = false;
    }
}

System::Void
BellboardIdEntry::bellboardIdEditor_Validated(System::Object^ sender, System::EventArgs^ e)
{
    successLbl->Text = "Click download to download the peal";
    errorProvider->SetError(bellboardIdEditor, "");
    downloadBtn->Enabled = true;
}
System::Void
BellboardIdEntry::bellboardIdEditor_Validating(System::Object^ sender, System::ComponentModel::CancelEventArgs^ e)
{
    String^ validChars = "1234567890";
    Int32 firstInvalidChar = -1;
    for (Int32 count = 0; count < bellboardIdEditor->Text->Length; ++count)
    {
        wchar_t temp = bellboardIdEditor->Text[count];
        if (validChars->IndexOf(temp) == -1)
        {
            firstInvalidChar = count;
            count = bellboardIdEditor->Text->Length;
        }
    }
    
    if (firstInvalidChar != -1)
    {
        // Cancel the event and select the text to be corrected by the user.
        e->Cancel = true;
        bellboardIdEditor->Select(firstInvalidChar, bellboardIdEditor->Text->Length);

        // Set the ErrorProvider error with the text to display.
        this->errorProvider->SetError(bellboardIdEditor, "Invalid Bellboard ID");
        downloadBtn->Enabled = false;
    }
}

System::Void
BellboardIdEntry::downloadBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    int number1;
    if (!int::TryParse(bellboardIdEditor->Text, number1))
    {
        MessageBox::Show("Bellboard ID must be a number");
        return;
    }

    std::wstring bellboardIdStr;
    DucoUtils::ConvertString(bellboardIdEditor->Text, bellboardIdStr);

    Duco::ObjectId existingId = database->Database().PealsDatabase().FindPealByBellboardId(bellboardIdStr);
    if (existingId.ValidId())
    {
        MessageBox::Show(this, "Peformance with that Bellboard id already exists in " + DucoUtils::ConvertString(existingId), "Performance already exists", MessageBoxButtons::OK);
    }
    else
    {
        parser->DownloadPerformance(bellboardIdStr);
    }
}

System::Void
BellboardIdEntry::closeBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    Close();
}

System::Void
BellboardIdEntry::BellboardIdEntry_Load(System::Object^ sender, System::EventArgs^ e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    successLbl->Text = "Enter valid bellboard id";
}

System::Void
BellboardIdEntry::Cancelled(System::String^ bellboardId, System::String^ errorMessage)
{
    SoundUtils::PlayErrorSound();
    DucoUILog::PrintError(errorMessage);
    if (bellboardId->Length <= 0)
    {
        successLbl->Text = "Performance download failed: " + bellboardId;
    }
    downloadBtn->Enabled = false;
    openPealBtn->Enabled = false;
}

System::Void
BellboardIdEntry::Completed(System::String^ bellboardId, System::Boolean errors)
{
    bool downloadedSuccess = false;
    if (bellboardId->Length > 0)
    {
        lastDownloadedPealId->ClearId();
        bool showingPealUI = false;
        Duco::ObjectId similarId = database->FindDuplicateObject(static_cast<const Duco::RingingObject&>(parser->CurrentPerformance()), TObjectType::EPeal);
        if (similarId.ValidId())
        {
            System::String^ existingBellBoardId = database->BellboardId(similarId);
            if (String::Compare(existingBellBoardId, bellboardId) != 0)
            {
                std::wstring newPealSummaryTitleStr;
                parser->CurrentPerformance().SummaryTitle(newPealSummaryTitleStr, database->Database());

                System::String^ newPealSummaryTitle = DucoUtils::ConvertString(newPealSummaryTitleStr);
                System::String^ existingPealSummaryTitle = database->SummaryTitle(similarId);
                String^ confirmationDialogText = "This peal might already exist in the database:\n" + newPealSummaryTitle + "\nas\n" + existingPealSummaryTitle + ".\n";
                String^ confirmationDialogTitle = "Add bellboard id to existing peal?";
                if (existingBellBoardId->Length > 0)
                {
                    confirmationDialogTitle = "Update peal's bellboard id?";
                    confirmationDialogText += "It already has a bell board id (" + existingBellBoardId + ") that doesnt match bellboard (" + bellboardId + ")\n";
                }
                confirmationDialogText += "Do you want to update the existing peal with this bellboard id? ";

                switch (MessageBox::Show(this, confirmationDialogText, confirmationDialogTitle, MessageBoxButtons::YesNoCancel, MessageBoxIcon::Question))
                {
                case System::Windows::Forms::DialogResult::Cancel:
                    downloadedSuccess = false;
                    break;
                case System::Windows::Forms::DialogResult::Yes:
                    if (database->AddWebReferencesToPeal(similarId, bellboardId, parser->CurrentPerformance().RingingWorldReference()))
                    {
                        downloadedSuccess = true;
                    }
                    else
                    {
                        SoundUtils::PlayErrorSound();
                    }
                    break;
                case System::Windows::Forms::DialogResult::No:
                    downloadedSuccess = false;
                    break;
                }
            }
            else if (existingBellBoardId->Length > 0)
            {
                MessageBox::Show(this, "A similar performance with a matching Bellboard ID has been found", "Matching performance found", MessageBoxButtons::OK, MessageBoxIcon::Information);
                SoundUtils::PlayFinishedSound();
            }
        }
        else if (errors)
        {
            PealUI^ displayPealUI = PealUI::PreparePealUI(database, MdiParent, parser->CurrentPerformance());
            displayPealUI->Show();
            showingPealUI = true;
            if (!parser->CurrentPerformance().AssociationId().ValidId())
            {
                displayPealUI->SetMissingAssociationFromDownload(DucoUtils::ConvertString(parser->CurrentPerformance().MissingAssociation()));
            }
            //Missing Tower
            if (parser->CurrentPerformance().MissingRingOnly())
            {
                displayPealUI->SetMissingRingFromDownload(DucoUtils::ConvertString(parser->CurrentPerformance().MissingRing(database->Database())));
            }
            else if (!parser->CurrentPerformance().TowerId().ValidId())
            {
                displayPealUI->SetMissingTowerFromDownload(DucoUtils::ConvertString(parser->CurrentPerformance().MissingTower()),
                    DucoUtils::ConvertString(parser->CurrentPerformance().MissingTenorWeight()),
                    DucoUtils::ConvertString(parser->CurrentPerformance().MissingTenorKey()),
                    DucoUtils::ConvertString(parser->CurrentPerformance().MissingTowerbaseId()),
                    DucoUtils::ConvertString(parser->CurrentPerformance().MissingDoveId()));
            }
            //Missing method
            if (!parser->CurrentPerformance().MethodId().ValidId())
            {
                displayPealUI->SetMissingMethodFromDownload(DucoUtils::ConvertString(parser->CurrentPerformance().MissingMethod()));
            }

            for (unsigned int bellNumber = 0; bellNumber <= parser->CurrentPerformance().NoOfBellsRung(database->Database()); ++bellNumber)
            {
                if (parser->CurrentPerformance().IsMissingRinger(bellNumber, false))
                {
                    displayPealUI->SetMissingRingerFromDownload(bellNumber, DucoUtils::ConvertString(parser->CurrentPerformance().MissingRinger(bellNumber, false, database->Database().Settings())), false, parser->CurrentPerformance().IsMissingRingerConductor(bellNumber, false));
                }
                if (parser->CurrentPerformance().IsMissingRinger(bellNumber, true))
                {
                    displayPealUI->SetMissingRingerFromDownload(bellNumber, DucoUtils::ConvertString(parser->CurrentPerformance().MissingRinger(bellNumber, false, database->Database().Settings())), true, parser->CurrentPerformance().IsMissingRingerConductor(bellNumber, false));
                }
            }
        }
        else
        {
            Peal* newPeal = new Peal(parser->CurrentPerformance());
            Duco::ObjectId newPealId = database->AddPeal(*newPeal);
            if (newPealId.ValidId())
            {
                downloadedSuccess = true;
                *lastDownloadedPealId = newPealId;
            }
        }

        if (parser->ImportedTower())
        {
            database->CallObservers(DucoUI::TEventType::EAdded, TObjectType::ETower, KNoId, KNoId);
        }
        if (parser->ImportedMethod())
        {
            database->CallObservers(DucoUI::TEventType::EAdded, TObjectType::EMethod, KNoId, KNoId);
        }
    }
    
    if (downloadedSuccess)
    {
        bellboardIdEditor->Text = "";
        downloadBtn->Enabled = lastDownloadedPealId->ValidId();
        openPealBtn->Enabled = true;
    }
    else
    {
        lastDownloadedPealId->ClearId();
        downloadBtn->Enabled = false;
        openPealBtn->Enabled = false;
        if (bellboardId->Length <= 0)
        {
            successLbl->Text = "Peal not found on Bellboard - please try a new ID.";
        }
        else
        {
            successLbl->Text = "Peal download had errors or warnings, check the results in the peal window which should open.";
        }
    }
}

System::Void
BellboardIdEntry::openPealBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (lastDownloadedPealId->ValidId())
    {
        PealUI::ShowPeal(database, MdiParent, *lastDownloadedPealId);
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

