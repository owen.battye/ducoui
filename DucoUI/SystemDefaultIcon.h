#pragma once

namespace DucoUI
{
    ref class WindowsSettings;

    ref class SystemDefaultIcon
    {
    public:
        static System::Drawing::Icon^ DefaultSystemIcon();

        static System::Drawing::Icon^ DefaultSystemIcon(DucoUI::WindowsSettings^ settings);

    protected:
        static System::Drawing::Icon^   defaultIcon = nullptr;
    };

}
