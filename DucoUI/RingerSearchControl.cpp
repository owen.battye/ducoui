#include "RingerSearchControl.h"
#include "DucoUtils.h"
#include "DatabaseManager.h"
#include <DatabaseSearch.h>

using namespace DucoUI;

RingerSearchControl::RingerSearchControl(Duco::TSearchFieldRingerArgument* argmnt, DucoUI::DatabaseManager^ theDatabase)
    : argument(argmnt)
{
    InitializeComponent();
    if (argument->FieldType() == Duco::TFieldType::EStringField)
    {
        ringerString->Visible = false;
        ringerName->Text = DucoUtils::ConvertString(argument->RingerName());
    }
    else
    {
        ringerName->Visible = false;
        ringerString->Text = theDatabase->RingerName(argument->RingerId(), Duco::PerformanceDate());
    }
    bell->Text = DucoUtils::ConvertString(argument->Position());
}

RingerSearchControl::!RingerSearchControl()
{
    delete argument;
    argument = NULL;
}

RingerSearchControl::~RingerSearchControl()
{
    this->!RingerSearchControl();
	if (components)
	{
		delete components;
	}
}

void
RingerSearchControl::AddSearchControl(Duco::DatabaseSearch& search)
{
    Duco::TSearchFieldRingerArgument* argcopy = new Duco::TSearchFieldRingerArgument(*argument);
    search.AddArgument(argcopy);
}

bool
RingerSearchControl::ShouldBeRemoved()
{
    return remove->Checked;
}