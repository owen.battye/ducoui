#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"

#include "RingerObjectBase.h"

#include "DatabaseGenUtils.h"
#include <Peal.h>
#include "DucoCommon.h"
#include "RingerList.h"
#include "DucoUtils.h"
#include <DucoConfiguration.h>
#include "DucoUILog.h"
using namespace std::chrono;

using namespace System;
using namespace System::ComponentModel;
using namespace System::Drawing;

using namespace Duco;
using namespace DucoUI;

#define strapperLblString L"Strapper"

RingerObjectBase::RingerObjectBase(DucoUI::RingerList^ theParent, unsigned int theBellNumber, bool isAStrapper)
:   System::Windows::Forms::UserControl(), parent(theParent), oldItem(nullptr), oldString(nullptr),
    bellNumber(theBellNumber), isStrapper(isAStrapper)
{
    InitializeComponent();
}

RingerObjectBase::~RingerObjectBase()
{
    if (components)
    {
        delete components;
    }
}

System::Void
RingerObjectBase::SetState(DucoWindowState newState)
{
    state = newState;

    switch (state)
    {
    case DucoWindowState::EEditMode:
    case DucoWindowState::ENewMode:
        {
        ringerLbl->Enabled = true;
        ringerEditor->Enabled = true;
        conductor->Enabled = true;
        }
        break;

    default:
        {
        ringerLbl->Enabled = false;
        ringerEditor->Enabled = false;
        conductor->Enabled = false;
        }
        break;
    }
}

System::Void
RingerObjectBase::SetNameAndConductor(System::String^ ringerName, bool conducted, bool strapper, int index)
{
    if (index != -1)
    {
        ringerEditor->Text = "";
        SetIndex(strapper, index);
    }
    else
    {
        SetIndex(strapper, -1);
        ringerEditor->Text = ringerName;
        ringerEditor->BackColor = KErrorColour;
    }
    conductor->Checked = conducted;
}

System::Void
RingerObjectBase::CheckConductor(bool isConductor, bool silentAndNonConducted, bool strapperIsConductor)
{
    conductor->Checked = isConductor;
    conductor->Visible = !silentAndNonConducted;
}

System::Void
RingerObjectBase::SetStrapper()
{
    ringerLbl->Text = strapperLblString;
}

System::Void
RingerObjectBase::BeginUpdate(System::Boolean saveOldValues)
{
    ringerEditor->BeginUpdate();
    if (saveOldValues)
    {
        if (ringerEditor->SelectedIndex != -1)
        {
            oldItem = (DucoUI::RingerItem^)ringerEditor->SelectedItem;
            oldString = nullptr;
        }
        else if (ringerEditor->Text->Length > 0)
        {
            oldString = ringerEditor->Text;
            oldItem = nullptr;
        }
    }
    ringerEditor->DataSource = nullptr;
}

System::Void
RingerObjectBase::EndUpdate(DucoUI::RingerDisplayCache^ cache)
{
    time_point<high_resolution_clock> start = high_resolution_clock::now();
    cache->PopulateControl((System::Windows::Forms::ListControl^)ringerEditor, false);
    if (oldItem != nullptr)
    {
        ringerEditor->SelectedIndex = cache->FindIndexOrAdd(oldItem->Id());
        oldItem = nullptr;
    }
    else if (oldString != nullptr)
    {
        ringerEditor->SelectedIndex = -1;
        ringerEditor->Text = oldString;
        oldString = nullptr;
    }
    ringerEditor->EndUpdate();
    DUCOUILOGDEBUGWITHTIME(start, "RingerObjectBase::EndUpdate took");
}

bool
RingerObjectBase::SetRinger(Duco::Peal& newPeal, int index, bool ringerIsConductor, int, bool, unsigned int noOfBellsInPeal)
{
    time_point<high_resolution_clock> start = high_resolution_clock::now();
    String^ bellNoString = "";
    if (isStrapper)
    {
        bellNoString = strapperLblString;
    }
    else if (bellNumber == 1 && !newPeal.Handbell())
    {
        bellNoString = "Treble";
    }
    else if (bellNumber == noOfBellsInPeal && !newPeal.Handbell())
    {
        bellNoString = "Tenor";
    }
    else 
    {
        bellNoString = Convert::ToString(bellNumber);
        if (newPeal.Handbell())
        {
            bellNoString = Convert::ToString((bellNumber*2) -1);
            bellNoString += "-";
            bellNoString += Convert::ToString(bellNumber*2);
        }
    }
    ringerLbl->Text = bellNoString;

    bool processedOk (false);
    if (state == DucoWindowState::EViewMode && index != -1)
    {
        SetIndex(false, index);
        conductor->Checked = ringerIsConductor;
        processedOk = true;
    }
    else if (state == DucoWindowState::EViewMode)
    {
        // No ringer, but it view mode, so clear the ringer;
        ringerEditor->Text = "";
        SetIndex(false, -1);
    }
    else if (ringerEditor->SelectedIndex > 0)
    { // edit or new mode, created new ringer control - ie ring changed.
        parent->RingerChanged((RingerItem^)ringerEditor->SelectedItem, conductor->Checked, bellNumber, isStrapper);
        processedOk = true;
    }
    else if (state == DucoWindowState::ENewMode || state == DucoWindowState::EAutoNewMode)
    {
        SetIndex(false, index);
        conductor->Checked = ringerIsConductor;
        processedOk = true;
    }
    DUCOUILOGDEBUGWITHTIME(start, "RingerObjectBase::SetRinger took");
    return processedOk;
}

System::Boolean
RingerObjectBase::KnownRinger()
{
    if (ringerEditor->SelectedItem != nullptr)
    {
        RingerItem^ ringer = (RingerItem^)ringerEditor->SelectedItem;
        if (ObjectId(ringer->Id()).ValidId())
        {
            return true;
        }
    }
    return false;
}

System::Boolean
RingerObjectBase::RingerSet()
{
    if (ringerEditor->DataSource == nullptr)
    {
        return false;
    }
    if (ringerEditor->SelectedText->Length > 0)
    {
        return true;
    }
    return KnownRinger();
}

System::String^
RingerObjectBase::RingerText()
{
    return ringerEditor->Text;
}

System::Void
RingerObjectBase::ringerEditor_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode)
    {
        if (ringerEditor->SelectedIndex != -1)
        {
            RingerItem^ object = (RingerItem^)ringerEditor->SelectedItem;
            parent->RingerChanged(object, conductor->Checked, bellNumber, isStrapper);
            CheckControlColour();
        }
    }
}

System::Void 
RingerObjectBase::ringerEditor_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode)
    {
        if (ringerEditor->SelectedIndex == -1)
        {
            parent->RingerChanged(ringerEditor->Text, conductor->Checked, bellNumber, isStrapper);
            CheckControlColour();
        }
    }
}

System::Void 
RingerObjectBase::conductor_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode)
    {
        parent->ConductorChanged(conductor->Checked, bellNumber, isStrapper);
    }
}

System::Void
RingerObjectBase::ringerEditor_Validated(System::Object^ sender, System::EventArgs^ e)
{
    if (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode)
    {
        if (ringerEditor->Focused && sender == ringerEditor && ringerEditor->DataSource != nullptr && ringerEditor->SelectedIndex == -1 && ringerEditor->Text->Trim()->Length > 0)
        {
            Int32 newIndex = parent->RingerChangedToUnknown(ringerEditor->Text, conductor->Checked, bellNumber, isStrapper);
            if (newIndex != Int32::MaxValue)
            {
                ringerEditor->SelectedIndex = newIndex;
            }
        }
    }
}

void
RingerObjectBase::InitializeComponent()
{
    this->ringerEditor = (gcnew System::Windows::Forms::ComboBox());
    this->conductor = (gcnew System::Windows::Forms::CheckBox());
    this->ringerLbl = (gcnew System::Windows::Forms::Label());
    this->SuspendLayout();
    // 
    // ringerLbl
    // 
    this->ringerLbl->AutoSize = false;
    this->ringerLbl->Location = System::Drawing::Point(KLabelXLocation, KYLabelPosition);
    this->ringerLbl->Name = L"ringerLbl";
    this->ringerLbl->Size = System::Drawing::Size(KLabelWidth, KLabelHeight);
    this->ringerLbl->TabIndex = 0;
    this->ringerLbl->Text = L"label1";
    this->ringerLbl->TextAlign = ContentAlignment::MiddleRight;

    // 
    // ringerEditor
    // 
    this->ringerEditor->FormattingEnabled = true;
    this->ringerEditor->Location = System::Drawing::Point(KEditorXLocation, KYPosition);
    this->ringerEditor->Name = L"ringerEditor";
    this->ringerEditor->Size = System::Drawing::Size(KEditorWidth, KEditorHeight);
    this->ringerEditor->TabIndex = 0;
    this->ringerEditor->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
    this->ringerEditor->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
    this->ringerEditor->TextChanged += gcnew System::EventHandler(this, &RingerObjectBase::ringerEditor_TextChanged);
    this->ringerEditor->Validated += gcnew System::EventHandler(this, &RingerObjectBase::ringerEditor_Validated);
    this->ringerEditor->SelectedIndexChanged += gcnew System::EventHandler(this, &RingerObjectBase::ringerEditor_SelectedIndexChanged);
    this->ringerEditor->MouseWheel += gcnew System::Windows::Forms::MouseEventHandler(this, &RingerObjectBase::ringerEditor_MouseWheel);
    // 
    // conductor
    // 
    this->conductor->AutoSize = true;
    this->conductor->Location = System::Drawing::Point(KConductorXLocation, KYLabelPosition);
    this->conductor->Name = L"conductor";
    this->conductor->Size = System::Drawing::Size(KCheckBoxWidth, KCheckBoxHeight);
    this->conductor->TabIndex = 0;
    this->conductor->Text = "";
    this->conductor->UseVisualStyleBackColor = true;
    this->conductor->CheckedChanged += gcnew System::EventHandler(this, &RingerObjectBase::conductor_CheckedChanged);
    // 
    // RingerObjectBase
    // 
    this->Name = L"RingerObjectBase";
    this->Size = System::Drawing::Size(KRingerWidth, KRingerLineHeight);
    this->AutoSize = true;

    Controls->AddRange(gcnew cli::array< System::Windows::Forms::Control^  >(3){ringerLbl, ringerEditor, conductor});
    this->ResumeLayout(false);
}

System::Boolean
RingerObjectBase::SetIndex(System::Boolean /*strapper*/, int index)
{
    if (ringerEditor->DataSource == nullptr)
    {
        DucoUILog::PrintError("Cannot set index - no data source");
        return false;
    }
    else if (ringerEditor->SelectedIndex != index)
    {
        try
        {
            ringerEditor->SelectedIndex = index;
        }
        catch (System::Exception^ e)
        {
            DucoUILog::PrintError("Cannot set index - exception: " + e->Message);
            return false;
        }
        CheckControlColour();
    }
    else
    {
        DUCOUILOGDEBUG("Set index not necessary as not changed");
    }
    return true;
}

System::Void
RingerObjectBase::CheckControlColour()
{
    if (ringerEditor->SelectedIndex != -1 && ringerEditor->Text->Length > 0)
    {
        ringerEditor->BackColor = SystemColors::ControlLightLight;
    }
    else
    {
        ringerEditor->BackColor = KErrorColour;
    }
}

System::Void
RingerObjectBase::ringerEditor_MouseWheel(System::Object^ sender, System::Windows::Forms::MouseEventArgs^ e)
{
    if (e->Delta != 0) // Mouse scrolled
    {
        ((System::Windows::Forms::HandledMouseEventArgs^)e)->Handled = true;
    }
}
