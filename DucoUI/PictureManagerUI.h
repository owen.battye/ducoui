#pragma once

namespace DucoUI
{
    public ref class PictureManagerUI : public System::Windows::Forms::Form,
                                        public DucoUI::IOpenObjectCallback
    {
    public:
        PictureManagerUI(DucoUI::DatabaseManager^ theDatabase);
        ~PictureManagerUI();

    public: // from IOpenObjectCallback
        virtual void OpenObject(Duco::ObjectId& pealId, Duco::TObjectType objectType);

    private:
        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void PictureManagerUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void PictureManagerUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e);
        System::Void PictureManagerUI_Resize(System::Object^  sender, System::EventArgs^  e);

    private:
        /// <summary>
        /// Required designer variable.
        /// </summary>
        System::ComponentModel::Container ^components;
        DucoUI::DatabaseManager^ database;

        DucoUI::PictureListControl^ pictureList;


#pragma region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        void InitializeComponent()
        {
            this->SuspendLayout();
            // 
            // PictureManagerUI
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(930, 479);
            this->Name = L"PictureManagerUI";
            this->Text = L"Picture manager";
            this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &PictureManagerUI::PictureManagerUI_FormClosing);
            this->Load += gcnew System::EventHandler(this, &PictureManagerUI::PictureManagerUI_Load);
            this->Resize += gcnew System::EventHandler(this, &PictureManagerUI::PictureManagerUI_Resize);
            this->ResumeLayout(false);

        }
#pragma endregion
    };
}
