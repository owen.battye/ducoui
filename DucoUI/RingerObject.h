#pragma once
#include "RingerObjectBase.h"
#include "DucoWindowState.h"

namespace Duco
{
    class DucoConfiguration;
    class Peal;
}

namespace DucoUI
{
    public ref class RingerObject :  public DucoUI::RingerObjectBase
    {
    public:
        RingerObject(DucoUI::RingerList^ theparent, unsigned int theBellNumber);
        virtual ~RingerObject();

        // from RingerObjectBase
        virtual System::Void SetNameAndConductor(System::String^ ringerName, bool conductor, bool strapper, int index) override;
        virtual System::Void SetState(DucoWindowState state) override;
        virtual System::Boolean SetRinger(Duco::Peal& newPeal, int index, bool ringerIsConductor, int strapperIndex, bool strapperIsConductor, unsigned int noOfBellsInPeal) override;
        virtual System::Void CheckConductor(bool isConductor, bool silentAndNonConducted, bool strapperIsConductor) override;
        virtual System::Boolean SetIndex(System::Boolean /*strapper*/, int index) override;
        System::Void BeginUpdate(System::Boolean saveOldValues);
        System::Void EndUpdate(DucoUI::RingerDisplayCache^ cache);

        // New functions
        System::Void SetHandbell(bool handbell);

        System::Void strapper_CheckedChanged(System::Object^  sender, System::EventArgs^  e);

        void RemoveStrapper(bool deleteControl);
        void AddStrapper();
        inline System::Boolean IncludesStrapper();
        inline DucoUI::RingerObjectBase^ Strapper();

    private:
        void InitializeComponent();

        System::Windows::Forms::CheckBox^   strapper;
        DucoUI::RingerObjectBase^           strapperCtrl;
    };

    System::Boolean
    RingerObject::IncludesStrapper()
    {
        return strapperCtrl != nullptr;
    }

    DucoUI::RingerObjectBase^
    RingerObject::Strapper()
    {
        return strapperCtrl;
    }
}
