#pragma once
namespace DucoUI
{
	public ref class WebsiteLinksUI : public System::Windows::Forms::Form
	{
	public:
		WebsiteLinksUI(DucoUI::DatabaseManager^ theDatabase);

	protected:
		~WebsiteLinksUI();

        System::Void cancelBtn_Click(System::Object^  sender, System::EventArgs^ e);
        System::Void saveBtn_Click(System::Object^  sender, System::EventArgs^ e);
        System::Void resetBtn_Click(System::Object^  sender, System::EventArgs^  e);

        void Populate(const Duco::DatabaseSettings& populateWith);
        void SaveSettings();

        System::Void WebsiteLinksUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void ClosingEvent( Object^ /*sender*/, System::ComponentModel::CancelEventArgs^ e);
        System::Void TextChanged(System::Object^  sender, System::EventArgs^  e);

    private:
        DucoUI::DatabaseManager^            database;
        System::Windows::Forms::Button^     saveBtn;
        System::Windows::Forms::TextBox^    bellBoardSuffixEditor;
        System::Windows::Forms::TextBox^    bellBoardEditor;
    private: System::Windows::Forms::TextBox^ felsteadUrlEditor;


        System::ComponentModel::IContainer^ components;
        bool                                populatingData;
        System::Windows::Forms::ToolTip^    toolTip1;
        System::Windows::Forms::TextBox^    pealBaseTowersUrl;
    private: System::Windows::Forms::TextBox^ pealBaseAssociationUrl;
    private: System::Windows::Forms::TextBox^ compLibUrl;








           bool                                settingsChanged;

#pragma region Windows Form Designer generated code
		void InitializeComponent()
		{
            this->components = (gcnew System::ComponentModel::Container());
            System::Windows::Forms::Button^ cancelBtn;
            System::Windows::Forms::GroupBox^ bellBoardGroup;
            System::Windows::Forms::GroupBox^ felsteadGrp;
            System::Windows::Forms::Button^ resetBtn;
            System::Windows::Forms::GroupBox^ pealbaseTowers;
            System::Windows::Forms::GroupBox^ pealbaseAssociationGroup;
            System::Windows::Forms::GroupBox^ compLibGroup;
            this->bellBoardSuffixEditor = (gcnew System::Windows::Forms::TextBox());
            this->bellBoardEditor = (gcnew System::Windows::Forms::TextBox());
            this->felsteadUrlEditor = (gcnew System::Windows::Forms::TextBox());
            this->pealBaseTowersUrl = (gcnew System::Windows::Forms::TextBox());
            this->pealBaseAssociationUrl = (gcnew System::Windows::Forms::TextBox());
            this->compLibUrl = (gcnew System::Windows::Forms::TextBox());
            this->saveBtn = (gcnew System::Windows::Forms::Button());
            this->toolTip1 = (gcnew System::Windows::Forms::ToolTip(this->components));
            cancelBtn = (gcnew System::Windows::Forms::Button());
            bellBoardGroup = (gcnew System::Windows::Forms::GroupBox());
            felsteadGrp = (gcnew System::Windows::Forms::GroupBox());
            resetBtn = (gcnew System::Windows::Forms::Button());
            pealbaseTowers = (gcnew System::Windows::Forms::GroupBox());
            pealbaseAssociationGroup = (gcnew System::Windows::Forms::GroupBox());
            compLibGroup = (gcnew System::Windows::Forms::GroupBox());
            bellBoardGroup->SuspendLayout();
            felsteadGrp->SuspendLayout();
            pealbaseTowers->SuspendLayout();
            pealbaseAssociationGroup->SuspendLayout();
            compLibGroup->SuspendLayout();
            this->SuspendLayout();
            // 
            // cancelBtn
            // 
            cancelBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            cancelBtn->Location = System::Drawing::Point(272, 325);
            cancelBtn->Name = L"cancelBtn";
            cancelBtn->Size = System::Drawing::Size(75, 23);
            cancelBtn->TabIndex = 0;
            cancelBtn->Text = L"Cancel";
            cancelBtn->Click += gcnew System::EventHandler(this, &WebsiteLinksUI::cancelBtn_Click);
            // 
            // bellBoardGroup
            // 
            bellBoardGroup->AutoSize = true;
            bellBoardGroup->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            bellBoardGroup->Controls->Add(this->bellBoardSuffixEditor);
            bellBoardGroup->Controls->Add(this->bellBoardEditor);
            bellBoardGroup->Location = System::Drawing::Point(12, 12);
            bellBoardGroup->Name = L"bellBoardGroup";
            bellBoardGroup->Size = System::Drawing::Size(335, 56);
            bellBoardGroup->TabIndex = 2;
            bellBoardGroup->TabStop = false;
            bellBoardGroup->Text = L"BellBoard Url";
            // 
            // bellBoardSuffixEditor
            // 
            this->bellBoardSuffixEditor->Location = System::Drawing::Point(248, 20);
            this->bellBoardSuffixEditor->Margin = System::Windows::Forms::Padding(3, 3, 3, 0);
            this->bellBoardSuffixEditor->Name = L"bellBoardSuffixEditor";
            this->bellBoardSuffixEditor->Size = System::Drawing::Size(81, 20);
            this->bellBoardSuffixEditor->TabIndex = 1;
            this->toolTip1->SetToolTip(this->bellBoardSuffixEditor, L"Peal display suffix");
            this->bellBoardSuffixEditor->TextChanged += gcnew System::EventHandler(this, &WebsiteLinksUI::TextChanged);
            // 
            // bellBoardEditor
            // 
            this->bellBoardEditor->Location = System::Drawing::Point(7, 20);
            this->bellBoardEditor->Margin = System::Windows::Forms::Padding(3, 3, 3, 0);
            this->bellBoardEditor->Name = L"bellBoardEditor";
            this->bellBoardEditor->Size = System::Drawing::Size(234, 20);
            this->bellBoardEditor->TabIndex = 0;
            this->toolTip1->SetToolTip(this->bellBoardEditor, L"Download link");
            this->bellBoardEditor->TextChanged += gcnew System::EventHandler(this, &WebsiteLinksUI::TextChanged);
            // 
            // felsteadGrp
            // 
            felsteadGrp->AutoSize = true;
            felsteadGrp->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            felsteadGrp->Controls->Add(this->felsteadUrlEditor);
            felsteadGrp->Location = System::Drawing::Point(12, 74);
            felsteadGrp->Name = L"felsteadGrp";
            felsteadGrp->Size = System::Drawing::Size(335, 56);
            felsteadGrp->TabIndex = 3;
            felsteadGrp->TabStop = false;
            felsteadGrp->Text = L"Felstead Url";
            // 
            // felsteadUrlEditor
            // 
            this->felsteadUrlEditor->Location = System::Drawing::Point(7, 20);
            this->felsteadUrlEditor->Margin = System::Windows::Forms::Padding(3, 3, 3, 0);
            this->felsteadUrlEditor->Name = L"felsteadUrlEditor";
            this->felsteadUrlEditor->Size = System::Drawing::Size(322, 20);
            this->felsteadUrlEditor->TabIndex = 0;
            this->toolTip1->SetToolTip(this->felsteadUrlEditor, L"Download link");
            this->felsteadUrlEditor->TextChanged += gcnew System::EventHandler(this, &WebsiteLinksUI::TextChanged);
            // 
            // resetBtn
            // 
            resetBtn->Location = System::Drawing::Point(11, 325);
            resetBtn->Name = L"resetBtn";
            resetBtn->Size = System::Drawing::Size(75, 23);
            resetBtn->TabIndex = 4;
            resetBtn->Text = L"Reset";
            resetBtn->UseVisualStyleBackColor = true;
            resetBtn->Click += gcnew System::EventHandler(this, &WebsiteLinksUI::resetBtn_Click);
            // 
            // pealbaseTowers
            // 
            pealbaseTowers->Controls->Add(this->pealBaseTowersUrl);
            pealbaseTowers->Location = System::Drawing::Point(12, 199);
            pealbaseTowers->Name = L"pealbaseTowers";
            pealbaseTowers->Size = System::Drawing::Size(335, 57);
            pealbaseTowers->TabIndex = 4;
            pealbaseTowers->TabStop = false;
            pealbaseTowers->Text = L"Pealbase tower Url";
            // 
            // pealBaseTowersUrl
            // 
            this->pealBaseTowersUrl->Location = System::Drawing::Point(7, 20);
            this->pealBaseTowersUrl->Name = L"pealBaseTowersUrl";
            this->pealBaseTowersUrl->Size = System::Drawing::Size(322, 20);
            this->pealBaseTowersUrl->TabIndex = 0;
            this->toolTip1->SetToolTip(this->pealBaseTowersUrl, L"Download link");
            // 
            // pealbaseAssociationGroup
            // 
            pealbaseAssociationGroup->Controls->Add(this->pealBaseAssociationUrl);
            pealbaseAssociationGroup->Location = System::Drawing::Point(12, 136);
            pealbaseAssociationGroup->Name = L"pealbaseAssociationGroup";
            pealbaseAssociationGroup->Size = System::Drawing::Size(335, 57);
            pealbaseAssociationGroup->TabIndex = 5;
            pealbaseAssociationGroup->TabStop = false;
            pealbaseAssociationGroup->Text = L"Pealbase association Url";
            // 
            // pealBaseAssociationUrl
            // 
            this->pealBaseAssociationUrl->Location = System::Drawing::Point(7, 20);
            this->pealBaseAssociationUrl->Name = L"pealBaseAssociationUrl";
            this->pealBaseAssociationUrl->Size = System::Drawing::Size(322, 20);
            this->pealBaseAssociationUrl->TabIndex = 0;
            this->toolTip1->SetToolTip(this->pealBaseAssociationUrl, L"Download link");
            // 
            // compLibGroup
            // 
            compLibGroup->Controls->Add(this->compLibUrl);
            compLibGroup->Location = System::Drawing::Point(12, 262);
            compLibGroup->Name = L"compLibGroup";
            compLibGroup->Size = System::Drawing::Size(335, 57);
            compLibGroup->TabIndex = 5;
            compLibGroup->TabStop = false;
            compLibGroup->Text = L"Comp lib Url";
            // 
            // compLibUrl
            // 
            this->compLibUrl->Location = System::Drawing::Point(7, 20);
            this->compLibUrl->Name = L"compLibUrl";
            this->compLibUrl->Size = System::Drawing::Size(322, 20);
            this->compLibUrl->TabIndex = 0;
            this->toolTip1->SetToolTip(this->compLibUrl, L"Download link");
            // 
            // saveBtn
            // 
            this->saveBtn->Enabled = false;
            this->saveBtn->Location = System::Drawing::Point(191, 325);
            this->saveBtn->Name = L"saveBtn";
            this->saveBtn->Size = System::Drawing::Size(75, 23);
            this->saveBtn->TabIndex = 0;
            this->saveBtn->Text = L"Save";
            this->saveBtn->Click += gcnew System::EventHandler(this, &WebsiteLinksUI::saveBtn_Click);
            // 
            // WebsiteLinksUI
            // 
            this->AcceptButton = this->saveBtn;
            this->CancelButton = cancelBtn;
            this->ClientSize = System::Drawing::Size(360, 357);
            this->Controls->Add(compLibGroup);
            this->Controls->Add(pealbaseAssociationGroup);
            this->Controls->Add(pealbaseTowers);
            this->Controls->Add(resetBtn);
            this->Controls->Add(felsteadGrp);
            this->Controls->Add(bellBoardGroup);
            this->Controls->Add(cancelBtn);
            this->Controls->Add(this->saveBtn);
            this->Name = L"WebsiteLinksUI";
            this->Text = L"Websites";
            this->Load += gcnew System::EventHandler(this, &WebsiteLinksUI::WebsiteLinksUI_Load);
            bellBoardGroup->ResumeLayout(false);
            bellBoardGroup->PerformLayout();
            felsteadGrp->ResumeLayout(false);
            felsteadGrp->PerformLayout();
            pealbaseTowers->ResumeLayout(false);
            pealbaseTowers->PerformLayout();
            pealbaseAssociationGroup->ResumeLayout(false);
            pealbaseAssociationGroup->PerformLayout();
            compLibGroup->ResumeLayout(false);
            compLibGroup->PerformLayout();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion
    };
}
