#include "DatabaseManager.h"

#include "OnThisDay.h"
#include "DucoUtils.h"
#include <Peal.h>
#include "PealSummaryControl.h"
#include "SystemDefaultIcon.h"
#include "DucoUILog.h"

using namespace DucoUI;

OnThisDay::OnThisDay(DucoUI::DatabaseManager^ theDatabase)
    : database(theDatabase)
{
    InitializeComponent();
}

OnThisDay::~OnThisDay()
{
    if (components)
    {
        delete components;
    }
}

System::Void
OnThisDay::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void
OnThisDay::OnThisDay_Load(System::Object^  sender, System::EventArgs^  e)
{
    this->Icon = SystemDefaultIcon::DefaultSystemIcon(database->WindowsSettings());
}

System::Void
OnThisDay::OnThisDay_Activated(System::Object^  sender, System::EventArgs^  e)
{
    DUCOUILOGDEBUG("OnThisDay_Activated");
    pealsPanel->Controls->Clear();
    Int32 pealNo = 0;
    System::Collections::Generic::List<System::UInt32>^ pealIds = gcnew System::Collections::Generic::List<System::UInt32>();
    database->OnThisDay(pealIds);
    for (pealNo = 0; pealIds->Count >= (pealNo + 1) && pealNo < 10; ++pealNo)
    {
        PealSummaryControl^ newPealCtrl = gcnew PealSummaryControl(database);
        pealsPanel->Controls->Add(newPealCtrl);
        newPealCtrl->SetPealDetails(pealIds[pealNo], pealIds->Count == 1);
        newPealCtrl->Width = pealsPanel->Width - 8;
    }
    if (pealNo == 1)
    {
        this->ClientSize = System::Drawing::Size(689, 372);
    }
    else
    {
        this->ClientSize = System::Drawing::Size(689, 97 + ((pealNo - 2) * 34));
    }
    pealsPanel->Resize += gcnew System::EventHandler(this, &OnThisDay::OnThisDay_Resize);
}


System::Void
OnThisDay::OnThisDay_Resize(Object^ sender, System::EventArgs^ /*e*/)
{
    int ctrlWidth = pealsPanel->Width - 6;
    if (this->VerticalScroll->Enabled)
    {
        ctrlWidth -= 18;
    }
    System::Collections::IEnumerator^ it = pealsPanel->Controls->GetEnumerator();
    while (it->MoveNext())
    {
        PealSummaryControl^ pealCtrl = static_cast<PealSummaryControl^>(it->Current);
        pealCtrl->Width = ctrlWidth;
    }
}