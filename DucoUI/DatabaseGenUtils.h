#pragma once

#include <set>
#include <list>
#include <string>

namespace Duco
{
    class Association;
    class DucoConfiguration;
    class MethodSeries;
    class ObjectId;
    class PerformanceDate;
}

namespace DucoUI
{
    ref class DatabaseManager;

    public ref class DatabaseGenUtils
    {
    public:
        /* Towers */
        static System::Void GenerateTowerOptions(System::Windows::Forms::ListBox^ control, System::Collections::Generic::List<System::Int16>^ allTowerNames, DucoUI::DatabaseManager^ database, const Duco::ObjectId& defaultTowerId, bool ignoreRemoved, bool ignoreTowersWithoutLocation, bool addBlank);
        static System::Void GenerateTowerOptions(System::Windows::Forms::ComboBox^ control, System::Collections::Generic::List<System::Int16>^ allTowerNames, DucoUI::DatabaseManager^ database, const Duco::ObjectId& defaultTowerId, bool ignoreRemoved, bool ignoreTowersWithoutLocation, bool addBlank);
        static bool AddTowerOption(System::Windows::Forms::ComboBox^ control, const Duco::ObjectId& towerId, System::Collections::Generic::List<System::Int16>^ allTowerNames, DucoUI::DatabaseManager^ database);
        static bool UpdateTowerOption(System::Windows::Forms::ComboBox^ control, const Duco::ObjectId& towerId, System::Collections::Generic::List<System::Int16>^ allTowerNames, DucoUI::DatabaseManager^ database);
        static bool RemoveTowerOption(System::Windows::Forms::ComboBox^ control, const Duco::ObjectId& towerId, System::Collections::Generic::List<System::Int16>^ allTowerNames);
        static void GetAllPlaceOptions(System::Windows::Forms::ComboBox^% countiesControl, System::Windows::Forms::ComboBox^% citiesControl, System::Windows::Forms::ComboBox^% namesControl, DucoUI::DatabaseManager^% database);
        static void GetAlCountyOptions(System::Windows::Forms::ComboBox^ countiesControl, DucoUI::DatabaseManager^ database);

        /* Rings */
        static System::Void GenerateRingOptions(System::Windows::Forms::ComboBox^ ringSelector, System::Windows::Forms::Label^ ringTenorLbl, System::Collections::Generic::List<System::Int16>^ allRingNames, DucoUI::DatabaseManager^ database, const Duco::ObjectId& towerId, const Duco::ObjectId& ringId, unsigned int noOfBells, bool addAll);

        /* Methods */
        static System::Void GenerateMethodOptions(System::Collections::Generic::List<System::Int16>^ allMethodIds,
                                            System::Windows::Forms::ComboBox^ methodEditor, DucoUI::DatabaseManager^ database,
                                            const Duco::ObjectId& currentMethodId, unsigned int noOfBells, bool withBobsAndSinglesOnly, bool anyStage, bool clearIfNotFound);
        static System::Void GenerateMethodOptions(System::Collections::Generic::List<System::Int16>^ allMethodIds,
                                            System::Windows::Forms::ComboBox^ methodEditor, DucoUI::DatabaseManager^ database,
                                            const Duco::ObjectId& currentMethodId, const Duco::MethodSeries& methSeries, bool excludeSpliced);
        static System::Void GenerateMethodOptions(System::Collections::Generic::List<System::Int16>^ allMethodIds,
                                                System::Windows::Forms::ComboBox^ methodEditor, DucoUI::DatabaseManager^ database,
                                                bool addblank);

        static System::Void AddMethodOption(const Duco::ObjectId& methodId, System::Collections::Generic::List<System::Int16>^ allMethodNames,
                                            unsigned int noOfBells, DucoUI::DatabaseManager^ database,
                                            System::Windows::Forms::ComboBox^ methodEditor);
        static bool UpdateMethodOption(const Duco::ObjectId& methodId, System::Collections::Generic::List<System::Int16>^ allMethodNames,
                                            unsigned int noOfBells, DucoUI::DatabaseManager^ database,
                                            System::Windows::Forms::ComboBox^ methodEditor);
        static bool RemoveMethodOption(const Duco::ObjectId& methodId, System::Collections::Generic::List<System::Int16>^ allMethodNames,
                                            unsigned int noOfBells, System::Windows::Forms::ComboBox^ methodEditor);

        /* Method Series */
        static System::Void GenerateMethodSeriesOptions(System::Windows::Forms::ComboBox^ control, System::Collections::Generic::List<System::Int16>^ allSeriesNames, DucoUI::DatabaseManager^ database, const Duco::ObjectId& seriesId, unsigned int noOfBells, bool clearOldValue);
        static System::Void AddMethodSeriesOption(const Duco::ObjectId& methodSeriesId, System::Collections::Generic::List<System::Int16>^ allMethodSeriesNames,
                                            unsigned int noOfBells, DucoUI::DatabaseManager^ database,
                                            System::Windows::Forms::ComboBox^ methodSeriesEditor);
        static bool UpdateMethodSeriesOption(const Duco::ObjectId& methodSeriesId, System::Collections::Generic::List<System::Int16>^ allMethodSeriesNames,
                                            unsigned int noOfBells, DucoUI::DatabaseManager^ database,
                                            System::Windows::Forms::ComboBox^ methodEditor);
        static bool RemoveMethodSeriesOption(const Duco::ObjectId& methodSeriesId, System::Collections::Generic::List<System::Int16>^ allMethodSeriesNames,
                                            unsigned int noOfBells, System::Windows::Forms::ComboBox^ methodSeriesEditor);

        /* Compositions */
        static System::Void GenerateCompositionOptions(System::Windows::Forms::ComboBox^ control, System::Collections::Generic::List<System::Int16>^ allCompositionNames, DucoUI::DatabaseManager^ database, const Duco::ObjectId& compositionId, unsigned int noOfBells, bool clearOldValue);
        static System::Void AddCompositionOption(const Duco::ObjectId& compositionId, System::Collections::Generic::List<System::Int16>^ allCompositionNames,
                                            unsigned int noOfBells, DucoUI::DatabaseManager^ database,
                                            System::Windows::Forms::ComboBox^ compositionEditor);
        static bool UpdateCompositionOption(const Duco::ObjectId& compositionId, System::Collections::Generic::List<System::Int16>^ allCompositionNames,
                                            unsigned int noOfBells, DucoUI::DatabaseManager^ database,
                                            System::Windows::Forms::ComboBox^ compositionEditor);
        static bool RemoveCompositionOption(const Duco::ObjectId& compositionId, System::Collections::Generic::List<System::Int16>^ allCompositionNames,
                                            unsigned int noOfBells, System::Windows::Forms::ComboBox^ compositionEditor);

        /* Associations */
        static System::Void GenerateAssociationOptions(System::Windows::Forms::ComboBox^ control, System::Collections::Generic::List<System::Int16>^ allAssociationIds, DucoUI::DatabaseManager^ database, const Duco::ObjectId& currentAssociationId, bool addBlank);
        static System::Void GenerateAssociationOptions(System::Windows::Forms::CheckedListBox^ control, System::Collections::Generic::List<System::Int16>^ allAssociationIds, DucoUI::DatabaseManager^ database, const Duco::Association& currentAssociation, bool showOnlyLinked);
        static System::Void PopulateAssociationOptions(System::Windows::Forms::ComboBox^ control, System::Collections::Generic::List<System::Int16>^ allAssociationIds, DucoUI::DatabaseManager^ database, const Duco::ObjectId& currentAssociationId);
        static System::Void AddAssociationOption(const Duco::ObjectId& associationId, System::Collections::Generic::List<System::Int16>^ allAssociationIds, DucoUI::DatabaseManager^ database, System::Windows::Forms::ComboBox^ control);
        static System::Void UpdateAssociationOption(const Duco::ObjectId& associationId, System::Collections::Generic::List<System::Int16>^ allAssociationIds, DucoUI::DatabaseManager^ database, System::Windows::Forms::ComboBox^ control);
        static System::Void RemoveAssociationOption(const Duco::ObjectId& associationId, System::Collections::Generic::List<System::Int16>^ allAssociationIds, System::Windows::Forms::ComboBox^ control);

        /* Stages */
        static System::Void GenerateStageOptions(System::Windows::Forms::ComboBox^ stageSelector, DucoUI::DatabaseManager^ database, bool includeAll);

        /* Method Types */
        static System::Void GenerateMethodTypeOptions(System::Windows::Forms::ComboBox^% typeEditor, DucoUI::DatabaseManager^ database);
        static System::Void GenerateMethodNameOptions(System::Windows::Forms::ComboBox^% nameEditor, DucoUI::DatabaseManager^ database);
        static System::Void GenerateMethodNotationOptions(System::Windows::Forms::ComboBox^% control, DucoUI::DatabaseManager^ database);

        /* Generic functions */
        static Duco::ObjectId FindId(System::Collections::Generic::List<System::Int16>^ allIds, System::Int32 index);
        static std::set<Duco::ObjectId> FindIds(System::Collections::Generic::List<System::Int16>^ allIds, System::Windows::Forms::ListBox::SelectedIndexCollection^ indexes);
        static System::Int32 FindIndex(System::Collections::Generic::List<System::Int16>^ allIds, const Duco::ObjectId& id);
        static System::Int32 FindIndex(System::Collections::Generic::List<System::Int16>^ allIds, const Duco::ObjectId& theRingerId, const Duco::PerformanceDate& pealDate, DucoUI::DatabaseManager^ database);
        static System::Collections::Generic::List<System::Int32>^ FindIndexes(System::Collections::Generic::List<System::Int16>^ allIds, const Duco::ObjectId& id);

    protected:
        static System::Void AddStringsToControl(Duco::DucoConfiguration& config, System::Windows::Forms::ComboBox^% control, std::set<std::wstring>& strings);
    };
}
