#pragma once
namespace DucoUI
{
    public ref class PealFeeObjectBase :  public System::Windows::Forms::UserControl
    {
    public:
        PealFeeObjectBase(DucoUI::DatabaseManager^ theDatabase, unsigned int theBellNumber, bool isAStrapper, Duco::Peal* thePeal);
        virtual ~PealFeeObjectBase();
        !PealFeeObjectBase();

        virtual bool SetPeal();

        event System::EventHandler^ dataChangedEventHandler;
        System::Void PealFeeObjectChanged(System::Object^  sender, System::EventArgs^  e);

    protected:
        virtual void OnRingerDataChanged(DucoUI::RingerEventArgs^ e);
        System::Void maskedTextBox1_Leave(System::Object^  sender, System::EventArgs^  e);

    private:
        void InitializeComponent();

    protected:
        DucoUI::DatabaseManager^            database;
        unsigned int                        bellNumber;
        bool                                isStrapper;

        Duco::Peal* const                   currentPeal;

        System::ComponentModel::Container^  components;

        System::Windows::Forms::Label^      ringerNameLbl;
        System::Windows::Forms::Label^      ringerLbl;
        System::Windows::Forms::MaskedTextBox^  maskedTextBox1;
    };
}
