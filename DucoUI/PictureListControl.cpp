#include "RingingDatabaseObserver.h"
#include "DatabaseManager.h"
#include "OpenObjectCallback.h"

#include "PictureListControl.h"

#include "InputIndexQueryUI.h"
#include "InputStringQueryUI.h"
#include "PictureControl.h"
#include <Picture.h>
#include <algorithm>
#include <iterator>
#include "SoundUtils.h"
#include "DucoUtils.h"
#include <RingingDatabase.h>
#include <PictureDatabase.h>
#include "PictureEventArgs.h"

using namespace System;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace DucoUI;
using namespace Duco;

PictureListControl::PictureListControl(DucoUI::DatabaseManager^ theDatabase, IOpenObjectCallback^ theCallback)
    : database(theDatabase), callback(theCallback), picturesToShowPerPage(1), singleSelectionMode(false), updateInProgress(false)
{
    InitializeComponent();
    pictureIdToShow = new Duco::ObjectId();
    lastShownPictureId = new Duco::ObjectId();
    firstShowPictureOnCurrentPage = new Duco::ObjectId();
    database->AddObserver(this);
}

PictureListControl::!PictureListControl()
{
    database->RemoveObserver(this);
    delete lastShownPictureId;
    delete firstShowPictureOnCurrentPage;
}

PictureListControl::~PictureListControl()
{
    this->!PictureListControl();
    if (components)
    {
        delete components;
    }
}

void
PictureListControl::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    switch (type)
    {
    case TObjectType::EPicture:
    case TObjectType::EPeal:
    case TObjectType::ETower:
    {
        RefreshList(false, false, false);
    }
    break;
    default:
        break;
    }
}

System::Void
PictureListControl::startBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->Database().PicturesDatabase().FirstObject(*pictureIdToShow, false))
    {
        RefreshList(false, false, false);
    }
}

System::Void
PictureListControl::nextPageBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    RefreshList(false, true, true);
}

System::Void
PictureListControl::previousPageBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    RefreshList(false, true, false);
}

System::Void
PictureListControl::endBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->Database().PicturesDatabase().LastObject(*pictureIdToShow, false))
    {
        RefreshList(false, false, false);
    }
}

System::Void
PictureListControl::RefreshList(bool reset, bool move, bool forwards)
{
    if (!updateInProgress)
    {
        Int32 highestIndex = 0;
        Int32 lowestIndex = 9999;
        updateInProgress = true;
        UInt32 numberOfPicturesPreviouslyShown = flowLayoutPanel1->Controls->Count;
        flowLayoutPanel1->Controls->Clear();
        {
            // determine how many pictures can be displayed
            PictureControl^ newPictureCtrl = gcnew PictureControl(database, this);
            System::Drawing::Size^ thisSize = flowLayoutPanel1->ClientSize;
            UInt16 picturesWide = thisSize->Width / (newPictureCtrl->Width + 6);
            UInt16 picturesHigh = thisSize->Height / (newPictureCtrl->Height + 6);
            picturesToShowPerPage = std::max(picturesWide * picturesHigh, 1);
        }

        if (pictureIdToShow->ValidId())
        {
            *lastShownPictureId = *pictureIdToShow;
            pictureIdToShow->ClearId();
        }
        else
        {
            if (reset || (move && !lastShownPictureId->ValidId() && forwards))
            {
                database->FirstPicture(*lastShownPictureId, false);
            }
            else if (move && !lastShownPictureId->ValidId())
            {
                database->LastPicture(*lastShownPictureId);
            }
            if (move && !forwards)
            {
                //Need to go back x pictures.
                UInt32 picturesToShowOnThisAttempt = picturesToShowPerPage + numberOfPicturesPreviouslyShown;
                while (picturesToShowOnThisAttempt > 0 && lastShownPictureId->ValidId())
                {
                    database->NextPicture(*lastShownPictureId, false, false);
                    --picturesToShowOnThisAttempt;
                }
                if (!lastShownPictureId->ValidId())
                {
                    database->FirstPicture(*lastShownPictureId, false);
                }
            }
            if (!reset && !move)
            {
                *lastShownPictureId = *firstShowPictureOnCurrentPage;
            }
            firstShowPictureOnCurrentPage->ClearId();
            if (!lastShownPictureId->ValidId())
            {
                database->FirstPicture(*lastShownPictureId, false);
            }
        }

        UInt32 picturesToShowOnThisAttempt = picturesToShowPerPage;
        while (picturesToShowOnThisAttempt > 0 && lastShownPictureId->ValidId())
        {
            Duco::Picture thePicture;
            if (database->FindPicture(*lastShownPictureId, thePicture, false))
            {
                if (!firstShowPictureOnCurrentPage->ValidId())
                {
                    *firstShowPictureOnCurrentPage = *lastShownPictureId;
                }

                highestIndex = Math::Max(highestIndex, Convert::ToInt32(lastShownPictureId->Id()));
                lowestIndex = Math::Min(lowestIndex, Convert::ToInt32(lastShownPictureId->Id()));
                PictureControl^ newPictureCtrl = gcnew PictureControl(database, this, thePicture);
                newPictureCtrl->EnableSelection();
                newPictureCtrl->callback = callback;
                flowLayoutPanel1->Controls->Add(newPictureCtrl);
            }

            database->NextPicture(*lastShownPictureId, true, false);

            --picturesToShowOnThisAttempt;
        }
        if (highestIndex == lowestIndex)
        {
            if (highestIndex != -1)
                locationLbl->Text = "Showing picture " + Convert::ToString(lowestIndex);
            else
                locationLbl->Text = "";
        }
        else if (database->NumberOfObjects(Duco::TObjectType::EPicture) > 0)
        {
            locationLbl->Text = "Showing pictures from " + Convert::ToString(lowestIndex) + " to " + Convert::ToString(highestIndex);
        }
        else
        {
            locationLbl->Text = "No pictures in database";
        }

        updateInProgress = false;
    }
}

System::Void
PictureListControl::newBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    PictureControl^ newPicture = gcnew PictureControl(database);
    newPicture->newBtn_Click(sender, e);
}

System::Void PictureListControl::pasteBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    PictureControl^ newPicture = gcnew PictureControl(database);
    newPicture->pasteBtn_Click(sender, e);
}

UInt16
PictureListControl::NumberOfSelectedControls()
{
    return flowLayoutPanel1->Controls->Count;
}

System::Void
PictureListControl::PicturesToRemove(std::set<Duco::ObjectId>& unusedPictureIds)
{
    unusedPictureIds.clear();

    IEnumerator^ it = flowLayoutPanel1->Controls->GetEnumerator();
    while (it->MoveNext())
    {
        PictureControl^ nextPicture = static_cast<PictureControl^>(it->Current);
        if (nextPicture->Selected())
        {
            unusedPictureIds.insert(nextPicture->PictureId());
        }
    }
}

System::Void
PictureListControl::deleteBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    switch (NumberOfSelectedControls())
    {
    case 0:
        MessageBox::Show("Select one picture or more to remove from database.", "No pictures selected");
        break;

    default:
        {
            std::set<Duco::ObjectId> unusedPictureIds;
            database->Database().FindUnusedPictures(unusedPictureIds);
            std::set<Duco::ObjectId> pictureIdsToRemove;
            PicturesToRemove(pictureIdsToRemove);
            std::set<Duco::ObjectId> pictureIdsToRemoveInUse;

            std::set_intersection(unusedPictureIds.begin(), unusedPictureIds.end(), pictureIdsToRemove.begin(), pictureIdsToRemove.end(), std::inserter(pictureIdsToRemoveInUse, pictureIdsToRemoveInUse.begin()));
            if (pictureIdsToRemoveInUse.size() != pictureIdsToRemove.size())
            {
                if (MessageBox::Show("One or more of the pictures selected to remove are in use. Are you sure you want to remove these pictures?", "Pictures in use", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning) == ::DialogResult::Cancel)
                {
                    break;
                }
            }
            database->RemoveAndDeletePictures(pictureIdsToRemove);
        }
        break;
    }
}

System::Void
PictureListControl::deleteUnusedBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    std::set<Duco::ObjectId> unusedPictureIds;
    database->Database().FindUnusedPictures(unusedPictureIds);

    if (unusedPictureIds.size() > 0)
    {
        if (!database->NothingInEditMode())
        {
            SoundUtils::PlayErrorSound();
            MessageBox::Show(this, "Close all open dialogs before trying again", "Dialogs open - cannot continue");
        }
        else if (MessageBox::Show(this, "Are you sure you want to delete the " + Convert::ToString(unusedPictureIds.size()) + " unused pictures? This operation cannot be undone.", "Delete unused pictures?", MessageBoxButtons::YesNo, MessageBoxIcon::Warning) == ::DialogResult::Yes)
        {
            database->DeletePictures(unusedPictureIds, false);
            RefreshList(true, false, false);
        }
    }
    else
    {
        SoundUtils::PlayFinishedSound();
    }
}

System::Void
PictureListControl::selectBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    PictureEventArgs^ args = gcnew PictureEventArgs();
    DucoUI::PictureControl^ selectedPicture = static_cast<DucoUI::PictureControl^>(FindSelectedPicture());
    if (selectedPicture != nullptr)
    {
        args->pictureId = new Duco::ObjectId(selectedPicture->PictureId());
    }
    pictureSelectedHandler(this, args);
}

System::Void
PictureListControl::cancelBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    closeHandler(this, e);
}

System::Void
PictureListControl::SetSingleSelectionMode()
{
    deleteUnusedBtn->Visible = false;
    deleteBtn->Visible = false;
    newBtn->Visible = false;
    pasteBtn->Visible = false;
    selectBtn->Visible = true;
    singleSelectionMode = true;
}

System::Void
PictureListControl::PictureSelected(System::Windows::Forms::UserControl^ selectedPicture)
{
    if (singleSelectionMode)
    {
        IEnumerator^ it = flowLayoutPanel1->Controls->GetEnumerator();
        while (it->MoveNext())
        {
            PictureControl^ nextPicture = static_cast<PictureControl^>(it->Current);
            if (selectedPicture != nextPicture && nextPicture->Selected())
            {
                nextPicture->Unselect();
            }
        }
    }
}

System::Windows::Forms::UserControl^
PictureListControl::FindSelectedPicture()
{
    if (singleSelectionMode)
    {
        IEnumerator^ it = flowLayoutPanel1->Controls->GetEnumerator();
        while (it->MoveNext())
        {
            PictureControl^ nextPicture = static_cast<PictureControl^>(it->Current);
            if (nextPicture->Selected())
            {
                return nextPicture;
            }
        }
    }
    return nullptr;
}

System::Void
PictureListControl::ShowPicture(Duco::ObjectId& pictureId)
{
    *pictureIdToShow = pictureId;
}

System::Void
PictureListControl::locationLbl_Click(System::Object^  sender, System::EventArgs^  e)
{
    System::Int16 id = -1;
    InputIndexQueryUI^ findIdDialog = gcnew InputIndexQueryUI("picture", id);
    System::Windows::Forms::DialogResult returnVal = findIdDialog->ShowDialog();
    if (returnVal == ::DialogResult::OK)
    {
        if (database->Database().PicturesDatabase().FindPicture(Duco::ObjectId(id)) != NULL)
        {
            (*pictureIdToShow) = id;
            RefreshList(false, false, false);
        }
    }
}

System::Void
PictureListControl::findBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    InputStringQueryUI^ findIdDialog = gcnew InputStringQueryUI("picture");
    System::Windows::Forms::DialogResult returnVal = findIdDialog->ShowDialog();
    if (returnVal == ::DialogResult::OK)
    {
        std::wstring searchStringForEngine = L"";
        DucoUtils::ConvertString(findIdDialog->searchString, searchStringForEngine);
        Duco::ObjectId pictureId = database->Database().FindPicture(searchStringForEngine, *pictureIdToShow);
        if (pictureId.ValidId())
        {
            (*pictureIdToShow) = pictureId;
            RefreshList(false, false, false);
            (*pictureIdToShow) = pictureId;
        }
        else
        {
            SoundUtils::PlayErrorSound();
        }
    }
}
