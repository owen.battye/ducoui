#pragma once
namespace DucoUI
{
    public ref class ConductedStatsUI : public System::Windows::Forms::Form, public DucoUI::RingingDatabaseObserver
    {
    public:
        ConductedStatsUI(DucoUI::DatabaseManager^ theDatabase);

        System::Void ConductedStatsUI_Load(System::Object^  sender, System::EventArgs^  e);

        // from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);
        System::Void filtersBtn_Click(System::Object^  sender, System::EventArgs^  e);

    protected:
        ~ConductedStatsUI();
        !ConductedStatsUI();
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ MethodId;
    protected:
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ MethodNameColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ methodsConductedColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ methodsChangesColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ methodsTimeColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ OrdersConductedColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ stagesChangesColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ stagesTimeColumn;

    protected:



        enum class TState
        {
            ENon = 0,
            EConductedOrders,
            EConductedMethod
        }      state;

        System::Void Restart(ConductedStatsUI::TState newState);
        System::Void backgroundWorker_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void backgroundWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void backgroundWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);

        System::Void TabChangedOrdersConducted(System::Object^  sender, System::EventArgs^  e);
        System::Void TabChangedMethodsConducted(System::Object^  sender, System::EventArgs^  e);

        System::Void methodsConductedData_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);
        System::Void ordersConductedData_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);

        System::Void methodsConductedData_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);
        System::Void ringerSelector_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);

        System::Void GenerateConductedOrderCounts(System::ComponentModel::DoWorkEventArgs^ e);
        System::Void GenerateConductedMethodCounts(System::ComponentModel::DoWorkEventArgs^ e);

    private:
        System::ComponentModel::Container^                  components;
        Duco::StatisticFilters*                             filters;
        System::Windows::Forms::ToolStripProgressBar^       progressBar;
        System::ComponentModel::BackgroundWorker^           backgroundWorker;
        System::Windows::Forms::DataGridView^               ordersConductedData;


        System::Windows::Forms::DataGridView^               methodsConductedData;





        DucoUI::DatabaseManager^                      database;
        bool                                                ordersConductedGenerated;
        bool                                                conductedMethodGenerated;
        bool                                                loading;
        TState                                              nextState;

        System::Void InitializeComponent(void)
        {
            System::Windows::Forms::StatusStrip^ statusStrip1;
            System::Windows::Forms::TabControl^ tabControl1;
            System::Windows::Forms::TabPage^ methodsConductedTab;
            System::Windows::Forms::TabPage^ ordersConductedTab;
            System::Windows::Forms::ToolStrip^ toolStrip1;
            System::Windows::Forms::ToolStripButton^ filtersBtn;
            System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(ConductedStatsUI::typeid));
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle7 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle8 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle5 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle6 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            this->progressBar = (gcnew System::Windows::Forms::ToolStripProgressBar());
            this->methodsConductedData = (gcnew System::Windows::Forms::DataGridView());
            this->ordersConductedData = (gcnew System::Windows::Forms::DataGridView());
            this->backgroundWorker = (gcnew System::ComponentModel::BackgroundWorker());
            this->MethodId = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->MethodNameColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->methodsConductedColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->methodsChangesColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->methodsTimeColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->OrdersConductedColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->stagesChangesColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->stagesTimeColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            tabControl1 = (gcnew System::Windows::Forms::TabControl());
            methodsConductedTab = (gcnew System::Windows::Forms::TabPage());
            ordersConductedTab = (gcnew System::Windows::Forms::TabPage());
            toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
            filtersBtn = (gcnew System::Windows::Forms::ToolStripButton());
            statusStrip1->SuspendLayout();
            tabControl1->SuspendLayout();
            methodsConductedTab->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->methodsConductedData))->BeginInit();
            ordersConductedTab->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ordersConductedData))->BeginInit();
            toolStrip1->SuspendLayout();
            this->SuspendLayout();
            // 
            // statusStrip1
            // 
            statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->progressBar });
            statusStrip1->Location = System::Drawing::Point(0, 470);
            statusStrip1->Name = L"statusStrip1";
            statusStrip1->Size = System::Drawing::Size(474, 22);
            statusStrip1->TabIndex = 1;
            statusStrip1->Text = L"statusStrip1";
            // 
            // progressBar
            // 
            this->progressBar->Name = L"progressBar";
            this->progressBar->Size = System::Drawing::Size(100, 16);
            // 
            // tabControl1
            // 
            tabControl1->Controls->Add(methodsConductedTab);
            tabControl1->Controls->Add(ordersConductedTab);
            tabControl1->Dock = System::Windows::Forms::DockStyle::Fill;
            tabControl1->Location = System::Drawing::Point(0, 25);
            tabControl1->Name = L"tabControl1";
            tabControl1->SelectedIndex = 0;
            tabControl1->Size = System::Drawing::Size(474, 445);
            tabControl1->TabIndex = 0;
            // 
            // methodsConductedTab
            // 
            methodsConductedTab->Controls->Add(this->methodsConductedData);
            methodsConductedTab->Location = System::Drawing::Point(4, 22);
            methodsConductedTab->Name = L"methodsConductedTab";
            methodsConductedTab->Padding = System::Windows::Forms::Padding(3);
            methodsConductedTab->Size = System::Drawing::Size(376, 419);
            methodsConductedTab->TabIndex = 2;
            methodsConductedTab->Text = L"Methods Conducted";
            methodsConductedTab->UseVisualStyleBackColor = true;
            // 
            // methodsConductedData
            // 
            this->methodsConductedData->AllowUserToAddRows = false;
            this->methodsConductedData->AllowUserToDeleteRows = false;
            this->methodsConductedData->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::DisplayedCells;
            this->methodsConductedData->AutoSizeRowsMode = System::Windows::Forms::DataGridViewAutoSizeRowsMode::DisplayedCells;
            this->methodsConductedData->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->methodsConductedData->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(5) {
                this->MethodId,
                    this->MethodNameColumn, this->methodsConductedColumn, this->methodsChangesColumn, this->methodsTimeColumn
            });
            this->methodsConductedData->Dock = System::Windows::Forms::DockStyle::Fill;
            this->methodsConductedData->Location = System::Drawing::Point(3, 3);
            this->methodsConductedData->Name = L"methodsConductedData";
            this->methodsConductedData->ReadOnly = true;
            this->methodsConductedData->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToDisplayedHeaders;
            this->methodsConductedData->Size = System::Drawing::Size(370, 413);
            this->methodsConductedData->TabIndex = 0;
            this->methodsConductedData->CellContentDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &ConductedStatsUI::methodsConductedData_CellContentDoubleClick);
            this->methodsConductedData->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &ConductedStatsUI::methodsConductedData_ColumnHeaderMouseClick);
            this->methodsConductedData->VisibleChanged += gcnew System::EventHandler(this, &ConductedStatsUI::TabChangedMethodsConducted);
            // 
            // ordersConductedTab
            // 
            ordersConductedTab->Controls->Add(this->ordersConductedData);
            ordersConductedTab->Location = System::Drawing::Point(4, 22);
            ordersConductedTab->Name = L"ordersConductedTab";
            ordersConductedTab->Padding = System::Windows::Forms::Padding(3);
            ordersConductedTab->Size = System::Drawing::Size(466, 419);
            ordersConductedTab->TabIndex = 1;
            ordersConductedTab->Text = L"Stages Conducted";
            ordersConductedTab->UseVisualStyleBackColor = true;
            // 
            // ordersConductedData
            // 
            this->ordersConductedData->AllowUserToAddRows = false;
            this->ordersConductedData->AllowUserToDeleteRows = false;
            this->ordersConductedData->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::DisplayedCells;
            this->ordersConductedData->AutoSizeRowsMode = System::Windows::Forms::DataGridViewAutoSizeRowsMode::DisplayedCells;
            this->ordersConductedData->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->ordersConductedData->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(3) {
                this->OrdersConductedColumn,
                    this->stagesChangesColumn, this->stagesTimeColumn
            });
            this->ordersConductedData->Dock = System::Windows::Forms::DockStyle::Fill;
            this->ordersConductedData->Location = System::Drawing::Point(3, 3);
            this->ordersConductedData->Name = L"ordersConductedData";
            this->ordersConductedData->ReadOnly = true;
            this->ordersConductedData->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToDisplayedHeaders;
            this->ordersConductedData->Size = System::Drawing::Size(460, 413);
            this->ordersConductedData->TabIndex = 1;
            this->ordersConductedData->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &ConductedStatsUI::ordersConductedData_ColumnHeaderMouseClick);
            this->ordersConductedData->VisibleChanged += gcnew System::EventHandler(this, &ConductedStatsUI::TabChangedOrdersConducted);
            // 
            // toolStrip1
            // 
            toolStrip1->GripStyle = System::Windows::Forms::ToolStripGripStyle::Hidden;
            toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { filtersBtn });
            toolStrip1->Location = System::Drawing::Point(0, 0);
            toolStrip1->Name = L"toolStrip1";
            toolStrip1->Size = System::Drawing::Size(474, 25);
            toolStrip1->TabIndex = 3;
            toolStrip1->Text = L"toolStrip1";
            // 
            // filtersBtn
            // 
            filtersBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
            filtersBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"filtersBtn.Image")));
            filtersBtn->ImageTransparentColor = System::Drawing::Color::Magenta;
            filtersBtn->Name = L"filtersBtn";
            filtersBtn->Size = System::Drawing::Size(42, 22);
            filtersBtn->Text = L"&Filters";
            filtersBtn->Click += gcnew System::EventHandler(this, &ConductedStatsUI::filtersBtn_Click);
            // 
            // backgroundWorker
            // 
            this->backgroundWorker->WorkerReportsProgress = true;
            this->backgroundWorker->WorkerSupportsCancellation = true;
            this->backgroundWorker->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &ConductedStatsUI::backgroundWorker_DoWork);
            this->backgroundWorker->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &ConductedStatsUI::backgroundWorker_ProgressChanged);
            this->backgroundWorker->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &ConductedStatsUI::backgroundWorker_RunWorkerCompleted);
            // 
            // MethodId
            // 
            this->MethodId->HeaderText = L"Id";
            this->MethodId->Name = L"MethodId";
            this->MethodId->ReadOnly = true;
            this->MethodId->Visible = false;
            this->MethodId->Width = 41;
            // 
            // MethodNameColumn
            // 
            this->MethodNameColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
            this->MethodNameColumn->HeaderText = L"Method";
            this->MethodNameColumn->Name = L"MethodNameColumn";
            this->MethodNameColumn->ReadOnly = true;
            // 
            // methodsConductedColumn
            // 
            this->methodsConductedColumn->HeaderText = L"Count";
            this->methodsConductedColumn->Name = L"methodsConductedColumn";
            this->methodsConductedColumn->ReadOnly = true;
            this->methodsConductedColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->methodsConductedColumn->Width = 60;
            // 
            // methodsChangesColumn
            // 
            this->methodsChangesColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            dataGridViewCellStyle7->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->methodsChangesColumn->DefaultCellStyle = dataGridViewCellStyle7;
            this->methodsChangesColumn->HeaderText = L"Changes";
            this->methodsChangesColumn->Name = L"methodsChangesColumn";
            this->methodsChangesColumn->ReadOnly = true;
            this->methodsChangesColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->methodsChangesColumn->Width = 74;
            // 
            // methodsTimeColumn
            // 
            this->methodsTimeColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            dataGridViewCellStyle8->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->methodsTimeColumn->DefaultCellStyle = dataGridViewCellStyle8;
            this->methodsTimeColumn->HeaderText = L"Time";
            this->methodsTimeColumn->Name = L"methodsTimeColumn";
            this->methodsTimeColumn->ReadOnly = true;
            this->methodsTimeColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->methodsTimeColumn->Width = 55;
            // 
            // OrdersConductedColumn
            // 
            this->OrdersConductedColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            this->OrdersConductedColumn->HeaderText = L"Count";
            this->OrdersConductedColumn->Name = L"OrdersConductedColumn";
            this->OrdersConductedColumn->ReadOnly = true;
            this->OrdersConductedColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->OrdersConductedColumn->Width = 60;
            // 
            // stagesChangesColumn
            // 
            this->stagesChangesColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            dataGridViewCellStyle5->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->stagesChangesColumn->DefaultCellStyle = dataGridViewCellStyle5;
            this->stagesChangesColumn->HeaderText = L"Changes";
            this->stagesChangesColumn->Name = L"stagesChangesColumn";
            this->stagesChangesColumn->ReadOnly = true;
            this->stagesChangesColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->stagesChangesColumn->Width = 74;
            // 
            // stagesTimeColumn
            // 
            this->stagesTimeColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            dataGridViewCellStyle6->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->stagesTimeColumn->DefaultCellStyle = dataGridViewCellStyle6;
            this->stagesTimeColumn->HeaderText = L"Time";
            this->stagesTimeColumn->Name = L"stagesTimeColumn";
            this->stagesTimeColumn->ReadOnly = true;
            this->stagesTimeColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
            this->stagesTimeColumn->Width = 55;
            // 
            // ConductedStatsUI
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(474, 492);
            this->Controls->Add(tabControl1);
            this->Controls->Add(toolStrip1);
            this->Controls->Add(statusStrip1);
            this->Name = L"ConductedStatsUI";
            this->Text = L"Conducted statistics";
            this->Load += gcnew System::EventHandler(this, &ConductedStatsUI::ConductedStatsUI_Load);
            statusStrip1->ResumeLayout(false);
            statusStrip1->PerformLayout();
            tabControl1->ResumeLayout(false);
            methodsConductedTab->ResumeLayout(false);
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->methodsConductedData))->EndInit();
            ordersConductedTab->ResumeLayout(false);
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ordersConductedData))->EndInit();
            toolStrip1->ResumeLayout(false);
            toolStrip1->PerformLayout();
            this->ResumeLayout(false);
            this->PerformLayout();

        }

        ref class ConductedStatsUIArgs
        {
        public:
            ConductedStatsUI::TState newState;
            Duco::ObjectId*          ringerId;
        };
    };
}
