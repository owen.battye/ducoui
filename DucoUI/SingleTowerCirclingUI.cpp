#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"
#include "GenericTablePrintUtil.h"

#include "SingleTowerCirclingUI.h"

#include <Tower.h>
#include "DucoUtils.h"
#include "DatabaseGenUtils.h"
#include "SystemDefaultIcon.h"
#include "DucoUIUtils.h"
#include <RingingDatabase.h>
#include "FiltersUI.h"
#include "SoundUtils.h"
#include "DucoTableSorter.h"
#include <Method.h>
#include <RingingDatabase.h>
#include <DucoConfiguration.h>
#include <PealDatabase.h>
#include <Ring.h>
#include <Ringer.h>
#include "DucoWindowState.h"
#include "ProgressWrapper.h"
#include "RingerUI.h"
#include "DucoCommon.h"
#include <StatisticFilters.h>
#include <DatabaseSettings.h>

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Drawing;
using namespace System::Drawing::Printing;
using namespace System::Windows::Forms;

const unsigned int KDefaultNumberOfColumns = 6;

SingleTowerCirclingUI::SingleTowerCirclingUI(DatabaseManager^ theDatabase, const Duco::ObjectId& theTowerId, System::Boolean theIncludeLinked)
    : database(theDatabase), populatingView(false), includeLinkedTowers(theIncludeLinked)
{
    towerId = new Duco::ObjectId(theTowerId);
    InitializeComponent();
    allRingIds = gcnew System::Collections::Generic::List<System::Int16>;
    allMethodIds = gcnew System::Collections::Generic::List<System::Int16>;

    Tower currentTower;
    if (database->FindTower(*towerId, currentTower, false))
    {
        this->Text += DucoUtils::ConvertString(currentTower.FullName());
    }
}

SingleTowerCirclingUI::!SingleTowerCirclingUI()
{
    delete towerId;
    database->RemoveObserver(this);
}

SingleTowerCirclingUI::~SingleTowerCirclingUI()
{
    this->!SingleTowerCirclingUI();
    if (components)
    {
        delete components;
    }
}

void
SingleTowerCirclingUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    switch (type)
    {
        case TObjectType::EOrderName:
            populatingView = true;
            DatabaseGenUtils::GenerateStageOptions(stageSelector, database, true);
            populatingView = false;
            break;

        case TObjectType::ETower:
            if (id != *towerId)
                return;
            populatingView = true;
            DatabaseGenUtils::GenerateRingOptions(ringSelector, nullptr, allRingIds, database, *towerId, -1, -1, true);
            populatingView = false;

        case TObjectType::EPeal:
            StartGenerator();
            break;
    }
}

System::Void
SingleTowerCirclingUI::SingleTowerCirclingUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    populatingView = true;
    DatabaseGenUtils::GenerateStageOptions(stageSelector, database, true);
    DatabaseGenUtils::GenerateRingOptions(ringSelector, nullptr, allRingIds, database, *towerId, -1, -1, true);
    DatabaseGenUtils::GenerateMethodOptions(allMethodIds, methodSelector, database, true);
    database->AddObserver(this);
    populatingView = false;
    StartGenerator();
}

System::Void
SingleTowerCirclingUI::methodSelector_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    StartGenerator();
}

System::Void
SingleTowerCirclingUI::ringSelector_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    StartGenerator();
}

System::Void
SingleTowerCirclingUI::stageSelector_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    StartGenerator();
}

System::Void 
SingleTowerCirclingUI::conducted_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    StartGenerator();
}

System::Void
SingleTowerCirclingUI::backgroundWorker1_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    progressBar->Value = e->ProgressPercentage;
    if (e->UserState != nullptr)
    {
        DataGridViewRow^ newRow = (DataGridViewRow^)e->UserState;
        dataGridView->Rows->Add(newRow);
    }
}

System::Void
SingleTowerCirclingUI::backgroundWorker1_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    if (restartGenerator)
    {
        StartGenerator();
    }
    else
    {
        progressBar->Value = 0;
        DucoTableSorter^ sorter = gcnew DucoTableSorter(true, EString);
        sorter->AddSortObject(ECount, false, 3);
        sorter->AddSortObject(ECount, false, 2);
        sorter->AddSortObject(EString, true, 1);
        dataGridView->Sort(sorter);
        stageSelector->Enabled = true;
        conducted->Enabled = true;
        ringSelector->Enabled = true;

        DataGridViewColumn^ newColumn = dataGridView->Columns[3];
        newColumn->HeaderCell->SortGlyphDirection = SortOrder::Descending;

    }
}

System::Void
SingleTowerCirclingUI::backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
    BackgroundWorker^ worker = dynamic_cast<BackgroundWorker^>(sender);
    if ( worker->CancellationPending )
    {
        e->Cancel = true;
        return;
    }

    SingleTowerCirclingUIArgs^ args = (SingleTowerCirclingUIArgs^)e->Argument;
    Duco::StatisticFilters filters (database->Database());
    filters.SetStage(args->orderNumber != -1, args->orderNumber);
    filters.SetMethod(args->methodId->ValidId(), *args->methodId);
    filters.SetIncludeLinkedTowers(includeLinkedTowers);
    filters.SetTower(true, *towerId);
    filters.SetRing(args->ringId->ValidId(), *args->ringId);

    std::set<Duco::ObjectId> ringersInTower;
    database->Database().PealsDatabase().GetRingersInTower(*args->towerId, ringersInTower);

    double noOfRingers = double(ringersInTower.size());
    bool ignoreRingersWithOnePeal = false;
    if (database->Config().DisableFeaturesForPerformance(ringersInTower.size()) && *args->ringId == -1)
    {
        ignoreRingersWithOnePeal = true;
    }
    double count (0);
    std::set<Duco::ObjectId>::const_iterator it = ringersInTower.begin();
    while (it != ringersInTower.end() && !worker->CancellationPending)
    {
        std::map<unsigned int, Duco::CirclingData> sortedBellPealCounts;
        size_t numberOfTimesCircled (0);
        Duco::PealLengthInfo noOfPeals;
        PerformanceDate firstCircledDate;
        size_t daysToFirstCircle = 0;
        if (args->conducted)
        {
            filters.SetConductor(true, *it);
        }
        else
        {
            filters.SetRinger(true, *it);
        }
        database->Database().GetTowerCircling(filters, sortedBellPealCounts, noOfPeals, numberOfTimesCircled, firstCircledDate, daysToFirstCircle);

        int progressPercentage = int((++count / noOfRingers)*double(100));
        if (noOfPeals.TotalPeals() <= 0 || (ignoreRingersWithOnePeal && noOfPeals.TotalPeals() <= 1))
        {
            worker->ReportProgress(progressPercentage);
        }
        else
        {
            // Add tower name, and peal totals here.
            DataGridViewRow^ newRow = gcnew DataGridViewRow();
            //newRow->HeaderCell->Value = DucoUtils::FormatIntToString(*it);
            DataGridViewCellCollection^ theCells = newRow->Cells;

            DataGridViewTextBoxCell^ idCell = gcnew DataGridViewTextBoxCell();
            idCell->Value = DucoUtils::ConvertString(*it);
            theCells->Add(idCell);

            DataGridViewTextBoxCell^ ringerNameCell = gcnew DataGridViewTextBoxCell();
            Ringer theRinger;
            if (database->FindRinger(*it, theRinger, false))
            {
                ringerNameCell->Value = DucoUtils::ConvertString(theRinger.FullName(database->Settings().LastNameFirst()));
            }
            theCells->Add(ringerNameCell);

            DataGridViewTextBoxCell^ circledCountCell = gcnew DataGridViewTextBoxCell();
            circledCountCell->Value = Convert::ToString(numberOfTimesCircled);
            theCells->Add(circledCountCell);

            DataGridViewTextBoxCell^ pealCountCell = gcnew DataGridViewTextBoxCell();
            pealCountCell->Value = Convert::ToString(noOfPeals.TotalPeals());
            theCells->Add(pealCountCell);

            DataGridViewTextBoxCell^ firstCircledDateCell = gcnew DataGridViewTextBoxCell();
            theCells->Add(firstCircledDateCell);

            DataGridViewTextBoxCell^ numberOfDaysCell = gcnew DataGridViewTextBoxCell();
            theCells->Add(numberOfDaysCell);

            bool circled (true);
            std::map<unsigned int, Duco::CirclingData>::const_iterator it2 = sortedBellPealCounts.begin();
            while (it2 != sortedBellPealCounts.end())
            {
                if (it2->second.PealCount() <= 0)
                    circled = false;
                DataGridViewTextBoxCell^ bellCell = gcnew DataGridViewTextBoxCell();
                bellCell->Value = Convert::ToString(it2->second.PealCount());
                theCells->Add(bellCell);
                ++it2;
            }

            if (circled)
            {
                firstCircledDateCell->Value = DucoUtils::ConvertString(firstCircledDate.Str());
                numberOfDaysCell->Value = DaysToString(daysToFirstCircle);
            }
        
            worker->ReportProgress(progressPercentage, newRow);
        }

        ++it;
    }
    delete args->towerId;
    delete args->ringId;
}

System::String^
SingleTowerCirclingUI::DaysToString(UInt64 days)
{
    UInt64 years = days / 365;
    UInt64 remainingDays = days - (years * 365);
    System::String^ dayString = "day";
    System::String^ yearString = "year";

    if (remainingDays != 1)
    {
        dayString += L"s";
    }
    if (years == 0)
    {
        return String::Format("{0} {1}", remainingDays, dayString);
    }
    if (years != 1)
    {
        yearString += L"s";
    }
    if (remainingDays == 0)
    {
        return String::Format("{0} {1}", years, yearString);
    }

    return String::Format("{0} {1} and {2} {3}", years, yearString, remainingDays, dayString);
}

System::Void 
SingleTowerCirclingUI::StartGenerator()
{
    if (populatingView)
        return;

    if (backgroundWorker1->IsBusy)
    {
        restartGenerator = true;
        backgroundWorker1->CancelAsync();
        return;
    }

    stageSelector->Enabled = false;
    conducted->Enabled = false;
    ringSelector->Enabled = false;
    ResetAllSortGlyphs();
    dataGridView->Rows->Clear();

    restartGenerator = false;

    Duco::ObjectId ringId = DatabaseGenUtils::FindId(allRingIds, ringSelector->SelectedIndex);
    CheckEnoughColumnsExist(ringId);

    Duco::ObjectId methodId = DatabaseGenUtils::FindId(allMethodIds, methodSelector->SelectedIndex);

    unsigned int orderNumber (0);
    bool isDualOrder (false);
    if (!database->FindOrderNumber(stageSelector->Text, orderNumber, isDualOrder))
    {
        orderNumber = -1;
    }

    SingleTowerCirclingUIArgs^ args = gcnew SingleTowerCirclingUIArgs;
    args->towerId = new Duco::ObjectId(*towerId);
    args->methodId = new Duco::ObjectId(methodId);
    args->orderNumber = orderNumber;
    args->conducted = conducted->Checked;
    args->ringId = new Duco::ObjectId(ringId);
    backgroundWorker1->RunWorkerAsync(args);
}

System::Void
SingleTowerCirclingUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void
SingleTowerCirclingUI::printBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    DucoUIUtils::Print(this, sender, gcnew System::Drawing::Printing::PrintPageEventHandler( this, &SingleTowerCirclingUI::printCircling ), database->PerformanceString(false) + "s summary", database->Database().Settings().UsePrintPreview(), printLandscape->Checked);
}

System::Void
SingleTowerCirclingUI::printCircling(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args)
{
    if (printingUtil == nullptr)
    {
        String^ titlePrintBuffer = "Ringers at ";
        if (conducted->Checked)
            titlePrintBuffer = "Conductors at ";

        String^ secondaryTitle = "";
        Duco::ObjectId ringId = DatabaseGenUtils::FindId(allRingIds, ringSelector->SelectedIndex);
        if (ringId.ValidId())
        {
            Duco::Tower currentTower;
            if (database->FindTower(*towerId, currentTower, false))
            {
                const Ring* const currentRing = currentTower.FindRing(ringId);
                if (currentRing != NULL)
                {
                    secondaryTitle = "On ring ";
                    secondaryTitle += DucoUtils::ConvertString(currentRing->Name());
                }
            }
        }
        else
        {
            secondaryTitle = "On any ring";
        }

        unsigned int orderNumber(0);
        bool isDualOrder(false);
        if (!database->FindOrderNumber(stageSelector->Text, orderNumber, isDualOrder))
        {
            orderNumber = -1;
        }
        if (orderNumber != -1)
        {
            secondaryTitle += " and of ";
            secondaryTitle += stageSelector->Text;
            if (isDualOrder)
            {
                String^ otherNumber = "";
                if (database->FindOrderName(orderNumber-1, otherNumber))
                {
                    secondaryTitle += " (and " + otherNumber + ")";
                }
            }
        }

        Duco::ObjectId methodId = DatabaseGenUtils::FindId(allMethodIds, methodSelector->SelectedIndex);
        if (methodId.ValidId())
        {
            Duco::Method currentMethod;
            if (database->FindMethod(methodId, currentMethod, false))
            {
                secondaryTitle = "and in method ";
                secondaryTitle += DucoUtils::ConvertString(currentMethod.FullName(database->Database()));
            }
        }

        printingUtil = gcnew GenericTablePrintUtil(database, args, titlePrintBuffer);
        printingUtil->SetObjects(dataGridView, *towerId, TObjectType::ETower, secondaryTitle, 6, dataGridView->Columns->Count);
    }

    PrintDocument^ printDoc = static_cast<PrintDocument^>(sender);
    printingUtil->printObject(args, printDoc->PrinterSettings);
    if (!args->HasMorePages)
    {
        printingUtil = nullptr;
    }
}

System::Void
SingleTowerCirclingUI::dataGridView_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    try
    {
        System::Windows::Forms::DataGridView^ theDataView = static_cast<System::Windows::Forms::DataGridView^>(sender);
        unsigned int rowIndex = e->RowIndex;
        DataGridViewRow^ currentRow = theDataView->Rows[rowIndex];
        unsigned int ringerId = Convert::ToInt16(currentRow->Cells[0]->Value);
        RingerUI::ShowRinger(database, this->MdiParent, ringerId);
    }
    catch (Exception^)
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void 
SingleTowerCirclingUI::CheckEnoughColumnsExist(const Duco::ObjectId& ringId)
{
    while (dataGridView->ColumnCount > KDefaultNumberOfColumns)
    {
        dataGridView->Columns->RemoveAt(dataGridView->ColumnCount-1);
    }

    Duco::Tower theTower;
    if (!database->FindTower(*towerId, theTower, false))
        return;

    std::vector<std::wstring> bellNames;
    theTower.AllBellNames(bellNames, ringId);

    unsigned int columnCount(KDefaultNumberOfColumns+1);
    std::vector<std::wstring>::const_iterator it = bellNames.begin();
    while (it != bellNames.end())
    {
        String^ newColumnName = String::Format("Bell{0:D}", columnCount);
        String^ newColumnTitle = DucoUtils::ConvertString(*it);
        int columnId = dataGridView->Columns->Add(newColumnName, newColumnTitle);
        dataGridView->Columns[columnId]->Width=30;
        dataGridView->Columns[columnId]->AutoSizeMode = DataGridViewAutoSizeColumnMode::None;
        dataGridView->Columns[columnId]->SortMode = DataGridViewColumnSortMode::Programmatic;
        ++columnCount;
        ++it;
    }
}

System::Void
SingleTowerCirclingUI::ResetAllSortGlyphs()
{
    ringerId->HeaderCell->SortGlyphDirection = SortOrder::None;
    ringerName->HeaderCell->SortGlyphDirection = SortOrder::None;
    pealCount->HeaderCell->SortGlyphDirection = SortOrder::None;
    circledCount->HeaderCell->SortGlyphDirection = SortOrder::None;
    circledDate->HeaderCell->SortGlyphDirection = SortOrder::None;
    numberOfDaysColumn->HeaderCell->SortGlyphDirection = SortOrder::None;

    for (int i (KDefaultNumberOfColumns); i < dataGridView->ColumnCount; ++i)
    {
        dataGridView->Columns[i]->HeaderCell->SortGlyphDirection = SortOrder::None;
    }
}

System::Void
SingleTowerCirclingUI::circledTowerData_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
    this->Cursor = Cursors::WaitCursor;

    DataGridViewColumn^ newColumn = dataGridView->Columns[e->ColumnIndex];
    if (newColumn->SortMode == System::Windows::Forms::DataGridViewColumnSortMode::Automatic)
    {
        ResetAllSortGlyphs();
        return;
    }
    DataGridViewColumn^ oldColumn = dataGridView->SortedColumn;
    if (oldColumn != nullptr && oldColumn != newColumn)
    {
        oldColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
    }

    SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;
    if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
        newSortOrder = SortOrder::Ascending;
    else
        newSortOrder = SortOrder::Descending;
    bool ascendingSortOrder = newSortOrder == SortOrder::Ascending;
    ResetAllSortGlyphs();

    newColumn->HeaderCell->SortGlyphDirection = newSortOrder;

    DucoTableSorter^ sorter = gcnew DucoTableSorter(true, EString);

    if (e->ColumnIndex == 4)
    {
        sorter->AddSortObject(EDate, ascendingSortOrder, e->ColumnIndex);
    }
    else
    {
        sorter->AddSortObject(ECount, ascendingSortOrder, e->ColumnIndex);
    }
    sorter->AddSortObject(EString, true, 1);
    dataGridView->Sort(sorter);

    this->Cursor = Cursors::Arrow;
}

#pragma region Windows Form Designer generated code
void SingleTowerCirclingUI::InitializeComponent(void)
{
    System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
    System::Windows::Forms::Label^ methodLbl;
    System::Windows::Forms::Button^ printBtn;
    System::Windows::Forms::Button^ closeBtn;
    System::Windows::Forms::StatusStrip^ statusStrip1;
    this->settingsPanel = (gcnew System::Windows::Forms::Panel());
    this->methodSelector = (gcnew System::Windows::Forms::ComboBox());
    this->stageSelector = (gcnew System::Windows::Forms::ComboBox());
    this->ringSelector = (gcnew System::Windows::Forms::ComboBox());
    this->conducted = (gcnew System::Windows::Forms::CheckBox());
    this->dataGridView = (gcnew System::Windows::Forms::DataGridView());
    this->buttonPanel = (gcnew System::Windows::Forms::Panel());
    this->printLandscape = (gcnew System::Windows::Forms::CheckBox());
    this->progressBar = (gcnew System::Windows::Forms::ToolStripProgressBar());
    this->backgroundWorker1 = (gcnew System::ComponentModel::BackgroundWorker());
    this->ringerId = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->ringerName = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->circledCount = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->pealCount = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->circledDate = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->numberOfDaysColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
    methodLbl = (gcnew System::Windows::Forms::Label());
    printBtn = (gcnew System::Windows::Forms::Button());
    closeBtn = (gcnew System::Windows::Forms::Button());
    statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
    tableLayoutPanel1->SuspendLayout();
    this->settingsPanel->SuspendLayout();
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->BeginInit();
    this->buttonPanel->SuspendLayout();
    statusStrip1->SuspendLayout();
    this->SuspendLayout();
    // 
    // tableLayoutPanel1
    // 
    tableLayoutPanel1->ColumnCount = 1;
    tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
    tableLayoutPanel1->Controls->Add(this->settingsPanel, 0, 0);
    tableLayoutPanel1->Controls->Add(this->dataGridView, 0, 1);
    tableLayoutPanel1->Controls->Add(this->buttonPanel, 0, 2);
    tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
    tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
    tableLayoutPanel1->Name = L"tableLayoutPanel1";
    tableLayoutPanel1->RowCount = 4;
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 62)));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 34)));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
    tableLayoutPanel1->Size = System::Drawing::Size(731, 495);
    tableLayoutPanel1->TabIndex = 0;
    // 
    // settingsPanel
    // 
    this->settingsPanel->Controls->Add(methodLbl);
    this->settingsPanel->Controls->Add(this->methodSelector);
    this->settingsPanel->Controls->Add(this->stageSelector);
    this->settingsPanel->Controls->Add(this->ringSelector);
    this->settingsPanel->Controls->Add(this->conducted);
    this->settingsPanel->Dock = System::Windows::Forms::DockStyle::Fill;
    this->settingsPanel->Location = System::Drawing::Point(3, 3);
    this->settingsPanel->Name = L"settingsPanel";
    this->settingsPanel->Size = System::Drawing::Size(725, 56);
    this->settingsPanel->TabIndex = 2;
    // 
    // methodLbl
    // 
    methodLbl->AutoSize = true;
    methodLbl->Location = System::Drawing::Point(221, 33);
    methodLbl->Name = L"methodLbl";
    methodLbl->Size = System::Drawing::Size(46, 13);
    methodLbl->TabIndex = 39;
    methodLbl->Text = L"Method:";
    // 
    // methodSelector
    // 
    this->methodSelector->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
    this->methodSelector->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
    this->methodSelector->FormattingEnabled = true;
    this->methodSelector->Location = System::Drawing::Point(274, 30);
    this->methodSelector->Name = L"methodSelector";
    this->methodSelector->Size = System::Drawing::Size(276, 21);
    this->methodSelector->TabIndex = 38;
    this->methodSelector->SelectedIndexChanged += gcnew System::EventHandler(this, &SingleTowerCirclingUI::methodSelector_SelectedIndexChanged);
    // 
    // stageSelector
    // 
    this->stageSelector->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
    this->stageSelector->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
    this->stageSelector->FormattingEnabled = true;
    this->stageSelector->Location = System::Drawing::Point(3, 3);
    this->stageSelector->Name = L"stageSelector";
    this->stageSelector->Size = System::Drawing::Size(182, 21);
    this->stageSelector->TabIndex = 37;
    this->stageSelector->SelectedIndexChanged += gcnew System::EventHandler(this, &SingleTowerCirclingUI::stageSelector_SelectedIndexChanged);
    // 
    // ringSelector
    // 
    this->ringSelector->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
    this->ringSelector->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
    this->ringSelector->FormattingEnabled = true;
    this->ringSelector->Location = System::Drawing::Point(274, 3);
    this->ringSelector->Name = L"ringSelector";
    this->ringSelector->Size = System::Drawing::Size(182, 21);
    this->ringSelector->TabIndex = 37;
    this->ringSelector->SelectedIndexChanged += gcnew System::EventHandler(this, &SingleTowerCirclingUI::ringSelector_SelectedIndexChanged);
    // 
    // conducted
    // 
    this->conducted->AutoSize = true;
    this->conducted->Location = System::Drawing::Point(3, 30);
    this->conducted->Name = L"conducted";
    this->conducted->Size = System::Drawing::Size(78, 17);
    this->conducted->TabIndex = 5;
    this->conducted->Text = L"Conducted";
    this->conducted->UseVisualStyleBackColor = true;
    this->conducted->CheckedChanged += gcnew System::EventHandler(this, &SingleTowerCirclingUI::conducted_CheckedChanged);
    // 
    // dataGridView
    // 
    this->dataGridView->AllowUserToAddRows = false;
    this->dataGridView->AllowUserToDeleteRows = false;
    this->dataGridView->ClipboardCopyMode = System::Windows::Forms::DataGridViewClipboardCopyMode::EnableAlwaysIncludeHeaderText;
    this->dataGridView->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
    this->dataGridView->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(6) {
        this->ringerId,
            this->ringerName, this->circledCount, this->pealCount, this->circledDate, this->numberOfDaysColumn
    });
    this->dataGridView->Dock = System::Windows::Forms::DockStyle::Fill;
    this->dataGridView->Location = System::Drawing::Point(3, 65);
    this->dataGridView->Name = L"dataGridView";
    this->dataGridView->ReadOnly = true;
    this->dataGridView->Size = System::Drawing::Size(725, 373);
    this->dataGridView->TabIndex = 1;
    this->dataGridView->CellContentDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &SingleTowerCirclingUI::dataGridView_CellContentDoubleClick);
    this->dataGridView->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &SingleTowerCirclingUI::circledTowerData_ColumnHeaderMouseClick);
    // 
    // buttonPanel
    // 
    this->buttonPanel->Controls->Add(this->printLandscape);
    this->buttonPanel->Controls->Add(printBtn);
    this->buttonPanel->Controls->Add(closeBtn);
    this->buttonPanel->Dock = System::Windows::Forms::DockStyle::Fill;
    this->buttonPanel->Location = System::Drawing::Point(3, 444);
    this->buttonPanel->Name = L"buttonPanel";
    this->buttonPanel->Size = System::Drawing::Size(725, 28);
    this->buttonPanel->TabIndex = 2;
    // 
    // printLandscape
    // 
    this->printLandscape->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
    this->printLandscape->AutoSize = true;
    this->printLandscape->Location = System::Drawing::Point(446, 7);
    this->printLandscape->Name = L"printLandscape";
    this->printLandscape->Size = System::Drawing::Size(114, 17);
    this->printLandscape->TabIndex = 2;
    this->printLandscape->Text = L"Print in Landscape";
    this->printLandscape->UseVisualStyleBackColor = true;
    // 
    // printBtn
    // 
    printBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
    printBtn->Location = System::Drawing::Point(566, 3);
    printBtn->Name = L"printBtn";
    printBtn->Size = System::Drawing::Size(75, 23);
    printBtn->TabIndex = 1;
    printBtn->Text = L"Print";
    printBtn->UseVisualStyleBackColor = true;
    printBtn->Click += gcnew System::EventHandler(this, &SingleTowerCirclingUI::printBtn_Click);
    // 
    // closeBtn
    // 
    closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
    closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
    closeBtn->Location = System::Drawing::Point(647, 3);
    closeBtn->Name = L"closeBtn";
    closeBtn->Size = System::Drawing::Size(75, 22);
    closeBtn->TabIndex = 0;
    closeBtn->Text = L"Close";
    closeBtn->UseVisualStyleBackColor = true;
    closeBtn->Click += gcnew System::EventHandler(this, &SingleTowerCirclingUI::closeBtn_Click);
    // 
    // statusStrip1
    // 
    statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->progressBar });
    statusStrip1->Location = System::Drawing::Point(0, 473);
    statusStrip1->Name = L"statusStrip1";
    statusStrip1->Size = System::Drawing::Size(731, 22);
    statusStrip1->TabIndex = 1;
    statusStrip1->Text = L"statusStrip1";
    // 
    // progressBar
    // 
    this->progressBar->Name = L"progressBar";
    this->progressBar->Size = System::Drawing::Size(100, 16);
    // 
    // backgroundWorker1
    // 
    this->backgroundWorker1->WorkerReportsProgress = true;
    this->backgroundWorker1->WorkerSupportsCancellation = true;
    this->backgroundWorker1->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &SingleTowerCirclingUI::backgroundWorker1_DoWork);
    this->backgroundWorker1->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &SingleTowerCirclingUI::backgroundWorker1_ProgressChanged);
    this->backgroundWorker1->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &SingleTowerCirclingUI::backgroundWorker1_RunWorkerCompleted);
    // 
    // ringerId
    // 
    this->ringerId->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
    this->ringerId->Frozen = true;
    this->ringerId->HeaderText = L"Id";
    this->ringerId->Name = L"ringerId";
    this->ringerId->ReadOnly = true;
    this->ringerId->Visible = false;
    // 
    // ringerName
    // 
    this->ringerName->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
    this->ringerName->Frozen = true;
    this->ringerName->HeaderText = L"Ringer";
    this->ringerName->Name = L"ringerName";
    this->ringerName->ReadOnly = true;
    this->ringerName->Width = 63;
    // 
    // circledCount
    // 
    this->circledCount->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
    this->circledCount->Frozen = true;
    this->circledCount->HeaderText = L"Circles";
    this->circledCount->Name = L"circledCount";
    this->circledCount->ReadOnly = true;
    this->circledCount->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
    this->circledCount->Width = 63;
    // 
    // pealCount
    // 
    this->pealCount->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
    this->pealCount->Frozen = true;
    this->pealCount->HeaderText = database->PerformanceString(false) + "s";
    this->pealCount->Name = L"pealCount";
    this->pealCount->ReadOnly = true;
    this->pealCount->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
    this->pealCount->Width = 58;
    // 
    // circledDate
    // 
    this->circledDate->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCellsExceptHeader;
    this->circledDate->Frozen = true;
    this->circledDate->HeaderText = L"First circled";
    this->circledDate->MinimumWidth = 50;
    this->circledDate->Name = L"circledDate";
    this->circledDate->ReadOnly = true;
    this->circledDate->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
    this->circledDate->Width = 50;
    // 
    // numberOfDaysColumn
    // 
    this->numberOfDaysColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
    this->numberOfDaysColumn->Frozen = true;
    this->numberOfDaysColumn->HeaderText = L"Days to circle";
    this->numberOfDaysColumn->Name = L"numberOfDaysColumn";
    this->numberOfDaysColumn->ReadOnly = true;
    this->numberOfDaysColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
    this->numberOfDaysColumn->Width = 69;
    // 
    // SingleTowerCirclingUI
    // 
    this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
    this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
    this->CancelButton = closeBtn;
    this->ClientSize = System::Drawing::Size(731, 495);
    this->Controls->Add(statusStrip1);
    this->Controls->Add(tableLayoutPanel1);
    this->Name = L"SingleTowerCirclingUI";
    this->Text = L"Ringers who have circled ";
    this->Load += gcnew System::EventHandler(this, &SingleTowerCirclingUI::SingleTowerCirclingUI_Load);
    tableLayoutPanel1->ResumeLayout(false);
    this->settingsPanel->ResumeLayout(false);
    this->settingsPanel->PerformLayout();
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->EndInit();
    this->buttonPanel->ResumeLayout(false);
    this->buttonPanel->PerformLayout();
    statusStrip1->ResumeLayout(false);
    statusStrip1->PerformLayout();
    this->ResumeLayout(false);
    this->PerformLayout();

}
#pragma endregion
