#pragma once

#include <string>

namespace DucoUI
{
    public ref class TableSorterByDate :  public System::Collections::IComparer
    {
    public:
        TableSorterByDate(bool newAscending, int newColumnId);
        virtual int Compare(System::Object^, System::Object^);

    protected:
        ~TableSorterByDate();
        void ConvertStringToDate(System::String^ dateStr, int& year, int& month, int& day);
        int Compare(System::Windows::Forms::DataGridViewRow^ firstRow, System::Windows::Forms::DataGridViewRow^ secondRow);
        int ConvertStrToMonth(const std::string& monthStr);

    private:
        int     columnId;
        bool    ascending;
    };
}
