#pragma once
#include "ProgressWrapper.h"

namespace Duco
{
    class ObjectId;
    class ExternalMethod;
    class MethodNotationDatabaseSingleImporter;
}

namespace DucoUI
{
    ref class DatabaseManager;

    public ref class ImportSingleMethod : public System::Windows::Forms::Form, public DucoUI::ProgressCallbackUI
    {
    public:
        ImportSingleMethod(DucoUI::DatabaseManager^ theDatabase, Duco::ObjectId& methodId, unsigned int order, System::String^ currentMethodName, System::Boolean importSingle);

        // from ProgressCallback
        virtual void Initialised();
        virtual void Step(int progressPercent);
        virtual void Complete();

    protected:
        !ImportSingleMethod();
        ~ImportSingleMethod();

    private:
        System::Void openFileBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void importBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void filter_TextChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void stageSelector_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void ImportSingleMethod_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void ImportSingleMethod_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e);
        System::Void methodDisplay_SelectionChanged(System::Object^  sender, System::EventArgs^  e);

        bool ImportMethod(System::Windows::Forms::DataGridViewRow^ theRow);
        System::Void AddMethod(const Duco::ExternalMethod& methodToAdd, double count, double noOfResults);
        System::Void StopProcessing();

        System::Void Restart();
        System::Void backgroundWorker_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void backgroundWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void backgroundWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);

    private:
        DucoUI::ProgressCallbackWrapper*            progressWrapper;
        System::ComponentModel::IContainer^         components;
        DucoUI::DatabaseManager^                    database;
        Duco::MethodNotationDatabaseSingleImporter* methodCollectionDatabase;
        System::Windows::Forms::ComboBox^           stageSelector;
        System::Windows::Forms::OpenFileDialog^     openFileDialog1;
        System::Windows::Forms::TextBox^            filter;
        System::String^                             databaseFilename;
        Duco::ObjectId&                             selectedMethodId;

        System::Windows::Forms::StatusStrip^                statusStrip1;
        System::Windows::Forms::Button^                     importBtn;
        System::Windows::Forms::Button^                     openFileBtn;
        System::Windows::Forms::ToolStripProgressBar^       progressBar;
        System::Windows::Forms::ToolStripStatusLabel^       progressLbl;
        System::Windows::Forms::DataGridView^               methodDisplay;
        System::ComponentModel::BackgroundWorker^           backgroundWorker;
        System::Windows::Forms::DataGridViewTextBoxColumn^  idColumn;
        System::Windows::Forms::DataGridViewTextBoxColumn^  nameColumn;
        System::Windows::Forms::DataGridViewTextBoxColumn^  notationColumn;
        System::Windows::Forms::ToolTip^  toolTip1;
        System::Boolean restart;
        System::Boolean importSingleMethod;

#pragma region Windows Form Designer generated code
        void InitializeComponent()
        {
            this->components = (gcnew System::ComponentModel::Container());
            System::Windows::Forms::Label^  stageLbl;
            System::Windows::Forms::Label^  filterLbl;
            System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
            this->filter = (gcnew System::Windows::Forms::TextBox());
            this->stageSelector = (gcnew System::Windows::Forms::ComboBox());
            this->openFileBtn = (gcnew System::Windows::Forms::Button());
            this->importBtn = (gcnew System::Windows::Forms::Button());
            this->methodDisplay = (gcnew System::Windows::Forms::DataGridView());
            this->idColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->nameColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->notationColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
            this->statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            this->progressBar = (gcnew System::Windows::Forms::ToolStripProgressBar());
            this->progressLbl = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->backgroundWorker = (gcnew System::ComponentModel::BackgroundWorker());
            this->toolTip1 = (gcnew System::Windows::Forms::ToolTip(this->components));
            stageLbl = (gcnew System::Windows::Forms::Label());
            filterLbl = (gcnew System::Windows::Forms::Label());
            tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            tableLayoutPanel1->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->methodDisplay))->BeginInit();
            this->statusStrip1->SuspendLayout();
            this->SuspendLayout();
            // 
            // stageLbl
            // 
            stageLbl->AutoSize = true;
            stageLbl->Location = System::Drawing::Point(3, 6);
            stageLbl->Margin = System::Windows::Forms::Padding(3, 6, 3, 0);
            stageLbl->Name = L"stageLbl";
            stageLbl->Size = System::Drawing::Size(72, 13);
            stageLbl->TabIndex = 0;
            stageLbl->Text = L"Choose stage";
            // 
            // filterLbl
            // 
            filterLbl->AutoSize = true;
            filterLbl->Location = System::Drawing::Point(3, 32);
            filterLbl->Margin = System::Windows::Forms::Padding(3, 6, 3, 0);
            filterLbl->Name = L"filterLbl";
            filterLbl->Size = System::Drawing::Size(29, 13);
            filterLbl->TabIndex = 3;
            filterLbl->Text = L"Filter";
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1->ColumnCount = 3;
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute, 101)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute, 78)));
            tableLayoutPanel1->Controls->Add(stageLbl, 0, 0);
            tableLayoutPanel1->Controls->Add(filterLbl, 0, 1);
            tableLayoutPanel1->Controls->Add(this->filter, 1, 1);
            tableLayoutPanel1->Controls->Add(this->stageSelector, 1, 0);
            tableLayoutPanel1->Controls->Add(this->openFileBtn, 2, 0);
            tableLayoutPanel1->Controls->Add(this->importBtn, 2, 3);
            tableLayoutPanel1->Controls->Add(this->methodDisplay, 0, 2);
            tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel1->Name = L"tableLayoutPanel1";
            tableLayoutPanel1->RowCount = 5;
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 26)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 25)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 26)));
            tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 22)));
            tableLayoutPanel1->Size = System::Drawing::Size(545, 316);
            tableLayoutPanel1->TabIndex = 6;
            // 
            // filter
            // 
            tableLayoutPanel1->SetColumnSpan(this->filter, 2);
            this->filter->Dock = System::Windows::Forms::DockStyle::Fill;
            this->filter->Location = System::Drawing::Point(101, 29);
            this->filter->Margin = System::Windows::Forms::Padding(0, 3, 3, 0);
            this->filter->Name = L"filter";
            this->filter->Size = System::Drawing::Size(441, 20);
            this->filter->TabIndex = 4;
            this->filter->TextChanged += gcnew System::EventHandler(this, &ImportSingleMethod::filter_TextChanged);
            // 
            // stageSelector
            // 
            this->stageSelector->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
            this->stageSelector->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
            this->stageSelector->Dock = System::Windows::Forms::DockStyle::Fill;
            this->stageSelector->FormattingEnabled = true;
            this->stageSelector->Location = System::Drawing::Point(101, 3);
            this->stageSelector->Margin = System::Windows::Forms::Padding(0, 3, 0, 0);
            this->stageSelector->Name = L"stageSelector";
            this->stageSelector->Size = System::Drawing::Size(366, 21);
            this->stageSelector->TabIndex = 1;
            this->stageSelector->SelectedIndexChanged += gcnew System::EventHandler(this, &ImportSingleMethod::stageSelector_SelectedIndexChanged);
            // 
            // openFileBtn
            // 
            this->openFileBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->openFileBtn->Location = System::Drawing::Point(468, 2);
            this->openFileBtn->Margin = System::Windows::Forms::Padding(0, 2, 2, 0);
            this->openFileBtn->Name = L"openFileBtn";
            this->openFileBtn->Size = System::Drawing::Size(75, 23);
            this->openFileBtn->TabIndex = 2;
            this->openFileBtn->Text = L"Choose file";
            this->toolTip1->SetToolTip(this->openFileBtn, L"Choose the file to import methods from (Download from the CC website)");
            this->openFileBtn->UseVisualStyleBackColor = true;
            this->openFileBtn->Click += gcnew System::EventHandler(this, &ImportSingleMethod::openFileBtn_Click);
            // 
            // importBtn
            // 
            this->importBtn->Location = System::Drawing::Point(467, 270);
            this->importBtn->Margin = System::Windows::Forms::Padding(0, 2, 2, 0);
            this->importBtn->Name = L"importBtn";
            this->importBtn->Size = System::Drawing::Size(75, 23);
            this->importBtn->TabIndex = 6;
            this->importBtn->Text = L"Import";
            this->toolTip1->SetToolTip(this->importBtn, L"Import the highlighted method or methods");
            this->importBtn->UseVisualStyleBackColor = true;
            this->importBtn->Click += gcnew System::EventHandler(this, &ImportSingleMethod::importBtn_Click);
            // 
            // methodDisplay
            // 
            this->methodDisplay->AllowUserToAddRows = false;
            this->methodDisplay->AllowUserToDeleteRows = false;
            this->methodDisplay->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::AllCells;
            this->methodDisplay->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->methodDisplay->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(3) {
                this->idColumn,
                    this->nameColumn, this->notationColumn
            });
            tableLayoutPanel1->SetColumnSpan(this->methodDisplay, 3);
            this->methodDisplay->Dock = System::Windows::Forms::DockStyle::Fill;
            this->methodDisplay->Location = System::Drawing::Point(3, 54);
            this->methodDisplay->Name = L"methodDisplay";
            this->methodDisplay->ReadOnly = true;
            this->methodDisplay->Size = System::Drawing::Size(539, 211);
            this->methodDisplay->TabIndex = 5;
            this->methodDisplay->SelectionChanged += gcnew System::EventHandler(this, &ImportSingleMethod::methodDisplay_SelectionChanged);
            // 
            // idColumn
            // 
            this->idColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->idColumn->HeaderText = L"Id";
            this->idColumn->Name = L"idColumn";
            this->idColumn->ReadOnly = true;
            this->idColumn->Visible = false;
            // 
            // nameColumn
            // 
            this->nameColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->nameColumn->HeaderText = L"Name";
            this->nameColumn->Name = L"nameColumn";
            this->nameColumn->ReadOnly = true;
            this->nameColumn->Width = 60;
            // 
            // notationColumn
            // 
            this->notationColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
            this->notationColumn->HeaderText = L"Place notation";
            this->notationColumn->Name = L"notationColumn";
            this->notationColumn->ReadOnly = true;
            // 
            // openFileDialog1
            // 
            this->openFileDialog1->DefaultExt = L"xml";
            this->openFileDialog1->Filter = L"XML files (*.xml)|*.xml";
            // 
            // statusStrip1
            // 
            this->statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) { this->progressBar, this->progressLbl });
            this->statusStrip1->Location = System::Drawing::Point(0, 294);
            this->statusStrip1->Name = L"statusStrip1";
            this->statusStrip1->Size = System::Drawing::Size(545, 22);
            this->statusStrip1->TabIndex = 7;
            this->statusStrip1->Text = L"statusStrip1";
            // 
            // progressBar
            // 
            this->progressBar->Name = L"progressBar";
            this->progressBar->Size = System::Drawing::Size(100, 16);
            this->progressBar->Step = 1;
            // 
            // progressLbl
            // 
            this->progressLbl->Name = L"progressLbl";
            this->progressLbl->Size = System::Drawing::Size(428, 17);
            this->progressLbl->Spring = true;
            this->progressLbl->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // backgroundWorker
            // 
            this->backgroundWorker->WorkerReportsProgress = true;
            this->backgroundWorker->WorkerSupportsCancellation = true;
            this->backgroundWorker->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &ImportSingleMethod::backgroundWorker_DoWork);
            this->backgroundWorker->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &ImportSingleMethod::backgroundWorker_ProgressChanged);
            this->backgroundWorker->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &ImportSingleMethod::backgroundWorker_RunWorkerCompleted);
            // 
            // ImportSingleMethod
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(545, 316);
            this->Controls->Add(this->statusStrip1);
            this->Controls->Add(tableLayoutPanel1);
            this->MinimumSize = System::Drawing::Size(471, 260);
            this->Name = L"ImportSingleMethod";
            this->Text = L"Import single method from library";
            this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &ImportSingleMethod::ImportSingleMethod_FormClosing);
            this->Load += gcnew System::EventHandler(this, &ImportSingleMethod::ImportSingleMethod_Load);
            tableLayoutPanel1->ResumeLayout(false);
            tableLayoutPanel1->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->methodDisplay))->EndInit();
            this->statusStrip1->ResumeLayout(false);
            this->statusStrip1->PerformLayout();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion
};
}
