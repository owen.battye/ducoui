#pragma once
namespace DucoUI
{
    /// <summary>
    /// Summary for PealTableUI
    /// </summary>
    public ref class PealTableUI : public System::Windows::Forms::Form, public DucoUI::RingingDatabaseObserver
    {
    public:
        PealTableUI(DucoUI::DatabaseManager^ theDatabase);
        static void ShowAllPeals(DucoUI::DatabaseManager^ theDatabase, System::Windows::Forms::Form^ parent);

        // from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

    protected:
        !PealTableUI();
        ~PealTableUI();
        System::Void dataGridView1_CellValueNeeded(System::Object^  sender, System::Windows::Forms::DataGridViewCellValueEventArgs^  e);
        System::Void dataGridView1_CellDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);
        System::Void dataGridView1_Scroll(System::Object^ sender, System::Windows::Forms::ScrollEventArgs^ e);

        System::Void SetCellStyle(Duco::Peal& foundPeal, int rowIndex, int columnIndex);
        System::Void PealTableUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void LoadData();

        System::Windows::Forms::DataGridView^  dataGridView1;
        DucoUI::DatabaseManager^ database;
        System::Boolean associationColumnUsed;
        System::Boolean timeColumnUsed;

        System::Collections::Generic::List<System::UInt64>^ pealIds;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ DateColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ AssociationColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ ChangesColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ MethodColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ towerOrHand;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ TowerColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ PealTimeColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ ConductorColumn;

    private:
        /// <summary>
        /// Required designer variable.
        /// </summary>
        System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        void InitializeComponent(void)
        {
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle5 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle6 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle7 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle8 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
            this->DateColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->AssociationColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->ChangesColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->MethodColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->towerOrHand = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->TowerColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->PealTimeColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->ConductorColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
            this->SuspendLayout();
            // 
            // dataGridView1
            // 
            this->dataGridView1->AllowUserToAddRows = false;
            this->dataGridView1->AllowUserToDeleteRows = false;
            this->dataGridView1->AllowUserToResizeRows = false;
            this->dataGridView1->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::DisplayedCells;
            this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->dataGridView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(8) {
                this->DateColumn,
                    this->AssociationColumn, this->ChangesColumn, this->MethodColumn, this->towerOrHand, this->TowerColumn, this->PealTimeColumn,
                    this->ConductorColumn
            });
            this->dataGridView1->Dock = System::Windows::Forms::DockStyle::Fill;
            this->dataGridView1->Location = System::Drawing::Point(0, 0);
            this->dataGridView1->Name = L"dataGridView1";
            this->dataGridView1->ReadOnly = true;
            this->dataGridView1->RowHeadersWidth = 70;
            this->dataGridView1->RowTemplate->ReadOnly = true;
            this->dataGridView1->Size = System::Drawing::Size(1078, 654);
            this->dataGridView1->TabIndex = 0;
            this->dataGridView1->VirtualMode = true;
            this->dataGridView1->CellDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &PealTableUI::dataGridView1_CellDoubleClick);
            this->dataGridView1->CellValueNeeded += gcnew System::Windows::Forms::DataGridViewCellValueEventHandler(this, &PealTableUI::dataGridView1_CellValueNeeded);
            this->dataGridView1->Scroll += gcnew System::Windows::Forms::ScrollEventHandler(this, &PealTableUI::dataGridView1_Scroll);
            // 
            // DateColumn
            // 
            this->DateColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
            dataGridViewCellStyle5->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->DateColumn->DefaultCellStyle = dataGridViewCellStyle5;
            this->DateColumn->HeaderText = L"Date";
            this->DateColumn->MinimumWidth = 80;
            this->DateColumn->Name = L"DateColumn";
            this->DateColumn->ReadOnly = true;
            this->DateColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->DateColumn->Width = 80;
            // 
            // AssociationColumn
            // 
            this->AssociationColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
            this->AssociationColumn->HeaderText = L"Association";
            this->AssociationColumn->MinimumWidth = 75;
            this->AssociationColumn->Name = L"AssociationColumn";
            this->AssociationColumn->ReadOnly = true;
            this->AssociationColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            // 
            // ChangesColumn
            // 
            this->ChangesColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
            dataGridViewCellStyle6->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
            this->ChangesColumn->DefaultCellStyle = dataGridViewCellStyle6;
            this->ChangesColumn->HeaderText = L"Changes";
            this->ChangesColumn->MinimumWidth = 35;
            this->ChangesColumn->Name = L"ChangesColumn";
            this->ChangesColumn->ReadOnly = true;
            this->ChangesColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->ChangesColumn->Width = 45;
            // 
            // MethodColumn
            // 
            this->MethodColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            this->MethodColumn->HeaderText = L"Method";
            this->MethodColumn->Name = L"MethodColumn";
            this->MethodColumn->ReadOnly = true;
            this->MethodColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->MethodColumn->Width = 49;
            // 
            // towerOrHand
            // 
            this->towerOrHand->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
            dataGridViewCellStyle7->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
            this->towerOrHand->DefaultCellStyle = dataGridViewCellStyle7;
            this->towerOrHand->HeaderText = L"T";
            this->towerOrHand->MaxInputLength = 1;
            this->towerOrHand->MinimumWidth = 20;
            this->towerOrHand->Name = L"towerOrHand";
            this->towerOrHand->ReadOnly = true;
            this->towerOrHand->Resizable = System::Windows::Forms::DataGridViewTriState::False;
            this->towerOrHand->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->towerOrHand->Width = 20;
            // 
            // TowerColumn
            // 
            this->TowerColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            dataGridViewCellStyle8->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
            this->TowerColumn->DefaultCellStyle = dataGridViewCellStyle8;
            this->TowerColumn->HeaderText = L"Tower";
            this->TowerColumn->Name = L"TowerColumn";
            this->TowerColumn->ReadOnly = true;
            this->TowerColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->TowerColumn->Width = 43;
            // 
            // PealTimeColumn
            // 
            this->PealTimeColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
            this->PealTimeColumn->HeaderText = L"Time";
            this->PealTimeColumn->Name = L"PealTimeColumn";
            this->PealTimeColumn->ReadOnly = true;
            this->PealTimeColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->PealTimeColumn->Width = 55;
            // 
            // ConductorColumn
            // 
            this->ConductorColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::DisplayedCells;
            this->ConductorColumn->HeaderText = L"Conductor";
            this->ConductorColumn->Name = L"ConductorColumn";
            this->ConductorColumn->ReadOnly = true;
            this->ConductorColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
            this->ConductorColumn->Width = 62;
            // 
            // PealTableUI
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(1078, 654);
            this->Controls->Add(this->dataGridView1);
            this->Name = L"PealTableUI";
            this->StartPosition = System::Windows::Forms::FormStartPosition::Manual;
            this->Text = L"All Performances";
            this->Load += gcnew System::EventHandler(this, &PealTableUI::PealTableUI_Load);
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
            this->ResumeLayout(false);

        }
#pragma endregion
    };
}
