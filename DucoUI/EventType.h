#pragma once

namespace DucoUI
{
enum TEventType
{
    EAdded,
    EUpdated,
    EDeleted,
    ECleared,
    EImportComplete
};

} // end namespace Duco
