#pragma once
namespace DucoUI
{

	public ref class ChangesCirclingUI : public System::Windows::Forms::Form, public DucoUI::RingingDatabaseObserver
	{
	public:
		ChangesCirclingUI(DucoUI::DatabaseManager^ theDatabase);

        // from RingingDatabaseObserver
    	virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

    protected:
		!ChangesCirclingUI();
        ~ChangesCirclingUI();
		void InitializeComponent();
        System::Void StartGenerator();

        System::Void ChangesCirclingUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void ChangesCirclingUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e);
        System::Void filtersBtn_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void backgroundWorker_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void backgroundWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void backgroundWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);

        System::Void dataGridView_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);

	private:
        bool                                            restart;
        DucoUI::DatabaseManager^                        database;
        Duco::StatisticFilters*                         filters;
        System::Windows::Forms::ToolStripProgressBar^   progressBar;
        System::Windows::Forms::DataGridView^           dataGridView;
        System::ComponentModel::BackgroundWorker^       backgroundWorker;

        System::Windows::Forms::ToolStripButton^        filtersBtn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ PealCount;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ changesColumn;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^ timeColumn;
           System::ComponentModel::Container^ components;
    };
}
