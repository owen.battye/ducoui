#include "TableSorterByDistance.h"

using namespace DucoUI;
using namespace System;
using namespace System::Windows::Forms;

TableSorterByDistance::TableSorterByDistance(bool newAscending, int newColumnId)
: TableSorterByFloat(newAscending, newColumnId)
{
}

TableSorterByDistance::~TableSorterByDistance()
{
}

float
TableSorterByDistance::ConvertValueToFloat(DataGridViewRow^ row, int cellNumber)
{
    float returnValue (0);
    try
    {
        Object^ object = nullptr;
        if (cellNumber != -1)
        {
            object = row->Cells[cellNumber]->Value;
        }
        else
        {
            object = row->HeaderCell->Value;
        }
        if (object != nullptr)
        {
            String^ objectStr = Convert::ToString(object);
            if (objectStr != nullptr && objectStr->Length > 0)
            {
                array<Char>^ validChars = {'0','1','2','3','4','5','6','7','8','9','.'};
                int lastIndex (objectStr->LastIndexOfAny(validChars));
                if ( lastIndex == (objectStr->Length - 1) )
                    returnValue = Convert::ToSingle(object);
                else
                {
                    String^ tempStr = objectStr->Substring(0, lastIndex+1);
                    returnValue = Convert::ToSingle(tempStr);
                }
            }
        }
    }
    catch (Exception^)
    {
        returnValue = 0;
    }
    return returnValue;
}
