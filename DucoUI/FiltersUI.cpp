#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"
#include <StatisticFilters.h>

#include "FiltersUI.h"

#include "DatabaseGenUtils.h"
#include "DucoUtils.h"
#include <RingingDatabase.h>
#include "SystemDefaultIcon.h"
#include <DatabaseSettings.h>
#include "ChooseRinger.h"

using namespace DucoUI;
using namespace Duco;
using namespace System;

FiltersUI::FiltersUI(Duco::StatisticFilters& newFilters, DucoUI::DatabaseManager^ theDatabase)
    : filters(newFilters), database(theDatabase), requireConductor(false), populating(false)
{
    InitializeComponent();

    ringerCache = RingerDisplayCache::CreateDisplayCacheWithRingers(database, true, true);
    conductorCache = RingerDisplayCache::CreateDisplayCacheWithConductors(database, true);
    allTowerIds = gcnew System::Collections::Generic::List<System::Int16>();
    allMethodIds = gcnew System::Collections::Generic::List<System::Int16>();
    allAssocaitionIds = gcnew System::Collections::Generic::List<System::Int16>();

    DatabaseGenUtils::GenerateAssociationOptions(associationEditor, allAssocaitionIds, database, KNoId, true);
    DatabaseGenUtils::GenerateTowerOptions(towerSelector, allTowerIds, database, KNoId, false, false, true);
    DatabaseGenUtils::GenerateMethodOptions(allMethodIds, methodSelector, database, true);
    DatabaseGenUtils::GenerateMethodTypeOptions(typeSelector, database);
    DatabaseGenUtils::GenerateStageOptions(stageSelector, database, true);
    ringerCache->PopulateControl(ringerSelector, false);
    conductorCache->PopulateControl(conductorSelector, false);
    Populate();
}

System::Void
FiltersUI::Populate()
{
    populating = true;
    includeWithdrawn->Checked = filters.IncludeWithdrawn();
    excludeValid->Checked = filters.ExcludeValid();
    towerBells->Checked = filters.IncludeTowerBell();
    handBells->Checked = filters.IncludeHandBell();
    excludeInvalidTimeOrChanges->Checked = filters.ExcludeInvalidTimeOrChanges();

    Duco::ObjectId id;
    ringerCheckbox->Checked = filters.Ringer(id);
    if (id.ValidId())
    {
        ringerSelector->SelectedIndex = ringerCache->FindIndexOrAdd(id);
    }
    else
    {
        ringerSelector->SelectedIndex = -1;
    }
    conductorCheckbox->Checked = filters.Conductor(id);
    linkedRingers->Checked = filters.IncludeLinkedRingers();
    if (id.ValidId())
    {
        conductorSelector->SelectedIndex = conductorCache->FindIndexOrAdd(id);
    }
    else
    {
        conductorSelector->SelectedIndex = -1;
    }
    towerCheckbox->Checked = filters.Tower(id);
    includeLinked->Checked = filters.IncludeLinkedTowers();
    if (id.ValidId())
    {
        towerSelector->SelectedIndex = DatabaseGenUtils::FindIndex(allTowerIds, id);
    }
    else
    {
        towerSelector->SelectedIndex = -1;
    }
    associationCheckbox->Checked = filters.Association(id);
    if (id.ValidId())
    {
        associationEditor->SelectedIndex = DatabaseGenUtils::FindIndex(allAssocaitionIds, id);
    }
    else
    {
        associationEditor->SelectedIndex = -1;
    }
    methodCheckbox->Checked = filters.Method(id);
    if (id.ValidId())
    {
        methodSelector->SelectedIndex = DatabaseGenUtils::FindIndex(allMethodIds, id);
    }
    else
    {
        methodSelector->SelectedIndex = -1;
    }
    std::wstring type;
    typeCheckbox->Checked = filters.Type(type);
    typeSelector->Text = DucoUtils::ConvertString(type);
    unsigned int stage = -1;
    stageCheckbox->Checked = filters.Stage(stage);
    System::String^ orderName = "";
    if (stage != -1 && database->FindOrderName(stage, orderName))
    {
        stageSelector->Text = orderName;
    }

    Duco::PerformanceDate date;
    startCheckBox->Checked = filters.StartDateEnabled(date);
    if (date.Valid())
    {
        this->startDate->Value = *DucoUtils::ConvertDate(date);
    }
    endCheckBox->Checked = filters.EndDateEnabled(date);
    if (date.Valid())
    {
        this->endDate->Value = *DucoUtils::ConvertDate(date);
    }
    populating = false;
}

System::Void
FiltersUI::RequireConductor()
{
    requireConductor = true;
}

FiltersUI::~FiltersUI()
{
    if (components)
    {
        delete components;
    }
}

System::Void
FiltersUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    std::string associationStr;
    DucoUtils::ConvertString(associationEditor->Text, associationStr);
    filters.SetAssociation(associationCheckbox->Checked, DatabaseGenUtils::FindId(allAssocaitionIds, associationEditor->SelectedIndex));
    filters.SetIncludeWithdrawn(includeWithdrawn->Checked);
    filters.SetExcludeValid(excludeValid->Checked);
    filters.SetIncludeLinkedRingers(linkedRingers->Checked);
    filters.SetIncludeTowerBell(towerBells->Checked);
    filters.SetIncludeHandBell(handBells->Checked);

    bool dualOrder = false;
    unsigned int stage = -1;
    database->FindOrderNumber(stageSelector->Text, stage, dualOrder);
    if (ringerSelector->SelectedIndex != -1)
    {
        RingerItem^ ringer = (RingerItem^)ringerSelector->SelectedItem;
        filters.SetRinger(ringerCheckbox->Checked, ringer->Id());
    }
    if (conductorSelector->SelectedIndex != -1)
    {
        RingerItem^ composer = (RingerItem^)conductorSelector->SelectedItem;
        filters.SetConductor(conductorCheckbox->Checked, composer->Id());
    }
    filters.SetTower(towerCheckbox->Checked, DatabaseGenUtils::FindId(allTowerIds, towerSelector->SelectedIndex));
    filters.SetIncludeLinkedTowers(includeLinked->Checked);
    filters.SetMethod(methodCheckbox->Checked, DatabaseGenUtils::FindId(allMethodIds, methodSelector->SelectedIndex));
    std::wstring type = L"";
    DucoUtils::ConvertString(typeSelector->Text, type);
    filters.SetType(typeCheckbox->Checked, type);
    filters.SetStage(stageCheckbox->Checked, stage);

    if (requireConductor && (!conductorCheckbox->Checked || conductorSelector->SelectedIndex == -1) )
    {
        filters.SetConductor(true, database->Database().Settings().DefaultRinger());
    }
    filters.SetStartDate(startCheckBox->Checked, DucoUtils::ConvertDate(startDate->Value));
    filters.SetEndDate(endCheckBox->Checked, DucoUtils::ConvertDate(endDate->Value));
    filters.SetExcludeInvalidTimeOrChanges(excludeInvalidTimeOrChanges->Checked);

    Close();
}

System::Void
FiltersUI::cancelBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void
FiltersUI::clearBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    filters.Clear();
    Populate();
}

System::Void
FiltersUI::associationEditor_SelectionChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (!populating)
        associationCheckbox->Checked = associationEditor->SelectedIndex > 0;
}

System::Void
FiltersUI::ringerSelector_SelectionChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (!populating)
        ringerCheckbox->Checked = ringerSelector->SelectedIndex > 0;
}

System::Void
FiltersUI::conductorSelector_SelectionChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (!populating)
        conductorCheckbox->Checked = conductorSelector->SelectedIndex > 0;
}

System::Void
FiltersUI::towerSelector_SelectionChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (!populating)
        towerCheckbox->Checked = towerSelector->SelectedIndex > 0;
}

System::Void
FiltersUI::methodSelector_SelectionChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (!populating)
        methodCheckbox->Checked = methodSelector->SelectedIndex > 0;
}

System::Void
FiltersUI::typeSelector_SelectionChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (!populating)
        typeCheckbox->Checked = typeSelector->Text->Trim()->Length > 0;
}

System::Void
FiltersUI::stageSelector_SelectionChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (!populating)
        stageCheckbox->Checked = stageSelector->SelectedIndex > 0;
}

System::Void
FiltersUI::startDate_ValueChanged(System::Object^ sender, System::EventArgs^ e)
{
    if (!populating)
        startCheckBox->Checked = true;
}

System::Void
FiltersUI::endDate_ValueChanged(System::Object^ sender, System::EventArgs^ e)
{
    if (!populating)
        endCheckBox->Checked = true;
}

System::Void
FiltersUI::thisYearButton_Click(System::Object^ sender, System::EventArgs^ e)
{
    int year = System::DateTime::Now.Year;
    System::DateTime startDate(year, 1, 1);
    System::DateTime endDate(year, 12, 31);

    this->startDate->Value = startDate;
    this->endDate->Value = endDate;
    this->startCheckBox->Checked = true;
    this->endCheckBox->Checked = true;
}

System::Void
FiltersUI::lastYearButton_Click(System::Object^ sender, System::EventArgs^ e)
{
    int year = System::DateTime::Now.AddYears(-1).Year;
    System::DateTime startDate(year, 1, 1);
    System::DateTime endDate(year, 12, 31);

    this->startDate->Value = startDate;
    this->endDate->Value = endDate;
    this->startCheckBox->Checked = true;
    this->endCheckBox->Checked = true;
}

System::Void
FiltersUI::ringerSelector_Validated(System::Object^ sender, System::EventArgs^ e)
{
    if (ringerSelector->SelectedIndex == -1 && ringerSelector->Text->Trim()->Length > 0)
    {
        RingerChangedToUnknown(ringerSelector->Text);
    }
}

System::Void
FiltersUI::RingerChangedToUnknown(System::String^ newRingerName)
{ // Used when a user leaves the control, and the ringer is unrecognised from the cache check its not just been filtered out for performance

    ObjectId ringerId;
    DucoUI::ChooseRinger^ chooseRingerDialog = gcnew DucoUI::ChooseRinger(database, newRingerName);
    if (chooseRingerDialog->ShowDialog() == System::Windows::Forms::DialogResult::OK)
    {
        ringerId = chooseRingerDialog->SelectedId();
    }

    if (ringerId.ValidId())
    {
        ringerSelector->BeginUpdate();
        Int32 newIndex = ringerCache->FindIndexOrAdd(ringerId);
        ringerSelector->EndUpdate();
        if (newIndex != Int32::MaxValue)
        {
            ringerSelector->SelectedIndex = newIndex;
        }
    }
}
