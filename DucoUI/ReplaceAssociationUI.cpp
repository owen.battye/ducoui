#include "ProgressWrapper.h"
#include "DatabaseManager.h"

#include "ReplaceAssociationUI.h"

#include <string>
#include "SoundUtils.h"
#include "DucoUtils.h"
#include "DatabaseGenUtils.h"
#include "SystemDefaultIcon.h"

using namespace DucoUI;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Windows::Forms;

ReplaceAssociationUI::ReplaceAssociationUI(DatabaseManager^ theDatabase)
: database(theDatabase)
{
    progressWrapper = new ProgressCallbackWrapper(this);
    allAssociationNames = gcnew System::Collections::Generic::List<Int16>();
    InitializeComponent();
}

ReplaceAssociationUI::!ReplaceAssociationUI()
{
    delete progressWrapper;
    progressWrapper = NULL;
}

ReplaceAssociationUI::~ReplaceAssociationUI()
{
    this->!ReplaceAssociationUI();
    if (components)
    {
        delete components;
    }
}

//**********************************************************************************************
// background worker
//**********************************************************************************************
System::Void 
ReplaceAssociationUI::backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
    ReplaceAssociationUIArgs^ args = (ReplaceAssociationUIArgs^)e->Argument;
    database->ReplaceAssociation(progressWrapper, *args->oldAssociationId, *args->newAssociationId);
    delete args->oldAssociationId;
    delete args->newAssociationId;
}

System::Void 
ReplaceAssociationUI::backgroundWorker1_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    progressBar->Value = e->ProgressPercentage;
}

System::Void 
ReplaceAssociationUI::backgroundWorker1_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    progressBar->Value = 0;
    Duco::ObjectId associationId = DatabaseGenUtils::FindId(allAssociationNames, newAssociation->SelectedIndex);
    size_t newNoOfPeals = database->NumberOfPealsByAssociation(associationId);
    newAssociationLbl->Text = "In " + Convert::ToString(newNoOfPeals) + " performances";
    statusLabel->Text = database->FindAssociationName(associationId) + " is now in " + Convert::ToString(newNoOfPeals) + " performances";

    DatabaseGenUtils::GenerateAssociationOptions(oldAssociation, allAssociationNames, database, KNoId, false);
    DatabaseGenUtils::GenerateAssociationOptions(newAssociation, allAssociationNames, database, KNoId, false);
    oldAssociationLbl->Text = "";
    newAssociationLbl->Text = "";
    SoundUtils::PlayFinishedSound();
    closeBtn->Enabled = true;
}

//**********************************************************************************************
// Call backs from database
//**********************************************************************************************
void
ReplaceAssociationUI::Initialised()
{

}

void
ReplaceAssociationUI::Step(int progressPercent)
{
    backgroundWorker1->ReportProgress(progressPercent);
}

void
ReplaceAssociationUI::Complete()
{
    backgroundWorker1->ReportProgress(100);
}

//**********************************************************************************************
// UI interface handling
//**********************************************************************************************

System::Void 
ReplaceAssociationUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void
ReplaceAssociationUI::swapBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    int oldIndex = oldAssociation->SelectedIndex;
    oldAssociation->SelectedIndex = newAssociation->SelectedIndex;
    newAssociation->SelectedIndex = oldIndex;
}

System::Void 
ReplaceAssociationUI::replaceBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
        return;
    }
    if (DucoUtils::ConfirmChanges(this))
    {
        closeBtn->Enabled = false;
        ReplaceAssociationUIArgs^ args = gcnew ReplaceAssociationUIArgs;
        args->oldAssociationId = new Duco::ObjectId(DatabaseGenUtils::FindId(allAssociationNames, oldAssociation->SelectedIndex));
        args->newAssociationId = new Duco::ObjectId(DatabaseGenUtils::FindId(allAssociationNames, newAssociation->SelectedIndex));
        backgroundWorker1->RunWorkerAsync(args);
    }
}

System::Void 
ReplaceAssociationUI::oldAssociation_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId oldAssociationId = DatabaseGenUtils::FindId(allAssociationNames, oldAssociation->SelectedIndex);
    Duco::ObjectId newAssociationId = DatabaseGenUtils::FindId(allAssociationNames, newAssociation->SelectedIndex);
    if (oldAssociationId.ValidId())
    {
        size_t newNoOfPeals = database->NumberOfPealsByAssociation(oldAssociationId);
        oldAssociationLbl->Text = "In " + Convert::ToString(newNoOfPeals) + " performances";
    }
    else
    {
        oldAssociationLbl->Text = "";
    }
    EnableReplaceBtn(oldAssociationId, newAssociationId);
}

System::Void
ReplaceAssociationUI::newAssociation_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId oldAssociationId = DatabaseGenUtils::FindId(allAssociationNames, oldAssociation->SelectedIndex);
    Duco::ObjectId newAssociationId = DatabaseGenUtils::FindId(allAssociationNames, newAssociation->SelectedIndex);
    if (newAssociationId.ValidId())
    {
        size_t newNoOfPeals = database->NumberOfPealsByAssociation(newAssociationId);
        newAssociationLbl->Text = "In " + Convert::ToString(newNoOfPeals) + " performances";
    }
    else
    {
        newAssociationLbl->Text = "";
    }
    EnableReplaceBtn(oldAssociationId, newAssociationId);
}

System::Void
ReplaceAssociationUI::EnableReplaceBtn(const Duco::ObjectId& oldAssociationId, const Duco::ObjectId& newAssociationId)
{
    replaceBtn->Enabled = oldAssociationId.ValidId() && newAssociationId.ValidId() && newAssociationId != oldAssociationId;
    if (replaceBtn->Enabled)
    {
        statusLabel->Text = "Replace " + database->FindAssociationName(oldAssociationId) + " with " + database->FindAssociationName(newAssociationId);
    }
    else if (newAssociationId == oldAssociationId && newAssociationId.ValidId())
    {
        statusLabel->Text = "Same association";
    }
    else
    {
        statusLabel->Text = "";
    }
}

System::Void
ReplaceAssociationUI::ReplaceAssociationUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    DatabaseGenUtils::GenerateAssociationOptions(oldAssociation, allAssociationNames, database, KNoId, false);
    DatabaseGenUtils::GenerateAssociationOptions(newAssociation, allAssociationNames, database, KNoId, false);
}

System::Void
ReplaceAssociationUI::ReplaceAssociationUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e)
{
    if (backgroundWorker1->IsBusy)
    {
        e->Cancel = true;
    }
}
