#pragma once
namespace DucoUI
{
    public ref class RenumberUI : public System::Windows::Forms::Form, public DucoUI::RenumberProgressCallbackUI
	{
	public:
        RenumberUI(DucoUI::DatabaseManager^ theDatabase, System::Windows::Forms::Form^ parent);

        // from RenumberProgressCallbackUI
        virtual void RenumberInitialised(Duco::RenumberProgressCallback::TRenumberStage newStage);
        virtual void RenumberStep(size_t progress, size_t total);
        virtual void RenumberComplete();

	protected:
		!RenumberUI();
		~RenumberUI();
        System::Void RenumberUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void RenumberUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e);
        System::Void backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void backgroundWorker1_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void backgroundWorker1_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);

    protected:
		RenumberProgressWrapper*					progressWrapper;
        DucoUI::DatabaseManager^              database;
        Duco::RenumberProgressCallback::TRenumberStage    stage;
        bool                                        updateText;
        int                                         previousTotalProgressPercent;

    private:
        System::Windows::Forms::ProgressBar^        totalProgress;
        System::Windows::Forms::ProgressBar^        taskProgress;
        System::Windows::Forms::Label^              taskLbl;
        System::ComponentModel::BackgroundWorker^   backgroundWorker1;
        System::ComponentModel::Container^          components;
        System::Windows::Forms::Form^               mainForm;

    protected:
        void InitializeComponent()
        {
            System::Windows::Forms::Label^  progressLbl;
            System::Windows::Forms::Label^  label2;
            System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(RenumberUI::typeid));
            this->totalProgress = (gcnew System::Windows::Forms::ProgressBar());
            this->taskProgress = (gcnew System::Windows::Forms::ProgressBar());
            this->taskLbl = (gcnew System::Windows::Forms::Label());
            this->backgroundWorker1 = (gcnew System::ComponentModel::BackgroundWorker());
            progressLbl = (gcnew System::Windows::Forms::Label());
            label2 = (gcnew System::Windows::Forms::Label());
            this->SuspendLayout();
            // 
            // progressLbl
            // 
            progressLbl->AutoSize = true;
            progressLbl->Location = System::Drawing::Point(12, 70);
            progressLbl->Name = L"progressLbl";
            progressLbl->Size = System::Drawing::Size(78, 13);
            progressLbl->TabIndex = 2;
            progressLbl->Text = L"Total Progress:";
            // 
            // label2
            // 
            label2->AutoSize = true;
            label2->Location = System::Drawing::Point(12, 14);
            label2->Name = L"label2";
            label2->Size = System::Drawing::Size(110, 13);
            label2->TabIndex = 3;
            label2->Text = L"Current task progress:";
            // 
            // totalProgress
            // 
            this->totalProgress->Location = System::Drawing::Point(12, 89);
            this->totalProgress->Margin = System::Windows::Forms::Padding(10);
            this->totalProgress->Name = L"totalProgress";
            this->totalProgress->Size = System::Drawing::Size(437, 23);
            this->totalProgress->TabIndex = 0;
            // 
            // taskProgress
            // 
            this->taskProgress->Location = System::Drawing::Point(12, 33);
            this->taskProgress->Margin = System::Windows::Forms::Padding(10);
            this->taskProgress->Name = L"taskProgress";
            this->taskProgress->Size = System::Drawing::Size(437, 23);
            this->taskProgress->TabIndex = 1;
            // 
            // taskLbl
            // 
            this->taskLbl->Location = System::Drawing::Point(221, 14);
            this->taskLbl->Name = L"taskLbl";
            this->taskLbl->Size = System::Drawing::Size(228, 13);
            this->taskLbl->TabIndex = 4;
            this->taskLbl->Text = L"Starting...";
            // 
            // backgroundWorker1
            // 
            this->backgroundWorker1->WorkerReportsProgress = true;
            this->backgroundWorker1->WorkerSupportsCancellation = true;
            this->backgroundWorker1->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &RenumberUI::backgroundWorker1_DoWork);
            this->backgroundWorker1->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &RenumberUI::backgroundWorker1_ProgressChanged);
            this->backgroundWorker1->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &RenumberUI::backgroundWorker1_RunWorkerCompleted);
            // 
            // RenumberUI
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->AutoSize = true;
            this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            this->ClientSize = System::Drawing::Size(462, 121);
            this->Controls->Add(this->taskLbl);
            this->Controls->Add(label2);
            this->Controls->Add(progressLbl);
            this->Controls->Add(this->taskProgress);
            this->Controls->Add(this->totalProgress);
            this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
            this->MaximizeBox = false;
            this->MinimizeBox = false;
            this->Name = L"RenumberUI";
            this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
            this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
            this->Text = L"Reindexing database";
            this->TopMost = true;
            this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &RenumberUI::RenumberUI_FormClosing);
            this->Load += gcnew System::EventHandler(this, &RenumberUI::RenumberUI_Load);
            this->ResumeLayout(false);
            this->PerformLayout();

        }
    };
}
