#pragma once

namespace DucoUI
{
    public ref class Form1 : public System::Windows::Forms::Form,
                             public DucoUI::ImportExportProgressCallbackUI,
                             public DucoUI::RingingDatabaseObserver,
                             public DucoUI::ProgressCallbackUI
    {
    public:
        Form1(array<System::String ^>^ newArgs);

        //from RingingDatabaseObserver
        virtual System::Void DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2);

        // from ProgressCallbackUI
        virtual void Initialised();
        virtual void Step(int progressPercent);
        virtual void Complete();

        // from ImportExportProgressCallbackUI
        virtual System::Void InitialisingImportExport(System::Boolean internalising, unsigned int versionNumber, size_t totalNumberOfObjects);
        virtual System::Void ObjectProcessed(System::Boolean internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase);
        virtual System::Void ImportExportComplete(System::Boolean internalising);
        virtual System::Void ImportExportFailed(System::Boolean internalising, int errorCode);
        event System::EventHandler<DucoUI::CompletedEventArgs^>^ ImportExportCompleteEvent;
        virtual void OnImportExportCompleteReached(DucoUI::CompletedEventArgs^ e)
        {
            ImportExportCompleteEvent(this, e);
        }

        // new methods
        System::Void  SetState();
        System::Void  SetNumberOfPealsLabel(size_t number, size_t withdrawnCount);
        System::Void  SetNumberOfTowersLabel(size_t number, size_t removed, size_t noOfLinkedTowers);
        System::Void  SetNumberOfMethodsLabel(size_t number);
        System::Void  SetNumberOfRingersLabel(size_t number);
        System::Void  SetNumberOfPicturesLabel(size_t number);
        System::Void  SetNumberOfAssociationsLabel(size_t number);
        System::Void  SetAllObjectLabels();
        System::Void  ClearAllObjectLabels();

        System::Void  OpenSettings();
        System::Void  OpenRinger(DucoUI::DucoWindowState state);
        System::Void  OpenMethod(DucoUI::DucoWindowState state);
        System::Void  OpenTower(DucoUI::DucoWindowState state);
        System::Void  OpenPeal(DucoUI::DucoWindowState state);

    protected:
        ~Form1();
        !Form1();
        void SetPealsMenuCommandTitle();

        System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void Form1_Activated(System::Object^  sender, System::EventArgs^  e);
        System::Void Form1_Shown(System::Object^ sender, System::EventArgs^ e);
        System::Void Form1_DragDrop(System::Object^  sender, System::Windows::Forms::DragEventArgs^  e);
        System::Void Form1_DragOver(System::Object^  sender, System::Windows::Forms::DragEventArgs^  e);
        System::Void Form1_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e);
        System::Void ClosingEvent( Object^ /*sender*/, System::ComponentModel::CancelEventArgs^ e );
        System::Void HandleFormClosing(Object^ /*sender*/, System::Windows::Forms::FormClosingEventArgs^ e);
        System::Void fileToolStripMenuItem_DropDownOpening(System::Object^  sender, System::EventArgs^  e);
        System::Void validateMenuItem_DropDownOpening(System::Object^  sender, System::EventArgs^  e);
        System::Void ringersToolStripMenuItem_DropDownOpening(System::Object^  sender, System::EventArgs^  e);
        System::Void validateAllIncludeWarnings_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void newToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void openToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void saveToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void saveAsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void onlineToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void mergeToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void importToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void preferencesToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void exitToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void newRingerToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void showRingerToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void defaultRingerToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void newMethodToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void importNotationToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void importMethodToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void showMethodToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void newTowerToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void showTowerToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void newPealToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void showPealToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void newAssociationToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void viewAssociationsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void associationSearchToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void showAllPealsMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void downloadToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void downloadByIdToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void peopleStatsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void associationStatsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) ;
        System::Void methodStatsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void genderStatsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void pealLengthToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void towerCirclingToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void methodCirclingToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void updateFromDoveToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void importFromDoveToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void updateTowerLocationsToolBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void importTenorKeysToolBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void yearStatsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void graphsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void PealFeesToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void BellsRungToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void renumberToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void yearOrderingToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void picturesToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void deleteUnusedToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void pealSearchToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void onThisDayToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void towerSearchToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void ringerSearchToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void methodSearchToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void defaultRingerConductedStatsToolStripMenuItem1_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void towerStatsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void clearInvalidTenorKeysToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void correctTowerTypesToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e);

        System::Void aboutToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void resetToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void checkForUpdatesToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void closeAllToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void validateAllPealsCmd_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void validateAllTowersCmd_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void validateAllMethodsCmd_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void validateAllSeriesCmd_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void validateAllCompositionsCmd_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void validateAllRingersCmd_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void methodSeriesToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void viewCompositionToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void milestonesToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void editStagesToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void monthsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void replaceRingerToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void replaceTowerToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void replaceAssociationMenu_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void duplicateAssociationMenu_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void numberOfChangesMenu_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void fastestSlowestPealsCmd_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void alphabetsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void viewSeriesToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void searchSeriesToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void searchCompositionToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void printPealsStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void databaseSummaryToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void associationReportToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void pealMapStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void faqToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void towersToolStripMenuItem_DropDownOpening(System::Object^ sender, System::EventArgs^ e);
        System::Void OpenTower(System::Object^ sender, System::EventArgs^ e);
        System::Void uppercaseAssociationsToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void uppercaseTownsToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void capitaliseTowersToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void capitaliseRingerNamesToolStripMenuItem_Click(System::Object ^ sender, System::EventArgs ^ e);
        System::Void capitaliseMethodsToolStripMenuItem_Click(System::Object ^ sender, System::EventArgs ^ e);

        System::Void removeDuplicateRingsToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void replaceShortMethodTypesToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void removeDuplicateMethodsToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void updateIdsAndRWRefs_Click(System::Object^ sender, System::EventArgs^ e);

        System::Void fileHistory1_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void fileHistory2_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void fileHistory3_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void fileHistory4_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void fileHistory5_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void OpenHistoryItem(unsigned int historyItemNo);
        System::Boolean OpenNewDatabase(System::String^% newFileName);

        System::Boolean CheckNoChildForms(System::Boolean showDialog, System::String^ str);
        System::Void SetFileNameDisplay(System::String^ newFileName);
        System::Boolean CheckUnsavedChanges(System::Boolean tryingToClose);
        System::Boolean SaveDatabase(System::Boolean saveAs, System::Boolean& savedAsXml, System::Boolean& savedAsCsv);
        System::Void PrintSummary(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args);
        System::Boolean CheckRebuildRecommended(System::Boolean forceStart);

    private:
        static void ImportExportCompleteHandler(Object^ sender, DucoUI::CompletedEventArgs^ e);
        DucoUI::CheckLatestVersion^                     latestVersionChecker;
        System::Windows::Forms::ToolStripStatusLabel^   pealsStatusLabel;
        System::Windows::Forms::ToolStripStatusLabel^   towersStatusLabel;
        System::Windows::Forms::ToolStripStatusLabel^   ringersStatusLabel;
        System::Windows::Forms::ToolStripStatusLabel^   methodsStatusLabel;

        System::Windows::Forms::ToolStripMenuItem^  newToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  openToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  historyToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  saveToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  mergeToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  importToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  preferencesToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem;

        System::Windows::Forms::ToolStripProgressBar^  toolStripProgressBar1;
        System::Windows::Forms::ToolStripMenuItem^  newRingerToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  showRingerToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  defaultRingerToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  newPealToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  showPealToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  viewAssociationsToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  newAssociationToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  printPealsToolStripMenuItem;

        System::Windows::Forms::ToolStripMenuItem^  newTowerToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  showTowerToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  newMethodToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  showMethodToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  aboutToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  peopleStatsToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  updateFromDoveToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  importFromDoveToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  pealMapStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  graphsToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  pealFeesToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  bellsRungToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  renumberToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  yearOrderingToolStripMenuItem;

    private: System::Windows::Forms::ToolStripMenuItem^  saveAsToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  deleteUnusedToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  pealSearchToolStripMenuItem;
    private: System::Windows::Forms::ToolStripStatusLabel^  fileNameLbl;
    private: System::Windows::Forms::ToolStripMenuItem^  towerSearchToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  ringerSearchToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  methodSearchToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  closeAllToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  importNotationToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  importMethodToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  pealLengthCmd;
    private: System::Windows::Forms::ToolStripMenuItem^  associationStatsToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  yearsToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  towerStatsToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  methodStatsToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  genderStatsToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  methodSeriesToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  compositionToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  picturesToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  editStagesToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  fastestSlowestPealsCmd;
    private: System::Windows::Forms::ToolStripMenuItem^  alphabetsToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  monthsToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  milestonesCmd;
    private: System::Windows::Forms::ToolStripMenuItem^  conductedStatsCmd;
    private: System::Windows::Forms::ToolStripMenuItem^  validateAllIncludeWarnings;
    private: System::Windows::Forms::ToolStripSeparator^ validateSeperator;
    private: System::Windows::Forms::ToolStripMenuItem^  validateAllPealsCmd;
    private: System::Windows::Forms::ToolStripMenuItem^  validateAllTowersCmd;
    private: System::Windows::Forms::ToolStripMenuItem^  validateAllRingersCmd;
    private: System::Windows::Forms::ToolStripMenuItem^  validateAllMethodsCmd;
    private: System::Windows::Forms::ToolStripMenuItem^  validateAllSeriesCmd;
    private: System::Windows::Forms::ToolStripMenuItem^  validateAllCompositionsCmd;
    private: System::Windows::Forms::ToolStripMenuItem^  towerCirclingToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  methodCirclingToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  yearCirclingToolStripMenuItem;

        System::Windows::Forms::ToolStripMenuItem^  fileHistory1;
        System::Windows::Forms::ToolStripMenuItem^  fileHistory2;
        System::Windows::Forms::ToolStripMenuItem^  fileHistory3;
        System::Windows::Forms::ToolStripMenuItem^  fileHistory4;
        System::Windows::Forms::ToolStripMenuItem^  fileHistory5;
        System::Windows::Forms::ToolStripMenuItem^  replaceRingerToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  replaceTowerToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  replaceAssociationMenu;
        System::Windows::Forms::ToolStripMenuItem^  numberOfChangesMenu;
        System::Windows::Forms::ToolStripMenuItem^  viewSeriesToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  searchSeriesToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  viewCompositionToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^  searchCompositionToolStripMenuItem;
        DucoUI::ImportExportProgressWrapper*	    importExportProgressWrapper;
        DucoUI::ProgressCallbackWrapper*            progressWrapper;
        System::ComponentModel::IContainer^         components;
        DucoUI::DatabaseManager^                    database;

        array<System::String^>^   args;
        System::Boolean                      savingOnExit;
        System::Boolean                      firstOpened;

        System::Windows::Forms::ToolStripMenuItem^     validateAllCmd;
        System::Windows::Forms::ToolStripMenuItem^     seriesToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^     fileMenu;
        System::Windows::Forms::ToolStripMenuItem^     doveToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^     faqToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^     pealsToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^     associationsToolStripMenuItem;
        System::Windows::Forms::ToolTip^               toolTip1;
        System::Windows::Forms::ToolStripMenuItem^     showAllPealsMenuItem;

        System::Windows::Forms::ToolStripStatusLabel^  picturesStatusLabel;
        System::Windows::Forms::ToolStripStatusLabel^  associationsStatusLabel;

        System::Windows::Forms::ToolStripMenuItem^     duplicateAssociationMenu;
        System::Windows::Forms::ToolStripMenuItem^     onThisDayToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^     updateTowerLocationsToolBtn;
        System::Windows::Forms::ToolStripMenuItem^     databaseSummaryToolStripMenuItem;
        System::Windows::Forms::ToolStripMenuItem^     associationReportToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^ topTowersToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^ checkForUpdatesToolStripMenuItem;
private: System::Windows::Forms::ToolStripSeparator^ toolStripSeparator2;
private: System::Windows::Forms::ToolStripMenuItem^ importTenorKeysToolBtn;
private: System::Windows::Forms::ToolStripMenuItem^ bellboardToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^ downloadToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^ downloadByIdToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^ clearInvalidTenorKeysToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^ updateToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^ uppercaseAssociationsToolStripMenuItem;

private: System::Windows::Forms::ToolStripMenuItem^ uppercaseTownsToolStripMenuItem;

private: System::Windows::Forms::ToolStripMenuItem^ replaceShortMethodTypesToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^ removeDuplicateRingsToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^ removeDuplicateMethodsToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^ correctTowerTypes;

private: System::Windows::Forms::ToolStripMenuItem^ updateIdsAndRWRefs;
private: System::Windows::Forms::ToolStripMenuItem^ onlineToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^ resetToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^ capitaliseTowersToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^ capitaliseRingerNamesToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^ capitaliseMethodsToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^ associationSearchToolStripMenuItem;




       System::String^ fileToOpenWhenSaveComplete;

        void InitializeComponent()
        {
            this->components = (gcnew System::ComponentModel::Container());
            System::Windows::Forms::ToolStripSeparator^ fileMenuSeperator2;
            System::Windows::Forms::ToolStripSeparator^ fileMenuSeperator4;
            System::Windows::Forms::ToolStripSeparator^ fileMenuSeperator5;
            System::Windows::Forms::ToolStripSeparator^ fileMenuSeperator1;
            System::Windows::Forms::ToolStripSeparator^ pealMenuSeperator1;
            System::Windows::Forms::ToolStripSeparator^ towerMenuSeperator1;
            System::Windows::Forms::ToolStripSeparator^ ringerMenuSeperator1;
            System::Windows::Forms::ToolStripSeparator^ methodMenuSeperator1;
            System::Windows::Forms::ToolStripSeparator^ methodMenuSeperator2;
            System::Windows::Forms::ToolStripSeparator^ toolsMenuSeperator3;
            System::Windows::Forms::ToolStripSeparator^ toolsMenuSeperator4;
            System::Windows::Forms::ToolStripSeparator^ statsMenuSeperator3;
            System::Windows::Forms::ToolStripSeparator^ statsMenuSeperator1;
            System::Windows::Forms::ToolStripMenuItem^ statisticsToolStripMenuItem;
            System::Windows::Forms::ToolStripSeparator^ statsMenuSeperator4;
            System::Windows::Forms::ToolStripSeparator^ statsMenuSeperator5;
            System::Windows::Forms::ToolStripSeparator^ statsMenuSeperator7;
            System::Windows::Forms::ToolStripMenuItem^ toolsToolStripMenuItem;
            System::Windows::Forms::ToolStripSeparator^ toolStripMenuItem5;
            System::Windows::Forms::ToolStripSeparator^ toolStripMenuItem1;
            System::Windows::Forms::ToolStripSeparator^ toolStripMenuItem2;
            System::Windows::Forms::ToolStripSeparator^ toolStripMenuItem3;
            System::Windows::Forms::ToolStripSeparator^ toolStripMenuItem4;
            System::Windows::Forms::ToolStripMenuItem^ methodsToolStripMenuItem;
            System::Windows::Forms::ToolStripMenuItem^ towersToolStripMenuItem;
            System::Windows::Forms::ToolStripSeparator^ towerMenuSeperator2;
            System::Windows::Forms::ToolStripMenuItem^ ringersToolStripMenuItem;
            System::Windows::Forms::MenuStrip^ menuStrip1;
            System::Windows::Forms::ToolStripSeparator^ fileMenuSeperator3;
            System::Windows::Forms::ToolStripMenuItem^ reportsToolStripMenuItem;
            System::Windows::Forms::ToolStripSeparator^ toolStripSeparator1;
            System::Windows::Forms::ToolStripSeparator^ pealMenuSeperator2;
            System::Windows::Forms::ToolStripSeparator^ associationsMenuSeperator1;
            System::Windows::Forms::ToolStripMenuItem^ helpToolStripMenuItem;
            System::Windows::Forms::ToolStripMenuItem^ windowToolStripMenuItem;
            System::Windows::Forms::StatusStrip^ statusStrip1;
            this->pealLengthCmd = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->milestonesCmd = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->fastestSlowestPealsCmd = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->onThisDayToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->pealMapStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->associationStatsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->towerStatsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->peopleStatsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->methodStatsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->methodSeriesToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->bellsRungToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->conductedStatsCmd = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->genderStatsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->monthsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->numberOfChangesMenu = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->alphabetsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->towerCirclingToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->methodCirclingToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->yearCirclingToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->pealFeesToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->graphsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->updateToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->yearOrderingToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->uppercaseAssociationsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->replaceAssociationMenu = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->duplicateAssociationMenu = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->uppercaseTownsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->capitaliseTowersToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->clearInvalidTenorKeysToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->replaceTowerToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->removeDuplicateRingsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->correctTowerTypes = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->capitaliseRingerNamesToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->replaceRingerToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->editStagesToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->capitaliseMethodsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->replaceShortMethodTypesToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->removeDuplicateMethodsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->picturesToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->validateAllCmd = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->validateAllIncludeWarnings = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->validateSeperator = (gcnew System::Windows::Forms::ToolStripSeparator());
            this->validateAllPealsCmd = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->validateAllTowersCmd = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->validateAllRingersCmd = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->validateAllMethodsCmd = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->validateAllSeriesCmd = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->validateAllCompositionsCmd = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->deleteUnusedToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->renumberToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->newMethodToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->importMethodToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->importNotationToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->showMethodToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->methodSearchToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->seriesToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->viewSeriesToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->searchSeriesToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->compositionToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->viewCompositionToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->searchCompositionToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->newTowerToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->showTowerToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->towerSearchToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->doveToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->importFromDoveToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->updateFromDoveToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->updateTowerLocationsToolBtn = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->importTenorKeysToolBtn = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->topTowersToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->newRingerToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->showRingerToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->defaultRingerToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->ringerSearchToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->fileMenu = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->newToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->openToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->historyToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->fileHistory1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->fileHistory2 = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->fileHistory3 = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->fileHistory4 = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->fileHistory5 = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->saveToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->saveAsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->onlineToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->mergeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->importToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->databaseSummaryToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->associationReportToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->preferencesToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->pealsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->newPealToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->bellboardToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->downloadToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->downloadByIdToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->updateIdsAndRWRefs = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->showPealToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->showAllPealsMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->pealSearchToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->printPealsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->associationsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->newAssociationToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->viewAssociationsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->aboutToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->resetToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->checkForUpdatesToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->toolStripSeparator2 = (gcnew System::Windows::Forms::ToolStripSeparator());
            this->faqToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->closeAllToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->toolStripProgressBar1 = (gcnew System::Windows::Forms::ToolStripProgressBar());
            this->pealsStatusLabel = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->associationsStatusLabel = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->towersStatusLabel = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->ringersStatusLabel = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->methodsStatusLabel = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->picturesStatusLabel = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->fileNameLbl = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->toolTip1 = (gcnew System::Windows::Forms::ToolTip(this->components));
            this->associationSearchToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            fileMenuSeperator2 = (gcnew System::Windows::Forms::ToolStripSeparator());
            fileMenuSeperator4 = (gcnew System::Windows::Forms::ToolStripSeparator());
            fileMenuSeperator5 = (gcnew System::Windows::Forms::ToolStripSeparator());
            fileMenuSeperator1 = (gcnew System::Windows::Forms::ToolStripSeparator());
            pealMenuSeperator1 = (gcnew System::Windows::Forms::ToolStripSeparator());
            towerMenuSeperator1 = (gcnew System::Windows::Forms::ToolStripSeparator());
            ringerMenuSeperator1 = (gcnew System::Windows::Forms::ToolStripSeparator());
            methodMenuSeperator1 = (gcnew System::Windows::Forms::ToolStripSeparator());
            methodMenuSeperator2 = (gcnew System::Windows::Forms::ToolStripSeparator());
            toolsMenuSeperator3 = (gcnew System::Windows::Forms::ToolStripSeparator());
            toolsMenuSeperator4 = (gcnew System::Windows::Forms::ToolStripSeparator());
            statsMenuSeperator3 = (gcnew System::Windows::Forms::ToolStripSeparator());
            statsMenuSeperator1 = (gcnew System::Windows::Forms::ToolStripSeparator());
            statisticsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            statsMenuSeperator4 = (gcnew System::Windows::Forms::ToolStripSeparator());
            statsMenuSeperator5 = (gcnew System::Windows::Forms::ToolStripSeparator());
            statsMenuSeperator7 = (gcnew System::Windows::Forms::ToolStripSeparator());
            toolsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            toolStripMenuItem5 = (gcnew System::Windows::Forms::ToolStripSeparator());
            toolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripSeparator());
            toolStripMenuItem2 = (gcnew System::Windows::Forms::ToolStripSeparator());
            toolStripMenuItem3 = (gcnew System::Windows::Forms::ToolStripSeparator());
            toolStripMenuItem4 = (gcnew System::Windows::Forms::ToolStripSeparator());
            methodsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            towersToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            towerMenuSeperator2 = (gcnew System::Windows::Forms::ToolStripSeparator());
            ringersToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
            fileMenuSeperator3 = (gcnew System::Windows::Forms::ToolStripSeparator());
            reportsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            toolStripSeparator1 = (gcnew System::Windows::Forms::ToolStripSeparator());
            pealMenuSeperator2 = (gcnew System::Windows::Forms::ToolStripSeparator());
            associationsMenuSeperator1 = (gcnew System::Windows::Forms::ToolStripSeparator());
            helpToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            windowToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            menuStrip1->SuspendLayout();
            statusStrip1->SuspendLayout();
            this->SuspendLayout();
            // 
            // fileMenuSeperator2
            // 
            fileMenuSeperator2->Name = L"fileMenuSeperator2";
            fileMenuSeperator2->Size = System::Drawing::Size(132, 6);
            // 
            // fileMenuSeperator4
            // 
            fileMenuSeperator4->Name = L"fileMenuSeperator4";
            fileMenuSeperator4->Size = System::Drawing::Size(132, 6);
            // 
            // fileMenuSeperator5
            // 
            fileMenuSeperator5->Name = L"fileMenuSeperator5";
            fileMenuSeperator5->Size = System::Drawing::Size(132, 6);
            // 
            // fileMenuSeperator1
            // 
            fileMenuSeperator1->Name = L"fileMenuSeperator1";
            fileMenuSeperator1->Size = System::Drawing::Size(132, 6);
            // 
            // pealMenuSeperator1
            // 
            pealMenuSeperator1->Name = L"pealMenuSeperator1";
            pealMenuSeperator1->Size = System::Drawing::Size(121, 6);
            // 
            // towerMenuSeperator1
            // 
            towerMenuSeperator1->Name = L"towerMenuSeperator1";
            towerMenuSeperator1->Size = System::Drawing::Size(177, 6);
            // 
            // ringerMenuSeperator1
            // 
            ringerMenuSeperator1->Name = L"ringerMenuSeperator1";
            ringerMenuSeperator1->Size = System::Drawing::Size(143, 6);
            // 
            // methodMenuSeperator1
            // 
            methodMenuSeperator1->Name = L"methodMenuSeperator1";
            methodMenuSeperator1->Size = System::Drawing::Size(155, 6);
            // 
            // methodMenuSeperator2
            // 
            methodMenuSeperator2->Name = L"methodMenuSeperator2";
            methodMenuSeperator2->Size = System::Drawing::Size(155, 6);
            // 
            // toolsMenuSeperator3
            // 
            toolsMenuSeperator3->Name = L"toolsMenuSeperator3";
            toolsMenuSeperator3->Size = System::Drawing::Size(163, 6);
            // 
            // toolsMenuSeperator4
            // 
            toolsMenuSeperator4->Name = L"toolsMenuSeperator4";
            toolsMenuSeperator4->Size = System::Drawing::Size(163, 6);
            // 
            // statsMenuSeperator3
            // 
            statsMenuSeperator3->Name = L"statsMenuSeperator3";
            statsMenuSeperator3->Size = System::Drawing::Size(234, 6);
            // 
            // statsMenuSeperator1
            // 
            statsMenuSeperator1->Name = L"statsMenuSeperator1";
            statsMenuSeperator1->Size = System::Drawing::Size(234, 6);
            // 
            // statisticsToolStripMenuItem
            // 
            statisticsToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(26) {
                this->pealLengthCmd,
                    this->milestonesCmd, this->fastestSlowestPealsCmd, this->onThisDayToolStripMenuItem, this->pealMapStripMenuItem, statsMenuSeperator1,
                    this->associationStatsToolStripMenuItem, this->towerStatsToolStripMenuItem, this->peopleStatsToolStripMenuItem, this->methodStatsToolStripMenuItem,
                    this->methodSeriesToolStripMenuItem, statsMenuSeperator3, this->bellsRungToolStripMenuItem, this->conductedStatsCmd, this->genderStatsToolStripMenuItem,
                    this->monthsToolStripMenuItem, this->numberOfChangesMenu, statsMenuSeperator4, this->alphabetsToolStripMenuItem, this->towerCirclingToolStripMenuItem,
                    this->methodCirclingToolStripMenuItem, this->yearCirclingToolStripMenuItem, statsMenuSeperator5, this->pealFeesToolStripMenuItem,
                    statsMenuSeperator7, this->graphsToolStripMenuItem
            });
            statisticsToolStripMenuItem->Name = L"statisticsToolStripMenuItem";
            statisticsToolStripMenuItem->Size = System::Drawing::Size(65, 20);
            statisticsToolStripMenuItem->Text = L"&Statistics";
            // 
            // pealLengthCmd
            // 
            this->pealLengthCmd->Name = L"pealLengthCmd";
            this->pealLengthCmd->Size = System::Drawing::Size(237, 22);
            this->pealLengthCmd->Text = L"Total performance lengths";
            this->pealLengthCmd->Click += gcnew System::EventHandler(this, &Form1::pealLengthToolStripMenuItem_Click);
            // 
            // milestonesCmd
            // 
            this->milestonesCmd->Name = L"milestonesCmd";
            this->milestonesCmd->Size = System::Drawing::Size(237, 22);
            this->milestonesCmd->Text = L"Milestones";
            this->milestonesCmd->Click += gcnew System::EventHandler(this, &Form1::milestonesToolStripMenuItem_Click);
            // 
            // fastestSlowestPealsCmd
            // 
            this->fastestSlowestPealsCmd->Name = L"fastestSlowestPealsCmd";
            this->fastestSlowestPealsCmd->Size = System::Drawing::Size(237, 22);
            this->fastestSlowestPealsCmd->Text = L"Fastest / Slowest performances";
            this->fastestSlowestPealsCmd->Click += gcnew System::EventHandler(this, &Form1::fastestSlowestPealsCmd_Click);
            // 
            // onThisDayToolStripMenuItem
            // 
            this->onThisDayToolStripMenuItem->Name = L"onThisDayToolStripMenuItem";
            this->onThisDayToolStripMenuItem->Size = System::Drawing::Size(237, 22);
            this->onThisDayToolStripMenuItem->Text = L"On this day";
            this->onThisDayToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::onThisDayToolStripMenuItem_Click);
            // 
            // pealMapStripMenuItem
            // 
            this->pealMapStripMenuItem->Name = L"pealMapStripMenuItem";
            this->pealMapStripMenuItem->Size = System::Drawing::Size(237, 22);
            this->pealMapStripMenuItem->Text = L"Performance map";
            this->pealMapStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::pealMapStripMenuItem_Click);
            // 
            // associationStatsToolStripMenuItem
            // 
            this->associationStatsToolStripMenuItem->Name = L"associationStatsToolStripMenuItem";
            this->associationStatsToolStripMenuItem->Size = System::Drawing::Size(237, 22);
            this->associationStatsToolStripMenuItem->Text = L"Associations";
            this->associationStatsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::associationStatsToolStripMenuItem_Click);
            // 
            // towerStatsToolStripMenuItem
            // 
            this->towerStatsToolStripMenuItem->Name = L"towerStatsToolStripMenuItem";
            this->towerStatsToolStripMenuItem->Size = System::Drawing::Size(237, 22);
            this->towerStatsToolStripMenuItem->Text = L"Towers";
            this->towerStatsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::towerStatsToolStripMenuItem_Click);
            // 
            // peopleStatsToolStripMenuItem
            // 
            this->peopleStatsToolStripMenuItem->Name = L"peopleStatsToolStripMenuItem";
            this->peopleStatsToolStripMenuItem->Size = System::Drawing::Size(237, 22);
            this->peopleStatsToolStripMenuItem->Text = L"People";
            this->peopleStatsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::peopleStatsToolStripMenuItem_Click);
            // 
            // methodStatsToolStripMenuItem
            // 
            this->methodStatsToolStripMenuItem->Name = L"methodStatsToolStripMenuItem";
            this->methodStatsToolStripMenuItem->Size = System::Drawing::Size(237, 22);
            this->methodStatsToolStripMenuItem->Text = L"Methods";
            this->methodStatsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::methodStatsToolStripMenuItem_Click);
            // 
            // methodSeriesToolStripMenuItem
            // 
            this->methodSeriesToolStripMenuItem->Name = L"methodSeriesToolStripMenuItem";
            this->methodSeriesToolStripMenuItem->Size = System::Drawing::Size(237, 22);
            this->methodSeriesToolStripMenuItem->Text = L"Method Series";
            this->methodSeriesToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::methodSeriesToolStripMenuItem_Click);
            // 
            // bellsRungToolStripMenuItem
            // 
            this->bellsRungToolStripMenuItem->Name = L"bellsRungToolStripMenuItem";
            this->bellsRungToolStripMenuItem->Size = System::Drawing::Size(237, 22);
            this->bellsRungToolStripMenuItem->Text = L"Bells rung";
            this->bellsRungToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::BellsRungToolStripMenuItem_Click);
            // 
            // conductedStatsCmd
            // 
            this->conductedStatsCmd->Name = L"conductedStatsCmd";
            this->conductedStatsCmd->Size = System::Drawing::Size(237, 22);
            this->conductedStatsCmd->Text = L"Conducted";
            this->conductedStatsCmd->Click += gcnew System::EventHandler(this, &Form1::defaultRingerConductedStatsToolStripMenuItem1_Click);
            // 
            // genderStatsToolStripMenuItem
            // 
            this->genderStatsToolStripMenuItem->Name = L"genderStatsToolStripMenuItem";
            this->genderStatsToolStripMenuItem->Size = System::Drawing::Size(237, 22);
            this->genderStatsToolStripMenuItem->Text = L"Gender";
            this->genderStatsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::genderStatsToolStripMenuItem_Click);
            // 
            // monthsToolStripMenuItem
            // 
            this->monthsToolStripMenuItem->Name = L"monthsToolStripMenuItem";
            this->monthsToolStripMenuItem->Size = System::Drawing::Size(237, 22);
            this->monthsToolStripMenuItem->Text = L"Months && Years";
            this->monthsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::monthsToolStripMenuItem_Click);
            // 
            // numberOfChangesMenu
            // 
            this->numberOfChangesMenu->Name = L"numberOfChangesMenu";
            this->numberOfChangesMenu->Size = System::Drawing::Size(237, 22);
            this->numberOfChangesMenu->Text = L"Number of changes";
            this->numberOfChangesMenu->Click += gcnew System::EventHandler(this, &Form1::numberOfChangesMenu_Click);
            // 
            // statsMenuSeperator4
            // 
            statsMenuSeperator4->Name = L"statsMenuSeperator4";
            statsMenuSeperator4->Size = System::Drawing::Size(234, 6);
            // 
            // alphabetsToolStripMenuItem
            // 
            this->alphabetsToolStripMenuItem->Name = L"alphabetsToolStripMenuItem";
            this->alphabetsToolStripMenuItem->Size = System::Drawing::Size(237, 22);
            this->alphabetsToolStripMenuItem->Text = L"Alphabets";
            this->alphabetsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::alphabetsToolStripMenuItem_Click);
            // 
            // towerCirclingToolStripMenuItem
            // 
            this->towerCirclingToolStripMenuItem->Name = L"towerCirclingToolStripMenuItem";
            this->towerCirclingToolStripMenuItem->Size = System::Drawing::Size(237, 22);
            this->towerCirclingToolStripMenuItem->Text = L"Tower circling";
            this->towerCirclingToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::towerCirclingToolStripMenuItem_Click);
            // 
            // methodCirclingToolStripMenuItem
            // 
            this->methodCirclingToolStripMenuItem->Name = L"methodCirclingToolStripMenuItem";
            this->methodCirclingToolStripMenuItem->Size = System::Drawing::Size(237, 22);
            this->methodCirclingToolStripMenuItem->Text = L"Method circling";
            this->methodCirclingToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::methodCirclingToolStripMenuItem_Click);
            // 
            // yearCirclingToolStripMenuItem
            // 
            this->yearCirclingToolStripMenuItem->Name = L"yearCirclingToolStripMenuItem";
            this->yearCirclingToolStripMenuItem->Size = System::Drawing::Size(237, 22);
            this->yearCirclingToolStripMenuItem->Text = L"Year circling";
            this->yearCirclingToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::yearStatsToolStripMenuItem_Click);
            // 
            // statsMenuSeperator5
            // 
            statsMenuSeperator5->Name = L"statsMenuSeperator5";
            statsMenuSeperator5->Size = System::Drawing::Size(234, 6);
            // 
            // pealFeesToolStripMenuItem
            // 
            this->pealFeesToolStripMenuItem->Name = L"pealFeesToolStripMenuItem";
            this->pealFeesToolStripMenuItem->Size = System::Drawing::Size(237, 22);
            this->pealFeesToolStripMenuItem->Text = L"Peal fees";
            this->pealFeesToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::PealFeesToolStripMenuItem_Click);
            // 
            // statsMenuSeperator7
            // 
            statsMenuSeperator7->Name = L"statsMenuSeperator7";
            statsMenuSeperator7->Size = System::Drawing::Size(234, 6);
            // 
            // graphsToolStripMenuItem
            // 
            this->graphsToolStripMenuItem->Name = L"graphsToolStripMenuItem";
            this->graphsToolStripMenuItem->Size = System::Drawing::Size(237, 22);
            this->graphsToolStripMenuItem->Text = L"Graphs";
            this->graphsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::graphsToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            toolsToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(6) {
                this->updateToolStripMenuItem,
                    toolsMenuSeperator3, this->validateAllCmd, toolsMenuSeperator4, this->deleteUnusedToolStripMenuItem, this->renumberToolStripMenuItem
            });
            toolsToolStripMenuItem->Name = L"toolsToolStripMenuItem";
            toolsToolStripMenuItem->Size = System::Drawing::Size(46, 20);
            toolsToolStripMenuItem->Text = L"&Tools";
            // 
            // updateToolStripMenuItem
            // 
            this->updateToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(22) {
                this->yearOrderingToolStripMenuItem,
                    toolStripMenuItem5, this->uppercaseAssociationsToolStripMenuItem, this->replaceAssociationMenu, this->duplicateAssociationMenu,
                    toolStripMenuItem1, this->uppercaseTownsToolStripMenuItem, this->capitaliseTowersToolStripMenuItem, this->clearInvalidTenorKeysToolStripMenuItem,
                    this->replaceTowerToolStripMenuItem, this->removeDuplicateRingsToolStripMenuItem, this->correctTowerTypes, toolStripMenuItem2,
                    this->capitaliseRingerNamesToolStripMenuItem, this->replaceRingerToolStripMenuItem, toolStripMenuItem3, this->editStagesToolStripMenuItem,
                    this->capitaliseMethodsToolStripMenuItem, this->replaceShortMethodTypesToolStripMenuItem, this->removeDuplicateMethodsToolStripMenuItem,
                    toolStripMenuItem4, this->picturesToolStripMenuItem
            });
            this->updateToolStripMenuItem->Name = L"updateToolStripMenuItem";
            this->updateToolStripMenuItem->Size = System::Drawing::Size(166, 22);
            this->updateToolStripMenuItem->Text = L"Update";
            // 
            // yearOrderingToolStripMenuItem
            // 
            this->yearOrderingToolStripMenuItem->Name = L"yearOrderingToolStripMenuItem";
            this->yearOrderingToolStripMenuItem->Size = System::Drawing::Size(262, 22);
            this->yearOrderingToolStripMenuItem->Text = L"Day ordering";
            this->yearOrderingToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::yearOrderingToolStripMenuItem_Click);
            // 
            // toolStripMenuItem5
            // 
            toolStripMenuItem5->Name = L"toolStripMenuItem5";
            toolStripMenuItem5->Size = System::Drawing::Size(259, 6);
            // 
            // uppercaseAssociationsToolStripMenuItem
            // 
            this->uppercaseAssociationsToolStripMenuItem->Name = L"uppercaseAssociationsToolStripMenuItem";
            this->uppercaseAssociationsToolStripMenuItem->Size = System::Drawing::Size(262, 22);
            this->uppercaseAssociationsToolStripMenuItem->Text = L"Uppercase associations";
            this->uppercaseAssociationsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::uppercaseAssociationsToolStripMenuItem_Click);
            // 
            // replaceAssociationMenu
            // 
            this->replaceAssociationMenu->Name = L"replaceAssociationMenu";
            this->replaceAssociationMenu->Size = System::Drawing::Size(262, 22);
            this->replaceAssociationMenu->Text = L"Replace association";
            this->replaceAssociationMenu->Click += gcnew System::EventHandler(this, &Form1::replaceAssociationMenu_Click);
            // 
            // duplicateAssociationMenu
            // 
            this->duplicateAssociationMenu->Name = L"duplicateAssociationMenu";
            this->duplicateAssociationMenu->Size = System::Drawing::Size(262, 22);
            this->duplicateAssociationMenu->Text = L"Remove duplicate associations";
            this->duplicateAssociationMenu->Click += gcnew System::EventHandler(this, &Form1::duplicateAssociationMenu_Click);
            // 
            // toolStripMenuItem1
            // 
            toolStripMenuItem1->Name = L"toolStripMenuItem1";
            toolStripMenuItem1->Size = System::Drawing::Size(259, 6);
            // 
            // uppercaseTownsToolStripMenuItem
            // 
            this->uppercaseTownsToolStripMenuItem->Name = L"uppercaseTownsToolStripMenuItem";
            this->uppercaseTownsToolStripMenuItem->Size = System::Drawing::Size(262, 22);
            this->uppercaseTownsToolStripMenuItem->Text = L"Uppercase towns";
            this->uppercaseTownsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::uppercaseTownsToolStripMenuItem_Click);
            // 
            // capitaliseTowersToolStripMenuItem
            // 
            this->capitaliseTowersToolStripMenuItem->Name = L"capitaliseTowersToolStripMenuItem";
            this->capitaliseTowersToolStripMenuItem->Size = System::Drawing::Size(262, 22);
            this->capitaliseTowersToolStripMenuItem->Text = L"Capitalise county and dedication";
            this->capitaliseTowersToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::capitaliseTowersToolStripMenuItem_Click);
            // 
            // clearInvalidTenorKeysToolStripMenuItem
            // 
            this->clearInvalidTenorKeysToolStripMenuItem->Name = L"clearInvalidTenorKeysToolStripMenuItem";
            this->clearInvalidTenorKeysToolStripMenuItem->Size = System::Drawing::Size(262, 22);
            this->clearInvalidTenorKeysToolStripMenuItem->Text = L"Clear invalid tenor keys";
            this->clearInvalidTenorKeysToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::clearInvalidTenorKeysToolStripMenuItem_Click);
            // 
            // replaceTowerToolStripMenuItem
            // 
            this->replaceTowerToolStripMenuItem->Name = L"replaceTowerToolStripMenuItem";
            this->replaceTowerToolStripMenuItem->Size = System::Drawing::Size(262, 22);
            this->replaceTowerToolStripMenuItem->Text = L"Replace tower";
            this->replaceTowerToolStripMenuItem->ToolTipText = L"Replaces one tower in all peals with another tower.";
            this->replaceTowerToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::replaceTowerToolStripMenuItem_Click);
            // 
            // removeDuplicateRingsToolStripMenuItem
            // 
            this->removeDuplicateRingsToolStripMenuItem->Name = L"removeDuplicateRingsToolStripMenuItem";
            this->removeDuplicateRingsToolStripMenuItem->Size = System::Drawing::Size(262, 22);
            this->removeDuplicateRingsToolStripMenuItem->Text = L"Remove duplicate rings";
            this->removeDuplicateRingsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::removeDuplicateRingsToolStripMenuItem_Click);
            // 
            // correctTowerTypes
            // 
            this->correctTowerTypes->Name = L"correctTowerTypes";
            this->correctTowerTypes->Size = System::Drawing::Size(262, 22);
            this->correctTowerTypes->Text = L"Correct tower types";
            this->correctTowerTypes->ToolTipText = L"Set tower as handbell, if all performaces at it are also handbell performances.";
            this->correctTowerTypes->Click += gcnew System::EventHandler(this, &Form1::correctTowerTypesToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            toolStripMenuItem2->Name = L"toolStripMenuItem2";
            toolStripMenuItem2->Size = System::Drawing::Size(259, 6);
            // 
            // capitaliseRingerNamesToolStripMenuItem
            // 
            this->capitaliseRingerNamesToolStripMenuItem->Name = L"capitaliseRingerNamesToolStripMenuItem";
            this->capitaliseRingerNamesToolStripMenuItem->Size = System::Drawing::Size(262, 22);
            this->capitaliseRingerNamesToolStripMenuItem->Text = L"Capitalise ringer names";
            this->capitaliseRingerNamesToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::capitaliseRingerNamesToolStripMenuItem_Click);
            // 
            // replaceRingerToolStripMenuItem
            // 
            this->replaceRingerToolStripMenuItem->Name = L"replaceRingerToolStripMenuItem";
            this->replaceRingerToolStripMenuItem->Size = System::Drawing::Size(262, 22);
            this->replaceRingerToolStripMenuItem->Text = L"Replace ringer";
            this->replaceRingerToolStripMenuItem->ToolTipText = L"Replaces one ringer in all peals with another ringer.";
            this->replaceRingerToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::replaceRingerToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            toolStripMenuItem3->Name = L"toolStripMenuItem3";
            toolStripMenuItem3->Size = System::Drawing::Size(259, 6);
            // 
            // editStagesToolStripMenuItem
            // 
            this->editStagesToolStripMenuItem->Name = L"editStagesToolStripMenuItem";
            this->editStagesToolStripMenuItem->Size = System::Drawing::Size(262, 22);
            this->editStagesToolStripMenuItem->Text = L"Edit stages";
            this->editStagesToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::editStagesToolStripMenuItem_Click);
            // 
            // capitaliseMethodsToolStripMenuItem
            // 
            this->capitaliseMethodsToolStripMenuItem->Name = L"capitaliseMethodsToolStripMenuItem";
            this->capitaliseMethodsToolStripMenuItem->Size = System::Drawing::Size(262, 22);
            this->capitaliseMethodsToolStripMenuItem->Text = L"Capitalise method names and types";
            this->capitaliseMethodsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::capitaliseMethodsToolStripMenuItem_Click);
            // 
            // replaceShortMethodTypesToolStripMenuItem
            // 
            this->replaceShortMethodTypesToolStripMenuItem->Name = L"replaceShortMethodTypesToolStripMenuItem";
            this->replaceShortMethodTypesToolStripMenuItem->Size = System::Drawing::Size(262, 22);
            this->replaceShortMethodTypesToolStripMenuItem->Text = L"Replace short method types";
            this->replaceShortMethodTypesToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::replaceShortMethodTypesToolStripMenuItem_Click);
            // 
            // removeDuplicateMethodsToolStripMenuItem
            // 
            this->removeDuplicateMethodsToolStripMenuItem->Name = L"removeDuplicateMethodsToolStripMenuItem";
            this->removeDuplicateMethodsToolStripMenuItem->Size = System::Drawing::Size(262, 22);
            this->removeDuplicateMethodsToolStripMenuItem->Text = L"Remove duplicate methods";
            this->removeDuplicateMethodsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::removeDuplicateMethodsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            toolStripMenuItem4->Name = L"toolStripMenuItem4";
            toolStripMenuItem4->Size = System::Drawing::Size(259, 6);
            // 
            // picturesToolStripMenuItem
            // 
            this->picturesToolStripMenuItem->Name = L"picturesToolStripMenuItem";
            this->picturesToolStripMenuItem->Size = System::Drawing::Size(262, 22);
            this->picturesToolStripMenuItem->Text = L"Manage pictures";
            this->picturesToolStripMenuItem->ToolTipText = L"Manage pictures.";
            this->picturesToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::picturesToolStripMenuItem_Click);
            // 
            // validateAllCmd
            // 
            this->validateAllCmd->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(8) {
                this->validateAllIncludeWarnings,
                    this->validateSeperator, this->validateAllPealsCmd, this->validateAllTowersCmd, this->validateAllRingersCmd, this->validateAllMethodsCmd,
                    this->validateAllSeriesCmd, this->validateAllCompositionsCmd
            });
            this->validateAllCmd->Name = L"validateAllCmd";
            this->validateAllCmd->Size = System::Drawing::Size(166, 22);
            this->validateAllCmd->Text = L"Validate";
            this->validateAllCmd->DropDownOpening += gcnew System::EventHandler(this, &Form1::validateMenuItem_DropDownOpening);
            // 
            // validateAllIncludeWarnings
            // 
            this->validateAllIncludeWarnings->CheckOnClick = true;
            this->validateAllIncludeWarnings->Name = L"validateAllIncludeWarnings";
            this->validateAllIncludeWarnings->Size = System::Drawing::Size(164, 22);
            this->validateAllIncludeWarnings->Text = L"Include warnings";
            this->validateAllIncludeWarnings->Click += gcnew System::EventHandler(this, &Form1::validateAllIncludeWarnings_Click);
            // 
            // validateSeperator
            // 
            this->validateSeperator->Name = L"validateSeperator";
            this->validateSeperator->Size = System::Drawing::Size(161, 6);
            // 
            // validateAllPealsCmd
            // 
            this->validateAllPealsCmd->Name = L"validateAllPealsCmd";
            this->validateAllPealsCmd->Size = System::Drawing::Size(164, 22);
            this->validateAllPealsCmd->Text = L"Performances";
            this->validateAllPealsCmd->ToolTipText = L"Checks all performances are valid";
            this->validateAllPealsCmd->Click += gcnew System::EventHandler(this, &Form1::validateAllPealsCmd_Click);
            // 
            // validateAllTowersCmd
            // 
            this->validateAllTowersCmd->Name = L"validateAllTowersCmd";
            this->validateAllTowersCmd->Size = System::Drawing::Size(164, 22);
            this->validateAllTowersCmd->Text = L"Towers";
            this->validateAllTowersCmd->ToolTipText = L"Checks all towers are valid";
            this->validateAllTowersCmd->Click += gcnew System::EventHandler(this, &Form1::validateAllTowersCmd_Click);
            // 
            // validateAllRingersCmd
            // 
            this->validateAllRingersCmd->Name = L"validateAllRingersCmd";
            this->validateAllRingersCmd->Size = System::Drawing::Size(164, 22);
            this->validateAllRingersCmd->Text = L"Ringers";
            this->validateAllRingersCmd->ToolTipText = L"Checks all ringers are valid";
            this->validateAllRingersCmd->Click += gcnew System::EventHandler(this, &Form1::validateAllRingersCmd_Click);
            // 
            // validateAllMethodsCmd
            // 
            this->validateAllMethodsCmd->Name = L"validateAllMethodsCmd";
            this->validateAllMethodsCmd->Size = System::Drawing::Size(164, 22);
            this->validateAllMethodsCmd->Text = L"Methods";
            this->validateAllMethodsCmd->ToolTipText = L"Checks all methods are valid";
            this->validateAllMethodsCmd->Click += gcnew System::EventHandler(this, &Form1::validateAllMethodsCmd_Click);
            // 
            // validateAllSeriesCmd
            // 
            this->validateAllSeriesCmd->Name = L"validateAllSeriesCmd";
            this->validateAllSeriesCmd->Size = System::Drawing::Size(164, 22);
            this->validateAllSeriesCmd->Text = L"Method series";
            this->validateAllSeriesCmd->ToolTipText = L"Checks all method series are valid";
            this->validateAllSeriesCmd->Click += gcnew System::EventHandler(this, &Form1::validateAllSeriesCmd_Click);
            // 
            // validateAllCompositionsCmd
            // 
            this->validateAllCompositionsCmd->Name = L"validateAllCompositionsCmd";
            this->validateAllCompositionsCmd->Size = System::Drawing::Size(164, 22);
            this->validateAllCompositionsCmd->Text = L"Compositions";
            this->validateAllCompositionsCmd->ToolTipText = L"Checks all compositions are valid";
            this->validateAllCompositionsCmd->Click += gcnew System::EventHandler(this, &Form1::validateAllCompositionsCmd_Click);
            // 
            // deleteUnusedToolStripMenuItem
            // 
            this->deleteUnusedToolStripMenuItem->Name = L"deleteUnusedToolStripMenuItem";
            this->deleteUnusedToolStripMenuItem->Size = System::Drawing::Size(166, 22);
            this->deleteUnusedToolStripMenuItem->Text = L"Delete unused";
            this->deleteUnusedToolStripMenuItem->ToolTipText = L"Delete unused towers, ringers, methods, series and compositions.";
            this->deleteUnusedToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::deleteUnusedToolStripMenuItem_Click);
            // 
            // renumberToolStripMenuItem
            // 
            this->renumberToolStripMenuItem->Name = L"renumberToolStripMenuItem";
            this->renumberToolStripMenuItem->Size = System::Drawing::Size(166, 22);
            this->renumberToolStripMenuItem->Text = L"Reindex database";
            this->renumberToolStripMenuItem->ToolTipText = L"Rebuild the indexes and renumber all peals, towers, ringers, methods, series and "
                L"compositions";
            this->renumberToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::renumberToolStripMenuItem_Click);
            // 
            // methodsToolStripMenuItem
            // 
            methodsToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(9) {
                this->newMethodToolStripMenuItem,
                    this->importMethodToolStripMenuItem, this->importNotationToolStripMenuItem, methodMenuSeperator1, this->showMethodToolStripMenuItem,
                    this->methodSearchToolStripMenuItem, methodMenuSeperator2, this->seriesToolStripMenuItem, this->compositionToolStripMenuItem
            });
            methodsToolStripMenuItem->Name = L"methodsToolStripMenuItem";
            methodsToolStripMenuItem->Size = System::Drawing::Size(66, 20);
            methodsToolStripMenuItem->Text = L"&Methods";
            // 
            // newMethodToolStripMenuItem
            // 
            this->newMethodToolStripMenuItem->Name = L"newMethodToolStripMenuItem";
            this->newMethodToolStripMenuItem->Size = System::Drawing::Size(158, 22);
            this->newMethodToolStripMenuItem->Text = L"New";
            this->newMethodToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::newMethodToolStripMenuItem_Click);
            // 
            // importMethodToolStripMenuItem
            // 
            this->importMethodToolStripMenuItem->Name = L"importMethodToolStripMenuItem";
            this->importMethodToolStripMenuItem->Size = System::Drawing::Size(158, 22);
            this->importMethodToolStripMenuItem->Text = L"Import method";
            this->importMethodToolStripMenuItem->ToolTipText = L"Import missing method from CC Library.";
            this->importMethodToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::importMethodToolStripMenuItem_Click);
            // 
            // importNotationToolStripMenuItem
            // 
            this->importNotationToolStripMenuItem->Name = L"importNotationToolStripMenuItem";
            this->importNotationToolStripMenuItem->Size = System::Drawing::Size(158, 22);
            this->importNotationToolStripMenuItem->Text = L"Import notation";
            this->importNotationToolStripMenuItem->ToolTipText = L"Import missing place notation for methods from CC library.";
            this->importNotationToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::importNotationToolStripMenuItem_Click);
            // 
            // showMethodToolStripMenuItem
            // 
            this->showMethodToolStripMenuItem->Name = L"showMethodToolStripMenuItem";
            this->showMethodToolStripMenuItem->Size = System::Drawing::Size(158, 22);
            this->showMethodToolStripMenuItem->Text = L"View";
            this->showMethodToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::showMethodToolStripMenuItem_Click);
            // 
            // methodSearchToolStripMenuItem
            // 
            this->methodSearchToolStripMenuItem->Name = L"methodSearchToolStripMenuItem";
            this->methodSearchToolStripMenuItem->Size = System::Drawing::Size(158, 22);
            this->methodSearchToolStripMenuItem->Text = L"Search";
            this->methodSearchToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::methodSearchToolStripMenuItem_Click);
            // 
            // seriesToolStripMenuItem
            // 
            this->seriesToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
                this->viewSeriesToolStripMenuItem,
                    this->searchSeriesToolStripMenuItem
            });
            this->seriesToolStripMenuItem->Name = L"seriesToolStripMenuItem";
            this->seriesToolStripMenuItem->Size = System::Drawing::Size(158, 22);
            this->seriesToolStripMenuItem->Text = L"Series";
            // 
            // viewSeriesToolStripMenuItem
            // 
            this->viewSeriesToolStripMenuItem->Name = L"viewSeriesToolStripMenuItem";
            this->viewSeriesToolStripMenuItem->Size = System::Drawing::Size(109, 22);
            this->viewSeriesToolStripMenuItem->Text = L"View";
            this->viewSeriesToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::viewSeriesToolStripMenuItem_Click);
            // 
            // searchSeriesToolStripMenuItem
            // 
            this->searchSeriesToolStripMenuItem->Name = L"searchSeriesToolStripMenuItem";
            this->searchSeriesToolStripMenuItem->Size = System::Drawing::Size(109, 22);
            this->searchSeriesToolStripMenuItem->Text = L"Search";
            this->searchSeriesToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::searchSeriesToolStripMenuItem_Click);
            // 
            // compositionToolStripMenuItem
            // 
            this->compositionToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
                this->viewCompositionToolStripMenuItem,
                    this->searchCompositionToolStripMenuItem
            });
            this->compositionToolStripMenuItem->Name = L"compositionToolStripMenuItem";
            this->compositionToolStripMenuItem->Size = System::Drawing::Size(158, 22);
            this->compositionToolStripMenuItem->Text = L"Compositions";
            // 
            // viewCompositionToolStripMenuItem
            // 
            this->viewCompositionToolStripMenuItem->Name = L"viewCompositionToolStripMenuItem";
            this->viewCompositionToolStripMenuItem->Size = System::Drawing::Size(109, 22);
            this->viewCompositionToolStripMenuItem->Text = L"View";
            this->viewCompositionToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::viewCompositionToolStripMenuItem_Click);
            // 
            // searchCompositionToolStripMenuItem
            // 
            this->searchCompositionToolStripMenuItem->Name = L"searchCompositionToolStripMenuItem";
            this->searchCompositionToolStripMenuItem->Size = System::Drawing::Size(109, 22);
            this->searchCompositionToolStripMenuItem->Text = L"Search";
            this->searchCompositionToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::searchCompositionToolStripMenuItem_Click);
            // 
            // towersToolStripMenuItem
            // 
            towersToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(7) {
                this->newTowerToolStripMenuItem,
                    towerMenuSeperator1, this->showTowerToolStripMenuItem, this->towerSearchToolStripMenuItem, towerMenuSeperator2, this->doveToolStripMenuItem,
                    this->topTowersToolStripMenuItem
            });
            towersToolStripMenuItem->Name = L"towersToolStripMenuItem";
            towersToolStripMenuItem->Size = System::Drawing::Size(55, 20);
            towersToolStripMenuItem->Text = L"&Towers";
            towersToolStripMenuItem->DropDownOpening += gcnew System::EventHandler(this, &Form1::towersToolStripMenuItem_DropDownOpening);
            // 
            // newTowerToolStripMenuItem
            // 
            this->newTowerToolStripMenuItem->Name = L"newTowerToolStripMenuItem";
            this->newTowerToolStripMenuItem->Size = System::Drawing::Size(180, 22);
            this->newTowerToolStripMenuItem->Text = L"New";
            this->newTowerToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::newTowerToolStripMenuItem_Click);
            // 
            // showTowerToolStripMenuItem
            // 
            this->showTowerToolStripMenuItem->Name = L"showTowerToolStripMenuItem";
            this->showTowerToolStripMenuItem->Size = System::Drawing::Size(180, 22);
            this->showTowerToolStripMenuItem->Text = L"View";
            this->showTowerToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::showTowerToolStripMenuItem_Click);
            // 
            // towerSearchToolStripMenuItem
            // 
            this->towerSearchToolStripMenuItem->Name = L"towerSearchToolStripMenuItem";
            this->towerSearchToolStripMenuItem->Size = System::Drawing::Size(180, 22);
            this->towerSearchToolStripMenuItem->Text = L"Search";
            this->towerSearchToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::towerSearchToolStripMenuItem_Click);
            // 
            // towerMenuSeperator2
            // 
            towerMenuSeperator2->Name = L"towerMenuSeperator2";
            towerMenuSeperator2->Size = System::Drawing::Size(177, 6);
            // 
            // doveToolStripMenuItem
            // 
            this->doveToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {
                this->importFromDoveToolStripMenuItem,
                    this->updateFromDoveToolStripMenuItem, this->updateTowerLocationsToolBtn, this->importTenorKeysToolBtn
            });
            this->doveToolStripMenuItem->Name = L"doveToolStripMenuItem";
            this->doveToolStripMenuItem->Size = System::Drawing::Size(180, 22);
            this->doveToolStripMenuItem->Text = L"&Dove";
            // 
            // importFromDoveToolStripMenuItem
            // 
            this->importFromDoveToolStripMenuItem->Name = L"importFromDoveToolStripMenuItem";
            this->importFromDoveToolStripMenuItem->Size = System::Drawing::Size(237, 22);
            this->importFromDoveToolStripMenuItem->Text = L"Import new towers";
            this->importFromDoveToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::importFromDoveToolStripMenuItem_Click);
            // 
            // updateFromDoveToolStripMenuItem
            // 
            this->updateFromDoveToolStripMenuItem->Name = L"updateFromDoveToolStripMenuItem";
            this->updateFromDoveToolStripMenuItem->Size = System::Drawing::Size(237, 22);
            this->updateFromDoveToolStripMenuItem->Text = L"Find Dove ids";
            this->updateFromDoveToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::updateFromDoveToolStripMenuItem_Click);
            // 
            // updateTowerLocationsToolBtn
            // 
            this->updateTowerLocationsToolBtn->Name = L"updateTowerLocationsToolBtn";
            this->updateTowerLocationsToolBtn->Size = System::Drawing::Size(237, 22);
            this->updateTowerLocationsToolBtn->Text = L"Update tower locations and ids";
            this->updateTowerLocationsToolBtn->Click += gcnew System::EventHandler(this, &Form1::updateTowerLocationsToolBtn_Click);
            // 
            // importTenorKeysToolBtn
            // 
            this->importTenorKeysToolBtn->Name = L"importTenorKeysToolBtn";
            this->importTenorKeysToolBtn->Size = System::Drawing::Size(237, 22);
            this->importTenorKeysToolBtn->Text = L"Import tenor keys";
            this->importTenorKeysToolBtn->Click += gcnew System::EventHandler(this, &Form1::importTenorKeysToolBtn_Click);
            // 
            // topTowersToolStripMenuItem
            // 
            this->topTowersToolStripMenuItem->Name = L"topTowersToolStripMenuItem";
            this->topTowersToolStripMenuItem->Size = System::Drawing::Size(180, 22);
            this->topTowersToolStripMenuItem->Text = L"Top towers";
            // 
            // ringersToolStripMenuItem
            // 
            ringersToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(5) {
                this->newRingerToolStripMenuItem,
                    ringerMenuSeperator1, this->showRingerToolStripMenuItem, this->defaultRingerToolStripMenuItem, this->ringerSearchToolStripMenuItem
            });
            ringersToolStripMenuItem->Name = L"ringersToolStripMenuItem";
            ringersToolStripMenuItem->Size = System::Drawing::Size(58, 20);
            ringersToolStripMenuItem->Text = L"&Ringers";
            ringersToolStripMenuItem->DropDownOpening += gcnew System::EventHandler(this, &Form1::ringersToolStripMenuItem_DropDownOpening);
            // 
            // newRingerToolStripMenuItem
            // 
            this->newRingerToolStripMenuItem->Name = L"newRingerToolStripMenuItem";
            this->newRingerToolStripMenuItem->Size = System::Drawing::Size(146, 22);
            this->newRingerToolStripMenuItem->Text = L"New";
            this->newRingerToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::newRingerToolStripMenuItem_Click);
            // 
            // showRingerToolStripMenuItem
            // 
            this->showRingerToolStripMenuItem->Name = L"showRingerToolStripMenuItem";
            this->showRingerToolStripMenuItem->Size = System::Drawing::Size(146, 22);
            this->showRingerToolStripMenuItem->Text = L"View";
            this->showRingerToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::showRingerToolStripMenuItem_Click);
            // 
            // defaultRingerToolStripMenuItem
            // 
            this->defaultRingerToolStripMenuItem->Name = L"defaultRingerToolStripMenuItem";
            this->defaultRingerToolStripMenuItem->Size = System::Drawing::Size(146, 22);
            this->defaultRingerToolStripMenuItem->Text = L"Default ringer";
            this->defaultRingerToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::defaultRingerToolStripMenuItem_Click);
            // 
            // ringerSearchToolStripMenuItem
            // 
            this->ringerSearchToolStripMenuItem->Name = L"ringerSearchToolStripMenuItem";
            this->ringerSearchToolStripMenuItem->Size = System::Drawing::Size(146, 22);
            this->ringerSearchToolStripMenuItem->Text = L"Search";
            this->ringerSearchToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::ringerSearchToolStripMenuItem_Click);
            // 
            // menuStrip1
            // 
            menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(10) {
                this->fileMenu, this->pealsToolStripMenuItem,
                    this->associationsToolStripMenuItem, towersToolStripMenuItem, ringersToolStripMenuItem, methodsToolStripMenuItem, statisticsToolStripMenuItem,
                    helpToolStripMenuItem, toolsToolStripMenuItem, windowToolStripMenuItem
            });
            menuStrip1->Location = System::Drawing::Point(0, 0);
            menuStrip1->MdiWindowListItem = windowToolStripMenuItem;
            menuStrip1->Name = L"menuStrip1";
            menuStrip1->Size = System::Drawing::Size(1285, 24);
            menuStrip1->TabIndex = 0;
            menuStrip1->Text = L"menuStrip1";
            // 
            // fileMenu
            // 
            this->fileMenu->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(16) {
                this->newToolStripMenuItem,
                    this->openToolStripMenuItem, this->historyToolStripMenuItem, fileMenuSeperator1, this->saveToolStripMenuItem, this->saveAsToolStripMenuItem,
                    this->onlineToolStripMenuItem, fileMenuSeperator2, this->mergeToolStripMenuItem, this->importToolStripMenuItem, fileMenuSeperator3,
                    reportsToolStripMenuItem, fileMenuSeperator4, this->preferencesToolStripMenuItem, fileMenuSeperator5, this->exitToolStripMenuItem
            });
            this->fileMenu->Name = L"fileMenu";
            this->fileMenu->Size = System::Drawing::Size(37, 20);
            this->fileMenu->Text = L"&File";
            this->fileMenu->DropDownOpening += gcnew System::EventHandler(this, &Form1::fileToolStripMenuItem_DropDownOpening);
            // 
            // newToolStripMenuItem
            // 
            this->newToolStripMenuItem->Name = L"newToolStripMenuItem";
            this->newToolStripMenuItem->Size = System::Drawing::Size(135, 22);
            this->newToolStripMenuItem->Text = L"&New";
            this->newToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::newToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this->openToolStripMenuItem->Name = L"openToolStripMenuItem";
            this->openToolStripMenuItem->Size = System::Drawing::Size(135, 22);
            this->openToolStripMenuItem->Text = L"&Open";
            this->openToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::openToolStripMenuItem_Click);
            // 
            // historyToolStripMenuItem
            // 
            this->historyToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(5) {
                this->fileHistory1,
                    this->fileHistory2, this->fileHistory3, this->fileHistory4, this->fileHistory5
            });
            this->historyToolStripMenuItem->Name = L"historyToolStripMenuItem";
            this->historyToolStripMenuItem->Size = System::Drawing::Size(135, 22);
            this->historyToolStripMenuItem->Text = L"Recent files";
            // 
            // fileHistory1
            // 
            this->fileHistory1->Name = L"fileHistory1";
            this->fileHistory1->Size = System::Drawing::Size(67, 22);
            this->fileHistory1->Click += gcnew System::EventHandler(this, &Form1::fileHistory1_Click);
            // 
            // fileHistory2
            // 
            this->fileHistory2->Name = L"fileHistory2";
            this->fileHistory2->Size = System::Drawing::Size(67, 22);
            this->fileHistory2->Click += gcnew System::EventHandler(this, &Form1::fileHistory2_Click);
            // 
            // fileHistory3
            // 
            this->fileHistory3->Name = L"fileHistory3";
            this->fileHistory3->Size = System::Drawing::Size(67, 22);
            this->fileHistory3->Click += gcnew System::EventHandler(this, &Form1::fileHistory3_Click);
            // 
            // fileHistory4
            // 
            this->fileHistory4->Name = L"fileHistory4";
            this->fileHistory4->Size = System::Drawing::Size(67, 22);
            this->fileHistory4->Click += gcnew System::EventHandler(this, &Form1::fileHistory4_Click);
            // 
            // fileHistory5
            // 
            this->fileHistory5->Name = L"fileHistory5";
            this->fileHistory5->Size = System::Drawing::Size(67, 22);
            this->fileHistory5->Click += gcnew System::EventHandler(this, &Form1::fileHistory5_Click);
            // 
            // saveToolStripMenuItem
            // 
            this->saveToolStripMenuItem->Name = L"saveToolStripMenuItem";
            this->saveToolStripMenuItem->Size = System::Drawing::Size(135, 22);
            this->saveToolStripMenuItem->Text = L"&Save";
            this->saveToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this->saveAsToolStripMenuItem->Name = L"saveAsToolStripMenuItem";
            this->saveAsToolStripMenuItem->Size = System::Drawing::Size(135, 22);
            this->saveAsToolStripMenuItem->Text = L"Save &As";
            this->saveAsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::saveAsToolStripMenuItem_Click);
            // 
            // onlineToolStripMenuItem
            // 
            this->onlineToolStripMenuItem->Name = L"onlineToolStripMenuItem";
            this->onlineToolStripMenuItem->Size = System::Drawing::Size(135, 22);
            this->onlineToolStripMenuItem->Text = L"Online";
            this->onlineToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::onlineToolStripMenuItem_Click);
            // 
            // mergeToolStripMenuItem
            // 
            this->mergeToolStripMenuItem->Name = L"mergeToolStripMenuItem";
            this->mergeToolStripMenuItem->Size = System::Drawing::Size(135, 22);
            this->mergeToolStripMenuItem->Text = L"&Merge";
            this->mergeToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::mergeToolStripMenuItem_Click);
            // 
            // importToolStripMenuItem
            // 
            this->importToolStripMenuItem->Name = L"importToolStripMenuItem";
            this->importToolStripMenuItem->Size = System::Drawing::Size(135, 22);
            this->importToolStripMenuItem->Text = L"&Import";
            this->importToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::importToolStripMenuItem_Click);
            // 
            // fileMenuSeperator3
            // 
            fileMenuSeperator3->Name = L"fileMenuSeperator3";
            fileMenuSeperator3->Size = System::Drawing::Size(132, 6);
            // 
            // reportsToolStripMenuItem
            // 
            reportsToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
                this->databaseSummaryToolStripMenuItem,
                    this->associationReportToolStripMenuItem
            });
            reportsToolStripMenuItem->Name = L"reportsToolStripMenuItem";
            reportsToolStripMenuItem->Size = System::Drawing::Size(135, 22);
            reportsToolStripMenuItem->Text = L"Reports";
            // 
            // databaseSummaryToolStripMenuItem
            // 
            this->databaseSummaryToolStripMenuItem->Name = L"databaseSummaryToolStripMenuItem";
            this->databaseSummaryToolStripMenuItem->Size = System::Drawing::Size(175, 22);
            this->databaseSummaryToolStripMenuItem->Text = L"Database summary";
            this->databaseSummaryToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::databaseSummaryToolStripMenuItem_Click);
            // 
            // associationReportToolStripMenuItem
            // 
            this->associationReportToolStripMenuItem->Name = L"associationReportToolStripMenuItem";
            this->associationReportToolStripMenuItem->Size = System::Drawing::Size(175, 22);
            this->associationReportToolStripMenuItem->Text = L"Association report";
            this->associationReportToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::associationReportToolStripMenuItem_Click);
            // 
            // preferencesToolStripMenuItem
            // 
            this->preferencesToolStripMenuItem->Name = L"preferencesToolStripMenuItem";
            this->preferencesToolStripMenuItem->Size = System::Drawing::Size(135, 22);
            this->preferencesToolStripMenuItem->Text = L"Preferences";
            this->preferencesToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::preferencesToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
            this->exitToolStripMenuItem->Size = System::Drawing::Size(135, 22);
            this->exitToolStripMenuItem->Text = L"E&xit";
            this->exitToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::exitToolStripMenuItem_Click);
            // 
            // pealsToolStripMenuItem
            // 
            this->pealsToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(9) {
                this->newPealToolStripMenuItem,
                    this->bellboardToolStripMenuItem, pealMenuSeperator1, this->showPealToolStripMenuItem, this->showAllPealsMenuItem, toolStripSeparator1,
                    this->pealSearchToolStripMenuItem, pealMenuSeperator2, this->printPealsToolStripMenuItem
            });
            this->pealsToolStripMenuItem->Name = L"pealsToolStripMenuItem";
            this->pealsToolStripMenuItem->Size = System::Drawing::Size(46, 20);
            this->pealsToolStripMenuItem->Text = L"&Peals";
            // 
            // newPealToolStripMenuItem
            // 
            this->newPealToolStripMenuItem->Name = L"newPealToolStripMenuItem";
            this->newPealToolStripMenuItem->Size = System::Drawing::Size(124, 22);
            this->newPealToolStripMenuItem->Text = L"New";
            this->newPealToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::newPealToolStripMenuItem_Click);
            // 
            // bellboardToolStripMenuItem
            // 
            this->bellboardToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
                this->downloadToolStripMenuItem,
                    this->downloadByIdToolStripMenuItem, this->updateIdsAndRWRefs
            });
            this->bellboardToolStripMenuItem->Name = L"bellboardToolStripMenuItem";
            this->bellboardToolStripMenuItem->Size = System::Drawing::Size(124, 22);
            this->bellboardToolStripMenuItem->Text = L"Bellboard";
            // 
            // downloadToolStripMenuItem
            // 
            this->downloadToolStripMenuItem->Name = L"downloadToolStripMenuItem";
            this->downloadToolStripMenuItem->Size = System::Drawing::Size(196, 22);
            this->downloadToolStripMenuItem->Text = L"Search on website";
            this->downloadToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::downloadToolStripMenuItem_Click);
            // 
            // downloadByIdToolStripMenuItem
            // 
            this->downloadByIdToolStripMenuItem->Name = L"downloadByIdToolStripMenuItem";
            this->downloadByIdToolStripMenuItem->Size = System::Drawing::Size(196, 22);
            this->downloadByIdToolStripMenuItem->Text = L"Download from id";
            this->downloadByIdToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::downloadByIdToolStripMenuItem_Click);
            // 
            // updateIdsAndRWRefs
            // 
            this->updateIdsAndRWRefs->Name = L"updateIdsAndRWRefs";
            this->updateIdsAndRWRefs->Size = System::Drawing::Size(196, 22);
            this->updateIdsAndRWRefs->Text = L"Update Ids and RW refs";
            this->updateIdsAndRWRefs->Click += gcnew System::EventHandler(this, &Form1::updateIdsAndRWRefs_Click);
            // 
            // showPealToolStripMenuItem
            // 
            this->showPealToolStripMenuItem->Name = L"showPealToolStripMenuItem";
            this->showPealToolStripMenuItem->Size = System::Drawing::Size(124, 22);
            this->showPealToolStripMenuItem->Text = L"Show last";
            this->showPealToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::showPealToolStripMenuItem_Click);
            // 
            // showAllPealsMenuItem
            // 
            this->showAllPealsMenuItem->Name = L"showAllPealsMenuItem";
            this->showAllPealsMenuItem->Size = System::Drawing::Size(124, 22);
            this->showAllPealsMenuItem->Text = L"Show all";
            this->showAllPealsMenuItem->Click += gcnew System::EventHandler(this, &Form1::showAllPealsMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            toolStripSeparator1->Name = L"toolStripSeparator1";
            toolStripSeparator1->Size = System::Drawing::Size(121, 6);
            // 
            // pealSearchToolStripMenuItem
            // 
            this->pealSearchToolStripMenuItem->Name = L"pealSearchToolStripMenuItem";
            this->pealSearchToolStripMenuItem->Size = System::Drawing::Size(124, 22);
            this->pealSearchToolStripMenuItem->Text = L"Search";
            this->pealSearchToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::pealSearchToolStripMenuItem_Click);
            // 
            // pealMenuSeperator2
            // 
            pealMenuSeperator2->Name = L"pealMenuSeperator2";
            pealMenuSeperator2->Size = System::Drawing::Size(121, 6);
            // 
            // printPealsToolStripMenuItem
            // 
            this->printPealsToolStripMenuItem->Name = L"printPealsToolStripMenuItem";
            this->printPealsToolStripMenuItem->Size = System::Drawing::Size(124, 22);
            this->printPealsToolStripMenuItem->Text = L"Print";
            this->printPealsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::printPealsStripMenuItem_Click);
            // 
            // associationsToolStripMenuItem
            // 
            this->associationsToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {
                this->newAssociationToolStripMenuItem,
                    associationsMenuSeperator1, this->viewAssociationsToolStripMenuItem, this->associationSearchToolStripMenuItem
            });
            this->associationsToolStripMenuItem->Name = L"associationsToolStripMenuItem";
            this->associationsToolStripMenuItem->Size = System::Drawing::Size(85, 20);
            this->associationsToolStripMenuItem->Text = L"&Associations";
            // 
            // newAssociationToolStripMenuItem
            // 
            this->newAssociationToolStripMenuItem->Name = L"newAssociationToolStripMenuItem";
            this->newAssociationToolStripMenuItem->Size = System::Drawing::Size(180, 22);
            this->newAssociationToolStripMenuItem->Text = L"New";
            this->newAssociationToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::newAssociationToolStripMenuItem_Click);
            // 
            // associationsMenuSeperator1
            // 
            associationsMenuSeperator1->Name = L"associationsMenuSeperator1";
            associationsMenuSeperator1->Size = System::Drawing::Size(177, 6);
            // 
            // viewAssociationsToolStripMenuItem
            // 
            this->viewAssociationsToolStripMenuItem->Name = L"viewAssociationsToolStripMenuItem";
            this->viewAssociationsToolStripMenuItem->Size = System::Drawing::Size(180, 22);
            this->viewAssociationsToolStripMenuItem->Text = L"View";
            this->viewAssociationsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::viewAssociationsToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            helpToolStripMenuItem->Alignment = System::Windows::Forms::ToolStripItemAlignment::Right;
            helpToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(5) {
                this->aboutToolStripMenuItem,
                    this->resetToolStripMenuItem, this->checkForUpdatesToolStripMenuItem, this->toolStripSeparator2, this->faqToolStripMenuItem
            });
            helpToolStripMenuItem->Name = L"helpToolStripMenuItem";
            helpToolStripMenuItem->Size = System::Drawing::Size(44, 20);
            helpToolStripMenuItem->Text = L"&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this->aboutToolStripMenuItem->Name = L"aboutToolStripMenuItem";
            this->aboutToolStripMenuItem->Size = System::Drawing::Size(170, 22);
            this->aboutToolStripMenuItem->Text = L"&About";
            this->aboutToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::aboutToolStripMenuItem_Click);
            // 
            // resetToolStripMenuItem
            // 
            this->resetToolStripMenuItem->Name = L"resetToolStripMenuItem";
            this->resetToolStripMenuItem->Size = System::Drawing::Size(170, 22);
            this->resetToolStripMenuItem->Text = L"&Reset";
            this->resetToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::resetToolStripMenuItem_Click);
            // 
            // checkForUpdatesToolStripMenuItem
            // 
            this->checkForUpdatesToolStripMenuItem->Name = L"checkForUpdatesToolStripMenuItem";
            this->checkForUpdatesToolStripMenuItem->Size = System::Drawing::Size(170, 22);
            this->checkForUpdatesToolStripMenuItem->Text = L"Check for &updates";
            this->checkForUpdatesToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::checkForUpdatesToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this->toolStripSeparator2->Name = L"toolStripSeparator2";
            this->toolStripSeparator2->Size = System::Drawing::Size(167, 6);
            // 
            // faqToolStripMenuItem
            // 
            this->faqToolStripMenuItem->Name = L"faqToolStripMenuItem";
            this->faqToolStripMenuItem->Size = System::Drawing::Size(170, 22);
            this->faqToolStripMenuItem->Text = L"&FAQs";
            this->faqToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::faqToolStripMenuItem_Click);
            // 
            // windowToolStripMenuItem
            // 
            windowToolStripMenuItem->Alignment = System::Windows::Forms::ToolStripItemAlignment::Right;
            windowToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->closeAllToolStripMenuItem });
            windowToolStripMenuItem->Name = L"windowToolStripMenuItem";
            windowToolStripMenuItem->Size = System::Drawing::Size(63, 20);
            windowToolStripMenuItem->Text = L"&Window";
            // 
            // closeAllToolStripMenuItem
            // 
            this->closeAllToolStripMenuItem->MergeAction = System::Windows::Forms::MergeAction::Insert;
            this->closeAllToolStripMenuItem->Name = L"closeAllToolStripMenuItem";
            this->closeAllToolStripMenuItem->Size = System::Drawing::Size(120, 22);
            this->closeAllToolStripMenuItem->Text = L"Close &All";
            this->closeAllToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::closeAllToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(8) {
                this->toolStripProgressBar1,
                    this->pealsStatusLabel, this->associationsStatusLabel, this->towersStatusLabel, this->ringersStatusLabel, this->methodsStatusLabel,
                    this->picturesStatusLabel, this->fileNameLbl
            });
            statusStrip1->Location = System::Drawing::Point(0, 907);
            statusStrip1->Name = L"statusStrip1";
            statusStrip1->ShowItemToolTips = true;
            statusStrip1->Size = System::Drawing::Size(1285, 22);
            statusStrip1->TabIndex = 1;
            statusStrip1->Text = L"statusStrip1";
            // 
            // toolStripProgressBar1
            // 
            this->toolStripProgressBar1->Name = L"toolStripProgressBar1";
            this->toolStripProgressBar1->Size = System::Drawing::Size(100, 16);
            // 
            // pealsStatusLabel
            // 
            this->pealsStatusLabel->Name = L"pealsStatusLabel";
            this->pealsStatusLabel->Padding = System::Windows::Forms::Padding(20, 0, 0, 0);
            this->pealsStatusLabel->Size = System::Drawing::Size(63, 17);
            this->pealsStatusLabel->Text = L"0 Peals";
            this->pealsStatusLabel->Click += gcnew System::EventHandler(this, &Form1::showPealToolStripMenuItem_Click);
            // 
            // associationsStatusLabel
            // 
            this->associationsStatusLabel->Name = L"associationsStatusLabel";
            this->associationsStatusLabel->Padding = System::Windows::Forms::Padding(20, 0, 0, 0);
            this->associationsStatusLabel->Size = System::Drawing::Size(78, 17);
            this->associationsStatusLabel->Text = L"0 Pictures";
            // 
            // towersStatusLabel
            // 
            this->towersStatusLabel->Name = L"towersStatusLabel";
            this->towersStatusLabel->Padding = System::Windows::Forms::Padding(20, 0, 0, 0);
            this->towersStatusLabel->Size = System::Drawing::Size(72, 17);
            this->towersStatusLabel->Text = L"0 Towers";
            this->towersStatusLabel->Click += gcnew System::EventHandler(this, &Form1::showTowerToolStripMenuItem_Click);
            // 
            // ringersStatusLabel
            // 
            this->ringersStatusLabel->Name = L"ringersStatusLabel";
            this->ringersStatusLabel->Padding = System::Windows::Forms::Padding(20, 0, 0, 0);
            this->ringersStatusLabel->Size = System::Drawing::Size(75, 17);
            this->ringersStatusLabel->Text = L"0 Ringers";
            this->ringersStatusLabel->Click += gcnew System::EventHandler(this, &Form1::showRingerToolStripMenuItem_Click);
            // 
            // methodsStatusLabel
            // 
            this->methodsStatusLabel->Name = L"methodsStatusLabel";
            this->methodsStatusLabel->Padding = System::Windows::Forms::Padding(20, 0, 0, 0);
            this->methodsStatusLabel->Size = System::Drawing::Size(83, 17);
            this->methodsStatusLabel->Text = L"0 Methods";
            this->methodsStatusLabel->Click += gcnew System::EventHandler(this, &Form1::showMethodToolStripMenuItem_Click);
            // 
            // picturesStatusLabel
            // 
            this->picturesStatusLabel->Name = L"picturesStatusLabel";
            this->picturesStatusLabel->Padding = System::Windows::Forms::Padding(20, 0, 0, 0);
            this->picturesStatusLabel->Size = System::Drawing::Size(78, 17);
            this->picturesStatusLabel->Text = L"0 Pictures";
            this->picturesStatusLabel->Click += gcnew System::EventHandler(this, &Form1::picturesToolStripMenuItem_Click);
            // 
            // fileNameLbl
            // 
            this->fileNameLbl->Name = L"fileNameLbl";
            this->fileNameLbl->Size = System::Drawing::Size(719, 17);
            this->fileNameLbl->Spring = true;
            this->fileNameLbl->Text = L"No database open";
            this->fileNameLbl->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
            // 
            // associationSearchToolStripMenuItem
            // 
            this->associationSearchToolStripMenuItem->Name = L"associationSearchToolStripMenuItem";
            this->associationSearchToolStripMenuItem->Size = System::Drawing::Size(180, 22);
            this->associationSearchToolStripMenuItem->Text = L"Search";
            this->associationSearchToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::associationSearchToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this->AllowDrop = true;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(1285, 929);
            this->Controls->Add(statusStrip1);
            this->Controls->Add(menuStrip1);
            this->IsMdiContainer = true;
            this->KeyPreview = true;
            this->MainMenuStrip = menuStrip1;
            this->Name = L"Form1";
            this->Text = L"Duco (x32) - Ringing Database";
            this->Activated += gcnew System::EventHandler(this, &Form1::Form1_Activated);
            this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
            this->Shown += gcnew System::EventHandler(this, &Form1::Form1_Shown);
            this->DragDrop += gcnew System::Windows::Forms::DragEventHandler(this, &Form1::Form1_DragDrop);
            this->DragOver += gcnew System::Windows::Forms::DragEventHandler(this, &Form1::Form1_DragOver);
            this->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::Form1_KeyPress);
            menuStrip1->ResumeLayout(false);
            menuStrip1->PerformLayout();
            statusStrip1->ResumeLayout(false);
            statusStrip1->PerformLayout();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
};
}

