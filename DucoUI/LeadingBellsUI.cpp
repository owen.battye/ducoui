#include "DatabaseManager.h"
#include "LeadingBellsUI.h"
#include "GenericTablePrintUtil.h"
#include <Ringer.h>
#include <DatabaseSettings.h>
#include "DucoUtils.h"
#include <RingingDatabase.h>
#include <PealDatabase.h>
#include <LeadingBellData.h>
#include <Tower.h>
#include "DucoUIUtils.h"
#include <DucoEngineUtils.h>

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::Windows::Forms;

LeadingBellsUI::LeadingBellsUI(DucoUI::DatabaseManager^ theDatabase, const Duco::ObjectId& theRingerId)
    : database(theDatabase)
{
    InitializeComponent();
    ringerId = new Duco::ObjectId(theRingerId);
}

LeadingBellsUI::~LeadingBellsUI()
{
    if (components)
    {
        delete components;
    }
    delete ringerId;
}

System::Void
LeadingBellsUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void
LeadingBellsUI::printBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    DucoUIUtils::Print(this, sender, gcnew System::Drawing::Printing::PrintPageEventHandler(this, &LeadingBellsUI::printData), "bells rung", database->Database().Settings().UsePrintPreview(), false);
}

System::Void
LeadingBellsUI::printData(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args)
{
    System::Drawing::Printing::PrintDocument^ printDoc = static_cast<System::Drawing::Printing::PrintDocument^>(sender);

    System::String^ title = "Lead bells rung for " + ringerLbl->Text;
    GenericTablePrintUtil^ printUtil = gcnew GenericTablePrintUtil(database, args, title);
    printUtil->SetObjects(dataGridView, false);
    printUtil->printObject(args, printDoc->PrinterSettings);
    args->HasMorePages = false;
}

System::Void
LeadingBellsUI::LeadingBellsUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Duco::Ringer theRinger;
    if (database->FindRinger(*ringerId, theRinger, false))
    {
        ringerLbl->Text = DucoUtils::ConvertString(theRinger.FullName(false));

        std::set<Duco::LeadingBellData> sortedBellPealCounts;
        database->Database().PealsDatabase().GetLeadingBells(*ringerId, sortedBellPealCounts);

        std::set<Duco::LeadingBellData>::const_iterator it = sortedBellPealCounts.begin();
        while (it != sortedBellPealCounts.end())
        {
            DataGridViewRow^ newRow = gcnew DataGridViewRow();
            DataGridViewTextBoxCell^ towerCell = gcnew DataGridViewTextBoxCell();
            Tower theTower;
            size_t bellNumber = it->BellNumber();
            std::wstring bellName = DucoEngineUtils::ToString(bellNumber, false);
            if (database->FindTower(it->TowerId(), theTower, true))
            {
                towerCell->Value = DucoUtils::ConvertString(theTower.FullName());
                bellName = theTower.BellName(bellNumber);
            }
            newRow->Cells->Add(towerCell);

            DataGridViewTextBoxCell^ bellCell = gcnew DataGridViewTextBoxCell();
            bellCell->Value = DucoUtils::ConvertString(bellName);
            newRow->Cells->Add(bellCell);

            DataGridViewTextBoxCell^ pealCountCell = gcnew DataGridViewTextBoxCell();
            pealCountCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->PealCount(), true));
            newRow->Cells->Add(pealCountCell);

            dataGridView->Rows->Add(newRow);
            ++it;
        }
    }
}
