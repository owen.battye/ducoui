#pragma once

namespace DucoUI {

	public ref class ChooseOrGenerateRinger : public System::Windows::Forms::Form
	{
	public:
        ChooseOrGenerateRinger(DatabaseManager^ theDatabase, const std::set<Duco::RingerPealDates>& ringerIds, const Duco::Peal& thePeal, System::String^ missingRinger, System::Boolean fromPealUI);

        Duco::ObjectId SelectedId();

	protected:
        ~ChooseOrGenerateRinger();
        System::Void ChooseRinger_Load(System::Object^ sender, System::EventArgs^ e);

        void CreateRingerControls(const Duco::Peal& thePeal, const std::set<Duco::RingerPealDates>& nearMisses);
        System::Void okButton_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void newBtn_Click(System::Object^ sender, System::EventArgs^ e);


    private:
        System::Windows::Forms::Button^ okButton;
        System::Windows::Forms::Label^ descriptionLabel;
        System::Windows::Forms::Label^ ringerLbl;
        System::Windows::Forms::FlowLayoutPanel^ ringers;
        DatabaseManager^ database;
        System::Collections::Generic::List<System::Int64>^ ringerIds;
        System::String^ missingRingerName;
        System::Windows::Forms::Button^ newBtn;
        System::ComponentModel::IContainer^ components;
        System::Windows::Forms::ToolTip^ toolTip1;
    private: System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
           Duco::ObjectId* newRingerId;

#pragma region Windows Form Designer generated code
		void InitializeComponent()
		{
            this->components = (gcnew System::ComponentModel::Container());
            System::Windows::Forms::Button^ cancelBtn;
            System::Windows::Forms::Label^ label2;
            this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
            this->okButton = (gcnew System::Windows::Forms::Button());
            this->descriptionLabel = (gcnew System::Windows::Forms::Label());
            this->ringerLbl = (gcnew System::Windows::Forms::Label());
            this->ringers = (gcnew System::Windows::Forms::FlowLayoutPanel());
            this->newBtn = (gcnew System::Windows::Forms::Button());
            this->toolTip1 = (gcnew System::Windows::Forms::ToolTip(this->components));
            cancelBtn = (gcnew System::Windows::Forms::Button());
            label2 = (gcnew System::Windows::Forms::Label());
            this->tableLayoutPanel1->SuspendLayout();
            this->SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this->tableLayoutPanel1->ColumnCount = 4;
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
                100)));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->tableLayoutPanel1->Controls->Add(this->okButton, 2, 4);
            this->tableLayoutPanel1->Controls->Add(cancelBtn, 3, 4);
            this->tableLayoutPanel1->Controls->Add(this->descriptionLabel, 0, 1);
            this->tableLayoutPanel1->Controls->Add(this->ringerLbl, 0, 0);
            this->tableLayoutPanel1->Controls->Add(this->ringers, 0, 2);
            this->tableLayoutPanel1->Controls->Add(this->newBtn, 1, 4);
            this->tableLayoutPanel1->Controls->Add(label2, 0, 3);
            this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
            this->tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
            this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
            this->tableLayoutPanel1->RowCount = 4;
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
            this->tableLayoutPanel1->Size = System::Drawing::Size(454, 243);
            this->tableLayoutPanel1->TabIndex = 0;
            // 
            // okButton
            // 
            this->okButton->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->okButton->Location = System::Drawing::Point(295, 217);
            this->okButton->Name = L"okButton";
            this->okButton->Size = System::Drawing::Size(75, 23);
            this->okButton->TabIndex = 3;
            this->okButton->Text = L"Choose";
            this->toolTip1->SetToolTip(this->okButton, L"Choose the selected ringer");
            this->okButton->UseVisualStyleBackColor = true;
            this->okButton->Click += gcnew System::EventHandler(this, &ChooseOrGenerateRinger::okButton_Click);
            // 
            // cancelBtn
            // 
            cancelBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            cancelBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            cancelBtn->Location = System::Drawing::Point(376, 217);
            cancelBtn->Name = L"cancelBtn";
            cancelBtn->Size = System::Drawing::Size(75, 23);
            cancelBtn->TabIndex = 1;
            cancelBtn->Text = L"Cancel";
            cancelBtn->UseVisualStyleBackColor = true;
            // 
            // descriptionLabel
            // 
            this->descriptionLabel->AutoSize = true;
            this->tableLayoutPanel1->SetColumnSpan(this->descriptionLabel, 4);
            this->descriptionLabel->Dock = System::Windows::Forms::DockStyle::Fill;
            this->descriptionLabel->Location = System::Drawing::Point(3, 22);
            this->descriptionLabel->Margin = System::Windows::Forms::Padding(3);
            this->descriptionLabel->Name = L"descriptionLabel";
            this->descriptionLabel->Size = System::Drawing::Size(448, 13);
            this->descriptionLabel->TabIndex = 2;
            this->descriptionLabel->Text = L"Add peal description here";
            // 
            // ringerLbl
            // 
            this->ringerLbl->AutoSize = true;
            this->tableLayoutPanel1->SetColumnSpan(this->ringerLbl, 4);
            this->ringerLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            this->ringerLbl->Location = System::Drawing::Point(3, 3);
            this->ringerLbl->Margin = System::Windows::Forms::Padding(3);
            this->ringerLbl->Name = L"ringerLbl";
            this->ringerLbl->Size = System::Drawing::Size(448, 13);
            this->ringerLbl->TabIndex = 3;
            this->ringerLbl->Text = L"Could not find an exact match for Choose ringer, create new or cancel to open Pea"
                L"l window";
            // 
            // ringers
            // 
            this->ringers->AutoScroll = true;
            this->ringers->AutoSize = true;
            this->ringers->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            this->tableLayoutPanel1->SetColumnSpan(this->ringers, 4);
            this->ringers->Dock = System::Windows::Forms::DockStyle::Fill;
            this->ringers->FlowDirection = System::Windows::Forms::FlowDirection::TopDown;
            this->ringers->Location = System::Drawing::Point(3, 41);
            this->ringers->MinimumSize = System::Drawing::Size(100, 45);
            this->ringers->Name = L"ringers";
            this->ringers->Size = System::Drawing::Size(448, 138);
            this->ringers->TabIndex = 4;
            this->ringers->WrapContents = false;
            // 
            // newBtn
            // 
            this->newBtn->Location = System::Drawing::Point(214, 217);
            this->newBtn->Name = L"newBtn";
            this->newBtn->Size = System::Drawing::Size(75, 23);
            this->newBtn->TabIndex = 2;
            this->newBtn->Text = L"Generate";
            this->toolTip1->SetToolTip(this->newBtn, L"Generate a new ringer");
            this->newBtn->UseVisualStyleBackColor = true;
            this->newBtn->Click += gcnew System::EventHandler(this, &ChooseOrGenerateRinger::newBtn_Click);
            // 
            // label2
            // 
            label2->AutoSize = true;
            this->tableLayoutPanel1->SetColumnSpan(label2, 4);
            label2->Dock = System::Windows::Forms::DockStyle::Fill;
            label2->Location = System::Drawing::Point(3, 185);
            label2->Margin = System::Windows::Forms::Padding(3);
            label2->Name = L"label2";
            label2->Size = System::Drawing::Size(448, 26);
            label2->TabIndex = 7;
            label2->Text = L"Duco only automatically accepts an exact match. To improve the likelyhood of an a"
                L"utomatic match, considering adding an Aka to a ringer.";
            // 
            // ChooseRinger
            // 
            this->AcceptButton = this->okButton;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->CancelButton = cancelBtn;
            this->ClientSize = System::Drawing::Size(454, 243);
            this->Controls->Add(this->tableLayoutPanel1);
            this->Name = L"ChooseRinger";
            this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
            this->Text = L"Choose ringer";
            this->Load += gcnew System::EventHandler(this, &ChooseOrGenerateRinger::ChooseRinger_Load);
            this->tableLayoutPanel1->ResumeLayout(false);
            this->tableLayoutPanel1->PerformLayout();
            this->ResumeLayout(false);

        }
#pragma endregion
    };
}
