#include "DatabaseManager.h"
#include "ProgressWrapper.h"

#include "BellboardUpdate.h"
#include "SoundUtils.h"
#include "SystemDefaultIcon.h"
#include <RingingDatabase.h>
#include <StatisticFilters.h>

using namespace DucoUI;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;

BellboardUpdate::BellboardUpdate(DucoUI::DatabaseManager^ theDatabase)
    : database(theDatabase)
{
    InitializeComponent();
    progressWrapper = new DucoUI::ProgressCallbackWrapper(this);
    cancellationFlag = new bool();
    (*cancellationFlag) = false;
}

BellboardUpdate::!BellboardUpdate()
{
    delete progressWrapper;
    delete cancellationFlag;
}


BellboardUpdate::~BellboardUpdate()
{
    this->!BellboardUpdate();
    if (components)
    {
        delete components;
    }
}

System::Void
BellboardUpdate::BellboardUpdate_Load(System::Object^ sender, System::EventArgs^ e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
}

System::Void
BellboardUpdate::closeBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (backgroundWorker->IsBusy)
    {
        (*cancellationFlag) = true;
    }
    else
    {
        Close();
    }
}

System::Void
BellboardUpdate::startBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (database->NothingInEditMode() && !backgroundWorker->IsBusy)
    {
        Duco::StatisticFilters filters(database->Database());
        (*cancellationFlag) = false;
        closeBtn->Enabled = false;
        startBtn->Visible = false;
        stopBtn->Visible = true;
        backgroundWorker->RunWorkerAsync();
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
BellboardUpdate::stopBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    (*cancellationFlag) = true;
}

System::Void
BellboardUpdate::backgroundWorker_DoWork(System::Object^ sender, System::ComponentModel::DoWorkEventArgs^ e)
{
    size_t noOfUpdatedObjects = database->CheckBellBoardForUpdatedReferences(progressWrapper, this->rwPagesOnly->Checked, *this->cancellationFlag);
    if (noOfUpdatedObjects > 0)
    {
        SoundUtils::PlayFinishedSound();
        e->Result = noOfUpdatedObjects;
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
BellboardUpdate::backgroundWorker_ProgressChanged(System::Object^ sender, System::ComponentModel::ProgressChangedEventArgs^ e)
{
    progressBar->Value = e->ProgressPercentage;
}

System::Void
BellboardUpdate::backgroundWorker_RunWorkerCompleted(System::Object^ sender, System::ComponentModel::RunWorkerCompletedEventArgs^ e)
{
    progressBar->Value = 0;
    closeBtn->Enabled = true;
    stopBtn->Visible = false;
    resultLbl->Text = "";
    if (e->Result != nullptr)
    {
        resultLbl->Text = e->Result->ToString() + " updated";
    }
    else
    {
        resultLbl->Text = "Completed - no updates";
    }
}

void
BellboardUpdate::Initialised()
{
    backgroundWorker->ReportProgress(0);
}

void
BellboardUpdate::Step(int progressPercent)
{
    backgroundWorker->ReportProgress(progressPercent);
}

void
BellboardUpdate::Complete()
{
}
