#include "DatabaseManager.h"

#include <AssociationDatabase.h>
#include <Association.h>
#include "DatabaseCallbackArgs.h"
#include "WindowsSettings.h"
#include <DucoConfiguration.h>
#include <RingingDatabase.h>
#include "DucoUtils.h"
#include <MethodNotationDatabaseUpdater.h>
#include "SoundUtils.h"
#include "RingingDatabaseObserver.h"
#include "Composition.h"
#include "CompositionDatabase.h"
#include "Ringer.h"
#include "RingerDatabase.h"
#include "Method.h"
#include "MethodDatabase.h"
#include "MethodSeries.h"
#include "MethodSeriesDatabase.h"
#include "Peal.h"
#include "PealDatabase.h"
#include "Picture.h"
#include "PictureDatabase.h"
#include "Tower.h"
#include "TowerDatabase.h"
#include <InvalidDatabaseVersionException.h>
#include <DucoEngineUtils.h>
#include "DucoCommon.h"
#include <StatisticFilters.h>
#include "DucoUILog.h"
#include "DucoWindowState.h"
#include "CheckLatestVersion.h"
#include "PrintAssociationReportUtil.h"
#include "ProgressWrapper.h"
#include "Form1.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::IO;
using namespace System::Collections;
using namespace System::Collections::Generic;
using namespace System::ComponentModel;
using namespace System::Windows::Forms;

DatabaseManager::DatabaseManager(DucoUI::Form1^ observer, System::Windows::Forms::IWin32Window^ newOwner)
    : owner(newOwner), importExportCallback(nullptr), mainForm(observer)
{
    settings = gcnew DucoUI::WindowsSettings(newOwner);
    editingPealNo = new Duco::ObjectId();
    editingMethodNo = new Duco::ObjectId();
    editingMethodSeriesNo = new Duco::ObjectId();
    editingTowerNo = new Duco::ObjectId();
    editingRingerNo = new Duco::ObjectId();
    editingCompositionNo = new Duco::ObjectId();
    editingAssociationNo = new Duco::ObjectId();
    config = new Duco::DucoConfiguration();
    config->Load(WindowsSettings::ConfigDir());

    progressWrapper = new ImportExportProgressWrapper(this);
    observerList = gcnew System::Collections::ArrayList(10);
    AddObserver(observer);
    backgroundWorker = (gcnew BackgroundWorker());
    backgroundWorker->WorkerReportsProgress = true;
    backgroundWorker->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &DatabaseManager::ProcessData);
    backgroundWorker->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &DatabaseManager::backgroundWorker_RunWorkerCompleted);
    backgroundWorker->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &DatabaseManager::backgroundWorker_ProgressChanged);
}

DatabaseManager::!DatabaseManager()
{
    delete progressWrapper;
    delete editingPealNo;
    delete editingMethodNo;
    delete editingMethodSeriesNo;
    delete editingTowerNo;
    delete editingRingerNo;
    delete editingCompositionNo;
    delete editingAssociationNo;
    delete database;
    config->Save();
    delete config;
}

DatabaseManager::~DatabaseManager()
{
    _ASSERT(observerList->Count == 0);
    this->!DatabaseManager();
    delete observerList;
}

System::Void
DatabaseManager::AddObserver(RingingDatabaseObserver^ obs)
{
    observerList->Add(obs);
}

System::Void
DatabaseManager::RemoveObserver(RingingDatabaseObserver^ obs)
{
    observerList->Remove(obs);
}

System::Void
DatabaseManager::NotifyImportComplete()
{
    importFromOtherFormatInProgress = false;
    CallObservers(EImportComplete, TObjectType::ENon, KNoId, KNoId);
}

unsigned int
DatabaseManager::ImportMethodPlaceNotations(Duco::ProgressCallback* const callback, System::String^ methodFileName, const std::map<std::wstring, Duco::ObjectId>& methodsToUpdate)
{
    std::string methodsFileName;
    DucoUtils::ConvertString(methodFileName, methodsFileName);
    Duco::MethodNotationDatabaseUpdater methodDatabase(database->MethodsDatabase(), methodsFileName.c_str(), callback, methodsToUpdate);
    methodDatabase.ReadMethods();

    if (methodDatabase.NoOfMethodsUpdated() > 0)
    {
        CallObservers(EUpdated, TObjectType::EMethod, KNoId, KNoId);
    }
    if (methodDatabase.ErrorParsing())
    {
        SoundUtils::PlayErrorSound();
    }
    return methodDatabase.NoOfMethodsUpdated();
}

System::Void
DatabaseManager::CallObservers(DucoUI::TEventType eventId, Duco::TObjectType type, Duco::ObjectId id, Duco::ObjectId id2)
{
    int count = observerList->Count;
    while (count > 0)
    {
        RingingDatabaseObserver^ observer = static_cast<RingingDatabaseObserver^>(observerList[count-1]);
        try
        {
            observer->DatabaseChanged(eventId, type, id, id2);
        }
        catch (Exception^ ex)
        {
            DucoUILog::PrintError("DatabaseManager::CallObservers");
            DucoUILog::PrintError(ex->ToString());
            observerList->Remove(observer);
            return;
        }
        --count;
    }
}

System::Void
DatabaseManager::ClearDatabase(System::Boolean quarterPealDb, System::Boolean createDefaults)
{
    if (database != NULL)
    {
        databaseFilename = "";
        database->ClearDatabase(quarterPealDb, createDefaults);
        CallObservers(ECleared, TObjectType::ENon, KNoId, KNoId);
    }
    else
    {
        database = new RingingDatabase();
        databaseFilename = ""; 
    }
}

Duco::RingingDatabase&
DatabaseManager::Database()
{
    if (!database)
        CreateDatabase(true);

    return *database;
}

System::String^
DatabaseManager::BackupFileName()
{
    System::String^ backupFileName = "";
    if (DatabaseOpen() && databaseFilename != nullptr && databaseFilename->Length > 0)
    {
        std::string dbfilename;
        DucoUtils::ConvertString(databaseFilename, dbfilename);
        std::string dbbackupfilename (database->BackupFileName(dbfilename.c_str()));
        backupFileName = DucoUtils::ConvertString(dbbackupfilename);
    }
    return backupFileName;
}

System::Void
DatabaseManager::HideBackupFile()
{
    String^ backupFileName = BackupFileName();
    if (File::Exists(backupFileName))
    {
        try
        {
            File::SetAttributes(backupFileName, FileAttributes::Hidden);
        }
        catch (Exception^ ex)
        {
            DucoUILog::PrintError("DatabaseManager::HideBackupFile");
            DucoUILog::PrintError(ex->ToString());
        }
    }
}

System::Void
DatabaseManager::DeleteDatabase()
{
    observerList->Clear();
    delete database;
    database = NULL;
    databaseFilename = "";
}

System::Void
DatabaseManager::CreateDatabase(System::Boolean pealDatabase)
{
    if (database != NULL)
    {
        database->ClearDatabase(!pealDatabase, true);
    }
    else
    {
        database = new RingingDatabase();
        database->ClearDatabase(!pealDatabase, true);
    }
    databaseFilename = ""; 
    CallObservers(ECleared, TObjectType::ENon, KNoId, KNoId);
}

System::Boolean
DatabaseManager::Internalise(ImportExportProgressCallback* newCallback, String^ fileName)
{
    if (backgroundWorker->IsBusy)
        return false;

    importExportCallback = newCallback;
    if (!database)
    {
        database = new RingingDatabase();
    }
    else
    {
        database->ClearDatabase(false, false);
    }
    databaseFilename = fileName;

    DatabaseCallbackArgs^ newWorkerArgs = gcnew DatabaseCallbackArgs(true);
    newWorkerArgs->fileName = fileName;

    import = true;
    backgroundWorker->RunWorkerAsync(newWorkerArgs);

    return true;
}

System::Boolean
DatabaseManager::Externalise(Duco::ImportExportProgressCallback* newCallback, bool& fileNameValid)
{
    return Externalise(databaseFilename, newCallback, fileNameValid);
}

System::Boolean
DatabaseManager::Externalise(String^ newFileName, Duco::ImportExportProgressCallback* newCallback, bool& fileNameValid)
{
    if (backgroundWorker->IsBusy)
        return false;

    importExportCallback = newCallback;
    int lastbackSlash = newFileName->LastIndexOf("\\");
    String^ directory = newFileName->Substring(0, lastbackSlash);
    if (System::IO::Directory::Exists(directory))
    {
        fileNameValid = true;
        if (newFileName->EndsWith(".duc"))
        {
            databaseFilename = newFileName;
        }

        DatabaseCallbackArgs^ newWorkerArgs = gcnew DatabaseCallbackArgs(false);
        newWorkerArgs->fileName = newFileName;

        import = false;
        backgroundWorker->RunWorkerAsync(newWorkerArgs);

        return true;
    }
    else
    {
        fileNameValid = false;
    }
    return false;
}

System::String^
DatabaseManager::RingerName(const Duco::ObjectId& ringerId)
{
    System::String^ ringerName = "";
    Duco::Ringer returnRinger;
    if (FindRinger(ringerId, returnRinger, false))
    {
        ringerName = DucoUtils::ConvertString(returnRinger.FullName(database->Settings().LastNameFirst()));
    }
    return ringerName;
}

System::String^
DatabaseManager::RingerName(const Duco::ObjectId& ringerId, const Duco::PerformanceDate& pealDate)
{
    System::String^ ringerName = "";
    Duco::Ringer returnRinger;
    if (FindRinger(ringerId, returnRinger, false))
    {
        ringerName = DucoUtils::ConvertString(returnRinger.FullName(pealDate, database->Settings().LastNameFirst()));
    }
    return ringerName;
}

Duco::ObjectId
DatabaseManager::AddRinger(Ringer& newRinger)
{
    Duco::ObjectId newRingerId = database->RingersDatabase().AddObject(newRinger);
    if (newRingerId.ValidId())
    {
        CallObservers(EAdded, TObjectType::ERinger, newRingerId, KNoId);
    }
    return newRingerId;
}

Duco::ObjectId
DatabaseManager::AddRinger(System::String^ newRingerName)
{
    std::wstring ringerName;
    DucoUtils::ConvertString(newRingerName, ringerName);
    Duco::ObjectId newRingerId = database->RingersDatabase().AddRinger(ringerName);
    if (newRingerId.ValidId())
    {
        CallObservers(EAdded, TObjectType::ERinger, newRingerId, KNoId);
    }
    return newRingerId;
}

Duco::ObjectId
DatabaseManager::SuggestRingers(System::String^ newRingerName, const Duco::PerformanceDate& performanceDate, std::set<Duco::RingerPealDates>& nearMatches)
{
    std::wstring ringerName;
    DucoUtils::ConvertString(newRingerName, ringerName);
    return database->SuggestRingers(ringerName, performanceDate, true, nearMatches);
}

System::Collections::Generic::List<Int64>^
DatabaseManager::SuggestAllRingers(System::String^ newRingerName)
{
    System::Collections::Generic::List<Int64>^ ringerIds = gcnew System::Collections::Generic::List<Int64>();

    std::wstring ringerName;
    DucoUtils::ConvertString(newRingerName, ringerName);
    std::set<Duco::ObjectId> natriveRingerIds;
    database->RingersDatabase().SuggestAllRingers(ringerName, natriveRingerIds);
    for (const auto& ringerId : natriveRingerIds)
    {
        ringerIds->Add(ringerId.Id());
    }


    return ringerIds;
}

System::Boolean
DatabaseManager::DeleteRinger(const Duco::ObjectId& ringerId, bool suspendObservers)
{
    if (ringerId == (*editingRingerNo))
    {
        if (database->RingersDatabase().DeleteObject(ringerId))
        {
            editingRingerNo->ClearId();
            if (!suspendObservers)
            {
                CallObservers(EDeleted, TObjectType::ERinger, ringerId, KNoId);
            }
            return true;
        }
    }
    return false;
}

System::Boolean
DatabaseManager::UpdateRinger(const Ringer& newRinger, bool notifyObservers)
{
    if (newRinger.Id() == *editingRingerNo)
    {
        if (database->RingersDatabase().UpdateObject(newRinger))
        {
            if (notifyObservers)
                CallObservers(EUpdated, TObjectType::ERinger, newRinger.Id(), KNoId);
            editingRingerNo->ClearId();
            return true;
        }
    }
    return false;
}

System::Boolean
DatabaseManager::FirstRinger(Duco::ObjectId& objectId, System::Boolean setIndex)
{
    return database->RingersDatabase().FirstObject(objectId, setIndex);
}

System::Boolean
DatabaseManager::LastRinger(Duco::ObjectId& objectId)
{
    return database->RingersDatabase().LastObject(objectId);
}

System::Boolean
DatabaseManager::FindRinger(const Duco::ObjectId& objectId, Duco::Ringer& returnRinger, Boolean setIndex)
{
    const Duco::Ringer* const foundRinger = database->RingersDatabase().FindRinger(objectId, setIndex);
    if (foundRinger != NULL)
    {
        returnRinger = *foundRinger;
        return true;
    }
    return false;
}

System::Boolean
DatabaseManager::NextRinger(Duco::ObjectId& objectId, System::Boolean forwards, System::Boolean setIndex)
{
    return database->RingersDatabase().NextObject(objectId, forwards, setIndex);
}

System::Boolean
DatabaseManager::ForwardMultipleRingers(Duco::ObjectId& objectId, System::Boolean forwards)
{
    return database->RingersDatabase().ForwardMultipleObjects(objectId, forwards);
}

System::Boolean
DatabaseManager::HasConducted(const Duco::ObjectId& objectId)
{
    StatisticFilters filters(*database);
    filters.SetConductor(true, objectId);

    return database->PealsDatabase().AnyMatch(filters);
}

System::Boolean
DatabaseManager::FindRingerIdDuringDownload(System::String^ ringerName, Duco::ObjectId& ringerId)
{
    if (config->DisableFeaturesForPerformance())
        return false; // enable turning this off for performance reasons.

    std::wstring theRingerName;
    DucoUtils::ConvertString(ringerName, theRingerName);
    return database->RingersDatabase().FindRingerIdDuringDownload(theRingerName, ringerId);
}


System::Boolean
DatabaseManager::ReplaceRinger(const Duco::ObjectId& oldRingerId, const Duco::ObjectId& newRingerId, bool deleteOldRinger)
{
    bool ringerReplaced = database->ReplaceRinger(oldRingerId, newRingerId, deleteOldRinger);
    if (ringerReplaced)
    {
        if (deleteOldRinger)
        {
            CallObservers(EDeleted, TObjectType::ERinger, oldRingerId, newRingerId);
        }
        CallObservers(EUpdated, TObjectType::EPeal, KNoId, KNoId);
    }
    return ringerReplaced;
}

System::Boolean
DatabaseManager::LinkRingers(const Duco::ObjectId& ringerIdOne, const Duco::ObjectId& ringerIdTwo)
{
    if (database->RingersDatabase().LinkRingers(ringerIdOne, ringerIdTwo))
    {
        CallObservers(EUpdated, TObjectType::ERinger, ringerIdOne, KNoId);
        CallObservers(EUpdated, TObjectType::ERinger, ringerIdTwo, KNoId);
        return true;
    }
    SoundUtils::PlayErrorSound();
    return false;
}

System::Boolean
DatabaseManager::ReplaceTower(const Duco::ObjectId& oldTowerId, const Duco::ObjectId& newTowerId)
{
    bool towerReplaced = database->ReplaceTower(oldTowerId, newTowerId);
    if (towerReplaced)
    {
        CallObservers(EDeleted, TObjectType::ETower, oldTowerId, KNoId);
        CallObservers(EUpdated, TObjectType::EPeal, KNoId, KNoId);
    }
    return towerReplaced;
}

System::String^
DatabaseManager::TowerName(const Duco::ObjectId& towerId)
{
    const Duco::Tower* const theTower = database->TowersDatabase().FindTower(towerId, true);
    System::String^ towerName = "";
    if (theTower != NULL)
    {
        towerName = DucoUtils::ConvertString(theTower->FullName());
    }

    return towerName;
}

System::Boolean
DatabaseManager::SetRingerGender(const Duco::ObjectId& ringerId, bool male)
{
    if (!editingRingerNo->ValidId() && database->RingersDatabase().SetRingerGender(ringerId, male))
    {
        CallObservers(EUpdated, TObjectType::ERinger, ringerId, KNoId);
        return true;
    }
    return false;
}

System::Boolean
DatabaseManager::AddTower(const Duco::DoveObject& newTower)
{
    Duco::ObjectId newTowerId = database->TowersDatabase().AddTower(newTower);
    if (newTowerId.ValidId())
    {
        CallObservers(EAdded, TObjectType::ETower, newTowerId, KNoId);
        return true;
    }
    return false;
}

System::Boolean
DatabaseManager::AddTower(Tower& newTower)
{
    if (database->TowersDatabase().AddObject(newTower).ValidId())
    {
        CallObservers(EAdded, TObjectType::ETower, newTower.Id(), KNoId);
        return true;
    }
    return false;
}

Duco::ObjectId
DatabaseManager::AddNewTower(Tower& newTower)
{
    Duco::ObjectId newTowerId = database->TowersDatabase().AddObject(newTower);
    if (newTowerId.ValidId())
        CallObservers(EAdded, TObjectType::ETower, newTowerId, KNoId);
    return newTowerId;
}

System::Boolean
DatabaseManager::UpdateTower(const Tower& tower, System::Boolean notifyObservers)
{
    if (tower.Id() == *editingTowerNo)
    {
        if (database->TowersDatabase().UpdateObject(tower))
        {
            if (notifyObservers)
                CallObservers(EUpdated, TObjectType::ETower, tower.Id(), KNoId);
            editingTowerNo->ClearId();
            return true;
        }
    }
    return false;
}

System::Void
DatabaseManager::ClearInvalidTenorKeys(Duco::ProgressCallback* callback)
{
    if (!DatabaseOpen() || InternaliseExternaliseInProgress() || editingTowerNo->ValidId())
    {
        SoundUtils::PlayErrorSound();
    }
    else
    {
        if (database->TowersDatabase().ClearInvalidTenorKeys(callback))
        {
            CallObservers(EUpdated, TObjectType::ETower, KNoId, KNoId);
        }
    }
}

System::Void
DatabaseManager::SetTowersWithHandbellPealsAsHandbells()
{
    if (!DatabaseOpen() || InternaliseExternaliseInProgress() || editingTowerNo->ValidId())
    {
        SoundUtils::PlayErrorSound();
    }
    else
    {
        if (database->SetTowersWithHandbellPealsAsHandbells())
        {
            CallObservers(EUpdated, TObjectType::ETower, KNoId, KNoId);
            SoundUtils::PlayFinishedSound();
        }
    }
}

System::Boolean
DatabaseManager::DeleteTower(const Duco::ObjectId& towerId, System::Boolean suspendObservers)
{
    if (towerId == *editingTowerNo)
    {
        if (database->TowersDatabase().DeleteObject(towerId))
        {
            editingTowerNo->ClearId();
            if (!suspendObservers)
            {
                CallObservers(EDeleted, TObjectType::ETower, towerId, KNoId);
            }
            return true;
        }
    }
    return false;
}

System::Boolean
DatabaseManager::DeleteRing(const Duco::ObjectId& towerId, const Duco::ObjectId& ringId, System::Boolean suspendObservers)
{
    if (towerId == *editingTowerNo)
    {
        if (database->TowersDatabase().DeleteRing(towerId, ringId))
        {
            editingTowerNo->ClearId();
            if (!suspendObservers)
            {
                CallObservers(EUpdated, TObjectType::ETower, towerId, KNoId);
            }
            return true;
        }
    }
    return false;
}

System::Boolean
DatabaseManager::FirstTower(Duco::ObjectId& objectId, System::Boolean setIndex)
{
    return database->TowersDatabase().FirstObject(objectId, setIndex);
}

System::Boolean
DatabaseManager::LastTower(Duco::ObjectId& objectId)
{
    return database->TowersDatabase().LastObject(objectId);
}

System::Boolean
DatabaseManager::FindTower(const Duco::ObjectId& objectId, Duco::Tower& theTower, System::Boolean setIndex)
{
    const Duco::Tower* const foundTower = database->TowersDatabase().FindTower(objectId, setIndex);
    if (foundTower != NULL)
    {
        theTower = *foundTower;
        return true;
    }
    return false;
}

System::Boolean
DatabaseManager::NextTower(Duco::ObjectId& objectId, System::Boolean forwards, System::Boolean setIndex)
{
    return database->TowersDatabase().NextObject(objectId, forwards, setIndex);
}

System::Boolean
DatabaseManager::ForwardMultipleTowers(Duco::ObjectId& objectId, System::Boolean forwards)
{
    return database->TowersDatabase().ForwardMultipleObjects(objectId, forwards);
}

unsigned int
DatabaseManager::NumberOfBellsRungInTower(const Duco::ObjectId& towerId)
{
    return database->PealsDatabase().NumberOfBellsRungInTower(towerId);
}

unsigned int
DatabaseManager::NumberOfPealsRungOnRing(const Duco::ObjectId& towerId, const ObjectId& ringId)
{
    StatisticFilters filters(*database);
    filters.SetTower(true, towerId);
    if (ringId.ValidId())
    {
        filters.SetRing(true, towerId);
    }

    return database->PealsDatabase().PerformanceInfo(filters).TotalPeals();
}

System::Boolean
DatabaseManager::MergeTowerRings(const Duco::ObjectId& towerId, const Duco::Tower& otherTower)
{
    return database->TowersDatabase().MergeTowerRings(towerId, otherTower);
}

Duco::ObjectId
DatabaseManager::AddMethod(Method& newMethod)
{
    Duco::ObjectId newMethodId = database->MethodsDatabase().AddObject(newMethod);
    if (newMethodId.ValidId())
    {
        CallObservers(EAdded, TObjectType::EMethod, newMethodId, KNoId);
    }
    return newMethodId;
}

Duco::ObjectId
DatabaseManager::AddMethod(System::String^ fullMethodName)
{
    std::wstring tempMethodName;
    DucoUtils::ConvertString(fullMethodName, tempMethodName);

    Duco::ObjectId methodId = database->MethodsDatabase().AddMethod(tempMethodName);
    if (methodId.ValidId())
    {
        CallObservers(EAdded, TObjectType::EMethod, methodId, KNoId);
    }
    return methodId;
}

System::Boolean
DatabaseManager::MethodNameMatchesOrder(System::String^ fullMethodName, unsigned int noOfBells)
{
    std::wstring tempMethodName;
    DucoUtils::ConvertString(fullMethodName, tempMethodName);
    return database->MethodsDatabase().MethodNameMatchesOrderInName(tempMethodName, noOfBells);
}

System::Boolean
DatabaseManager::UpdateMethod(const Method& method, System::Boolean notifyObservers)
{
    if (*editingMethodNo == method.Id())
    {
        if (database->MethodsDatabase().UpdateObject(method))
        {
            editingMethodNo->ClearId();
            if (notifyObservers)
                CallObservers(EUpdated, TObjectType::EMethod, method.Id(), KNoId);
            return true;
        }
    }
    return false;
}

System::Boolean
DatabaseManager::DeleteMethod(const Duco::ObjectId& methodId, System::Boolean suspendObservers)
{
    if (methodId == *editingMethodNo)
    {
        if (database->MethodsDatabase().DeleteObject(methodId))
        {
            editingMethodNo->ClearId();
            if (!suspendObservers)
            {
                CallObservers(EDeleted, TObjectType::EMethod, methodId, KNoId);
            }
            return true;
        }
    }
    return false;
}

System::Boolean
DatabaseManager::FirstMethod(Duco::ObjectId& objectId, System::Boolean setIndex)
{
    return database->MethodsDatabase().FirstObject(objectId, setIndex);
}

System::Boolean
DatabaseManager::LastMethod(Duco::ObjectId& objectId)
{
    return database->MethodsDatabase().LastObject(objectId);
}

System::Boolean
DatabaseManager::FindMethod(const Duco::ObjectId& objectId, Duco::Method& theMethod, System::Boolean setIndex)
{
    const Duco::Method* const theFoundMethod = database->MethodsDatabase().FindMethod(objectId, setIndex);
    if (theFoundMethod != NULL)
    {
        theMethod = *theFoundMethod;
        return true;
    }
    return false;
}

System::Boolean
DatabaseManager::FindMethod(unsigned int noOfBells, const std::wstring& placeNotation, Duco::Method& theMethod)
{
    const Duco::Method* const theFoundMethod = database->MethodsDatabase().FindMethod(noOfBells, placeNotation);
    if (theFoundMethod != NULL)
    {
        theMethod = *theFoundMethod;
        return true;
    }
    return false;
}

System::Boolean
DatabaseManager::NextMethod(Duco::ObjectId& objectId, System::Boolean forwards, System::Boolean setIndex)
{
    return database->MethodsDatabase().NextObject(objectId, forwards, setIndex);
}

System::Boolean
DatabaseManager::ForwardMultipleMethods(Duco::ObjectId& objectId, System::Boolean forwards)
{
    return database->MethodsDatabase().ForwardMultipleObjects(objectId, forwards);
}

System::Boolean
DatabaseManager::UpdateNotation(const Duco::ObjectId& methodId, const std::wstring& newFullNotation)
{
    return database->MethodsDatabase().UpdateNotation(methodId, newFullNotation);
}

System::Boolean
DatabaseManager::SetNumberOfLeads(const Duco::ObjectId& methodId, unsigned int noOfLeads)
{
    return database->MethodsDatabase().SetNumberOfLeads(methodId, noOfLeads);
}

unsigned int 
DatabaseManager::HighestValidOrderNumber()
{
    return database->MethodsDatabase().HighestValidOrderNumber();
}

unsigned int 
DatabaseManager::LowestValidOrderNumber()
{
    return database->MethodsDatabase().LowestValidOrderNumber();
}

unsigned int 
DatabaseManager::LowestValidOrderNumber(const Duco::ObjectId& towerId, const Duco::ObjectId& ringId)
{
    return database->PealsDatabase().LowestValidOrderNumber(towerId, ringId);
}

System::Boolean
DatabaseManager::FindComposition(const Duco::ObjectId& objectId, Duco::Composition& theComposition, System::Boolean setIndex)
{
    const Duco::Composition* const composition = database->CompositionsDatabase().FindComposition(objectId, setIndex);
    if (composition == NULL)
        return false;

    theComposition = *composition;
    return true;
}

System::String^
DatabaseManager::FindCompositionFullName(const Duco::ObjectId& objectId)
{
    System::String^ compositionFullName = "";
    Duco::Composition theComposition(-1, database->MethodsDatabase());
    if (FindComposition(objectId, theComposition, false))
    {
        std::wstring title;
        std::wstring composer;
        theComposition.FullName(*database, title, composer);
        compositionFullName = DucoUtils::ConvertString(title);
        if (composer.length() > 0)
        {
            if (title.length() > 0)
            {
                compositionFullName += ", by ";
            }
            compositionFullName += DucoUtils::ConvertString(composer);
        }
    }
    return compositionFullName;
}

System::Boolean
DatabaseManager::FirstComposition(Duco::ObjectId& objectId, System::Boolean setIndex)
{
    return database->CompositionsDatabase().FirstObject(objectId, setIndex);
}

System::Boolean
DatabaseManager::LastComposition(Duco::ObjectId& objectId)
{
    return database->CompositionsDatabase().LastObject(objectId);
}

System::Boolean
DatabaseManager::NextComposition(Duco::ObjectId& objectId, System::Boolean forwards, System::Boolean setIndex)
{
    return database->CompositionsDatabase().NextObject(objectId, forwards, setIndex);
}

System::Boolean
DatabaseManager::ForwardMultipleCompositions(Duco::ObjectId& objectId, System::Boolean forwards)
{
    return database->CompositionsDatabase().ForwardMultipleObjects(objectId, forwards);
}

Duco::ObjectId
DatabaseManager::AddComposition(Duco::Composition& newComposition)
{
    Duco::ObjectId newCompositionId = database->CompositionsDatabase().AddObject(newComposition);
    if (newCompositionId.ValidId())
        CallObservers(EAdded, TObjectType::EComposition, newCompositionId, KNoId);

    return newCompositionId;
}

System::Boolean
DatabaseManager::UpdateComposition(const Duco::Composition& composition, System::Boolean notifyObservers)
{
    if (*editingCompositionNo == composition.Id())
    {
        if (database->CompositionsDatabase().UpdateObject(composition))
        {
            editingCompositionNo->ClearId();
            if (notifyObservers)
                CallObservers(EUpdated, TObjectType::EComposition, composition.Id(), KNoId);
            return true;
        }
    }
    return false;
}

System::Boolean
DatabaseManager::DeleteComposition(const Duco::ObjectId& compositionId, System::Boolean suspendObservers)
{
    if (compositionId == *editingCompositionNo)
    {
        if (database->CompositionsDatabase().DeleteObject(compositionId))
        {
            editingCompositionNo->ClearId();
            if (!suspendObservers)
            {
                CallObservers(EDeleted, TObjectType::EComposition, compositionId, KNoId);
            }
            return true;
        }
    }
    return false;
}

System::Boolean
DatabaseManager::FindAssociation(const Duco::ObjectId& objectId, Duco::Association& theAssociation, System::Boolean setIndex)
{
    const Duco::Association* const association = database->AssociationsDatabase().FindAssociation(objectId, setIndex);
    if (association == NULL)
        return false;

    theAssociation = *association;
    return true;
}

System::Boolean
DatabaseManager::FirstAssociation(Duco::ObjectId& objectId, System::Boolean setIndex)
{
    return database->AssociationsDatabase().FirstObject(objectId, setIndex);
}

System::Boolean
DatabaseManager::LastAssociation(Duco::ObjectId& objectId)
{
    return database->AssociationsDatabase().LastObject(objectId);
}

System::Boolean
DatabaseManager::NextAssociation(Duco::ObjectId& objectId, System::Boolean forwards, System::Boolean setIndex)
{
    return database->AssociationsDatabase().NextObject(objectId, forwards, setIndex);
}

System::Boolean
DatabaseManager::ForwardMultipleAssociation(Duco::ObjectId& objectId, System::Boolean forwards)
{
    return database->AssociationsDatabase().ForwardMultipleObjects(objectId, forwards, 10);
}

System::String^
DatabaseManager::FindAssociationName(const Duco::ObjectId& objectId)
{
    const Duco::Association* const theAssociation = database->AssociationsDatabase().FindAssociation(objectId, false);
    if (theAssociation != NULL)
    {
        return DucoUtils::ConvertString(theAssociation->Name());
    }
    return "Missing Association";
}

Duco::ObjectId
DatabaseManager::AddAssociation(Duco::Association& newAssociation)
{
    Duco::ObjectId newAssociationId = database->AssociationsDatabase().AddObject(newAssociation);
    if (newAssociationId.ValidId())
        CallObservers(EAdded, TObjectType::EAssociationOrSociety, newAssociationId, KNoId);

    return newAssociationId;
}

Duco::ObjectId
DatabaseManager::AddAssociation(System::String^ newAssociationName)
{
    std::wstring tempName;
    DucoUtils::ConvertString(newAssociationName, tempName);
    Duco::ObjectId newAssociationId = database->AssociationsDatabase().CreateAssociation(tempName);
    if (newAssociationId.ValidId())
        CallObservers(EAdded, TObjectType::EAssociationOrSociety, newAssociationId, KNoId);

    return newAssociationId;
}

System::Boolean
DatabaseManager::DeleteAssociation(const Duco::ObjectId& associationId, System::Boolean suspendObservers)
{
    if (associationId == *editingAssociationNo)
    {
        if (database->AssociationsDatabase().DeleteObject(associationId))
        {
            editingAssociationNo->ClearId();
            if (!suspendObservers)
            {
                CallObservers(EDeleted, TObjectType::EAssociationOrSociety, associationId, KNoId);
            }
            return true;
        }
    }
    return false;
}

System::Boolean
DatabaseManager::UpdateAssociation(const Duco::Association& association, System::Boolean notifyObservers)
{
    if (*editingAssociationNo == association.Id())
    {
        if (database->AssociationsDatabase().UpdateObject(association))
        {
            editingAssociationNo->ClearId();
            if (notifyObservers)
                CallObservers(EUpdated, TObjectType::EAssociationOrSociety, association.Id(), KNoId);
            return true;
        }
    }
    return false;
}
System::Collections::Generic::List<System::UInt64>^
DatabaseManager::PealsInMethod(const Duco::ObjectId& methodId, Duco::PerformanceDate& firstPealDate)
{
    System::Collections::Generic::List<System::UInt64>^ performanceIds = gcnew System::Collections::Generic::List<System::UInt64>();
    std::set<Duco::ObjectId> pealIds;
    StatisticFilters filters(*database);
    filters.SetMethod(true, methodId);

    firstPealDate = database->PealsDatabase().AllMatches(filters, pealIds).DateOfFirstPeal();
    DucoUtils::Convert(pealIds, performanceIds);
    
    return performanceIds;
}

System::Boolean
DatabaseManager::FirstPealInMethod(const Duco::ObjectId& methodId, Duco::PerformanceDate& firstPealDate)
{
    StatisticFilters filters(*database);
    filters.SetMethod(true, methodId);
    Duco::PealLengthInfo info = database->PealsDatabase().PerformanceInfo(filters);
    firstPealDate = info.DateOfFirstPeal();

    return info.TotalPeals() > 0;
}

Duco::ObjectId
DatabaseManager::AddPeal(Peal& newPeal)
{
    Duco::ObjectId newPealId = database->PealsDatabase().AddObject(newPeal);
    if (newPealId.ValidId())
        CallObservers(EAdded, TObjectType::EPeal, newPealId, KNoId);

    return newPealId;
}

System::Void
DatabaseManager::DeletePeal(const Duco::ObjectId& pealId, System::Boolean suspendObservers)
{
    if (pealId == *editingPealNo)
    {
        if (database->PealsDatabase().DeleteObject(pealId))
        {
            editingPealNo->ClearId();
            if (!suspendObservers)
            {
                CallObservers(EDeleted, TObjectType::EPeal, pealId, KNoId);
            }
        }
    }
}

System::Boolean
DatabaseManager::UpdatePeal(const Peal& newPeal, System::Boolean notifyObservers)
{
    if (*editingPealNo == newPeal.Id())
    {
        if (database->PealsDatabase().UpdateObject(newPeal))
        {
            editingPealNo->ClearId();
            if (notifyObservers)
                CallObservers(EUpdated, TObjectType::EPeal, newPeal.Id(), KNoId);
            return true;
        }
    }
    return false;
}

System::Boolean
DatabaseManager::FirstPeal(Duco::ObjectId& objectId, System::Boolean setIndex)
{
    return database->PealsDatabase().FirstObject(objectId, setIndex);
}

System::Boolean
DatabaseManager::LastPeal(Duco::ObjectId& objectId)
{
    return database->PealsDatabase().LastObject(objectId);
}

System::String^
DatabaseManager::LastDateString()
{
    System::String^ returnVal = "";
    ObjectId objectId;
    if (database->PealsDatabase().LastObject(objectId))
    {
        Duco::Peal thePeal;
        if (FindPeal(objectId, thePeal, false))
        {
            returnVal = "Up to and including ";
            returnVal += DucoUtils::ConvertString(thePeal.Date(), true);
        }
    }
    return returnVal;
}

System::Boolean
DatabaseManager::FindPeal(const Duco::ObjectId& objectId, Duco::Peal& thePeal, System::Boolean setIndex)
{
    const Duco::Peal* const foundPeal = database->PealsDatabase().FindPeal(objectId, setIndex);
    if (foundPeal == NULL)
        return false;

    thePeal = *foundPeal;

    return true;
}

Duco::ObjectId
DatabaseManager::FindPeal(const Duco::PerformanceDate& pealDate)
{
    return database->PealsDatabase().FindPeal(pealDate);
}

System::String^
DatabaseManager::SummaryTitle(const Duco::ObjectId& pealId)
{
    const Duco::Peal* const thePeal = database->PealsDatabase().FindPeal(pealId, true);
    if (thePeal != NULL)
    {
        std::wstring summaryTitle;
        thePeal->SummaryTitle(summaryTitle, *database);
        return DucoUtils::ConvertString(summaryTitle);
    }
    return "";
}

System::String^
DatabaseManager::BellboardId(const Duco::ObjectId& pealId)
{
    const Duco::Peal* const thePeal = database->PealsDatabase().FindPeal(pealId);
    if (thePeal != NULL)
    {
        return DucoUtils::ConvertString(thePeal->BellBoardId());
    }
    return "";
}

System::Boolean
DatabaseManager::AddWebReferencesToPeal(const Duco::ObjectId& similarId, System::String^ bellboardId, const std::wstring& ringingWorldReference)
{
    if (StartEdit(TObjectType::EPeal, similarId))
    {
        Duco::Peal thePeal;
        if (FindPeal(similarId, thePeal, false))
        {
            std::wstring bellboardIdStr;
            DucoUtils::ConvertString(bellboardId, bellboardIdStr);
            thePeal.SetBellBoardId(bellboardIdStr);
            if (ringingWorldReference.size() > 0 && !thePeal.ValidRingingWorldLink())
            {
                thePeal.SetRingingWorldReference(ringingWorldReference);
            }
            if (UpdatePeal(thePeal, true))
            {
                return true;
            }
        }
    }
    return false;
}

System::Boolean
DatabaseManager::ForwardMultiplePeals(Duco::ObjectId& objectId, System::Boolean forwards)
{
    return database->PealsDatabase().ForwardMultipleObjects(objectId, forwards);
}

System::Boolean
DatabaseManager::NextPeal(Duco::ObjectId& objectId, System::Boolean forwards, System::Boolean setIndex)
{
    return database->PealsDatabase().NextObject(objectId, forwards, setIndex);
}

System::Void
DatabaseManager::GetAllComposersByPealCount(std::map<std::wstring, Duco::PealLengthInfo>& sortedComposerCounts, bool ignoreBracketed, const Duco::StatisticFilters& filters)
{
    return database->PealsDatabase().GetAllComposersByPealCount(filters, ignoreBracketed, sortedComposerCounts);
}

size_t
DatabaseManager::NoOfPealsAsConductor(const Duco::ObjectId& ringerId, const Duco::PerformanceDate& dateUntil)
{
    StatisticFilters filters(*database);
    filters.SetRinger(true, ringerId);
    filters.SetEndDate(true, dateUntil);

    return database->PealsDatabase().PerformanceInfo(filters).TotalPeals();
}

size_t
DatabaseManager::NoOfPealsInTower(const Duco::ObjectId& towerId, const Duco::ObjectId& upToAndIncludePealId)
{
    Duco::StatisticFilters filters (*database);
    filters.SetTower(true, towerId);
    return database->PealsDatabase().PerformanceInfo(filters, upToAndIncludePealId).TotalPeals();
}

System::Boolean
DatabaseManager::FindPealsWithUnpaidFees(const Duco::ObjectId& feePayerId, const Duco::ObjectId& associationId, unsigned int year, std::set<Duco::UnpaidPealFeeData>& pealIds)
{
    return database->PealsDatabase().FindPealsWithUnpaidFees(feePayerId, associationId, year, pealIds);
}

System::Boolean
DatabaseManager::FindOrderNumber(System::String^ name, unsigned int& order, bool& dualOrder)
{
    std::wstring stageName;
    DucoUtils::ConvertString(name, stageName);
    return database->MethodsDatabase().FindOrderNumber(stageName, order, dualOrder);
}

System::Boolean
DatabaseManager::FindOrderName(unsigned int order, System::String^% newName)
{
    std::wstring newNameStr;
    if (database->MethodsDatabase().FindOrderName(order, newNameStr))
    {
        newName = DucoUtils::ConvertString(newNameStr);
        return true;
    }
    newName = "";    
    return false;
}

System::Boolean
DatabaseManager::AddOrderName(unsigned int order, System::String^% newName)
{
    std::wstring newOrderName;
    DucoUtils::ConvertString(newName, newOrderName);
    if (database->MethodsDatabase().AddOrderName(order, newOrderName))
    {
        CallObservers(EAdded, TObjectType::EOrderName, order, KNoId);
        return true;
    }
    return false;
}

void 
DatabaseManager::UpdateOrderName(unsigned int order, System::String^ newName, System::Boolean notifyObservers)
{
    std::wstring orderName;
    DucoUtils::ConvertString(newName, orderName);
    
    if (database->MethodsDatabase().UpdateOrderName(order, orderName) && notifyObservers)
        CallObservers(EUpdated, TObjectType::EOrderName, order, KNoId);
}

System::Boolean
DatabaseManager::RemoveOrderName(unsigned int order)
{
    if (database->MethodsDatabase().RemoveOrderName(order))
    {
        CallObservers(EDeleted, TObjectType::EOrderName, order, KNoId);
        return true;
    }
    return false;
}

System::Void
DatabaseManager::GetAllMethodOrders(std::set<unsigned int>& methodOrders)
{
    database->MethodsDatabase().GetAllMethodOrders(methodOrders);
}

System::Void
DatabaseManager::GetAllOrders(std::vector<unsigned int>& orders)
{
    database->MethodsDatabase().GetAllOrders(orders);
}

Duco::ObjectId
DatabaseManager::AddMethodSeries(Duco::MethodSeries& newMethodSeries)
{
    Duco::ObjectId newSeriesId = database->MethodSeriesDatabase().AddObject(newMethodSeries);
    if (newSeriesId.ValidId())
        CallObservers(EAdded, TObjectType::EMethodSeries, newSeriesId, KNoId);

    return newSeriesId;
}

System::Boolean
DatabaseManager::UpdateMethodSeries(const Duco::MethodSeries& methodSeries, System::Boolean notifyObservers)
{
    if (*editingMethodSeriesNo == methodSeries.Id())
    {
        if (database->MethodSeriesDatabase().UpdateObject(methodSeries))
        {
            editingMethodSeriesNo->ClearId();
            if (notifyObservers)
                CallObservers(EUpdated, TObjectType::EMethodSeries, methodSeries.Id(), KNoId);
            return true;
        }
    }
    return false;
}

System::Boolean
DatabaseManager::DeleteMethodSeries(const Duco::ObjectId& methodSeriesId, System::Boolean suspendObservers)
{
    if (methodSeriesId == *editingMethodSeriesNo)
    {
        if (database->MethodSeriesDatabase().DeleteObject(methodSeriesId))
        {
            editingMethodSeriesNo->ClearId();
            if (!suspendObservers)
            {
                CallObservers(EDeleted, TObjectType::EMethodSeries, methodSeriesId, KNoId);
            }
            return true;
        }
    }
    return false;
}

System::Void
DatabaseManager::RebuildDatabase(Duco::RenumberProgressCallback* callback)
{
    if (NothingInEditMode())
    {
        if (database->RebuildDatabase(callback))
        {
            CallObservers(EUpdated, TObjectType::EPeal, KNoId, KNoId);
        }
    }
}

System::Void
DatabaseManager::SwapPeals(const Duco::ObjectId& pealId1, const Duco::ObjectId& pealId2, unsigned int selectedIndex)
{
    if (!editingPealNo->ValidId())
    {
        if (database->PealsDatabase().SwapPeals(pealId1, pealId2, selectedIndex, true))
        {
            CallObservers(EUpdated, TObjectType::EPeal, pealId1, KNoId);
            CallObservers(EUpdated, TObjectType::EPeal, pealId2, KNoId);
        }
    }
}

System::Boolean
DatabaseManager::ReplaceAssociation(Duco::ProgressCallback* callback, const Duco::ObjectId& oldAssociation, const Duco::ObjectId& newAssociation)
{
    bool changesMade (false);
    if (NothingInEditMode())
    {
        changesMade = database->PealsDatabase().ReplaceAssociation(callback, oldAssociation, newAssociation);
        if (changesMade)
        {
            CallObservers(EUpdated, TObjectType::EPeal, KNoId, KNoId);
        }
    }
    return changesMade;
}

System::Boolean
DatabaseManager::CheckDefaultRinger()
{
    if (!database->Settings().DefaultRingerSet())
    {
        NoDefaultRinger();
        return false;
    }
    return true;
}

System::Void
DatabaseManager::NoDefaultRinger()
{
    MessageBox::Show(owner, "Please set a default ringer in settings", "No default ringer", MessageBoxButtons::OK, MessageBoxIcon::Warning );
}

System::Void
DatabaseManager::ProcessData(System::Object^ sender, System::ComponentModel::DoWorkEventArgs^ e)
{
    BackgroundWorker^ worker = dynamic_cast<BackgroundWorker^>(sender);
    DatabaseCallbackArgs^ args = dynamic_cast<DatabaseCallbackArgs^>(e->Argument);
    try
    {
        if (args->Internalising())
        {
            ProcessDataInternalise(worker, e);
        }
        else
        {
            ProcessDataExternalise(worker, e);
        }
    }
    catch (Exception^ ex)
    {
        DucoUILog::PrintError("DatabaseManager::ProcessData");
        DucoUILog::PrintError(ex->ToString());
    }
}

System::Void
DatabaseManager::ProcessDataExternalise(BackgroundWorker^ worker, System::ComponentModel::DoWorkEventArgs^ e)
{
    DatabaseCallbackArgs^ args = dynamic_cast<DatabaseCallbackArgs^>(e->Argument);
    DatabaseCallbackArgs^ resultArgs = gcnew DatabaseCallbackArgs(args->Internalising());

    std::string tempFileName;
    DucoUtils::ConvertString(args->fileName, tempFileName);
    if (args->fileName->EndsWith(".xml"))
    {
        std::string originalFilename;
        DucoUtils::ConvertString(Filename(), originalFilename);
        if (!database->ExternaliseToXml(tempFileName.c_str(), progressWrapper, originalFilename, config->ExportDucoIdsWithXml()))
        {
            resultArgs->error = -1;
        }
    }
    else if (args->fileName->EndsWith(".csv"))
    {
        std::string originalFilename;
        DucoUtils::ConvertString(Filename(), originalFilename);
        if (!database->ExternaliseToCsv(tempFileName.c_str(), progressWrapper, originalFilename))
        {
            resultArgs->error = -1;
        }
    }
    else if (!database->Externalise(tempFileName.c_str(), progressWrapper))
    {
        resultArgs->error = -1;
    }
    e->Result = resultArgs;

}

System::Void
DatabaseManager::ProcessDataInternalise(BackgroundWorker^ worker, System::ComponentModel::DoWorkEventArgs^ e)
{
    DatabaseCallbackArgs^ args = dynamic_cast<DatabaseCallbackArgs^>(e->Argument);

    std::string tempFileName;
    DucoUtils::ConvertString(args->fileName, tempFileName);

    DatabaseCallbackArgs^ resultArgs = gcnew DatabaseCallbackArgs(args->Internalising());
    try
    {
        unsigned int databaseVersion (0);
        database->Internalise(tempFileName.c_str(), progressWrapper, databaseVersion);
        resultArgs->error = 0;
    }
    catch (Duco::InvalidDatabaseVersionException& ex)
    {
        resultArgs->error = ex.ErrorCode();
        DucoUILog::PrintError("DatabaseManager::ProcessDataInternalise database version not supported");
        DucoUILog::PrintError(ex.ErrorCode().ToString());
    }
    catch (Exception^ ex)
    {
        resultArgs->error = -1;
        DucoUILog::PrintError("DatabaseManager::ProcessDataInternalise general exception reading database");
        DucoUILog::PrintError(ex->ToString());

    }
    e->Result = resultArgs;
}

System::Void
DatabaseManager::backgroundWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    try
    {
        DatabaseCallbackArgs^ args = dynamic_cast<DatabaseCallbackArgs^>(e->UserState);
        if (e->ProgressPercentage == 0 && !args->currentObjectId->ValidId())
        {
            importExportCallback->InitialisingImportExport(args->Internalising(), args->databaseVersion, args->totalObjects);
            if (args->Internalising())
                CallObservers(ECleared, TObjectType::ENon, KNoId, KNoId);
        }
        else if (e->ProgressPercentage < 100)
        {
            importExportCallback->ObjectProcessed(args->Internalising(), args->objectType, *args->currentObjectId, e->ProgressPercentage, args->objectsOfThisType);
        }
    }
    catch (Exception^ ex)
    {
        DucoUILog::PrintError("DatabaseManager::backgroundWorker_ProgressChanged");
        DucoUILog::PrintError(ex->ToString());
        MessageBox::Show("Error during database event");
    }
}

System::DateTime^
DatabaseManager::DatabaseCreatedDate()
{
    time_t dbTime = database->CreatedDate();
    struct tm *ptm = gmtime(&dbTime);

    return DateTime(ptm->tm_year + 1900, ptm->tm_mon+1, ptm->tm_mday);
}

System::Void
DatabaseManager::backgroundWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    DUCOUILOGDEBUG("DatabaseManager::backgroundWorker_RunWorkerCompleted");
    DatabaseCallbackArgs^ args = dynamic_cast<DatabaseCallbackArgs^>(e->Result);

    DucoUI::CompletedEventArgs^ eventArgs = gcnew DucoUI::CompletedEventArgs();
    eventArgs->Import = args->Internalising();
    mainForm->OnImportExportCompleteReached(eventArgs);
    DUCOUILOGDEBUG("DatabaseManager::backgroundWorker_RunWorkerCompleted Local");
    switch (args->error)
    {
    default:
        {
            if (args->Internalising())
            {
                CallObservers(EImportComplete, TObjectType::ENon, KNoId, KNoId);
            }
            else
            {
                HideBackupFile();
            }
        }
        break;

    case KInvalidDatabaseVersionError:
        MessageBox::Show(owner, "Update Duco at http://" + KWebSite, "Database version not supported", MessageBoxButtons::OK, MessageBoxIcon::Error);
        ClearDatabase(false, true);
        break;

    case -1:
        MessageBox::Show(owner, "Cannot open file", "Error opening file", MessageBoxButtons::OK, MessageBoxIcon::Error);
        ClearDatabase(false, true);
        break;
    }
    DUCOUILOGDEBUG("DatabaseManager::backgroundWorker_RunWorkerCompleted END");
}

System::Void
DatabaseManager::InitialisingImportExport(System::Boolean internalising, unsigned int versionNumber,
                                          size_t totalNumberOfObjects)
{
    DatabaseCallbackArgs^ args = gcnew DatabaseCallbackArgs(internalising);
    args->databaseVersion = versionNumber;
    args->totalObjects = totalNumberOfObjects;
    backgroundWorker->ReportProgress(0, args);
}

System::Void
DatabaseManager::ObjectProcessed(System::Boolean internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
{
    DatabaseCallbackArgs^ args = gcnew DatabaseCallbackArgs(internalising);
    *args->currentObjectId = objectId;
    args->objectType = objectType;
    args->objectsOfThisType = numberInThisSubDatabase;
    backgroundWorker->ReportProgress(totalPercentage, args);
}

System::Void
DatabaseManager::ImportExportComplete(System::Boolean internalising)
{
    DatabaseCallbackArgs^ args = gcnew DatabaseCallbackArgs(internalising);
    backgroundWorker->ReportProgress(100, args);
}

System::Void
DatabaseManager::ImportExportFailed(System::Boolean internalising, int errorCode)
{
    DatabaseCallbackArgs^ args = gcnew DatabaseCallbackArgs(internalising);
    backgroundWorker->ReportProgress(100, args);
}

System::Boolean
DatabaseManager::DataChanged()
{
    if (!database)
        return false;
    else if (backgroundWorker->IsBusy && !import)
        return false;
    return database->DataChanged();
}

System::Void
DatabaseManager::Reset()
{
    editingPealNo->ClearId();
    editingMethodNo->ClearId();
    editingMethodSeriesNo->ClearId();
    editingTowerNo->ClearId();
    editingRingerNo->ClearId();
    editingCompositionNo->ClearId();
    editingAssociationNo->ClearId();
}

System::Boolean
DatabaseManager::StartEdit(Duco::TObjectType objectType, const Duco::ObjectId& objectId)
{
    if (InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
        return false;
    }
    if (!objectId.ValidId())
    { // Dont care if we're editing two things, and one is new.
        return true;
    }
    System::Boolean returnVal (false);
    switch(objectType)
    {
    case TObjectType::EPeal:
        if (!editingPealNo->ValidId())
        {
            *editingPealNo = objectId;
            returnVal = true;
        }
        break;

    case TObjectType::EMethod:
        if (!editingMethodNo->ValidId())
        {
            *editingMethodNo = objectId;
            returnVal = true;
        }
        break;
    case TObjectType::EMethodSeries:
        if (!editingMethodSeriesNo->ValidId())
        {
            *editingMethodSeriesNo = objectId;
            returnVal = true;
        }
        break;
    case TObjectType::ETower:
        if (!editingTowerNo->ValidId())
        {
            *editingTowerNo = objectId;
            returnVal = true;
        }
        break;
    case TObjectType::ERinger:
        if (!editingRingerNo->ValidId())
        {
            *editingRingerNo = objectId;
            returnVal = true;
        }
        break;
    case TObjectType::EComposition:
        if (!editingCompositionNo->ValidId())
        {
            *editingCompositionNo = objectId;
            returnVal = true;
        }
        break;
    case TObjectType::EAssociationOrSociety:
        if (!editingAssociationNo->ValidId())
        {
            *editingAssociationNo = objectId;
            returnVal = true;
        }
        break;
    default:
        break;
    }
    return returnVal;
}

System::Boolean
DatabaseManager::NothingInEditMode()
{
    if (!editingPealNo->ValidId() && !editingMethodNo->ValidId() && !editingMethodSeriesNo->ValidId() &&
        !editingTowerNo->ValidId() && !editingRingerNo->ValidId() && !editingCompositionNo->ValidId() && !editingAssociationNo->ValidId())
    {
        return true;
    }
    return false;
}


System::Void
DatabaseManager::CancelEdit(Duco::TObjectType objectType, const Duco::ObjectId& objectId)
{
    if (objectId.ValidId())
    {
        switch (objectType)
        {
        case TObjectType::EPeal:
            if (objectId == *editingPealNo)
                editingPealNo->ClearId();
            break;
        case TObjectType::EMethod:
            if (objectId == *editingMethodNo)
                editingMethodNo->ClearId();
            break;
        case TObjectType::EMethodSeries:
            if (objectId == *editingMethodSeriesNo)
                editingMethodSeriesNo->ClearId();
            break;
        case TObjectType::ETower:
            if (objectId == *editingTowerNo)
                editingTowerNo->ClearId();
            break;
        case TObjectType::ERinger:
            if (objectId == *editingRingerNo)
                editingRingerNo->ClearId();
            break;
        case TObjectType::EComposition:
            if (*editingCompositionNo == objectId)
                editingCompositionNo->ClearId();
            break;
        case TObjectType::EAssociationOrSociety:
            if (*editingAssociationNo == objectId)
                editingAssociationNo->ClearId();
            break;
        default:
            break;
        }
    }
}

System::Boolean
DatabaseManager::FirstMethodSeries(Duco::ObjectId& objectId, System::Boolean setIndex)
{
    return database->MethodSeriesDatabase().FirstObject(objectId, setIndex);
}

System::Boolean
DatabaseManager::LastMethodSeries(Duco::ObjectId& objectId)
{
    return database->MethodSeriesDatabase().LastObject(objectId);
}

System::Boolean
DatabaseManager::FindMethodSeries(const Duco::ObjectId& objectId, Duco::MethodSeries& theSeries, System::Boolean setIndex)
{
    const Duco::MethodSeries* const foundSeries = database->MethodSeriesDatabase().FindMethodSeries(objectId, setIndex);
    if (foundSeries != NULL)
    {
        theSeries = *foundSeries;
        return true;
    }
    return false;
}

System::Boolean
DatabaseManager::NextMethodSeries(Duco::ObjectId& objectId, System::Boolean forwards, System::Boolean setIndex)
{
    return database->MethodSeriesDatabase().NextObject(objectId, forwards, setIndex);
}

System::Boolean
DatabaseManager::ForwardMultipleMethodSeries(Duco::ObjectId& objectId, System::Boolean forwards)
{
    return database->MethodSeriesDatabase().ForwardMultipleObjects(objectId, forwards);
}

System::Collections::Generic::List<System::UInt64>^
DatabaseManager::PealsInSeries(const Duco::ObjectId& seriesId, Duco::PerformanceDate& pealDate)
{
    System::Collections::Generic::List<System::UInt64>^ performanceIds = gcnew System::Collections::Generic::List<System::UInt64>();
    std::set<Duco::ObjectId> pealIds;
    if (database->PealsDatabase().PealsInSeries(seriesId, pealDate, pealIds))
    {
        DucoUtils::Convert(pealIds, performanceIds);
    }

    return performanceIds;
}

System::Boolean
DatabaseManager::LastPealInMethodSeries(const Duco::ObjectId& seriesId, Duco::PerformanceDate& lastPealDate)
{
    Duco::PerformanceDate completedDate;
    Duco::PealLengthInfo allPealsInfo;
    size_t unrungMethods;
    const MethodSeries* const theSeries = database->MethodSeriesDatabase().FindMethodSeries(seriesId);
    if (theSeries != NULL)
    {
        if (theSeries->SeriesCompletedDate(*database, completedDate, unrungMethods, allPealsInfo))
        {
            lastPealDate = allPealsInfo.DateOfLastPeal();
            return true;
        }
    }
    return false;
}

System::Boolean
DatabaseManager::SuggestMethodSeries(System::String^ methods, unsigned int order, Duco::ObjectId& methodSeriesId, std::set<Duco::ObjectId>& methodIds)
{
    std::wstring methodsStr;
    DucoUtils::ConvertString(methods, methodsStr);
    return database->SuggestMethodSeries(methodsStr, order, methodSeriesId, methodIds);
}

DucoUI::WindowsSettings^
DatabaseManager::WindowsSettings()
{
    return settings;
}

Duco::DatabaseSettings&
DatabaseManager::Settings()
{
    return database->Settings();
}

Duco::DucoConfiguration&
DatabaseManager::Config()
{
    return *config;
}

System::String^
DatabaseManager::DoveDataFile()
{
    return DucoUtils::ConvertString(config->DoveDataFile());
}

System::Void
DatabaseManager::SetDoveDataFile(System::String^ newValue)
{
    std::string temp;
    DucoUtils::ConvertString(newValue, temp);
    config->SetDoveDataFile(temp);
}

System::String^
DatabaseManager::XmlMethodCollectionFile()
{
    return DucoUtils::ConvertString(config->XmlMethodCollectionFile());
}

System::Void
DatabaseManager::SetXmlMethodCollectionFile(System::String^ newValue)
{
    std::string temp;
    DucoUtils::ConvertString(newValue, temp);
    config->SetXmlMethodCollectionFile(temp);
}


System::Boolean
DatabaseManager::UniqueObject(const RingingObject& object, TObjectType objectType)
{
    return !FindDuplicateObject(object, objectType).ValidId();
}

Duco::ObjectId
DatabaseManager::FindDuplicateObject(const RingingObject& object, TObjectType objectType)
{
    switch (objectType)
    {
    case TObjectType::EPeal:
        return database->PealsDatabase().FindDuplicateObject(object, *database);
    case TObjectType::EMethod:
        return database->MethodsDatabase().FindDuplicateObject(object, *database);
    case TObjectType::EMethodSeries:
        return database->MethodSeriesDatabase().FindDuplicateObject(object, *database);
    case TObjectType::ETower:
        return database->TowersDatabase().FindDuplicateObject(object, *database);
    case TObjectType::ERinger:
        return database->RingersDatabase().FindDuplicateObject(object, *database);
    case TObjectType::EPicture:
        return database->PicturesDatabase().FindDuplicateObject(object, *database);
    case TObjectType::EAssociationOrSociety:
        return database->AssociationsDatabase().FindDuplicateObject(object, *database);
    default:
        return KNoId;
    }
}

System::Boolean
DatabaseManager::ObjectExists(Duco::TObjectType objectType, const Duco::ObjectId& id)
{
    switch (objectType)
    {
    case TObjectType::EPeal:
        return database->PealsDatabase().ObjectExists(id);
    case TObjectType::EMethod:
        return database->MethodsDatabase().ObjectExists(id);
    case TObjectType::EMethodSeries:
        return database->MethodSeriesDatabase().ObjectExists(id);
    case TObjectType::ETower:
        return database->TowersDatabase().ObjectExists(id);
    case TObjectType::ERinger:
        return database->RingersDatabase().ObjectExists(id);
    case TObjectType::EComposition:
        return database->CompositionsDatabase().ObjectExists(id);
    case TObjectType::EPicture:
        return database->PicturesDatabase().ObjectExists(id);
    default:
        break;
    }
    return false;
}

size_t
DatabaseManager::NumberOfValidObjects(Duco::TObjectType objectType)
{
    switch (objectType)
    {
    case TObjectType::EPeal:
    {
        StatisticFilters filters(*database);
        return database->PealsDatabase().PerformanceInfo(filters).TotalPeals();
    }

    default:
        return NumberOfObjects(objectType);
    }
}


size_t
DatabaseManager::NumberOfObjects(Duco::TObjectType objectType)
{
    switch (objectType)
    {
    case TObjectType::EPeal:
        return database->PealsDatabase().NumberOfObjects();
    case TObjectType::EMethod:
        return database->MethodsDatabase().NumberOfObjects();
    case TObjectType::EMethodSeries:
        return database->MethodSeriesDatabase().NumberOfObjects();
    case TObjectType::ETower:
        return database->TowersDatabase().NumberOfObjects();
    case TObjectType::ERinger:
        return database->RingersDatabase().NumberOfObjects();
    case TObjectType::EOrderName:
        return database->MethodsDatabase().NoOfOrderNames();
    case TObjectType::EComposition:
        return database->CompositionsDatabase().NumberOfObjects();
    case TObjectType::EPicture:
        return database->PicturesDatabase().NumberOfObjects();
    case TObjectType::EAssociationOrSociety:
        return database->AssociationsDatabase().NumberOfObjects();
    default:
        return -1;
    }
}

System::Boolean
DatabaseManager::IsDualOrder(System::String^% orderNameToFind)
{
    std::wstring orderNameToFindStr;
    DucoUtils::ConvertString(orderNameToFind->ToLower()->Trim(), orderNameToFindStr);

    return database->MethodsDatabase().DualOrder(orderNameToFindStr);
}

System::Boolean
DatabaseManager::CheckRebuildRecommended()
{
    return (!config->DisableFeaturesForPerformance() && database->RebuildRecommended());
}

Duco::ObjectId
DatabaseManager::SuggestAssociation(System::String^ name)
{
    std::wstring tempName;
    DucoUtils::ConvertString(name, tempName);
    return database->AssociationsDatabase().SuggestAssociation(tempName);
}

System::Boolean
DatabaseManager::RemoveDuplicateAssociations()
{
    if (database->RemoveDuplicateAssociations())
    {
        CallObservers(TEventType::EDeleted, TObjectType::EAssociationOrSociety, KNoId, KNoId);
        CallObservers(TEventType::EUpdated, TObjectType::EPeal, KNoId, KNoId);
        return true;
    }
    return false;
}

size_t
DatabaseManager::NumberOfPealsByAssociation(const Duco::ObjectId& associationId)
{
    StatisticFilters filters(*database);
    filters.SetAssociation(true, associationId);
    return database->PealsDatabase().PerformanceInfo(filters).TotalPeals();
}

Duco::ObjectId
DatabaseManager::SuggestTower(System::String^ title, unsigned int noOfBells)
{
    std::wstring tempTitle;
    DucoUtils::ConvertString(title, tempTitle);
    return database->TowersDatabase().SuggestTower(tempTitle, noOfBells);
}

Duco::ObjectId
DatabaseManager::SuggestMethod(System::String^ methodName)
{
    std::wstring tempName;
    DucoUtils::ConvertString(methodName, tempName);
    return database->MethodsDatabase().SuggestMethod(tempName);
}

unsigned int
DatabaseManager::FindOrderNumber(System::String^% methodName)
{
    std::wstring tempName;
    std::wstring processedName;
    DucoUtils::ConvertString(methodName, tempName);
    unsigned int possibleOrderNumber = 0;
    bool dualOrderMethod = false;
    database->MethodsDatabase().RemoveOrderNameFromMethodName(tempName, processedName, possibleOrderNumber, dualOrderMethod);

    return possibleOrderNumber;
}

System::String^
DatabaseManager::FullName(Duco::TObjectType objectType, const Duco::ObjectId& objectId)
{
    switch (objectType)
    {
        case TObjectType::EPeal:
        {
            Duco::Peal thePeal;
            if (FindPeal(objectId, thePeal, false))
            {
                std::wstring pealTitle;
                thePeal.ShortTitle(pealTitle);
                return DucoUtils::ConvertString(pealTitle);
            }
        }
        break;

        case TObjectType::EMethod:
            {
            Duco::Method theMethod;
            if (FindMethod(objectId, theMethod, false))
                return DucoUtils::ConvertString(theMethod.FullName(*database));
            }
            break;
        case TObjectType::EMethodSeries:
            {
            Duco::MethodSeries theMethodSeries;
            if (FindMethodSeries(objectId, theMethodSeries, false))
                return DucoUtils::ConvertString(theMethodSeries.Name());
            }
            break;
        case TObjectType::ETower:
            {
            Duco::Tower theTower;
            if (FindTower(objectId, theTower, false))
                return DucoUtils::ConvertString(theTower.FullName());
            }
            break;
        case TObjectType::ERinger:
            {
            Duco::Ringer theRinger;
            if (FindRinger(objectId, theRinger, false))
            {
                return DucoUtils::ConvertString(theRinger.FullName(database->Settings().LastNameFirst()));
            }
            }
            break;

        default:
        case TObjectType::ERing:
        case TObjectType::EOrderName:
        case TObjectType::EComposition:
        case TObjectType::ESettings:
        case TObjectType::EPicture:
            break;
    }
    return "";
}

System::Boolean
DatabaseManager::UpdatePicture(const Picture& updatedPicture, bool notifyObservers)
{
    if (NothingInEditMode())
//    if (updatedPicture.Id() == *editingPictureNo)
    {
        if (database->PicturesDatabase().UpdateObject(updatedPicture))
        {
            if (notifyObservers)
                CallObservers(EUpdated, TObjectType::EPicture, updatedPicture.Id(), KNoId);
//            updatedPicture->ClearId();
            return true;
        }
    }
    return false;
}

System::Boolean
DatabaseManager::FindPicture(const Duco::ObjectId& objectId, Duco::Picture& thePicture, System::Boolean setIndex)
{
    const Duco::Picture* const foundPicture = database->PicturesDatabase().FindPicture(objectId, setIndex);
    if (foundPicture == NULL)
        return false;

    thePicture = *foundPicture;

    return true;
}

System::Boolean
DatabaseManager::FindPicture(const Duco::ObjectId& pictureId, System::Collections::Generic::List<System::UInt64>^% pealIds, System::Collections::Generic::List<System::UInt64>^% towerIds)
{
    std::set<Duco::ObjectId> tempIds;
    database->PealsDatabase().FindPicture(pictureId, tempIds);
    DucoUtils::Convert(tempIds, pealIds);
    database->TowersDatabase().FindPicture(pictureId, tempIds);
    DucoUtils::Convert(tempIds, towerIds);

    return pealIds->Count > 0 || towerIds->Count > 0;
}

Duco::ObjectId
DatabaseManager::AddPicture(Duco::Picture& newPicture)
{
    Duco::ObjectId newId = database->PicturesDatabase().AddObject(newPicture);
    if (newId.ValidId())
    {
        CallObservers(EAdded, TObjectType::EPicture, newId, KNoId);
    }
    return newId;
}

System::Boolean
DatabaseManager::FirstPicture(Duco::ObjectId& objectId, System::Boolean setIndex)
{
    return database->PicturesDatabase().FirstObject(objectId, setIndex);
}

System::Boolean
DatabaseManager::LastPicture(Duco::ObjectId& objectId)
{
    return database->PicturesDatabase().LastObject(objectId);
}

System::Boolean
DatabaseManager::ForwardMultiplePictures(Duco::ObjectId& objectId, System::Boolean forwards)
{
    return database->PicturesDatabase().ForwardMultipleObjects(objectId, forwards);
}

System::Boolean
DatabaseManager::NextPicture(Duco::ObjectId& objectId, System::Boolean forwards, System::Boolean setIndex)
{
    return database->PicturesDatabase().NextObject(objectId, forwards, setIndex);
}

System::Boolean
DatabaseManager::RemoveAndDeletePictures(const std::set<Duco::ObjectId>& pictureIds)
{
    if (NothingInEditMode())
    {
        database->RemovePictures(pictureIds);
        std::set<Duco::ObjectId>::const_iterator it = pictureIds.begin();
        while (it != pictureIds.end())
        {
            CallObservers(EDeleted, TObjectType::EPicture, *it, KNoId);
            ++it;
        }
        return true;
    }
    return false;
}

System::Boolean
DatabaseManager::DeletePictures(const std::set<Duco::ObjectId>& pictureIds, System::Boolean suspendObservers)
{
    bool allRemoved = true;
    std::set<Duco::ObjectId>::const_iterator it = pictureIds.begin();
    while (it != pictureIds.end())
    {
        if (database->PicturesDatabase().DeleteObject(*it))
        {
            if (!suspendObservers)
                CallObservers(EDeleted, TObjectType::EPicture, *it, KNoId);
        }
        else
        {
            allRemoved = false;
        }
        ++it;
    }
    return allRemoved;
}

System::Void
DatabaseManager::GetDateRange(System::DateTime^% startDate, System::DateTime^% endDate)
{
    StatisticFilters filters(*database);

    Duco::PealLengthInfo info = database->PealsDatabase().PerformanceInfo(filters);

    startDate = DucoUtils::ConvertDate(info.DateOfFirstPeal());
    endDate = DucoUtils::ConvertDate(info.DateOfLastPeal());
}

Duco::ObjectId
DatabaseManager::FindLastPealBeforeDate(System::DateTime^ lastPealDate)
{
    Duco::PerformanceDate startDateNative = DucoUtils::ConvertDate(lastPealDate);
    StatisticFilters filters(*database);
    filters.SetEndDate(true, startDateNative);

    return database->PealsDatabase().PerformanceInfo(filters).LastPealId();
}

System::Collections::Generic::HashSet<System::String^>^
DatabaseManager::RemoveExistingBellBoardPealIds(System::Collections::Generic::Queue<System::String^>^ allPealIds)
{
    System::Collections::Generic::Queue<System::String^>::Enumerator it = allPealIds->GetEnumerator();
    std::list<std::wstring> missingBellBoardIds;
    while (it.MoveNext())
    {
        std::wstring current;
        DucoUtils::ConvertString(it.Current, current);
        missingBellBoardIds.push_back(current);
    }

    //Removes the known peals and returns them in a seperate list.
    std::set<std::wstring> knownIds = database->PealsDatabase().RemoveExistingBellBoardPealIds(missingBellBoardIds);
    std::set<std::wstring>::const_iterator knownIt = knownIds.begin();
    System::Collections::Generic::HashSet<System::String^>^ knownPealIds = gcnew System::Collections::Generic::HashSet<System::String^>();
    while (knownIt != knownIds.end())
    {
        knownPealIds->Add(DucoUtils::ConvertString(*knownIt));
        ++knownIt;
    }
    allPealIds->Clear();
    std::list<std::wstring>::const_reverse_iterator missingIt = missingBellBoardIds.rbegin();
    while (missingIt != missingBellBoardIds.rend())
    {
        allPealIds->Enqueue(DucoUtils::ConvertString(*missingIt));
        ++missingIt;
    }
    if (knownPealIds->Count == allPealIds->Count)
    {
        DUCOUILOGDEBUG("All bell board peal id found");
    }

    return knownPealIds;
}

System::Boolean
DatabaseManager::AnyOnThisDay()
{
    return database->PealsDatabase().AnyOnThisDay(PerformanceDate());
}

System::Void
DatabaseManager::OnThisDay(System::Collections::Generic::List<System::UInt32>^ pealIds)
{
    std::set<Duco::ObjectId> tempPealIds = database->PealsDatabase().OnThisDay(PerformanceDate());
    std::set<Duco::ObjectId>::const_iterator it = tempPealIds.begin();
    while (it != tempPealIds.end())
    {
        pealIds->Add(it->Id());
        ++it;
    }
}

System::String^
DatabaseManager::PealMap(size_t& numberOfTowersPlotted, size_t& numberOfTowersNotPlotted, System::Boolean zeroPerformances, System::Boolean onePerformance, System::Boolean multiplePerformances, System::Boolean europeOnly, System::Boolean combineLinked, System::Boolean useFelsteadPealCount, System::DateTime^ startDate, System::DateTime^ endDate, System::String^ county)
{
    numberOfTowersPlotted = 0;
    numberOfTowersNotPlotted = 0;
    Duco::PerformanceDate engineStartDate = DucoUtils::ConvertDate(startDate);
    Duco::PerformanceDate engineEndDate = DucoUtils::ConvertDate(endDate);
    std::wstring engineCounty;
    DucoUtils::ConvertString(county, engineCounty);

    System::String^ returnValue = "";

    Duco::StatisticFilters filters(*database);
    filters.SetStartDate(engineStartDate.Valid(), engineStartDate);
    filters.SetEndDate(engineEndDate.Valid(), engineEndDate);
    filters.SetCounty(engineCounty.size() > 0, engineCounty);
    filters.SetEuropeOnly(europeOnly);

    //if (database->Settings().UseMapBox())
    {
        returnValue = DucoUtils::ConvertString(database->MapBoxMap(numberOfTowersPlotted, numberOfTowersNotPlotted, config->MapBoxApi(), zeroPerformances, onePerformance, multiplePerformances, combineLinked, useFelsteadPealCount, filters));

        String^ tempFile = System::IO::Path::GetTempPath() + "PealMapLink_" + DucoUtils::ConvertString(DucoEngineUtils::ToString(numberOfTowersPlotted)) + ".html";
        StreamWriter^ sw = gcnew StreamWriter(tempFile);
        sw->WriteLine(returnValue);
        sw->Close();

        returnValue = tempFile;
    }

    return returnValue;
}

System::Boolean
DatabaseManager::UppercaseAssociations(Duco::ProgressCallback* const callback)
{
    if (database->AssociationsDatabase().UppercaseAssociations(callback))
    {
        CallObservers(TEventType::EUpdated, TObjectType::EAssociationOrSociety, KNoId, KNoId);
        return true;
    }
    return false;
}

System::Boolean
DatabaseManager::UppercaseCities(Duco::ProgressCallback* const callback)
{
    if (database->TowersDatabase().UppercaseCities(callback))
    {
        CallObservers(TEventType::EUpdated, TObjectType::ETower, KNoId, KNoId);
        return true;
    }
    return false;
}

System::Boolean
DatabaseManager::CapitaliseFields(Duco::ProgressCallback* const callback, System::Boolean methodName, System::Boolean methodType, System::Boolean county, System::Boolean dedication, System::Boolean ringerNames)
{
    System::Boolean processed = false;
    if (database->CapitaliseFields(*callback, methodName, methodType, county, dedication, ringerNames))
    {
        if (methodName || methodType)
            CallObservers(TEventType::EUpdated, TObjectType::EMethod, KNoId, KNoId);
        if (county || dedication)
            CallObservers(TEventType::EUpdated, TObjectType::ETower, KNoId, KNoId);
        if (ringerNames)
            CallObservers(TEventType::EUpdated, TObjectType::ERinger, KNoId, KNoId);
        processed = true;
    }
    return processed;
}

System::Boolean
DatabaseManager::RemoveDuplicateRings(Duco::ProgressCallback* const callback)
{
    if (database->PealsDatabase().RemoveDuplicateRings(callback))
    {
        CallObservers(TEventType::EUpdated, TObjectType::ETower, KNoId, KNoId);
        return true;
    }
    return false;
}

System::Boolean
DatabaseManager::ReplaceMethodTypes(Duco::ProgressCallback* const callback)
{
    if (database->MethodsDatabase().ReplaceMethodTypes(callback, settings->ConfigDir()))
    {
        CallObservers(TEventType::EUpdated, TObjectType::EMethod, KNoId, KNoId);
        return true;
    }
    return false;
}

System::Boolean
DatabaseManager::RemoveDuplicateMethods(Duco::ProgressCallback* const callback)
{
    if (database->PealsDatabase().RemoveDuplicateMethods(callback))
    {
        CallObservers(TEventType::EUpdated, TObjectType::EMethod, KNoId, KNoId);
        return true;
    }
    return false;
}

System::Boolean
DatabaseManager::ImportMissingTenorKeys(const Duco::DoveDatabase& doveDatabase)
{
    if (database->TowersDatabase().ImportMissingTenorKeys(doveDatabase))
    {
        CallObservers(TEventType::EUpdated, TObjectType::ETower, KNoId, KNoId);
        return true;
    }
    return false;
}

size_t
DatabaseManager::CheckBellBoardForUpdatedReferences(Duco::ProgressCallback* callBack, bool rwPagesOnly, bool& cancellationFlag)
{
    size_t noOfUpdatedPeals = database->CheckBellBoardForUpdatedReferences(*callBack, rwPagesOnly, cancellationFlag);
    if (noOfUpdatedPeals > 0)
    {
        CallObservers(TEventType::EUpdated, TObjectType::ETower, KNoId, KNoId);
    }
    return noOfUpdatedPeals;
}

System::String^
DatabaseManager::PerformanceString(System::Boolean includePlurality)
{
    System::String^ type = "Quarter";
    if (Settings().PealDatabase())
    {
        type = "Peal";
    }
    if (includePlurality && database->PealsDatabase().NumberOfObjects() > 0)
    {
        type += "s";
    }
    return type;
}
