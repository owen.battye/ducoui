#include "TableSorterByCount.h"

using namespace DucoUI;
using namespace System;
using namespace System::Windows::Forms;

TableSorterByCount::TableSorterByCount(bool newAscending, int newColumnId)
:   ascending(newAscending), columnId(newColumnId)
{
}

TableSorterByCount::~TableSorterByCount()
{

}

int 
TableSorterByCount::Compare(System::Object^ firstObject, System::Object^ secondObject)
{
    DataGridViewRow^ row1 = static_cast<DataGridViewRow^>(firstObject);
    DataGridViewRow^ row2 = static_cast<DataGridViewRow^>(secondObject);
    if (row1 == row2)
        return 0;

    int row1Value = ConvertValueToInt(row1,columnId);
    int row2Value = ConvertValueToInt(row2,columnId);

    return ascending ? row1Value - row2Value : row2Value - row1Value;
}

int
TableSorterByCount::ConvertValueToInt(DataGridViewRow^ row, int cellNumber)
{
    int returnValue = 0;
    try
    {
        Object^ object = nullptr;
        if (cellNumber == -1)
        { // Assume this means header cell.
            object = row->HeaderCell->Value;
        }
        else
        {
            object = row->Cells[cellNumber]->Value;
        }

        String^ objectAsString = Convert::ToString(object);
        if (objectAsString->Contains(","))
        {
            objectAsString = objectAsString->Replace(",", String::Empty);
        }

        if (objectAsString != nullptr)
            returnValue = Convert::ToInt16(objectAsString);
    }
    catch (Exception^)
    {
        returnValue = 0;
    }
    return returnValue;
}

