#include "DatabaseImportUtil.h"
#include "DatabaseManager.h"
#include "ImportProgress.h"
#include <SettingsFile.h>
#include "WindowsSettings.h"

#include "ImportFileUI.h"
#include "WinRKv2Import.h"
#include "WinRKv3Import.h"
#include "SoundUtils.h"
#include "SystemDefaultIcon.h"
#include "CSVImportSettingsUI.h"
#include "FileTypeDetector.h"
#include "GenericImport.h"
#include "DucoCommon.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::Windows::Forms;
using namespace System::Data::Odbc;

#define KUnknownFileType "Unknown file type"

ImportFileUI::ImportFileUI(DatabaseManager^ theDatabase, System::String^ newFileName)
:   database(theDatabase), stage(WinRkImportProgressCallback::TImportStage::ENotStarted), fileName(newFileName),
    importer (nullptr)
{
    InitializeComponent();
    totalProgress->Value = 0;
    totalProgress->Minimum = 0;
    totalProgress->Maximum = 100;
}

ImportFileUI::~ImportFileUI()
{
    if (components)
    {
        delete components;
    }
    delete importer;
}

void
ImportFileUI::InitializeComponent()
{
    System::Windows::Forms::Label^ label1;
    System::Windows::Forms::Label^ label2;
    System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel1;
    this->fileNameTextBox = (gcnew System::Windows::Forms::TextBox());
    this->taskProgress = (gcnew System::Windows::Forms::ProgressBar());
    this->totalProgress = (gcnew System::Windows::Forms::ProgressBar());
    this->settingsBtn = (gcnew System::Windows::Forms::Button());
    this->startBtn = (gcnew System::Windows::Forms::Button());
    this->fileTypeTextBox = (gcnew System::Windows::Forms::TextBox());
    this->closeBtn = (gcnew System::Windows::Forms::Button());
    this->taskLbl = (gcnew System::Windows::Forms::Label());
    label1 = (gcnew System::Windows::Forms::Label());
    label2 = (gcnew System::Windows::Forms::Label());
    tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
    tableLayoutPanel1->SuspendLayout();
    this->SuspendLayout();
    // 
    // label1
    // 
    label1->AutoSize = true;
    label1->Location = System::Drawing::Point(3, 32);
    label1->Margin = System::Windows::Forms::Padding(3, 6, 3, 3);
    label1->Name = L"label1";
    label1->Size = System::Drawing::Size(77, 13);
    label1->TabIndex = 2;
    label1->Text = L"Task progress:";
    // 
    // label2
    // 
    label2->AutoSize = true;
    label2->Dock = System::Windows::Forms::DockStyle::Fill;
    label2->Location = System::Drawing::Point(3, 82);
    label2->Margin = System::Windows::Forms::Padding(3, 6, 3, 3);
    label2->Name = L"label2";
    label2->Size = System::Drawing::Size(77, 13);
    label2->TabIndex = 4;
    label2->Text = L"Total progress:";
    // 
    // tableLayoutPanel1
    // 
    tableLayoutPanel1->ColumnCount = 5;
    tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
    tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 50)));
    tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
    tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 50)));
    tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
    tableLayoutPanel1->Controls->Add(this->settingsBtn, 0, 5);
    tableLayoutPanel1->Controls->Add(this->taskLbl, 1, 1);
    tableLayoutPanel1->Controls->Add(this->startBtn, 2, 5);
    tableLayoutPanel1->Controls->Add(label1, 0, 1);
    tableLayoutPanel1->Controls->Add(label2, 0, 3);
    tableLayoutPanel1->Controls->Add(this->fileTypeTextBox, 4, 0);
    tableLayoutPanel1->Controls->Add(this->fileNameTextBox, 0, 0);
    tableLayoutPanel1->Controls->Add(this->totalProgress, 0, 4);
    tableLayoutPanel1->Controls->Add(this->closeBtn, 4, 5);
    tableLayoutPanel1->Controls->Add(this->taskProgress, 0, 2);
    tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
    tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
    tableLayoutPanel1->Name = L"tableLayoutPanel1";
    tableLayoutPanel1->RowCount = 6;
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
    tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
    tableLayoutPanel1->Size = System::Drawing::Size(631, 166);
    tableLayoutPanel1->TabIndex = 9;
    // 
    // fileNameTextBox
    // 
    tableLayoutPanel1->SetColumnSpan(this->fileNameTextBox, 4);
    this->fileNameTextBox->Dock = System::Windows::Forms::DockStyle::Fill;
    this->fileNameTextBox->Location = System::Drawing::Point(3, 3);
    this->fileNameTextBox->Name = L"fileNameTextBox";
    this->fileNameTextBox->ReadOnly = true;
    this->fileNameTextBox->Size = System::Drawing::Size(518, 20);
    this->fileNameTextBox->TabIndex = 0;
    this->fileNameTextBox->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &ImportFileUI::fileName_MouseDown);
    // 
    // taskProgress
    // 
    tableLayoutPanel1->SetColumnSpan(this->taskProgress, 5);
    this->taskProgress->Dock = System::Windows::Forms::DockStyle::Fill;
    this->taskProgress->Location = System::Drawing::Point(3, 51);
    this->taskProgress->Name = L"taskProgress";
    this->taskProgress->Size = System::Drawing::Size(625, 22);
    this->taskProgress->Step = 5;
    this->taskProgress->TabIndex = 3;
    // 
    // totalProgress
    // 
    tableLayoutPanel1->SetColumnSpan(this->totalProgress, 5);
    this->totalProgress->Dock = System::Windows::Forms::DockStyle::Fill;
    this->totalProgress->Location = System::Drawing::Point(7, 105);
    this->totalProgress->Margin = System::Windows::Forms::Padding(7);
    this->totalProgress->Name = L"totalProgress";
    this->totalProgress->Size = System::Drawing::Size(617, 24);
    this->totalProgress->TabIndex = 5;
    // 
    // settingsBtn
    // 
    this->settingsBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
    this->settingsBtn->Location = System::Drawing::Point(3, 140);
    this->settingsBtn->Name = L"settingsBtn";
    this->settingsBtn->Size = System::Drawing::Size(75, 23);
    this->settingsBtn->TabIndex = 6;
    this->settingsBtn->Text = L"Settings";
    this->settingsBtn->UseVisualStyleBackColor = true;
    this->settingsBtn->Visible = false;
    this->settingsBtn->Click += gcnew System::EventHandler(this, &ImportFileUI::settingsBtn_Click);
    // 
    // startBtn
    // 
    this->startBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left)
        | System::Windows::Forms::AnchorStyles::Right));
    this->startBtn->Location = System::Drawing::Point(266, 140);
    this->startBtn->Name = L"startBtn";
    this->startBtn->Size = System::Drawing::Size(75, 23);
    this->startBtn->TabIndex = 7;
    this->startBtn->Text = L"Start";
    this->startBtn->UseVisualStyleBackColor = true;
    this->startBtn->Click += gcnew System::EventHandler(this, &ImportFileUI::startBtn_Click);
    // 
    // fileTypeTextBox
    // 
    this->fileTypeTextBox->Dock = System::Windows::Forms::DockStyle::Fill;
    this->fileTypeTextBox->Location = System::Drawing::Point(527, 3);
    this->fileTypeTextBox->Name = L"fileTypeTextBox";
    this->fileTypeTextBox->ReadOnly = true;
    this->fileTypeTextBox->Size = System::Drawing::Size(101, 20);
    this->fileTypeTextBox->TabIndex = 1;
    // 
    // closeBtn
    // 
    this->closeBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
    this->closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
    this->closeBtn->Location = System::Drawing::Point(553, 140);
    this->closeBtn->Name = L"closeBtn";
    this->closeBtn->Size = System::Drawing::Size(75, 23);
    this->closeBtn->TabIndex = 8;
    this->closeBtn->Text = L"Close";
    this->closeBtn->UseVisualStyleBackColor = true;
    this->closeBtn->Click += gcnew System::EventHandler(this, &ImportFileUI::closeBtn_Click);
    // 
    // taskLbl
    // 
    this->taskLbl->AutoSize = true;
    tableLayoutPanel1->SetColumnSpan(this->taskLbl, 3);
    this->taskLbl->Location = System::Drawing::Point(86, 32);
    this->taskLbl->Margin = System::Windows::Forms::Padding(3, 6, 3, 3);
    this->taskLbl->Name = L"taskLbl";
    this->taskLbl->Size = System::Drawing::Size(0, 13);
    this->taskLbl->TabIndex = 8;
    // 
    // ImportFileUI
    // 
    this->AcceptButton = this->startBtn;
    this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
    this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
    this->CancelButton = this->closeBtn;
    this->ClientSize = System::Drawing::Size(631, 166);
    this->Controls->Add(tableLayoutPanel1);
    this->MinimizeBox = false;
    this->MinimumSize = System::Drawing::Size(647, 205);
    this->Name = L"ImportFileUI";
    this->Text = L"Import file";
    this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &ImportFileUI::ImportFileUI_FormClosing);
    this->Load += gcnew System::EventHandler(this, &ImportFileUI::ImportFileUI_Load);
    this->Shown += gcnew System::EventHandler(this, &ImportFileUI::ImportFileUI_Shown);
    tableLayoutPanel1->ResumeLayout(false);
    tableLayoutPanel1->PerformLayout();
    this->ResumeLayout(false);

}

void
ImportFileUI::Initialised(WinRkImportProgressCallback::TImportStage newStage)
{
    stage = newStage;
    taskProgress->Value = 0;
    taskProgress->Minimum = 0;
    taskProgress->Maximum = 100;
    switch (stage)
    {
    case WinRkImportProgressCallback::TImportStage::EImportingRingers:
        taskLbl->Text = "Importing Ringers";
        break;
    case WinRkImportProgressCallback::TImportStage::EImportingRingerAkas:
        taskLbl->Text = "Importing Ringers alternative names";
        break;
    case WinRkImportProgressCallback::TImportStage::EImportingTowers:
        taskLbl->Text = "Importing Towers";
        break;
    case WinRkImportProgressCallback::TImportStage::EImportingCities:
        taskLbl->Text = "Importing Cities";
        break;
    case WinRkImportProgressCallback::TImportStage::EImportingCounties:
        taskLbl->Text = "Importing Counties";
        break;
    case WinRkImportProgressCallback::TImportStage::EImportingRings:
        taskLbl->Text = "Importing Rings";
        break;
    case WinRkImportProgressCallback::TImportStage::EImportingTenors:
        taskLbl->Text = "Importing Tenors";
        break;
    case WinRkImportProgressCallback::TImportStage::EImportingMethods:
        taskLbl->Text = "Importing Methods";
        break;
    case WinRkImportProgressCallback::TImportStage::EImportingMethodTypes:
        taskLbl->Text = "Importing Method types";
        break;
    case WinRkImportProgressCallback::TImportStage::EImportingMethodNames:
        taskLbl->Text = "Importing Method names";
        break;
    case WinRkImportProgressCallback::TImportStage::EImportingPeals:
        taskLbl->Text = "Importing Performances";
        break;
    case WinRkImportProgressCallback::TImportStage::EImportingSocieties:
        taskLbl->Text = "Importing Societies";
        break;
    case WinRkImportProgressCallback::TImportStage::EImportingComposers:
        taskLbl->Text = "Importing Composers";
        break;
    case WinRkImportProgressCallback::TImportStage::EImportingMultiMethods:
        taskLbl->Text = "Importing Multi Methods";
        break;
    case WinRkImportProgressCallback::TImportStage::EImportingOrderNames:
        taskLbl->Text = "Importing Order Names";
        break;
    default:
        taskLbl->Text = "";
        break;
    }
    taskLbl->Refresh();
}

void
ImportFileUI::Step(int percent)
{
    taskProgress->Value = percent;

    double progressPercentage (percent);

    double stageNumber = importer->StageNumber(stage);
    double totalNumberOfStages = importer->TotalNoOfStages();
    int taskProgressAsPercentOfTotal = int(progressPercentage/totalNumberOfStages);
    int completedTasksAsProgressAsPercentOfTotal = int(100*(stageNumber / totalNumberOfStages));
    totalProgress->Value = taskProgressAsPercentOfTotal + completedTasksAsProgressAsPercentOfTotal;
}

void
ImportFileUI::Complete(bool cancelled)
{
    delete importer;
    importer = nullptr;
    database->NotifyImportComplete();
    if (cancelled)
    {
        database->ClearDatabase(false, false);
        SoundUtils::PlayErrorSound();
    }
    else
    {
        SoundUtils::PlayFinishedSound();
    }
    Close();
}


System::Void
ImportFileUI::ImportFileUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();

    if (fileName->Length == 0)
    {
        if (!OpenFileSelectionDlg())
            preventOpening = true;
    }
    else
    {
        fileNameTextBox->Text = fileName;
        CheckFileType();
        if (importer == nullptr)
            preventOpening = true;
    }
}

System::Void
ImportFileUI::ImportFileUI_Shown(System::Object^  sender, System::EventArgs^  e)
{
    if (preventOpening)
        Close();
}

System::Void
ImportFileUI::ImportFileUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e)
{
    if (importer != nullptr && importer->ImportInProgress())
    {
        e->Cancel = true;
    }
}

System::Void
ImportFileUI::settingsBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    CSVImportSettingsUI^ settingsUI = gcnew CSVImportSettingsUI(database->WindowsSettings(), KDefaultCsvImportSettings);
    settingsUI->MdiParent = this->MdiParent;
    settingsUI->Show();
}

System::Void
ImportFileUI::startBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    settingsBtn->Enabled = false;
    if (fileNameTextBox->Text->Length == 0)
    {
        OpenFileSelectionDlg();
        if (fileNameTextBox->Text->Length == 0)
            return;
    }
    startBtn->Enabled = false;
    if (importer == nullptr)
    {
        CheckFileType();
    }
    if (importer != nullptr)
    {
        importer->StartImport();
    }
}

System::Void
ImportFileUI::CheckFileType()
{
    if (importer != nullptr)
    {
        delete importer;
        importer = nullptr;
    }

    DucoUI::FileTypeDetector^ versionDetector = gcnew FileTypeDetector();
    TFileType fileType = versionDetector->FileType(fileNameTextBox->Text);

    switch (fileType)
    {
    case EWinRkVersion3:
        {
        settingsBtn->Visible = false;
        importer = gcnew WinRKv3Import(database, this, fileNameTextBox->Text);
        }
        break;
    case EWinRkVersion2:
        {
        settingsBtn->Visible = false;
        importer = gcnew WinRKv2Import(database, this, fileNameTextBox->Text);
        }
        break;

    case ECsv:
        settingsBtn->Visible = true;
        settingsBtn->Enabled = true;
    case EPealBasePBR:
    case EPealBaseTSV:
    case EWinRkVersion1:
        {
        importer = gcnew GenericImport(database, this, fileNameTextBox->Text, database->WindowsSettings()->ConfigDir(), fileType);
        }
        break;

    case ENoDatabaseDrivers:
        settingsBtn->Visible = false;
#ifdef _WIN64
        MessageBox::Show(this, "Cannot import file, no 64 bit Microsoft Access database driver installed on your system", KUnknownFileType, MessageBoxButtons::OK, MessageBoxIcon::Error);
#else
        MessageBox::Show(this, "Cannot import file, no 32 bit Microsoft Access database driver installed on your system", KUnknownFileType, MessageBoxButtons::OK, MessageBoxIcon::Error);
#endif
        break;

    default:
        settingsBtn->Visible = false;
        MessageBox::Show(this, "Cannot import file, unknown file type", KUnknownFileType, MessageBoxButtons::OK, MessageBoxIcon::Error );
        break;
    }

    delete versionDetector;
    if (fileType == EUnknownFileType || fileType == ENoDatabaseDrivers)
    {
        startBtn->Enabled = false;
        fileTypeTextBox->Text = KUnknownFileType;
    }
    else
    {
        startBtn->Enabled = true;
        fileTypeTextBox->Text = importer->FileType();
    }
}

bool
ImportFileUI::StartImport()
{
    bool importStarted (true);

    try
    {
        database->ConvertStarted();
        database->ClearDatabase(false, true);
        importer->StartImport();
    }
    catch (OdbcException^ odbcEx) 
    {
        MessageBox::Show(this, odbcEx->ToString(), "ODBC Error importing file", MessageBoxButtons::OK, MessageBoxIcon::Error );
        database->ClearDatabase(false, true);
        importStarted = false;
    }
    catch (Exception^ ex) 
    {
        MessageBox::Show(this, ex->ToString(), "Error importing file", MessageBoxButtons::OK, MessageBoxIcon::Error );
        database->ClearDatabase(false, true);
        importStarted = false;
    }

    return importStarted;
    }

System::Void
ImportFileUI::closeBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (importer == nullptr)
    {
        Close();
    }
    else
    {
        if (MessageBox::Show(this, "Are you sure you want to cancel the file import?", "Cancel import?", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning )  == ::DialogResult::OK)
        {
            if (importer->ImportInProgress())
                importer->Cancel();
            else
                Close();
        }
    }
}

System::Void
ImportFileUI::fileName_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
    if (importer == nullptr || !importer->ImportInProgress())
    {
        OpenFileSelectionDlg();
    }
}

bool
ImportFileUI::OpenFileSelectionDlg()
{
    OpenFileDialog^ fileDialog = gcnew OpenFileDialog ();
    fileDialog->Filter = "Peal files(*.v30;*.mdb;*.pbr,*.plf,*.tsv,*.csv)|*.v30;*.mdb;*.pbr;*.plf;*.tsv;*.csv|WinRK v3 databases (*.v30)|*.v30|WinRk v2 databases (*.mdb)|*.mdb|WinRk v1 databases (*.plf)|*.plf|PealBase databases (*.pbr,*.tsv)|*.pbr;*.tsv|CSV Files (*.csv)|*.csv|All files (*.*)|*.*" ;
    fileDialog->RestoreDirectory = true;

    switch (fileDialog->ShowDialog())
    {
    case System::Windows::Forms::DialogResult::OK:
        {
            try
            {
                fileName = fileDialog->FileName;
                fileDialog->OpenFile()->Close();
                fileNameTextBox->Text = fileName;
                CheckFileType();
            }
            catch (Exception^ ex)
            {
                settingsBtn->Visible = false;
                SoundUtils::PlayErrorSound();
                MessageBox::Show(this, "Error: Could not open file: " + ex->Message);
            }
        }
    default:
        return true;

    case System::Windows::Forms::DialogResult::Cancel:
        return false;
    }
}
