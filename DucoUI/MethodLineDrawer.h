#pragma once

#include "LeadType.h"

namespace Duco
{
    class Method;
    class Change;
    class Lead;
    class Music;
    class PlaceNotation;
}

#define KTrebleColour  Color::Red
#define KNormalColour  Color::Blue
#define KGridColour    Color::Gainsboro
#define KBellsColour   Color::Gainsboro 
#define KLeadEndColour Color::FromArgb(255,169,169,169)
#define KMusicalColour Color::DarkSalmon
#define KRollupColour  Color::Fuchsia

#define KGenericMethodFont FontFamily::GenericSansSerif

#define KLeadStartFontSize          8.0F
#define KAllBellsFontSize           2.625F

#define KLeadStartSize              1.5F
#define KLineDrawingPenSize         0.75F
#define KTrebleLineDrawingPenSize   0.3F
#define KGridLinesPenSize           0.525F

#define KPositionGap                6.0F
#define KBlowGap                    3.0F
#define KPositionOffset             7.5F
#define KBlowOffset                 7.5F

namespace DucoUI
{
    ref class DatabaseManager;

    public ref class MethodLineDrawer abstract
    {
    public:
        bool StartDrawing(unsigned int startingFromBell);
        void ShiftLineRight(float additionalXPosWrapOffset);
        void SetPrintCallings(bool printCalls);

    protected:
        MethodLineDrawer(System::Drawing::Graphics^ lineDrwr, Duco::Method& theMethod, DucoUI::DatabaseManager^ theDatabase, System::ComponentModel::BackgroundWorker^ bgWorker);
        !MethodLineDrawer();
        virtual ~MethodLineDrawer();
        System::Void CreateGridPens();
        virtual System::Void DrawGridLines() = 0;
        virtual bool DrawLine() = 0;
        virtual bool ShowMusic();
        virtual System::Void DrawLead(System::Drawing::Pen^% penToUse) = 0;
        bool ShowTreble();
        virtual System::Void MoveTrebleLineAndDraw(System::Drawing::Pen^% penToUse) = 0;
        virtual bool DrawCallings();
        virtual System::Void PrintRow(unsigned int changeNumber);
        virtual System::Void PrintRow(unsigned int changeNo, Duco::Change& change, float yOffset);
        System::Void CreateNextLead();
        void CheckHighestPosition(System::Drawing::PointF nextPosition);

    private:
        System::Void DrawCalling(System::String^ leadDescription, float& yOffset, Duco::TLeadType leadEnd);
        System::Void AddPointToLine(System::Collections::Generic::List<System::Drawing::PointF>^% line, unsigned int position, unsigned int bellNumber, unsigned int changeNumber, float yOffset);

    protected: // member data
        Duco::Music*                        music;
        Duco::Method&                       currentMethod;
        DucoUI::DatabaseManager^      database;
        System::Drawing::Font^              leadStartFont;
        System::ComponentModel::BackgroundWorker^ worker;

        Duco::Lead*                         singleLead;
        Duco::PlaceNotation*                notation;
        System::Drawing::Graphics^          lineDrawer;

        System::Drawing::SolidBrush^        linesDrawingBrush; // Helps to show places

        System::Drawing::Pen^               gridDrawingPen;
        System::Drawing::SolidBrush^        leadStartBrush;
        System::Drawing::Pen^               lineDrawingPen;
        System::Drawing::Pen^               trebleDrawingPen;
        System::Drawing::Pen^               gridTrebleDrawingPen;
        System::Drawing::Font^              callingTitleFont;
        System::Drawing::Font^              allBellsFont;
        System::Drawing::SolidBrush^        allBellsBrush;

        unsigned int                        drawingBell;
        unsigned int                        leadNumber;
        float                               perLeadXOffsetValue;
        float                               xPosWrapOffset;
        float                               maxXPosition;
        float                               maxYPosition;

        bool                                printCallings;

        System::Drawing::Imaging::Metafile^ methodDrawing;
        System::Collections::Generic::List<System::Drawing::PointF>^    leadLine;
        System::Drawing::Drawing2D::GraphicsPath^                       treblePath;
    };
}
