#include "WinRKv2Import.h"
#include "AssociationDatabase.h"
#include "DatabaseManager.h"
#include "DucoUtils.h"
#include <DucoEngineUtils.h>
#include <RingingDatabase.h>
#include <Ring.h>
#include <Ringer.h>
#include <Tower.h>
#include <Peal.h>
#include <MethodDatabase.h>
#include "WinRKv2Import.h"
#include <RingingDatabase.h>
#include <RingerNameChange.h>
#include <TowerDatabase.h>
#include <RingerDatabase.h>
#include "DucoCommon.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace std;
using namespace System::Data::Odbc;
using namespace System::Windows::Forms;

WinRKv2Import::WinRKv2Import(DucoUI::DatabaseManager^ theDatabase, DucoUI::WinRkImportProgressCallback^ newCallback, System::String^ theFileName)
: DatabaseImportUtil(theDatabase, newCallback, theFileName)
{
}

WinRKv2Import::~WinRKv2Import()
{
    if (connection != nullptr)
        connection->Close();
}

void
WinRKv2Import::Import(System::ComponentModel::BackgroundWorker^ worker, System::ComponentModel::DoWorkEventArgs^  e)
{
    String^ connectionString = "Driver={Microsoft Access Driver (*.mdb)};DBQ=" + fileName + ";provider=SQLOLEDB;User Id="";Password=""";
    connection = gcnew OdbcConnection (connectionString);
    connection->Open();
    OdbcCommand^ command = connection->CreateCommand();

    map<unsigned int, wstring> towerTenors;
    map<unsigned int, Duco::ObjectId> societies;
    map<unsigned int, wstring> composers;
    map<Duco::ObjectId, Duco::ObjectId> ringerAkas; // oldId to newId

    if (!worker->CancellationPending)
        ImportRingers(command, ringerAkas, worker);
    if (!worker->CancellationPending)
        ImportTowers(command, towerTenors, worker);
    database->Database().MethodsDatabase().CreateDefaultBellNames();

    if (!worker->CancellationPending)
    {
        map<unsigned int, wstring> societiesFromImport;
        ImportSingleStringFromTable(command, "Societies", societiesFromImport, DucoUI::WinRkImportProgressCallback::TImportStage::EImportingSocieties, worker);
        map<unsigned int, wstring>::const_iterator socConversionIt = societiesFromImport.begin();
        while (socConversionIt != societiesFromImport.end())
        {
            Duco::ObjectId newSocId = database->Database().AssociationsDatabase().SuggestAssociationOrCreate(socConversionIt->second);
            std::pair<unsigned int, Duco::ObjectId> newSocConversion(socConversionIt->first, newSocId);
            societies.insert(newSocConversion);
            ++socConversionIt;
        }
    }
    if (!worker->CancellationPending)
    {
        ImportSingleStringFromTable(command, "Composers", composers, DucoUI::WinRkImportProgressCallback::TImportStage::EImportingComposers, worker);
    }

    if (!worker->CancellationPending)
        ImportPeals(command, societies, composers, ringerAkas, towerTenors, worker);
    if (!worker->CancellationPending)
        database->Database().PostWinRkImportChecks();

    towerTenors.clear();
    societies.clear();
    composers.clear();

    if (worker->CancellationPending)
        e->Cancel = true;
}

System::String^
WinRKv2Import::FileType()
{
    return "WinRk (version 2)";
}

/*****************************************************************************************
* Import from tables
******************************************************************************************/

void
WinRKv2Import::ImportPeals(OdbcCommand^ command,
                         const std::map<unsigned int, Duco::ObjectId>& societies, 
                         const std::map<unsigned int, std::wstring>& composers, 
                         const std::map<Duco::ObjectId, Duco::ObjectId>& ringerAkas,
                         const std::map<unsigned int, std::wstring>& towerTenors,
                         System::ComponentModel::BackgroundWorker^ worker)
{
    command->CommandText = "SELECT COUNT(*) FROM Peals";
    int noOfRecords = (int)command->ExecuteScalar();
    int count(0);
    ReportProgress(DucoUI::WinRkImportProgressCallback::TImportStage::EImportingPeals, noOfRecords, count);
    command->CommandText = "SELECT * FROM Peals";
    OdbcDataReader^ reader = command->ExecuteReader();
    bool containsPeals = false;
    bool containsQuarters = false;

    while (reader->Read() && !worker->CancellationPending)
    {
        ++count;
        bool quarterPeal = reader->GetBoolean(40);
        if (quarterPeal && !containsQuarters)
        {
            containsQuarters = true;
        }
        else if (!quarterPeal && !containsPeals)
        {
            containsPeals = true;
        }

        unsigned int pealId = reader->GetInt32(0);
        unsigned int societyId = reader->GetInt32(1);
        unsigned int towerId = reader->GetInt32(2);
        Duco::PerformanceDate performanceDate = DucoUtils::ConvertDate(reader->GetDate(3));
        Duco::PerformanceTime performanceTime = DucoUtils::ToTime(ReadString(reader, 5));

        unsigned int changes = reader->GetInt32(6);
        wstring methodName;
        ReadString(reader, 7, methodName);
        wstring methodType;
        ReadString(reader, 8, methodType);
        wstring orderName;
        ReadString(reader, 9, orderName);
        unsigned int order = -1;
        bool splicedOrder = false;

        if (database->Database().MethodsDatabase().FindOrderNumber(orderName, order, splicedOrder))
        {
            unsigned int composerId = reader->GetInt32(10);
            Duco::ObjectId conductorId = DatabaseImportUtil::CheckRingerAkaIds(reader->GetInt32(11), ringerAkas);
            bool handBells = reader->GetBoolean(12);

            int handbellOffset = 0;
            std::map<unsigned int, Duco::ObjectId> ringerIds;
            int bellNumber (0);
            for (int fieldNumber = 13;  fieldNumber <= 32; ++fieldNumber)
            {
                Duco::ObjectId ringerId = reader->GetInt32(fieldNumber);
                if (ringerId.Id() == 0)
                {
                    ringerId.ClearId();
                }
                if (ringerId.ValidId())
                {
                    ringerId = DatabaseImportUtil::CheckRingerAkaIds(ringerId, ringerAkas);
                    pair<unsigned int, Duco::ObjectId> newObject(++bellNumber, ringerId);
                    ringerIds.insert(newObject);
                }
                if (handBells)
                {
                    ++fieldNumber;
                    ++handbellOffset;
                }
            }
            size_t noOfBells (ringerIds.size());
            if (handBells)
            {
                noOfBells *= 2;
            }

            // Just incase the number of ringers doesn't match the order of the method
            if (DucoEngineUtils::IsEven(order) && ringerIds.size() > order)
            {
                order = unsigned int (ringerIds.size());
            }
            else if (!DucoEngineUtils::IsEven(order) && ringerIds.size() > (order+1))
            {
                order = unsigned int (ringerIds.size() -1);
            }

            // Add strapper
            Duco::ObjectId strapperId = reader->GetInt32(33);
            if (strapperId.Id() == 0)
            {
                strapperId.ClearId();
            }
            if (strapperId.ValidId())
            {
                strapperId = DatabaseImportUtil::CheckRingerAkaIds(strapperId, ringerAkas);
            }
            wstring notes;
            ReadString(reader, 34, notes);
            wstring splicedMethods;
            ReadString(reader, 35, splicedMethods);

            std::map<unsigned int, Duco::ObjectId>::const_iterator socIt = societies.find(societyId);
            std::map<unsigned int, std::wstring>::const_iterator compIt = composers.find(composerId);
            Duco::ObjectId ringId = FindOrAddRing(towerId, noOfBells, towerTenors);
            Duco::ObjectId methodId = SuggestMethodId(methodName, methodType, order);

            if (ringId.ValidId() && methodId.ValidId())
            {
                std::wstring composer;
                if (compIt != composers.end())
                {
                    composer = compIt->second;
                }
                std::set<unsigned int> composerIds;
                composerIds.insert(composerId);
                std::set<Duco::ObjectId> conductorIds;
                if (conductorId > 0)
                {
                    conductorIds.insert(conductorId);
                }
                Duco::ObjectId newSocietyId;
                if (societies.find(societyId) != societies.end())
                {
                    newSocietyId = societies.find(societyId)->second;
                }
                Peal newPeal (pealId, newSocietyId, towerId, ringId, methodId, composer, notes, performanceDate,
                    performanceTime, changes, handBells, ringerIds, ESingleConductor, conductorIds, 0);

                if (splicedMethods.length() > 0)
                {
                    newPeal.AddMultiMethods(-1, splicedMethods);
                }
                if (strapperId.ValidId())
                {
                    newPeal.SetRingerId(strapperId, unsigned int(ringerIds.size()), true);
                }
                database->AddPeal(newPeal);
            }
            else
            {
                _ASSERT(false);
            }
        }

        ReportProgress(DucoUI::WinRkImportProgressCallback::TImportStage::EImportingPeals, noOfRecords, count);
        if (containsQuarters && !containsPeals)
        {
            database->Settings().SetPealDatabase(false);
        }
    }
    reader->Close();
}

void
WinRKv2Import::ImportRingers(OdbcCommand^ command, std::map<Duco::ObjectId, Duco::ObjectId>& ringerAkas,
                             System::ComponentModel::BackgroundWorker^ worker)
{
    command->CommandText = "SELECT COUNT(*) FROM Ringers";
    int noOfRecords = (int)command->ExecuteScalar();
    int count(0);

    ReportProgress(DucoUI::WinRkImportProgressCallback::TImportStage::EImportingRingers, noOfRecords, count);
    std::map<Duco::ObjectId, Duco::OldRingerNameChange*> unprocessedRingerAkas;

    command->CommandText = "SELECT * FROM Ringers";
    OdbcDataReader^ reader = command->ExecuteReader();

    while (reader->Read() && !worker->CancellationPending)
    {
        Duco::ObjectId ringerId = reader->GetInt16(0);
        if (ringerId.Id() == 0)
            ringerId.ClearId();
        std::wstring ringerName;
        ReadString(reader,2, ringerName);
        wstring::size_type seperator = ringerName.find(fieldSeperator);
        wstring::size_type spaceBetweenFields = 2;
        bool reverseFields = false;
        if (seperator == wstring::npos)
        {
            seperator = ringerName.find(altFieldSeperator);
            spaceBetweenFields = 1;
            reverseFields = true;
        }
        if (seperator == wstring::npos)
        {
            seperator = 0;
            spaceBetweenFields = 0;
        }

        std::wstring nextRingerSurname;
        std::wstring nextRingerForename;
        if (seperator != wstring::npos)
            {
                nextRingerSurname = ringerName.substr(0, seperator);
                if (ringerName.length() > seperator+2)
                {
                    nextRingerForename = ringerName.substr(seperator+spaceBetweenFields);
                }
            }
        else
        {
                nextRingerSurname = ringerName;
        }
        if (reverseFields)
        {
            std::wstring tempName = nextRingerForename;
            nextRingerForename = DucoEngineUtils::Trim(nextRingerSurname);
            nextRingerSurname = DucoEngineUtils::Trim(tempName);
        }
        else
        {
            nextRingerForename = DucoEngineUtils::Trim(nextRingerForename);
            nextRingerSurname = DucoEngineUtils::Trim(nextRingerSurname);
        }

        Ringer ringer(ringerId, nextRingerForename, nextRingerSurname);

        bool ringerAkaAdded (false);
        if (!reader->IsDBNull(11))
        {
            Duco::ObjectId previousRingerId = reader->GetInt32(11);
            std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator it = ringerAkas.find(previousRingerId);
            if (it != ringerAkas.end())
            {
                previousRingerId = it->second;
            }
            if (previousRingerId > 0 && previousRingerId != -1)
            {
                Duco::PerformanceDate nameChangedDate (ReadDateFromV2(reader, 12));
                Ringer oldRinger;
                if (database->FindRinger(previousRingerId, oldRinger, false))
                {
                    Ringer copiedRinger (oldRinger);
                    CheckForOldNameChanges(copiedRinger, ringerId, unprocessedRingerAkas, ringerAkas);
                    copiedRinger.AddNameChange(ringer, nameChangedDate);
                    if (database->Database().RingersDatabase().UpdateObject(copiedRinger))
                    {
                        pair<Duco::ObjectId, Duco::ObjectId> newObject(ringerId, previousRingerId);
                        ringerAkas.insert(newObject);
                        ringerAkaAdded = true;
                    }
                }
                if (!ringerAkaAdded)
                {
                    Duco::OldRingerNameChange* newName = new Duco::OldRingerNameChange();
                    newName->surname = nextRingerSurname;
                    newName->forename = nextRingerForename;
                    newName->date = nameChangedDate;
                    newName->otherRingerId = ringerId;
                    pair<Duco::ObjectId, Duco::OldRingerNameChange*> newObject(previousRingerId, newName);
                    unprocessedRingerAkas.insert(newObject);
                    ringerAkaAdded = true;
                }
            }
        }

        if (!ringerAkaAdded)
        {
            CheckForOldNameChanges(ringer, ringerId, unprocessedRingerAkas, ringerAkas);
            database->AddRinger(ringer);
        }
        ReportProgress(DucoUI::WinRkImportProgressCallback::TImportStage::EImportingRingers, noOfRecords, ++count);
    }

    reader->Close();
}

void
WinRKv2Import::CheckForOldNameChanges(Ringer& ringer, const Duco::ObjectId& otherRingerId,
                                      std::map<Duco::ObjectId, Duco::OldRingerNameChange*>& unprocessedRingerAkas,
                                      std::map<Duco::ObjectId, Duco::ObjectId>& ringerAkas)
{
    std::map<Duco::ObjectId, Duco::OldRingerNameChange*>::iterator otherAkasIt = unprocessedRingerAkas.find(ringer.Id().Id());
    if (otherAkasIt != unprocessedRingerAkas.end())
    {
        ringer.InsertNameChange(otherAkasIt->second->forename,
                         otherAkasIt->second->surname,
                         otherAkasIt->second->date);
        pair<Duco::ObjectId,Duco::ObjectId> newObject(otherAkasIt->second->otherRingerId, ringer.Id());
        ringerAkas.insert(newObject);
        delete otherAkasIt->second;
        unprocessedRingerAkas.erase(otherAkasIt);
    }
    otherAkasIt = unprocessedRingerAkas.find(otherRingerId);
    if (otherAkasIt != unprocessedRingerAkas.end())
    {
        ringer.InsertNameChange(otherAkasIt->second->forename,
                         otherAkasIt->second->surname,
                         otherAkasIt->second->date);
        pair<Duco::ObjectId,Duco::ObjectId> newObject(otherAkasIt->second->otherRingerId, ringer.Id());
        ringerAkas.insert(newObject);
        delete otherAkasIt->second;
        unprocessedRingerAkas.erase(otherAkasIt);
    }
}

void
WinRKv2Import::ImportTowers(OdbcCommand^ command, map<unsigned int, wstring>& towerTenors,
                         System::ComponentModel::BackgroundWorker^ worker)
{
    towerTenors.clear();

    command->CommandText = "SELECT COUNT(*) FROM Towers";
    int noOfRecords = (int)command->ExecuteScalar();
    int count(0);

    ReportProgress(DucoUI::WinRkImportProgressCallback::TImportStage::EImportingTowers, noOfRecords, count);
    command->CommandText = "SELECT * FROM Towers";
    OdbcDataReader^ reader = command->ExecuteReader();

    while (reader->Read() && !worker->CancellationPending)
    {
        unsigned int towerId = reader->GetInt16(0);
        wstring city;
        ReadString(reader, 1, city);
        wstring county;
        ReadString(reader, 2, county);
        wstring dedication;
        ReadString(reader, 3, dedication);

        wstring tenorUnits;
        ReadString(reader, 5, tenorUnits);
        wstring tenorStr;
        if (reader->IsDBNull(4))
        {
            tenorStr.append(tenorUnits);
        }
        else
        {
            unsigned int tenorInt = unsigned int(reader->GetFloat(4));
            const size_t bufferSize (tenorUnits.length() + 5);
            wchar_t* tenor = new wchar_t[bufferSize];
            swprintf_s(tenor, bufferSize, L"%d%s", tenorInt, tenorUnits.c_str());
            tenorStr.append(tenor);
            delete[] tenor;
        }

        wstring notes;
        ReadString(reader, 10, notes);

        Tower tower(towerId, dedication, city, county, -1);
        database->AddTower(tower);

        std::pair<unsigned int, wstring> newObject(towerId, tenorStr);
        towerTenors.insert(newObject).second;

        ReportProgress(DucoUI::WinRkImportProgressCallback::TImportStage::EImportingTowers, noOfRecords, ++count);
    }
    reader->Close();
}

Duco::ObjectId
WinRKv2Import::SuggestMethodId(const std::wstring& methodName, const std::wstring& methodType, unsigned int order)
{
    Duco::ObjectId methodId = database->Database().MethodsDatabase().SuggestMethod(methodName, methodType, order);
    if (!methodId.ValidId())
    {
        methodId = database->Database().MethodsDatabase().AddMethod(methodName, methodType, order, false);
        database->CallObservers(EAdded, TObjectType::EMethod, methodId, KNoId);
    }
    return methodId;
}

Duco::ObjectId
WinRKv2Import::FindOrAddRing(unsigned int towerId, unsigned int noOfBells, const map<unsigned int, wstring>& towerTenors)
{
    const Tower* const theTower = database->Database().TowersDatabase().FindTower(towerId);

    Duco::ObjectId ringId;
    if (!theTower->SuggestRing(noOfBells, L"", L"", ringId, true))
    {
        std::wstring tenorName;
        map<unsigned int, wstring>::const_iterator it = towerTenors.find(towerId);
        if (it != towerTenors.end())
        {
            tenorName.append(it->second);
        }

        if (theTower->Bells() == -1 || theTower->Bells() < noOfBells)
        {
            Tower* newTower = new Tower(*theTower);
            newTower->SetNoOfBells(noOfBells);
            ObjectId newRingId = newTower->NextFreeRingId();
            std::set<unsigned int> newBells;
            GenerateBells(noOfBells, newTower->Bells(), newBells);
            std::wstring newRingName = DucoEngineUtils::ToString(noOfBells) + KRingNameSuffix;
            Ring newRing(newRingId, noOfBells, newRingName, newBells, tenorName, L"");
            newTower->AddRing(newRing);

            if (database->Database().TowersDatabase().UpdateObject(*newTower))
            {
                ringId = newRingId;
            }
        }
        else if (theTower->Bells() == noOfBells)
        {
            _ASSERT(false);
        }
        else if (theTower->Bells() > noOfBells)
        {
            Tower* newTower = new Tower(*theTower);
            ObjectId newRingId = newTower->NextFreeRingId();
            std::set<unsigned int> newBells;
            GenerateBells(noOfBells, newTower->Bells(), newBells);
            std::wstring newRingName = DucoEngineUtils::ToString(noOfBells) + KRingNameSuffix;
            Ring newRing(newRingId, noOfBells, newRingName, newBells, tenorName, L"");
            newTower->AddRing(newRing);
            if (database->Database().TowersDatabase().UpdateObject(*newTower))
            {
                ringId = newRingId;
            }
        }
    }

    return ringId;
}

void
WinRKv2Import::GenerateBells(unsigned int noOfBellsInRing, unsigned int noOfBellsInTower, std::set<unsigned int>& newBells)
{
    newBells.clear();
    unsigned int i = noOfBellsInTower;
    for (unsigned int count = 0; count < noOfBellsInRing; ++count)
    {
        newBells.insert(i);
        --i;
    }
}

double 
WinRKv2Import::StageNumber(WinRkImportProgressCallback::TImportStage currentStage)
{
    double stageNumber = (double)currentStage;
    switch (currentStage)
    {
    case WinRkImportProgressCallback::TImportStage::EImportingRingers:
    default:
        break;
    case WinRkImportProgressCallback::TImportStage::EImportingTowers:
        stageNumber -= 3;
        break;
    case WinRkImportProgressCallback::TImportStage::EImportingSocieties:
    case WinRkImportProgressCallback::TImportStage::EImportingComposers:
    case WinRkImportProgressCallback::TImportStage::EImportingPeals:
        stageNumber -= 3;
        stageNumber -= 7;
        break;
    }
    return stageNumber-1;
}

double
WinRKv2Import::TotalNoOfStages()
{
    return (double)5;
}
