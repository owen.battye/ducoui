#pragma once

namespace DucoUI
{
    public ref class RingerItem
    {
    public:
        RingerItem(System::String^ _displayName, System::UInt32 _id);
        System::String^ DisplayName();
        System::Void SetDisplayName(System::String^ newName);
        System::UInt32 Id();
        System::String^ ToString() override;

    private:
        System::String^ displayName;
        System::UInt32 id;
    };

    public ref class RingerDisplayCache
    {
    public:
        static DucoUI::RingerDisplayCache^ CreateDisplayCacheWithConductors(DucoUI::DatabaseManager^ database, System::Boolean addBlank);
        static DucoUI::RingerDisplayCache^ CreateDisplayCacheWithFeePayers(DucoUI::DatabaseManager^ database, System::Boolean addBlank);
        static DucoUI::RingerDisplayCache^ CreateDisplayCacheWithRingers(DucoUI::DatabaseManager^ database, System::Boolean currentRingers, System::Boolean addBlank);
        static DucoUI::RingerDisplayCache^ CreateDisplayCacheWithRingers(DucoUI::DatabaseManager^ database, const Duco::PerformanceDate& _nameRenderingDate, System::Boolean currentRingers, System::Boolean addBlank);

        ~RingerDisplayCache();
        !RingerDisplayCache();

        System::Boolean NeedRegeneration(const Duco::PerformanceDate& _nameRenderingDate);

        System::Void GenerateFeePayersOptions(const Duco::ObjectId& associationId);
        System::Void GenerateRingerOptions(const Duco::PerformanceDate& _nameRenderingDate);
        System::Void GenerateConductorOptions();
        System::Void PopulateControl(System::Windows::Forms::ComboBox^ control, System::Boolean setDefaultRinger);
        System::Void PopulateControl(System::Windows::Forms::ListBox^ control, System::Boolean setDefaultRinger);
        System::Void PopulateControl(System::Windows::Forms::ListControl^ control, System::Boolean setDefaultRinger);
        System::Void RepopulateControl(System::Windows::Forms::ComboBox^ toControl, Duco::ObjectId& ringerId);
        System::Int32 AddRingerOption(const Duco::ObjectId& ringerId);
        System::Boolean UpdateRingerOption(const Duco::ObjectId& ringerId);
        System::Boolean RemoveRingerOption(const Duco::ObjectId& ringerId);

        std::set<Duco::ObjectId> FindIds(System::Windows::Forms::ListBox::SelectedObjectCollection^ indexes);
        System::Int32 FindIndexOrAdd(const Duco::ObjectId& id);
        System::Boolean RingerSubsetDisplayEnabled();

    private:
        RingerDisplayCache(DucoUI::DatabaseManager^ database, const Duco::PerformanceDate& perfDate, System::Boolean addBlank, System::Boolean currentRingers);

        DucoUI::DatabaseManager^ database;
        System::ComponentModel::BindingList<DucoUI::RingerItem^>^ bindingList;
        Duco::PerformanceDate* nameRenderingDate;
        System::Boolean addBlank;
        System::Boolean currentRingers;
        System::Int32 yearsToCache;

        System::Boolean ringerSubsetDisplayEnabled;
    };
}