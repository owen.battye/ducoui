#include <set>
#include <ObjectId.h>
#include "DatabaseManager.h"
#include <RingerPealDates.h>

#include "ChooseOrGenerateRinger.h"

#include <Peal.h>
#include "DucoUtils.h"
#include "SystemDefaultIcon.h"
#include <StatisticFilters.h>
#include <RingingDatabase.h>
#include <PealDatabase.h>
#include "SoundUtils.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace DucoUI;

ChooseOrGenerateRinger::ChooseOrGenerateRinger(DatabaseManager^ theDatabase, const std::set<Duco::RingerPealDates>& nearMisses, const Duco::Peal& thePeal, System::String^ missingRinger, System::Boolean fromPealUI)
    : database(theDatabase)
{
    newRingerId = new Duco::ObjectId();
    ringerIds = gcnew System::Collections::Generic::List<System::Int64>(nearMisses.size());
    missingRingerName = missingRinger;

    InitializeComponent();
    this->Text += L" to replace " + missingRinger;
    CreateRingerControls(thePeal, nearMisses);
    if (nearMisses.size() == 0)
    {
        this->okButton->Enabled = false;
        if (fromPealUI)
        {
            ringerLbl->Text = L"Could not find a similar match for " + missingRingerName + "; Choose cancel to generate a new one";
            newBtn->Visible = false;
        }
        else
        {
            ringerLbl->Text = L"Could not find a similar match for " + missingRingerName + "; Generate a new ringer or click cancel to open Peal window";
        }
    }
    else
    {
        if (fromPealUI)
        {
            ringerLbl->Text = L"Could not find an exact match for " + missingRingerName + "; Choose ringer or cancel to generate a new one";
            newBtn->Visible = false;
        }
        else
        {
            ringerLbl->Text = L"Could not find an exact match for " + missingRingerName + "; Choose ringer, generate a new one or click cancel to open Peal window";
        }
    }

    std::wstring title;
    thePeal.SummaryTitle(title, theDatabase->Database());
    descriptionLabel->Text = L"Peal info: " + DucoUtils::ConvertString(title) + L" on " + DucoUtils::ConvertString(thePeal.Date().Str());
    this->tableLayoutPanel1->PerformLayout();
}

ChooseOrGenerateRinger::~ChooseOrGenerateRinger()
{
    delete newRingerId;
    if (components)
    {
        delete components;
    }
}

void
ChooseOrGenerateRinger::CreateRingerControls(const Duco::Peal& thePeal, const std::set<Duco::RingerPealDates>& nearMisses)
{
    Boolean foundSingle = true;
    for (const auto& it : nearMisses)
    {
        System::Windows::Forms::RadioButton^ radioButton1 = gcnew System::Windows::Forms::RadioButton();
        ringers->Controls->Add(radioButton1);
        radioButton1->AutoSize = true;
        radioButton1->Location = System::Drawing::Point(3, 3);
        radioButton1->Name = L"radioButton1";
        radioButton1->Size = System::Drawing::Size(85, 17);
        radioButton1->TabIndex = 0;
        radioButton1->TabStop = true;
        radioButton1->Text = database->RingerName(it.ringerId, thePeal.Date());
        ringerIds->Add(it.ringerId.Id());
        radioButton1->UseVisualStyleBackColor = true;
        if (it.BeforeAndAfter())
        {
            radioButton1->Checked = true;
        }
        radioButton1->Text += L" (" + DucoUtils::ConvertString(it.Description()) + L")";
    }
    if (ringerIds->Count == 0)
    {
        System::Windows::Forms::Label^ noRingersLbl = (gcnew System::Windows::Forms::Label());
        noRingersLbl->AutoSize = true;
        ringers->Controls->Add(noRingersLbl);
        noRingersLbl->Dock = System::Windows::Forms::DockStyle::Fill;
        noRingersLbl->Location = System::Drawing::Point(3, 3);
        noRingersLbl->Margin = System::Windows::Forms::Padding(3);
        noRingersLbl->Name = L"noRingersLbl";
        noRingersLbl->Size = System::Drawing::Size(448, 13);
        noRingersLbl->TabIndex = 3;
        noRingersLbl->Text = L"No similar ringers found";
    }
}

System::Void
ChooseOrGenerateRinger::ChooseRinger_Load(System::Object^ sender, System::EventArgs^ e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
}

Duco::ObjectId
ChooseOrGenerateRinger::SelectedId()
{
    if (newRingerId->ValidId())
        return *newRingerId;

    for (int i = 0; i < ringers->Controls->Count; ++i)
    {
        System::Windows::Forms::RadioButton^ radioButton = dynamic_cast<System::Windows::Forms::RadioButton^>(ringers->Controls[i]);
        if (radioButton->Checked)
        {
            return Duco::ObjectId(ringerIds[i]);
        }
    }
    return Duco::ObjectId();
}

System::Void
ChooseOrGenerateRinger::okButton_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (SelectedId().ValidId())
    {
        this->DialogResult = ::DialogResult::OK;
        Close();
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
ChooseOrGenerateRinger::newBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    *newRingerId = database->AddRinger(missingRingerName);
    if (newRingerId->ValidId())
    {
        this->DialogResult = ::DialogResult::OK;
    }
    Close();
}
