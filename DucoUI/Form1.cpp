#include "ProgressWrapper.h"
#include "ImportExportProgressWrapper.h"
#include "RingingDatabaseObserver.h"
#include "DucoWindowState.h"
#include "DatabaseManager.h"
#include "CheckLatestVersion.h"
#include "printAssociationReportUtil.h"
#include "SearchUtils.h"
#include "PrintPealSearchUtil.h"
#include "RingerDisplayCache.h"

#include "Form1.h"

#include <AssociationDatabase.h>
#include <TowerDatabase.h>
#include <MethodDatabase.h>
#include "BellboardParserWrapper.h"
#include <BellboardPerformanceParser.h>
#include "AssociationReportOptions.h"
#include <Association.h>
#include "BellboardIdEntry.h"
#include "BellboardUpdate.h"
#include <DatabaseSettings.h>
#include "DucoUtils.h"
#include "DucoCommon.h"
#include "DayOrderUI.h"
#include "DucoUIUtils.h"
#include "PealMapUI.h"
#include "StagesUI.h"
#include "OpenObjectCallback.h"
#include "PictureListControl.h"
#include "PictureManagerUI.h"
#include "AlphabetsUI.h"
#include "MethodSeriesUI.h"
#include "SoundUtils.h"
#include <DoveDatabase.h>
#include "ImportDoveIdsUI.h"
#include "PrintCompositionUtil.h"
#include "CompositionUI.h"
#include "MethodUI.h"
#include "PrintSummaryUtil.h"
#include <MilestoneData.h>
#include "MilestoneUI.h"
#include "PictureControl.h"
#include "RingerList.h"
#include <Tower.h>
#include "PrintBandSummaryUtil.h"
#include <DownloadedPeal.h>
#include "PealUI.h"
#include "ChangesCirclingUI.h"
#include "ReplaceAssociationUI.h"
#include "PealSpeedUI.h"
#include "DoveSearchWrapper.h"
#include <DoveSearch.h>
#include "GenericTablePrintUtil.h"
#include "SearchDoveUI.h"
#include "TowerStatsUI.h"
#include "DeleteUnusedUI.h"
#include "ConductedStatsUI.h"
#include "PrintPealsUI.h"
#include <RingingDatabase.h>
#include "PealTableUI.h"
#include "RenumberProgressWrapper.h"
#include "RenumberUI.h"
#include "TowerUI.h"
#include "RingerUI.h"
#include "MethodSearchUI.h"
#include "CompositionSearchUI.h"
#include "PeopleStatsUI.h"
#include "YearCirclingUI.h"
#include "MethodsCircledUI.h"
#include "GraphsUI.h"
#include "UnpaidPealFeesUI.h"
#include <PealDatabase.h>
#include <DucoVersionNumber.h>
#include "AboutUI.h"
#include "SettingsUI.h"
#include <DucoConfiguration.h>
#include "TowerSearchUI.h"
#include "MethodSearchUI.h"
#include "ImportProgress.h"
#include "DatabaseImportUtil.h"
#include "ImportFileUI.h"
#include "ImportNotationUI.h"
#include "MethodSeriesSearch.h"
#include "GenderStatsUI.h"
#include <MethodNotationDatabaseSingleImporter.h>
#include "DownloadPealUI.h"
#include "AssociationStatsUI.h"
#include "ReplaceTowerUI.h"
#include "MonthsUI.h"
#include "ReplaceRingerUI.h"
#include "RingerSearchUI.h"
#include <MergeDatabase.h>
#include "MergeUI.h"
#include "ImportSingleMethod.h"
#include "PealLengthUI.h"
#include "MethodStatsUI.h"
#include "CompletedSeriesUI.h"
#include "TowersCircledUI.h"
#include "BellsRungUI.h"
#include "WindowsSettings.h"
#include "AssociationsUI.h"
#include "SystemDefaultIcon.h"
#include "OnThisDay.h"
#include "DucoUILog.h"
#include "ImportDoveLocationUI.h"
#include "ImportDoveTowerTenorUI.h"
#include "PealSearchUI.h"
#include <StatisticFilters.h>
#include "AssociationSearchUI.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::Drawing::Printing;
using namespace System::IO;
using namespace System::Windows::Forms;


Form1::Form1(array<System::String ^>^ newArgs)
{
    importExportProgressWrapper = new ImportExportProgressWrapper(this);
    progressWrapper = new ProgressCallbackWrapper(this);
    args = newArgs;
    InitializeComponent();
#ifdef _WIN64
    this->Text = L"Duco - Ringing Database";
#endif
    this->ImportExportCompleteEvent += gcnew EventHandler<CompletedEventArgs^>(Form1::ImportExportCompleteHandler);
    firstOpened = true;
}

Form1::!Form1()
{
    if (database != nullptr)
    {
        database->RemoveObserver(this);
    }

    delete progressWrapper;
    progressWrapper = NULL;
}

Form1::~Form1()
{
    this->!Form1();
    if (database != nullptr)
        database->DeleteDatabase();
    if (components)
    {
        delete components;
    }
}

System::Void
Form1::SetState()
{
    Boolean importExportInProgress = database != nullptr && database->InternaliseExternaliseInProgress();
    Boolean databaseOpen = database != nullptr && database->DatabaseOpen() && !importExportInProgress;
    saveToolStripMenuItem->Enabled = databaseOpen && !importExportInProgress;
    saveAsToolStripMenuItem->Enabled = databaseOpen;
    onlineToolStripMenuItem->Enabled = databaseOpen;
    preferencesToolStripMenuItem->Enabled = databaseOpen;
    newPealToolStripMenuItem->Enabled = databaseOpen;
    showPealToolStripMenuItem->Enabled = databaseOpen;
    showAllPealsMenuItem->Enabled = databaseOpen;
    editStagesToolStripMenuItem->Enabled = databaseOpen;
    associationStatsToolStripMenuItem->Enabled = databaseOpen;
    associationSearchToolStripMenuItem->Enabled = databaseOpen;

    newAssociationToolStripMenuItem->Enabled = databaseOpen;
    viewAssociationsToolStripMenuItem->Enabled = databaseOpen;
    downloadToolStripMenuItem->Enabled = databaseOpen;
    downloadByIdToolStripMenuItem->Enabled = databaseOpen;
    towerCirclingToolStripMenuItem->Enabled = databaseOpen;
    methodCirclingToolStripMenuItem->Enabled = databaseOpen;
    yearCirclingToolStripMenuItem->Enabled = databaseOpen;
    graphsToolStripMenuItem->Enabled = databaseOpen;
    pealFeesToolStripMenuItem->Enabled = databaseOpen;
    bellsRungToolStripMenuItem->Enabled = databaseOpen;
    conductedStatsCmd->Enabled = databaseOpen;
    pealSearchToolStripMenuItem->Enabled = databaseOpen;
    onThisDayToolStripMenuItem->Enabled = databaseOpen;
    towerSearchToolStripMenuItem->Enabled = databaseOpen;
    updateFromDoveToolStripMenuItem->Enabled = databaseOpen;
    updateTowerLocationsToolBtn->Enabled = databaseOpen;
    importTenorKeysToolBtn->Enabled = databaseOpen;
    importFromDoveToolStripMenuItem->Enabled = databaseOpen;
    pealMapStripMenuItem->Enabled = databaseOpen;
    ringerSearchToolStripMenuItem->Enabled = databaseOpen;
    methodSearchToolStripMenuItem->Enabled = databaseOpen;
    importNotationToolStripMenuItem->Enabled = databaseOpen;
    importMethodToolStripMenuItem->Enabled = databaseOpen;
    peopleStatsToolStripMenuItem->Enabled = databaseOpen;
    pealLengthCmd->Enabled = databaseOpen;
    milestonesCmd->Enabled = databaseOpen;
    towerStatsToolStripMenuItem->Enabled = databaseOpen;
    clearInvalidTenorKeysToolStripMenuItem->Enabled = databaseOpen;
    methodStatsToolStripMenuItem->Enabled = databaseOpen;
    genderStatsToolStripMenuItem->Enabled = databaseOpen;
    picturesToolStripMenuItem->Enabled = databaseOpen;
    monthsToolStripMenuItem->Enabled = databaseOpen;
    replaceRingerToolStripMenuItem->Enabled = databaseOpen;
    replaceTowerToolStripMenuItem->Enabled = databaseOpen;
    replaceAssociationMenu->Enabled = databaseOpen;
    duplicateAssociationMenu->Enabled = databaseOpen;
    numberOfChangesMenu->Enabled = databaseOpen;
    fastestSlowestPealsCmd->Enabled = databaseOpen;
    alphabetsToolStripMenuItem->Enabled = databaseOpen;
    viewSeriesToolStripMenuItem->Enabled = databaseOpen;
    searchSeriesToolStripMenuItem->Enabled = databaseOpen;
    searchCompositionToolStripMenuItem->Enabled = databaseOpen;
    viewCompositionToolStripMenuItem->Enabled = databaseOpen;
    methodSeriesToolStripMenuItem->Enabled = databaseOpen;
    printPealsToolStripMenuItem->Enabled = databaseOpen;
    databaseSummaryToolStripMenuItem->Enabled = databaseOpen;
    associationReportToolStripMenuItem->Enabled = databaseOpen;
    mergeToolStripMenuItem->Enabled = databaseOpen;
    newTowerToolStripMenuItem->Enabled = databaseOpen;
    showTowerToolStripMenuItem->Enabled = databaseOpen;
    newRingerToolStripMenuItem->Enabled = databaseOpen;
    defaultRingerToolStripMenuItem->Enabled = databaseOpen;
    showRingerToolStripMenuItem->Enabled = databaseOpen;
    newMethodToolStripMenuItem->Enabled = databaseOpen;
    showMethodToolStripMenuItem->Enabled = databaseOpen;
    renumberToolStripMenuItem->Enabled = databaseOpen;
    yearOrderingToolStripMenuItem->Enabled = databaseOpen;
    deleteUnusedToolStripMenuItem->Enabled = databaseOpen;
    validateAllPealsCmd->Enabled = databaseOpen;
    validateAllTowersCmd->Enabled = databaseOpen;
    validateAllMethodsCmd->Enabled = databaseOpen;
    validateAllRingersCmd->Enabled = databaseOpen;
    validateAllSeriesCmd->Enabled = databaseOpen;
    validateAllCompositionsCmd->Enabled = databaseOpen;
    topTowersToolStripMenuItem->Enabled = databaseOpen;
    uppercaseAssociationsToolStripMenuItem->Enabled = databaseOpen;
    uppercaseTownsToolStripMenuItem->Enabled = databaseOpen;
    capitaliseRingerNamesToolStripMenuItem->Enabled = databaseOpen;
    capitaliseMethodsToolStripMenuItem->Enabled = databaseOpen;
    capitaliseTowersToolStripMenuItem->Enabled = databaseOpen;
    replaceShortMethodTypesToolStripMenuItem->Enabled = databaseOpen;
    removeDuplicateRingsToolStripMenuItem->Enabled = databaseOpen;
    removeDuplicateMethodsToolStripMenuItem->Enabled = databaseOpen;
    clearInvalidTenorKeysToolStripMenuItem->Enabled = databaseOpen;
    updateIdsAndRWRefs->Enabled = databaseOpen;
    correctTowerTypes->Enabled = databaseOpen;

    // All others must go here.
    newToolStripMenuItem->Enabled = !importExportInProgress;
    openToolStripMenuItem->Enabled = !importExportInProgress;
    importToolStripMenuItem->Enabled = !importExportInProgress;
    historyToolStripMenuItem->Enabled = !importExportInProgress;
}

System::Void
Form1::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    switch (eventId)
    {
    case EAdded:
    case EDeleted:
        switch (type)
        {
            case TObjectType::EPeal:
                {
                StatisticFilters filters(database->Database());
                Duco::PealLengthInfo noOfPeals = database->Database().PealsDatabase().PerformanceInfo(filters);
                filters.SetIncludeWithdrawn(true);
                filters.SetExcludeValid(true);
                Duco::PealLengthInfo withdrawnCount = database->Database().PealsDatabase().PerformanceInfo(filters);
                SetNumberOfPealsLabel(noOfPeals.TotalPeals(), withdrawnCount.TotalPeals());
                }
                break;
            case TObjectType::ETower:
                {
                    size_t noOfLinkedTowers(0);
                    SetNumberOfTowersLabel(database->Database().NumberOfActiveTowers(noOfLinkedTowers), database->Database().NumberOfRemovedTowers(), noOfLinkedTowers);
                }
                break;
            case TObjectType::ERinger:
                SetNumberOfRingersLabel(database->NumberOfObjects(TObjectType::ERinger));
                break;
            case TObjectType::EMethod:
                SetNumberOfMethodsLabel(database->NumberOfObjects(TObjectType::EMethod));
                break;
            case TObjectType::EPicture:
                SetNumberOfPicturesLabel(database->NumberOfObjects(TObjectType::EPicture));
                break;
            case TObjectType::EAssociationOrSociety:
                SetNumberOfAssociationsLabel(database->NumberOfObjects(TObjectType::EAssociationOrSociety));
                break;
            case TObjectType::EComposition:
            default:
                break;
        }
        break;

    case ECleared:
        {
        SetState();
        ClearAllObjectLabels();
        SetPealsMenuCommandTitle();
        }
        break;

    case EImportComplete:
        SetState();
        // no break on purpose

    case EUpdated:
        SetPealsMenuCommandTitle();
        SetAllObjectLabels();
        break;

    default:
        break;
    }
}

void
Form1::Initialised()
{
    toolStripProgressBar1->Maximum = 100;
    toolStripProgressBar1->Minimum = 0;
    toolStripProgressBar1->Value = 0;
}

void
Form1::Step(int progressPercent)
{
    if (progressPercent > 100)
    {
        progressPercent = 100;
    }
    toolStripProgressBar1->Value = progressPercent;
}

void
Form1::Complete()
{
    toolStripProgressBar1->Value = 0;
}

System::Void 
Form1::InitialisingImportExport(Boolean internalising, unsigned int versionNumber, size_t totalNumberOfObjects)
{
    toolStripProgressBar1->Maximum = 100;
    toolStripProgressBar1->Minimum = 0;
    toolStripProgressBar1->Value = 0;
}

System::Void 
Form1::ObjectProcessed(System::Boolean internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
{
    DUCOUILOGDEBUG("Form1::ObjectProcessed: " + Convert::ToString(totalPercentage) + ": " + Convert::ToString(numberInThisSubDatabase));
    try
    {
        toolStripProgressBar1->Value = totalPercentage;
        if (internalising)
        {
            switch (objectType)
            {
            case TObjectType::EPeal:
                SetNumberOfPealsLabel(numberInThisSubDatabase, 0);
                break;
            case TObjectType::EMethod:
                SetNumberOfMethodsLabel(numberInThisSubDatabase);
                break;
            case TObjectType::ETower:
                SetNumberOfTowersLabel(numberInThisSubDatabase, 0, 0);
                break;
            case TObjectType::ERinger:
                SetNumberOfRingersLabel(numberInThisSubDatabase);
                break;
            case TObjectType::EPicture:
                SetNumberOfPicturesLabel(numberInThisSubDatabase);
                break;
            case TObjectType::EAssociationOrSociety:
                SetNumberOfAssociationsLabel(numberInThisSubDatabase);
                break;
            case TObjectType::EComposition:
            default:
                break;
            }
        }
    }
    catch (Exception^ ex)
    {
        DucoUILog::PrintError("Form1::ObjectProcessed Error");
        MessageBox::Show(MdiParent, ex->ToString(), "Error during export/import operation");
    }
}
void
Form1::ImportExportCompleteHandler(Object^ sender, DucoUI::CompletedEventArgs^ e)
{
    DUCOUILOGDEBUG("Form1::ImportExportCompleteHandler");
    Form1^ form1 = static_cast<DucoUI::Form1^>(sender);
    form1->ImportExportComplete(e->Import);
}

System::Void 
Form1::ImportExportComplete(System::Boolean internalising)
{
    DUCOUILOGDEBUG("Form1::ImportExportComplete");
    toolStripProgressBar1->Value = 0;

    if (savingOnExit)
    {
        Close();
    }
    else if (fileToOpenWhenSaveComplete != nullptr && fileToOpenWhenSaveComplete->Length > 0)
    {
        OpenNewDatabase(fileToOpenWhenSaveComplete);
        fileToOpenWhenSaveComplete = "";
    }
    else if (internalising)
    {
        if (database->WindowsSettings()->AutoRenumberDisabled() || !CheckRebuildRecommended(false))
        {
            if (database->NumberOfObjects(TObjectType::EPeal) > 0)
            {
                PealTableUI::ShowAllPeals(database, this);
                if (!database->Config().DisableFeaturesForPerformance())
                {
                    if (database->AnyOnThisDay())
                    {
                        OnThisDay^ onThisDayUI = gcnew OnThisDay(database);
                        onThisDayUI->MdiParent = this;
                        onThisDayUI->Show();
                    }
                }
            }
        }
    }
}

System::Void
Form1::ImportExportFailed(Boolean internalising, int errorCode)
{
    toolStripProgressBar1->Value = 0;
    if (savingOnExit)
    {
        Close();
    }
    else if (fileToOpenWhenSaveComplete != nullptr && fileToOpenWhenSaveComplete->Length > 0)
    {
        OpenNewDatabase(fileToOpenWhenSaveComplete);
        fileToOpenWhenSaveComplete = "";
    }
    else if (internalising)
    {
        MessageBox::Show("Error reading file");
    }
}

Boolean
Form1::CheckRebuildRecommended(Boolean forceStart)
{
    if (!latestVersionChecker->ShowingUpgradeDialog() && !database->InternaliseExternaliseInProgress())
    {
        if ((forceStart || database->CheckRebuildRecommended()) && CheckNoChildForms(true, "rebuilding indexes"))
        {
            if (this->Visible && this->CanFocus)
            {
                // modal child windows are not open
                RenumberUI^ uiForm = gcnew RenumberUI(database, this);
                try
                {
                    uiForm->ShowDialog();
                    return true;
                }
                catch (Exception^ e)
                {
                    DucoUILog::PrintError("Form1::CheckRebuildRecommended Showing renumber dialog");
                    DucoUILog::PrintError(e->ToString());
                }
            }
        }
    }
    if (forceStart)
    {
        SoundUtils::PlayErrorSound();
    }
    return false;
}

System::Void 
Form1::SetPealsMenuCommandTitle()
{
    if (database->Settings().PealDatabase())
    {
        pealsToolStripMenuItem->Text = L"&Peals";
    }
    else
    {
        pealsToolStripMenuItem->Text = L"&Quarters";
    }
}

System::Void
Form1::SetAllObjectLabels()
{
    StatisticFilters filters(database->Database());
    size_t noOfPeals = database->Database().PealsDatabase().PerformanceInfo(filters).TotalPeals();
    filters.SetExcludeValid(true);
    filters.SetIncludeWithdrawn(true);
    size_t withdrawnCount = database->Database().PealsDatabase().PerformanceInfo(filters).TotalPeals();
    SetNumberOfPealsLabel(noOfPeals, withdrawnCount);
    size_t noOfLinkedTowers (0);
    size_t noOfActiveTowers = database->Database().NumberOfActiveTowers(noOfLinkedTowers);
    SetNumberOfTowersLabel(noOfActiveTowers, database->Database().NumberOfRemovedTowers(), noOfLinkedTowers);
    SetNumberOfMethodsLabel(database->NumberOfObjects(TObjectType::EMethod));
    SetNumberOfRingersLabel(database->NumberOfObjects(TObjectType::ERinger));
    SetNumberOfPicturesLabel(database->NumberOfObjects(TObjectType::EPicture));
    SetNumberOfAssociationsLabel(database->NumberOfObjects(TObjectType::EAssociationOrSociety));
    SetFileNameDisplay(database->Filename());
}

System::Void
Form1::ClearAllObjectLabels()
{
    SetNumberOfPealsLabel(0, 0);
    SetNumberOfTowersLabel(0, 0, 0);
    SetNumberOfMethodsLabel(0);
    SetNumberOfRingersLabel(0);
    SetNumberOfPicturesLabel(0);
    SetNumberOfAssociationsLabel(0);
    SetFileNameDisplay(database->Filename());
}

System::Void 
Form1::SetNumberOfPealsLabel(size_t totalNoOfPeals, size_t withdrawnCount)
{
    try
    {
        if (database->Settings().PealDatabase())
        {
            if (totalNoOfPeals == 1)
                pealsStatusLabel->Text = ("1 Peal");
            else
                pealsStatusLabel->Text = (totalNoOfPeals + " Peals");
        }
        else if (totalNoOfPeals == 1)
            pealsStatusLabel->Text = ("1 Quarter");
        else
            pealsStatusLabel->Text = (totalNoOfPeals + " Quarter peals");

        if (withdrawnCount == 1)
            pealsStatusLabel->ToolTipText = "Plus 1 withdrawn performance";
        else if (withdrawnCount > 1)
            pealsStatusLabel->ToolTipText = "Plus " + withdrawnCount + " withdrawn performances";
        else
        {
            pealsStatusLabel->ToolTipText = "";
        }
    }
    catch (Exception^ ex)
    {
        DucoUILog::PrintError(ex->ToString());
    }
}

System::Void
Form1::SetNumberOfTowersLabel(size_t number, size_t removed, size_t noOfLinkedTowers)
{
    try
    {
        if (number == 1)
        {
            towersStatusLabel->Text = ("1 Tower");
        }
        else
        {
            towersStatusLabel->Text = (number + " Towers");
        }

        String^ additionalTowersText = "(";

        bool added(false);
        if (removed == 1)
        {
            additionalTowersText += "1 removed tower";
            added = true;
        }
        else if (removed > 1)
        {
            additionalTowersText += removed + " removed towers";
            added = true;
        }
        if (noOfLinkedTowers > 0)
        {
            if (added)
                additionalTowersText += " and ";
            if (noOfLinkedTowers == 1)
            {
                additionalTowersText += "1 linked tower";
                added = true;
            }
            else if (noOfLinkedTowers > 1)
            {
                additionalTowersText += noOfLinkedTowers + " linked towers";
                added = true;
            }
        }

        if (added)
            towersStatusLabel->ToolTipText = additionalTowersText + ")";
        else
        {
            towersStatusLabel->ToolTipText = "";
        }
    }
    catch (Exception^ ex)
    {
        DucoUILog::PrintError(ex->ToString());
    }
}

System::Void
Form1::SetNumberOfPicturesLabel(size_t number)
{
    try
    {
        if (number == 1)
        {
            picturesStatusLabel->Text = ("1 Picture");
            picturesStatusLabel->Visible = true;
        }
        else
        {
            picturesStatusLabel->Text = (number + " Pictures");
            picturesStatusLabel->Visible = (number > 0);
        }
    }
    catch (Exception^ ex)
    {
        DucoUILog::PrintError(ex->ToString());
    }
}

System::Void
Form1::SetNumberOfAssociationsLabel(size_t number)
{
    try
    {
        if (number == 1)
        {
            associationsStatusLabel->Text = ("1 Association");
        }
        else
        {
            associationsStatusLabel->Text = (number + " Associations");
        }
    }
    catch (Exception^ ex)
    {
        DucoUILog::PrintError(ex->ToString());
    }
}

System::Void
Form1::SetNumberOfMethodsLabel(size_t number)
{
    try
    {
        if (number == 1)
        {
            methodsStatusLabel->Text = ("1 Method");
        }
        else
        {
            methodsStatusLabel->Text = (number + " Methods");
        }
    }
    catch (Exception^ ex)
    {
        DucoUILog::PrintError(ex->ToString());
    }
}

System::Void
Form1::SetNumberOfRingersLabel(size_t number)
{
    try
    {
        if (number == 1)
            ringersStatusLabel->Text = ("1 Ringer");
        else
            ringersStatusLabel->Text = (number + " Ringers");
    }
    catch (Exception^ ex)
    {
        DucoUILog::PrintError(ex->ToString());
    }
}

System::Void 
Form1::OpenSettings()
{
    SettingsUI^ settingsUI = gcnew SettingsUI(database);
    settingsUI->MdiParent = this;
    settingsUI->Show();
}

System::Void 
Form1::OpenRinger(DucoWindowState state)
{
    if (database->NumberOfObjects(TObjectType::ERinger) > 0 || state == DucoWindowState::ENewMode)
    {
        RingerUI^ ringerUI = gcnew RingerUI(database, state);
        ringerUI->MdiParent = this;
        ringerUI->Show();
    }
    else
        SoundUtils::PlayErrorSound();
}

System::Void 
Form1::OpenMethod(DucoWindowState state)
{
    if (database->NumberOfObjects(TObjectType::EMethod) > 0 || state == DucoWindowState::ENewMode)
    {
        MethodUI^ methodUI = gcnew MethodUI(database, state);
        methodUI->MdiParent = this;
        methodUI->Show();
    }
    else
        SoundUtils::PlayErrorSound();
}

System::Void
Form1::OpenTower(DucoWindowState state)
{
    if (database->NumberOfObjects(TObjectType::ETower) > 0 || state == DucoWindowState::ENewMode)
    {
        TowerUI^ towerUI = gcnew TowerUI(database, state);
        towerUI->MdiParent = this;
        towerUI->Show();
    }
    else
        SoundUtils::PlayErrorSound();
}

System::Void 
Form1::OpenPeal(DucoWindowState state)
{
    if (database->NumberOfObjects(TObjectType::EPeal) > 0 || state == DucoUI::DucoWindowState::ENewMode)
    {
        PealUI^ pealUI = gcnew PealUI(database,  state);
        pealUI->MdiParent = this;
        pealUI->Show();
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
Form1::defaultRingerConductedStatsToolStripMenuItem1_Click(System::Object^  sender, System::EventArgs^  e)
{
    ConductedStatsUI^ statsUI = gcnew ConductedStatsUI(database);
    statsUI->MdiParent = this;
    statsUI->Show();
}

System::Void 
Form1::towerStatsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    TowerStatsUI^ statsUI = gcnew TowerStatsUI(database);
    statsUI->MdiParent = this;
    statsUI->Show();
}

System::Void
Form1::preferencesToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    OpenSettings();
}

System::Void 
Form1::aboutToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    AboutUI^ aboutUI = gcnew AboutUI(database);
    aboutUI->MdiParent = this;
    aboutUI->Show();
}

System::Void
Form1::resetToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (database != nullptr && CheckNoChildForms(false, ""))
    {
        database->Reset();
    }
}

System::Void
Form1::checkForUpdatesToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e)
{
    database->Config().EnableNextVersion();
    latestVersionChecker = gcnew CheckLatestVersion(this, database, true);
}

System::Void
Form1::closeAllToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    CheckNoChildForms(false, "");
}

System::Void 
Form1::validateAllPealsCmd_Click(System::Object^  sender, System::EventArgs^  e)
{
    PealSearchUI^ uiForm = gcnew PealSearchUI(database, true);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void
Form1::validateAllTowersCmd_Click(System::Object^  sender, System::EventArgs^  e)
{
    TowerSearchUI^ validateUI = gcnew TowerSearchUI(database, true);
    validateUI->MdiParent = this;
    validateUI->Show();
}

System::Void
Form1::validateAllMethodsCmd_Click(System::Object^  sender, System::EventArgs^  e)
{
    MethodSearchUI^ validateUI = gcnew MethodSearchUI(database, true);
    validateUI->MdiParent = this;
    validateUI->Show();
}

System::Void
Form1::validateAllSeriesCmd_Click(System::Object^  sender, System::EventArgs^  e)
{
    MethodSeriesSearch^ validateUI = gcnew MethodSeriesSearch(database, true);
    validateUI->MdiParent = this;
    validateUI->Show();
}

System::Void
Form1::validateAllCompositionsCmd_Click(System::Object^  sender, System::EventArgs^  e)
{
    CompositionSearchUI^ validateUI = gcnew CompositionSearchUI(database, true);
    validateUI->MdiParent = this;
    validateUI->Show();
}

System::Void
Form1::validateAllRingersCmd_Click(System::Object^  sender, System::EventArgs^  e)
{
    RingerSearchUI^ validateUI = gcnew RingerSearchUI(database, true);
    validateUI->MdiParent = this;
    validateUI->Show();
}

System::Void 
Form1::exitToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void
Form1::mergeToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    MergeUI^ formUI = gcnew MergeUI(database); 
    formUI->MdiParent = this;
    formUI->Show();
}

System::Void
Form1::importToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (!CheckNoChildForms(true, "the import"))
        return;
    if (database->DataChanged())
    {
        if (!CheckUnsavedChanges(false))
            return;
    }
    ImportFileUI^ formUI = gcnew ImportFileUI(database, ""); 
    formUI->MdiParent = this;
    formUI->Show();
}

System::Void
Form1::newToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (!CheckNoChildForms(true, "creating a new database"))
        return;
    if (database->DataChanged())
    {
        if (!CheckUnsavedChanges(false))
            return;
    }
    if (database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
        return;
    }

    Boolean pealDatabase = true;
    switch (MessageBox::Show(this, "Will the new database contain full peals?", "Database type", MessageBoxButtons::YesNoCancel, MessageBoxIcon::Question, MessageBoxDefaultButton::Button1))
    {
        case ::DialogResult::Cancel:
            break;
        
        case ::DialogResult::No:
            pealDatabase = false;
        case ::DialogResult::Yes:
            database->CreateDatabase(pealDatabase);
            break;
    }
}

System::Void
Form1::clearInvalidTenorKeysToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (!database->DatabaseOpen() && !database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
        return;
    }
    switch (MessageBox::Show(this, "Warning, this change cannot be undone and will clear all tenor keys with invalid characters", "Clear invalid tenor keys", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning, MessageBoxDefaultButton::Button1))
    {
    case ::DialogResult::Cancel:
        break;

    case ::DialogResult::OK:
        database->ClearInvalidTenorKeys(progressWrapper);
        SoundUtils::PlayFinishedSound();
        break;
    }
}

System::Void
Form1::correctTowerTypesToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (!database->DatabaseOpen() && !database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
        return;
    }
    database->SetTowersWithHandbellPealsAsHandbells();
}


System::Void 
Form1::openToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (!CheckNoChildForms(true, "opening a new file"))
        return;
    if (database->DataChanged())
    {
        if (!CheckUnsavedChanges(false))
            return;
    }
    OpenFileDialog^ openFileDialog = gcnew OpenFileDialog();
    openFileDialog->Filter = "Duco files (*.duc)|*.duc";
    openFileDialog->RestoreDirectory = true;
    try
    {

        if (database->Filename()->Length > 0)
        {
            openFileDialog->InitialDirectory = System::IO::Path::GetDirectoryName(database->Filename());
        }
    }
    catch (Exception^ e)
    {
        DucoUILog::PrintError("Form1::openToolStripMenuItem_Click InitialDirectory");
        DucoUILog::PrintError(e->ToString());
    }

    if (openFileDialog->ShowDialog() == System::Windows::Forms::DialogResult::OK)
    {
        try
        {
            String^ fileName = openFileDialog->FileName;
            openFileDialog->OpenFile()->Close();
            OpenNewDatabase(fileName);
        }
        catch (Exception^ ex)
        {
            SoundUtils::PlayErrorSound();
            MessageBox::Show(this, "Error: Could not open file: " + ex->Message);
            DucoUILog::PrintError("Form1::openToolStripMenuItem_Click");
            DucoUILog::PrintError(ex->ToString());
        }
    }
}

System::Void 
Form1::saveToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    Boolean savedAsXml(false);
    Boolean savedAsCsv(false);
    if (SaveDatabase(false, savedAsXml, savedAsCsv))
    {
        if (!savedAsXml && !savedAsCsv)
            SetFileNameDisplay(database->Filename());
    }
}

System::Void
Form1::saveAsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    Boolean savedAsXml(false);
    Boolean savedAsCsv(false);
    if (SaveDatabase(true, savedAsXml, savedAsCsv))
    {
        if (!savedAsXml && !savedAsCsv)
            SetFileNameDisplay(database->Filename());
    }
}

System::Void
Form1::onlineToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e)
{
    System::Diagnostics::Process::Start(KDucoOnline);
}

System::Void 
Form1::importMethodToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{ 
    if (database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
    }
    else
    {
        Duco::ObjectId methodId;
        ImportSingleMethod^ formUI = gcnew ImportSingleMethod(database, methodId, -1, "", false); 
        formUI->MdiParent = this;
        formUI->Show();
    }
}

System::Void 
Form1::importNotationToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{ 
    if (database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
    }
    else
    {
        ImportNotationUI^ formUI = gcnew ImportNotationUI(database); 
        formUI->MdiParent = this;
        formUI->Show();
    }
}

System::Void 
Form1::newRingerToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    OpenRinger(DucoWindowState::ENewMode);
}

System::Void 
Form1::defaultRingerToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    RingerUI::ShowRinger(database, this, database->Settings().DefaultRinger());
}

System::Void 
Form1::ringersToolStripMenuItem_DropDownOpening(System::Object^  sender, System::EventArgs^  e)
{
    defaultRingerToolStripMenuItem->Visible = database->DatabaseOpen() && database->Settings().DefaultRingerSet();
}

System::Void 
Form1::showRingerToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    OpenRinger(DucoWindowState::EViewMode);
}

System::Void 
Form1::newMethodToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    OpenMethod(DucoWindowState::ENewMode);
}

System::Void 
Form1::showMethodToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    OpenMethod(DucoWindowState::EViewMode);
}

System::Void 
Form1::newTowerToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    OpenTower(DucoWindowState::ENewMode);
}

System::Void 
Form1::showTowerToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    OpenTower(DucoWindowState::EViewMode);
}

System::Void 
Form1::showPealToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    OpenPeal(DucoWindowState::EViewMode);
}

System::Void
Form1::newAssociationToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    AssociationsUI^ uiForm = gcnew AssociationsUI(database, DucoWindowState::ENewMode);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void
Form1::viewAssociationsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->NumberOfObjects(TObjectType::ETower) > 0)
    {
        AssociationsUI^ uiForm = gcnew AssociationsUI(database, DucoWindowState::EViewMode);
        uiForm->MdiParent = this;
        uiForm->Show();
    }
}

System::Void
Form1::associationSearchToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e)
{
    AssociationSearchUI^ uiForm = gcnew AssociationSearchUI(database);
    uiForm->MdiParent = this;
    uiForm->Show();
}   

System::Void
Form1::showAllPealsMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
        return;
    }
    PealTableUI^ formUI = gcnew PealTableUI(database);
    formUI->MdiParent = this;
    formUI->Show();
}

System::Void 
Form1::newPealToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    OpenPeal(DucoWindowState::ENewMode);
}

System::Void 
Form1::downloadToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
        return;
    }
    DownloadPealUI^ formUI = gcnew DownloadPealUI(database); 
    formUI->MdiParent = this;
    formUI->Show();
}

System::Void
Form1::downloadByIdToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
        return;
    }
    BellboardIdEntry^ formUI = gcnew BellboardIdEntry(database);
    formUI->MdiParent = this;
    formUI->Show();
}

System::Void 
Form1::peopleStatsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    PeopleStatsUI^ statsUI = gcnew PeopleStatsUI(database);
    statsUI->MdiParent = this;
    statsUI->Show();
}

System::Void 
Form1::methodStatsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    MethodStatsUI^ formUI = gcnew MethodStatsUI(database); 
    formUI->MdiParent = this;
    formUI->Show();
}

System::Void 
Form1::genderStatsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    GenderStatsUI^ formUI = gcnew GenderStatsUI(database);
    formUI->MdiParent = this;
    formUI->Show();
}

System::Void 
Form1::associationStatsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    AssociationStatsUI^ formUI = gcnew AssociationStatsUI(database); 
    formUI->MdiParent = this;
    formUI->Show();
}

System::Void 
Form1::pealLengthToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    PealLengthUI^ formUI = gcnew PealLengthUI(database); 
    formUI->MdiParent = this;
    formUI->Show();
}

System::Void 
Form1::towerCirclingToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    TowersCircledUI^ statsUI = gcnew TowersCircledUI(database);
    statsUI->MdiParent = this;
    statsUI->Show();
}

System::Void 
Form1::methodCirclingToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    MethodsCircledUI^ statsUI = gcnew MethodsCircledUI(database);
    statsUI->MdiParent = this;
    statsUI->Show();
}

System::Void 
Form1::yearStatsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    YearCirclingUI^ yearUI = gcnew YearCirclingUI(database);
    yearUI->MdiParent = this;
    yearUI->Show();
}

System::Void 
Form1::graphsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    GraphsUI^ graphUI = gcnew GraphsUI(database);
    graphUI->MdiParent = this;
    graphUI->Show();
}

System::Void 
Form1::PealFeesToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    UnpaidPealFeesUI^ pealFeesUI = gcnew UnpaidPealFeesUI(database);
    pealFeesUI->MdiParent = this;
    pealFeesUI->Show();
}

System::Void 
Form1::BellsRungToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    BellsRungUI^ bellsRungUI = gcnew BellsRungUI(database);
    bellsRungUI->MdiParent = this;
    bellsRungUI->Show();
}

System::Void
Form1::methodSeriesToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    CompletedSeriesUI^ uiForm = gcnew CompletedSeriesUI(database);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void
Form1::viewCompositionToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    CompositionUI^ uiForm = gcnew CompositionUI(database);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void 
Form1::milestonesToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    MilestoneUI^ uiForm = gcnew MilestoneUI(database);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void
Form1::monthsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    MonthsUI^ uiForm = gcnew MonthsUI(database);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void
Form1::replaceRingerToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
        return;
    }
    ReplaceRingerUI^ uiForm = gcnew ReplaceRingerUI(database);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void
Form1::replaceTowerToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
        return;
    }
    ReplaceTowerUI^ uiForm = gcnew ReplaceTowerUI(database);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void
Form1::faqToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    String^ faqsLocation = gcnew String("http://" + KWebSite + "/faqs.html");
    System::Diagnostics::Process::Start( faqsLocation );
}

System::Void
Form1::replaceAssociationMenu_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
        return;
    }
    ReplaceAssociationUI^ uiForm = gcnew ReplaceAssociationUI(database);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void
Form1::duplicateAssociationMenu_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (!database->DatabaseOpen() && !database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
    }
    else if (database->RemoveDuplicateAssociations())
    {
        CheckRebuildRecommended(false);
        SoundUtils::PlayFinishedSound();
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
Form1::uppercaseAssociationsToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (!database->DatabaseOpen() && !database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
    }
    else if (database->UppercaseAssociations(progressWrapper))
    {
        SoundUtils::PlayFinishedSound();
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
Form1::uppercaseTownsToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (!database->DatabaseOpen() && !database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
    }
    else if (database->UppercaseCities(progressWrapper))
    {
        SoundUtils::PlayFinishedSound();
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
Form1::capitaliseTowersToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (!database->DatabaseOpen() && !database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
    }
    else if (database->CapitaliseFields(progressWrapper, false, false, true, true, false))
    {
        SoundUtils::PlayFinishedSound();
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
Form1::capitaliseRingerNamesToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (!database->DatabaseOpen() && !database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
    }
    else if (database->CapitaliseFields(progressWrapper, false, false, false, false, true))
    {
        SoundUtils::PlayFinishedSound();
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
Form1::capitaliseMethodsToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (!database->DatabaseOpen() && !database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
    }
    else if (database->CapitaliseFields(progressWrapper, true, true, false, false, false))
    {
        SoundUtils::PlayFinishedSound();
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}


System::Void
Form1::removeDuplicateRingsToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (!database->DatabaseOpen() && !database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
    }
    else if (database->RemoveDuplicateRings(progressWrapper))
    {
        SoundUtils::PlayFinishedSound();
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
Form1::replaceShortMethodTypesToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (!database->DatabaseOpen() && !database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
    }
    else if (database->ReplaceMethodTypes(progressWrapper))
    {
        SoundUtils::PlayFinishedSound();
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
Form1::removeDuplicateMethodsToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (!database->DatabaseOpen() && !database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
    }
    else if (database->RemoveDuplicateMethods(progressWrapper))
    {
        CheckRebuildRecommended(false);
        SoundUtils::PlayFinishedSound();
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
Form1::updateIdsAndRWRefs_Click(System::Object^ sender, System::EventArgs^ e)
{
    BellboardUpdate^ formUI = gcnew BellboardUpdate(database);
    formUI->MdiParent = this;
    formUI->Show();
}

System::Void
Form1::numberOfChangesMenu_Click(System::Object^  sender, System::EventArgs^  e)
{
    ChangesCirclingUI^ uiForm = gcnew ChangesCirclingUI(database);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void
Form1::fastestSlowestPealsCmd_Click(System::Object^  sender, System::EventArgs^  e)
{
    PealSpeedUI^ uiForm = gcnew PealSpeedUI(database);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void
Form1::alphabetsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    AlphabetsUI^ uiForm = gcnew AlphabetsUI(database);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void
Form1::viewSeriesToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    MethodSeriesUI^ uiForm = gcnew MethodSeriesUI(database);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void
Form1::searchSeriesToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    MethodSeriesSearch^ uiForm = gcnew MethodSeriesSearch(database, false);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void
Form1::searchCompositionToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    CompositionSearchUI^ uiForm = gcnew CompositionSearchUI(database, false);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void
Form1::editStagesToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
        return;
    }
    StagesUI^ uiForm = gcnew StagesUI(database);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void 
Form1::renumberToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    CheckRebuildRecommended(true);
}

System::Void 
Form1::picturesToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
        return;
    }
    PictureManagerUI^ uiForm = gcnew PictureManagerUI(database);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void 
Form1::yearOrderingToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
        return;
    }
    DayOrderUI^ uiForm = gcnew DayOrderUI(database);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void 
Form1::pealMapStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
        return;
    }
    PealMapUI^ uiForm = gcnew PealMapUI(database);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void 
Form1::updateFromDoveToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
        return;
    }
    ImportDoveIdsUI^ uiForm = gcnew ImportDoveIdsUI(database);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void
Form1::updateTowerLocationsToolBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
        return;
    }
    ImportDoveLocationUI^ uiForm = gcnew ImportDoveLocationUI(database);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void
Form1::importTenorKeysToolBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
    if (database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
        return;
    }
    ImportDoveTowerTenorUI^ uiForm = gcnew ImportDoveTowerTenorUI(database);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void 
Form1::importFromDoveToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->InternaliseExternaliseInProgress())
    {
        SoundUtils::PlayErrorSound();
        return;
    }
    SearchDoveUI^ uiForm = gcnew SearchDoveUI(database);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void 
Form1::deleteUnusedToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->InternaliseExternaliseInProgress() || !CheckNoChildForms(true, "deletes"))
    {
        SoundUtils::PlayErrorSound();
        return;
    }
    Boolean changesMade (false);
    DeleteUnusedUI^ uiForm = gcnew DeleteUnusedUI(database, changesMade);
    uiForm->ShowDialog();
    if (changesMade)
    {
        CheckRebuildRecommended(false);
    }
}

System::Void 
Form1::pealSearchToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    PealSearchUI^ uiForm = gcnew PealSearchUI(database, false);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void
Form1::onThisDayToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    OnThisDay^ onThisDayUI = gcnew OnThisDay(database);
    onThisDayUI->MdiParent = this;
    onThisDayUI->Show();
}

System::Void 
Form1::towerSearchToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    TowerSearchUI^ uiForm = gcnew TowerSearchUI(database, false);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void 
Form1::ringerSearchToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    RingerSearchUI^ uiForm = gcnew RingerSearchUI(database, false);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void 
Form1::methodSearchToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    MethodSearchUI^ uiForm = gcnew MethodSearchUI(database, false);
    uiForm->MdiParent = this;
    uiForm->Show();
}

System::Void 
Form1::Form1_DragOver(System::Object^  sender, System::Windows::Forms::DragEventArgs^  e)
{
    if ( e->Data->GetDataPresent( DataFormats::Text ) || e->Data->GetDataPresent( DataFormats::FileDrop ) )
    {
        if (e->Data->GetDataPresent("FileName"))
        {
            array<System::String ^>^ filenameObjects = static_cast<array<System::String ^>^>(e->Data->GetData("FileName"));
            if (filenameObjects->Length == 1)
            {
                String^ fileName = static_cast<System::String^>(filenameObjects->GetValue(0));
                fileName = fileName->Trim()->ToLower();
                if (fileName->EndsWith(".duc") || fileName->EndsWith(".v30") || fileName->EndsWith(".mdb") ||
                    fileName->EndsWith(".csv") || fileName->EndsWith(".pbr") || fileName->EndsWith(".tsv"))
                    e->Effect = DragDropEffects::Copy;
            }
        }
    }
}

System::Void 
Form1::Form1_DragDrop(System::Object^  sender, System::Windows::Forms::DragEventArgs^  e)
{
    if (e->Data->GetDataPresent("FileName"))
    {
        array<System::String ^>^ filenameObjects = static_cast<array<System::String ^>^>(e->Data->GetData("FileName"));
        if (filenameObjects->Length == 1)
        {
            String^ fileName = static_cast<System::String^>(filenameObjects->GetValue(0));
            fileName = Path::GetFullPath(fileName);
            fileName = fileName->Trim()->ToLower();

            if (fileName->EndsWith(".duc") && File::Exists(fileName))
            {
                if (!CheckNoChildForms(true, "opening a new file"))
                    return;
                if (database->DataChanged())
                {
                    if (!CheckUnsavedChanges(false))
                        return;
                }
                OpenNewDatabase(fileName);
            }
            else if ((fileName->EndsWith(".pbr") || fileName->EndsWith(".v30") || fileName->EndsWith(".mdb") ||
                      fileName->EndsWith(".csv") || fileName->EndsWith(".pbr") || fileName->EndsWith(".tsv"))
                      && File::Exists(fileName))
            {
                if (!CheckNoChildForms(true, "opening a new file"))
                    return;
                if (database->DataChanged())
                {
                    if (!CheckUnsavedChanges(false))
                        return;
                }
            ImportFileUI^ formUI = gcnew ImportFileUI(database, fileName); 
            formUI->MdiParent = this;
            formUI->Show();
            }
        }
    }
}


System::Void 
Form1::Form1_Load(System::Object^  sender, System::EventArgs^  e)
{
    this->Closing += gcnew System::ComponentModel::CancelEventHandler(this, &Form1::ClosingEvent);
    this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &Form1::HandleFormClosing);
    database = gcnew DatabaseManager(this, this);
    this->Icon = SystemDefaultIcon::DefaultSystemIcon(database->WindowsSettings());

    SetState();
    database->WindowsSettings()->GetHistory(historyToolStripMenuItem, fileHistory1, fileHistory2, fileHistory3, fileHistory4, fileHistory5);
}

System::Void
Form1::Form1_Activated(System::Object^ sender, System::EventArgs^ e)
{
}

System::Void
Form1::Form1_Shown(System::Object ^ sender, System::EventArgs ^ e)
{
    if (firstOpened)
    {
        Boolean fileFromCommandLineParametersOpened = false;
        if (args->GetLength(0) > 0)
        {
            if (File::Exists(args[0]))
            {
                fileFromCommandLineParametersOpened = OpenNewDatabase(args[0]);
            }
        }

        if (!fileFromCommandLineParametersOpened)
        {
            if (database != nullptr)
            {
                System::String^ lastFileName = database->WindowsSettings()->HistoryItem(1);
                if (lastFileName->Length > 0 && System::IO::File::Exists(lastFileName))
                {
                    OpenNewDatabase(lastFileName);
                }
            }
        }
        latestVersionChecker = gcnew CheckLatestVersion(this, database, false);
    }
    firstOpened = false;
}

System::Void 
Form1::ClosingEvent( Object^ /*sender*/, System::ComponentModel::CancelEventArgs^ e )
{
    if (!CheckNoChildForms(true, "closing Duco"))
    {
        e->Cancel = true;
    }
    else if (database != nullptr)
    {
        if (database->InternaliseExternaliseInProgress())
        {
            e->Cancel = true;
        }
        else if ( database != nullptr && database->DataChanged() )
        {
            savingOnExit = false;
            if (!CheckUnsavedChanges(true) || savingOnExit)
            {
                e->Cancel = true;
            }
        }
    }
}

System::Void 
Form1::HandleFormClosing(Object^ /*sender*/, System::Windows::Forms::FormClosingEventArgs^ e)
{
    if (!e->Cancel)
    {
        database->WindowsSettings()->SaveSettings();
    }
}

System::Void 
Form1::fileToolStripMenuItem_DropDownOpening(System::Object^  sender, System::EventArgs^  e)
{
    saveToolStripMenuItem->Enabled = database->DataChanged();
}

System::Void 
Form1::validateMenuItem_DropDownOpening(System::Object^  sender, System::EventArgs^  e)
{
    validateAllIncludeWarnings->Checked = database->Config().IncludeWarningsInValidation();
    validateAllIncludeWarnings->Visible = !database->Config().DisableFeaturesForPerformance();
    validateSeperator->Visible = !database->Config().DisableFeaturesForPerformance();
    validateAllSeriesCmd->Visible = database->NumberOfObjects(TObjectType::EMethodSeries) > 0;
    validateAllCompositionsCmd->Visible = database->NumberOfObjects(TObjectType::EComposition) > 0;
}

System::Void
Form1::validateAllIncludeWarnings_Click(System::Object^  sender, System::EventArgs^  e)
{
    database->Config().SetIncludeWarningsInValidation(validateAllIncludeWarnings->Checked);
}

Boolean
Form1::CheckNoChildForms(Boolean showDialog, String^ str)
{
    array<Form^>^ mdiChildren = MdiChildren;
    int noOfChildForms = mdiChildren->Length;

    for (int x=0; x < mdiChildren->Length; ++x)
    {
        Form^ tempChild = dynamic_cast<Form^>(mdiChildren[x]);
        tempChild->Close();
    }

    noOfChildForms = MdiChildren->Length;
    if (noOfChildForms > 0)
    {
        if (showDialog && str->Length > 0)
        {
            MessageBox::Show(this, "You must close all open dialogs before " + str + " can continue", "Error - Open dialogs", MessageBoxButtons::OK, MessageBoxIcon::Warning);
        }
        return false;
    }
    return true;
}

System::Void
Form1::SetFileNameDisplay(String^ newFileName)
{
    try
    {
        fileNameLbl->Text = DucoUtils::FormatFilename(newFileName);
        fileNameLbl->ToolTipText = newFileName;
    }
    catch (Exception^ ex)
    {
        DucoUILog::PrintError(ex->ToString());
    }
}

Boolean
Form1::CheckUnsavedChanges(Boolean tryingToClose)
{
    if (database->DataChanged())
    {
        String^ dbFilename = database->Filename();
        if (dbFilename->Length == 0)
        {
            dbFilename = "the new database";
        }
        SoundUtils::PlayErrorSound();
        switch (MessageBox::Show(this, "Do you want to save changes to " + dbFilename + "?", "Duco - Unsaved database changes", MessageBoxButtons::YesNoCancel, MessageBoxIcon::Warning ))
        {
        case ::DialogResult::Cancel:
            savingOnExit = false;
            return false;

        case ::DialogResult::Yes:
            {
            bool savedAsXml(false);
            bool savedAsCsv(false);
            SaveDatabase(false, savedAsXml, savedAsCsv);
            if (tryingToClose)
                savingOnExit = true;
            }
            // no break here on purpose

        case ::DialogResult::No:
            break;
        }
    }
    return true;
}

Boolean
Form1::SaveDatabase(Boolean saveAs, Boolean& savedAsXml, Boolean& savedAsCsv)
{
    savedAsXml = false;
    savedAsCsv = false;
    if (saveAs || database->Filename() == nullptr || database->Filename()->Length == 0)
    {
        SaveFileDialog^ saveFileDialog = gcnew SaveFileDialog();
        saveFileDialog->Filter = "Duco files (*.duc)|*.duc|Export to csv(*.csv)|*.csv|Export to xml(*.xml)|*.xml";
        saveFileDialog->RestoreDirectory = true;

        switch (saveFileDialog->ShowDialog())
        {
        case System::Windows::Forms::DialogResult::OK:
            {
                try
                {
                    String^ fileName = saveFileDialog->FileName;
                    saveFileDialog->OpenFile()->Close();
                    Boolean validFilename;
                    if (database->Externalise(fileName, importExportProgressWrapper, validFilename))
                    {
                        if (fileName->EndsWith(".xml"))
                        {
                            savedAsXml = true;
                        }
                        else if (fileName->EndsWith(".csv"))
                        {
                            savedAsCsv = true;
                        }
                        else
                        {
                            database->WindowsSettings()->AddToHistory(fileName, historyToolStripMenuItem, fileHistory1, fileHistory2, fileHistory3, fileHistory4, fileHistory5);
                        }
                        return true;
                    }
                    else if (!validFilename)
                    {
                        MessageBox::Show(this, "Cannot save to file", "Error", MessageBoxButtons::OK, MessageBoxIcon::Error);
                    }
                    else
                    {
                        SoundUtils::PlayErrorSound();
                    }
                }
                catch (Exception^ ex)
                {
                    SoundUtils::PlayErrorSound();
                    DucoUILog::PrintError("Form1::SaveDatabase");
                    DucoUILog::PrintError(ex->ToString());
                    MessageBox::Show(this, "Error: Could not open file: " + ex->Message);
                }
            }
        case System::Windows::Forms::DialogResult::Cancel:
            savingOnExit = false;
            break;
        default:
            break;
        }
    }
    else
    {
        Boolean validFileName;
        if (!database->Externalise(importExportProgressWrapper, validFileName))
        {
           if (!validFileName)
            {
                MessageBox::Show(this, "Directory doesn't exist", "Error cannot save file", MessageBoxButtons::OK, MessageBoxIcon::Error);
            }
            else
            {
                SoundUtils::PlayErrorSound();
            }
        }
    }
    return false;
}

System::Void 
Form1::fileHistory1_Click(System::Object^  sender, System::EventArgs^  e)
{
    OpenHistoryItem(1);
}

System::Void
Form1::fileHistory2_Click(System::Object^  sender, System::EventArgs^  e)
{
    OpenHistoryItem(2);
}

System::Void 
Form1::fileHistory3_Click(System::Object^  sender, System::EventArgs^  e)
{
    OpenHistoryItem(3);
}

System::Void 
Form1::fileHistory4_Click(System::Object^  sender, System::EventArgs^  e)
{
    OpenHistoryItem(4);
}

System::Void 
Form1::fileHistory5_Click(System::Object^  sender, System::EventArgs^  e)
{
    OpenHistoryItem(5);
}

System::Void
Form1::OpenHistoryItem(unsigned int historyItemNo)
{
    if (!CheckNoChildForms(true, "opening a new file"))
        return;
    if (database->DataChanged())
    {
        if (!CheckUnsavedChanges(false))
            return;
    }
    String^ fileName = database->WindowsSettings()->HistoryItem(historyItemNo);
    String^ dir = Directory::GetDirectoryRoot(fileName);
    Boolean error = false;
    try
    {
        if (!dir->StartsWith("\\"))
        {
            DriveInfo drive(dir->Substring(0, 1));
            if (!drive.IsReady)
            {
                error = true;
            }
        }
    }
    catch (Exception^ ex)
    {
        DucoUILog::PrintError(ex->ToString());
        error = true;
    }
    bool removeFromHistory = false;
    if (error)
    {
        if (MessageBox::Show(this, "Cannot find the drive this file existed on, Do you want to remove it from your file history?", "Error - Drive doesn't exist", MessageBoxButtons::YesNo, MessageBoxIcon::Warning) == ::DialogResult::Yes)
        {
            removeFromHistory = true;
        }
    }
    else if (!File::Exists(fileName))
    {
        if (MessageBox::Show(this, "This file doesnt exist, do you want to remove it from your file history?", "Error - File doesn't exist", MessageBoxButtons::YesNo, MessageBoxIcon::Warning) == ::DialogResult::Yes)
        {
            removeFromHistory = true;
        }
    }
    else
    {
        OpenNewDatabase(fileName);
    }
    if (removeFromHistory)
    {
        if (database->WindowsSettings()->RemoveHistoryItem(historyItemNo))
        {
            database->WindowsSettings()->GetHistory(historyToolStripMenuItem, fileHistory1, fileHistory2, fileHistory3, fileHistory4, fileHistory5);
        }
    }
}

Boolean
Form1::OpenNewDatabase(String^% newFileName)
{
    DUCOUILOGDEBUG("Form1::OpenNewDatabase: " + newFileName);
    if (database->InternaliseExternaliseInProgress())
    {
        fileToOpenWhenSaveComplete = newFileName;
    }
    else if (database->Internalise(importExportProgressWrapper, newFileName))
    {
        database->WindowsSettings()->AddToHistory(newFileName, historyToolStripMenuItem, fileHistory1, fileHistory2, fileHistory3, fileHistory4, fileHistory5);
        SetFileNameDisplay(newFileName);
        SetState();
        DUCOUILOGDEBUG("Form1::OpenNewDatabase END true");
        return true;
    }
    return false;
    DUCOUILOGDEBUG("Form1::OpenNewDatabase END false");
}

System::Void
Form1::printPealsStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    PrintPealsUI^ printUI = gcnew PrintPealsUI(database);
    printUI->MdiParent = this;
    printUI->Show();
}

System::Void
Form1::databaseSummaryToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e)
{
    DucoUIUtils::Print(this, sender, gcnew System::Drawing::Printing::PrintPageEventHandler(this, &Form1::PrintSummary), "summary", database->Database().Settings().UsePrintPreview(), false);
}

System::Void
Form1::PrintSummary(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args)
{
    PrintDocument^ printDoc = static_cast<PrintDocument^>(sender);

    PrintSummaryUtil printUtil(database, args);
    printUtil.printObject(args, printDoc->PrinterSettings);
}

System::Void
Form1::associationReportToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e)
{
    AssociationReportOptions^ reportOptions = gcnew AssociationReportOptions(database);
    reportOptions->MdiParent = this;
    reportOptions->Show();
}

System::Void
Form1::Form1_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e)
{
    if (MdiChildren->Length > 0 || e->Handled)
        return;

    switch (e->KeyChar)
    {
        case (wchar_t)System::Windows::Forms::Keys::S:
        {
            if (Control::ModifierKeys == Keys::Control)
            {
                if (CheckUnsavedChanges(true))
                    SetFileNameDisplay(database->Filename());
                e->Handled = true;
            }
        }
        break;
        case (wchar_t)System::Windows::Forms::Keys::X:
        {
            if (Control::ModifierKeys == Keys::Shift)
            {
                Close();
                e->Handled = true;
            }
        }
        break;

        default:
            break;
    }
}

public ref class OpenTowerEventArgs : public System::EventArgs
{
public:
    property UInt64 towerId;
};

void
Form1::OpenTower(System::Object^ sender, System::EventArgs^ e)
{
    OpenTowerEventArgs^ towerArgs = static_cast<OpenTowerEventArgs^>(static_cast<System::Windows::Forms::ToolStripMenuItem^>(sender)->Tag);
    TowerUI::ShowTower(database, this, towerArgs->towerId, -1);
}

System::Void
Form1::towersToolStripMenuItem_DropDownOpening(System::Object^ sender, System::EventArgs^ e)
{
    topTowersToolStripMenuItem->DropDownItems->Clear();
    if (database != nullptr && database->DatabaseOpen() && !database->InternaliseExternaliseInProgress())
    {
        std::list<Duco::ObjectId> towerIds;
        database->Database().PealsDatabase().GetTopTowers(5, towerIds, database->Database());

        std::list<Duco::ObjectId>::const_reverse_iterator it = towerIds.rbegin();
        while (it != towerIds.rend())
        {
            System::Windows::Forms::ToolStripMenuItem^ newItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            newItem->Name = L"newTowerItem";
            newItem->Size = System::Drawing::Size(235, 22);
            newItem->Text = database->TowerName(*it);
            OpenTowerEventArgs^ towerArgs = gcnew OpenTowerEventArgs();
            towerArgs->towerId = it->Id();
            newItem->Tag = towerArgs;
            newItem->Click += gcnew System::EventHandler(this, &Form1::OpenTower);
            topTowersToolStripMenuItem->DropDownItems->Add(newItem);
            ++it;
        }
    }
}
