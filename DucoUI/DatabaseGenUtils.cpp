#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"

#include "DatabaseGenUtils.h"

#include "DucoUtils.h"
#include <Ring.h>
#include <Ringer.h>
#include <RingingDatabase.h>
#include <RingerDatabase.h>
#include <DucoConfiguration.h>
#include <Peal.h>
#include <PealDatabase.h>
#include <Method.h>
#include <MethodDatabase.h>
#include <TowerDatabase.h>
#include "Tower.h"
#include <DucoEngineUtils.h>
#include <Composition.h>
#include <CompositionDatabase.h>
#include <MethodSeries.h>
#include <MethodSeriesDatabase.h>
#include <Association.h>
#include <AssociationDatabase.h>
#include <StatisticFilters.h>
#include "DucoCommon.h"
#include <chrono>
#include "DucoUILog.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::Collections::Generic;
using namespace std::chrono;

//*******************************************************************************************************
// Generic functions
//*******************************************************************************************************

Duco::ObjectId
DatabaseGenUtils::FindId(System::Collections::Generic::List<System::Int16>^ allIds, Int32 index)
{
    Duco::ObjectId id;
    try
    {
        if (index != -1)
        {
            Object^ idObj = allIds[index];
            id = Convert::ToInt16(idObj);
        }
    }
    catch (Exception^)
    {
        id = -1;
    }
    return id;
}

std::set<Duco::ObjectId>
DatabaseGenUtils::FindIds(System::Collections::Generic::List<System::Int16>^ allIds, System::Windows::Forms::ListBox::SelectedIndexCollection^ indexes)
{
    std::set<Duco::ObjectId> ids;
    System::Collections::IEnumerator^ it = indexes->GetEnumerator();
    while (it->MoveNext())
    {
        ids.insert(DatabaseGenUtils::FindId(allIds, (Int32)it->Current));
    }

    return ids;
}

System::Collections::Generic::List<System::Int32>^
DatabaseGenUtils::FindIndexes(System::Collections::Generic::List<System::Int16>^ allIds, const Duco::ObjectId& id)
{
    System::Collections::Generic::List<System::Int32>^ indexes = gcnew System::Collections::Generic::List<System::Int32>();
    Int32 lastIndex = 0;
    while (lastIndex != -1)
    {
        lastIndex = allIds->IndexOf(id.Id(), lastIndex);
        if (lastIndex != -1)
        {
            indexes->Add(lastIndex);
            ++lastIndex;
        }
    }
    return indexes;
}

System::Int32
DatabaseGenUtils::FindIndex(System::Collections::Generic::List<System::Int16>^ allIds, const Duco::ObjectId& id)
{
    return allIds->IndexOf(id.Id());
}

System::Int32
DatabaseGenUtils::FindIndex(System::Collections::Generic::List<System::Int16>^ allIds, const Duco::ObjectId& theRingerId, const Duco::PerformanceDate& pealDate, DucoUI::DatabaseManager^ database)
{
    System::Int32 index = DatabaseGenUtils::FindIndex(allIds, theRingerId);
    size_t akaNo;
    if (database->Database().RingersDatabase().RingerAkaRequired(theRingerId, pealDate, akaNo))
    {
        if (FindId(allIds, index + akaNo) == theRingerId)
        {
            index += (int)akaNo;
        }
    }
    return index;
}

//*******************************************************************************************************
// Towers
//*******************************************************************************************************
System::Void
DatabaseGenUtils::GenerateTowerOptions(System::Windows::Forms::ListBox^ control, System::Collections::Generic::List<Int16>^ allTowerNames, DatabaseManager^ database, const Duco::ObjectId& defaultTowerId, bool ignoreRemoved, bool ignoreTowersWithoutLocation, bool addBlank)
{
    time_point<high_resolution_clock> start = high_resolution_clock::now();
    control->BeginUpdate();
    control->Items->Clear();
    control->Text = "";
    allTowerNames->Clear();

    if (addBlank)
    {
        Int16 noTowerId = -1;
        allTowerNames->Add(noTowerId);
        control->Items->Add("");
    }

    Duco::ObjectId currentTowerId;
    bool towerFound(database->FirstTower(currentTowerId, true));
    while (towerFound)
    {
        Tower theTower;
        if (database->FindTower(currentTowerId, theTower, true) && (ignoreRemoved || !theTower.Removed()) && (!ignoreTowersWithoutLocation || theTower.PositionValid()))
        {
            Int16 nextTowerId = currentTowerId.Id();
            String^ towerName = DucoUtils::ConvertString(theTower.FullName());
            if (towerName->Length > 0)
            {
                std::wstring tenorDesc = theTower.HeaviestTenor();
                if (tenorDesc.length() > 0)
                {
                    towerName += " (" + DucoUtils::ConvertString(tenorDesc) + ")";
                }

                allTowerNames->Add(nextTowerId);
                control->Items->Add(towerName);
                if (currentTowerId == defaultTowerId)
                {
                    control->Text = towerName;
                }
            }
        }
        towerFound = database->NextTower(currentTowerId, true, true);
    }
    control->EndUpdate();
    DUCOUILOGDEBUGWITHTIME(start, "Generate tower options took");
}

System::Void 
DatabaseGenUtils::GenerateTowerOptions(System::Windows::Forms::ComboBox^ control, System::Collections::Generic::List<Int16>^ allTowerNames, DatabaseManager^ database, const Duco::ObjectId& defaultTowerId, bool ignoreRemoved, bool ignoreTowersWithoutLocation, bool addBlank)
{
    time_point<high_resolution_clock> start = high_resolution_clock::now();
    if (database->Config().DisableFeaturesForPerformance())
    {
        control->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::None;
    }
    control->BeginUpdate();
    control->Items->Clear();
    control->Text = "";
    allTowerNames->Clear();

    if (addBlank)
    {
        Int16 noTowerId = -1;
        allTowerNames->Add(noTowerId);
        control->Items->Add("");
    }

    Duco::ObjectId currentTowerId;
    bool towerFound (database->FirstTower(currentTowerId, true));
    while (towerFound)
    {
        Tower theTower; 
        if (database->FindTower(currentTowerId, theTower, true) && (!ignoreRemoved || !theTower.Removed()) && (!ignoreTowersWithoutLocation || theTower.PositionValid()) )
        {
            Int16 nextTowerId = currentTowerId.Id();
            String^ towerName = DucoUtils::ConvertString(theTower.FullName());
            if (towerName->Length > 0)
            {
                std::wstring tenorDesc = theTower.HeaviestTenor();
                if (tenorDesc.length() > 0)
                {
                    towerName += " (" + DucoUtils::ConvertString(tenorDesc)  + ")";
                }

                allTowerNames->Add(nextTowerId);
                control->Items->Add(towerName);
                if (currentTowerId == defaultTowerId)
                {
                    control->Text = towerName;
                }
            }
        }
        towerFound = database->NextTower(currentTowerId, true, true);
    }
    control->EndUpdate();
    DUCOUILOGDEBUGWITHTIME(start, "Generate tower options took");
}

bool
DatabaseGenUtils::AddTowerOption(System::Windows::Forms::ComboBox^ control, const Duco::ObjectId& towerId, System::Collections::Generic::List<Int16>^ allTowerNames, DatabaseManager^ database)
{
    Int16 nextTowerId = towerId.Id();
    if (!allTowerNames->Contains(nextTowerId))
    {
        Tower theTower;
        if (database->FindTower(towerId, theTower, true))
        {
            String^ newTowerName = DucoUtils::ConvertString(theTower.FullName());
            std::wstring tenorDesc = theTower.HeaviestTenor();
            if (tenorDesc.length() > 0)
            {
                newTowerName += " (" + DucoUtils::ConvertString(tenorDesc) + ")";
            }
            control->Items->Add(newTowerName);
            allTowerNames->Add(nextTowerId);
            return true;
        }
    }
    return false;
}

bool
DatabaseGenUtils::UpdateTowerOption(System::Windows::Forms::ComboBox^ control, const Duco::ObjectId& towerId, System::Collections::Generic::List<Int16>^ allTowerNames, DatabaseManager^ database)
{
    Duco::Tower theTower;
    if (database->FindTower(towerId, theTower, true))
    {
        Int16 thisTowerId = towerId.Id();
        String^ newTowerName = DucoUtils::ConvertString(theTower.FullName());
        std::wstring tenorDesc = theTower.HeaviestTenor();
        if (tenorDesc.length() > 0)
        {
            newTowerName += " (" + DucoUtils::ConvertString(tenorDesc) + ")";
        }
        int index = allTowerNames->IndexOf(thisTowerId);
        control->Items[index] = newTowerName;
        return true;
    }
    return false;
}

bool
DatabaseGenUtils::RemoveTowerOption(System::Windows::Forms::ComboBox^ control, const Duco::ObjectId& towerId, System::Collections::Generic::List<Int16>^ allTowerNames)
{
    Int16 thisTowerId = towerId.Id();
    if (allTowerNames->Contains(thisTowerId))
    {
        int index = allTowerNames->IndexOf(thisTowerId);
        allTowerNames->Remove(thisTowerId);
        control->Items->RemoveAt(index);
        return true;
    }
    return false;
}

void
DatabaseGenUtils::GetAllPlaceOptions(System::Windows::Forms::ComboBox^% countiesControl, System::Windows::Forms::ComboBox^% citiesControl, System::Windows::Forms::ComboBox^% namesControl, DatabaseManager^% database)
{
    std::set<std::wstring> allCounties;
    std::set<std::wstring> allCities;
    std::set<std::wstring> allNames;
    database->Database().TowersDatabase().GetAllPlaceOptions(allCounties, allCities, allNames);

    AddStringsToControl(database->Config(), countiesControl, allCounties);
    AddStringsToControl(database->Config(), citiesControl, allCities);
    AddStringsToControl(database->Config(), namesControl, allNames);
}

void
DatabaseGenUtils::GetAlCountyOptions(System::Windows::Forms::ComboBox^ countiesControl, DucoUI::DatabaseManager^ database)
{
    std::set<std::wstring> allCounties;
    std::set<std::wstring> allCities;
    std::set<std::wstring> allNames;
    database->Database().TowersDatabase().GetAllPlaceOptions(allCounties, allCities, allNames);
    allCounties.insert(L"");

    AddStringsToControl(database->Config(), countiesControl, allCounties);
}

//*******************************************************************************************************
// Rings
//*******************************************************************************************************

System::Void 
DatabaseGenUtils::GenerateRingOptions(System::Windows::Forms::ComboBox^ ringSelector, System::Windows::Forms::Label^ ringTenorLbl, System::Collections::Generic::List<System::Int16>^ allRingNames, DatabaseManager^ database, const Duco::ObjectId& towerId, const Duco::ObjectId& ringId, unsigned int noOfBells, bool addAll)
{
    time_point<high_resolution_clock> start = high_resolution_clock::now();
    ringSelector->BeginUpdate();
    ringSelector->Items->Clear();
    ringSelector->Text = "";
    allRingNames->Clear();

    Duco::Tower theTower;
    if (database->FindTower(towerId, theTower, true))
    {
        unsigned int defaultIndex = -1;
        String^ defaultRingTenor = "";
        unsigned int defaultIndexBells = 0;
        bool actualRingId = false;

        std::vector<Duco::ObjectId> ringIds;
        theTower.GetRingIds(ringIds);
        for (const auto& it : ringIds)
        {
            const Ring* const theRing = theTower.FindRing(it);
            if (theRing != NULL)
            {
                String^ ringName = DucoUtils::ConvertString(theRing->Name());
                Int16 nextRingId = it.Id();
                allRingNames->Add(nextRingId);
                unsigned int newIndex = ringSelector->Items->Add(ringName);
                if (ringId == theRing->Id())
                {
                    actualRingId = true;
                    defaultIndex = newIndex;
                    defaultRingTenor = DucoUtils::ConvertString(theRing->TenorDescription());
                }
                else if (!actualRingId && noOfBells != -1 && theRing->NoOfBells() == noOfBells)
                {
                    defaultIndex = newIndex;
                    defaultIndexBells = 99;
                    defaultRingTenor = DucoUtils::ConvertString(theRing->TenorDescription());
                }
                else if (!actualRingId && theRing->NoOfBells() > defaultIndexBells && !addAll)
                {
                    defaultIndex = newIndex;
                    defaultIndexBells = theRing->NoOfBells();
                    defaultRingTenor = DucoUtils::ConvertString(theRing->TenorDescription());
                }
            }
        }

        if (ringSelector->Items->Count > 0 && defaultIndex != -1)
            ringSelector->SelectedIndex = defaultIndex;
        if (ringTenorLbl != nullptr)
        {
            ringTenorLbl->Text = defaultRingTenor;
        }
    }
    if (addAll)
    {
        ringSelector->Items->Add("All rings");
        ringSelector->SelectedIndex = ringSelector->Items->Count-1;
    }

    ringSelector->EndUpdate();
    DUCOUILOGDEBUGWITHTIME(start, "Generate ring options took");
}

//*******************************************************************************************************
// Methods
//*******************************************************************************************************
System::Void
DatabaseGenUtils::GenerateMethodOptions(System::Collections::Generic::List<Int16>^ allMethodIds,
                                        System::Windows::Forms::ComboBox^ control, DatabaseManager^ database, 
                                        const Duco::ObjectId& currentMethodId, unsigned int noOfBells, bool withBobsAndSinglesOnly, bool anyStage, bool clearIfNotFound)
{
    time_point<high_resolution_clock> start = high_resolution_clock::now();
    if (database->Config().DisableFeaturesForPerformance())
    {
        control->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::None;
    }
    if (noOfBells <= 0 || noOfBells == -1)
        anyStage = true;

    control->BeginUpdate();
    control->Items->Clear();
    allMethodIds->Clear();

    String^ oldMethodName = control->Text;
    control->Text = "";

    bool foundById (false);
    unsigned int defaultIndex (-1);

    Duco::ObjectId methodId;
    database->Database().MethodsDatabase().FirstObject(methodId, true);
    
    while (methodId.ValidId())
    {
        Method theMethod;
        if (database->FindMethod(methodId, theMethod, true) && (!withBobsAndSinglesOnly || theMethod.ContainsBobAndSingleNotation()) )
        {
            if (anyStage || DucoEngineUtils::NoOfBellsMatch(theMethod.Order(), noOfBells) || theMethod.Id() == currentMethodId)
            {
                Int16 nextMethodId = methodId.Id();
                String^ methodName = DucoUtils::ConvertString(theMethod.FullName(database->Database().MethodsDatabase()));
                allMethodIds->Add(nextMethodId);
                unsigned int newIndex = control->Items->Add(methodName);

                if (currentMethodId == theMethod.Id())
                {
                    defaultIndex = newIndex;
                    foundById = true;
                }
                else if (!foundById && methodName->Length > 0 && String::Compare(methodName, oldMethodName) == 0)
                {
                    defaultIndex = newIndex;
                }
            }
        }
        database->Database().MethodsDatabase().NextObject(methodId,true, true);
    }
    control->EndUpdate();
    if (defaultIndex != -1)
    {
        control->SelectedIndex = defaultIndex;
    }
    else if (oldMethodName->Length > 0)
    {
        if (!clearIfNotFound)
        {
            control->Text = oldMethodName;
            control->BackColor = KErrorColour;
        }
    }
    DUCOUILOGDEBUGWITHTIME(start, "Generate method options took");
}

System::Void
DatabaseGenUtils::GenerateMethodOptions(System::Collections::Generic::List<Int16>^ allMethodIds,
                                        System::Windows::Forms::ComboBox^ control, DatabaseManager^ database, 
                                        bool addBlank)
{
    time_point<high_resolution_clock> start = high_resolution_clock::now();
    if (database->Config().DisableFeaturesForPerformance())
    {
        control->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::None;
    }
    control->BeginUpdate();
    control->Items->Clear();
    allMethodIds->Clear();

    if (addBlank)
    {
        Int16 newMethodId = -1;
        allMethodIds->Add(newMethodId);
        control->Items->Add("");
    }


    std::set<Duco::ObjectId> methodIds;
    database->Database().MethodsDatabase().GetAllMethodsByNumberOfBells(methodIds, -1, true, false);

    for (const auto& methodIt: methodIds)
    {
        Method theMethod;
        if (database->FindMethod(methodIt, theMethod, true))
        {
            Int16 nextMethodId = methodIt.Id();
            String^ methodName = DucoUtils::ConvertString(theMethod.FullName(database->Database().MethodsDatabase()));
            allMethodIds->Add(nextMethodId);
            control->Items->Add(methodName);
        }
    }
    methodIds.clear();
    control->EndUpdate();
    DUCOUILOGDEBUGWITHTIME(start, "Generate method options (2) took");
}

System::Void
DatabaseGenUtils::GenerateMethodOptions(System::Collections::Generic::List<Int16>^ allMethodIds,
                                        System::Windows::Forms::ComboBox^ control, DatabaseManager^ database, 
                                        const Duco::ObjectId& currentMethodId, const Duco::MethodSeries& methSeries,
                                        bool excludeSpliced)
{
    time_point<high_resolution_clock> start = high_resolution_clock::now();
    if (database->Config().DisableFeaturesForPerformance())
    {
        control->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::None;
    }
    control->BeginUpdate();
    control->Items->Clear();
    allMethodIds->Clear();

    std::set<Duco::ObjectId> methodIds;
    database->Database().MethodsDatabase().GetAllMethodsByNumberOfBells(methodIds, methSeries.NoOfBells(), !methSeries.SingleOrder(), excludeSpliced);

    String^ tempOldMethod = control->Text;
    if (currentMethodId.ValidId())
    {
        tempOldMethod = ""; 
    }
    control->Text = "";

    unsigned int defaultIndex = -1;
    for (const auto& methodIt: methodIds)
    {
        if (!methSeries.ContainsMethod(methodIt))
        {
            Method theMethod;
            if (database->FindMethod(methodIt, theMethod, true))
            {
                Int16 nextMethodId = methodIt.Id();
                String^ methodName = DucoUtils::ConvertString(theMethod.FullName(database->Database().MethodsDatabase()));
                allMethodIds->Add(nextMethodId);
                unsigned int newIndex = control->Items->Add(methodName);

                if (currentMethodId == theMethod.Id())
                {
                    defaultIndex = newIndex;
                }
                else if (methodName->Length > 0 && String::Compare(methodName,tempOldMethod) == 0)
                {
                    defaultIndex = newIndex;
                }
            }
        }
    }
    if (defaultIndex != -1)
    {
        control->SelectedIndex = defaultIndex;
    }
    else if (tempOldMethod->Length > 0)
    {
        control->Text = tempOldMethod;
    }
    methodIds.clear();
    control->EndUpdate();
    DUCOUILOGDEBUGWITHTIME(start, "Generate method options (3) took");
}

System::Void 
DatabaseGenUtils::AddMethodOption(const Duco::ObjectId& methodId, System::Collections::Generic::List<Int16>^ allMethodNames,
                        unsigned int noOfBells, DatabaseManager^ database,
                        System::Windows::Forms::ComboBox^ methodEditor)
{
    Int16 thisMethodId = methodId.Id();
    if (!allMethodNames->Contains(thisMethodId))
    {
        Method theMethod;
        if (database->FindMethod(methodId, theMethod, true))
        {
            if (DucoEngineUtils::NoOfBellsMatch(theMethod.Order(), noOfBells))
            {
                String^ newMethodName = DucoUtils::ConvertString(theMethod.FullName(database->Database().MethodsDatabase()));
                methodEditor->Items->Add(newMethodName);
                allMethodNames->Add(thisMethodId);
            }
        }
    }
}

bool
DatabaseGenUtils::UpdateMethodOption(const Duco::ObjectId& methodId, System::Collections::Generic::List<Int16>^ allMethodNames,
                        unsigned int noOfBells, DatabaseManager^ database,
                        System::Windows::Forms::ComboBox^ methodEditor)
{
    Method theMethod;
    if (database->FindMethod(methodId, theMethod, true))
    {
        Int16 thisMethodId = methodId.Id();
        int index = allMethodNames->IndexOf(thisMethodId);
        if (index != -1)
        {
            String^ newMethodName = DucoUtils::ConvertString(theMethod.FullName(database->Database().MethodsDatabase()));
            methodEditor->Items[index] = newMethodName;
            return true;
        }
    }
    return false;
}

bool 
DatabaseGenUtils::RemoveMethodOption(const Duco::ObjectId& methodId, System::Collections::Generic::List<Int16>^ allMethodNames,
                                     unsigned int noOfBells, System::Windows::Forms::ComboBox^ methodEditor)
{
    Int16 thisMethodId = methodId.Id();
    if (allMethodNames->Contains(thisMethodId))
    {
        int index = allMethodNames->IndexOf(thisMethodId);
        allMethodNames->Remove(thisMethodId);
        methodEditor->Items->RemoveAt(index);
        return true;
    }
    return false;
}

//*******************************************************************************************************
// Associations
//*******************************************************************************************************
System::Void
DatabaseGenUtils::GenerateAssociationOptions(System::Windows::Forms::ComboBox^ control, System::Collections::Generic::List<System::Int16>^ allAssociationIds, DucoUI::DatabaseManager^ database, const Duco::ObjectId& currentAssociationId, bool addBlank)
{
    time_point<high_resolution_clock> start = high_resolution_clock::now();
    Duco::StatisticFilters filters(database->Database());
    std::map<Duco::ObjectId, Duco::PealLengthInfo> associations;
    database->Database().PealsDatabase().GetAssociationsPealCount(filters, associations, false);

    Duco::ObjectId objectIdToFind = currentAssociationId;

    control->BeginUpdate();
    control->Items->Clear();
    control->Text = "";
    allAssociationIds->Clear();

    if (addBlank)
    {
        allAssociationIds->Add(KNoId);
        control->Items->Add("");
    }

    Int16 indexOfDefault = -1;
    // Populate association options
    for (const auto& [key, value] : associations)
    {
        Int16 currentIndex = control->Items->Add(database->FindAssociationName(key));
        Int16 nextAssociationId = key.Id();
        allAssociationIds->Add(nextAssociationId);
        if (objectIdToFind.ValidId() && key == objectIdToFind)
        {
            indexOfDefault = currentIndex;
        }
    }
    if (indexOfDefault != -1)
        control->SelectedIndex = indexOfDefault;

    control->EndUpdate();
    DUCOUILOGDEBUGWITHTIME(start, "Generate Association options took");
}

System::Void
DatabaseGenUtils::PopulateAssociationOptions(System::Windows::Forms::ComboBox^ control, System::Collections::Generic::List<System::Int16>^ allAssociationIds, DucoUI::DatabaseManager^ database, const Duco::ObjectId& currentAssociationId)
{
    time_point<high_resolution_clock> start = high_resolution_clock::now();
    control->BeginUpdate();
    control->Items->Clear();
    control->Text = "";

    Int16 indexOfDefault = -1;

    // Populate tower options
    System::Collections::Generic::List<System::Int16>::Enumerator it = allAssociationIds->GetEnumerator();
    while (it.MoveNext())
    {
        Int16 nextAssociationNumber = it.Current;
        Duco::ObjectId nextAssociationId = nextAssociationNumber;
        Int16 currentIndex = control->Items->Add(database->FindAssociationName(nextAssociationId));
        if (currentAssociationId.ValidId() && nextAssociationId == currentAssociationId)
        {
            indexOfDefault = currentIndex;
        }
    }
    if (indexOfDefault != -1)
        control->SelectedIndex = indexOfDefault;

    control->EndUpdate();
    DUCOUILOGDEBUGWITHTIME(start, "Populate Association options took");
}

System::Void
DatabaseGenUtils::GenerateAssociationOptions(System::Windows::Forms::CheckedListBox^ control, System::Collections::Generic::List<System::Int16>^ allAssociationIds, DucoUI::DatabaseManager^ database, const Duco::Association& currentAssociation, bool showOnlyLinked)
{
    time_point<high_resolution_clock> start = high_resolution_clock::now();
    std::set<Duco::ObjectId> associationIds;
    database->Database().AssociationsDatabase().GetAllObjectIds(associationIds);

    control->BeginUpdate();
    control->Items->Clear();
    control->Text = "";
    allAssociationIds->Clear();

    // Populate association options
    for (const auto& key : associationIds)
    {
        if (key != currentAssociation.Id())
        {
            Int16 nextAssociationId = key.Id();
            if ((showOnlyLinked && currentAssociation.LinkedToAssociation(key)) || !showOnlyLinked)
            {
                Int16 currentIndex = control->Items->Add(database->FindAssociationName(key));
                allAssociationIds->Add(nextAssociationId);
                if (currentAssociation.LinkedToAssociation(key))
                {
                    control->SetItemChecked(currentIndex, true);
                }
            }
        }
    }

    control->EndUpdate();
    DUCOUILOGDEBUGWITHTIME(start, "Generate Association options 2 took");
}

System::Void
DatabaseGenUtils::AddAssociationOption(const Duco::ObjectId& associationId, System::Collections::Generic::List<System::Int16>^ allAssociationIds, DucoUI::DatabaseManager^ database, System::Windows::Forms::ComboBox^ control)
{
    Int16 thisAssociationId = associationId.Id();
    if (!allAssociationIds->Contains(thisAssociationId))
    {
        control->Items->Add(database->FindAssociationName(associationId));
        allAssociationIds->Add(thisAssociationId);
    }
}

System::Void
DatabaseGenUtils::UpdateAssociationOption(const Duco::ObjectId& associationId, System::Collections::Generic::List<System::Int16>^ allAssociationIds, DucoUI::DatabaseManager^ database, System::Windows::Forms::ComboBox^ control)
{
    Int16 thisAssociationId = associationId.Id();
    int index = allAssociationIds->IndexOf(thisAssociationId);
    if (index != -1)
    {
        String^ associationName = database->FindAssociationName(associationId);
        control->Items[index] = associationName;
    }
}

System::Void
DatabaseGenUtils::RemoveAssociationOption(const Duco::ObjectId& associationId, System::Collections::Generic::List<System::Int16>^ allAssociationIds, System::Windows::Forms::ComboBox^ control)
{
    Int16 thisAssociationId = associationId.Id();
    if (allAssociationIds->Contains(thisAssociationId))
    {
        int index = allAssociationIds->IndexOf(thisAssociationId);
        allAssociationIds->Remove(thisAssociationId);
        control->Items->RemoveAt(index);
    }
}

//*******************************************************************************************************
// Stages
//*******************************************************************************************************

System::Void
DatabaseGenUtils::GenerateStageOptions(System::Windows::Forms::ComboBox^ stageSelector, DatabaseManager^ database, bool includeAll)
{
    stageSelector->BeginUpdate();
    stageSelector->Items->Clear();
    String^ defaultStage = "";
    if (includeAll)
    {
        defaultStage = gcnew String("All Stages");
        stageSelector->Items->Add(defaultStage);
    }
    std::vector<std::wstring> orderNames;
    database->Database().MethodsDatabase().GetAllOrderNames(orderNames);
    for (const auto& it : orderNames)
    {
        stageSelector->Items->Add(DucoUtils::ConvertString(it));
    }
    stageSelector->Text = defaultStage;
    stageSelector->EndUpdate();
}

System::Void
DatabaseGenUtils::GenerateMethodTypeOptions(System::Windows::Forms::ComboBox^% typeEditor, DatabaseManager^ database)
{
    std::set<std::wstring> types;
    database->Database().MethodsDatabase().GetAllMethodTypes(types);
    AddStringsToControl(database->Config(), typeEditor, types);
}

System::Void 
DatabaseGenUtils::GenerateMethodNameOptions(System::Windows::Forms::ComboBox^% control, DatabaseManager^ database)
{
    std::set<std::wstring> names;
    database->Database().MethodsDatabase().GetAllMethodNames(names);
    AddStringsToControl(database->Config(), control, names);
}

System::Void 
DatabaseGenUtils::GenerateMethodNotationOptions(System::Windows::Forms::ComboBox^% control, DatabaseManager^ database)
{
    std::set<std::wstring> placeNotations;
    database->Database().MethodsDatabase().GetAllPlaceNotations(placeNotations);
    AddStringsToControl(database->Config(), control, placeNotations);
}

System::Void
DatabaseGenUtils::AddStringsToControl(Duco::DucoConfiguration& config, System::Windows::Forms::ComboBox^% control, std::set<std::wstring>& strings)
{
    if (config.DisableFeaturesForPerformance())
    {
        control->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::None;
    }
    control->BeginUpdate();
    control->Items->Clear();
    for (const auto& it : strings)
    {
        control->Items->Add(DucoUtils::ConvertString(it));
    }
    strings.clear();
    control->EndUpdate();
}

//*******************************************************************************************************
// Compositions
//*******************************************************************************************************
System::Void
DatabaseGenUtils::GenerateCompositionOptions(System::Windows::Forms::ComboBox^ control,
                                             System::Collections::Generic::List<System::Int16>^ allCompositionNames,
                                             DucoUI::DatabaseManager^ database,
                                             const Duco::ObjectId& compositionId,
                                             unsigned int noOfBells,
                                             bool clearOldValue)
{
    if (database->Config().DisableFeaturesForPerformance())
    {
        control->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::None;
    }
    control->BeginUpdate();
    control->Items->Clear();
    allCompositionNames->Clear();

    std::set<Duco::ObjectId> compositionsIds;
    database->Database().CompositionsDatabase().GetCompositionsByNoOfBells(compositionsIds, database->Database().MethodsDatabase(), noOfBells);

    String^ tempOldComposition = control->Text;
    if (compositionId != -1 || clearOldValue)
    {
        tempOldComposition = ""; 
    }
    control->Text = "";

    unsigned int defaultIndex = -1;
    for (const auto& idIt : compositionsIds)
    {
        Duco::Composition theComposition(-1, database->Database().MethodsDatabase());
        if (database->FindComposition(idIt, theComposition, true))
        {
            Int16 nextCompositionId = idIt.Id();
            std::wstring title;
            std::wstring composer;
            theComposition.FullName(database->Database(), title, composer);
            String^ compositionName = DucoUtils::ConvertString(title);
            compositionName += " " + DucoUtils::ConvertString(composer);
            allCompositionNames->Add(nextCompositionId);
            unsigned int newIndex = control->Items->Add(compositionName);

            if (compositionId == theComposition.Id())
            {
                defaultIndex = newIndex;
            }
            else if (compositionName->Length > 0 && String::Compare(compositionName,tempOldComposition) == 0)
            {
                defaultIndex = newIndex;
            }
        }
    }
    if (defaultIndex != -1)
    {
        control->SelectedIndex = defaultIndex;
    }
    else if (tempOldComposition->Length > 0)
    {
        control->Text = tempOldComposition;
    }
    compositionsIds.clear();
    control->EndUpdate();
}

System::Void 
DatabaseGenUtils::AddCompositionOption(const Duco::ObjectId& compositionId, System::Collections::Generic::List<Int16>^ allCompositionNames,
                        unsigned int noOfBells, DatabaseManager^ database,
                        System::Windows::Forms::ComboBox^ compositionEditor)
{
    Int16 thisCompositionId = compositionId.Id();
    if (!allCompositionNames->Contains(thisCompositionId))
    {
        Duco::Composition theComposition(-1, database->Database().MethodsDatabase());
        if (database->FindComposition(compositionId, theComposition, true))
        {
            if (DucoEngineUtils::NoOfBellsMatch(theComposition.Order(database->Database().MethodsDatabase()), noOfBells))
            {
                std::wstring title;
                std::wstring composer;
                theComposition.FullName(database->Database(), title, composer);
                String^ compositionName = DucoUtils::ConvertString(title);
                compositionName += " " + DucoUtils::ConvertString(composer);
                compositionEditor->Items->Add(compositionName);
                allCompositionNames->Add(thisCompositionId);
            }
        }
    }
}

bool
DatabaseGenUtils::UpdateCompositionOption(const Duco::ObjectId& compositionId, System::Collections::Generic::List<Int16>^ allCompositionNames,
                        unsigned int noOfBells, DatabaseManager^ database,
                        System::Windows::Forms::ComboBox^ compositionEditor)
{
    Duco::Composition theComposition(-1, database->Database().MethodsDatabase());
    if (database->FindComposition(compositionId, theComposition, true))
    {
        Int16 thisCompositionId = compositionId.Id();
        int index = allCompositionNames->IndexOf(thisCompositionId);
        if (index != -1)
        {
            std::wstring title;
            std::wstring composer;
            theComposition.FullName(database->Database(), title, composer);
            String^ compositionName = DucoUtils::ConvertString(title);
            compositionName += " " + DucoUtils::ConvertString(composer);
            compositionEditor->Items[index] = compositionName;
            return true;
        }
    }
    return false;
}

bool 
DatabaseGenUtils::RemoveCompositionOption(const Duco::ObjectId& compositionId, System::Collections::Generic::List<Int16>^ allCompositionNames,
                                     unsigned int noOfBells, System::Windows::Forms::ComboBox^ compositionEditor)
{
    Int16 thisCompositionId = compositionId.Id();
    if (allCompositionNames->Contains(thisCompositionId))
    {
        int index = allCompositionNames->IndexOf(thisCompositionId);
        allCompositionNames->Remove(thisCompositionId);
        compositionEditor->Items->RemoveAt(index);
        return true;
    }
    return false;
}

//*******************************************************************************************************
// Method Series
//*******************************************************************************************************
System::Void
DatabaseGenUtils::GenerateMethodSeriesOptions(System::Windows::Forms::ComboBox^ seriesEditor,
                                              System::Collections::Generic::List<System::Int16>^ allSeriesNames, 
                                              DatabaseManager^ database,
                                              const Duco::ObjectId& seriesId,
                                              unsigned int noOfBells,
                                              bool clearOldValue)
{
    seriesEditor->BeginUpdate();
    seriesEditor->Items->Clear();
    allSeriesNames->Clear();

    std::vector<Duco::ObjectId> seriesIds;
    database->Database().MethodSeriesDatabase().GetMethodSeriesByNoOfBells(seriesIds, noOfBells);

    String^ tempOldMethod = seriesEditor->Text;
    if (seriesId != -1 || clearOldValue)
    {
        tempOldMethod = ""; 
    }
    seriesEditor->Text = "";

    unsigned int defaultIndex = -1;
    for (const auto& seriesIt : seriesIds)
    {
        MethodSeries theSeries;
        if (database->FindMethodSeries(seriesIt, theSeries, true))
        {
            Int16 nextSeriesId = seriesIt.Id();
            String^ seriesName = DucoUtils::ConvertString(theSeries.Name());
            allSeriesNames->Add(nextSeriesId);
            unsigned int newIndex = seriesEditor->Items->Add(seriesName);

            if (seriesId == theSeries.Id())
            {
                defaultIndex = newIndex;
            }
            else if (seriesName->Length > 0 && String::Compare(seriesName,tempOldMethod) == 0)
            {
                defaultIndex = newIndex;
            }
        }
    }
    if (defaultIndex != -1)
    {
        seriesEditor->SelectedIndex = defaultIndex;
    }
    else if (tempOldMethod->Length > 0)
    {
        seriesEditor->Text = tempOldMethod;
    }
    seriesIds.clear();
    seriesEditor->EndUpdate();
}

System::Void 
DatabaseGenUtils::AddMethodSeriesOption(const Duco::ObjectId& methodSeriesId, System::Collections::Generic::List<Int16>^ allMethodSeriesNames,
                        unsigned int noOfBells, DatabaseManager^ database,
                        System::Windows::Forms::ComboBox^ methodSeriesEditor)
{
    Int16 thisMethodSeriesId = methodSeriesId.Id();
    if (!allMethodSeriesNames->Contains(thisMethodSeriesId))
    {
        MethodSeries theMethodSeries;
        if (database->FindMethodSeries(methodSeriesId, theMethodSeries, true))
        {
            if (DucoEngineUtils::NoOfBellsMatch(theMethodSeries.NoOfBells(), noOfBells))
            {
                String^ newMethodSeriesName = DucoUtils::ConvertString(theMethodSeries.Name());
                methodSeriesEditor->Items->Add(newMethodSeriesName);
                allMethodSeriesNames->Add(thisMethodSeriesId);
            }
        }
    }
}

bool
DatabaseGenUtils::UpdateMethodSeriesOption(const Duco::ObjectId& methodSeriesId, System::Collections::Generic::List<Int16>^ allMethodSeriesNames,
                        unsigned int noOfBells, DatabaseManager^ database,
                        System::Windows::Forms::ComboBox^ methodSeriesEditor)
{
    MethodSeries theMethodSeries;
    if (database->FindMethodSeries(methodSeriesId, theMethodSeries, true))
    {
        Int16 thisMethodSeriesId = methodSeriesId.Id();
        int index = allMethodSeriesNames->IndexOf(thisMethodSeriesId);
        if (index != -1)
        {
            String^ newMethodSeriesName = DucoUtils::ConvertString(theMethodSeries.Name());
            methodSeriesEditor->Items[index] = newMethodSeriesName;
            return true;
        }
    }
    return false;
}

bool 
DatabaseGenUtils::RemoveMethodSeriesOption(const Duco::ObjectId& methodSeriesId, System::Collections::Generic::List<Int16>^ allMethodSeriesNames,
                                     unsigned int noOfBells, System::Windows::Forms::ComboBox^ methodSeriesEditor)
{
    Int16 thisMethodSeriesId = methodSeriesId.Id();
    if (allMethodSeriesNames->Contains(thisMethodSeriesId))
    {
        int index = allMethodSeriesNames->IndexOf(thisMethodSeriesId);
        allMethodSeriesNames->Remove(thisMethodSeriesId);
        methodSeriesEditor->Items->RemoveAt(index);
        return true;
    }
    return false;
}

//EOF
