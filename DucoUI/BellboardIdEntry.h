#pragma once

namespace DucoUI
{

    public ref class BellboardIdEntry : public System::Windows::Forms::Form, public DucoUI::BellboardParserUIWrapper
    {
    public:
        BellboardIdEntry(DucoUI::DatabaseManager^ theDatabase);
        virtual System::Void Cancelled(System::String^ bellboardId, System::String^ errorMessage);
        virtual System::Void Completed(System::String^ bellboardId, System::Boolean errors);

    protected:
        System::Void BellboardIdEntry_Load(System::Object^ sender, System::EventArgs^ e);

        ~BellboardIdEntry();
        System::Void downloadBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void closeBtn_Click(System::Object ^ sender, System::EventArgs ^ e);
        System::Void openPealBtn_Click(System::Object^ sender, System::EventArgs^ e);
        System::Void bellboardIdEditor_TextChanged(System::Object^ sender, System::EventArgs^ e);
        System::Void bellboardIdEditor_Validated(System::Object^ sender, System::EventArgs^ e);
        System::Void bellboardIdEditor_Validating(System::Object ^ sender, System::ComponentModel::CancelEventArgs ^ e);

    private: System::Windows::Forms::Button^ closeBtn;
    private: System::Windows::Forms::Button^ downloadBtn;
    private: System::Windows::Forms::TextBox^ bellboardIdEditor;

    private:
        DucoUI::DatabaseManager^ database;
        Duco::BellboardPerformanceParser* parser;
        DucoUI::BellboardParserWrapper* callbackWrapper;

        System::Net::Http::HttpClient^ downloadHandler;
        System::Windows::Forms::Label^ successLbl;
        System::Windows::Forms::Button^ openPealBtn;
    private: System::ComponentModel::IContainer^ components;

    private: System::Windows::Forms::Label^ validityLbl;
    private: System::Windows::Forms::ErrorProvider^ errorProvider;
           Duco::ObjectId* lastDownloadedPealId;

#pragma region Windows Form Designer generated code
        void InitializeComponent()
        {
            this->components = (gcnew System::ComponentModel::Container());
            System::Windows::Forms::TableLayoutPanel^ tableLayoutPanel;
            System::Windows::Forms::GroupBox^ idGroupBox;
            this->closeBtn = (gcnew System::Windows::Forms::Button());
            this->downloadBtn = (gcnew System::Windows::Forms::Button());
            this->validityLbl = (gcnew System::Windows::Forms::Label());
            this->bellboardIdEditor = (gcnew System::Windows::Forms::TextBox());
            this->successLbl = (gcnew System::Windows::Forms::Label());
            this->openPealBtn = (gcnew System::Windows::Forms::Button());
            this->errorProvider = (gcnew System::Windows::Forms::ErrorProvider(this->components));
            tableLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
            idGroupBox = (gcnew System::Windows::Forms::GroupBox());
            tableLayoutPanel->SuspendLayout();
            idGroupBox->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->errorProvider))->BeginInit();
            this->SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            tableLayoutPanel->ColumnCount = 2;
            tableLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            tableLayoutPanel->Controls->Add(this->closeBtn, 1, 2);
            tableLayoutPanel->Controls->Add(this->downloadBtn, 0, 2);
            tableLayoutPanel->Controls->Add(idGroupBox, 0, 0);
            tableLayoutPanel->Controls->Add(this->successLbl, 0, 1);
            tableLayoutPanel->Controls->Add(this->openPealBtn, 1, 1);
            tableLayoutPanel->Dock = System::Windows::Forms::DockStyle::Fill;
            tableLayoutPanel->Location = System::Drawing::Point(0, 0);
            tableLayoutPanel->Name = L"tableLayoutPanel";
            tableLayoutPanel->RowCount = 3;
            tableLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 58)));
            tableLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
            tableLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
            tableLayoutPanel->Size = System::Drawing::Size(353, 135);
            tableLayoutPanel->TabIndex = 0;
            // 
            // closeBtn
            // 
            this->closeBtn->DialogResult = System::Windows::Forms::DialogResult::Cancel;
            this->closeBtn->Location = System::Drawing::Point(275, 109);
            this->closeBtn->Name = L"closeBtn";
            this->closeBtn->Size = System::Drawing::Size(75, 23);
            this->closeBtn->TabIndex = 4;
            this->closeBtn->Text = L"Close";
            this->closeBtn->UseVisualStyleBackColor = true;
            this->closeBtn->Click += gcnew System::EventHandler(this, &BellboardIdEntry::closeBtn_Click);
            // 
            // downloadBtn
            // 
            this->downloadBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
            this->downloadBtn->Location = System::Drawing::Point(194, 109);
            this->downloadBtn->Name = L"downloadBtn";
            this->downloadBtn->Size = System::Drawing::Size(75, 23);
            this->downloadBtn->TabIndex = 3;
            this->downloadBtn->Text = L"Download";
            this->downloadBtn->UseVisualStyleBackColor = true;
            this->downloadBtn->Click += gcnew System::EventHandler(this, &BellboardIdEntry::downloadBtn_Click);
            // 
            // idGroupBox
            // 
            tableLayoutPanel->SetColumnSpan(idGroupBox, 2);
            idGroupBox->Controls->Add(this->validityLbl);
            idGroupBox->Controls->Add(this->bellboardIdEditor);
            idGroupBox->Dock = System::Windows::Forms::DockStyle::Fill;
            idGroupBox->Location = System::Drawing::Point(3, 3);
            idGroupBox->Name = L"idGroupBox";
            idGroupBox->Size = System::Drawing::Size(347, 52);
            idGroupBox->TabIndex = 2;
            idGroupBox->TabStop = false;
            idGroupBox->Text = L"Bellboard id";
            // 
            // validityLbl
            // 
            this->validityLbl->AutoSize = true;
            this->validityLbl->Location = System::Drawing::Point(161, 23);
            this->validityLbl->Name = L"validityLbl";
            this->validityLbl->Size = System::Drawing::Size(0, 13);
            this->validityLbl->TabIndex = 1;
            // 
            // bellboardIdEditor
            // 
            this->bellboardIdEditor->Location = System::Drawing::Point(10, 20);
            this->bellboardIdEditor->Name = L"bellboardIdEditor";
            this->bellboardIdEditor->Size = System::Drawing::Size(145, 20);
            this->bellboardIdEditor->TabIndex = 0;
            this->bellboardIdEditor->TextChanged += gcnew System::EventHandler(this, &BellboardIdEntry::bellboardIdEditor_TextChanged);
            this->bellboardIdEditor->Validating += gcnew System::ComponentModel::CancelEventHandler(this, &BellboardIdEntry::bellboardIdEditor_Validating);
            this->bellboardIdEditor->Validated += gcnew System::EventHandler(this, &BellboardIdEntry::bellboardIdEditor_Validated);
            // 
            // successLbl
            // 
            this->successLbl->AutoSize = true;
            this->successLbl->Dock = System::Windows::Forms::DockStyle::Fill;
            this->successLbl->Location = System::Drawing::Point(3, 58);
            this->successLbl->Name = L"successLbl";
            this->successLbl->Size = System::Drawing::Size(266, 48);
            this->successLbl->TabIndex = 3;
            this->successLbl->Text = L"Click Download";
            // 
            // openPealBtn
            // 
            this->openPealBtn->Location = System::Drawing::Point(275, 61);
            this->openPealBtn->Name = L"openPealBtn";
            this->openPealBtn->Size = System::Drawing::Size(75, 23);
            this->openPealBtn->TabIndex = 2;
            this->openPealBtn->Text = L"Open peal";
            this->openPealBtn->UseVisualStyleBackColor = true;
            this->openPealBtn->Click += gcnew System::EventHandler(this, &BellboardIdEntry::openPealBtn_Click);
            // 
            // errorProvider
            // 
            this->errorProvider->ContainerControl = this;
            // 
            // BellboardIdEntry
            // 
            this->AcceptButton = this->downloadBtn;
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->CancelButton = this->closeBtn;
            this->ClientSize = System::Drawing::Size(353, 135);
            this->Controls->Add(tableLayoutPanel);
            this->Name = L"BellboardIdEntry";
            this->Text = L"Download bellboard peal";
            this->Load += gcnew System::EventHandler(this, &BellboardIdEntry::BellboardIdEntry_Load);
            tableLayoutPanel->ResumeLayout(false);
            tableLayoutPanel->PerformLayout();
            idGroupBox->ResumeLayout(false);
            idGroupBox->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->errorProvider))->EndInit();
            this->ResumeLayout(false);

        }
#pragma endregion
    };
}
