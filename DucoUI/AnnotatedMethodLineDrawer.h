#pragma once

#include "NormalMethodLineDrawer.h"

namespace Duco
{
    class Change;
    class Method;
}

namespace DucoUI
{
    ref class DatabaseManager;

    public ref class AnnotatedMethodLineDrawer : public NormalMethodLineDrawer
    {
    public:
        AnnotatedMethodLineDrawer(System::Drawing::Graphics^ lineDrwr, Duco::Method& theMethod, DucoUI::DatabaseManager^ theDatabase, System::ComponentModel::BackgroundWorker^ bgWorker);
        
    protected: // from base class MethodLineDrawer
        virtual bool ShowMusic() override;

    protected: // new functions
    };
}
