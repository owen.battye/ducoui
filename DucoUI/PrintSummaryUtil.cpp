#include "PrintSummaryUtil.h"
#include "DatabaseManager.h"
#include "DucoCommon.h"
#include "DucoUtils.h"
#include <PealLengthInfo.h>
#include <DatabaseSettings.h>
#include <RingingDatabase.h>
#include <PealDatabase.h>
#include <Peal.h>
#include <Ringer.h>
#include <Tower.h>
#include <Method.h>
#include <StatisticFilters.h>
#include <DucoEngineUtils.h>

using namespace System;
using namespace System::Drawing;
using namespace Duco;
using namespace DucoUI;

const float KFontSize = 10;
const float KMarginOffset = 30;

PrintSummaryUtil::PrintSummaryUtil(DatabaseManager^ theDatabase, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
:	PrintUtilBase(theDatabase, KFontSize, printArgs, false)
{
    smallBoldFont = gcnew System::Drawing::Font( KDefaultPrintFont, KFontSize-2, FontStyle::Bold);
}

System::Void
PrintSummaryUtil::printObject(System::Drawing::Printing::PrintPageEventArgs^ args, System::Drawing::Printing::PrinterSettings^ printSettings)
{
    addDucoFooter(smallFont, gcnew StringFormat, args);

    StringFormat^ defaultStringFormat = gcnew StringFormat();
    array<Single>^ tabStops = {KMarginOffset};
    defaultStringFormat->SetTabStops(KMarginOffset, tabStops);

    String^ printBuffer = "Summary for database: ";
    printBuffer += DucoUtils::FormatFilename(database->Filename());
    args->Graphics->DrawString(printBuffer, boldFont, Brushes::Black, leftMargin, yPos, defaultStringFormat );
    yPos += 2*lineHeight;

    Duco::StatisticFilters filters(database->Database());
    PealLengthInfo allPealsInfo = database->Database().PealsDatabase().PerformanceInfo(filters);
    filters.SetExcludeInvalidTimeOrChanges(true);
    PealLengthInfo lengthInfo = database->Database().PealsDatabase().PerformanceInfo(filters);

    if (allPealsInfo.TotalPeals() > 1)
    {
        if (database->Settings().DefaultRingerSet())
        {
            Duco::Ringer defaultRinger;
            if (database->FindRinger(database->Settings().DefaultRinger(), defaultRinger, false))
            {
                printBuffer = DucoUtils::ConvertString(defaultRinger.FullName(false)) + " has rung ";
            }
        }
        else
        {
            printBuffer = "There are ";
        }
        printBuffer += Convert::ToString(database->NumberOfValidObjects(TObjectType::EPeal));
        printBuffer += " valid " + database->PerformanceString(false) + "s ";
        if (database->Settings().DefaultRingerSet())
        {
            printBuffer += "during the last ";
        }
        else
        {
            printBuffer += "in the database, which have been rung in the ";
        }
        printBuffer += DucoUtils::ConvertString(allPealsInfo.DurationBetweenFirstAndLastPealString());

        printBuffer += " between " + DucoUtils::ConvertString(allPealsInfo.DateOfFirstPealString(), true);
        printBuffer += " and " + DucoUtils::ConvertString(allPealsInfo.DateOfLastPealString(), true) + ".";
        PrintLongString(printBuffer, normalFont, args);
    }

    Duco::StatisticFilters handBellFilters(database->Database());
    handBellFilters.SetIncludeTowerBell(false);
    size_t noOfHandBellPeals = database->Database().PealsDatabase().PerformanceInfo(handBellFilters).TotalPeals();
    if (noOfHandBellPeals > 0)
    {
        printBuffer = "\tincluding " + Convert::ToString(noOfHandBellPeals) + " handbell ";
        if (database->Settings().PealDatabase())
        {
            printBuffer += "peals.";
        }
        else
        {
            printBuffer += "quarters.";
        }
        args->Graphics->DrawString(printBuffer, normalFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
        yPos += lineHeight;
    }

    yPos += lineHeight;

    if (database->Settings().PealDatabase())
    {
        args->Graphics->DrawString("Record peal lengths", boldFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
    }
    else
    {
        args->Graphics->DrawString("Record quarter peal lengths", boldFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
    }

    yPos += lineHeight;
    PrintPealTitle("Longest performance by changes: ", lengthInfo.LongestChangesId(), false, args);
    PrintPealTitle("Longest performance by time: ", lengthInfo.LongestMinutesId(), false, args);
    PrintPealTitle("Shortest performance by changes: ", lengthInfo.ShortestChangesId(), false, args);
    PrintPealTitle("Shortest performance by time: ", lengthInfo.ShortestMinutesId(), false, args);
    PrintPealTitle("Slowest performance: ", lengthInfo.SlowestCpmId(), true, args);
    PrintPealTitle("Fastest performance: ", lengthInfo.FastestCpmId(), true, args);
    yPos += lineHeight;

    args->Graphics->DrawString("Leading ringers", boldFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
    yPos += lineHeight;
    {
        std::multimap<Duco::PealLengthInfo, ObjectId> topFiveRingers;
        database->Database().PealsDatabase().GetTopRingers(filters, 5, true, topFiveRingers);
        std::multimap<Duco::PealLengthInfo, ObjectId>::const_reverse_iterator it = topFiveRingers.rbegin();
        while (it != topFiveRingers.rend())
        {
            PrintRinger(it->second, it->first, args);
            ++it;
        }
    }
    yPos += lineHeight;

    args->Graphics->DrawString("Leading towers", boldFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
    yPos += lineHeight;
    {
        std::multimap<Duco::PealLengthInfo, Duco::ObjectId> topFiveTowers;
        database->Database().PealsDatabase().GetTopTowers(filters, 5, topFiveTowers);
        std::multimap<Duco::PealLengthInfo, Duco::ObjectId>::const_reverse_iterator it = topFiveTowers.rbegin();
        while (it != topFiveTowers.rend())
        {
            PrintTower(it->second, it->first, args);
            ++it;
        }
    }
    yPos += lineHeight;

    args->Graphics->DrawString("Leading methods", boldFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
    yPos += lineHeight;
    {
        std::multimap<Duco::PealLengthInfo, Duco::ObjectId> topFiveMethods;
        database->Database().PealsDatabase().GetTopFiveMethods(filters, topFiveMethods);
        std::multimap<Duco::PealLengthInfo, Duco::ObjectId>::const_reverse_iterator it = topFiveMethods.rbegin();
        while (it != topFiveMethods.rend())
        {
            PrintMethod(it->second, it->first, args);
            ++it;
        }
    }
    yPos += lineHeight;

    if (database->Settings().PealDatabase())
    {
        args->Graphics->DrawString("Total peal lengths", boldFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
    }
    else
    {
        args->Graphics->DrawString("Total quarter peal lengths", boldFont, Brushes::Black, leftMargin, yPos, defaultStringFormat);
    }


    yPos += lineHeight;
    PrintLongString(DucoUtils::ConvertString(DucoEngineUtils::ToString(lengthInfo.TotalChanges(), true)) + " changes", smallFont, float(args->MarginBounds.Left + KMarginOffset), args);
    PrintLongString(DucoUtils::PrintPealTime(lengthInfo.TotalMinutes(), true), smallFont, float(args->MarginBounds.Left + KMarginOffset), args);
    yPos += lineHeight;


    Duco::ObjectId mostNortherly;
    Duco::ObjectId mostSoutherly;
    Duco::ObjectId mostEasterly;
    Duco::ObjectId mostWesterly;
    filters.SetExcludeInvalidTimeOrChanges(false);
    if (database->Database().PealsDatabase().GetFurthestPeals(filters, mostNortherly, mostSoutherly, mostEasterly, mostWesterly))
    {
        if (CheckForDifferentTowerIds(mostNortherly, mostSoutherly, mostEasterly, mostWesterly))
        {
            PrintLongString("Most distant performances", boldFont, float(args->MarginBounds.Left), args);
            PrintPeal(mostNortherly, "northerly", args);
            PrintPeal(mostEasterly,  "easterly", args);
            PrintPeal(mostSoutherly, "southerly", args);
            PrintPeal(mostWesterly,  "westerly", args);
        }
    }
}

System::Void
PrintSummaryUtil::PrintPealTitle(System::String^ title, const Duco::ObjectId& pealId, bool includeSpeed, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
{
    Duco::Peal peal;
    if (database->FindPeal(pealId, peal, false))
    {
        PrintLongString(title, smallBoldFont, float(printArgs->MarginBounds.Left + KMarginOffset), printArgs);

        std::wstring pealTitle;
        peal.SummaryTitle(pealTitle, database->Database());
        String^ printBuffer = "\t" + DucoUtils::ConvertString(pealTitle);
        if (includeSpeed)
        {
            printBuffer += " at ";
            printBuffer += DucoUtils::FormatFloatToString(peal.ChangesPerMinute());
            printBuffer += " changes per minute.";
        }
        else
        {
            printBuffer += ".";
        }
        PrintLongString(printBuffer, smallFont, float(printArgs->MarginBounds.Left + KMarginOffset), printArgs);
    }
}

System::Void
PrintSummaryUtil::PrintRinger(const ObjectId& ringerId, const Duco::PealLengthInfo& noOfPeals, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
{
    Duco::Ringer ringer;
    if (database->FindRinger(ringerId, ringer, false))
    {
        String^ printBuffer = "";
        if (database->Settings().PealDatabase())
        {
            printBuffer += noOfPeals.TotalPeals() + " peals: ";
        }
        else
        {
            printBuffer += noOfPeals.TotalPeals() + " quarters: ";
        }
        printBuffer += DucoUtils::ConvertString(ringer.FullName(false));
        PrintLongString(printBuffer, smallFont, float(printArgs->MarginBounds.Left + KMarginOffset), printArgs);
    }
}

System::Void
PrintSummaryUtil::PrintTower(const Duco::ObjectId& towerId, const Duco::PealLengthInfo& noOfPeals, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
{
    Duco::Tower tower;
    if (database->FindTower(towerId, tower, false))
    {
        String^ printBuffer = "";
        if (database->Settings().PealDatabase())
            printBuffer += noOfPeals.TotalPeals() + " peals: ";
        else
            printBuffer += noOfPeals.TotalPeals() + " quarters: ";
        printBuffer += DucoUtils::ConvertString(tower.FullName());
        PrintLongString(printBuffer, smallFont, float(printArgs->MarginBounds.Left + KMarginOffset), printArgs);
    }
}

System::Void
PrintSummaryUtil::PrintMethod(const Duco::ObjectId& methodId, const Duco::PealLengthInfo& noOfPeals, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
{
    Duco::Method method;
    if (database->FindMethod(methodId, method, false))
    {
        String^ printBuffer = "";
        if (database->Settings().PealDatabase())
            printBuffer += noOfPeals.TotalPeals() + " peals: ";
        else
            printBuffer += noOfPeals.TotalPeals() + " quarters: ";

        printBuffer += DucoUtils::ConvertString(method.FullName(database->Database()));
        PrintLongString(printBuffer, smallFont, float(printArgs->MarginBounds.Left + KMarginOffset), printArgs);
    }
}

System::Void
PrintSummaryUtil::PrintPeal(const Duco::ObjectId& pealId, System::String^ title, System::Drawing::Printing::PrintPageEventArgs^ printArgs)
{
    Duco::Peal peal;
    if (database->FindPeal(pealId, peal, false))
    {
        Duco::Tower tower;
        if (database->FindTower(peal.TowerId(), tower, false))
        {
            String^ printBuffer = "Most " + title;
            if (database->Settings().PealDatabase())
                printBuffer += " peal:";
            else
                printBuffer += " quarter:";

            PrintLongString(printBuffer, smallBoldFont, float(printArgs->MarginBounds.Left + KMarginOffset), printArgs);
            std::wstring pealTitle;
            peal.SummaryTitle(pealTitle, database->Database());
            printBuffer = DucoUtils::ConvertString(pealTitle);
            PrintLongString(printBuffer, smallFont, float(printArgs->MarginBounds.Left + KMarginOffset), printArgs);
        }
    }
}

bool
PrintSummaryUtil::CheckForDifferentTowerIds(const Duco::ObjectId& mostNortherly, const Duco::ObjectId& mostSoutherly, const Duco::ObjectId& mostEasterly, const Duco::ObjectId& mostWesterly)
{
    Duco::ObjectId northId = GetTowerId(mostNortherly);
    if (northId != GetTowerId(mostSoutherly))
        return true;
    if (northId != GetTowerId(mostEasterly))
        return true;
    if (northId != GetTowerId(mostWesterly))
        return true;

    return false;
}



Duco::ObjectId
PrintSummaryUtil::GetTowerId(const Duco::ObjectId& pealId)
{
    Duco::ObjectId towerId;
    Duco::Peal peal;
    if (database->FindPeal(pealId, peal, false))
    {
        towerId = peal.TowerId();
    }
    return towerId;
}
