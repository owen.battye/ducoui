#include "FileTypeDetector.h"

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::Data::Odbc;

FileTypeDetector::FileTypeDetector()
{
}

FileTypeDetector::~FileTypeDetector()
{
    if (connection != nullptr)
        connection->Close();
}

Duco::TFileType
FileTypeDetector::FileType(System::String^ fileName)
{
    if (fileName->EndsWith(".pbr", StringComparison::CurrentCultureIgnoreCase))
    {
        return EPealBasePBR;
    }
    if (fileName->EndsWith(".tsv", StringComparison::CurrentCultureIgnoreCase))
    {
        return EPealBaseTSV;
    }
    if (fileName->EndsWith(".csv", StringComparison::CurrentCultureIgnoreCase))
    {
        return ECsv;
    }
    if (fileName->EndsWith(".plf", StringComparison::CurrentCultureIgnoreCase))
    {
        return EWinRkVersion1;
    }
    try
    {
        String^ connectionString = "Driver={Microsoft Access Driver (*.mdb)};DBQ=" + fileName + ";provider=SQLOLEDB;User Id="";Password=""";
        connection = gcnew OdbcConnection (connectionString);
        connection->Open();
    }
    catch (OdbcException^ ex)
    {
        if (ex->ErrorCode == -2146232009)
        {
            return ENoDatabaseDrivers;
        }

        return EUnknownFileType;
    }

    if (CheckTableExists("Version 3"))
    {
        return EWinRkVersion3;
    }
    else if (CheckTableExists("Composers") && CheckTableExists("Peals") && CheckTableExists("Ringers")
             && CheckTableExists("Societies") && CheckTableExists("Towers"))
    {
        return EWinRkVersion2;
    }

    return EUnknownFileType;
}

bool
FileTypeDetector::CheckTableExists(System::String^ tableName)
{
    try
    {
        OdbcCommand^ command = connection->CreateCommand();
        command->CommandText = "SELECT COUNT(*) FROM \"" + tableName +"\"";
        command->ExecuteScalar();
        return true;
    }
    catch (OdbcException^)
    {
        return false;
    }
    return false;
}
