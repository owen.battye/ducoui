#include "InputStringQueryUI.h"
#include "SystemDefaultIcon.h"
#include "SoundUtils.h"
#include "DucoUIUtils.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace DucoUI;

InputStringQueryUI::InputStringQueryUI(System::String^ promptStr)
{
    InitializeComponent();
    this->Text = L"Find " + promptStr;
    searchString = gcnew System::String("");
}

InputStringQueryUI::~InputStringQueryUI()
{
    if (components)
    {
        delete components;
    }
}

System::Void 
InputStringQueryUI::InputStringQueryUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
}

System::Void 
InputStringQueryUI::okBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    bool suceeded = false;
    if (idEditor->Text->Length > 0)
    {
        try
        {
            searchString = idEditor->Text;
            this->DialogResult = ::DialogResult::OK;
            suceeded = true;
        }
        catch (Exception^)
        {
            SoundUtils::PlayErrorSound();
            idEditor->Text = "";
        }
    }
    
    if (suceeded)
        Close();
}

System::Void
InputStringQueryUI::cancelBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    searchString = "";
    Close();
}
