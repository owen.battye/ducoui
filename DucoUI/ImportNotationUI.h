#pragma once
#include "ProgressWrapper.h"
#include <map>
#include <ObjectId.h>

namespace DucoUI
{
    ref class DatabaseManager;

    public ref class ImportNotationUI : public System::Windows::Forms::Form, public DucoUI::ProgressCallbackUI
    {
    public:
        ImportNotationUI(DucoUI::DatabaseManager^ theDatabase);

        // from ProgressCallback
        virtual void Initialised();
        virtual void Step(int progressPercent);
        virtual void Complete();

    protected:
        ~ImportNotationUI();
        !ImportNotationUI();
        void InitializeComponent();

        bool OpenFileSelectionDlg();
        System::Void GenerateMethodOptions();

        System::Void closeBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void importBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void downloadFileBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void websiteBtn_Click(System::Object^  sender, System::EventArgs^  e);
        System::Void openFileBtn_Click(System::Object^  sender, System::EventArgs^  e);

        System::Void oneMethod_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void allMethods_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
        System::Void methodList_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);

        System::Void ImportNotationUI_Load(System::Object^  sender, System::EventArgs^  e);
        System::Void ImportNotationUI_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e);

        System::Void importMethodsWorker_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
        System::Void importMethodsWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
        System::Void importMethodsWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);

        System::Void downloadClient_DownloadFileCompleted(System::Object^  sender, System::ComponentModel::AsyncCompletedEventArgs^  e);

    private:
        DucoUI::DatabaseManager^                    database;
        DucoUI::ProgressCallbackWrapper*            progressWrapper;
        System::Windows::Forms::TextBox^            fileNameEditor;
        System::Windows::Forms::Button^             importBtn;
        std::map<Duco::ObjectId, std::wstring>*     methods;
        static System::Threading::ManualResetEvent^ allDone;

        System::Windows::Forms::RadioButton^        allMethods;
        System::Windows::Forms::RadioButton^        oneMethod;
        System::Windows::Forms::ComboBox^           methodList;
        System::Windows::Forms::Button^             closeBtn;
        System::Windows::Forms::Button^             openFileBtn;
        System::Windows::Forms::Button^             downloadFileBtn;
        System::Windows::Forms::ToolStripProgressBar^  progressBar;
        System::Windows::Forms::ToolStripStatusLabel^  noOfMethodsUpdated;
        System::ComponentModel::BackgroundWorker^   importMethodsWorker;

        System::Net::WebClient^                     downloadClient;
        System::Windows::Forms::Button^             websiteBtn;
        System::ComponentModel::IContainer^         components;
        System::Windows::Forms::StatusStrip^        statusStrip1;

        System::Windows::Forms::ToolTip^            toolTip;
    };
}
