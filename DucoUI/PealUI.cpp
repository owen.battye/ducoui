#include "DatabaseManager.h"
#include "RingingDatabaseObserver.h"
#include "RingerDisplayCache.h"
#include "DucoWindowState.h"
#include "OpenObjectCallback.h"
#include "PictureListControl.h"
#include "PictureControl.h"
#include "RingerList.h"
#include <Tower.h>
#include "PrintBandSummaryUtil.h"
#include <DownloadedPeal.h>

#include "PealUI.h"

#include <Peal.h>
#include "DucoCommon.h"
#include "SystemDefaultIcon.h"
#include "InputIndexQueryUI.h"
#include <MethodDatabase.h>
#include "SoundUtils.h"
#include "DatabaseGenUtils.h"
#include <RingingDatabase.h>
#include "ProgressWrapper.h"
#include "PrintCompositionUtil.h"
#include "CompositionUI.h"
#include <DatabaseSettings.h>
#include "DucoUtils.h"
#include <DucoConfiguration.h>
#include "PrintPealUtil.h"
#include <StatisticFilters.h>
#include <Method.h>
#include "PealFeesUI.h"
#include "DucoUIUtils.h"
#include "TowerUI.h"
#include "MethodUI.h"
#include "InputDateQueryUI.h"
#include "MethodSeriesUI.h"
#include <Method.h>
#include <MethodSeries.h>
#include <RingingDatabase.h>
#include <Ring.h>
#include <Ringer.h>
#include "PealStatsUI.h"
#include "ImportSingleMethod.h"
#include "PictureEventArgs.h"
#include <DucoEngineUtils.h>
#include "UploadPealUI.h"
#include "DucoUILog.h"

using namespace System::Collections;
using namespace System::Drawing;
using namespace System::Drawing::Printing;
using namespace System::Windows::Forms;
using namespace DucoUI;
using namespace Duco;
using namespace std;
using namespace std::chrono;

PealUI::PealUI(DatabaseManager^ theDatabase, DucoWindowState newState)
:   database(theDatabase), state(newState),
    generatingNewMethod(false), generatingNewSeries(false), generatingNewComposition(false),
    generatingNewTower(false), dataChanged(false), currentPeal(NULL), populatingFromDownload(false)
{
    allTowerNames = gcnew System::Collections::Generic::List<System::Int16>();
    allRingNames = gcnew System::Collections::Generic::List<System::Int16>();
    allMethodNames = gcnew System::Collections::Generic::List<System::Int16>();
    allSeriesNames = gcnew System::Collections::Generic::List<System::Int16>();
    allCompositionNames = gcnew System::Collections::Generic::List<System::Int16>();
    allAssociationNames = gcnew System::Collections::Generic::List<System::Int16>();
    currentTower = new Duco::Tower();
    InitializeComponent();
    InitializePictureControl();
    SetWindowTitle();
    CreateRingersList();
}

PealUI::PealUI(DatabaseManager^ theDatabase, const Duco::ObjectId& pealId)
:   database(theDatabase), state(DucoWindowState::EViewMode),
    generatingNewMethod(false), generatingNewSeries(false), generatingNewComposition(false),
    generatingNewTower(false), dataChanged(false), populatingFromDownload(false)
{
    allTowerNames = gcnew System::Collections::Generic::List<System::Int16>();
    allRingNames = gcnew System::Collections::Generic::List<System::Int16>();
    allMethodNames = gcnew System::Collections::Generic::List<System::Int16>();
    allSeriesNames = gcnew System::Collections::Generic::List<System::Int16>();
    allCompositionNames = gcnew System::Collections::Generic::List<System::Int16>();
    allAssociationNames = gcnew System::Collections::Generic::List<System::Int16>();
    currentTower = new Duco::Tower();
    InitializeComponent();
    InitializePictureControl();
    SetWindowTitle();
    currentPeal = new Peal();
    database->FindPeal(pealId, *currentPeal, false);
    CreateRingersList();
}

PealUI::PealUI(DatabaseManager^ theDatabase, const Duco::DownloadedPeal& thePeal)
:   database(theDatabase), state(DucoWindowState::ENewMode),
    generatingNewMethod(false), generatingNewSeries(false), generatingNewComposition(false),
    generatingNewTower(false), dataChanged(false), populatingFromDownload(true)
{
    allTowerNames = gcnew System::Collections::Generic::List<System::Int16>();
    allRingNames = gcnew System::Collections::Generic::List<System::Int16>();
    allMethodNames = gcnew System::Collections::Generic::List<System::Int16>();
    allSeriesNames = gcnew System::Collections::Generic::List<System::Int16>();
    allCompositionNames = gcnew System::Collections::Generic::List<System::Int16>();
    allAssociationNames = gcnew System::Collections::Generic::List<System::Int16>();
    currentTower = new Duco::Tower();
    InitializeComponent();
    InitializePictureControl();
    SetWindowTitle();
    currentPeal = new DownloadedPeal(thePeal);
    CreateRingersList();
}

void
PealUI::ShowPeal(DatabaseManager^ theDatabase, System::Windows::Forms::Form^ parent, const Duco::ObjectId& pealId)
{
    if (pealId != -1 && theDatabase->ObjectExists(TObjectType::EPeal, pealId))
    {
        PealUI^ ui = gcnew PealUI(theDatabase, pealId);
        ui->MdiParent = parent;
        ui->Show();
    }
}

PealUI^
PealUI::PreparePealUI(DatabaseManager^ theDatabase, System::Windows::Forms::Form^ parent, const Duco::DownloadedPeal& thePeal)
{
    PealUI^ ui = gcnew PealUI(theDatabase, thePeal);
    ui->MdiParent = parent;
    ui->state = DucoWindowState::EAutoNewMode;
    return ui;
}

PealUI::!PealUI()
{
    delete currentPeal;
    delete currentTower;
}

PealUI::~PealUI()
{
    this->!PealUI();
    if (components)
    {
        delete components;
    }
}

System::Void 
PealUI::PealUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    this->Closing += gcnew System::ComponentModel::CancelEventHandler(this, &PealUI::ClosingEvent);
    this->Icon = SystemDefaultIcon::DefaultSystemIcon();
    pictureCtrl->DisableSelection();

    DatabaseGenUtils::GenerateTowerOptions(towerEditor, allTowerNames, database, -1, false, false, false);
    GenerateComposerOptions();
   
    switch (state)
    {
    case DucoWindowState::EViewMode:
    default:
        if (currentPeal != NULL)
        {
            PopulateView();
            SetViewState();
        }
        else
        {
            SetViewStateAndShowLastPeal();
        }
        break;
    case DucoWindowState::ENewMode:
    case DucoWindowState::EAutoNewMode:
    {
        if (populatingFromDownload)
        {
            ringerList->SetState(state);
            PopulateView();
            dataChanged = true;
        }
        else
        {
            ClearView();
        }
        SetNewState();
        }
        break;
    }
    database->AddObserver(this);
}

System::Void
PealUI::ClosingEvent(System::Object^ object, System::ComponentModel::CancelEventArgs^ e)
{
    if ( dataChanged )
    {
        if ( MessageBox::Show(this, "Do you want to continue and lose changes?", "Duco - Unsaved changes to peal", MessageBoxButtons::YesNo, MessageBoxIcon::Warning ) == ::DialogResult::No )
        {
            e->Cancel = true;
        }
    }
    if (!e->Cancel)
    {
        database->CancelEdit(TObjectType::EPeal, currentPeal->Id());
        database->RemoveObserver(this);
    }
}

void
PealUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    switch (type)
    {
    case TObjectType::EPicture:
        if (id == currentPeal->PictureId())
        {
            PopulateView();
        }
        break;

    case TObjectType::EPeal:
        if (id != currentPeal->Id() && id.ValidId())
        {
            PopulateIndexIndicator();
        }
        else
        {
            switch (eventId)
            {
            case EDeleted:
                SetViewStateAndShowLastPeal();
                break;
            case EUpdated:
                if (state == DucoWindowState::EViewMode)
                {
                    PopulateViewWithPeal(currentPeal->Id());
                }
                // don't add close, saving changes to existing performance is a problem if you do!
                break;
            }
        }
        break;

    case TObjectType::ETower:
        switch (eventId)
        {
        case EAdded:
            DatabaseGenUtils::AddTowerOption(towerEditor, id, allTowerNames, database);
            break;
        case EUpdated:
            generatingNewTower = true;
            DatabaseGenUtils::UpdateTowerOption(towerEditor, id, allTowerNames, database);
            generatingNewTower = false;
            break;
        case EDeleted:
            DatabaseGenUtils::RemoveTowerOption(towerEditor, id, allTowerNames);
            break;
        }
        break;

    case TObjectType::EMethod:
        switch (eventId)
        {
        case EAdded:
            DatabaseGenUtils::AddMethodOption(id, allMethodNames, currentPeal->NoOfBellsRung(database->Database()), database, methodEditor);
            break;
        case EUpdated:
            DatabaseGenUtils::UpdateMethodOption(id, allMethodNames, currentPeal->NoOfBellsRung(database->Database()), database, methodEditor);
            break;
        case EDeleted:
            DatabaseGenUtils::RemoveMethodOption(id, allMethodNames, currentPeal->NoOfBellsRung(database->Database()), methodEditor);
            break;
        }
        break;

    case TObjectType::EMethodSeries:
        switch (eventId)
        {
        case EAdded:
            DatabaseGenUtils::AddMethodSeriesOption(id, allSeriesNames, currentPeal->NoOfBellsRung(database->Database()), database, seriesEditor);
            break;
        case EUpdated:
            DatabaseGenUtils::UpdateMethodSeriesOption(id, allSeriesNames, currentPeal->NoOfBellsRung(database->Database()), database, seriesEditor);
            break;
        case EDeleted:
            DatabaseGenUtils::RemoveMethodSeriesOption(id, allSeriesNames, currentPeal->NoOfBellsRung(database->Database()), seriesEditor);
            break;
        }
        break;

    case TObjectType::EComposition:
        switch (eventId)
        {
        case EAdded:
            DatabaseGenUtils::AddCompositionOption(id, allCompositionNames, currentPeal->NoOfBellsRung(database->Database()), database, compositionEditor);
            break;
        case EUpdated:
            DatabaseGenUtils::UpdateCompositionOption(id, allCompositionNames, currentPeal->NoOfBellsRung(database->Database()), database, compositionEditor);
            break;
        case EDeleted:
            DatabaseGenUtils::RemoveCompositionOption(id, allCompositionNames, currentPeal->NoOfBellsRung(database->Database()), compositionEditor);
            break;
        }
        break;

    case TObjectType::EAssociationOrSociety:
        switch (eventId)
        {
        case EAdded:
            DatabaseGenUtils::AddAssociationOption(id, allAssociationNames, database, associationEditor);
            break;
        case EUpdated:
            DatabaseGenUtils::UpdateAssociationOption(id, allAssociationNames, database, associationEditor);
            break;
        case EDeleted:
            DatabaseGenUtils::RemoveAssociationOption(id, allAssociationNames, associationEditor);
            break;
        }
        break;

    default:
        break;
    }
}

void
PealUI::SetWindowTitle()
{
    this->Text = database->PerformanceString(true);
}

void
PealUI::CreateRingersList()
{
    ringerList = gcnew RingerList(database);
    ringerTabLayoutPanel->Controls->Add(this->ringerList, 0, 0);
    this->ringerList->Dock = System::Windows::Forms::DockStyle::Fill;
    this->ringerList->Location = System::Drawing::Point(9, 5);
    this->ringerList->Name = L"ringerList";
    this->ringerList->TabIndex = 43;
    this->ringerList->Text = L"Ringers";
    this->ringerList->AutoScroll = true;
    this->ringerList->dataChangedEventHandler += gcnew System::EventHandler(this, &PealUI::RingerObjectChanged);
}

void
PealUI::InitializePictureControl()
{
    this->pictureCtrl = (gcnew DucoUI::PictureControl(database));
    // 
    // pictureCtrl
    // 
    this->pictureCtrl->Dock = System::Windows::Forms::DockStyle::Fill;
    this->pictureCtrl->Location = System::Drawing::Point(39, 55);
    this->pictureCtrl->Name = L"pictureCtrl";
    this->pictureCtrl->Size = System::Drawing::Size(100, 100);
    this->pictureCtrl->TabIndex = 1;
    this->pictureCtrl->pictureEventHandler += gcnew System::EventHandler(this, &PealUI::PictureEvent);
    picturePage->Controls->Add(this->pictureCtrl);
}

void
PealUI::SetNewState()
{
    state = DucoWindowState::ENewMode;
    printCmd->Visible = false;
    previousBtn->Visible = false;
    back10Btn->Visible = false;
    forward10Btn->Visible = false;
    startBtn->Visible = false;
    endBtn->Visible = false;
    nextBtn->Visible = false;
    editBtn->Visible = false;
    pictureCtrl->SetEditMode(currentPeal->PictureId().ValidId());
    saveBtn->Visible = true;
    closeBtn->Visible = false;
    cancelBtn->Visible = true;
    findMethodBtn->Visible = true;
    findTowerBtn->Visible = true;
    createMissingRingersBtn->Visible = true;
    deleteBtn->Visible = false;
    pealSpeedBtn->Visible = false;
    bandStatsBtn->Visible = false;
    pealFeesBtn->Visible = false;

    towerEditor->Enabled = true;
    associationEditor->Enabled = true;
    changesEditor->ReadOnly = false;
    timeEditor->ReadOnly = false;
    methodEditor->Enabled = true;
    dateEditor->Enabled = true;
    composerEditor->Enabled = true;
    seriesEditor->Enabled = true;
    compositionEditor->Enabled = true;
    footnotesEditor->ReadOnly = false;
    handbellSelector->Enabled = true;
    wrongStagesSelector->Enabled = true;
    notable->Enabled = true;
    simulatedSound->Enabled = true;
    doubleHanded->Enabled = true;
    ringerList->SetState(state);
    ringSelector->Enabled = true;
    multiMethodEditor->ReadOnly = false;
    jointConductor->Enabled = true;
    silentConductor->Enabled = true;
    singleConductor->Enabled = true;
    uploadBtn->Visible = false;
    rwReference->ReadOnly = false;
    bellBoardReference->ReadOnly = false;
    openBellboardBtn->Visible = false;
    openRingingWorldBtn->Visible = false;
    notesEditor->ReadOnly = false;
    withdrawnEditor->Enabled = true;
    this->footnotesEditor->Size = System::Drawing::Size(304, 75);
    this->multiMethodEditor->Size = System::Drawing::Size(304, 75);
    this->footnotesEditor->Visible = true;
    this->footNotesLbl->Visible = true;
    this->multiMethodLbl->Visible = true;
    this->multiMethodEditor->Visible = true;
}

void
PealUI::SetEditState()
{
    state = DucoWindowState::EEditMode;
    printCmd->Visible = false;
    previousBtn->Visible = false;
    back10Btn->Visible = false;
    forward10Btn->Visible = false;
    startBtn->Visible = false;
    endBtn->Visible = false;
    nextBtn->Visible = false;
    editBtn->Visible = false;
    pictureCtrl->SetEditMode(currentPeal->PictureId().ValidId());
    saveBtn->Visible = true;
    closeBtn->Visible = false;
    cancelBtn->Visible = true;
    findMethodBtn->Visible = true;
    findTowerBtn->Visible = true;
    createMissingRingersBtn->Visible = true;
    deleteBtn->Visible = true;
    findMethodBtn->Enabled = true;
    pealSpeedBtn->Visible = false;
    bandStatsBtn->Visible = false;
    pealFeesBtn->Visible = false;

    ringerList->SetState(state);
    towerEditor->Enabled = true;
    associationEditor->Enabled = true;
    changesEditor->ReadOnly = false;
    timeEditor->ReadOnly = false;
    methodEditor->Enabled = true;
    dateEditor->Enabled = true;
    composerEditor->Enabled = true;
    seriesEditor->Enabled = true;
    compositionEditor->Enabled = true;
    footnotesEditor->ReadOnly = false;
    ringSelector->Enabled = true;
    handbellSelector->Enabled = true;
    wrongStagesSelector->Enabled = true;
    doubleHanded->Enabled = true;
    multiMethodEditor->ReadOnly = false;
    jointConductor->Enabled = true;
    silentConductor->Enabled = true;
    singleConductor->Enabled = true;
    viewOnlineBtn->Enabled = currentPeal->ValidWebsiteLink();
    viewOnlineBtn->Visible = currentPeal->ValidWebsiteLink();
    uploadBtn->Visible = false;
    rwReference->ReadOnly = false;
    bellBoardReference->ReadOnly = false;
    openBellboardBtn->Visible = false;
    openRingingWorldBtn->Visible = false;
    notesEditor->ReadOnly = false;
    withdrawnEditor->Enabled = true;
    notable->Enabled = true;
    simulatedSound->Enabled = true;
    this->footnotesEditor->Size = System::Drawing::Size(304, 75);
    this->multiMethodEditor->Size = System::Drawing::Size(304, 75);
    this->footnotesEditor->Visible = true;
    this->footNotesLbl->Visible = true;
    this->multiMethodLbl->Visible = true;
    this->multiMethodEditor->Visible = true;
}

void 
PealUI::SetViewState()
{
    state = DucoWindowState::EViewMode;
    printCmd->Visible = true;
    previousBtn->Visible = true;
    back10Btn->Visible = true;
    forward10Btn->Visible = true;
    startBtn->Visible = true;
    endBtn->Visible = true;
    nextBtn->Visible = true;
    editBtn->Visible = true;
    pictureCtrl->SetViewMode();
    saveBtn->Visible = false;
    closeBtn->Visible = true;
    cancelBtn->Visible = false;
    findMethodBtn->Visible = true;
    findMethodBtn->Enabled = true;
    findTowerBtn->Visible = true;
    createMissingRingersBtn->Visible = false;
    deleteBtn->Visible = false;
    pealSpeedBtn->Visible = true;
    bandStatsBtn->Visible = true;
    pealFeesBtn->Visible = true;
    notable->Enabled = false;
    simulatedSound->Enabled = false;
    closeBtn->Text = "Close";

    ringerList->SetState(state);
    towerEditor->Enabled = false;
    associationEditor->Enabled = false;
    changesEditor->ReadOnly = true;
    timeEditor->ReadOnly = true;
    methodEditor->Enabled = false;
    dateEditor->Enabled = false;
    composerEditor->Enabled = false;
    seriesEditor->Enabled = false;
    compositionEditor->Enabled = false;
    footnotesEditor->ReadOnly = true;
    handbellSelector->Enabled = false;
    wrongStagesSelector->Enabled = false;
    doubleHanded->Enabled = false;
    ringSelector->Enabled = false;
    multiMethodEditor->ReadOnly = true;
    jointConductor->Enabled = false;
    silentConductor->Enabled = false;
    singleConductor->Enabled = false;
    rwReference->ReadOnly = true;
    bellBoardReference->ReadOnly = true;
    openBellboardBtn->Visible = true;
    openRingingWorldBtn->Visible = true;
    notesEditor->ReadOnly = true;
    withdrawnEditor->Enabled = false;
}

void 
PealUI::PopulateViewWithPeal(const Duco::ObjectId& pealId)
{
    Duco::Peal findPeal;
    if (database->FindPeal(pealId, findPeal, false))
    {
        if (currentPeal)
            delete currentPeal;
        currentPeal = new Peal(findPeal);
        pictureCtrl->SetPeal(currentPeal);
        database->FindTower(currentPeal->TowerId(), *currentTower, true);
        PopulateView();
    }
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

void 
PealUI::ClearView()
{
    if (currentPeal == NULL)
    {
        currentPeal = new Peal();
        pictureCtrl->SetPeal(currentPeal);
    }
    currentPeal->Clear();
    currentPeal->SetDate(DucoUtils::ConvertDate(System::DateTime::Today));
    validationLabel->Text = "";
    ringTenorLbl->Text = "";
    PopulateView();
}

System::Void 
PealUI::PopulateIndexIndicator()
{
    System::String^ newString = "";
    if (currentPeal->Id() != -1)
    {
        newString = System::String::Format("{0:D}/{1:D}", currentPeal->Id().Id(), database->NumberOfObjects(TObjectType::EPeal));
    }
    pealIdLbl->Text = newString;
    pealIdLbl->Visible = currentPeal->Id() != -1;
}

System::Void 
PealUI::PopulateView()
{
    time_point<high_resolution_clock> start = high_resolution_clock::now();
    PopulateIndexIndicator();
    ringerList->SetPeal(currentPeal);
    SetConductorStates();
    System::Int32 oldTowerIndex = towerEditor->SelectedIndex;
    System::Int32 newTowerIndex = DatabaseGenUtils::FindIndex(allTowerNames, currentPeal->TowerId());
    if (oldTowerIndex == newTowerIndex)
    {   // The ring needs updating manually.
        ringSelector->SelectedIndex = DatabaseGenUtils::FindIndex(allRingNames, currentPeal->RingId());
    }
    else
    {   // This will update the ring automatically.
        towerEditor->SelectedIndex = newTowerIndex;
    }
    DatabaseGenUtils::GenerateAssociationOptions(associationEditor, allAssociationNames, database, currentPeal->AssociationId(), false);
    changesEditor->Text = Convert::ToString(currentPeal->NoOfChanges());
    timeEditor->Text = DucoUtils::PrintPealTime(currentPeal->Time(), false);
    generatingNewMethod = true;
    DatabaseGenUtils::GenerateMethodOptions(allMethodNames, methodEditor, database, currentPeal->MethodId(), currentPeal->NoOfBellsRung(database->Database()), false, currentPeal->WrongStage(), state != DucoWindowState::EAutoNewMode);
    generatingNewMethod = false;
    generatingNewSeries = true;
    DatabaseGenUtils::GenerateMethodSeriesOptions(seriesEditor, allSeriesNames, database, currentPeal->SeriesId(), currentPeal->NoOfBellsRung(database->Database()), true);
    seriesGroup->Visible = database->Database().MethodsDatabase().MethodIsSpliced(currentPeal->MethodId()) || currentPeal->SeriesId().ValidId();
    generatingNewSeries = false;
    generatingNewComposition = true;
    DatabaseGenUtils::GenerateCompositionOptions(compositionEditor, allCompositionNames, database, currentPeal->CompositionId(), currentPeal->NoOfBellsRung(database->Database()), true);
    generatingNewComposition = false;
    System::DateTime^ akaDate = DucoUtils::ConvertDate(currentPeal->Date());
    dateEditor->Value = *akaDate;
    composerEditor->Text = DucoUtils::ConvertString(currentPeal->Composer());
    notable->Checked = currentPeal->Notable();
    simulatedSound->Checked = currentPeal->SimulatedSound();

    footnotesEditor->Text = DucoUtils::ConvertString(currentPeal->Footnotes());
    multiMethodEditor->Text = DucoUtils::ConvertString(currentPeal->MultiMethods());
    handbellSelector->Checked = currentPeal->Handbell();
    wrongStagesSelector->Checked = currentPeal->WrongStage();
    doubleHanded->Checked = currentPeal->DoubleHanded();
    viewOnlineBtn->Enabled = currentPeal->ValidWebsiteLink();
    viewOnlineBtn->Visible = currentPeal->ValidWebsiteLink();
    uploadBtn->Visible = !currentPeal->Withdrawn() && !currentPeal->ValidBellBoardId() && database->Database().Settings().BellBoardURL().length() > 0 && database->Database().Settings().BellBoardViewSuffixURL().length() > 0;
    uploadBtn->Enabled = !currentPeal->Withdrawn() && !currentPeal->ValidBellBoardId() && database->Database().Settings().BellBoardURL().length() > 0 && database->Database().Settings().BellBoardViewSuffixURL().length() > 0;
    rwReference->Text = DucoUtils::ConvertString(currentPeal->RingingWorldReference());
    bellBoardReference->Text = DucoUtils::ConvertString(currentPeal->BellBoardId());
    notesEditor->Text = DucoUtils::ConvertString(currentPeal->Notes());
    withdrawnEditor->Checked = currentPeal->Withdrawn();
    pictureCtrl->SetPicture(currentPeal->PictureId(), false);
    UpdateValidationLabel();
    if (currentPeal->ContainsFootnotes() && !currentPeal->ContainsMultiMethods())
    {
        this->footnotesEditor->Size = System::Drawing::Size(304, 150);
        this->multiMethodEditor->Size = System::Drawing::Size(304, 0);
        this->footnotesEditor->Visible = true;
        this->footNotesLbl->Visible = true;
        this->multiMethodLbl->Visible = false;
        this->multiMethodEditor->Visible = false;
    }
    else if (!currentPeal->ContainsFootnotes() && currentPeal->ContainsMultiMethods())
    {
        this->footnotesEditor->Size = System::Drawing::Size(304, 0);
        this->multiMethodEditor->Size = System::Drawing::Size(304, 150);
        this->footnotesEditor->Visible = false;
        this->footNotesLbl->Visible = false;
        this->multiMethodLbl->Visible = true;
        this->multiMethodEditor->Visible = true;
    }
    else
    {
        this->footnotesEditor->Size = System::Drawing::Size(304, 75);
        this->multiMethodEditor->Size = System::Drawing::Size(304, 75);
        this->footnotesEditor->Visible = true;
        this->footNotesLbl->Visible = true;
        this->multiMethodLbl->Visible = true;
        this->multiMethodEditor->Visible = true;
    }
    dataChanged = false;
    DUCOUILOGDEBUGWITHTIME(start, "PealUI::PopulateView took");
}

System::Void
PealUI::DownloadComplete()
{
    populatingFromDownload = false;
    UpdateValidationLabel();
}


System::Void
PealUI::UpdateValidationLabel()
{
    if (!populatingFromDownload)
    {
        if (currentPeal->Valid(database->Database(), false, true))
        {
            validationLabel->Text = "";
        }
        else
        {
            validationLabel->Text = DucoUtils::ConvertString(currentPeal->ErrorString(database->Settings(), false));
        }
    }
}

System::Void 
PealUI::GenerateComposerOptions()
{
    if (database->Config().DisableFeaturesForPerformance())
    {
        composerEditor->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::None;
    }

    std::map<std::wstring, Duco::PealLengthInfo> composers;
    Duco::StatisticFilters filters(database->Database());
    database->GetAllComposersByPealCount(composers, false, filters);

    std::map<std::wstring, Duco::PealLengthInfo>::const_iterator it = composers.begin();
    while (it != composers.end())
    {
        composerEditor->Items->Add(DucoUtils::ConvertString(it->first));
        ++it;
    }
}

System::Void 
PealUI::HandbellSelector_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::ENewMode || state == DucoWindowState::EEditMode || state == DucoWindowState::EAutoNewMode)
    {
        currentPeal->SetHandbell(handbellSelector->Checked);
        ringerList->SetHandbell(currentPeal->Handbell());

        dataChanged = true;
        ringerList->UpdateNoOfBells(-1);
        DatabaseGenUtils::GenerateMethodOptions(allMethodNames, methodEditor, database, currentPeal->MethodId(), currentPeal->NoOfBellsRung(database->Database()), false, currentPeal->WrongStage(), true);
        DatabaseGenUtils::GenerateMethodSeriesOptions(seriesEditor, allSeriesNames, database, currentPeal->SeriesId(), currentPeal->NoOfBellsRung(database->Database()), false);
        DatabaseGenUtils::GenerateCompositionOptions(compositionEditor, allCompositionNames, database, currentPeal->CompositionId(), currentPeal->NoOfBellsRung(database->Database()), false);
        UpdateValidationLabel();
    }
}

System::Void 
PealUI::WrongStagesSelector_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::ENewMode || state == DucoWindowState::EEditMode || state == DucoWindowState::EAutoNewMode)
    {
        currentPeal->SetWrongStage(wrongStagesSelector->Checked);

        dataChanged = true;
        DatabaseGenUtils::GenerateMethodOptions(allMethodNames, methodEditor, database, currentPeal->MethodId(), currentPeal->NoOfBellsRung(database->Database()), false, currentPeal->WrongStage(), true);
        DatabaseGenUtils::GenerateMethodSeriesOptions(seriesEditor, allSeriesNames, database, currentPeal->SeriesId(), currentPeal->NoOfBellsRung(database->Database()), false);
        DatabaseGenUtils::GenerateCompositionOptions(compositionEditor, allCompositionNames, database, currentPeal->CompositionId(), currentPeal->NoOfBellsRung(database->Database()), false);
        UpdateValidationLabel();
    }
}

System::Void 
PealUI::DoubleHanded_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::ENewMode || state == DucoWindowState::EEditMode || state == DucoWindowState::EAutoNewMode)
    {
        currentPeal->SetDoubleHanded(doubleHanded->Checked);
        UpdateValidationLabel();
        dataChanged = true;
    }
}

System::Void
PealUI::notable_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
{
    if (state == DucoWindowState::ENewMode || state == DucoWindowState::EEditMode || state == DucoWindowState::EAutoNewMode)
    {
        currentPeal->SetNotable(notable->Checked);
        dataChanged = true;
    }
}

System::Void
PealUI::SimulatedSound_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
{
    if (state == DucoWindowState::ENewMode || state == DucoWindowState::EEditMode || state == DucoWindowState::EAutoNewMode)
    {
        currentPeal->SetSimulatedSound(simulatedSound->Checked);
        dataChanged = true;
    }
}


System::Void 
PealUI::PreviousPeal(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::EViewMode)
    {
        Duco::ObjectId pealId = currentPeal->Id();
        if (!pealId.ValidId())
        {
            EndBtn_Click(sender, e);
        }
        else if (database->NextPeal(pealId, false, false))
        {
            PopulateViewWithPeal(pealId);
        }
        else
        {
            SoundUtils::PlayErrorSound();
        }
    }
}

System::Void
PealUI::NextPeal(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::EViewMode)
    {
        Duco::ObjectId pealId = currentPeal->Id();
        if (!pealId.ValidId())
        {
            StartBtn_Click(sender, e);
        }
        else if (database->NextPeal(pealId, true, false))
        {
            PopulateViewWithPeal(pealId);
        }
        else
        {
            SoundUtils::PlayErrorSound();
        }
    }
}

System::Void 
PealUI::Back10Btn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::EViewMode)
    {
        Duco::ObjectId pealId = currentPeal->Id();
        if (!pealId.ValidId())
        {
            StartBtn_Click(sender, e);
        }
        else if (database->ForwardMultiplePeals(pealId, false))
        {
            PopulateViewWithPeal(pealId);
        }
        else
        {
            SoundUtils::PlayErrorSound();
        }
    }
}

System::Void
PealUI::Forward10Btn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::EViewMode)
    {
        Duco::ObjectId pealId = currentPeal->Id();
        if (!pealId.ValidId())
        {
            StartBtn_Click(sender, e);
        }
        else if (database->ForwardMultiplePeals(pealId, true))
        {
            PopulateViewWithPeal(pealId);
        }
        else
        {
            SoundUtils::PlayErrorSound();
        }
    }
}

System::Void
PealUI::PealFeesBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    PealFeesUI^ dialog = gcnew PealFeesUI(database, currentPeal->Id());
    dialog->ShowDialog();
}

System::Void
PealUI::EndBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId pealId;
    database->LastPeal(pealId);
    PopulateViewWithPeal(pealId);
}

System::Void
PealUI::StartBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId pealId;
    database->FirstPeal(pealId, false);
    PopulateViewWithPeal(pealId);
}

System::Void
PealUI::EditBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (database->StartEdit(TObjectType::EPeal, currentPeal->Id()))
        SetEditState();
    else
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void 
PealUI::DeleteBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (MessageBox::Show(this, "Are you sure you want to delete this peal?", "Warning", ::MessageBoxButtons::OKCancel, ::MessageBoxIcon::Warning) == ::DialogResult::OK)
    {
        Duco::ObjectId currentPealId = currentPeal->Id();
        database->NextPeal(currentPealId, true, false);
        database->DeletePeal(currentPeal->Id(), false);
        SetViewState();
        PopulateViewWithPeal(currentPealId);
    }
}

System::Void
PealUI::PrintCmd_Click(System::Object^  sender, System::EventArgs^  e)
{
    DucoUIUtils::Print(this, sender, gcnew System::Drawing::Printing::PrintPageEventHandler( this, &PealUI::PrintPeal ), "peal", database->Database().Settings().UsePrintPreview(), false);
}

System::Void
PealUI::PrintPeal(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args)
{
    PrintDocument^ printDoc = static_cast<PrintDocument^>(sender);
    PrintPealUtil printUtil(database, args);
    printUtil.SetObjects(currentPeal, currentTower);
    printUtil.printObject(args, printDoc->PrinterSettings);
}

System::Void 
PealUI::CopyToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    System::String^ pealString = DucoUtils::ConvertString(currentPeal->AssociationName(database->Database()));
    pealString += KNewlineChar;
    pealString += DucoUtils::ConvertString(currentTower->City());
    bool printCounty (currentTower->County().length() > 0);
    if (!printCounty)
        pealString += ".";
    else
        pealString += ", ";
    if (printCounty)
    {
        pealString += DucoUtils::ConvertString(currentTower->County());
        pealString += ".";
    }
    pealString += KNewlineChar;
    pealString += DucoUtils::ConvertString(currentTower->Name());
    const Ring* const theRing = currentTower->FindRing(currentPeal->RingId());
    if (theRing != NULL)
    {
        pealString += " (";
        pealString += DucoUtils::ConvertString(theRing->TenorDescription());
        pealString += ")";
    }
    pealString += KNewlineChar;
    pealString += "On " + DucoUtils::ConvertString(currentPeal->Date(), true);
    pealString += " in ";
    pealString += DucoUtils::PrintPealTime(currentPeal->Time(), true) + KNewlineChar;
    pealString += DucoUtils::ConvertString(DucoEngineUtils::ToString(currentPeal->NoOfChanges(), true));
    Method theMethod;
    if (database->FindMethod(currentPeal->MethodId(), theMethod, false))
    {
        pealString += " ";
        pealString += DucoUtils::ConvertString(theMethod.FullName(database->Database()));
    }
    pealString += KNewlineChar;

    if (currentPeal->ContainsComposer())
    {
        pealString += "Composed by: ";
        pealString += DucoUtils::ConvertString(currentPeal->Composer());
        pealString += KNewlineChar;
    }
    if (currentPeal->ContainsMultiMethods())
    {
        pealString += DucoUtils::ConvertString(currentPeal->MultiMethods());
        pealString += KNewlineChar;
    }
    for (unsigned int bellNo(1); bellNo <= currentPeal->NoOfBellsRung(database->Database()); ++bellNo)
    {
        if (bellNo == 1)
        {
            pealString += "Treble\t";
        }
        else if (bellNo == currentPeal->NoOfBellsRung(database->Database()))
        {
            pealString += "Tenor\t";
        }
        else
        {
            pealString += Convert::ToString(bellNo);
            pealString += "\t";
        }
        Duco::ObjectId ringerId;
        if (currentPeal->RingerId(bellNo, false, ringerId))
        {   
            Duco::Ringer theRinger;
            if (database->FindRinger(ringerId, theRinger, false))
            {
                pealString += DucoUtils::ConvertString(theRinger.FullName(currentPeal->Date(), database->Settings().PrintSurnameFirst()));
            }

            if (currentPeal->ContainsConductor(ringerId))
            {
                pealString += " (C)";
            }
            pealString += KNewlineChar;
        }
        if (currentPeal->RingerId(bellNo, true, ringerId))
        {
            Ringer theStrapper;
            if (database->FindRinger(ringerId, theStrapper, false))
            {
                pealString += DucoUtils::ConvertString(theStrapper.FullName(currentPeal->Date(), database->Settings().PrintSurnameFirst()));
            }

            if (currentPeal->ContainsConductor(ringerId))
            {
                pealString += " (C)";
            }
            pealString += KNewlineChar;
        }
    }
    if (currentPeal->ContainsFootnotes())
    {
        pealString += DucoUtils::ConvertString(currentPeal->Footnotes());
    }

    // comments & counts
    Boolean foundLink = false;
    if (currentPeal->ValidRingingWorldLink())
    {
        foundLink = true;
        pealString += KNewlineChar + "Ringing world: " + DucoUtils::ConvertString(currentPeal->RingingWorldReference());
    }
    if (currentPeal->ValidBellBoardId())
    {
        foundLink = true;
        pealString += KNewlineChar + "Bellboard id: " + DucoUtils::ConvertString(currentPeal->BellBoardId());
    }
    if (!foundLink)
    {
        pealString += KNewlineChar + DucoUtils::ConvertString(currentPeal->FindReference());
    }
    pealString += KNewlineChar + "Peal number ";
    pealString += DucoUtils::ConvertString(currentPeal->Id());
    if (currentPeal->ContainsConductor(database->Settings().DefaultRinger()))
    {
        pealString += " (";
        pealString += DucoUtils::ConvertString(DucoEngineUtils::ToString(database->NoOfPealsAsConductor(database->Settings().DefaultRinger(), currentPeal->Date()), true));
        pealString += ")";
    }
    pealString += "; In this tower ";
    
    pealString += DucoUtils::ConvertString(DucoEngineUtils::ToString(database->NoOfPealsInTower(currentPeal->TowerId(), currentPeal->Id()), true));
    Clipboard::SetDataObject(pealString);
}

System::Void
PealUI::ViewOnlineToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (currentPeal->ValidWebsiteLink())
    {
        System::String^ webSite = DucoUtils::ConvertString(currentPeal->WebsiteLink(database->Settings()));
        System::Diagnostics::Process::Start(webSite);
    }
}

System::Void
PealUI::uploadBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    UploadPealUI^ uploadUI = gcnew UploadPealUI(database, *currentPeal);
    uploadUI->MdiParent = this->MdiParent;
    uploadUI->Show();
    Close();
}

System::Void
PealUI::openBellboardBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::EViewMode)
    {
        if (currentPeal->ValidBellBoardId())
        {
            System::Diagnostics::Process::Start(DucoUtils::ConvertString(currentPeal->BellBoardLink(database->Settings())));
        }
        else
        {
            SoundUtils::PlayErrorSound();
        }
    }
}

System::Void
PealUI::openRingingWorldBtn_Click(System::Object^ sender, System::EventArgs^ e) 
{
    if (state == DucoWindowState::EViewMode)
    {
        if (currentPeal->ValidRingingWorldLink())
        {
            System::Diagnostics::Process::Start(DucoUtils::ConvertString(currentPeal->RingingWorldLink(database->Settings())));
        }
        else
        {
            SoundUtils::PlayErrorSound();
        }
    }
}


System::Void
PealUI::StatsBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    PealStatsUI^ statsUI = gcnew PealStatsUI(database, currentPeal->Id());
    statsUI->MdiParent = this->MdiParent;
    statsUI->Show();
}

System::Void
PealUI::BandStatsBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    DucoUIUtils::Print(this, sender, gcnew System::Drawing::Printing::PrintPageEventHandler(this, &PealUI::PrintBandStatsInTower), "band", database->Database().Settings().UsePrintPreview(), false);
}

System::Void
PealUI::PrintBandStatsInTower(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args)
{
    PrintDocument^ printDoc = static_cast<PrintDocument^>(sender);
    if (printBandStatsUtil == nullptr)
    {
        printBandStatsUtil = gcnew PrintBandSummaryUtil(database, args, currentPeal->Id());
        printBandStatsUtil->printObject(args, printDoc->PrinterSettings);
    }
    if (printBandStatsUtil->morePages)
    {
        args->HasMorePages = true;
    }
    else
    {
        printBandStatsUtil = nullptr;
    }
}

System::Void
PealUI::CancelBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    switch (state)
    {
    case DucoWindowState::ENewMode:
    case DucoWindowState::EAutoNewMode:
        Close();
        break;
    default:
        if (dataChanged)
        {
            if (DucoUtils::ConfirmLooseChanges("Performance", this))
                return;
            // set data changed to false, so the closing event will not prompt the user again
            dataChanged = false;
        }
        if (state == DucoWindowState::EEditMode)
            database->CancelEdit(TObjectType::EPeal, currentPeal->Id());
        SetViewState();
        PopulateViewWithPeal(currentPeal->Id());
        break;
    }
}

System::Void 
PealUI::CloseBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    Close();
}

System::Void
PealUI::SaveBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (associationEditor->SelectedIndex == -1)
    {
        CreateMissingAssociation(sender, e);
    }
    if (towerEditor->SelectedIndex == -1)
    {
        CreateMissingTower(sender, e);
    }
    if (methodEditor->SelectedIndex == -1)
    {
        CreateMissingMethod(sender, e);
    }
    CreateMissingRingersBtn_Click(sender, e);

    bool dialogAlreadyShown (false);
    bool pealValid = currentPeal->ValidPealDetails(database->Database(), false, true);
    if (pealValid)
    {
        pealValid = currentPeal->ValidPealRingers(database->Database(), false, false);
        std::bitset<KMaxErrorBitsetSize> test = currentPeal->ErrorCode();
        test.reset(EPeal_DuplicateRingers);
        if (!pealValid && test.any())
        {
            if (/*database->Settings().AllowErrors() &&*/
                MessageBox::Show(this, DucoUtils::ConvertString(currentPeal->ErrorString(database->Database().Settings(), true)), "Please check all the ringers are entered correctly", MessageBoxButtons::OKCancel) == ::DialogResult::OK)
            {
                    pealValid = true;
            }
            else
            {
                tabControl1->SelectedIndex = 1;
                MessageBox::Show(this, "Please check all the ringers are valid", "Please check all the data is entered correctly");
                pealValid = false;
            }
            dialogAlreadyShown = true;
        }
        else
        {
            pealValid = currentPeal->ValidPealConductors(false, false);
            if (!pealValid)
            {
                if (/*database->Settings().AllowErrors() && */
                            MessageBox::Show(this, "Please check that the number of conductors is valid, ignore?", "Please check all the data is entered correctly", MessageBoxButtons::OKCancel) == ::DialogResult::OK)
                {
                    pealValid = true;
                }
                else
                {
                    tabControl1->SelectedIndex = 1;
                    pealValid = false;
                }
                dialogAlreadyShown = true;
            }
            bool asStrapper (false);
            if (!currentPeal->ContainsDefaultRinger(database->Settings(), asStrapper))
            {
                if (dialogAlreadyShown || MessageBox::Show(this, "This peal doesn't contain the default ringer, do you want to continue?", "Default ringer missing", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == ::DialogResult::No)
                    return;
            }
            std::map<Duco::ObjectId, unsigned int> ringers;
            if (currentPeal->DuplicateRingers(ringers))
            {
                if (!currentPeal->DoubleHanded())
                {
                    Duco::Ringer theRinger;
                    if (ringers.size()==1 && database->FindRinger(ringers.begin()->first, theRinger, false))
                    {
                        System::String^ ringerNameStr = DucoUtils::ConvertString(theRinger.FullName(database->Settings().LastNameFirst()));
                        System::String^ dialogText = "This peal contains " + ringerNameStr + " twice, do you want to continue?";
                        if (MessageBox::Show(this, dialogText, "Duplicate ringers", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == ::DialogResult::No)
                            return;
                    }
                    else
                    {
                        if (MessageBox::Show(this, "This peal contains duplicate ringers, do you want to continue?", "Duplicate ringers", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == ::DialogResult::No)
                            return;
                    }
                }
            }
            else if (currentPeal->DoubleHanded() && ringers.size() == 0)
            {
                if (MessageBox::Show(this, "This peal has Double Handed marked on the Other tab, but no ringers doing so, do you want to continue?", "No double handed ringers", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == ::DialogResult::No)
                    return;
            }
        }
    }
    else
    {
        tabControl1->SelectedIndex = 0;
        std::wstring errorString = currentPeal->ErrorString(database->Database().Settings(), database->Config().IncludeWarningsInValidation());
        if (errorString.length() > 0)
        {
            dialogAlreadyShown = true;
            if (MessageBox::Show(this, DucoUtils::ConvertString(errorString) + "\nDo you want to continue?", "Peal contains errors", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == ::DialogResult::No)
                return;
            else
                pealValid = true;
        }
    }

    if (!pealValid)
    {
        if (!dialogAlreadyShown)
        {
            if (DucoUtils::ShowInvalidPealDataDialog(database->Settings(), *currentPeal, this))
                return;
            else
                pealValid = true;
        }
        else
            return;
    }
    if (state == DucoWindowState::ENewMode || state == DucoWindowState::EAutoNewMode)
    {
        // Check peal is unique;
        if (!database->UniqueObject(*currentPeal, TObjectType::EPeal))
        {
            if (MessageBox::Show(this, "This performance is very similar to another on the same day, do you want to continue?", "Duplicate performance", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == ::DialogResult::No)
                return;
        }
    }

    if (dataChanged)
    {
        switch (state)
        {
            case DucoWindowState::EEditMode:
                database->UpdatePeal(*currentPeal, true);
                dataChanged = false;
                break;
            case DucoWindowState::ENewMode:
            case DucoWindowState::EAutoNewMode:
            {
                Duco::ObjectId newPealId = database->AddPeal(*currentPeal);
                if (!newPealId.ValidId())
                    return;
                database->FindPeal(newPealId, *currentPeal, false);
                dataChanged = false;
                break;
            }
        }
    }
    else
    {
        database->CancelEdit(Duco::TObjectType::EPeal, currentPeal->Id());
    }
    state = DucoWindowState::EViewMode;
    SetViewState();
    PopulateView();
}

System::Void 
PealUI::TowerChanged(System::Object^  sender, System::EventArgs^  e)
{
    Duco::ObjectId newTowerId = DatabaseGenUtils::FindId(allTowerNames, towerEditor->SelectedIndex);
    if (newTowerId.ValidId())
    {
        database->FindTower(newTowerId, *currentTower, false);
    }
    switch (state)
    {
    case DucoWindowState::ENewMode:
    case DucoWindowState::EAutoNewMode:
    case DucoWindowState::EEditMode:
        {
        currentPeal->SetTowerId(newTowerId);
        if (!updatingExistingTower)
            currentPeal->SetRingId(-1);
        DatabaseGenUtils::GenerateRingOptions(ringSelector, ringTenorLbl, allRingNames, database, currentPeal->TowerId(), currentPeal->RingId(), currentPeal->NoOfBellsRung(database->Database()), false);
        towerEditor->BackColor = SystemColors::ControlLightLight;
        dataChanged = true;
        UpdateValidationLabel();
        }
        break;

    case DucoWindowState::EViewMode:
        DatabaseGenUtils::GenerateRingOptions(ringSelector, ringTenorLbl, allRingNames, database, currentPeal->TowerId(), currentPeal->RingId(), currentPeal->NoOfBellsRung(database->Database()), false);
        break;

    default:
        break;
    } 
}

System::Void 
PealUI::TowerTextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (!generatingNewTower && (state == DucoWindowState::ENewMode || state == DucoWindowState::EEditMode || state == DucoWindowState::EAutoNewMode))
    {
        currentPeal->SetTowerId(-1);
        currentPeal->SetRingId(-1);
        ringSelector->Items->Clear();
        ringSelector->Text = "";
        towerEditor->BackColor = KErrorColour;
        UpdateValidationLabel();
        dataChanged = true;
    }
}

System::Void 
PealUI::RingChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::ENewMode || state == DucoWindowState::EEditMode || state == DucoWindowState::EAutoNewMode)
    {
        const Ring* const oldRing = currentTower->FindRing(currentPeal->RingId());
        Duco::ObjectId newRingId = DatabaseGenUtils::FindId(allRingNames,ringSelector->SelectedIndex);

        const Ring* const newRing = currentTower->FindRing(newRingId);
        currentPeal->SetRingId(newRingId);

        unsigned int noOfBellsRung = currentPeal->NoOfBellsRung(database->Database());
        if (oldRing == nullptr || (newRing->NoOfBells() != oldRing->NoOfBells()))
        {
            ringerList->UpdateNoOfBells(newRing->NoOfBells());
            DatabaseGenUtils::GenerateMethodOptions(allMethodNames, methodEditor, database, currentPeal->MethodId(), noOfBellsRung, false, currentPeal->WrongStage(), false);
            DatabaseGenUtils::GenerateMethodSeriesOptions(seriesEditor, allSeriesNames, database, currentPeal->SeriesId(), noOfBellsRung, false);
            DatabaseGenUtils::GenerateCompositionOptions(compositionEditor, allCompositionNames, database, currentPeal->CompositionId(), noOfBellsRung, false);
        }
        if (noOfBellsRung == newRing->NoOfBells())
        {
            ringSelector->BackColor = SystemColors::ControlLightLight;
        }
        else
        {
            ringSelector->BackColor = KErrorColour;
        }
        ringTenorLbl->Text = DucoUtils::ConvertString(newRing->TenorDescription());
        UpdateValidationLabel();
        dataChanged = true;
        findMethodBtn->Enabled = true;
    }
}

System::Void
PealUI::RingTextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::ENewMode || state == DucoWindowState::EEditMode || state == DucoWindowState::EAutoNewMode)
    {
        if (ringSelector->SelectedIndex == -1)
        {
            currentPeal->SetRingId(-1);
            ringSelector->BackColor = KErrorColour;
            dataChanged = true;
            findMethodBtn->Enabled = false;
            ringTenorLbl->Text = "";
            UpdateValidationLabel();
        }
    }
}

System::Void
PealUI::NumericEditor_KeyDown( Object^ sender, System::Windows::Forms::KeyEventArgs^ e )
{
    // Initialize the flag to false.
    nonNumberEntered = true;
    Keys currentKeys = Control::ModifierKeys;

    if (DucoUIUtils::NumericKey(e, currentKeys))
        nonNumberEntered = false;
    else if (e->KeyCode == Keys::OemSemicolon)
    {
        if (sender == timeEditor && currentKeys == Keys::Shift)
            nonNumberEntered = false;
    }
    else if (e->KeyCode == Keys::V)
    {
        if (currentKeys == Keys::Control)
        {
            IDataObject^ iData = Clipboard::GetDataObject();
            if ( iData->GetDataPresent( DataFormats::Text ) )
            {
                nonNumberEntered = false;
            }
        }
        // Not resetting noNumberEntered on purpose
    }
    else if (e->KeyCode == Keys::C)
    { // Copy
        if (currentKeys == Keys::Control)
        {
            nonNumberEntered = false;
        }
    }
    else if (e->KeyCode == Keys::OemPeriod || e->KeyCode == Keys::Decimal || e->KeyCode == Keys::Oem5 || e->KeyCode == Keys::Oem2)
    {
        if (sender == rwReference)
            nonNumberEntered = false;
    }
}

System::Void
PealUI::NumericEditor_KeyPressed(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e)
{
    // Check for the flag being set in the KeyDown event.
    if ( nonNumberEntered == true )
    {
        // Stop the character from being entered into the control since it is non-numerical.
        e->Handled = true;
    }
}

System::Void
PealUI::ChangesEditor_TextChanged(System::Object^  /*sender*/, System::EventArgs^  /*e*/)
{
    if (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode || state == DucoWindowState::EAutoNewMode)
    {
        currentPeal->SetChanges(DucoUtils::ConvertString(changesEditor->Text));
        UpdateValidationLabel();
        dataChanged = true;
    }
}

System::Void
PealUI::TimeEditor_TextChanged(System::Object^  /*sender*/, System::EventArgs^  /*e*/)
{
    if (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode || state == DucoWindowState::EAutoNewMode)
    {
        PerformanceTime newPealTime (DucoUtils::ToTime(timeEditor->Text));
        currentPeal->SetTime(newPealTime);
        UpdateValidationLabel();
        dataChanged = true;
    }
}

System::Void
PealUI::MultiMethodEditor_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode || state == DucoWindowState::EAutoNewMode)
    {
        std::wstring newMultiMethods;
        DucoUtils::ConvertString(multiMethodEditor->Text, newMultiMethods);
        currentPeal->SetMultiMethods(newMultiMethods);
        dataChanged = true;
    }
}

System::Void
PealUI::FootnotesEditor_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode || state == DucoWindowState::EAutoNewMode)
    {
        std::wstring newFootnotes;
        DucoUtils::ConvertString(footnotesEditor->Text, newFootnotes);
        currentPeal->SetFootnotes(newFootnotes);
        UpdateValidationLabel();
        dataChanged = true;
    }
}

System::Void
PealUI::DateEditor_ValueChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode || state == DucoWindowState::EAutoNewMode)
    {
        currentPeal->SetDate(DucoUtils::ConvertDate(dateEditor->Value));
        UpdateValidationLabel();
        ringerList->PealDateChanged(currentPeal->Date());
        dataChanged = true;
    }
}

System::Void
PealUI::ComposerEditor_ValueChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode || state == DucoWindowState::EAutoNewMode)
    {
        std::wstring newComposer;
        DucoUtils::ConvertString(composerEditor->Text, newComposer);
        currentPeal->SetComposer(newComposer);
        dataChanged = true;
    }
}

System::Void
PealUI::SeriesEditor_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    if ( (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode || state == DucoWindowState::EAutoNewMode) && !generatingNewSeries)
    {
        Duco::ObjectId newSeriesId = DatabaseGenUtils::FindId(allSeriesNames, seriesEditor->SelectedIndex);
        currentPeal->SetSeriesId(newSeriesId);
        dataChanged = true;
    }
}

System::Void
PealUI::SeriesEditor_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if ( (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode || state == DucoWindowState::EAutoNewMode) && !generatingNewSeries)
    {
        if (seriesEditor->SelectedIndex == -1)
        {
            currentPeal->SetSeriesId(-1);
            dataChanged = true;
        }
    }
}

System::Void
PealUI::CompositionEditor_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    if ( (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode || state == DucoWindowState::EAutoNewMode) && !generatingNewComposition)
    {
        Duco::ObjectId newCompositionId = DatabaseGenUtils::FindId(allCompositionNames,compositionEditor->SelectedIndex);
        currentPeal->SetCompositionId(newCompositionId);
        dataChanged = true;
    }
}

System::Void
PealUI::CompositionEditor_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if ( (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode || state == DucoWindowState::EAutoNewMode) && !generatingNewComposition)
    {
        if (compositionEditor->SelectedIndex == -1)
        {
            currentPeal->SetCompositionId(-1);
            dataChanged = true;
        }
    }
}

System::Void
PealUI::AssociationEditor_ValueChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode || state == DucoWindowState::EAutoNewMode)
    {
        currentPeal->SetAssociationId(DatabaseGenUtils::FindId(allAssociationNames, associationEditor->SelectedIndex));
        if (!currentPeal->AssociationId().ValidId())
        {
            associationEditor->BackColor = KErrorColour;
        }
        UpdateValidationLabel();
        dataChanged = true;
    }
}

System::Void
PealUI::AssociationEditor_SelectedIndexChanged(System::Object^ editor, System::EventArgs^  e)
{
    if (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode || state == DucoWindowState::EAutoNewMode)
    {
        Duco::ObjectId associationId = DatabaseGenUtils::FindId(allAssociationNames, associationEditor->SelectedIndex);
        currentPeal->SetAssociationId(associationId);
        associationEditor->BackColor = SystemColors::ControlLightLight;
        UpdateValidationLabel();
        dataChanged = true;
    }
}

System::Void
PealUI::CreateMissingAssociation(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::ENewMode || state == DucoWindowState::EEditMode || state == DucoWindowState::EAutoNewMode)
    {
        if (!currentPeal->AssociationId().ValidId() && associationEditor->Text->Length > 0)
        {
            Duco::ObjectId associationId = database->AddAssociation(associationEditor->Text);
            if (associationId.ValidId())
            {
                currentPeal->SetAssociationId(associationId);
            }
        }
    }
}

System::Void
PealUI::CreateMissingTower(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::ENewMode || state == DucoWindowState::EEditMode || state == DucoWindowState::EAutoNewMode)
    {
        if (towerEditor->Text->Length > 0)
        {
            if (towerEditor->SelectedIndex == -1)
            {
                Duco::ObjectId suggestedTowerId = database->SuggestTower(towerEditor->Text, currentPeal->NoOfBellsRung(database->Database()));
                if (suggestedTowerId != -1)
                {
                    Tower suggestedTower;
                    if (database->FindTower(suggestedTowerId, suggestedTower, false))
                    {
                        tabControl1->SelectedIndex = 0;
                        switch (MessageBox::Show(this, "A similar tower already exists: " + DucoUtils::ConvertString(suggestedTower.FullName()) + "\n Would you like to use this instead?", "Tower suggestion", MessageBoxButtons::YesNoCancel, MessageBoxIcon::Warning))
                        {
                            case ::DialogResult::Yes:
                            {
                                // Suggested tower accepted, save it and return.
                                System::Int32 newTowerIndex = DatabaseGenUtils::FindIndex(allTowerNames, suggestedTower.Id());
                                towerEditor->SelectedIndex = newTowerIndex;
                                return;
                            }
                            case ::DialogResult::Cancel:
                                return;
                            case ::DialogResult::No:
                            default:
                                break;
                        }
                    }
                }
                generatingNewTower = true;
                System::String^ tempNewMethodName = "";
                if (methodEditor->SelectedIndex == -1)
                {
                    tempNewMethodName = methodEditor->Text;
                }
                Duco::ObjectId newTowerId = TowerUI::CreateNewTower(database, towerEditor->Text, ringSelector->Text, downloadTenorWeight, downloadTenorKey, downloadTowerbaseId, downloadDoveId, currentPeal->NoOfBellsRung(database->Database()), currentPeal->Handbell());
                if (newTowerId.ValidId())
                {
                    downloadTenorWeight = "";
                    downloadTenorKey = "";
                    downloadTowerbaseId = "";
                    downloadDoveId = "";
                    currentPeal->SetTowerId(newTowerId);
                    DatabaseGenUtils::AddTowerOption(towerEditor, newTowerId, allTowerNames, database);
                    towerEditor->SelectedIndex = DatabaseGenUtils::FindIndex(allTowerNames, newTowerId);
                }
                if (tempNewMethodName->Length > 0)
                {
                    methodEditor->Text = tempNewMethodName;
                }
                generatingNewTower = false;
            }
            else
            {
                TowerUI::ShowTower(database, this->MdiParent, currentPeal->TowerId(), currentPeal->RingId());
            }
        }
    }
    else
    {
        TowerUI::ShowTower(database, this->MdiParent, currentPeal->TowerId(), currentPeal->RingId());
    }
}

System::Void
PealUI::CreateMissingMethod(System::Object^  sender, System::EventArgs^  e)
{
    if ( (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode || state == DucoWindowState::EAutoNewMode) && methodEditor->SelectedIndex == -1)
    {
        Duco::ObjectId suggestedMethodId = database->SuggestMethod(methodEditor->Text);
        bool stopProcessing = false;
        if (suggestedMethodId.ValidId())
        {
            Method suggestedMethod;
            if (database->FindMethod(suggestedMethodId, suggestedMethod, false))
            {
                tabControl1->SelectedIndex = 0;
                switch (MessageBox::Show(this, "A similar method already exists: " + DucoUtils::ConvertString(suggestedMethod.FullName(database->Database())) + "\n Would you like to use this instead?", "Method suggestion", MessageBoxButtons::YesNoCancel, MessageBoxIcon::Warning))
                {
                    case ::DialogResult::Yes:
                    {
                        // Suggested method accepted, save it and return.
                        System::Int32 newMethodIndex = DatabaseGenUtils::FindIndex(allMethodNames, suggestedMethod.Id());
                        methodEditor->SelectedIndex = newMethodIndex;
                        stopProcessing = true;
                        break;
                    }
                    case ::DialogResult::No:
                        stopProcessing = false;
                        break;
                    case ::DialogResult::Cancel:
                        stopProcessing = true;
                        break;

                }
            }
        }
        else if ((methodEditor->Text->Length == 0 || database->Config().ImportNewMethod()) && MessageBox::Show("Import method from CC method collection?", "Method missing", MessageBoxButtons::OKCancel) == ::DialogResult::OK)
        {   // Import method from CC collection
            Duco::ObjectId methodId;
            unsigned int noOfBells(-1);
            const Ring* const currentRing = currentTower->FindRing(currentPeal->RingId());
            if (currentRing != NULL)
            {
                noOfBells = currentRing->NoOfBells();

                tabControl1->SelectedIndex = 0;
                ImportSingleMethod^ importMethodUI = gcnew ImportSingleMethod(database, methodId, noOfBells, methodEditor->Text, true);
                importMethodUI->ShowDialog();
                if (methodId.ValidId())
                {
                    Duco::Method newMethod;
                    if (database->FindMethod(methodId, newMethod, false))
                    {
                        methodEditor->Text = DucoUtils::ConvertString(newMethod.FullName(database->Database()));
                        stopProcessing = true;
                    }
                }
            }
        }
        if (!stopProcessing && methodEditor->Text->Length > 0)
        {
            if (!database->MethodNameMatchesOrder(methodEditor->Text, currentPeal->NoOfBellsRung(database->Database())))
            {
                SoundUtils::PlayErrorSound();
                MessageBox::Show(this, "The number of bells in the ring does not match the number of bells in the method. Check method stage, tower or number of bells in the selected ring.", "Cannot create method - number of bells mismatch");
            }
            else
            {
                Duco::ObjectId createdMethodId = database->AddMethod(methodEditor->Text);
                currentPeal->SetMethodId(createdMethodId);
                System::Int32 newMethodIndex = DatabaseGenUtils::FindIndex(allMethodNames, createdMethodId.Id());
                methodEditor->SelectedIndex = newMethodIndex;
            }
        }
    }
    else
    {
        MethodUI::ShowMethod(database, this->MdiParent, currentPeal->MethodId());
    }
}

System::Void
PealUI::CreateMissingRingersBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    ringerList->CreateNewRingers(tabControl1);
}

System::Void
PealUI::FindSeriesBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (currentPeal->SeriesId().ValidId())
    {
        MethodSeriesUI::ShowSeries(database, MdiParent, currentPeal->SeriesId());
    }
    else if (state == DucoWindowState::ENewMode || state == DucoWindowState::EEditMode || state == DucoWindowState::EAutoNewMode)
    {
        Duco::ObjectId suggestedSeriesId;
        std::set<Duco::ObjectId> suggestedMethodIds;
        bool dualOrderMethod;
        if (database->SuggestMethodSeries(multiMethodEditor->Text, currentPeal->NoOfBellsInMethod(database->Database().MethodsDatabase(), dualOrderMethod), suggestedSeriesId, suggestedMethodIds))
        {
            Duco::MethodSeries theSeries;
            if (database->FindMethodSeries(suggestedSeriesId, theSeries, false) && MessageBox::Show(MdiParent, "No series is set, would you like to use:\n " + DucoUtils::ConvertString(theSeries.Name()) + "?", "No method series", MessageBoxButtons::YesNo, MessageBoxIcon::Question, MessageBoxDefaultButton::Button1) == ::DialogResult::Yes)
            {
                seriesEditor->SelectedIndex = DatabaseGenUtils::FindIndex(allSeriesNames, suggestedSeriesId);
            }
        }
        else if (suggestedMethodIds.size() >= 2)
        {
            if (MessageBox::Show(MdiParent, "No series is set, would you like to create one?", "Missing method series", MessageBoxButtons::YesNo, MessageBoxIcon::Question, MessageBoxDefaultButton::Button1) == ::DialogResult::Yes)
            {
                Duco::ObjectId newSeriesId;
                MethodSeriesUI::CreateSeries(database, MdiParent, suggestedMethodIds, newSeriesId);
                if (newSeriesId.ValidId())
                {
                    seriesEditor->SelectedIndex = DatabaseGenUtils::FindIndex(allSeriesNames, newSeriesId);
                }
            }
        }
    }
}

System::Void
PealUI::FindCompositionBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (currentPeal->CompositionId().ValidId())
    {
        CompositionUI::ShowComposition(database, MdiParent, currentPeal->CompositionId());
    }
}

System::Void 
PealUI::MethodEditor_SelectedIndexChanged(System::Object^ editor, System::EventArgs^  e)
{
    if ((state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode || state == DucoWindowState::EAutoNewMode) && !updatingExistingTower && !generatingNewMethod)
    {
        Duco::ObjectId methodId = DatabaseGenUtils::FindId(allMethodNames, methodEditor->SelectedIndex);
        currentPeal->SetMethodId(methodId);
        methodEditor->BackColor = SystemColors::ControlLightLight;
        if (!database->Database().MethodsDatabase().MethodIsSpliced(methodId))
            currentPeal->SetSeriesId(-1);
        seriesGroup->Visible = database->Database().MethodsDatabase().MethodIsSpliced(methodId);
        UpdateValidationLabel();
        dataChanged = true;
    }
}

System::Void 
PealUI::MethodEditor_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (!generatingNewMethod && (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode || state == DucoWindowState::EAutoNewMode) && methodEditor->SelectedIndex == -1)
    {
        if (methodEditor->SelectedIndex == -1)
        {
            currentPeal->SetMethodId(-1);
            methodEditor->BackColor = KErrorColour;
            UpdateValidationLabel();
            dataChanged = true;
        }
    }
}

System::Void
PealUI::RwReference_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode || state == DucoWindowState::EAutoNewMode)
    {
        std::wstring newRWReference;
        DucoUtils::ConvertString(rwReference->Text, newRWReference);
        currentPeal->SetRingingWorldReference(newRWReference);
        dataChanged = true;
    }
}

System::Void
PealUI::BellBoardReference_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode || state == DucoWindowState::EAutoNewMode)
    {
        std::wstring newBellBoardId;
        DucoUtils::ConvertString(bellBoardReference->Text, newBellBoardId);
        currentPeal->SetBellBoardId(newBellBoardId);
        dataChanged = true;
    }
}

System::Void
PealUI::NotesEditor_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode || state == DucoWindowState::EAutoNewMode)
    {
        std::wstring newNotes;
        DucoUtils::ConvertString(notesEditor->Text, newNotes);
        currentPeal->SetNotes(newNotes);
        dataChanged = true;
    }
}

System::Void
PealUI::WithdrawnEditor_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode || state == DucoWindowState::EAutoNewMode)
    {
        currentPeal->SetWithdrawn(withdrawnEditor->Checked);
        dataChanged = true;
    }
}

System::Void
PealUI::TabPage1_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
    if (state == DucoWindowState::EViewMode)
    {
        if (dateEditor->Bounds.Contains(e->Location))
        {
            Duco::ObjectId id;
            InputDateQueryUI^ findDateDialog = gcnew InputDateQueryUI(database, id, DucoUtils::ConvertDate(currentPeal->Date()));
            System::Windows::Forms::DialogResult returnVal = findDateDialog->ShowDialog();
            if (returnVal == ::DialogResult::OK)
            {
                PopulateViewWithPeal(id);
            }
        }
    }
}

System::Void
PealUI::PealIdLabel_Click(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::EViewMode)
    {
        System::Int16 id = -1;
        InputIndexQueryUI^ findIdDialog = gcnew InputIndexQueryUI("peal", id);
        System::Windows::Forms::DialogResult returnVal = findIdDialog->ShowDialog();
        if (returnVal == ::DialogResult::OK)
        {
            PopulateViewWithPeal(id);
        }
    }
}

System::Void
PealUI::SetMissingAssociationFromDownload(System::String^ newAssociation)
{
    associationEditor->Text = newAssociation;
    associationEditor->BackColor = KErrorColour;
}

System::Void
PealUI::SetMissingRingerFromDownload(int bellNumber, System::String^ name, bool strapper, bool conductor)
{
    ringerList->AddMissingRinger(bellNumber, name, conductor, strapper);
}

System::Void
PealUI::SetMissingMethodFromDownload(System::String^ methodName)
{
    methodEditor->Text = methodName;
    methodEditor->BackColor = KErrorColour;
}

System::Void
PealUI::SetMissingRingFromDownload(System::String^ ringName)
{
    ringSelector->Text = ringName;
    ringSelector->BackColor = KErrorColour;
    ringerList->UpdateNoOfBells(-1);
}

System::Void
PealUI::SetMissingTowerFromDownload(System::String^ towerName, System::String^ tenorWeight, System::String^ tenorKey, System::String^ towerbaseId, System::String^ doveId)
{
    downloadTenorWeight = tenorWeight;
    downloadTenorKey = tenorKey;
    downloadTowerbaseId = towerbaseId;
    downloadDoveId = doveId;
    towerEditor->Text = towerName;

    ringSelector->BackColor = KErrorColour;
    towerEditor->BackColor = KErrorColour;
}

System::Void
PealUI::SingleConductor_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode || state == DucoWindowState::EAutoNewMode)
    {
        if (static_cast<System::Windows::Forms::RadioButton^>(sender)->Checked == true)
        {
            currentPeal->SetConductedType(ESingleConductor);
            ringerList->RefreshConductorControls();
            UpdateValidationLabel();
            dataChanged = true;
        }
    }
}

System::Void
PealUI::SilentConductor_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode || state == DucoWindowState::EAutoNewMode)
    {
        if (static_cast<System::Windows::Forms::RadioButton^>(sender)->Checked == true)
        {
            currentPeal->SetConductedType(ESilentAndNonConducted);
            ringerList->RefreshConductorControls();
            UpdateValidationLabel();
            dataChanged = true;
        }
    }
}

System::Void
PealUI::JointConductor_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::EEditMode || state == DucoWindowState::ENewMode || state == DucoWindowState::EAutoNewMode)
    {
        if (static_cast<System::Windows::Forms::RadioButton^>(sender)->Checked == true)
        {
            currentPeal->SetConductedType(EJointlyConducted);
            ringerList->RefreshConductorControls();
            UpdateValidationLabel();
            dataChanged = true;
        }
    }
}

System::Void
PealUI::SetConductorStates()
{
    TConductorType conductorType = currentPeal->ConductedType();
    switch (conductorType)
    {
    case ESingleConductor:
    default:
        singleConductor->Checked = true;
        break;
    case ESilentAndNonConducted:
        silentConductor->Checked = true;
        break;
    case EJointlyConducted:
        jointConductor->Checked = true;
        break;
    }
    dataChanged = true;
    ringerList->RefreshConductorControls();
}

System::Void
PealUI::SetViewStateAndShowLastPeal()
{
    SetViewState();
    Duco::ObjectId firstPealId;
    database->LastPeal(firstPealId);
    PopulateViewWithPeal(firstPealId);
}

System::Void
PealUI::RingerObjectChanged(System::Object^  sender, System::EventArgs^  e)
{
    if (state == DucoWindowState::ENewMode || state == DucoWindowState::EEditMode || state == DucoWindowState::EAutoNewMode)
    {
        UpdateValidationLabel();
        dataChanged = true;
    }
}

System::Void
PealUI::PictureEvent(System::Object^  sender, System::EventArgs^  e)
{
    DucoUI::PictureEventArgs^  args = static_cast<DucoUI::PictureEventArgs^>(e);

    if (args->pictureId != NULL)
    {
        currentPeal->SetPictureId(*args->pictureId);
    }
    else
    {
        currentPeal->SetPictureId(KNoId);
    }
    pictureCtrl->SetPicture(currentPeal->PictureId(), false);
    dataChanged = true;
}
