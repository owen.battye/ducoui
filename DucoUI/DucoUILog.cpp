#include <string>
#include "DucoUILog.h"
#include "DucoUtils.h"

using namespace DucoUI;
using namespace System::IO;
using namespace System::Text;
using namespace std::chrono;

const long long KTenMegabytes = 1024 * 1024 * 10;
#define KNewLineChar "\r\n"

DucoUILog::DucoUILog()
{
    if (fileHandle == nullptr)
    {
        try
        {
            System::String^ programData = System::Environment::GetFolderPath(System::Environment::SpecialFolder::CommonApplicationData);
            System::String^ debugFilenameOnly = "DucoUIDebug.log";

            System::String^ debugFilename = System::IO::Path::Combine(programData, "Rocketpole", "Duco", debugFilenameOnly);

            long long fileSize = 0;
            if (File::Exists(debugFilename))
            {
                FileInfo^ fileInfo = gcnew FileInfo(debugFilename);
                fileSize = fileInfo->Length;
            }
            if (fileSize > KTenMegabytes)
            {
                fileHandle = gcnew System::IO::FileStream(debugFilename, System::IO::FileMode::Truncate);
            }
            else
            {
                fileHandle = gcnew System::IO::FileStream(debugFilename, System::IO::FileMode::Append);
            }
            UnicodeEncoding^ unicode = gcnew UnicodeEncoding;
            System::String^ textToPrint = "\r\nLog created: " + System::DateTime::Now.ToShortDateString() + " " + System::DateTime::Now.ToShortTimeString() + "\r\n";
            fileHandle->WriteAsync(unicode->GetBytes(textToPrint), 0, unicode->GetByteCount(textToPrint));
        }
        catch (System::Exception^ ex)
        {
            std::string textToPrintStr;
            DucoUtils::ConvertString(ex->Message, textToPrintStr);
            _RPTFN(_CRT_WARN, textToPrintStr.c_str(), 0);
        }
    }
}

void
DucoUILog::Close()
{
    if (fileHandle != nullptr)
    {
        fileHandle->Flush();
        fileHandle->Close();
    }
}

void
DucoUILog::PrintDebug(System::String^ textToPrint, System::DateTime^ startTime)
{
#if defined(_DEBUG) || defined(ENABLELOGGING)
    System::DateTime^ endTime = gcnew System::DateTime();
    System::TimeSpan duration = startTime->Subtract(*endTime);

    textToPrint += KNewLineChar;
    textToPrint = "******  " + System::Convert::ToString(duration.TotalMilliseconds) + "ms: " + textToPrint;

    std::string textToPrintStr;
    DucoUtils::ConvertString(textToPrint, textToPrintStr);
    PrintDebugToConsole(textToPrintStr);

    DucoUILog^ logger = gcnew DucoUILog();
    if (logger->fileHandle != nullptr)
    {
        UnicodeEncoding^ unicode = gcnew UnicodeEncoding;
        logger->fileHandle->WriteAsync(unicode->GetBytes(textToPrint), 0, unicode->GetByteCount(textToPrint));
        fileHandle->Flush();
    }
#endif // DEBUG
}

void
DucoUILog::PrintDebug(std::chrono::steady_clock::time_point start, System::String^ text)
{
    auto stop = high_resolution_clock::now();
    microseconds duration = duration_cast<microseconds>(stop - start);

    if (duration.count() > 10000) //10 microseconds / 0.01 seconds
    {
        System::String^ message = "******  " + duration.count() + "us: " + text;

        _RPTF0(_CRT_WARN, message + KNewLineChar);
        PrintDebug(message);
    }
}

void
DucoUILog::PrintDebug(System::String^ textToPrint)
{
#if defined(_DEBUG) || defined(ENABLELOGGING)
    textToPrint += KNewLineChar;

    std::string textToPrintStr;
    DucoUtils::ConvertString(textToPrint, textToPrintStr);
    PrintDebugToConsole(textToPrintStr);

    try
    {
        DucoUILog^ logger = gcnew DucoUILog();
        if (logger->fileHandle != nullptr)
        {
            UnicodeEncoding^ unicode = gcnew UnicodeEncoding;
            logger->fileHandle->WriteAsync(unicode->GetBytes(textToPrint), 0, unicode->GetByteCount(textToPrint));
            fileHandle->Flush();
        }
    }
    catch (System::Exception^ ex)
    {
        std::string textToPrintStr;
        DucoUtils::ConvertString(ex->Message, textToPrintStr);
        _RPTFN(_CRT_WARN, textToPrintStr.c_str(), 0);
    }
#endif // DEBUG
}

void
DucoUILog::PrintDebugToConsole(const std::string& testToPrintStr)
{
#if defined(_DEBUG) || defined(ENABLELOGGING)
    _RPTF0(_CRT_WARN, testToPrintStr.c_str());
#endif // DEBUG
}

void
DucoUILog::PrintError(System::String^ originalErrorText)
{
    try
    {
        DucoUILog^ logger = gcnew DucoUILog();
        if (logger->fileHandle != nullptr)
        {
            System::String^ textToPrint = "***** " + originalErrorText + KNewLineChar;
            UnicodeEncoding^ unicode = gcnew UnicodeEncoding;
            logger->fileHandle->WriteAsync(unicode->GetBytes(textToPrint), 0, unicode->GetByteCount(textToPrint));
            fileHandle->Flush();
#if defined(_DEBUG) || defined(ENABLELOGGING)
            std::string textToPrintStr;
            DucoUtils::ConvertString(textToPrint, textToPrintStr);
            PrintDebugToConsole(textToPrintStr);
#endif // DEBUG
        }
    }
    catch (System::Exception^ ex)
    {
        std::string textToPrintStr;
        DucoUtils::ConvertString(ex->Message, textToPrintStr);
        _RPTFN(_CRT_WARN, textToPrintStr.c_str(), 0);
    }
}

