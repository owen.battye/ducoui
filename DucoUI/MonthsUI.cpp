#include "RingingDatabaseObserver.h"
#include <StatisticFilters.h>
#include "DatabaseManager.h"
#include "RingerDisplayCache.h"

#include "MonthsUI.h"

#include "DucoUtils.h"
#include <map>
#include "SystemDefaultIcon.h"
#include "FiltersUI.h"
#include <RingingDatabase.h>
#include <PealDatabase.h>
#include <Peal.h>
#include <DucoEngineUtils.h>

using namespace Duco;
using namespace DucoUI;
using namespace System::Windows::Forms;
using namespace System::ComponentModel;
using namespace System::Collections;

MonthsUI::MonthsUI(DatabaseManager^ theDatabase)
:   database(theDatabase)
{
    filters = new Duco::StatisticFilters(database->Database());
    InitializeComponent();
}

MonthsUI::!MonthsUI()
{
    delete filters;
    database->RemoveObserver(this);
}

MonthsUI::~MonthsUI()
{
    this->!MonthsUI();
    backgroundGenerator->CancelAsync();
    if (components)
    {
        delete components;
    }
}

void
MonthsUI::DatabaseChanged(DucoUI::TEventType eventId, Duco::TObjectType type, const Duco::ObjectId& id, const Duco::ObjectId& id2)
{
    switch (type)
    {
    case TObjectType::EPeal:
        GenerateData();
        break;

    default:
        break;
    }
}

System::Void
MonthsUI::settingsChanged(System::Object^  sender, System::EventArgs^  e)
{
    GenerateData();
}

System::Void
MonthsUI::MonthsUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    GenerateData();
    database->AddObserver(this);
}

System::Void
MonthsUI::GenerateData()
{
    if (backgroundGenerator->IsBusy)
    {
        restartGenerator = true;
        backgroundGenerator->CancelAsync();
        return;
    }

    if (!backgroundGenerator->IsBusy)
    {
        restartGenerator = false;
        dataGridView->Rows->Clear();
        backgroundGenerator->RunWorkerAsync();
    }
}

System::Void
MonthsUI::filtersBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    FiltersUI^ filtersDialog = gcnew FiltersUI(*filters, database);
    System::Windows::Forms::DialogResult returnVal = filtersDialog->ShowDialog(this);
    if (returnVal == System::Windows::Forms::DialogResult::OK)
    {
        GenerateData();
    }
}

System::Void
MonthsUI::backgroundGenerator_DoWork(System::Object^ sender, System::ComponentModel::DoWorkEventArgs^  e)
{
    BackgroundWorker^ worker = dynamic_cast<BackgroundWorker^>(sender);
    if ( worker->CancellationPending )
    {
        e->Cancel = true;
    }

    std::set<unsigned int> years;
    database->Database().PealsDatabase().GetYearRange(*filters, years);

    float numberOfYears = float(years.size());
    float count (0);
    size_t total (0);
    std::set<unsigned int>::const_iterator yearIt = years.begin();
    while (yearIt != years.end() && !worker->CancellationPending)
    {
        size_t yearTotal (0);
        ++count;
        std::map<unsigned int, Duco::PealLengthInfo> monthCounts;
        database->Database().PealsDatabase().GetAllMonthsByPealCount(*filters , *yearIt, monthCounts);

        DataGridViewRow^ newRow = gcnew DataGridViewRow();
        newRow->HeaderCell->Value = System::Convert::ToString(*yearIt);

        for (int month (1); month <= 12; ++month)
        {
            std::map<unsigned int, Duco::PealLengthInfo>::const_iterator it = monthCounts.find(month);
            DataGridViewTextBoxCell^ newCell = gcnew DataGridViewTextBoxCell();
            if (it != monthCounts.end())
            {
                newCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->second.TotalPeals(), true));
                yearTotal += it->second.TotalPeals();
                total += it->second.TotalPeals();
            }
            newRow->Cells->Add(newCell);
        }
        DataGridViewTextBoxCell^ yearTotalCell = gcnew DataGridViewTextBoxCell();
        yearTotalCell->Value = System::Convert::ToString(yearTotal);
        newRow->Cells->Add(yearTotalCell);
        DataGridViewTextBoxCell^ runningTotalCell = gcnew DataGridViewTextBoxCell();
        runningTotalCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(total));
        newRow->Cells->Add(runningTotalCell);
        int percentComplete = int ((count / numberOfYears) * float(100));
        if (!worker->CancellationPending)
            backgroundGenerator->ReportProgress(percentComplete, newRow);
        ++yearIt;
    }
    if (worker->CancellationPending)
        e->Cancel = true;
}

System::Void
MonthsUI::backgroundGenerator_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    if (!backgroundGenerator->CancellationPending && progressBar != nullptr)
    {
        progressBar->Value = e->ProgressPercentage;
        if (e != nullptr)
        {
            if (e->UserState != nullptr)
            {
                dataGridView->Rows->Add((DataGridViewRow^)e->UserState);
            }
        }
    }
}

System::Void
MonthsUI::backgroundGenerator_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    if (restartGenerator)
    {
        progressBar->Value = 0;
        GenerateData();
    }
    else if (!e->Cancelled)
    {
        progressBar->Value = 0;
    }
}


void
MonthsUI::InitializeComponent()
{
    System::Windows::Forms::StatusStrip^  statusStrip1;
    System::Windows::Forms::ToolStrip^  toolStrip1;
    System::Windows::Forms::ToolStripButton^  filtersBtn;
    System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(MonthsUI::typeid));
    System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle1 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
    System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle3 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
    System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle2 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
    this->progressBar = (gcnew System::Windows::Forms::ToolStripProgressBar());
    this->dataGridView = (gcnew System::Windows::Forms::DataGridView());
    this->januaryColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->februaryColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->marchColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->aprilColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->mayColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->juneColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->julyColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->augustColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->septemberColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->octoberColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->novemberColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->decemberColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->totalColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->runningTotalColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
    this->backgroundGenerator = (gcnew System::ComponentModel::BackgroundWorker());
    statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
    toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
    filtersBtn = (gcnew System::Windows::Forms::ToolStripButton());
    statusStrip1->SuspendLayout();
    toolStrip1->SuspendLayout();
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->BeginInit();
    this->SuspendLayout();
    // 
    // statusStrip1
    // 
    statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->progressBar });
    statusStrip1->Location = System::Drawing::Point(0, 380);
    statusStrip1->Name = L"statusStrip1";
    statusStrip1->Size = System::Drawing::Size(549, 22);
    statusStrip1->TabIndex = 1;
    statusStrip1->Text = L"statusStrip1";
    // 
    // progressBar
    // 
    this->progressBar->Name = L"progressBar";
    this->progressBar->Size = System::Drawing::Size(100, 16);
    // 
    // toolStrip1
    // 
    toolStrip1->GripStyle = System::Windows::Forms::ToolStripGripStyle::Hidden;
    toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { filtersBtn });
    toolStrip1->Location = System::Drawing::Point(0, 0);
    toolStrip1->Name = L"toolStrip1";
    toolStrip1->Size = System::Drawing::Size(549, 25);
    toolStrip1->TabIndex = 2;
    toolStrip1->Text = L"toolStrip1";
    // 
    // filtersBtn
    // 
    filtersBtn->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
    filtersBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"filtersBtn.Image")));
    filtersBtn->ImageTransparentColor = System::Drawing::Color::Magenta;
    filtersBtn->Name = L"filtersBtn";
    filtersBtn->Size = System::Drawing::Size(42, 22);
    filtersBtn->Text = L"&Filters";
    filtersBtn->Click += gcnew System::EventHandler(this, &MonthsUI::filtersBtn_Click);
    // 
    // dataGridView
    // 
    this->dataGridView->AllowUserToAddRows = false;
    this->dataGridView->AllowUserToDeleteRows = false;
    this->dataGridView->ClipboardCopyMode = System::Windows::Forms::DataGridViewClipboardCopyMode::EnableAlwaysIncludeHeaderText;
    dataGridViewCellStyle1->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
    dataGridViewCellStyle1->BackColor = System::Drawing::SystemColors::Control;
    dataGridViewCellStyle1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
        System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
    dataGridViewCellStyle1->ForeColor = System::Drawing::SystemColors::WindowText;
    dataGridViewCellStyle1->SelectionBackColor = System::Drawing::SystemColors::Highlight;
    dataGridViewCellStyle1->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
    dataGridViewCellStyle1->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
    this->dataGridView->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
    this->dataGridView->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
    this->dataGridView->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(14) {
        this->januaryColumn,
            this->februaryColumn, this->marchColumn, this->aprilColumn, this->mayColumn, this->juneColumn, this->julyColumn, this->augustColumn,
            this->septemberColumn, this->octoberColumn, this->novemberColumn, this->decemberColumn, this->totalColumn, this->runningTotalColumn
    });
    dataGridViewCellStyle3->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
    dataGridViewCellStyle3->BackColor = System::Drawing::SystemColors::Window;
    dataGridViewCellStyle3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
        System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
    dataGridViewCellStyle3->ForeColor = System::Drawing::SystemColors::ControlText;
    dataGridViewCellStyle3->SelectionBackColor = System::Drawing::SystemColors::Highlight;
    dataGridViewCellStyle3->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
    dataGridViewCellStyle3->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
    this->dataGridView->DefaultCellStyle = dataGridViewCellStyle3;
    this->dataGridView->Dock = System::Windows::Forms::DockStyle::Fill;
    this->dataGridView->Location = System::Drawing::Point(0, 25);
    this->dataGridView->Name = L"dataGridView";
    this->dataGridView->ReadOnly = true;
    this->dataGridView->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToDisplayedHeaders;
    this->dataGridView->Size = System::Drawing::Size(549, 355);
    this->dataGridView->StandardTab = true;
    this->dataGridView->TabIndex = 1;
    // 
    // januaryColumn
    // 
    this->januaryColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
    this->januaryColumn->HeaderText = L"Jan";
    this->januaryColumn->Name = L"januaryColumn";
    this->januaryColumn->ReadOnly = true;
    this->januaryColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
    this->januaryColumn->Width = 30;
    // 
    // februaryColumn
    // 
    this->februaryColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
    this->februaryColumn->HeaderText = L"Feb";
    this->februaryColumn->Name = L"februaryColumn";
    this->februaryColumn->ReadOnly = true;
    this->februaryColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
    this->februaryColumn->Width = 31;
    // 
    // marchColumn
    // 
    this->marchColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
    this->marchColumn->HeaderText = L"Mar";
    this->marchColumn->Name = L"marchColumn";
    this->marchColumn->ReadOnly = true;
    this->marchColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
    this->marchColumn->Width = 31;
    // 
    // aprilColumn
    // 
    this->aprilColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
    this->aprilColumn->HeaderText = L"Apr";
    this->aprilColumn->Name = L"aprilColumn";
    this->aprilColumn->ReadOnly = true;
    this->aprilColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
    this->aprilColumn->Width = 29;
    // 
    // mayColumn
    // 
    this->mayColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
    this->mayColumn->HeaderText = L"May";
    this->mayColumn->Name = L"mayColumn";
    this->mayColumn->ReadOnly = true;
    this->mayColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
    this->mayColumn->Width = 33;
    // 
    // juneColumn
    // 
    this->juneColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
    this->juneColumn->HeaderText = L"Jun";
    this->juneColumn->Name = L"juneColumn";
    this->juneColumn->ReadOnly = true;
    this->juneColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
    this->juneColumn->Width = 30;
    // 
    // julyColumn
    // 
    this->julyColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
    this->julyColumn->HeaderText = L"Jul";
    this->julyColumn->Name = L"julyColumn";
    this->julyColumn->ReadOnly = true;
    this->julyColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
    this->julyColumn->Width = 26;
    // 
    // augustColumn
    // 
    this->augustColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
    this->augustColumn->HeaderText = L"Aug";
    this->augustColumn->Name = L"augustColumn";
    this->augustColumn->ReadOnly = true;
    this->augustColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
    this->augustColumn->Width = 32;
    // 
    // septemberColumn
    // 
    this->septemberColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
    this->septemberColumn->HeaderText = L"Sep";
    this->septemberColumn->Name = L"septemberColumn";
    this->septemberColumn->ReadOnly = true;
    this->septemberColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
    this->septemberColumn->Width = 32;
    // 
    // octoberColumn
    // 
    this->octoberColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
    this->octoberColumn->HeaderText = L"Oct";
    this->octoberColumn->Name = L"octoberColumn";
    this->octoberColumn->ReadOnly = true;
    this->octoberColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
    this->octoberColumn->Width = 30;
    // 
    // novemberColumn
    // 
    this->novemberColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
    this->novemberColumn->HeaderText = L"Nov";
    this->novemberColumn->Name = L"novemberColumn";
    this->novemberColumn->ReadOnly = true;
    this->novemberColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
    this->novemberColumn->Width = 33;
    // 
    // decemberColumn
    // 
    this->decemberColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
    this->decemberColumn->HeaderText = L"Dec";
    this->decemberColumn->Name = L"decemberColumn";
    this->decemberColumn->ReadOnly = true;
    this->decemberColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
    this->decemberColumn->Width = 33;
    // 
    // totalColumn
    // 
    this->totalColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
    dataGridViewCellStyle2->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleRight;
    dataGridViewCellStyle2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->totalColumn->DefaultCellStyle = dataGridViewCellStyle2;
    this->totalColumn->HeaderText = L"Year";
    this->totalColumn->Name = L"totalColumn";
    this->totalColumn->ReadOnly = true;
    this->totalColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
    this->totalColumn->Width = 35;
    // 
    // runningTotalColumn
    // 
    this->runningTotalColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
    this->runningTotalColumn->HeaderText = L"Total";
    this->runningTotalColumn->Name = L"runningTotalColumn";
    this->runningTotalColumn->ReadOnly = true;
    this->runningTotalColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
    this->runningTotalColumn->Width = 37;
    // 
    // backgroundGenerator
    // 
    this->backgroundGenerator->WorkerReportsProgress = true;
    this->backgroundGenerator->WorkerSupportsCancellation = true;
    this->backgroundGenerator->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &MonthsUI::backgroundGenerator_DoWork);
    this->backgroundGenerator->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &MonthsUI::backgroundGenerator_ProgressChanged);
    this->backgroundGenerator->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &MonthsUI::backgroundGenerator_RunWorkerCompleted);
    // 
    // MonthsUI
    // 
    this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
    this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
    this->ClientSize = System::Drawing::Size(549, 402);
    this->Controls->Add(this->dataGridView);
    this->Controls->Add(toolStrip1);
    this->Controls->Add(statusStrip1);
    this->Name = L"MonthsUI";
    this->Text = L"Month and year statistics";
    this->Load += gcnew System::EventHandler(this, &MonthsUI::MonthsUI_Load);
    statusStrip1->ResumeLayout(false);
    statusStrip1->PerformLayout();
    toolStrip1->ResumeLayout(false);
    toolStrip1->PerformLayout();
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->EndInit();
    this->ResumeLayout(false);
    this->PerformLayout();
}