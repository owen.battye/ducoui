#include "RingingDatabaseObserver.h"
#include "DatabaseManager.h"
#include "RingerDisplayCache.h"
#include "DucoWindowState.h"

#include "MethodsCircledUI.h"

#include "DucoUtils.h"
#include "GenericTablePrintUtil.h"
#include "MethodUI.h"
#include "SoundUtils.h"
#include "DucoUIUtils.h"
#include <StatisticFilters.h>
#include "FiltersUI.h"
#include "SystemDefaultIcon.h"
#include "DucoTableSorter.h"
#include <RingingDatabase.h>
#include <DucoConfiguration.h>
#include <Method.h>
#include <PealDatabase.h>
#include <DatabaseSettings.h>
#include <DucoEngineUtils.h>

using namespace Duco;
using namespace DucoUI;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Windows::Forms;

MethodsCircledUI::MethodsCircledUI(DucoUI::DatabaseManager^ theDatabase)
:   database(theDatabase), totalNumberOfMethodsCircled(0),
    restartGenerator(false), loading(true), startingGenerator(false)
{
    InitializeComponent();
    filters = new Duco::StatisticFilters(database->Database());
}

MethodsCircledUI::MethodsCircledUI(DucoUI::DatabaseManager^ theDatabase, const Duco::StatisticFilters& newFilters)
    : database(theDatabase), totalNumberOfMethodsCircled(0),
    restartGenerator(false), loading(true), startingGenerator(false)
{
    InitializeComponent();
    filters = new Duco::StatisticFilters(database->Database());
    (*filters) = newFilters;
}


MethodsCircledUI::~MethodsCircledUI()
{
    this->!MethodsCircledUI();
    if (components)
    {
        delete components;
    }
}

MethodsCircledUI::!MethodsCircledUI()
{
    delete filters;
}

System::Void
MethodsCircledUI::MethodsCircledUI_Load(System::Object^  sender, System::EventArgs^  e)
{
    Icon = SystemDefaultIcon::DefaultSystemIcon();
    CheckEnoughColumnsExist();
    loading = false;
    StartGenerator();
}

System::Void
MethodsCircledUI::SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
    StartGenerator();
}

System::Void
MethodsCircledUI::conducted_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
    StartGenerator();
}

System::Void
MethodsCircledUI::StartGenerator()
{
    if (loading)
        return;

    if (backgroundWorker->IsBusy || startingGenerator)
    {
        restartGenerator = true;
        backgroundWorker->CancelAsync();
        return;
    }

    startingGenerator = true;
    totalNumberOfMethodsCircled = 0;

    ResetAllSortGlyphs();
    resultsCount->Text = "";
    dataGridView->Rows->Clear();
    restartGenerator = false;

    backgroundWorker->RunWorkerAsync();
    startingGenerator = false;
}

System::Void
MethodsCircledUI::backgroundWorker_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
    BackgroundWorker^ worker = dynamic_cast<BackgroundWorker^>(sender);

    std::map<Duco::ObjectId, Duco::PealLengthInfo> sortedMethodIds;

    database->Database().PealsDatabase().GetMethodsPealCount(*filters, sortedMethodIds);

    double noOfPeals = double(sortedMethodIds.size());
    double count (0);

    bool ignoreMethodsWithOnePeal (false);
    Duco::ObjectId towerId;
    if ( (!filters->Tower(towerId)) &&
        (database->Config().DisableFeaturesForPerformance(sortedMethodIds.size())))
    {
        ignoreMethodsWithOnePeal = true;
    }

    std::map<Duco::ObjectId, Duco::PealLengthInfo>::const_iterator it = sortedMethodIds.begin();
    while (!worker->CancellationPending && it != sortedMethodIds.end())
    {
        if (ignoreMethodsWithOnePeal && it->second.TotalPeals() <= 1)
        {
            ++count;
        }
        else
        {
            Duco::Method nextMethod;
            if (database->FindMethod(it->first, nextMethod, true))
            {
                size_t numberOfTimesCircled (0);
                std::map<unsigned int, Duco::CirclingData> bellPealCount;
                PerformanceDate firstCircledDate;
                StatisticFilters localFilters(*filters);
                localFilters.SetMethod(true, it->first);

                database->Database().GetMethodCircling(localFilters, bellPealCount, numberOfTimesCircled, firstCircledDate);

                // Add tower name, and peal totals here.
                DataGridViewRow^ newRow = gcnew DataGridViewRow();
                DataGridViewCellCollection^ theCells = newRow->Cells;

                DataGridViewTextBoxCell^ idCell = gcnew DataGridViewTextBoxCell();
                idCell->Value = DucoUtils::ConvertString(nextMethod.Id());
                theCells->Add(idCell);

                DataGridViewTextBoxCell^ methodNameCell = gcnew DataGridViewTextBoxCell();
                methodNameCell->Value = DucoUtils::ConvertString(nextMethod.FullName(database->Database()));
                theCells->Add(methodNameCell);

                DataGridViewTextBoxCell^ methodCircledCell = gcnew DataGridViewTextBoxCell();
                methodCircledCell->Value = Convert::ToString(numberOfTimesCircled);
                theCells->Add(methodCircledCell);
                if (numberOfTimesCircled >= 1)
                    ++totalNumberOfMethodsCircled;

                DataGridViewTextBoxCell^ methodPealCountCell = gcnew DataGridViewTextBoxCell();
                methodPealCountCell->Value = DucoUtils::ConvertString(DucoEngineUtils::ToString(it->second.TotalPeals(), true));
                theCells->Add(methodPealCountCell);

                DataGridViewTextBoxCell^ dateCell = gcnew DataGridViewTextBoxCell();
                if (numberOfTimesCircled > 0)
                    dateCell->Value = DucoUtils::ConvertString(firstCircledDate.Str());
                theCells->Add(dateCell);

                bool addMethod (false);
                std::map<unsigned int, Duco::CirclingData>::const_iterator it2 = bellPealCount.begin();
                while (it2 != bellPealCount.end())
                {
                    DataGridViewTextBoxCell^ nextBellCell = gcnew DataGridViewTextBoxCell();

                    if (it2->second.PealCount() > 0)
                    {
                        addMethod = true;
                    }
                    String^ newBellPealCountString = DucoUtils::ConvertString(DucoEngineUtils::ToString(it2->second.PealCount(), true));
                    nextBellCell->Value = newBellPealCountString;
                    theCells->Add(nextBellCell);
                    ++it2;
                }

                if (addMethod)
                    worker->ReportProgress(int((++count / noOfPeals)*double(100)), newRow);
                else
                    worker->ReportProgress(int((++count / noOfPeals)*double(100)));

                bellPealCount.clear();
            }
            else
            {
                ++count;
            }
        }
        ++it;
    }
}

System::Void
MethodsCircledUI::backgroundWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
    progressBar->Value = e->ProgressPercentage;

    if (e->UserState != nullptr)
    {
        DataGridViewRow^ newRow = (DataGridViewRow^)e->UserState;
        dataGridView->Rows->Add(newRow);
    }
}

System::Void
MethodsCircledUI::backgroundWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
    progressBar->Value = 0;

    if (restartGenerator)
    {
        StartGenerator();
    }
    else
    {
        dataGridView->Refresh();
        if (totalNumberOfMethodsCircled == 1)
            resultsCount->Text = Convert::ToString(totalNumberOfMethodsCircled) + " method circled";
        else
            resultsCount->Text = Convert::ToString(totalNumberOfMethodsCircled) + " methods circled";

        DucoTableSorter^ sorter = gcnew DucoTableSorter(true, EString);
        sorter->AddSortObject(ECount, false, 3);
        sorter->AddSortObject(ECount, false, 2);
        sorter->AddSortObject(EString, true, 1);
        dataGridView->Sort(sorter);

        DataGridViewColumn^ newColumn = dataGridView->Columns[3];
        newColumn->HeaderCell->SortGlyphDirection = SortOrder::Descending;
    }
}

System::Void 
MethodsCircledUI::CheckEnoughColumnsExist()
{
    for (unsigned int columnCount(0); columnCount < database->HighestValidOrderNumber(); ++columnCount)
    {
        String^ newColumnName = String::Format("Bell{0:D}", columnCount+1);
        String^ newColumnTitle = Convert::ToString(columnCount+1);
        int columnId = dataGridView->Columns->Add(newColumnName, newColumnTitle);
        dataGridView->Columns[columnId]->Width=30;
        dataGridView->Columns[columnId]->AutoSizeMode = DataGridViewAutoSizeColumnMode::None;
        dataGridView->Columns[columnId]->SortMode = DataGridViewColumnSortMode::NotSortable;
    }
}

System::Void
MethodsCircledUI::dataGridView_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
    DataGridViewColumn^ newColumn = dataGridView->Columns[e->ColumnIndex];
    if (newColumn->SortMode == System::Windows::Forms::DataGridViewColumnSortMode::Automatic)
    {
        ResetAllSortGlyphs();
        return;
    }
    DataGridViewColumn^ oldColumn = dataGridView->SortedColumn;
    if (oldColumn != nullptr && oldColumn != newColumn)
    {
        oldColumn->HeaderCell->SortGlyphDirection = SortOrder::None;
    }

    SortOrder newSortOrder = newColumn->HeaderCell->SortGlyphDirection;
    if (newSortOrder == SortOrder::None || newSortOrder == SortOrder::Descending)
        newSortOrder = SortOrder::Ascending;
    else
        newSortOrder = SortOrder::Descending;
    ResetAllSortGlyphs();

    newColumn->HeaderCell->SortGlyphDirection = newSortOrder;

    int secondColumnId = 3;
    if (e->ColumnIndex == 3)
        secondColumnId = 2;

    DucoTableSorter^ sorter = gcnew DucoTableSorter(newSortOrder == SortOrder::Ascending, EString);

    if (e->ColumnIndex == 4)
    {
        sorter->AddSortObject(EDate, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
    }
    else
    {
        sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, e->ColumnIndex);
        sorter->AddSortObject(ECount, newSortOrder == SortOrder::Ascending, secondColumnId);
        sorter->AddSortObject(EString, newSortOrder == SortOrder::Ascending, 1);
    }
    dataGridView->Sort(sorter);
}

System::Void
MethodsCircledUI::dataGridView_CellContentDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
    try
    {
        unsigned int rowIndex = e->RowIndex;
        DataGridViewRow^ currentRow = dataGridView->Rows[rowIndex];
        unsigned int methodId = Convert::ToInt16(currentRow->Cells[0]->Value);
        MethodUI::ShowMethod(database, this->MdiParent, methodId);
    }
    catch (Exception^)
    {
        SoundUtils::PlayErrorSound();
    }
}

System::Void
MethodsCircledUI::ResetAllSortGlyphs()
{
    for (int i (0); i < dataGridView->ColumnCount; ++i)
    {
        dataGridView->Columns[i]->HeaderCell->SortGlyphDirection = SortOrder::None;
    }
}

System::Void
MethodsCircledUI::filtersBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    FiltersUI^ filtersDialog = gcnew FiltersUI(*filters, database);
    System::Windows::Forms::DialogResult returnVal = filtersDialog->ShowDialog(this);
    if (returnVal == System::Windows::Forms::DialogResult::OK)
    {
        StartGenerator();
    }
}

System::Void
MethodsCircledUI::printBtn_Click(System::Object^  sender, System::EventArgs^  e)
{
    DucoUIUtils::Print(this, sender, gcnew System::Drawing::Printing::PrintPageEventHandler( this, &MethodsCircledUI::printData ), "method circling", database->Database().Settings().UsePrintPreview(), false);
}

System::Void
MethodsCircledUI::printData(System::Object^ sender, System::Drawing::Printing::PrintPageEventArgs^ args)
{
    if (printUtil == nullptr)
    {
        printUtil = gcnew GenericTablePrintUtil(database, args, "Method circling for ");
        printUtil->SetObjects(dataGridView, filters->RingerId(), TObjectType::ERinger, "", 5, dataGridView->Columns->Count);
    }
    System::Drawing::Printing::PrintDocument^ printDoc = static_cast<System::Drawing::Printing::PrintDocument^>(sender);
    printUtil->printObject(args, printDoc->PrinterSettings);
    if (!args->HasMorePages)
    {
        printUtil = nullptr;
    }
}
