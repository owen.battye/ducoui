#pragma once

#include "MethodLineDrawer.h"
#include <list>

namespace Duco
{
    class Change;
}

namespace DucoUI
{
    public ref class NormalMethodLineDrawer : public MethodLineDrawer
{
    public:
        NormalMethodLineDrawer(System::Drawing::Graphics^ lineDrwr, Duco::Method& theMethod, DucoUI::DatabaseManager^ theDatabase, System::ComponentModel::BackgroundWorker^ bgWorker);
        !NormalMethodLineDrawer();
        virtual ~NormalMethodLineDrawer();

    protected: // from base class MethodLineDrawer
        virtual void DrawGridLines() override;
        virtual bool DrawLine() override;
        virtual System::Void MoveTrebleLineAndDraw(System::Drawing::Pen^% penToUse) override;

    protected: // new functions
        virtual System::Void DrawLead(System::Drawing::Pen^% penToUse) override;
    };
}
