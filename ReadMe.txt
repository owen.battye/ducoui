Duco is pretty much under constant development, as and when I find extra things I want it to do... so if you think of anything just email support@rocketpole.co.uk. I'll probably be more than happy to add it, especially if it's another silly stat.

Importing from Dove.
There is a file that will never be documented in the installation directory: DoveSaints.txt. It is used when importing data from Dove data files, to match towers against dove and to correct common mistakes and abbreviations in saints - but not all.
DoveSaints.txt, maps common saint names to the name in Dove. Often this is because they've changed, are often spelt differently, or just common mistakes. Feel free to change this at will, the format is:
"Real saint name","Doves abbreviation","Alt name 1","Alt Name 2", ...
A hash at the beginning of a line instructs Duco to ignore just that line. Names with space in, need to be enclosed in quotes: "xxx xxx". The first name on a line is the one that will be used once a tower is corrected.

Generating music.
There's one more text file in the installation directory, Music.txt, that is used to generate music data in compositions.
Lines starting with a "#" are ignored.
Lines starting with "!" are assumed to be roll ups to find anywhere in a change and the string after a "." is the name of this roll up type.
Otherwise lines are assumed to be changes to search for. Where an "x" is any bell, the first string after the first "." is the name of the change, and subsequent changes after "."'s are sub categories of the change to find.

Auto reindex on file open
On open a file, if duco detects that a reindex is necessary, it'll do it automatically. If for some reason you need to stop that, you can disable it via a registry key, set HKCU\SOFTWARE\Rocketpole\Duco\DisableAutoReindex to True
